
var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};


var ThoughtText = "New Thought";
var tableView = mainWindow.tableViews()[0];

target.frontMostApp().toolbar().buttons()["Compose"].tap();
target.frontMostApp().toolbar().buttons()[2].tap();
target.delay(4);
mainWindow.pickers()[0].wheels()[0].tapWithOptions({tapOffset:{x:0.36, y:0.87}});
mainWindow.textViews()[0].tap();
target.frontMostApp().keyboard().typeString(ThoughtText);
target.frontMostApp().navigationBar().rightButton().tap();
target.delay(2);
target.frontMostApp().navigationBar().rightButton().tap();
//target.delay();
tableView.cells()["Thoughts"].tap();
target.delay(5);
if (tableView.cells()[0].staticTexts()[ThoughtText].isValid())
	UIALogger.logPass("Posted new thought");