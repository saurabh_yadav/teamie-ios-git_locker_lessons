// Use-case: Login as as student user. Select user menu. View user profile & check if user profile screen appears. Logout and check if login screen appears again.

#import "ScreenCheckLibrary.js"

var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

var data = {
	username: "anuj-rajput",
	password: "Rajput007",
	fullname: "Anuj Rajput"
};

UIATarget.onAlert = function onAlert(alert) {
	var title = alert.name();
	target.frontMostApp().alert().defaultButton().tap();
	return true;
}

mainWindow.logElementTree();
mainWindow.textFields()["LoginUsername"].logElement();

if (checkIfLoginScreen()) {
	// login as a student user
	mainWindow.textFields()["LoginUsername"].tap();
	mainWindow.textFields()["LoginUsername"].setValue(data.username);
	mainWindow.secureTextFields()["LoginUserPassword"].tap();
	mainWindow.secureTextFields()["LoginUserPassword"].setValue(data.password);
	
	target.frontMostApp().mainWindow().buttons()["LoginButton"].tap();
}

// tap on the Show user Menu button
target.frontMostApp().navigationBar().leftButton().tap();
// view the user's profile
target.frontMostApp().mainWindow().tableViews()[0].cells()["My Profile"].tap();
// check if user is on user profile screen
if (checkIfUserProfileScreen(data)) {
	UIALogger.logPass("Navigation to User Profile Screen successful");
}
else {
	UIALogger.logFail("Navigation to User Profile Screen failed");
}

// tap on the user menu button once again
target.frontMostApp().mainWindow().scrollViews()[0].navigationBar().leftButton().tap();

// Select user logout
target.frontMostApp().mainWindow().tableViews()[0].cells()["Logout"].tap();

target.delay(2);

// Ensure user is logged out
if (checkIfLoginScreen()) {
	UIALogger.logPass("Logout operation successful");
}
else {
	UIALogger.logFail("Logout operation has not taken to home screen");
}