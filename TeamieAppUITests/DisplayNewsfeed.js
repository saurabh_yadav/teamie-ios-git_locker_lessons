
var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};

function checkIfLoginScreen() {
	if (mainWindow.textFields()["LoginUsername"].isValid()) {
		if (mainWindow.secureTextFields()["LoginUserPassword"].isValid()) {
			if (mainWindow.buttons()["LoginButton"].isValid()) {
				UIALogger.logPass("All Login elements present");
				return true;
			}
			else {
				UIALogger.logFail("Login button not present");
				return false;
			}
		}
		else {
			UIALogger.logFail("Password field not present");
			return false;
		}
	}
	else {
		UIALogger.logFail("Username field not present");
		return false;
	}
}

// If not logged in, then Login using specified login
function logIn() {
	if (!mainWindow.tableViews()[0].cells()[0].isValid()) {
		checkIfLoginScreen();
	
		// login as a student user
		mainWindow.textFields()["LoginUsername"].tap();
		mainWindow.textFields()["LoginUsername"].setValue(userData.username);
		mainWindow.secureTextFields()["LoginUserPassword"].tap();
		mainWindow.secureTextFields()["LoginUserPassword"].setValue(userData.password);

		mainWindow.textFields()[1].tap(); // Tap Institute Field
		mainWindow.textFields()[1].setValue(userData.institute); // Fill data in Institute Field	
		
		target.frontMostApp().mainWindow().buttons()["LoginButton"].tap();
	}
}

function logOut() {
	if (!mainWindow.textFields()["LoginUsername"].isValid()) {
		target.frontMostApp().navigationBar().leftButton().tap();
		mainWindow.tableViews()[0].cells()[0].staticTexts()[0].scrollToVisible();
		
		mainWindow.tableViews()[0].cells()["Logout"].tap();
		target.tap({x:230,y:290});
		//mainWindow.buttons()["Yes, log me out!"].tap();
	}
}

if (mainWindow.tableViews()[0].cells()[0].isValid() && mainWindow.tableViews()[0].cells()[0].buttons()["icon thought action"].isValid() && !mainWindow.staticTexts()["This classroom's Newsfeed is empty."].isValid()) {
	UIALogger.logPass("Newsfeed is present");
	
	// Navigate to classroom with Empty newsfeed
	//target.frontMostApp().navigationBar().leftButton().tap();
	//mainWindow.tableViews()[0].cells()["Class 1"].tap();
}

// Check if empty newsfeed message is displayed
else if (mainWindow.staticTexts()["This classroom's Newsfeed is empty."].isValid()) {
	UIALogger.logPass("Newsfeed is present: 0 thoughts");
		
	// Navigate to classroom with Empty newsfeed
	//target.frontMostApp().navigationBar().leftButton().tap();
	//mainWindow.tableViews()[0].cells()["My Newsfeed"].tap();
}
