var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

// return false if Login Screen. Return true otherwise
function checkIfLoginScreen() {
	if (mainWindow.textFields()["LoginUsername"].isValid()) {
		if (mainWindow.secureTextFields()["LoginUserPassword"].isValid()) {
			if (mainWindow.buttons()["LoginButton"].isValid()) {
				UIALogger.logPass("All Login elements present");
				return true;
			}
			else {
				UIALogger.logFail("Login button not present");
				return false;
			}
		}
		else {
			UIALogger.logFail("Password field not present");
			return false;
		}
	}
	else {
		UIALogger.logFail("Username field not present");
		return false;
	}
}

//checkIfLoginScreen();

function checkIfUserProfileScreen(userData) {
	mainWindow.scrollViews()[0].logElementTree();
	
	if (!mainWindow.scrollViews()[0].images()["UserProfileImage"].isValid()) {
		UIALogger.logFail("User Profile photo not present");
		return false;
	}
	
	if (!mainWindow.scrollViews()[0].staticTexts()["UserProfilePoints"].isValid()) {
		UIALogger.logFail("User Profile Teamie points not present");
		return false;
	}
	
	if (!mainWindow.scrollViews()[0].navigationBar().staticTexts()[userData.fullname].isValid()) {
		UIALogger.logFail(("Wrong User Profile name. Profile name must be: %@").replace("%@", userData.fullname));
		return false;
	}
	
	return true;
}


UIATextField.prototype["clear"] = function()
{
     if (!this.hasKeyboardFocus())
          this.tap();
     this.tapWithOptions({tapOffset:{x:0.8, y:0.5}});
};

UIAKeyboard.prototype["KEYBOARD_TYPE_UNKNOWN"] = -1;
UIAKeyboard.prototype["KEYBOARD_TYPE_ALPHA"] = 0;
UIAKeyboard.prototype["KEYBOARD_TYPE_ALPHA_CAPS"] = 1;
UIAKeyboard.prototype["KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION"] = 2;
UIAKeyboard.prototype["keyboardType"] = function()
    {
        if (this.keys().firstWithName("a").toString() != "[object UIAElementNil]")
            return this.KEYBOARD_TYPE_ALPHA;
        else if (this.keys().firstWithName("A").toString() != "[object UIAElementNil]")
            return this.KEYBOARD_TYPE_ALPHA_CAPS;
        else if (this.keys().firstWithName("1").toString() != "[object UIAElementNil]")
            return this.KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION;
        else
            return this.KEYBOARD_TYPE_UNKNOWN;
    };

UIATextField.prototype["typeString"] = function(pstrString, pbClear)
{
    if (!this.hasKeyboardFocus())
        this.tap();

    UIATarget.localTarget().delay(0.5);

    if (pbClear || pstrString.length == 0)
        this.clear();

    if (pstrString.length > 0)
    {
        var app = UIATarget.localTarget().frontMostApp();
        var keyboard = app.keyboard();
        var intKeyboardType = keyboard.keyboardType();
        var bIsAllCaps = (intKeyboardType == keyboard.KEYBOARD_TYPE_ALPHA_CAPS); //Handles autocapitalizationType = UITextAutocapitalizationTypeAllCharacters
        var intNewKeyboardType = intKeyboardType;
        var keys = app.keyboard().keys();
        var buttons = app.keyboard().buttons();
        for (i = 0; i < pstrString.length; i++)
        {
            var strChar = pstrString.charAt(i);
            if ((/[a-z]/.test(strChar)) && intKeyboardType == keyboard.KEYBOARD_TYPE_ALPHA_CAPS && !bIsAllCaps)
            {
                buttons.firstWithName("shift").tap();
                intKeyboardType = keyboard.KEYBOARD_TYPE_ALPHA;
            }
            else if ((/[A-Z]/.test(strChar)) && intKeyboardType == keyboard.KEYBOARD_TYPE_ALPHA)
            {
                buttons.firstWithName("shift").tap();
                intKeyboardType = keyboard.KEYBOARD_TYPE_ALPHA_CAPS;
            }
            else if ((/[A-z]/.test(strChar)) && intKeyboardType == keyboard.KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION)
            {
                buttons.firstWithName("more, letters").tap();
                intKeyboardType = keyboard.KEYBOARD_TYPE_ALPHA;
            }
            else if ((/[0-9.]/.test(strChar)) && intKeyboardType != keyboard.KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION)
            {
                buttons.firstWithName("more, numbers").tap();
                intKeyboardType = keyboard.KEYBOARD_TYPE_NUMBER_AND_PUNCTUATION;
            }

            if ((/[a-z]/.test(strChar)) && intKeyboardType == keyboard.KEYBOARD_TYPE_ALPHA_CAPS)
                strChar = strChar.toUpperCase();
            if (strChar == " ")
                keys["space"].tap();
            else if (/[0-9]/.test(strChar)) // Need to change strChar to the index key of the number because strChar = "0" will tap "1" and strChar = "1" will tap "2"
            {
                if (strChar == "0")
                    strChar = "9";
                else
                    strChar = (parseInt(strChar) - 1).toString();
                keys[strChar].tap()
            }
            else
                keys[strChar].tap();
            UIATarget.localTarget().delay(0.5);
        }
    }
};