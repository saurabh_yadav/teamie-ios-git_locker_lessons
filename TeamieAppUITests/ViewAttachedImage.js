
var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};

var tableView = mainWindow.tableViews()[0];

for (var i=0;i<tableView.cells().length;i++) {
	if (tableView.cells()[i].label.indexOf("/Attached Image/") != -1) {
		tableView.cells()[i].scrollToVisible();
		tableView.cells()[i].tap();

		if (target.frontMostApp().navigationBar().leftButton().label == "Back" && target.frontMostApp().navigationBar().search("/Shared by/").isValid())
			UIALogger.logPass("Opened Image");
	}
}