var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};

function checkIfLoginScreen() {
	if (mainWindow.textFields()["LoginUsername"].isValid()) {
		if (mainWindow.secureTextFields()["LoginUserPassword"].isValid()) {
			if (mainWindow.buttons()["LoginButton"].isValid()) {
				UIALogger.logPass("All Login elements present");
				return true;
			}
			else {
				UIALogger.logFail("Login button not present");
				return false;
			}
		}
		else {
			UIALogger.logFail("Password field not present");
			return false;
		}
	}
	else {
		UIALogger.logFail("Username field not present");
		return false;
	}
}

function logOut() {
	if (!mainWindow.textFields()["LoginUsername"].isValid()) {
		target.frontMostApp().navigationBar().leftButton().tap();
		mainWindow.tableViews()[0].cells()[0].staticTexts()[0].scrollToVisible();
		
		mainWindow.tableViews()[0].cells()["Logout"].tap();
		target.frontMostApp().alert().defaultButton().tap();
	}
}

// If not logged in, then Login using specified login
function logIn() {
	if (!mainWindow.tableViews()[0].cells()[0].isValid()) {
		checkIfLoginScreen();
	
		// login as a student user
		mainWindow.textFields()["LoginUsername"].tap();
		mainWindow.textFields()["LoginUsername"].setValue(userData.username);
		mainWindow.secureTextFields()["LoginUserPassword"].tap();
		mainWindow.secureTextFields()["LoginUserPassword"].setValue(userData.password);

		mainWindow.textFields()[1].tap(); // Tap Institute Field
		mainWindow.textFields()[1].setValue(userData.institute); // Fill data in Institute Field	
		
		target.frontMostApp().mainWindow().buttons()["LoginButton"].tap();
	}
}