
var target = UIATarget.localTarget();
var app = target.frontMostApp();
var mainWindow = app.mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};


//target.delay(5);

var tableView = mainWindow.tableViews()[0];

var clickedClassroomIndex = Math.floor(Math.random() * tableView.cells().length);
var classroomName = tableView.cells()[clickedClassroomIndex].value();

tableView.cells()[clickedClassroomIndex].scrollToVisible(); // Scroll to thought
tableView.cells()[clickedClassroomIndex].buttons()["Classroom Label"].tap(); // Tap on classroom label button
target.delay(5); // Wait for classroom newsfeed to load

var numThoughts = tableView.cells().length;

for (var cellIndex=0;cellIndex<numThoughts;cellIndex++) {
	if (tableView.cells()[clickedClassroomIndex].buttons()["Classroom Label"].isValid()) {
		if (tableView.cells()[clickedClassroomIndex].buttons()["Classroom Label"].value() == classroomName)
			UIALogger.logPass("Correct Classroom Thought: " + tableView.cells()[clickedClassroomIndex].buttons()["Classroom Label"].value());
		else
			UIALogger.logFail("Incorrect Classroom Thought: " + tableView.cells()[clickedClassroomIndex].buttons()["Classroom Label"].value());
	}
}

UIALogger.logMessage("All thoughts checked");