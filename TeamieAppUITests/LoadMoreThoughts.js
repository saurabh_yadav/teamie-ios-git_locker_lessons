
var target = UIATarget.localTarget();
var app = target.frontMostApp();
var mainWindow = app.mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};


//target.delay(5);


var tableView = mainWindow.tableViews()[0];
var newsfeedCount = mainWindow.tableViews()[0].cells().length;

UIALogger.logMessage("Newsfeed Entity Count: " + newsfeedCount);

tableView.cells()["Load More..."].scrollToVisible();
tableView.cells()["Load More..."].tap();

target.delay(5);

var updatedNewsfeedCount = mainWindow.tableViews()[0].cells().length;

if (updatedNewsfeedCount > newsfeedCount) {
	UIALogger.logWarning("New Newsfeed Entity Count: " + updatedNewsfeedCount);
	UIALogger.logPass("Loading more thoughts...");
}
else
	UIALogger.logFail("Not loading thoughts");