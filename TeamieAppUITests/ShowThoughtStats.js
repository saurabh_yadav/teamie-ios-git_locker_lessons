
var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};

// If not logged in, then Login using specified login
function logIn() {
	if (!mainWindow.tableViews()[0].cells()[0].isValid()) {
		checkIfLoginScreen();
	
		// login as a student user
		mainWindow.textFields()["LoginUsername"].tap();
		mainWindow.textFields()["LoginUsername"].setValue(userData.username);
		mainWindow.secureTextFields()["LoginUserPassword"].tap();
		mainWindow.secureTextFields()["LoginUserPassword"].setValue(userData.password);

		mainWindow.textFields()[1].tap(); // Tap Institute Field
		mainWindow.textFields()[1].setValue(userData.institute); // Fill data in Institute Field	
		
		target.frontMostApp().mainWindow().buttons()["LoginButton"].tap();
	}
}

function logOut() {
	if (!mainWindow.textFields()["LoginUsername"].isValid()) {
		target.frontMostApp().navigationBar().leftButton().tap();
		mainWindow.tableViews()[0].cells()[0].staticTexts()[0].scrollToVisible();
		
		mainWindow.tableViews()[0].cells()["Logout"].tap();
		target.tap({x:230,y:290});
		//mainWindow.buttons()["Yes, log me out!"].tap();
	}
}

var tableView = mainWindow.tableViews()[0];

for (var cellIndex=0;cellIndex<tableView.cells().length;cellIndex++)
{
	if (tableView.cells()[cellIndex].buttons()[2].isValid())
		// TODO: Add/Modify Accessibility Labels to every element
		UIALogger.logPass("Thought Stats: " + tableView.cells()[cellIndex].buttons()[2].label());
		// TODO: Change .buttons()[2] to .buttons()[<Like_Accessibility_Label>]	
	else
		UIALogger.logFail("Thought Stats Not Visible");
	
	tableView.cells()[cellIndex].scrollToVisible();
	target.delay(2);
}

