
var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};

// If not logged in, then Login using specified login
function logIn() {
	if (!mainWindow.tableViews()[0].cells()[0].isValid()) {
		checkIfLoginScreen();
	
		// login as a student user
		mainWindow.textFields()["LoginUsername"].tap();
		mainWindow.textFields()["LoginUsername"].setValue(userData.username);
		mainWindow.secureTextFields()["LoginUserPassword"].tap();
		mainWindow.secureTextFields()["LoginUserPassword"].setValue(userData.password);

		mainWindow.textFields()[1].tap(); // Tap Institute Field
		mainWindow.textFields()[1].setValue(userData.institute); // Fill data in Institute Field	
		
		target.frontMostApp().mainWindow().buttons()["LoginButton"].tap();
	}
}

function logOut() {
	if (!mainWindow.textFields()["LoginUsername"].isValid()) {
		target.frontMostApp().navigationBar().leftButton().tap();
		mainWindow.tableViews()[0].cells()[0].staticTexts()[0].scrollToVisible();
		
		mainWindow.tableViews()[0].cells()["Logout"].tap();
		target.tap({x:230,y:290});
		//mainWindow.buttons()["Yes, log me out!"].tap();
	}
}

var tableView = mainWindow.tableViews()[0];

for (var cellIndex=0;cellIndex<tableView.cells().length;cellIndex++)
{
	target.delay(2);
	tableView.cells()[cellIndex].buttons()[2].tap() 
	target.delay(2);
	
	if (mainWindow.scrollViews()[0].staticTexts()[0].isValid() && 
	mainWindow.scrollViews()[0].staticTexts()[1].isValid() && 
	target.frontMostApp().mainWindow().scrollViews()[0].buttons()["Inside Teamie"].isValid() &&
	target.frontMostApp().toolbar().textFields()[0].isValid())
		UIALogger.logPass("Thought View Did Open");
	else
		UIALogger.logPass("Thought View Did Not Open");
			
	target.frontMostApp().navigationBar().leftButton().tap();
}

mainWindow.logElementTree();