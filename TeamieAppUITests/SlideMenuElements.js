
var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};


var tableView = mainWindow.tableViews()[0];

tableView.cells()[0].dragInsideWithOptions({startOffset:{x:0.0, y:0.0}, endOffset:{x:0.8, y:0.0}, duration:0.1});

target.delay(0.1);
if (tableView.cells()["My Newsfeed"].isValid()) {
	UIALogger.logPass("Slide Menu opened");
	if (tableView.groups()["Personal"].isValid() && tableView.groups()["Classrooms"].isValid() && tableView.groups()["Settings"].isValid())
		UIALogger.logPass("All groups present");
}