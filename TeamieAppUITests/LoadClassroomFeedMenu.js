
var target = UIATarget.localTarget();
var app = target.frontMostApp();
var mainWindow = app.mainWindow();

var userData = {
	username: "anuj-rajput",
	password: "password",
	institute:"Instant Demo",
	fullname: "Anuj Rajput"
};


//target.delay(5);


target.frontMostApp().navigationBar().leftButton().tap();
var tableView = mainWindow.tableViews()[0];
var numMenuEntries = mainWindow.tableViews()[0].cells().length;
UIALogger.logPass("Num: " + numMenuEntries);

for (var menuEntryIndex=4;menuEntryIndex<=(numMenuEntries-3);menuEntryIndex++) {
	var classroomName = tableView.cells()[menuEntryIndex].label;
	mainWindow.tableViews()[0].cells()[menuEntryIndex].tap();
	target.delay(4);
	
	var numThoughts = tableView.cells().length;
	
	for (var cellIndex=0;cellIndex<numThoughts;cellIndex++) {
		if (tableView.cells()[cellIndex].buttons()["Classroom Label"].isValid()) {
			if (tableView.cells()[cellIndex].buttons()["Classroom Label"].value() == classroomName)
				UIALogger.logPass("Correct Classroom Thought: " + tableView.cells()[cellIndex].buttons()["Classroom Label"].value());
			else
				UIALogger.logFail("Incorrect Classroom Thought: " + tableView.cells()[cellIndex].buttons()["Classroom Label"].value());
		}
	}
	target.frontMostApp().navigationBar().leftButton().tap();
}
UIALogger.logMessage("All thoughts checked");