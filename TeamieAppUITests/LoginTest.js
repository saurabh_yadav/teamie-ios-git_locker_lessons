#import "ScreenCheckLibrary.js"

var target = UIATarget.localTarget();
var mainWindow = target.frontMostApp().mainWindow();

var doesAlertAppear = false;

UIATarget.onAlert = function onAlert(alert) {
	var title = alert.name();
	doesAlertAppear = true;
	return false;
}

CheckForIncorrectUsernamePassword(true, true);
CheckForIncorrectUsernamePassword(true, false);
CheckForIncorrectUsernamePassword(false, true);
CheckForIncorrectUsernamePassword(false, false);

function CheckForIncorrectUsernamePassword(enterUsername, enterPassword) {
	if (!checkIfLoginScreen()) {
		UIALogger.logFail("This is not a login screen");
		return;
	}

	UIALogger.logPass("Screen has all login elements");
	
	mainWindow.textFields()["LoginUsername"].tap();
	
	if (enterUsername) {
		mainWindow.textFields()["LoginUsername"].setValue("Test Username");
	}
	else {
		mainWindow.textFields()["LoginUsername"].setValue("");
	}
	
	mainWindow.secureTextFields()["LoginUserPassword"].tap();	
	
	if (enterPassword) {
		mainWindow.secureTextFields()["LoginUserPassword"].setValue("Test Password");
	}
	else {
		mainWindow.secureTextFields()["LoginUserPassword"].setValue("");
	}
	
	mainWindow.buttons()["LoginButton"].tap();
	target.delay(2);

	if (doesAlertAppear) {
		UIALogger.logPass("Incorrect username and password does not login");
		doesAlertAppear = false;
	}
	else {
		UIALogger.logFail("Incorrect username & password does not bring up alert");
	}
}