#Important
+ refreshToken workflow

#iOS 8 Compatibility
+ Registration for push notifications is a bit different, read [this](http://stackoverflow.com/questions/24454033/registerforremotenotificationtypes-is-not-supported-in-ios-8-0-and-later)

#Global Consistency
+ Change refresh to use UIRefreshControl instead of subclassing from PullRefreshTableViewController as [given](https://github.com/leah/PullToRefresh) by its creator
+ Make this refresh call for all places where the user needs to wait
  + Newsfeed
  + Lesson listing
+ Infinite load wherever possible
  + Notifications
  + Lesson listing

#Network
+ Look to allow cancelling of network requests if the viewcontroller is closed
+ Persistence (choose between core data and NSCoding - NSUserDefaults)

#UI stuff
+ Lesson reading screen UI trouble
+ Thought full view UI trouble
+ Newsfeed layout issues
+ Should error messages be always displayed or only in selective screens?

#Web Services stuff
+ User object to be consistent among all responses
+ Classroom object to be consistent among all responses
+ Datestamp should be in Unix timestamp format
+ Time limit should be in seconds
+ Boolean values should always be 0 or 1 (Not TRUE or FALSE)
+ Primary Keys (nid, uid, tid, etc), Numbers, Boolean, Datestamps, Time limit should be a number object
+ URLs should be absolute and not relative in most cases
