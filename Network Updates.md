#Network Updates
RestKit has been removed from the project and we are now using AFNetworking and Mantle for our networking, serialization (and in the near future, persistence) needs. 

**Why RestKit had to go** - the version we were using (.10) has lost community support in favour of the next version (.20). Upgrading to this new version (which uses AFNetworking internally) would have required a similar level of effort. Furthermore found RestKit too complicated and not well documented enough - a look at it's Github [README](https://github.com/RestKit/RestKit/blob/development/README.md) is enough to scare most people. More importantly, it doesn't at all fit well into our code from a Software Engineering standpoint (request building, response parsing and mapping all in one enormous file).

**How is what we have now better** - A more streamlined process for making requests, parsing logic modularized into it's own seperate classes and an architecture which does not make you pull your hair out trying to understand it.

##A brief intro of the participants

###AFNetworking [Repo](https://github.com/AFNetworking/AFNetworking)
> A delightful iOS and OS X networking framework [http://afnetworking.com](http://afnetworking.com)

Considered the de-facto standard among Cocoa networking libraries, provides a very nice wrapper around NSURLConnection without going overboard (think RestKit)

**Understanding the AFNetworking API is *not required* but definitely helpful while creating new API requests**

###Mantle [Repo](https://github.com/Mantle/Mantle)
> Model framework for Cocoa and Cocoa Touch

Provides a simple model layer which does much of the boilerplate coding for us (`<NSCopying>`, `<NSCoding>`, `-isEqual:`, `-hash`), with helper functions for mapping responses to classes and a smooth integration with Core Data for persistance.

**Understanding the [Mantle API](https://github.com/Mantle/Mantle/blob/master/README.md) is *necessary* for creating new mapping objects**. That page provides all the information needed in a concise, clear fashion.

##New Architecture
`AFRequestOperationManager` is subclassed in `TMEBaseClient` which contains all the request related methods. `MTLModel` is subclassed in `TMEModel`, which is a parent class for all our models.

###Request
All requests are made through a singleton class, and the success, failure and completion (which happens after both success and failure) cases are handled through blocks which are passed along with the request. The class provides two variations of the request method which are documented in the class.

The blocks have access to variables outside the block (`self` will behave as normal) and we do not have to worry about the viewcontroller going out of memory or causing a strong retain cycle.

###Mapping
The response returned by the request would already have parsed the JSON to a `NSDictionary` (or `NSArray`), which can be then mapped to any subclass of `TMEModel` if necessary, else can use the response object.

##Example

Making a request, parsing and storing it in a local array
```
self.classrooms = [NSArray new];

[Teamie]
[[TMEClient sharedClient]
  request:TeamieUserNewsfeedRequest
  parameters:@{@"nid": self.nid}
  completion:^{
    [TeamieGlobals dismissActivityLabels];
  }
  success:^(NSDictionary *response) {
    self.classrooms = [TMEClassroom parseArray:response[@"classrooms"]];
  }
  failure:nil //All failures are logged by default, so extra actions can happen here
];
```