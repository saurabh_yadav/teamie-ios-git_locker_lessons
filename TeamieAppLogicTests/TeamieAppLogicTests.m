//
//  TeamieAppLogicTests.m
//  TeamieAppLogicTests
//
//  Created by Ramchander Krishna on 4/2/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TeamieAppLogicTests.h"
#import "TeamieGlobals.h"

@implementation TeamieAppLogicTests

- (void)setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

// To test the extractNumberFromURL: method in TeamieGlobals.h

- (void)testExtractNumberFromURL {
    
    NSString* string1 = @"<a href=\"https://instant-qa.theteamie.com/people/john-praveen\">John Praveen</a> has responded to <a href=\"/people/ben\">ben</a>'s <a href=\"/thought/457\">question</a> in <a href=\"/class/Maths-classroom\">Maths classroom</a> <br/><br/><span class=\"thought-comment-body span18 fluid text closerBlack field-item even\">No</span>";
    NSString* string2 = @"<a href=\"https://instant.theteamie.com/user/19\">John Praveen</a> has echoed your <a href=\"/profile/5148\">question</a> in <a href=\"/class/Maths-classroom\">Maths classroom</a>";
    
    STAssertNil([TeamieGlobals extractNumberFromURL:string1 prefix:@"newsfeed"], @"Extracts number when there is no number in URL");
    STAssertEqualObjects([TeamieGlobals extractNumberFromURL:string2 prefix:@"profile"], @"5148", @"Unable to extract number from URL string correctly");
    // STAssertNotNil([TeamieGlobals extractNumberFromURL:string1 prefix:@"thought/"], @"Temporary fail test case");
    
}

// To test the appFontFor: method in TeamieGlobals.h

- (void)testAppFontFor {
    
    STAssertTrue([[TeamieGlobals appFontFor:nil] isKindOfClass:[UIFont class]], @"Returns a not-nil Font even if input given is nil");
    
    STAssertTrue([[TeamieGlobals appFontFor:@"aslkdjaslkdj"] isKindOfClass:[UIFont class]], @"Returns a not-nil font even for a garbage input");
}

// To test the createFileOfType:withData:andName: method

- (void)testCreateFile {
    
    NSArray* fileData = [NSArray arrayWithObjects:@"Test Col 1", @"Test Col 2", @"\n10,5", @"\n34,45", @"\n23,65", nil];
    
    NSDictionary* fileInfo1 = [TeamieGlobals createFileOfType:@"csv" withData:fileData andName:@"TestFile1"];
    
    // Make a set of assertions to ensure the file was created as expected
    STAssertNotNil(fileInfo1, @"Creates a CSV file with some simple test data.");
    STAssertTrue([fileInfo1 valueForKey:@"filename"] && [fileInfo1 valueForKey:@"mimetype"] && [fileInfo1 valueForKey:@"filepath"], @"The returned file info contains the expected dictionary keys");
    STAssertNotNil([NSData dataWithContentsOfFile:[fileInfo1 valueForKey:@"filepath"]], @"The CSV file created is not empty.");
    
    NSDictionary* fileInfo2 = [TeamieGlobals createFileOfType:@"excel" withData:fileData andName:@"TestFile2"];
    STAssertNil(fileInfo2, @"Does not create file of any type other than csv");
    
}

@end
