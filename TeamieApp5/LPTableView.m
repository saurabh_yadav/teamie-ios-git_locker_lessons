//
//  LPTableView.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 18/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "LPTableView.h"
@interface LPTableView()

@property (nonatomic,readwrite) NSNumber *index;

@end

@implementation LPTableView

- (id)initWithNumberOfLessonPages:(int)numberOfLessonPages index:(int)index
{
    self = [super initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width - (2*Padding), RowHeight * numberOfLessonPages) style:UITableViewStylePlain];
    
    if (self) {
        self.index = [NSNumber numberWithInt:index];
    }
    
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
