//
//  ScoreBarView.m
//  TeamieApp5
//
//  Created by Raunak on 18/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ScoreBarView.h"

@interface ScoreBarView ()
@property (nonatomic, strong) UIView* baseView;
@property (nonatomic, strong) UIView* barView;
@end

@implementation ScoreBarView

- (id)initWithFrame:(CGRect)frame barColor:(UIColor*)barColor baseColor:(UIColor*)baseColor barRatio:(CGFloat)ratio
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _barColor = barColor;
        _baseColor = baseColor;
        _barRatio = ratio;
        _baseView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        _baseView.backgroundColor = baseColor;
        _barView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, frame.size.height)];
        _barView.backgroundColor = barColor;
        _barView.alpha = 0.5;
        _baseView.alpha = 0.5;
        [self addSubview:_baseView];
        [self addSubview:_barView];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame barColor:(UIColor*)barColor baseColor:(UIColor*)baseColor {
    return [self initWithFrame:frame barColor:barColor baseColor:baseColor barRatio:0.0];
}

- (id)initWithFrame:(CGRect)frame {
    return [self initWithFrame:frame barColor:[UIColor greenColor] baseColor:[UIColor redColor] barRatio:0.0];
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return [self initWithFrame:[self frame]];
}

- (void)setBarRatio:(CGFloat)barRatio {
    if (barRatio < 0) {
        _barRatio = 0.0;
    }
    else if (barRatio > 1) {
        _barRatio = 1.0;
    }
    else {
        _barRatio = barRatio;
    }
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    self.baseView.backgroundColor = self.baseColor;
    self.barView.backgroundColor = self.barColor;
    self.barView.frame = CGRectMake(0, 0, (self.barRatio * self.frame.size.width), self.frame.size.height);
}


@end
