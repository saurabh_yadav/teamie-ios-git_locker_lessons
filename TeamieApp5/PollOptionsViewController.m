//
//  PollOptionsViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 23/05/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "PollOptionsViewController.h"
#import "TeamieAppDelegate.h"
#import "ShareBoxPollOptions.h"
#import "ShareBoxViewController.h"

@interface PollOptionsViewController ()

@end

@implementation PollOptionsViewController

@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize context = _context;
@synthesize entity = _entity;
@synthesize addItemCell = _addItemCell;
@synthesize delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // set background image for tableview
//    UIImage *bgImage = [UIImage imageNamed:@"bg.jpg"];
//    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:bgImage];
//    [bgImageView setFrame:self.tableView.frame];
    UIColor* color = [UIColor colorWithHex:@"#b5b5b5" alpha:1.0];
    [self.tableView setBackgroundColor:color];
    
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.tableView.frame = CGRectMake(0, self.tableView.frame.origin.y + 22.0, self.tableView.frame.size.width, self.tableView.frame.size.height - 22.0);
    }
//    [self.tableView setBackgroundView:bgImageView];
    
    
    NSError *error = nil;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    
}


- (void)viewDidUnload {
    [self setTableView:nil];
    [self setNavigationBar:nil];
    [self setTableNavigationItem:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// set/unset editing table view 
-(IBAction)editButtonAction:(id)sender {
    
    if([self.tableView isEditing]) {
        [self.tableView setEditing:NO animated:YES];
        self.tableNavigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editButtonAction:)];
    } else {
        [self.tableView setEditing:YES animated:YES];
        self.tableNavigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(editButtonAction:)];
    }
    
}
// dismiss modalview
- (IBAction)dismissPollModal:(UIBarButtonItem*)sender {
    [self dismissModalViewControllerAnimated:YES];
    if (sender.tag == 2) {  // Only if the Done button is tapped, then notify the delegate of the dismissal. Because then the poll options will be saved by the delegate
        [self.delegate pollOptionsViewControllerDidDismiss];
    }
}

- (void)deleteExistingPollOptions {
    NSFetchRequest * allPollOptions = [[NSFetchRequest alloc] init];
    [allPollOptions setEntity:[NSEntityDescription entityForName:@"ShareBoxPollOptions" inManagedObjectContext:self.context]];
    [allPollOptions setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * options = [self.context executeFetchRequest:allPollOptions error:&error];
    //error handling goes here
    for (NSManagedObject * option in options) {
        [self.context deleteObject:option];
    }
    NSError *saveError = nil;
    [self.context save:&saveError];
}


#pragma mark - Table view data source


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [[self.fetchedResultsController sections] count] + 1; // + 1 extra section for adding new items
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // if section = 0 then it is for adding new items , we need only 1 row in it
    if(section == 0) {
        return 1;
    }
    
    // get count of items from fetchedresultscontroller
    id<NSFetchedResultsSectionInfo> secInfo = [[self.fetchedResultsController sections] objectAtIndex:section - 1];
    return [secInfo numberOfObjects]; 
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // if section is = then we need cell for adding new items , we have a property for this
    if(indexPath.section == 0) {
        return self.addItemCell;
    }
    
    // creating cells for items
    static NSString *CellIdentifier = @"pollItemCell";
    PollItemCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];

    // setting Item object for cell
    cell.currentItem = [self.fetchedResultsController objectAtIndexPath:[self subSectionToIndexPath:indexPath]];
    // setting itemDelegate to self , we will update context after cell controls update Item object
    cell.itemDelegate = self;
    // custom initialization for custom cell , this will set background image and alpha , right and left button images and label
    [cell customInit];
    
    return cell;
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {

        return @"Poll Options";
}

// we want to have custom view for header , without its background color/image
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    // we do not want any custom view for new item section , just return empty view
    if(section == 0) {
        return [[UIView alloc] initWithFrame:CGRectZero];
    }
    
    // create new label
    UILabel *temp = [[UILabel alloc] init];
    // assign text to label from nsfetchresultscontroller
    temp.text = [self tableView:tableView titleForHeaderInSection:section];
    // white color text
//    temp.textColor = [UIColor whiteColor];
    // clear background , we do not want any custom background fot this label
    temp.backgroundColor = [UIColor clearColor];
    // align center label`s text
    temp.textAlignment = NSTextAlignmentCenter;
    // make font a little bit more than Items font
    temp.font = [UIFont boldSystemFontOfSize:18];
    // make size to fit view
    [temp sizeToFit];
    return temp;
}



// we do not want our Add New Item row to be Delete-able , so we will return NO for this item
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
     return indexPath.section != 0;
 }
 

// this is called when object is edited , we will listen only for delete right now
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
       // get Item from fetchresult controller wich was deleted from tableview
        ShareBoxPollOptions *it = [self.fetchedResultsController objectAtIndexPath:[self subSectionToIndexPath:indexPath]];
        // Delete object from context
        [self.context deleteObject:it];
        // create error holder
        NSError *error = nil;
        // save contect after deleting an object
        [self.context save:&error];
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // we do not want anything if add new cell was selected
    if(indexPath.section == 0) {
        return;
    }
}

// while TextField is focused and user scrolls tableview , we want to hide keyboard
-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self hideKeyBoard];
}

#pragma mark -
#pragma mark Fetched Results Controller
// Initialize fetchresultController
-(NSFetchedResultsController *)fetchedResultsController {
    
    // if we already have fetchresultscontroller just return it
    if(_fetchedResultsController == nil) {
        // create new request
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        // add Item entity
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"ShareBoxPollOptions" inManagedObjectContext:self.context];
        [fetchRequest setEntity:entity];
        
        [fetchRequest setEntity:self.entity];
        // order by order number ascending
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:YES];
        // create array for containing our sort descriptors
        NSArray *sortDescriptors = @[sortDescriptor];
        // set sort descriptors to fetchrequest
        [fetchRequest setSortDescriptors:sortDescriptors];
        _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.context sectionNameKeyPath:nil cacheName:nil];
        
        _fetchedResultsController.delegate = self;

    }
    
    return _fetchedResultsController;
}
// fetched results controller has some changes , we will update our table view  , so prepare tableview for it
-(void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}
// fetched results controller changed object , we updated our tableview
-(void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}

// this method gets called while nsfetchresultscontroller changes objects , we will perform our actions according changetype
-(void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    

    
    switch (type) {
            // fetchedresultscontroller has a new item  , so lets add it to our tableview
        case NSFetchedResultsChangeInsert:
            [self.tableView insertRowsAtIndexPaths:@[[self addSectionToIndexPath:newIndexPath]] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            // fetchedresultscontroller removed an item , so lets remove from tableview also
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteRowsAtIndexPaths:@[[self addSectionToIndexPath:indexPath]] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
            //fetchedresultscontroller changed an object , we want to update cell for that row
        case NSFetchedResultsChangeUpdate: {
            PollItemCell *cell = (PollItemCell *)[self.tableView cellForRowAtIndexPath:[self addSectionToIndexPath:indexPath]];
            [cell updateCell];
        }
            break;
            // fetchedresutsctonroller moved object , also move it in our tableview
        case NSFetchedResultsChangeMove:
            [self.tableView deleteRowsAtIndexPaths:@[[self addSectionToIndexPath:indexPath]] withRowAnimation:UITableViewRowAnimationAutomatic];
            [self.tableView insertRowsAtIndexPaths:@[[self addSectionToIndexPath:newIndexPath]] withRowAnimation:UITableViewRowAnimationAutomatic];
    }

    
}

// this method gets called when WHOLE section is changed , if there is no rows in sections it is deleted from our tableview
-(void)controller:(NSFetchedResultsController *)controller didChangeSection:(id<NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch (type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex + 1] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex + 1] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
    
}


#pragma mark -
#pragma mark PollItemCell Delegate
// PollItemCell Delegate method , after changing an object , we want to persist it and update our tableview
-(void)cellItemDidUpdate {
    
    NSError *error = nil;
    
    [self.context save:&error];
}

#pragma mark -
#pragma mark Menu & Detail view delegate
// user has made changes , lets persist it and nsfetchedresultscontroller will be updated
-(void)saveItem {
    
    NSError *error = nil;
    
    [self.context save:&error];
}

#pragma mark -
#pragma mark Lazy Inits

// These are some basic lazy initializations

-(NSEntityDescription *)entity {
    if(_entity == nil) {
        _entity = [NSEntityDescription entityForName:@"ShareBoxPollOptions" inManagedObjectContext:self.context];
    }
    
    return _entity;
}

-(NSManagedObjectContext *)context {
    
    if(_context == nil) {
        TeamieAppDelegate *del = [[UIApplication sharedApplication] delegate];
        _context = del.managedObjectContext;
    }
    
    return _context;
}


-(NewPollItemCell *)addItemCell {
    if(_addItemCell == nil) {
        _addItemCell = [self.tableView dequeueReusableCellWithIdentifier:@"newPollItemCell"];
        
        if(_addItemCell == nil) {
            _addItemCell = [[NewPollItemCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"newPollItemCell"];
        }
        [_addItemCell customInit];
        
        // we want to have toolbar in keyboard with Add/Cancel button
        UIToolbar *keyBoardToolBar = [[UIToolbar alloc] init];
        keyBoardToolBar.barStyle = UIBarStyleDefault;
        [keyBoardToolBar setTranslucent:YES];
        [keyBoardToolBar sizeToFit];
        
        // flexible space between buttons
        UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *addNewItemButton = [[UIBarButtonItem alloc] initWithTitle:@"Add Poll Option" style:UIBarButtonItemStyleBordered target:self action:@selector(addNewItem:)];
        addNewItemButton.tintColor = [UIColor colorWithHex:@"#BE202E" alpha:0.8];

        UIBarButtonItem *cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelButtonTouched:)];
        keyBoardToolBar.items = @[cancel,space,addNewItemButton];
        
        _addItemCell.textField.inputAccessoryView = keyBoardToolBar;
        _addItemCell.textField.rightViewMode = UITextFieldViewModeAlways;
    }
    
    return _addItemCell;
}

#pragma mark -
#pragma mark NSindexPath Add/Substract Section

// we need to recalculate indexPath because of our extra section for adding new items
-(NSIndexPath *)addSectionToIndexPath:(NSIndexPath *)indexPath {
    return [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section + 1];
}

-(NSIndexPath *)subSectionToIndexPath:(NSIndexPath *)indexPath {
    return [NSIndexPath indexPathForRow:indexPath.row inSection:indexPath.section - 1];
}

#pragma mark -
#pragma mark TextField & New Item Methods
// hide new item textfield`s keyboard and set its text to empty string
-(void)hideKeyBoard {
    [self.addItemCell.textField setText:@""];
    [self.addItemCell.textField resignFirstResponder];
}

// User wants to add new item , lets do it
-(void)addNewItem:(id)sender {
    
    // if there is no text entered , we will just return for now , also we could inform user about this , or we could tell them what is minimum length of Item title
    if([self.addItemCell.textField.text length] < 1) {
        [self hideKeyBoard];
        return;
    }
    
    // if there is something in textfield , we create a new object
    ShareBoxPollOptions *it = [[ShareBoxPollOptions alloc] initWithEntity:self.entity insertIntoManagedObjectContext:self.context];
    // customize it
    NSNumber *order = 0;
    if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"pollOptionsOrder"] intValue] != 0) {
        order = [NSNumber numberWithInt:[[[NSUserDefaults standardUserDefaults] stringForKey:@"pollOptionsOrder"] intValue]];
    }

    order = [NSNumber numberWithInt:[order integerValue] + 1];
    it.title = self.addItemCell.textField.text;
    it.order = [NSNumber numberWithInt:[[[NSUserDefaults standardUserDefaults] stringForKey:@"pollOptionsOrder"] intValue]];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@", order] forKey:@"pollOptionsOrder"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    NSError *error = nil;
    // and save 
    [self.context save:&error];
    
    [self hideKeyBoard];
}
// user canceled adding new item  , just hide keyboard and set its content to empty string
-(void)cancelButtonTouched:(id)sender {
    [self hideKeyBoard];
}

#pragma mark UIGestureRecognizer private methods
- (IBAction)handleLongPress:(UILongPressGestureRecognizer*)sender {
    //TODO: Editing/Rearranging on LongPressGesture
}

@end
