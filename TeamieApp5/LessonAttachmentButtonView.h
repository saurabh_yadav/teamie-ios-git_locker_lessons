//
//  LessonAttachmentButtonView.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 23/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonAttachmentButtonView : UIView
@property (strong, nonatomic) IBOutlet UIButton *button;
@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) IBOutlet UILabel *label;

@end
