//
//  NewsfeedItemComment.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class NewsfeedItem, UserProfile;

@interface NewsfeedItemComment : NSManagedObject

@property (nonatomic, retain) NSNumber * cid;
@property (nonatomic, retain) NSNumber * created;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSString * htmlMessage;
@property (nonatomic, retain) NSString * timestampText;
@property (nonatomic, retain) NSNumber * voteCount;
@property (nonatomic, retain) NSNumber * isCorrect;
@property (nonatomic, retain) UserProfile *author;
@property (nonatomic, retain) NewsfeedItem *newsfeedItem;


// Comment Actions
@property (nonatomic, retain) NSNumber * markRightAccess;
@property (nonatomic, retain) NSNumber * reportAccess;
@property (nonatomic, retain) NSNumber * markRightStatus;
@property (nonatomic, retain) NSNumber * reportStatus;
@property (nonatomic, retain) NSNumber * voteUpStatus;
@property (nonatomic, retain) NSNumber * voteDownStatus;

@end