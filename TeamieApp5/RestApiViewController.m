//
//  RestApiViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/26/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "RestApiViewController.h"

@implementation RestApiViewController

@synthesize apiCaller = _apiCaller;
@synthesize parentRevealController = _parentRevealController;
@synthesize coachMarksView;

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

#ifdef DEBUG
    NSLog(@"The device token: %@. User logged in: %d. Device registered: %d", [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsDeviceToken"], [self.apiCaller teamieUserIsLoggedIn], [[NSUserDefaults standardUserDefaults] boolForKey:@"hasRegisteredDevice"]);
#endif
    
    //if user is logged in and the APNS Device token is set, then contact server to save the token
    if ([self.apiCaller teamieUserIsLoggedIn] && [[NSUserDefaults standardUserDefaults] boolForKey:@"hasRegisteredDevice"] == NO) {
        id deviceToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"apnsDeviceToken"];
        if (deviceToken) {
            [self.apiCaller performRestRequest:TeamieUserDeviceRegisterRequest withParams:[NSDictionary dictionaryWithKeysAndObjects:@"token", deviceToken, @"type", @"ios", nil]];
        }
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveRefreshTokenNotification:)
                                                 name:@"refreshTokenNotification"
                                               object:nil];
    
    // Add event listeners for user menu navigation
    if ([self.parentRevealController respondsToSelector:@selector(addEventListeners:)]) {
        [self.parentRevealController performSelector:@selector(addEventListeners:) withObject:self];
    }
    
    if (self.navigationController && self.navigationController.navigationBarHidden == NO) {
        // set the color of the navigation bar
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithHex:@"#BE202E" alpha:1.0]];
        //self.navigationBarTintColor = [UIColor colorWithHex:@"#be202e" alpha:1.0];
    }
}

- (void)viewDidUnload {
    [[[[RKObjectManager sharedManager] client] requestQueue] cancelRequestsWithDelegate:self.apiCaller];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.parentRevealController = nil;
    self.apiCaller = nil;
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // when a RestApiViewController is about to disappear dismiss any activity labels related to it.
    [TeamieGlobals dismissActivityLabels];
    
    // cancels active REST requests when ViewController is about to disappear
    //[TeamieGlobals cancelTeamieRestRequests];
}

-(TeamieRestAPI*)apiCaller {
    if (_apiCaller == nil) {
        _apiCaller = [[TeamieRestAPI alloc] init];
        _apiCaller.delegate = self;
        // notify self if there is a change in the network availability status
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:RKReachabilityDidChangeNotification object:_apiCaller];
    }
    return _apiCaller;
}

#pragma mark - TeamieRESTRequest delegate methods
-(void)requestSuccessful:(TeamieRESTRequest)requestName {
    [TeamieGlobals dismissActivityLabels];
#ifdef DEBUG
    NSLog(@"RestApiViewController: Request#%i Successful", requestName);
#endif
}

-(void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    [TeamieGlobals dismissActivityLabels];
#ifdef DEBUG
    NSLog(@"RestApiViewController: Request#%i Successful with response: %@", requestName, response);
#endif
}

-(void)requestFailed:(NSString *)message forRequest:(TeamieRESTRequest)requestName {
    [TeamieGlobals dismissActivityLabels];
    if (message != nil) {
#ifdef DEBUG
        NSLog(@"Request message: %@", message);
#endif
        [TeamieGlobals addTSMessageInController:self title:TeamieLocalizedString(@"MSG_REQUEST_FAILED", nil) message:message type:@"error" duration:5 withCallback:nil];
    }
}

-(void)requestLoading:(float)progress forRequest:(TeamieRESTRequest)requestName {
    // nothing much to do
}

-(void)reachabilityChanged:(NSNotification *)notification {
    // to prevent multiple notifications from popping up
    static int numNotifications = 0;
    
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    if (![observer isNetworkReachable]) {
        [TeamieGlobals dismissActivityLabels];
        if (numNotifications == 0) {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:TeamieLocalizedString(@"NETWORK_UNAVAILABLE", nil) message:TeamieLocalizedString(@"NETWORK_UNAVAILABLE_ERROR", nil) delegate:self cancelButtonTitle:TeamieLocalizedString(@"BTN_OK", nil) otherButtonTitles:nil];
            [alert show];
        }
        numNotifications++;
    }
    else {
        numNotifications = 0;
    }
}

#pragma mark - NSNotification methods

- (void)receiveRefreshTokenNotification:(NSNotification*)notification {
    // this method gets called after the refresh token generates a new access token
}

#pragma mark - UIView Rotation Methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    return YES;
}

/*- (void)reachabilityChanged:(NSNotification*)notification {
    RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
    
    if ([observer isNetworkReachable]) {
#ifdef DEBUG
        NSLog(@"We're online! %@", [self class]);
#endif
    } else {
#ifdef DEBUG
        NSLog(@"We've gone offline! %@", [self class]);
#endif
    }
}*/

#pragma mark - Lazy Instantiation methods

-(void)setParentRevealController:(id)parentRevealController {
    
    _parentRevealController = parentRevealController;
    
    // Add event listeners for user menu navigation
    if ([_parentRevealController respondsToSelector:@selector(addEventListeners:)]) {
        [_parentRevealController performSelector:@selector(addEventListeners:) withObject:self];
    }
    
}

@end
