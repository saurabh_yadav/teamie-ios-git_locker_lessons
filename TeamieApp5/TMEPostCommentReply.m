//
//  TMEPostCommentReply.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 19/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostCommentReply.h"

@implementation TMEPostCommentReply

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    mapping[@"cid"] = @"rid";
    return [mapping copy];
}

+ (Class)classForParsingJSONDictionary:(NSDictionary *)JSONDictionary {
    return TMEPostCommentReply.class;
}

@end