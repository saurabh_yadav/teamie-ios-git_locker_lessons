//
//  TMEBadge.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEBadge.h"

@implementation TMEBadge

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"name": @"badge.name",
        @"bid": @"badge.bid",
        @"date": @"badge.timestamp",
        @"awardedBy": @"badge.awarded_by_uid",
        @"href": @"badge.href",
    };
}

+ (NSValueTransformer *)bidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)awardedByJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)dateJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)hrefJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

@end
