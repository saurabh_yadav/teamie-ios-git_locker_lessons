//
//  LSCollapseClick.h
//  CollapseClickDemo
//
//  Created by Wei Wenbo on 28/5/14.
//  Copyright (c) 2014 Ben Gordon. All rights reserved.
//

#import "CollapseClick.h"
#import "TMELesson.h"

@protocol LSCollapseClickDelegate<CollapseClickDelegate>
@required
- (TMELesson *)lesson:(int)index;
- (NSNumber *)classroomID ;
- (NSNumber *)classroomIDforIndex:(int) index;
@optional
- (UIColor *) colorForLessonInfo:(int)index;
- (void) didClickedLessonHeaderButton:(int)index;
-(void) didclickAuthorImageButton:(int) index;
@end

@interface LSCollapseClick : CollapseClick
@property (weak) id<LSCollapseClickDelegate> CollapseClickDelegate;
@end
