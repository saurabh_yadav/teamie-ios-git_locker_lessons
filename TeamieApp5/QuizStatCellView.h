//
//  QuizStatCellView.h
//  TeamieApp5
//
//  Created by Raunak on 28/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoreBarView.h"

@interface QuizStatCellView : UIView
@property (strong, nonatomic) IBOutlet UILabel *textLabel;
@property (strong, nonatomic) IBOutlet ScoreBarView *statBar;

+(QuizStatCellView*)cellWithText:(NSString*)text value:(NSNumber*)value baseValue:(NSNumber*)baseValue;

@end
