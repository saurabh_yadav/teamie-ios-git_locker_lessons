//
//  UserProfileCollapseClick.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 7/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UserProfileCollapseClick.h"
#import "UserProfileCollapseClickCell.h"

@interface CollapseClick()

-(void)didSelectCollapseClickButton:(UIButton *)titleButton;

@end

@implementation UserProfileCollapseClick

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - Load Data
-(void)reloadCollapseClick {
    // Set Up: Height
    float totalHeight = 0;
    
    // If Arrays aren't Init'd, Init them
    if (!(self.isClickedArray)) {
        self.isClickedArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
    }
    
    if (!(self.dataArray)) {
        self.dataArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
    }
    
    // Make sure they are clear
    [self.isClickedArray removeAllObjects];
    [self.dataArray removeAllObjects];
    
    // Remove all subviews
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
    
    // Add cells
    for (int xx = 0; xx < [CollapseClickDelegate numberOfCellsForCollapseClick]; xx++) {
        // Create Cell
        UserProfileCollapseClickCell *cell = [UserProfileCollapseClickCell newCollapseClickCellWithTitle:[CollapseClickDelegate titleForCollapseClickAtIndex:xx] index:xx content:[CollapseClickDelegate viewForCollapseClickContentViewAtIndex:xx]];
        
        
        // Set cell.TitleView's backgroundColor
        if ([(id)CollapseClickDelegate respondsToSelector:@selector(colorForCollapseClickTitleViewAtIndex:)]) {
            cell.TitleView.backgroundColor = [CollapseClickDelegate colorForCollapseClickTitleViewAtIndex:xx];
        }
        else {
            cell.TitleView.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1.0];
        }
        
        
        // Set cell.TitleLabel's Color
        if ([(id)CollapseClickDelegate respondsToSelector:@selector(colorForTitleLabelAtIndex:)]) {
            cell.TitleLabel.textColor = [CollapseClickDelegate colorForTitleLabelAtIndex:xx];
        }
        else {
            cell.TitleLabel.textColor = [UIColor whiteColor];
        }
        
        
        // Set cell.TitleArrow's Color
        if ([(id)CollapseClickDelegate respondsToSelector:@selector(colorForTitleArrowAtIndex:)]) {
            //            [cell.TitleArrow drawWithColor:[CollapseClickDelegate colorForTitleArrowAtIndex:xx]];
            
            cell.arrow.image = [TeamieUIGlobals defaultPicForPurpose:@"right-arrow" withSize:25 andColor:[CollapseClickDelegate colorForTitleArrowAtIndex:xx]];
        }
        else {
            //            [cell.TitleArrow drawWithColor:[UIColor colorWithWhite:0.0 alpha:0.35]];
            cell.arrow.image = [TeamieUIGlobals defaultPicForPurpose:@"right-arrow" withSize:25 andColor:[UIColor colorWithWhite:0.0 alpha:0.35]];
        }

        
        // Set cell.TitleView font
        cell.TitleLabel.font = [TeamieGlobals appFontFor:@"userProfileSectionTitleText"];
        
        // Set cell.ContentView's size
        cell.ContentView.frame = CGRectMake(0, UserProfileHeaderHeight + HeaderContentPaddings, self.frame.size.width, cell.ContentView.frame.size.height);
        
        // Set cell's size
        cell.frame = CGRectMake(0, totalHeight, self.frame.size.width, UserProfileHeaderHeight);
        
        
        // Add target to Button
        [cell.TitleButton addTarget:self action:@selector(didSelectCollapseClickButton:) forControlEvents:UIControlEventTouchUpInside];
        
        // Add cell
        [self addSubview:cell];
        
        // Add to DataArray & isClickedArray
        [self.isClickedArray addObject:[NSNumber numberWithBool:NO]];
        [self.dataArray addObject:cell];
        
        // Calculate totalHeight
        totalHeight += UserProfileHeaderHeight + CellDistance;
    }
    
    // Set self's ContentSize and ContentOffset
    [self setContentSize:CGSizeMake(self.frame.size.width, totalHeight)];
    [self setContentOffset:CGPointZero];
}

#pragma mark - Reposition Cells
-(void)repositionCollapseClickCellsBelowIndex:(int)index withOffset:(float)offset {
    for (int yy = index+1; yy < self.dataArray.count; yy++) {
        CollapseClickCell *cell = [self.dataArray objectAtIndex:yy];
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y + offset, cell.frame.size.width, cell.frame.size.height);
    }
    
    // Resize self.ContentSize
    CollapseClickCell *lastCell = [self.dataArray objectAtIndex:self.dataArray.count - 1];
    [self setContentSize:CGSizeMake(self.frame.size.width, lastCell.frame.origin.y + lastCell.frame.size.height)];
}

#pragma mark - Open CollapseClickCell
-(void)openCollapseClickCellAtIndex:(int)index animated:(BOOL)animated {
    // Check if it's not open first
    if ([[self.isClickedArray objectAtIndex:index] boolValue] != YES) {
        float duration = 0;
        if (animated) {
            duration = 0.25;
        }
        [UIView animateWithDuration:duration animations:^{
            // Resize Cell
            UserProfileCollapseClickCell *cell = [self.dataArray objectAtIndex:index];
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.ContentView.frame.origin.y + cell.ContentView.frame.size.height);
            
            // Change Arrow orientation
            CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI/2);
            cell.arrow.transform = transform;
            
            // Change isClickedArray
            [self.isClickedArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:YES]];
            
            // Reposition all CollapseClickCells below Cell
            [self repositionCollapseClickCellsBelowIndex:index withOffset:cell.ContentView.frame.size.height + HeaderContentPaddings];
        }];
    }
}

#pragma mark - Close CollapseClickCell
-(void)closeCollapseClickCellAtIndex:(int)index animated:(BOOL)animated {
    // Check if it's open first
    if ([[self.isClickedArray objectAtIndex:index] boolValue] == YES) {
        float duration = 0;
        if (animated) {
            duration = 0.25;
        }
        [UIView animateWithDuration:duration animations:^{
            // Resize Cell
            UserProfileCollapseClickCell *cell = [self.dataArray objectAtIndex:index];
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, UserProfileHeaderHeight);
            
            // Change Arrow orientation
            CGAffineTransform transform = CGAffineTransformMakeRotation(0);
            cell.arrow.transform = transform;
            
            // Change isClickedArray
            [self.isClickedArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:NO]];
            
            // Reposition all CollapseClickCells below Cell
            [self repositionCollapseClickCellsBelowIndex:index withOffset:-1*(cell.ContentView.frame.size.height + HeaderContentPaddings)];
        }];
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
