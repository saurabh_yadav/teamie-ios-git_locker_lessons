//
//  LPTableViewCell.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 26/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublishStatusLabel.h"
#import "UILabelWithPadding.h"

@interface LPTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
//@property (weak, nonatomic) IBOutlet PublishStatusLabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabelWithPadding *PublishStatusLabel;
@property (weak, nonatomic) IBOutlet UIImageView *tickImageView;
@end
