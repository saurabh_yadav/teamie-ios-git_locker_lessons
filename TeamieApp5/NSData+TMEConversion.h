//
//  NSData+TMEConversion.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 24/2/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (TMEConversion)

- (NSString *)hexadecimalString;

@end
