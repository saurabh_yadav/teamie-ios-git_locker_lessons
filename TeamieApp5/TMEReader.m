//
//  ReaderObject.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 14/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEReader.h"

@implementation TMEReader

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"last_access": @"last_access.on",
        @"access_timezone": @"last_access.timezone"
    };
}

+ (NSValueTransformer *)num_readJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)last_accessJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)access_timezoneJSONTransformer {
    return [TMEModel timezoneJSONTransformer];
}

+ (NSValueTransformer *)user_profile_imageJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEImage.class];
}

@end
