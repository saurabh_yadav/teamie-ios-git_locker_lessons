//
//  ThoughtImageAttachmentView.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 16/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ThoughtAttachmentView.h"

@interface TMEPostImageAttachmentView : ThoughtAttachmentView

- (id)initWithImageAttachments:(NSArray*)imageAttachments delegate:(id<ThoughtAttachmentViewDelegate>)delegate width:(CGFloat)attachmentWidth;
- (void)pushViewController:(UIViewController*)viewController animated:(BOOL)animated;
@property (strong, nonatomic) UIViewController *viewController;
@end
