//
//  LessonTableViewController.h
//  TeamieApp5
//
//  Created by Raunak on 10/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

@class LessonObject;

#import "TableRestApiViewController.h"

@interface LessonTableViewController : TableRestApiViewController

@property (nonatomic, strong) LessonObject* lesson;
- (id)initWithLessonDetails:(LessonObject*)lesson;

@end
