//
//  LessonPagesList.m
//  TeamieApp5
//
//  Created by Raunak on 10/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "LessonPagesList.h"

@implementation LessonPagesList

@synthesize title;
@synthesize descriptionTitle;
@synthesize descriptionValue;
@synthesize lessonPages;

@end
