//
//  TMEQuestion.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 24/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEQuestion.h"

@implementation TMEQuestion

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    mapping[@"echosCount"] = @"statistics.same.count";
    return mapping;
}

@end
