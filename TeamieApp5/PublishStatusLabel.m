//
//  PublishStatusLabel.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 3/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "PublishStatusLabel.h"

@implementation PublishStatusLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, PublishStatusLeftRightPaddings, 0, PublishStatusLeftRightPaddings};
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}
//
//- (void)configurePublishStatusLabelWithStatus:(PublishStatus)publishStatus
//{
//    switch (publishStatus) {
//        case PublishStatusRemoval:
//            [self removeFromSuperview];
//            break;
//        case PublishStatusDrafted:
//            [self publishStatusLabelText:@"Draft" fontColor:[UIColor colorWithHex:@"#333333" alpha:1.0] backgroundColor:[UIColor colorWithHex:@"#f5dc9a" alpha:1.0]];
//            break;
//        case PublishStatusPublished:
//            [self publishStatusLabelText:@"Published" fontColor:[UIColor colorWithHex:@"#ffffff" alpha:1.0] backgroundColor:[UIColor colorWithHex:@"#41A389" alpha:1.0]];
//            break;
//        default:
//            break;
//    }
//}

- (void)publishStatusLabelText:(NSString *)text fontColor:(UIColor *)fontColor backgroundColor:(UIColor *)backgroundColor
{
//    UIFont *font = [TeamieGlobals appFontFor:@"boldFontWithSize10"];
//    
//    CGSize expectedTextSize = [text sizeWithFont:font];
//    
//    CGSize expectedSize = CGSizeMake(expectedTextSize.width + 3 * PublishStatusLeftRightPaddings, expectedTextSize.height);
//    //Step 1, keep the origin, change the frame size to expected size
//    CGRect frame = self.frame;
//    CGPoint oldOrigin = frame.origin;
//    CGFloat oldFrameWidth = frame.size.width;
//    CGFloat centerY = self.center.y;
//    frame.size = expectedSize;
//    
//    
//    //Step 2, find the width offset
//    CGFloat newFrameWidth = frame.size.width;
//    CGFloat widthOffset = newFrameWidth - oldFrameWidth;
//    
//    //Step 3,move the origin left with the same width offset.
//    CGPoint newOrigin = CGPointMake(oldOrigin.x - widthOffset, oldOrigin.y);
//    frame.origin = newOrigin;
//    
//    //Step 4, configure the UIlabel
//    self.font = font;
//    self.text = text;
//    self.textColor = fontColor;
//    self.backgroundColor = backgroundColor;
    
//    self.frame = frame;
    self.layer.cornerRadius = 2.0;
    self.layer.masksToBounds = YES;
    self.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    self.textAlignment = NSTextAlignmentCenter;
    
    //Step 5,move the center back to the middle area
//    self.center = CGPointMake(self.center.x, centerY);
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
