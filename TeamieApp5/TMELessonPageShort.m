//
//  TMELessonPageShort.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMELessonPageShort.h"
#import "TMELessonAction.h"

@implementation TMELessonPageShort

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"is_Read" : @"is_read",
             };
}

+ (NSValueTransformer *)nidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)statusJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)authorJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEUser.class];
}

+ (NSValueTransformer *)actionsJSONTransformer {
    return [TMEModel listJSONTransformer:TMELessonAction.class];
}

@end
