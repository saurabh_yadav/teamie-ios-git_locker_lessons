//
//  UserCalendarViewController.m
//  TeamieApp5
//
//  Created by Raunak on 14/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "UserCalendarViewController.h"
#import <TapkuLibrary/TapkuLibrary.h>
#import "TMEClient.h"

#define CALENDAR_MONTH_OFFSET 50.0

@interface UserCalendarViewController () {
    BOOL isCalendarDisplayed;
    Class _popoverClass;
    BOOL shouldDisplayLabel;
}

@property (nonatomic,strong) WEPopoverController* wePopoverController;
@property (nonatomic,strong) TKCalendarMonthView* calendarMonth;
@property (nonatomic,strong) TKCalendarDayView* calendarDay;
@property (nonatomic,strong) NSDictionary* sortedCalendarItems;
@property (nonatomic,strong) NSDictionary* colorMapping;
@property (nonatomic,strong) NSDictionary* calendarNameMapping;
@property (nonatomic,strong) NSMutableArray* selectedCalendars;
@property (nonatomic,strong) UITapGestureRecognizer* tapToDismiss;

@end

@implementation UserCalendarViewController

@synthesize calendarItems;
@synthesize wePopoverController;
@synthesize calendarMonth;
@synthesize calendarDay;
@synthesize sortedCalendarItems;
@synthesize colorMapping;
@synthesize calendarNameMapping;
@synthesize selectedCalendars;
@synthesize tapToDismiss;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - View lifecycle
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        _popoverClass = [UIPopoverController class];
    }
    else {
        _popoverClass = [WEPopoverController class];
    }
    
    [[TMEClient sharedClient]
     request:TeamieUserCalendarRequest
     parameters:nil
     completion:^{
         [TeamieGlobals dismissActivityLabels];
     } success:^(id response) {
         if (![response isKindOfClass:[NSArray class]]) {
             return;
         }
         self.calendarItems = response;
         [self.calendarDay reloadData];
         [self.calendarMonth reloadData];
         [self addRightBarButtonItem];
         
         [self.coachMarksView start];
         [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"%@%@", COACH_MARKS_SHOWN_PREFIX, NSStringFromClass([self class])]];
         [[NSUserDefaults standardUserDefaults] synchronize];
     } failure:^(NSError *error) {
         TKEmptyView* view = [[TKEmptyView alloc] initWithFrame:self.view.frame emptyViewImage:TKEmptyViewImageSearch title:TeamieLocalizedString(@"MSG_TITLE_CALENDARS_NOT_FOUND", nil) subtitle:TeamieLocalizedString(@"MSG_CALENDARS_NOT_FOUND", nil)];
         [self.view addSubview:view];
     }];
    
    self.navigationItem.title = TeamieLocalizedString(@"TITLE_CALENDARS", nil);
    [self.view setBackgroundColor:[UIColor whiteColor]];
    shouldDisplayLabel = YES;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
    
    self.coachMarksView = [TeamieUIGlobals initializeCoachMarksView:NSStringFromClass([self class]) forView:self.navigationController.view forElements:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    if (shouldDisplayLabel) {
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING", nil) margin:10.f yOffset:10.0f];
        shouldDisplayLabel = NO;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)viewDidUnload
{
    self.calendarItems = nil;
    self.wePopoverController = nil;
    self.calendarDay = nil;
    self.calendarMonth = nil;
    self.sortedCalendarItems = nil;
    self.colorMapping = nil;
    self.calendarNameMapping = nil;
    self.selectedCalendars = nil;
    [super viewDidUnload];
}

#pragma mark - Teamie REST API Method

#pragma mark - Calendar Delegate Methods

- (NSArray *) calendarDayTimelineView:(TKCalendarDayView*)calendarDayTimeline eventsForDate:(NSDate *)eventDate{
    
    NSArray* events = [self getEventsForDate:eventDate];
    NSMutableArray* result = [NSMutableArray array];
    for(CalendarItem* ci in events) {
        TKCalendarDayEventView* event = [calendarDayTimeline dequeueReusableEventView];
        if(event == nil) {
            event = [TKCalendarDayEventView eventView];
        }
        event.titleLabel.text = ci.description;
        event.startDate = ci.from_date;
        event.endDate = ci.to_date;
        NSUInteger componentFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit;
        NSDateComponents *components = [[NSCalendar currentCalendar] components:componentFlags fromDate:ci.to_date];
        if (components.hour == 0 && components.minute == 0) {
            components.day++;
            event.endDate = [[NSCalendar currentCalendar] dateFromComponents:components];
        }
        event.color = [self.colorMapping objectForKey:ci.cid];
        [result addObject:event];
    }
    return result;
}

- (void) calendarMonthView:(TKCalendarMonthView *)monthView didSelectDate:(NSDate *)date {
    [self toggleCalendarMonth];
    [self.calendarDay setDate:date];
}

- (NSArray*) calendarMonthView:(TKCalendarMonthView *)monthView marksFromDate:(NSDate *)startDate toDate:(NSDate *)lastDate {
    NSDate* d = startDate;
    NSMutableArray* data = [NSMutableArray array];
    while (YES) {
        NSArray* events = [self getEventsForDate:d];
        if (events!=nil && [events count]>0) {
            [data addObject:@"YES"];
        }
        else {
            [data addObject:@"NO"];
        }
        
        NSDateComponents *info = [d dateComponentsWithTimeZone:monthView.timeZone];
		info.day++;
		d = [NSDate dateWithDateComponents:info];
		if([d compare:lastDate]==NSOrderedDescending) break;
    }
    return data;
}

- (void) calendarDayTimelineView:(TKCalendarDayView *)calendarDay didMoveToDate:(NSDate *)date {
    isCalendarDisplayed = YES;
    [self.calendarMonth selectDate:date];
}

- (void) calendarDayTimelineView:(TKCalendarDayView*)calendarDayTimeline eventViewWasSelected:(TKCalendarDayEventView *)eventView{
	TKLog(@"%@",eventView.titleLabel.text);
}

- (void) calendarDayTimelineViewDateLabelWasTapped:(TKCalendarDayView *)calendar {
    [self.calendarMonth selectDate:calendar.date];
}

- (void) calendarMonthViewDateLabelWasTapped:(TKCalendarMonthView *)monthView {
    [self toggleCalendarMonth];
}

- (void) calendarMonthView:(TKCalendarMonthView *)monthView monthWillChange:(NSDate *)month animated:(BOOL)animated {
    [self.tapToDismiss setEnabled:NO];
}

- (void) calendarMonthView:(TKCalendarMonthView *)monthView monthDidChange:(NSDate *)month animated:(BOOL)animated {
    [self.tapToDismiss setEnabled:YES];
}

#pragma mark - Button Click Methods
- (void) barButtonPressed:(id)sender {
    UIBarButtonItem* button = sender;
    if(!self.wePopoverController) {
        CalendarMenuViewController* contentViewController = [[CalendarMenuViewController alloc] initWithStyle:UITableViewStylePlain];
        contentViewController.contentSizeForViewInPopover = CGSizeMake(200, 50);
        
        NSMutableArray* entries = [NSMutableArray array];
        
        /*for (NSNumber* cid in self.sortedCalendarItems) {
            TTTeamieCalendarItem* item = nil;
            if ([self.selectedCalendars containsObject:cid]) {
                item = [TTTeamieCalendarItem itemWithText:[self.calendarNameMapping objectForKey:cid] state:CheckmarkChecked color:[self.colorMapping objectForKey:cid]];
            }
            else {
                item = [TTTeamieCalendarItem itemWithText:[self.calendarNameMapping objectForKey:cid] state:CheckmarkNone color:[self.colorMapping objectForKey:cid]];
            }
            [entries addObject:item];
        }
        
        TeamieListDataSource* dataSource = [[TeamieListDataSource alloc] initWithItems:entries];
        contentViewController.dataSource = dataSource;*/
        contentViewController.menuDelegate  = self;
        [contentViewController setNumOfMenuItems:entries.count];
        self.wePopoverController = [[_popoverClass alloc] initWithContentViewController:contentViewController];
        self.wePopoverController.delegate = self;
        //    self.wePopoverController.passthroughViews = [NSArray arrayWithObject:self.navigationController.navigationBar];
        [self.wePopoverController presentPopoverFromBarButtonItem:button permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown) animated:YES];
    }
    else {
        [self.wePopoverController dismissPopoverAnimated:YES];
        self.wePopoverController = nil;
    }
}

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    self.wePopoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

- (void) menuDidSelectObjectWithData:(id)data {
    /*if ([data isKindOfClass:[TTTableCheckedTextItem class]]) {
        for (NSNumber* cid in [self.calendarNameMapping allKeys]) {
            if ([[self.calendarNameMapping objectForKey:cid] isEqualToString:((TTTableCheckedTextItem*)data).text]) {
                [self.selectedCalendars addObject:cid];
            }
        }
    }*/
    [self.calendarDay reloadData];
    [self.calendarMonth reloadData];
}

- (void) menuDidDeselectObjectWithData:(id)data {
    /*if ([data isKindOfClass:[TTTableCheckedTextItem class]]) {
        for (NSNumber* cid in [self.calendarNameMapping allKeys]) {
            if ([[self.calendarNameMapping objectForKey:cid] isEqualToString:((TTTableCheckedTextItem*)data).text]) {
                [self.selectedCalendars removeObject:cid];
            }
        }
    }*/
    [self.calendarDay reloadData];
    [self.calendarMonth reloadData];
}

#pragma mark - Private Methods

- (void)addRightBarButtonItem {
    if (self.navigationController) {
        if (!self.navigationItem.rightBarButtonItem) {
            
            UIBarButtonItem* calendarButton = [[UIBarButtonItem alloc] initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"filter" withSize:20.0 andColor:[UIColor whiteColor]] style:UIBarButtonItemStyleBordered target:self action:@selector(barButtonPressed:)];
            self.navigationItem.rightBarButtonItem = calendarButton;
        }
    }
}

- (void)toggleCalendarMonth {
    if (!isCalendarDisplayed) {
		// Show
        [self.calendarDay disableGestures];
        [self.tapToDismiss setEnabled:YES];
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.5];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            calendarMonth.frame = CGRectMake(self.view.center.x - (self.calendarMonth.frame.size.width/2), self.view.center.y - (self.calendarMonth.frame.size.height/2), calendarMonth.frame.size.width, calendarMonth.frame.size.height);
        }
        else {
            calendarMonth.frame = CGRectMake(0, 0, calendarMonth.frame.size.width, calendarMonth.frame.size.height);
        }

		[UIView commitAnimations];
    } else {
		// Hide
        [self.calendarDay enableGestures];
        [self.tapToDismiss setEnabled:NO];
		[UIView beginAnimations:nil context:NULL];
		[UIView setAnimationDuration:0.5];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            calendarMonth.frame = CGRectMake(self.view.center.x - (self.calendarMonth.frame.size.width/2), -calendarMonth.frame.size.height-CALENDAR_MONTH_OFFSET, calendarMonth.frame.size.width, calendarMonth.frame.size.height);
        }
        else {
            calendarMonth.frame = CGRectMake(0, -self.calendarMonth.frame.size.height-CALENDAR_MONTH_OFFSET, calendarMonth.frame.size.width, calendarMonth.frame.size.height);
        }

		[UIView commitAnimations];
	}
    isCalendarDisplayed = !isCalendarDisplayed;
}

- (NSArray*)getEventsForDate:(NSDate*)date {
    if (self.calendarItems == nil) {
        return nil;
    }
    
    NSMutableArray* events = [NSMutableArray array];
    for (CalendarItem* event in self.calendarItems) {
        if ([self.selectedCalendars containsObject:event.cid]) {
            NSUInteger componentFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
            NSDateComponents *startDateComponents = [[NSCalendar currentCalendar] components:componentFlags fromDate:event.from_date];
            NSDateComponents *endDateComponents = [[NSCalendar currentCalendar] components:componentFlags fromDate:event.to_date];
            NSDateComponents *currentDateComponents = [[NSCalendar currentCalendar] components:componentFlags fromDate:date];
            NSDate* date1 = [[NSCalendar currentCalendar] dateFromComponents:startDateComponents];
            NSDate* date2 = [[NSCalendar currentCalendar] dateFromComponents:endDateComponents];
            NSDate* date3 = [[NSCalendar currentCalendar] dateFromComponents:currentDateComponents];
            if ([date1 compare:date3] == NSOrderedSame || [date2 compare:date3] == NSOrderedSame || ([date1 compare:date3] == NSOrderedAscending && [date2 compare:date3] == NSOrderedDescending)) {
                [events addObject:event];
            }
        }
    }
    return events;
}

- (void)dismissCalendarMonth {
    [self toggleCalendarMonth];
}

#pragma mark - Lazy Instantiation Methods

- (NSArray*)calendarItems {
    if (!calendarItems) {
        calendarItems = [NSArray array];
    }
    return calendarItems;
}

- (NSDictionary*)sortedCalendarItems {
    if (!sortedCalendarItems) {
        NSMutableDictionary* result = [NSMutableDictionary dictionary];
        for (CalendarItem* ci in self.calendarItems) {
            NSArray* events = [result objectForKey:ci.cid];
            if (events == nil) {
                events = [NSMutableArray array];
            }
            [((NSMutableArray*)events) addObject:ci];
            [result setObject:events forKey:ci.cid];
        }
        sortedCalendarItems = result;
    }
    return sortedCalendarItems;
}

- (NSDictionary*)colorMapping {
    if (!colorMapping) {
        NSMutableDictionary* result = [NSMutableDictionary dictionary];
        for (NSNumber* cid in [self.sortedCalendarItems allKeys]) {
            [result setObject:[UIColor randomLightColor] forKey:cid];
        }
        colorMapping = result;
    }
    return colorMapping;
}

- (NSMutableArray*)selectedCalendars {
    if (!selectedCalendars) {
        selectedCalendars = [NSMutableArray arrayWithArray:[self.sortedCalendarItems allKeys]];
    }
    return selectedCalendars;
}

- (NSDictionary*)calendarNameMapping {
    if (!calendarNameMapping) {
        NSMutableDictionary* result = [NSMutableDictionary dictionary];
        for (NSNumber* cid in [self.sortedCalendarItems allKeys]) {
            CalendarItem* ci = [((NSArray*)[self.sortedCalendarItems objectForKey:cid]) objectAtIndex:0];
            [result setObject:ci.calendar_name forKey:ci.cid];
        }
        calendarNameMapping = result;
    }
    return calendarNameMapping;
}

- (TKCalendarDayView*)calendarDay {
    if (!calendarDay) {
        calendarDay = [[TKCalendarDayView alloc] initWithFrame:self.view.bounds];
        calendarDay.delegate = self;
        calendarDay.dataSource = self;
        calendarDay.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.view addSubview:calendarDay];
    }
    return calendarDay;
}

- (TKCalendarMonthView*)calendarMonth {
    if (!calendarMonth) {
        calendarMonth = [[TKCalendarMonthView alloc] init];
        calendarMonth.delegate = self;
        calendarMonth.dataSource = self;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            calendarMonth.frame = CGRectMake(self.view.center.x - (calendarMonth.frame.size.width/2), -calendarMonth.frame.size.height-CALENDAR_MONTH_OFFSET, calendarMonth.frame.size.width, calendarMonth.frame.size.height);
            calendarMonth.layer.borderColor = [UIColor blackColor].CGColor;
            calendarMonth.layer.borderWidth = 2.0f;
        }
        else {
            calendarMonth.frame = CGRectMake(0, -calendarMonth.frame.size.height-CALENDAR_MONTH_OFFSET, calendarMonth.frame.size.width, calendarMonth.frame.size.height);
        }
        isCalendarDisplayed = NO;
        [self.view addSubview:calendarMonth];
    }
    return calendarMonth;
}

- (UITapGestureRecognizer*)tapToDismiss {
    if (!tapToDismiss) {
        tapToDismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissCalendarMonth)];
        [self.view addGestureRecognizer:tapToDismiss];
    }
    return tapToDismiss;
}

@end