//
//  ScoreBarView.h
//  TeamieApp5
//
//  Created by Raunak on 18/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreBarView : UIView

@property (nonatomic, strong) UIColor* barColor;
@property (nonatomic, strong) UIColor* baseColor;
@property (nonatomic) CGFloat barRatio;

- (id)initWithFrame:(CGRect)frame barColor:(UIColor*)barColor baseColor:(UIColor*)baseColor;
- (id)initWithFrame:(CGRect)frame barColor:(UIColor*)barColor baseColor:(UIColor*)baseColor barRatio:(CGFloat)ratio;

@end
