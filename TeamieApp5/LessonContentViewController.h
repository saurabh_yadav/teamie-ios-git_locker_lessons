//
//  LessonContentViewController.h
//  TeamieApp5
//
//  Created by Raunak on 23/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "LessonAttachmentsViewController.h"
#import "PublishStatusLabel.h"
#import "TMELessonPage.h"
#import "UILabelWithPadding.h"

static NSString *publishedColorHex = @"#41A389";
static NSString *draftedColorHex = @"#f5dc9a";

@protocol LessonContentDelegate

- (void)hideToolBar;
- (void)showToolBar;
- (void)setEnableButtonColor:(NSString *)colorHex;
- (void)setAccess:(NSDictionary *)access;
- (void)setPublishStatus:(PublishStatus)publishStatus;
@end

@interface LessonContentViewController : UIViewController <UIWebViewDelegate, LessonAttachmentsControllerDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate,UITableViewDataSource,UITableViewDelegate>

@property (nonatomic,strong) NSNumber* lessonID;
@property (nonatomic,strong) TMELessonPage * lessonPage;
@property (nonatomic,weak) id<LessonContentDelegate> delegate;

- (id)initWithLessonID:(NSNumber*)lid;
//- (void)displayAttachmentsForEvent:(UIEvent*)event;

- (void)updateToolbar;

//- (void)enableButtonPressed;
//- (void)removeButtonPressed;
@end
