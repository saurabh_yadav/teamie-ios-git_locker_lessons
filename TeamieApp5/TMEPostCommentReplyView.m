//
//  TMEPostCommentReplyView.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 23/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostCommentReplyView.h"

#import "TMEImageAttachment.h"
#import "TMEPostAttachment.h"
#import "TMEPostAttachmentView.h"
#import "UserProfileViewController.h"
#import "TMEPostAction.h"
#import "TMEClient.h"

#import "UIView+TMEEssentials.h"

#import <Masonry/Masonry.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import <SDWebImage/UIImageView+WebCache.h>

@implementation TMEPostCommentReplyView

+ (instancetype)viewWithReply:(TMEPostCommentReply *)reply delegate:(UIViewController<TMEPostCommentViewDelegate> *)delegate {
    TMEPostCommentReplyView *postCommentReplyView = [super new];
    if (postCommentReplyView) {
        postCommentReplyView.postCommentLevel = TMEPostCommentLevelReply;
        postCommentReplyView.comment = reply;
        postCommentReplyView.delegate = delegate;
        
        [postCommentReplyView createHeader];
        [postCommentReplyView loadContent];
        [postCommentReplyView loadAttachments];
        [postCommentReplyView loadRepliesView];
        [postCommentReplyView loadReplies];
        [postCommentReplyView addActions];
        [postCommentReplyView setStatus];
        
        postCommentReplyView.authorNameLabel.font = [TeamieGlobals appFontFor:@"replyTitle"];
        postCommentReplyView.timePostedLabel.font = [TeamieGlobals appFontFor:@"replySubtitle"];
        postCommentReplyView.commentTextView.font = [TeamieGlobals appFontFor:@"replyBody"];
        postCommentReplyView.commentTextView.backgroundColor = [TeamieUIGlobalColors postCommentReplyBackgroundColor];
        postCommentReplyView.backgroundColor = [TeamieUIGlobalColors postCommentReplyBackgroundColor];
        postCommentReplyView.leftActionView.hidden = YES;
        
        postCommentReplyView.imageSize = @20;
        postCommentReplyView.leftIndent = 15;
        postCommentReplyView.rightIndent = 5;
        [postCommentReplyView addConstraints];
        [postCommentReplyView addCaret];
    }
    return postCommentReplyView;
}

- (void)addCaret {
    UILabel *caret = [self createAndAddSubView:UILabel.class];
    caret.text = @"\u203A";
    caret.font = [UIFont fontWithName:@"OpenSans" size:15.0];
    caret.textColor = [TeamieUIGlobalColors defaultTextColor];
    
    [caret mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(12);
        make.centerY.equalTo(self.authorImageButton);
    }];
}

- (void)delete {
    [self.delegate deleteReply:self.comment inComment:self.comment.parentComment];
}

@end
