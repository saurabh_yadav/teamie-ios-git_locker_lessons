//
//  MessageThreadViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/25/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"

@interface MessageThreadViewController : TableRestApiViewController

@property (strong, nonatomic) NSArray* messageItems;

- (id) initWithItems:(NSArray*)items;

@end
