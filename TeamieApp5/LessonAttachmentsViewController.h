//
//  LessonAttachmentsViewController.h
//  TeamieApp5
//
//  Created by Raunak on 22/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol LessonAttachmentsControllerDelegate
- (void) lessonAttachmentsControllerDidSelectOptionWithData:(NSDictionary*)option;
@end

@interface LessonAttachmentsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) id<LessonAttachmentsControllerDelegate> delegate;
@property (nonatomic, strong) NSArray* fileAttachments;
@property (nonatomic, strong) NSArray* linkAttachments;

- (id)initWithFileAttachments:(NSArray*)fileAttachments linkAttachments:(NSArray*)linkAttachments;
@end
