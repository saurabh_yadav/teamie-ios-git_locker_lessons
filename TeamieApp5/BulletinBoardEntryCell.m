//
//  BulletinBoardEntryCell.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 11/9/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "BulletinBoardEntryCell.h"
#import "TMENotification.h"

@implementation BulletinBoardEntryCell

@synthesize mainText;
@synthesize subText;
@synthesize imageView;
@synthesize options;

- (void)awakeFromNib {
    imageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)setCellData:(TMENotification *)bulletinEntry {
    mainText.attributedText = bulletinEntry.mainText;
    subText.text = bulletinEntry.subText;
    imageView.image = bulletinEntry.postImage;
    self.backgroundColor = !bulletinEntry.isRead?[TeamieUIGlobalColors headerViewBackgroundColor]:[UIColor whiteColor];
}

- (CGFloat)getCellHeight:(TMENotification *)bulletinEntry {
    mainText.attributedText = bulletinEntry.mainText;

    //Force cell to layout subviews immediately
    [self setNeedsLayout];
    [self layoutIfNeeded];

    //Get height of contentView
    CGFloat height = [self.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;

    //Add an extra point to the height to account for the cell separator, which is added between the bottom of the cell's contentView and the bottom of the table view cell.
    return height + 1.0f;
}

@end
