//
//  TeamieAppDelegate.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/16/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TeamieAppDelegate.h"

#import "TMEClient.h"
#import "UserProfileViewController.h"
#import "TeamieRevealController.h"
#import "SlideMenuController.h"
#import "BulletinBoardViewController.h"
#import "InstituteSelectViewController.h"
#import "TMEAnalyticsController.h"
#import "TMEPostViewController.h"
#import "TMENewsfeedViewController.h"
#import "NSData+TMEConversion.h"
#import "TMEPostCreateAudioAttachmentViewController.h"

#import <GooglePlus/GooglePlus.h>
#import <CoreData/CoreData.h>
#import <AFNetworkActivityLogger/AFNetworkActivityLogger.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>

@implementation TeamieAppDelegate

@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

// Prototype declaration of a custom Exception Handler
void uncaughtExceptionHandler(NSException *exception);

#pragma mark - UIApplicationDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);

    // Register with the Apple Push Notification Service so we can receive notifications from our server
    // application.  Upon successful registration, our didRegisterForRemoteNotificationsWithDeviceToken:
    // delegate callback will be invoked with our unique device token.
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        //////FOR iOS 8
        UIUserNotificationSettings* notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [application registerUserNotificationSettings:notificationSettings];
    } else {
        //////FOR iOS 7
        [application registerForRemoteNotificationTypes: UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert];
    }

    // Clear any push notifications that the app might have created
    [application setApplicationIconBadgeNumber: 0];
    [application cancelAllLocalNotifications];

    // Allow app to detect Shake Events
    [application setApplicationSupportsShakeToEdit:YES];
  
    // Check if app opened up from push notification
    NSDictionary *notification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (notification) {
        // Set the bulletin board as the initial view controller
        // LoginViewController will now open up Bulletin Board controller as soon as app loads
        // @see transitionToHomeScreen() method of LoginViewController.m
        [[NSUserDefaults standardUserDefaults] setObject:@"bulletin" forKey:@"launch_initial_viewcontroller"];
    }
    
    // Set navigation bar color for all viewcontrollers
    NSShadow* shadow = [NSShadow new];
    shadow.shadowOffset = CGSizeMake(0.0f, 1.0f);
    shadow.shadowColor = [UIColor colorWithHex:@"#000000" alpha:1.0];
    
    [[UINavigationBar appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[TeamieUIGlobalColors navigationBarTextColor], NSForegroundColorAttributeName, [TeamieGlobals appFontFor:@"navigationTitle"], NSFontAttributeName, shadow, NSShadowAttributeName, nil]];
    [[UINavigationBar appearance] setTintColor:[TeamieUIGlobalColors navigationBarTextColor]];
    [[UINavigationBar appearance] setBarTintColor:[TeamieUIGlobalColors navigationBarBackgroundColor]];

    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTintColor:[TeamieUIGlobalColors navigationBarTextColor]];
    [[UIBarButtonItem appearanceWhenContainedIn:[UINavigationBar class], nil] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[TeamieUIGlobalColors navigationBarTextColor], NSForegroundColorAttributeName, [TeamieGlobals appFontFor:@"navigationTitle"], NSFontAttributeName,nil] forState:UIControlStateNormal];
    
    [[UIToolbar appearance] setBarTintColor:[TeamieUIGlobalColors toolbarBackgroundColor]];
    [[UIBarButtonItem appearanceWhenContainedIn:[UIToolbar class], nil] setTintColor:[TeamieUIGlobalColors toolbarTextColor]];

#ifdef DEBUG
    //Logs all API requests and responses
    [[AFNetworkActivityLogger sharedLogger] startLogging];
    [[AFNetworkActivityLogger sharedLogger] setLevel:AFLoggerLevelInfo];
#endif
    
    [TMEAnalyticsController startTrackingOnGoogleAnalytics];
    [Fabric with:@[CrashlyticsKit]];

    return YES;
}

/*
 Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
 */
- (void)applicationDidBecomeActive:(UIApplication *)application {
    // clear the push notifications list
    [application setApplicationIconBadgeNumber: 1];
    [application setApplicationIconBadgeNumber: 0];
    [application cancelAllLocalNotifications];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"pasteboardUrlToShareBox" object:self];
}

/*
 Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
 If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
 */
- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    // Post Notification to the Audio Recorder
    [[NSNotificationCenter defaultCenter] postNotificationName:@"stopAudioRecording" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshSeenPosts" object:nil];
}

- (void)applicationWillResignActive:(UIApplication *)application{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshSeenPosts" object:nil];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    //This is called by the google login handler, after the user signs in to his google account
    //Control then goes on to [LoginViewController finishedWithAuth:error:]
    if ([GPPURLHandler handleURL:url sourceApplication:sourceApplication annotation:annotation]) {
        return YES;
    }

    if ([[TMEClient sharedClient] isLoggedIn]) {
        if ([url isFileURL]) {
            NSString* type = [[url absoluteString] pathExtension];
            if ([type isEqualToString:@"png"] || [type isEqualToString:@"jpeg"] || [type isEqualToString:@"jpg"]) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSData dataWithContentsOfURL:url] forKey:@"cache_thought_image"];
            }
            
            if ([type isEqualToString:@"pdf"] || [type isEqualToString:@"txt"] || [type isEqualToString:@"doc"] || [type isEqualToString:@"docx"] || [type isEqualToString:@"ppt"] || [type isEqualToString:@"pptx"] || [type isEqualToString:@"xls"] || [type isEqualToString:@"xlsx"] ) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSData dataWithContentsOfURL:url] forKey:@"cache_thought_file"];
                [[NSUserDefaults standardUserDefaults] setObject:type forKey:@"cache_thought_file_extension"];
            }
            
            UIViewController* vc;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"ShareBoxNavigationController"];
            }
            else {
                vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"ShareBoxViewController"];
            }
            [[self.window.rootViewController presentedViewController] dismissViewControllerAnimated:NO completion:nil];
            [self.window.rootViewController presentViewController:vc animated:YES completion:nil];
            
        }
    }
    return YES;
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[NSUserDefaults standardUserDefaults] setObject:[deviceToken hexadecimalString] forKey:@"apnsDeviceToken"];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
#ifdef DEBUG
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error);
#endif
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    if ([application applicationState] == UIApplicationStateInactive) {
        // implies the app will be entering foreground after tapping on a push notification
        // Set the bulletin board as the initial view controller
        BulletinBoardViewController* bVC = [[BulletinBoardViewController alloc] init];
        UINavigationController* nVC = [[UINavigationController alloc] initWithRootViewController:bVC];
        [self swapOutToViewController:nVC];
        
        // clear the notifications list of the app
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 1];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber: 0];
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
    }
    if ([application applicationState] == UIApplicationStateActive) {
        // this implies that the app is active and in front of the user
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UIApplicationDidReceiveNotificationWhenActive" object:self userInfo:userInfo];
    }
}

#pragma mark - Core Data stack

/**
 Returns the managed object context for the application.
 If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
 */
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil)
    {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"TeamieCoreData" ofType:@"momd"];
    NSURL *momURL = [NSURL fileURLWithPath:path];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:momURL];
    
    //NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"TeamieCoreData" withExtension:@"momd"];
    //__managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (__persistentStoreCoordinator != nil)
    {
        return __persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"TeamieCoreData.sqlite"];
    
    // set the data migration option to automatically infer the data model migration
    NSDictionary* migrationOptions = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:migrationOptions error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter: 
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
//        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }  
    
    return __persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

/**
 Returns the URL to the application's Documents directory.
 */
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark - Exception Handlers

/**
 Method that would handle an exception whenever it's raised in the code
 And helps to print out the stack trace
 Based on suggestion from stackoverflow thread: http://stackoverflow.com/questions/7841610/xcode-4-2-debug-doesnt-symbolicate-stack-call
 */
void uncaughtExceptionHandler(NSException *exception) {
#ifdef DEBUG
    NSLog(@"CRASH: %@", exception);
    NSLog(@"Stack Trace: %@", [exception callStackSymbols]);
#endif
}

#pragma mark - API Methods

-(void)transitionToHomeScreen {
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"whitelabel_list"]) {
        NSDictionary* dict = @{@"LessonSingular"    : @"Lesson",
                               @"LessonPlural"      : @"Lessons",
                               @"QuizSingular"      : @"Quiz",
                               @"QuizPlural"        : @"Quizzes",
                               @"ThoughtSingular"   : @"Thought",
                               @"ThoughtPlural"     : @"Thoughts",
                               @"ClassroomSingular" : @"Classroom",
                               @"ClassroomPlural"   : @"Classrooms"};
        [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"whitelabel_list"];
    }

    UIViewController *frontViewController;

    // Check if the initial view controller is set
    // @see application:didFinishLaunchingWithOptions: method on how 'launch_initial_viewcontroller' is set
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"launch_initial_viewcontroller"]) {
        // check if the launch_initial_viewcontroller key has the value 'bulletin' stored in it.
        if ([[[NSUserDefaults standardUserDefaults] stringForKey:@"launch_initial_viewcontroller"] isEqualToString:@"bulletin"]) {
            // Initialize an empty bulletin board view controller
            // The Bulletin board view controller will take care of loading the bulletin items
            frontViewController = [[BulletinBoardViewController alloc] init];
        }
        
        // Removes the initial view controller setting from NSUserDefaults
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"launch_initial_viewcontroller"];
    }

    // If the front view controller is not set, then set it to be the newsfeed view controller
    if (!frontViewController) {
        frontViewController = [TMENewsfeedViewController newsfeed];
    }

    UINavigationController *frontNavigationViewController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
    UIViewController* slideMenuViewController = [[SlideMenuController alloc] init];

    self.window.rootViewController = [TeamieRevealController revealControllerWithFrontViewController:frontNavigationViewController
                                                                                  leftViewController:slideMenuViewController];
}

- (void)transitionToLoginScreen {
    // Set the login view controller as the root view controller
    
    InstituteSelectViewController* loginVC = [[InstituteSelectViewController alloc] initFromStoryboard];

    UINavigationController* navVC = [[UINavigationController alloc] initWithRootViewController:loginVC];

    self.window.rootViewController = navVC;
}

#pragma mark - Private Helper

- (void)swapOutToViewController:(UIViewController*)viewController {
    
    UIViewController *rootViewController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    
    if ([viewController isKindOfClass:[UINavigationController class]]) {
        TeamieRevealController *revealController = (TeamieRevealController *)((UINavigationController *)rootViewController).topViewController;
        [revealController dismissViewControllerAnimated:NO completion:nil];
        revealController.frontViewController = viewController;
    }
    else {
#ifdef DEBUG
        NSLog(@"Sorry! The topmost view controller is of type %@ and not of type %@", [rootViewController class], [UINavigationController class]);
#endif
    }
}

@end
