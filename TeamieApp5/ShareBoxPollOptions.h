//
//  ShareBoxPollOptions.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 23/05/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

// This object is Managed by CoreData

@interface ShareBoxPollOptions : NSManagedObject

@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSString * title;

@end
