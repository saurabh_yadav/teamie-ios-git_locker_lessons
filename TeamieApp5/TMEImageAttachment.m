//
//  TMEImageAttachment.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEImageAttachment.h"

@implementation TMEImageAttachment

+ (NSValueTransformer *)thumbnailJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

- (UIImage *)previewImage {
    return [TeamieUIGlobals defaultPicForPurpose:@"image"
                                        withSize:15
                                        andColor:[TeamieUIGlobalColors defaultTextColor]];
}

- (NSString *)displaySubTitle {
    return @"Image";
}

@end
