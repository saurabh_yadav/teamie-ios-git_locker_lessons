//
//  ThoughtImageViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/16/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

@interface ThoughtImageViewController : UIViewController

- (void)loadImageWithUrl:(NSString*)url WithCaption:(NSString*)caption WithTitle:(NSString*)title;

- (void)dismissPhotoView:(id)sender;

@end
