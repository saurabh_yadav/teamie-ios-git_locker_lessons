//
//  LessonAttachmentsViewController.m
//  TeamieApp5
//
//  Created by Raunak on 22/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "LessonAttachmentsViewController.h"

@interface LessonAttachmentsViewController ()
@property (nonatomic, strong) UITableView* tableView;
@end

@implementation LessonAttachmentsViewController

- (id)initWithFileAttachments:(NSArray*)fileAttachments linkAttachments:(NSArray*)linkAttachments {
    self = [super init];
    if (self) {
        _fileAttachments = fileAttachments;
        _linkAttachments = linkAttachments;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
//    self.tableView.scrollEnabled = NO;
//    self.tableView.bounces = NO;
    [self.view addSubview:self.tableView];
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    switch (section) {
        case 0:
            count = [self.fileAttachments count];
            break;
        case 1:
            count = [self.linkAttachments count];
            break;
        default:
            break;
    }
    return count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [self.fileAttachments count] > 0 ? 20.0 : 0;
            break;
        case 1:
            return [self.linkAttachments count] > 0 ? 20.0 : 0;
            break;
        default:
            return 0;
            break;
    }
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return [self.fileAttachments count] > 0 ? @"Files" : nil;
            break;
        case 1:
            return [self.linkAttachments count] > 0 ? @"Links" : nil;
            break;
        default:
            return nil;
            break;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"menuCellIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menuCellIdentifier"];
    }
    
    NSDictionary *tempDict;
    switch (indexPath.section) {
        case 0:
            tempDict = [self.fileAttachments objectAtIndex:indexPath.row];
            cell.textLabel.text = [[NSURL URLWithString:(NSString*)[tempDict objectForKey:@"href"]] lastPathComponent];
            break;
        case 1:
            tempDict = [self.linkAttachments objectAtIndex:indexPath.row];
            cell.textLabel.text = [tempDict objectForKey:@"title"];
            cell.detailTextLabel.text = [tempDict objectForKey:@"description"];
            break;
        default:
            break;
    }    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
            [self.delegate lessonAttachmentsControllerDidSelectOptionWithData:[self.fileAttachments objectAtIndex:indexPath.row]];
            break;
        case 1:
            [self.delegate lessonAttachmentsControllerDidSelectOptionWithData:[self.linkAttachments objectAtIndex:indexPath.row]];
            break;
        default:
            break;
    }
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
