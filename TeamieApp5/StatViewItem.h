//
//  StatViewItem.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 9/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StatViewItem : NSObject

@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) NSString * subtitle;
@property (nonatomic, strong) NSString * statText;
@property (nonatomic, strong) NSString * statSubtext;
@property (nonatomic, strong) NSNumber * barWidth;

+ (StatViewItem*)itemWithText:(NSString *)text subtitle:(NSString*)subtitle statText:(NSString*)statText statSubtext:(NSString*)statSubtext barWidth:(NSNumber*)barWidth;

@end
