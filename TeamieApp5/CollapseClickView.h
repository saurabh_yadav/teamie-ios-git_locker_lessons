//
//  CollapseClickView.h
//  CollapseClick
//
//  Created by Ben Gordon on 2/28/13.
//  Copyright (c) 2013 Ben Gordon. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "CollapseClickCellView.h"
#import "QuestionPageViewController.h"

#define kCCPad 10

//////////////
// Delegate //
//////////////
@protocol CollapseClickViewDelegate
@required
-(int)numberOfCellsForCollapseClick;
-(QuestionPageViewController*)detailsForCollapseClickAtIndex:(int)index;
-(NSNumber*)maxScoreForCollapseClickAtIndex:(int)index;

@optional
-(UIColor *)colorForCollapseClickTitleViewAtIndex:(int)index;
-(UIColor *)colorForTitleLabelAtIndex:(int)index;
-(UIColor *)colorForTitleArrowAtIndex:(int)index;
-(void)didClickCollapseClickCellAtIndex:(int)index isNowOpen:(BOOL)open;
-(void)didFinishLoad;

@end




///////////////
// Interface //
///////////////
@interface CollapseClickView : UIScrollView <UIScrollViewDelegate>  {
    __weak id <CollapseClickViewDelegate> CollapseClickDelegate;
}

// Delegate
@property (weak) id <CollapseClickViewDelegate> CollapseClickDelegate;

// Properties
@property (nonatomic, retain) NSMutableArray *isClickedArray;
@property (nonatomic, retain) NSMutableArray *dataArray;
@property (nonatomic, retain) NSMutableArray* displayArray;

// Methods
-(void)reloadCollapseClick;
-(CollapseClickCellView *)collapseClickCellForIndex:(int)index;
-(void)scrollToCollapseClickCellAtIndex:(int)index animated:(BOOL)animated;
-(UIView *)contentViewForCellAtIndex:(int)index;
-(void)openCollapseClickCellAtIndex:(int)index animated:(BOOL)animated;
-(void)closeCollapseClickCellAtIndex:(int)index animated:(BOOL)animated;
-(void)openCollapseClickCellsWithIndexes:(NSArray *)indexArray animated:(BOOL)animated;
-(void)closeCollapseClickCellsWithIndexes:(NSArray *)indexArray animated:(BOOL)animated;

@end
