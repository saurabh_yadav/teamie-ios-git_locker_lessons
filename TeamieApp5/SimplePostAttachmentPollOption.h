//
//  SimplePostAttachmentPollOption.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 5/16/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEPostAttachment.h"

@interface SimplePostAttachmentPollOption : NSObject

@property (nonatomic, strong) NSNumber * currentUserVote;
@property (nonatomic, strong) NSNumber * numVotes;
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * choiceKey;
@property (nonatomic, strong) TMEPostAttachment *newsfeedItemAttachment;

//- (NSNumber*)getFraction;

@end
