//
//  TMEPostAttachment.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 5/15/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostAttachment.h"
#import "TMELinkAttachment.h"
#import "TMEImageAttachment.h"
#import "TMEFileAttachment.h"
#import "TMEVideoAttachment.h"
#import "TMEAudioAttachment.h"

@implementation TMEPostAttachment

+ (NSValueTransformer *)hrefJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

- (UIImage*)previewImage {
    //Overridden in subclasses
    return nil;
}

- (NSString *)displaySubTitle {
    //Overridden in subclasses
    return nil;
}

- (NSString *)attachmentType {
    if ([self isMemberOfClass:TMELinkAttachment.class]) {
        return @"link";
    }
    else if ([self isMemberOfClass:TMEVideoAttachment.class]) {
        return @"video";
    }
    else if ([self isMemberOfClass:TMEAudioAttachment.class]) {
        return @"audio";
    }
    else if ([self isMemberOfClass:TMEFileAttachment.class]) {
        return @"file";
    }
    else if ([self isMemberOfClass:TMEImageAttachment.class]) {
        return @"image";
    }
}

@end
