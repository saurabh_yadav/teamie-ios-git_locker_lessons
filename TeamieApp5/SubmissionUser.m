//
//  SubmissionUser.m
//  TeamieApp5
//
//  Created by Raunak on 17/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "SubmissionUser.h"

@implementation SubmissionUser

@synthesize name;
@synthesize uid;
@synthesize mail;
@synthesize realName;
@synthesize displayPic;

@end
