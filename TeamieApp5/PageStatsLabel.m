//
//  PageStatsLabel.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 2/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "PageStatsLabel.h"

@implementation PageStatsLabel

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect {
    UIEdgeInsets insets = {0, PageStatsLeftRightPaddings, 0, PageStatsLeftRightPaddings};
    return [super drawTextInRect:UIEdgeInsetsInsetRect(rect, insets)];
}

- (void)setTextWhileStickToRightTopCornor:(NSString *)pageReadStatsString
{
    UIFont *font = [TeamieGlobals appFontFor:@"regularFontWithSize11"];

    CGSize expectedTextSize = [pageReadStatsString sizeWithFont:font];
    
    CGSize expectedSize = CGSizeMake(expectedTextSize.width + 2 * PageStatsLeftRightPaddings, expectedTextSize.height);
    
    //Step 1, keep the origin, change the frame size to expected size
    CGRect frame = self.frame;
    CGPoint oldOrigin = frame.origin;
    CGFloat oldFrameWidth = frame.size.width;
    CGFloat centerY = self.center.y;
    frame.size = expectedSize;
    
    
    //Step 2, find the width offset
    CGFloat newFrameWidth = frame.size.width;
    CGFloat widthOffset = newFrameWidth - oldFrameWidth;
    
    //Step 3,move the origin left with the same width offset.
    CGPoint newOrigin = CGPointMake(oldOrigin.x - widthOffset, oldOrigin.y);
    frame.origin = newOrigin;
    
    
    //Step 4, configure the UIlabel
    self.font = font;
    self.text = pageReadStatsString;
    self.textColor = [UIColor colorWithHex:@"#ffffff" alpha:1.0];
    
    self.frame = frame;
    self.layer.cornerRadius = 5.0;
    self.layer.masksToBounds = YES;
    self.backgroundColor = [UIColor colorWithHex:@"#5f6f8c" alpha:1.0];
    self.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    self.textAlignment = NSTextAlignmentCenter;
    //    pageReadStatsLabel.textAlignment = nste
    
    //Step 5,move the center back to the middle area
    self.center = CGPointMake(self.center.x, centerY);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
