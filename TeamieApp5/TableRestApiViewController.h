//
//  TableRestApiViewController.h
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PullRefreshTableViewController.h"

@interface TableRestApiViewController : PullRefreshTableViewController

@property (nonatomic, retain) WSCoachMarksView* coachMarksView;
@property (nonatomic, retain) NSMutableSet *operationsSet;

@property (weak, nonatomic) id parentRevealController;

- (void)showErrorViewWithTitle:(NSString*)title subtitle:(NSString*)subtitle image:(UIImage*)image;
- (void)dismissErrorView;

@end
