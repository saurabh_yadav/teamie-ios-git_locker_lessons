//
//  UIView+UIView_Border.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 16/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum{
    kSolidLine,
    kDashedLine
}LineStyle;

@interface UIView (Border)

- (void)drawTopBorderWithColor:(CGColorRef)borderColor borderWidth:(CGFloat)borderWidth;
- (void)drawRightBorderWithColor:(CGColorRef)borderColor borderWidth:(CGFloat)borderWidth;
- (void)drawBottomBorderWithColor:(CGColorRef)borderColor borderWidth:(CGFloat)borderWidth;
- (void)drawLeftBorderWithColor:(CGColorRef)borderColor borderWidth:(CGFloat)borderWidth;

- (void)drawBorderAtTop:(BOOL)drawTop right:(BOOL)drawRight bottom:(BOOL)drawBottom left:(BOOL)drawLeft withColor:(CGColorRef)borderColor borderWith:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius;

- (void)drawBorderAtTop:(BOOL)drawTop right:(BOOL)drawRight bottom:(BOOL)drawBottom left:(BOOL)drawLeft withColor:(CGColorRef)borderColor borderWith:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius lineStyle:(LineStyle)lineStyle;

- (void)drawInnerShadowWithColor:(CGColorRef)color radius:(CGFloat)radius;

- (void)clearBorderLayers;

@end
