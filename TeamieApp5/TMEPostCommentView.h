//
//  TMEPostCommentView.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 23/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TMEPostComment.h"
#import "TMEPostCommentReply.h"

@class TMEPostCommentView;
@class TMEPostCommentReplyView;

typedef NS_ENUM(NSInteger, TMEPostCommentLevel) {
    TMEPostCommentLevelComment,
    TMEPostCommentLevelReply
};

@protocol TMEPostCommentViewDelegate

- (void)showPostCommentMoreActionsSheet:(TMEPostCommentView *)commentView;
- (void)replyButtonClicked:(NSNumber *)commentID;
- (void)loadComments;
//The request will be sent through the TMEPostCommentView class, this delegate function should just handle the animation
- (void)deleteComment:(TMEPostComment *)comment;
//The request will be sent through the TMEPostCommentReplyView class, this delegate function should just handle the animation
- (void)deleteReply:(TMEPostCommentReply *)reply inComment:(TMEPostComment *)comment;

@end

@interface TMEPostCommentView : UIView
<UIActionSheetDelegate>

#pragma mark - Subviews

@property (nonatomic, weak) UIButton *authorImageButton;
@property (nonatomic, weak) UILabel *authorNameLabel;
@property (nonatomic, weak) UILabel *timePostedLabel;
@property (nonatomic, weak) UIButton *moreButton;
@property (nonatomic, weak) UITextView *commentTextView;
@property (nonatomic, weak) UIView *attachmentsView;
@property (nonatomic, weak) UIView *repliesView;

@property (nonatomic, weak) UIView *leftActionView;
@property (nonatomic, weak) UIButton *repliesCountButton;
@property (nonatomic, weak) UIButton *replyButton;

@property (nonatomic, weak) UIView *rightActionView;
@property (nonatomic, weak) UILabel *likesCountLabel;
@property (nonatomic, weak) UIButton *likeButton;
@property (nonatomic, weak) UILabel *votesCountLabel;
@property (nonatomic, weak) UIButton *voteUpButton;
@property (nonatomic, weak) UIButton *voteDownButton;
@property (nonatomic, weak) UIButton *markRightButton;

#pragma mark - Private Properties

@property (nonatomic, strong) TMEPostComment *comment;
@property (nonatomic, weak) UIViewController<TMEPostCommentViewDelegate> *delegate;
@property (nonatomic) CGFloat leftIndent;
@property (nonatomic) CGFloat rightIndent;
@property (nonatomic, strong) NSNumber *imageSize;
//Whether this comment is a comment to a post or a reply to a comment
@property (nonatomic) TMEPostCommentLevel postCommentLevel;

+ (instancetype)viewWithComment:(TMEPostComment *)comment delegate:(UIViewController<TMEPostCommentViewDelegate> *)delegate;
- (UIActionSheet *)moreActionsSheet;

#pragma mark - For Adding Subviews

//Should only be called by subclass
- (void)createHeader;
- (void)loadContent;
- (void)loadAttachments;
- (void)loadRepliesView;
- (void)loadReplies;
- (void)addActions;
- (void)setStatus;
- (void)addConstraints;

@end

