//
//  SiteInfo.m
//  TeamieApp5
//
//  Created by Raunak on 24/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "TMESiteInfo.h"

@implementation TMESiteInfo

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"siteName": @"sitename",
        @"siteSlogan": @"slogan",
        @"siteSubSlogan": @"sub_slogan",
        @"siteLogoURL": @"logo",
        @"isTeamieLoginSupported": @"workflows.login.teamie",
        @"isGoogleLoginSupported": @"workflows.login.google",
        @"isTeamieRegisterSupported": @"workflows.register.teamie",
        @"baseURL": NSNull.null
    };
}

@end
