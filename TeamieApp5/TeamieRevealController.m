//
//  TeamieRevealController.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 23/9/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TeamieRevealController.h"
#import "UIBadgedBarButtonItem.h"
#import "BulletinBoardViewController.h"
#import "SlideMenuController.h"
#import "TMEClient.h"
#import "TeamieAppDelegate.h"
#import "TMEClassroom.h"
#import "TMELessonPage.h"
#import "TSMessage.h"
#import "TMEPostCreateViewController.h"
#import <TSMessages/TSMessageView.h>

@interface TeamieRevealController()<TSMessageViewProtocol>

@property (nonatomic, weak) UIBadgedBarButtonItem *menuNotificationBadge;
@property (nonatomic, weak) UIViewController<TMERevealDelegate> *topViewController;
@property (nonatomic, strong, readwrite) NSNumber *unreadNotificationsCount;
@property (nonatomic, strong) NSString *linkURL;

@end

@implementation TeamieRevealController

#pragma mark lifecycle

- (void)viewDidLoad {
   [super viewDidLoad];
   self.view.backgroundColor = [TeamieUIGlobalColors navigationBarBackgroundColor];
   
   //This is needed for getting site level permissions
   [self loadUserMenu];
   
   [[NSNotificationCenter defaultCenter]
    addObserver:self
    selector:@selector(remoteNotificationRecieved:)
    name:@"UIApplicationDidReceiveNotificationWhenActive"
    object:nil];

   [[NSNotificationCenter defaultCenter]
    addObserver:self
    selector:@selector(sendPasteboardUrlToShareBox:)
    name:@"pasteboardUrlToShareBox"
    object:nil];
   
   [[NSNotificationCenter defaultCenter]
    addObserver:self
    selector:@selector(createPostWithLink:)
    name:@"createThoughtWithLink"
    object:nil];
}

#pragma mark PKRevealController

//Enough to override this method as other init methods for PKRevealController end up in this function
- (instancetype)initWithFrontViewController:(UIViewController *)frontViewController
                         leftViewController:(UIViewController *)leftViewController
                        rightViewController:(UIViewController *)rightViewController
                                    options:(NSDictionary *)options {
    self = [super initWithFrontViewController:frontViewController
                           leftViewController:leftViewController
                          rightViewController:rightViewController
                                      options:options];

    if (self) {
        [self setReferenceInTopViewController];
    }
    return self;
}

//Only other place frontViewController is set
- (void)setFrontViewController:(UIViewController *)frontViewController {
    [super setFrontViewController:frontViewController];
    [self setReferenceInTopViewController];
}

#pragma mark private

- (void)setReferenceInTopViewController {
    if ([((UINavigationController *)self.frontViewController).topViewController conformsToProtocol:@protocol(TMERevealDelegate)]) {
        self.topViewController = (UIViewController<TMERevealDelegate> *)((UINavigationController *)self.frontViewController).topViewController;
        [self.topViewController setParentRevealController:self];
        [self.topViewController performSelector:@selector(setParentRevealController:) withObject:self];
        [self addSlideMenuButtonWithBadge];
        if ([self.topViewController isKindOfClass:[BulletinBoardViewController class]]) {
            [self unsetUnreadNotificationsCount];
        }
        else {
            [self getNumberOfNewNotifications];
        }
    }
    else {
        self.topViewController = nil;
    }
}

- (void)addSlideMenuButtonWithBadge {
    self.topViewController.navigationItem.leftBarButtonItem =
    [[UIBadgedBarButtonItem alloc] initWithTargetAndImage:self
                                                   action:@selector(toggleMenu)
                                         forControlEvents:UIControlEventTouchDown
                                                    image:[TeamieUIGlobals defaultPicForPurpose:@"list"
                                                                                       withSize:25.0
                                                                                       andColor:[UIColor whiteColor]]];
    self.menuNotificationBadge = (UIBadgedBarButtonItem *)self.topViewController.navigationItem.leftBarButtonItem;
}

- (void)toggleMenu {
    [self showViewController:self.state == PKRevealControllerShowsFrontViewController? self.leftViewController : self.frontViewController];
}

- (void)getNumberOfNewNotifications {
    [[TMEClient sharedClient]
     request:TeamieUserBulletinBoardCountRequest
     parameters:nil
     loadingMessage:nil
     success:^(NSDictionary *response) {
         self.unreadNotificationsCount = response[@"unread"];
         [self.menuNotificationBadge updateBadgeValue:[self.unreadNotificationsCount intValue] refresh:TRUE];
     } failure:^(NSError *error) {
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
             [[TMEClient sharedClient] logout:nil];
             [((TeamieAppDelegate *)[[UIApplication sharedApplication] delegate]) transitionToLoginScreen];
         });
         [TeamieGlobals addTSMessageInController:self
                                           title:TMELocalize(@"error.request-failed")
                                         message:TMELocalize(@"error.access-expired")
                                            type:@"error"
                                        duration:4
                                    withCallback:nil];
     }];
}

- (void)remoteNotificationRecieved:(NSNotification *) notification {
    [self.menuNotificationBadge updateBadgeValue:1];
}

- (void)unsetUnreadNotificationsCount {
   self.unreadNotificationsCount = 0;
   [self.menuNotificationBadge updateBadgeValue:0 refresh:TRUE];
}

- (void)loadUserMenu {
   [[TMEClient sharedClient]
    request:TeamieUserMenuRequest
    parameters:nil
    loadingMessage:nil
    success:^(NSDictionary *response) {
       [[NSUserDefaults standardUserDefaults] setObject:response[@"permissions"] forKey:SITE_PERMISSIONS_KEY];
    } failure:nil];
}

- (void)sendPasteboardUrlToShareBox:(NSNotification*)notification{
   
   [TSMessage setDelegate:self];
   [TSMessage setDefaultViewController:self];
   
   // Add a button inside the message
   [[TSMessageView appearance] setTintColor:UIColor.greenColor];
   NSString  *linkString;
   NSString *detectedLink;
   UIPasteboard *appPasteBoard = [UIPasteboard generalPasteboard];
   linkString = [appPasteBoard string];
   //        NSLog(@" %@", aStringforPasteBoard);
   if (linkString) {
      NSDataDetector *linkDetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink error:nil];
      NSArray *matches = [linkDetector matchesInString:linkString options:0 range:NSMakeRange(0, [linkString length])];
      for (NSTextCheckingResult *match in matches) {
         if ([match resultType] == NSTextCheckingTypeLink) {
            NSURL *url = [match URL];
            NSLog(@"found URL: %@", url);
            detectedLink = [url absoluteString];
         }
      }
      const int clipLength = 30;
      NSString *shortString;
      if([detectedLink length]>clipLength)
      {
         shortString = [NSString stringWithFormat:@"%@...",[detectedLink substringToIndex:clipLength]];
      }
      else{
         shortString = detectedLink;
      }
      if(detectedLink){
         [TSMessage addCustomDesignFromFileWithName:@"AlternativeDesign.json"];
         [TSMessage showNotificationInViewController:self
                                               title:@"Share Copeid URL on Teamie?"
                                            subtitle:shortString
                                               image:nil
                                                type:TSMessageNotificationTypeMessage
                                            duration:5
                                            callback:nil
                                         buttonTitle:@"   Share   "
                                      buttonCallback:^{
                                         NSDictionary* url = [[NSDictionary alloc] initWithObjectsAndKeys:detectedLink, @"urlString" , nil];
                                      [[NSNotificationCenter defaultCenter] postNotificationName:@"createThoughtWithLink" object:self userInfo:url];
                                      }
                                          atPosition:TSMessageNotificationPositionBottom
                                canBeDismissedByUser:YES];
      }
      self.linkURL = detectedLink;
   }
   [appPasteBoard setValue:@"" forPasteboardType:UIPasteboardNameGeneral];
}
- (void)createPostWithLink:(id)sender{
   
   NSLog(@"inside the callback");

   [self.topViewController.navigationController pushViewController:[[TMEPostCreateViewController alloc] initWithCopiedLink:self.linkURL]
                                        animated:YES];
}


@end
