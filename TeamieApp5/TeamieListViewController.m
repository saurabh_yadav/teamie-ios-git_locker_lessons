//
//  TeamieListViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/12/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TeamieListViewController.h"
#import "LessonObject.h"
#import "QuizSimpleObject.h"
#import "Leaderboard.h"
#import "TeamieUser.h"

#import "UserProfileViewController.h"

#import "TeamieWebViewController.h"
#import "LessonPageViewController.h"
#import "LessonTableViewController.h"
#import <TapkuLibrary/TKEmptyView.h>
#import "QuizSubmissionsViewController.h"

#import "UIScrollView+SVInfiniteScrolling.h"

NSInteger MAX_ITEMS_PER_PAGE = 15;


@interface TeamieListViewController ()

@property (nonatomic, assign) TeamieEntityType entityType;
@property (nonatomic, strong) NSNumber* entityId;

- (void)makeWebRequests;    // Private method that makes the required web request

@end

@implementation TeamieListViewController
@synthesize navigationBar = _navigationBar;
@synthesize entityType = _entityType;
@synthesize entityId = _entityId;

- (id)initWithEntity:(TeamieEntityType)entityType withId:(NSNumber*)entityId {
    
    // init the viewController from the storyboard
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:[TeamieGlobals storyboardIdentifier] bundle:nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"TeamieListViewController"];
    
    if (self != nil) {
        _entityId = entityId;
        _entityType = entityType;
        //[TTStyleSheet setGlobalStyleSheet:[[TeamieListStyleSheet alloc] init]];
    }
    
    return self;
}

- (id)initWithEntity:(TeamieEntityType)entityType {
    return [self initWithEntity:entityType withId:nil];
}

- (void)viewDidUnload
{
    [self setNavigationBar:nil];
    [super viewDidUnload];
    _entityId = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // change the color of the navigation bar if any
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHex:@"#BE202E" alpha:1.0]];
}

-(void)viewDidLoad {
    
    [super viewDidLoad];
    
    // if Navigation bar is already present, then hide the Profile Nav Bar
    if(self.navigationController && self.navigationController.navigationBarHidden == NO)
    {
        self.navigationBar.hidden = YES;
    }

    if (!self.parentRevealController) {
        [TeamieGlobals addBackButtonToViewController:self withCallback:@selector(dismissListViewController:)];
    }
    
    // Set defaults to begin with.
    _currentPage = 1;
    _loadMore = YES;
    
    [self makeWebRequests];
    
    // Add a infinite scroll detector if the VC is showing list of lessons or quizzes
    if ((_entityType == TeamieLessonEntity || _entityType == TeamieQuizEntity) && _entityId == nil) {
        __unsafe_unretained typeof(self) weakSelf = self;   // necessary to obtain a weak reference to self in order to have it inside block below
        
        // Add an infinite scrolling detector
        [weakSelf.tableView addInfiniteScrollingWithActionHandler:^{
            if (_loadMore) {    // If 'Load More' is still enabled
                // increment the page number
                _currentPage++;
                // make the web requests for notices
                [weakSelf makeWebRequests];
            }
            else {  // cannot load more lah!
                [weakSelf.tableView.infiniteScrollingView stopAnimating];
            }
        }];
    }
    
    // customize display properties of tableView
    //self.variableHeightRows = YES;
    
    CGFloat navigationBarHeight = 0.0f;
    if (!(self.navigationBar.hidden == YES)) {
        navigationBarHeight = self.navigationBar.frame.size.height;
    }
    CGRect tempFrame = self.tableView.frame;
    tempFrame.origin.y = navigationBarHeight;
    tempFrame.size.height = self.view.frame.size.height - navigationBarHeight;
    self.tableView.frame = tempFrame;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)makeWebRequests {
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    TeamieRESTRequest requestType;
    
    switch (_entityType) {
        case TeamieLessonEntity:
            if (_entityId != nil) {
                [params setValue:[NSString stringWithFormat:@"%qi", [_entityId longLongValue]] forKey:@"nid"];
                requestType = TeamieUserLessonsRequest;
            }
            else {
                [params setObject:[NSString stringWithFormat:@"%i", _currentPage] forKey:@"page"];
                [params setObject:[NSString stringWithFormat:@"%i", MAX_ITEMS_PER_PAGE] forKey:@"items_per_page"];
                requestType = TeamieUserLockerLessonRequest;
            }
            break;
        case TeamieQuizEntity:
            if (_entityId != nil) {
                [params setValue:[NSString stringWithFormat:@"%qi", [_entityId longLongValue]] forKey:@"nid"];
                [params setObject:[NSString stringWithFormat:@"%i", MAX_ITEMS_PER_PAGE] forKey:@"items_per_page"];
                requestType = TeamieUserQuizzesRequest;
            }
            else {
                [params setObject:[NSString stringWithFormat:@"%i", _currentPage] forKey:@"page"];
                [params setObject:[NSString stringWithFormat:@"%i", MAX_ITEMS_PER_PAGE] forKey:@"items_per_page"];
                requestType = TeamieUserLockerQuizRequest;
            }
            break;
        case TeamieLeaderboardEntity:
            [params setValue:[NSString stringWithFormat:@"%qi", [_entityId longLongValue]] forKey:@"nid"];
            requestType = TeamieUserLeaderboardRequest;
            break;
        case TeamieMembersEntity:
            [params setValue:[NSString stringWithFormat:@"%qi", [_entityId longLongValue]] forKey:@"nid"];
            [params setValue:@"0" forKey:@"items_per_page"];
            requestType = TeamieUserMembersRequest;
            break;
            
        default:
#ifdef DEBUG
            NSLog(@"TeamieEntityType #%d not yet supported.", _entityType);
#endif
            return;
            break;
    }
    
    [self.apiCaller performRestRequest:requestType withParams:params];
    
    // Shows up the loading activity indicator within the tableView
    //[self showLoading:YES];
}

#pragma mark - TeamieRESTApi Delegate mathods

- (void)requestFailed:(NSString *)message forRequest:(TeamieRESTRequest)requestName {
    if (requestName == TeamieUserLockerQuizRequest || requestName == TeamieUserLockerLessonRequest) {
        TKEmptyView* view = [[TKEmptyView alloc] initWithFrame:self.view.frame emptyViewImage:TKEmptyViewImageSearch title:@"Service Unavailable" subtitle:@""];
        [self.view addSubview:view];
    }
    
    [self.tableView.infiniteScrollingView stopAnimating];   // stop animating infinite scroll loading
}

-(void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    
    NSMutableArray* entries = [[NSMutableArray alloc] init];
    
    /*if ([_loadedEntries count] && [self.dataSource isKindOfClass:[TeamieListDataSource class]]) {      // If there are items already present then add them to the array list.
        [entries addObjectsFromArray:((TeamieListDataSource*)self.dataSource).items];
    }*/
    
    // No more loading items the moment, empty result is returned.
    if ([response respondsToSelector:@selector(count)] && [response count] == 0) {
        _loadMore = NO;
    }
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd, yyyy"];
    
    [self.tableView.infiniteScrollingView stopAnimating];   // stop animating infinite scroll loading
    
    if (requestName == TeamieUserLessonsRequest || requestName == TeamieUserLockerLessonRequest) {
        
        for (LessonObject* lesson in response) {
            
            NSString* lessonText = [NSString stringWithFormat:@"<div class=\"titleText\">%@</div>", [TeamieGlobals encodeStringToXHTML:lesson.title], nil];
            if (lesson.desc) { lessonText = [lessonText stringByAppendingFormat:@"<div class=\"valueText\">%@</div>", [TeamieGlobals encodeStringToXHTML:lesson.desc], nil]; }

            if (lesson.pages) { lessonText = [lessonText stringByAppendingFormat:@"<div class=\"valueText\"><span class=\"labelText\">No. of pages:</span> %qi</div>", [lesson.pages longLongValue], nil]; }
            
            if (lesson.numVotes) { lessonText = [lessonText stringByAppendingFormat:@"<div class=\"valueText\"><span class=\"labelText\">Votes:</span> %qi</div>", [lesson.numVotes longLongValue], nil]; }
            
            // Add the tags info if it is available
            if ([lesson.tags count]) {
                lessonText = [lessonText stringByAppendingFormat:@"<div class=\"valueText\"><span class=\"labelText\">Tags:</span> %@</div>", [lesson.tags componentsJoinedByString:@", "], nil];
            }
            
            /*TTStyledText * lessonStyledText = [TTStyledText textFromXHTML:lessonText lineBreaks:YES URLs:YES];
            TTTableStyledTextItem * lessonEntry = [TTTableStyledTextItem itemWithText:lessonStyledText];
            lessonEntry.padding = UIEdgeInsetsMake(10, 10, 10, 10);
            
            [entries addObject:lessonEntry];*/
            
        }
        
    }
    else if (requestName == TeamieUserQuizzesRequest || requestName == TeamieUserLockerQuizRequest) {
        
        for (QuizSimpleObject* quiz in response) {
            
            NSString * quizText = [NSString stringWithFormat:@"<div class=\"titleText\">%@</div>", quiz.title, nil];
            
            if (quiz.type) {
                quizText = [quizText stringByAppendingFormat:@"<div class=\"valueText\"><span class=\"labelText\">Type:</span> %@</div>", quiz.type, nil];
            }
            
            if (quiz.deadlineDate) {
                quizText = [quizText stringByAppendingFormat:@"<div><span class=\"labelText\">Deadline:</span> <span class=\"importantText\">%@</span></div>", [dateFormat stringFromDate:[NSDate dateWithTimeIntervalSince1970:[quiz.deadlineDate doubleValue]]], nil];
            }
            else {
                quizText = [quizText stringByAppendingFormat:@"<div><span class=\"labelText\">Deadline:</span> <span class=\"importantText\">%@</span></div>", TeamieLocalizedString(@"NOT_AVAILABLE_VERBOSE", nil), nil];
            }
            
            if (quiz.numVotes) {
                quizText = [quizText stringByAppendingFormat:@"<div class=\"valueText\"><span class=\"labelText\">Votes:</span> %qi</div>", [quiz.numVotes longLongValue], nil];
            }
            
            /*TTStyledText * quizStyledText = [TTStyledText textFromXHTML:quizText lineBreaks:YES URLs:YES];
            TTTeamieQuizItem * quizEntry = [TTTeamieQuizItem itemWithText:quizStyledText showSubmissionButton:quiz.hasAccess forQuizTitle:(NSString*)quiz.title withQID:quiz.qid actionDelegate:self];
            quizEntry.padding = UIEdgeInsetsMake(10, 10, 10, 10);
            
            [entries addObject:quizEntry];*/
            
        }
        
    }
    else if (requestName == TeamieUserLeaderboardRequest) {
        // sort the response objects in decreasing order of Teamie points
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"userPoints" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray* sortedResponse = [response sortedArrayUsingDescriptors:sortDescriptors];
        
        /*for (Leaderboard* userObject in sortedResponse) {
            TTTeamieUserProfileItem* leaderboardItem = [TTTeamieUserProfileItem itemWithText:userObject.realName subtitle:[NSString stringWithFormat:@"%@ points", [userObject.userPoints stringValue]] imageURL:userObject.profileImage profileData:userObject delegate:self selector:@selector(didSelectListItem:)];
            [entries addObject:leaderboardItem];
        }*/
    }
    else if (requestName == TeamieUserMembersRequest) {
        for (TeamieUser* userObject in response) {
            NSString* userRoleText = @"";
            for (NSDictionary* role in userObject.classRoles) {
                if ([role valueForKey:@"role_name"]) {
                    userRoleText = [userRoleText stringByAppendingFormat:@"%@ ", [role valueForKey:@"role_name"]];
                }
            }
            /*TTTeamieUserProfileItem* memberItem = [TTTeamieUserProfileItem itemWithText:userObject.realName subtitle:userRoleText imageURL:userObject.profileImage profileData:userObject delegate:self selector:@selector(didSelectListItem:)];
            [entries addObject:memberItem];
            
            [entries sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                return [((TTTeamieUserProfileItem*)obj1).text localizedCaseInsensitiveCompare:((TTTeamieUserProfileItem*)obj2).text];
            }];*/
        }
    }
    else {
#ifdef DEBUG
        NSLog(@"%@ class is unable to understand the TeamieRESTRequest type.", [self class]);
#endif
        return;
    }
    
    // Get the classroom name given the nid
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"(nid == %qi)", [_entityId longLongValue]];
    NSArray* matchingClassrooms = [self.apiCaller getStoredEntitiesOfClass:[Classroom class] withPredicate:predicate];
    if ([matchingClassrooms count] >= 1) {
        // Set the title of the view as the title of classroom
        Classroom* classItem = [matchingClassrooms objectAtIndex:0];
        NSString* viewTitle = nil;
        
        if (requestName == TeamieUserLessonsRequest) {
            viewTitle = [NSString stringWithFormat:@"%@: %@", classItem.name, TeamieLocalizedString(@"MENU_NAME_LESSONS", nil)];
        }
        else if (requestName == TeamieUserQuizzesRequest) {
            viewTitle = [NSString stringWithFormat:@"%@: %@", classItem.name, TeamieLocalizedString(@"MENU_NAME_QUIZZES", nil)];
        }
        else if (requestName == TeamieUserLeaderboardRequest) {
            viewTitle = [NSString stringWithFormat:@"%@: %@", classItem.name, TeamieLocalizedString(@"MENU_NAME_LEADERBOARD", nil)];
        }
        else if (requestName == TeamieUserMembersRequest) {
            viewTitle = [NSString stringWithFormat:@"%@: %@", classItem.name, TeamieLocalizedString(@"MENU_NAME_MEMBERS", nil)];
        }
        else {  // default title is the classroom name
            viewTitle = classItem.name;
        }
        
        if (self.navigationBar.isHidden == NO) {
            self.navigationBar.topItem.title = viewTitle;
        }
        else if (self.navigationItem) {
            self.navigationItem.title = viewTitle;
        }
    }
    
    if (requestName == TeamieUserLockerQuizRequest || requestName == TeamieUserLockerLessonRequest) {
        NSString* viewTitle = nil;
        viewTitle = [NSString stringWithFormat:@"%@: %@", TeamieLocalizedString(@"MENU_SECTION_LOCKERROOM", nil), (requestName == TeamieUserLockerLessonRequest) ? TeamieLocalizedString(@"MENU_NAME_LESSONS", nil) : TeamieLocalizedString(@"MENU_NAME_QUIZZES", nil)];
        if (self.navigationBar.isHidden == NO) {
            self.navigationBar.topItem.title = viewTitle;
        }
        else if (self.navigationItem) {
            self.navigationItem.title = viewTitle;
        }
        
    }
    
    //TeamieListDataSource* dataSource = [[TeamieListDataSource alloc] initWithItems:entries];
    //self.dataSource = dataSource;
    
    // If there are loaded entries already, then add the response to them. If not, make them equal to the response.
    if ([_loadedEntries isKindOfClass:[NSArray class]]) {
        // store the loaded response for later use
        _loadedEntries = [((NSArray*)_loadedEntries) arrayByAddingObjectsFromArray:response];
    }
    else {
        _loadedEntries = response;
    }
}

- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath {
    
    NSString* entityUrl = nil;
    NSString* baseUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"baseURL"];
    
    if ([_loadedEntries isKindOfClass:[NSArray class]] && baseUrl) {
        if (indexPath.row < ((NSArray*)_loadedEntries).count) {
            if (_entityType == TeamieLessonEntity) {
                LessonObject* lesson = [((NSArray*)_loadedEntries) objectAtIndex:indexPath.row];
//                if (OAUTH_LOGIN_ENABLED) {
//                    entityUrl = [NSString stringWithFormat:@"%@/merge-session/?access_token=%@&redirect_url=%@", TEAMIE_DEFAULT_OAUTH_REST_URL, [[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"], [NSString stringWithFormat:@"node/%qi", [lesson.nid longLongValue]]];
//                }
//                else {
//                    entityUrl = [NSString stringWithFormat:@"%@/node/%qi", baseUrl, [lesson.nid longLongValue]];
//                }
                LessonTableViewController* lpvc = [[LessonTableViewController alloc] initWithLessonDetails:lesson];
                self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:nil action:nil];
                [self.navigationController pushViewController:lpvc animated:YES];
            }
            else if (_entityType == TeamieQuizEntity) {
                Quiz* quiz = [((NSArray*)_loadedEntries) objectAtIndex:indexPath.row];
                if (OAUTH_LOGIN_ENABLED) {
                    entityUrl = [NSString stringWithFormat:@"%@/merge-session/?access_token=%@&redirect_url=%@", TEAMIE_DEFAULT_OAUTH_REST_URL, [[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"], [NSString stringWithFormat:@"quiz/%qi", [quiz.qid longLongValue]]];
                }
                else {
                    entityUrl = [NSString stringWithFormat:@"%@/quiz/%qi", baseUrl, [quiz.qid longLongValue]];
                }
            }
        }
    }
    
    if (entityUrl) {
        // Open up the lesson or quiz in Safari
        // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:entityUrl]];
        //TeamieWebViewController * webController = [[TeamieWebViewController alloc] init];
        //[webController openURL:[NSURL URLWithString:entityUrl]];
        //webController.modalPresentationStyle = UIModalPresentationFullScreen;
        //webController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        //UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webController];
        //[self presentViewController:navigationController animated:YES completion:nil];
    }
    
    // deselect the selected row
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    //[super didSelectObject:object atIndexPath:indexPath];
}

#pragma mark - Model View Controller method

- (void)dismissListViewController:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
}

#pragma mark - TTTeamieUserProfileItem Delegate method

- (void)didSelectListItem:(id)sender {
    
    // check what type of object is the sender and accordingly take action
    NSNumber * userID = nil;
    
    /*if (![sender isKindOfClass:[TTTeamieUserProfileItem class]]) {
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
#ifdef DEBUG
        NSLog(@"Error: The sender is of unrecognized class type which is %@", NSStringFromClass([sender class]));
        return;
#endif
    }
    
    id profileData = ((TTTeamieUserProfileItem*)sender).profileData;
    
    if ([profileData isKindOfClass:[Leaderboard class]]) {
        userID = ((Leaderboard*)profileData).uid;
    }
    else if ([profileData isKindOfClass:[TeamieUser class]]) {
        userID = ((TeamieUser*)profileData).uid;
    }
    else if ([profileData isKindOfClass:[UserProfile class]]) {
        userID = ((TeamieUser*)profileData).uid;
    }
    else {
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
#ifdef DEBUG
        NSLog(@"The profileData in the sender is of unrecognized class type which is %@", NSStringFromClass([sender class]));
        return;
#endif
    }
    
    if (userID != nil) {
        UserProfileViewController* profileVC = [[UserProfileViewController alloc] initWithUserID:[userID longLongValue]];
        if (self.navigationController) {
            // if the current view controller is within a navigation view controller then add the user profile to it
            [self.navigationController pushViewController:profileVC animated:YES];
        }
        else {  // else present it as a modal view controller
            [self presentModalViewController:profileVC animated:YES];
        }
    }*/
}

- (void)buttonPressed:(id)sender {
    UIButton* button = (UIButton*)sender;
    QuizSubmissionsViewController* qsVC = [[QuizSubmissionsViewController alloc] initWithQID:[NSNumber numberWithInteger:button.tag] andTitle:button.accessibilityIdentifier];
    [self.navigationController pushViewController:qsVC animated:YES];
}

@end
