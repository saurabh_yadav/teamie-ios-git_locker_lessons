//
//  LSCollapseClick.m
//  CollapseClickDemo
//
//  Created by Wei Wenbo on 28/5/14.
//  Copyright (c) 2014 Ben Gordon. All rights reserved.
//

#import "LSCollapseClick.h"
#import "LSCollapseClickCell.h"


@interface CollapseClick()

//private methods owned by CollapseClick. Declared here to remove warnings.
- (void)repositionCollapseClickCellsBelowIndex:(int)index withOffset:(float)offset;
- (void)reloadCollapseClick;
- (void)didSelectCollapseClickButton:(UIButton *)titleButton;
@end

@implementation LSCollapseClick

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - Load Data
-(void)reloadCollapseClick {
    // Set Up: Height
    float totalHeight = LSPaddings;
    
    // If Arrays aren't Init'd, Init them
    if (!(self.isClickedArray)) {
        self.isClickedArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
    }
    
    if (!(self.dataArray)) {
        self.dataArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
    }
    
    // Make sure they are clear
    [self.isClickedArray removeAllObjects];
    [self.dataArray removeAllObjects];
    
    // Remove all subviews
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
    
    // Add cells
    for (int xx = 0; xx < [CollapseClickDelegate numberOfCellsForCollapseClick]; xx++) {
        // Create Cell
        LSCollapseClickCell *cell = [LSCollapseClickCell newCollapseClickCellWithTitle:[CollapseClickDelegate titleForCollapseClickAtIndex:xx] index:xx content:[CollapseClickDelegate viewForCollapseClickContentViewAtIndex:xx] lesson:[self.CollapseClickDelegate lesson:xx] classroomID:[self.CollapseClickDelegate classroomIDforIndex:xx]];
        
        
        // Set cell.TitleView's backgroundColor
        if ([(id)CollapseClickDelegate respondsToSelector:@selector(colorForCollapseClickTitleViewAtIndex:)]) {
            cell.TitleView.backgroundColor = [CollapseClickDelegate colorForCollapseClickTitleViewAtIndex:xx];
        }
        else {
            cell.TitleView.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1.0];
        }
        
        
        // Set cell.TitleLabel's Color
        if ([(id)CollapseClickDelegate respondsToSelector:@selector(colorForTitleLabelAtIndex:)]) {
            cell.TitleLabel.textColor = [CollapseClickDelegate colorForTitleLabelAtIndex:xx];
        }
        else {
            cell.TitleLabel.textColor = [UIColor whiteColor];
        }
        
        
        // Set cell.TitleArrow's Color
        if ([(id)CollapseClickDelegate respondsToSelector:@selector(colorForTitleArrowAtIndex:)]) {
//            [cell.TitleArrow drawWithColor:[CollapseClickDelegate colorForTitleArrowAtIndex:xx]];
            cell.arrow.image = [TeamieUIGlobals defaultPicForPurpose:@"right-arrow" withSize:25 andColor:[CollapseClickDelegate colorForTitleArrowAtIndex:xx]];

        }
        else {
//            [cell.TitleArrow drawWithColor:[UIColor colorWithWhite:0.0 alpha:0.35]];
            cell.arrow.image = [TeamieUIGlobals defaultPicForPurpose:@"right-arrow" withSize:25 andColor:[UIColor colorWithWhite:0.0 alpha:0.35]];

        }
        
        // Set cell.LessonInfo's color
        if ([(id)self.CollapseClickDelegate respondsToSelector:@selector(colorForLessonInfo:)]) {
            cell.LessonInfo.backgroundColor = [self.CollapseClickDelegate colorForLessonInfo:xx];
        }
        else {
            [cell.TitleArrow drawWithColor:[UIColor colorWithWhite:0.0 alpha:0.35]];
        }

        
        // Set cell.ContentView's size
        cell.ContentView.frame = CGRectMake(0, LSCollapseHeaderHeight + LSLessonHeaderHeight, self.frame.size.width, cell.ContentView.frame.size.height);

        // Set cell's size
        cell.frame = CGRectMake(0, totalHeight, self.frame.size.width, LSCollapseHeaderHeight + LSLessonHeaderHeight);
        
        cell.containerView.frame = CGRectMake(cell.containerView.frame.origin.x, cell.containerView.frame.origin.y, cell.containerView.frame.size.width, LSCollapseHeaderHeight + LSLessonHeaderHeight);
        // Add target to Button
        [cell.TitleButton addTarget:self action:@selector(didSelectCollapseClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.lessonHeaderButton addTarget:self action:@selector(didClickedLessonHeaderButton:) forControlEvents:UIControlEventTouchUpInside];
        [cell.authorImageButton addTarget:self action:@selector(didclickAuthorImageButton:) forControlEvents:UIControlEventTouchUpInside];

        // Add cell
        [self addSubview:cell];
        
        // Add to DataArray & isClickedArray
        [self.isClickedArray addObject:[NSNumber numberWithBool:NO]];
        [self.dataArray addObject:cell];
        
        // Calculate totalHeight
        totalHeight += LSCollapseHeaderHeight + LSPaddings + LSLessonHeaderHeight;
    }
    
    // Set self's ContentSize and ContentOffset
    [self setContentSize:CGSizeMake(self.frame.size.width, totalHeight)];
    [self setContentOffset:CGPointZero];
}

#pragma mark - Close CollapseClickCell
-(void)closeCollapseClickCellAtIndex:(int)index animated:(BOOL)animated {
    // Check if it's open first

    if ([[self.isClickedArray objectAtIndex:index] boolValue] == YES) {
        float duration = 0;
        if (animated) {
            duration = 0.25;
        }
        [UIView animateWithDuration:duration animations:^{
            // Resize Cell
            LSCollapseClickCell *cell = [self.dataArray objectAtIndex:index];
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, LSCollapseHeaderHeight + LSLessonHeaderHeight);
            cell.containerView.frame = CGRectMake(cell.containerView.frame.origin.x, cell.containerView.frame.origin.y, cell.containerView.frame.size.width, LSCollapseHeaderHeight + LSLessonHeaderHeight);
            // Change Arrow orientation
            CGAffineTransform transform = CGAffineTransformMakeRotation(0);
            cell.arrow.transform = transform;
            
            // Change isClickedArray
            [self.isClickedArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:NO]];
            
            // Reposition all CollapseClickCells below Cell
            [self repositionCollapseClickCellsBelowIndex:index withOffset:-1*(cell.ContentView.frame.size.height)];
        }];
    }
}


//#pragma mark - Open CollapseClickCell
-(void)openCollapseClickCellAtIndex:(int)index animated:(BOOL)animated {
    // Check if it's not open first
    if ([[self.isClickedArray objectAtIndex:index] boolValue] != YES) {
        float duration = 0;
        if (animated) {
            duration = 0.25;
        }
        [UIView animateWithDuration:duration animations:^{
            // Resize Cell
            LSCollapseClickCell *cell = [self.dataArray objectAtIndex:index];
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.ContentView.frame.origin.y + cell.ContentView.frame.size.height );
            
            cell.containerView.frame = CGRectMake(cell.containerView.frame.origin.x, cell.containerView.frame.origin.y, cell.containerView.frame.size.width, cell.ContentView.frame.origin.y + cell.ContentView.frame.size.height);
            // Change Arrow orientation
            CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI/2);
            cell.arrow.transform = transform;
            
            // Change isClickedArray
            [self.isClickedArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:YES]];
            
            // Reposition all CollapseClickCells below Cell
            [self repositionCollapseClickCellsBelowIndex:index withOffset:cell.ContentView.frame.size.height];
        }];
    }
    
    
}

- (void)didClickedLessonHeaderButton:(UIButton *)lessonHeaderButton{
    [self.CollapseClickDelegate didClickedLessonHeaderButton:(int)lessonHeaderButton.tag];
}

- (void) didclickAuthorImageButton: (UIButton *)autorImageButton{
    [self.CollapseClickDelegate didclickAuthorImageButton:(int)autorImageButton.tag];
}
-(void)repositionCollapseClickCellsBelowIndex:(int)index withOffset:(float)offset {
    for (int yy = index+1; yy < self.dataArray.count; yy++) {
        CollapseClickCell *cell = [self.dataArray objectAtIndex:yy];
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y + offset, cell.frame.size.width, cell.frame.size.height);
    }
    
    // Resize self.ContentSize
    CollapseClickCell *lastCell = [self.dataArray objectAtIndex:self.dataArray.count - 1];
    [self setContentSize:CGSizeMake(self.frame.size.width, lastCell.frame.origin.y + lastCell.frame.size.height + LSPaddings)];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (id<LSCollapseClickDelegate>)CollapseClickDelegate{
    return (id<LSCollapseClickDelegate>) CollapseClickDelegate;
}

@end
