//
//  TMEPostCreateAudioAttachmentViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 25/03/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TMEPostCreateAudioAttachmentDelegate <NSObject>

/**
 *  Delegate method which is called when Recording is complete
 *
 *  @param path File Path to where the file is stored on the disk
 *  @param time Length of Audio in seconds
 */
- (void)didCompleteRecordingAudioWithFilePath:(NSURL *)path andAudioLength:(CGFloat)time;

@end

@interface TMEPostCreateAudioAttachmentViewController : UIViewController

@property (nonatomic, strong) id <TMEPostCreateAudioAttachmentDelegate> delegate;
@property (nonatomic) CGFloat progress;

@end
