//
//  InfoViewCell.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 30/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoViewCell : UITableViewCell
@property (strong, nonatomic) UILabel *title;
@property (strong, nonatomic) UILabel *titleValue;

@end
