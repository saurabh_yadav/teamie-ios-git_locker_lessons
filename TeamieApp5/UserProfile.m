//
//  UserProfile.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/25/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "UserProfile.h"
#import "MessageItem.h"
#import "MessageThread.h"
#import "NewsfeedItem.h"
#import "NewsfeedItemComment.h"
#import "UserLoginData.h"


@implementation UserProfile

@dynamic displayPic;
@dynamic email;
@dynamic fullname;
@dynamic profileURL;
@dynamic uid;
@dynamic commentObject;
@dynamic newsfeedItemObjects;
@dynamic userObject;
@dynamic messageThreads;
@dynamic messageItemsWritten;

@end
