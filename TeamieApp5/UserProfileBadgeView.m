//
//  UserProfileBadgeView.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 12/7/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "UserProfileBadgeView.h"

@implementation UserProfileBadgeView

- (id)init {
    // load the view from the XIB file
    self = [[[NSBundle mainBundle] loadNibNamed:@"UserProfileBadgeView" owner:self options:nil] objectAtIndex:0];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"UserProfileBadgeView" owner:self options:nil] objectAtIndex:0];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
    }
    return self;
}

- (void)awakeFromNib {
    self.badgeTitleLabel.font = [TeamieGlobals appFontFor:@"userProfileBadgeTitleText"];
}

@end
