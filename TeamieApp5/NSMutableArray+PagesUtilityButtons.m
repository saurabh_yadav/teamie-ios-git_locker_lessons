//
//  NSMutableArray+PagesUtilityButtons.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 21/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "NSMutableArray+PagesUtilityButtons.h"
#import "NSString+FontAwesome.h"

@implementation NSMutableArray (PagesUtilityButtons)
- (void) addPagesUtilityButtonWithColor:(UIColor *)color icon:(NSString *)icon titleColor:(UIColor *)titleColor{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = color;
    //    [button setTitle:[NSString fontAwesomeIconStringForIconIdentifier:icon] forState:UIControlStateNormal];
    //    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //    [button setAttributedTitle:attributedString forState:UIControlStateNormal];
    //    [button setTitle:[NSString fontAwesomeIconStringForIconIdentifier:@"icon-ok"] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"FontAwesome" size:17] ;
    [button setTitle:[NSString fontAwesomeIconStringForIconIdentifier:icon] forState:UIControlStateNormal];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
//    button. = tintColor;
    [self addObject:button];
}

- (void) setButtonTitleColorAtIndex:(int)index color:(UIColor *)titleColor
{
    UIButton *button = [self objectAtIndex:index];
    [button setTitleColor:titleColor forState:UIControlStateNormal];
}

@end
