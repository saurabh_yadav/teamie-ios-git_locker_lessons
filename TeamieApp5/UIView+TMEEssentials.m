//
//  UIView+TMEEssentials.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 18/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UIView+TMEEssentials.h"
#import <Masonry/Masonry.h>

@interface UIBorderView : UIView

+ (instancetype)borderWithColor:(UIColor *)color;

@end

@implementation UIBorderView

+ (instancetype)borderWithColor:(UIColor *)color {
    UIBorderView *borderView = [UIBorderView new];
    borderView.backgroundColor = color;
    return borderView;
}

@end

@implementation UIView(TMEEssentials)

- (void)addBorderToTop:(BOOL)top
                bottom:(BOOL)bottom
                  left:(BOOL)left
                 right:(BOOL)right {
    [self addBorderToTop:top
                  bottom:bottom
                    left:left
                   right:right
                   width:BORDER_THICKNESS];
}

- (void)addBorderToTop:(BOOL)top
                bottom:(BOOL)bottom
                  left:(BOOL)left
                 right:(BOOL)right
                 width:(CGFloat)width {
    [self addBorderToTop:top
                  bottom:bottom
                    left:left
                   right:right
                   width:width
                   color:[TeamieUIGlobalColors borderColor]];
}

- (void)addBorderToTop:(BOOL)top
                bottom:(BOOL)bottom
                  left:(BOOL)left
                 right:(BOOL)right
                 width:(CGFloat)width
                 color:(UIColor *)color {
    if (top) {
        UIView *border = [UIBorderView borderWithColor:color];
        [self insertSubview:border atIndex:10];
        [border mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.width.equalTo(self);
            make.height.equalTo(@(width));
        }];
    }
    if (bottom) {
        UIView *border = [UIBorderView borderWithColor:color];
        [self insertSubview:border atIndex:10];
        [border mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.left.width.equalTo(self);
            make.height.equalTo(@(width));
        }];
    }
    if (left) {
        UIView *border = [UIBorderView borderWithColor:color];
        [self insertSubview:border atIndex:10];
        [border mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.height.equalTo(self);
            make.width.equalTo(@(width));
        }];
    }
    if (right) {
        UIView *border = [UIBorderView borderWithColor:color];
        [self insertSubview:border atIndex:10];
        [border mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.height.equalTo(self);
            make.width.equalTo(@(width));
        }];
    }
}

- (void)clearBorders {
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:UIBorderView.class]) {
            [view removeFromSuperview];
        }
    }
}

- (id)createAndAddSubView:(Class)modelClass {
    id temporaryView = [modelClass new];
    [self insertSubview:(UIView *)temporaryView atIndex:self.subviews.count];
    return temporaryView;
}

@end
