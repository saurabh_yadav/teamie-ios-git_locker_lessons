//
//  QuizSubmissions.h
//  TeamieApp5
//
//  Created by Raunak on 23/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "QuizStatistics.h"

@interface QuizSubmissions : NSObject

@property (nonatomic,strong) NSArray* questions;
@property (nonatomic,strong) QuizStatistics* stats;
@property (nonatomic,strong) NSArray* details;

@end
