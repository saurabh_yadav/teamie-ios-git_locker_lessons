//
//  TMEPostCommentReplyView.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 23/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TMEPostCommentReply.h"
#import "TMEPostCommentView.h"

@interface TMEPostCommentReplyView : TMEPostCommentView

+ (instancetype)viewWithReply:(TMEPostCommentReply *)reply delegate:(UIViewController<TMEPostCommentViewDelegate> *)delegate;

@property (nonatomic, strong) TMEPostCommentReply *comment;

@end
