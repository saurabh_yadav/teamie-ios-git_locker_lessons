//
//  ThoughtViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/8/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"

@interface ThoughtViewController : UIViewController <UITextFieldDelegate, UIActionSheetDelegate> {
    @private
    UITapGestureRecognizer* tapRecognizer;
    
    NSMutableArray* _pollBars;
    NSMutableArray* _pollLabels;
    NSMutableArray* _pollCheckboxes;
    NSMutableArray* _pollKeys;
}

// Child view controllers
@property (strong, nonatomic) TableRestApiViewController* commentStreamController;

// IBOutlets
@property (weak, nonatomic) IBOutlet UIImageView*thoughtAuthorDisplayPic;
@property (weak, nonatomic) IBOutlet UILabel *thoughtAuthorName;
@property (weak, nonatomic) IBOutlet UILabel *thoughtMessageBody;
@property (weak, nonatomic) IBOutlet UIScrollView *thoughtView;
@property (weak, nonatomic) IBOutlet UIImageView *thoughtTypeIndicator;
@property (weak, nonatomic) IBOutlet UILabel *thoughtTimestamp;
@property (weak, nonatomic) IBOutlet UIButton *thoughtClassroomName;
@property (weak, nonatomic) IBOutlet UIView *thoughtInfoBar;

@property (weak, nonatomic) IBOutlet UIView *commentStreamView;
@property (weak, nonatomic) IBOutlet UIToolbar *bottomToolbar;
@property (weak, nonatomic) IBOutlet UITextField *commentEditor;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *commentPostButton;
@property (weak, nonatomic) IBOutlet UIImageView *commentStreamHeader;


// Data properties
@property (strong, nonatomic) NewsfeedItem* thoughtData;
@property (strong, nonatomic) NSNumber* thoughtID;      // the ID of the thought to load
@property (strong, nonatomic) id thoughtViewDelegate;   // the delegate view controller that must be notified when the thought view controller is dismissed.

// Public API methods
- (id)initWithThoughtID:(NSNumber*)thoughtId delegate:(id)thoughtDelegate;  // initialize a thought view controller with a given thought ID and delegate
- (void)loadThoughtWithTid:(NSNumber*)tid reload:(BOOL)shouldReload;  // load the thought with the given tid into the viewController
//  shouldReload parameter if true, load from web; else load from local db
- (void)dismissThoughtView:(id)sender; // called when the back button is pressed
- (void)thoughtActionSelected:(id)sender;   // called when the thought action button on the navigation bar is pressed
- (void)thoughtPollOptionSelected:(id)sender;   // called whenever any of the poll checkboxes are pressed
- (void)thoughtRefreshed;                       // called whenever refresh button is pressed
- (void)showThoughtActionSheet:(id)sender;      // presents UIActionSheet for Thought Actions

// IBActions
- (IBAction)postThoughtComment:(id)sender;
- (IBAction)textfieldEditingChanged:(id)sender;

// Notification methods
- (void)keyboardWillShow:(NSNotification*)note;
- (void)keyboardWillHide:(NSNotification*)note;
- (void)didTapAnywhere:(UITapGestureRecognizer*)recognizer;

// Comment Actions
- (void)upVote:(id)sender;
- (void)downVote:(id)sender;
- (void)markAsRight:(id)sender;

@end
