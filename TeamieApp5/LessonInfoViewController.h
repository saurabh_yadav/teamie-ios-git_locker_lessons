//
//  LessonInfoViewController.h
//  LS2
//
//  Created by Wei Wenbo on 3/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LessonInfoCollapseClick.h"
#import "TMELesson.h"
#import "StatsFieldView.h"
#import "GoalFieldView.h"
#import "InfoFieldView.h"
#import "ClassroomFieldView.h"
#import "TitleFieldView.h"
#import "PagesFieldView.h"
#import "AuthorFieldView.h"
#import "CompleteReadersField.h"
#import "PagesTableViewCell.h"
#import "CoverImageView.h"
#import <MWPhotoBrowser/MWPhotoBrowser.h>

typedef enum {
    SubjectCell = 0,
    CampusCell,
    SchoolLevelCell,
    infoCellCount
}infoCellName;

typedef enum{
    GoalSection = 0,
    LessonPagesSection,
    ClassroomSection,
    LessonInfoSection,
    LessonStatsSection,
    menuSectionCount,
}menuSectionName;
@interface LessonInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,LessonInfoCollapseClickDelegate,SWTableViewCellDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MWPhotoBrowserDelegate>

//Methods
- (id)initWithLesson:(NSNumber *)lessonID;


//Properties
@property (strong, nonatomic) IBOutlet AuthorFieldView *authorField;
@property (weak, nonatomic) IBOutlet LessonInfoCollapseClick *myCollapseClick;
@property (strong, nonatomic) IBOutlet TitleFieldView *titleField;
@property (strong, nonatomic) IBOutlet GoalFieldView *goalField;
@property (strong, nonatomic) IBOutlet ClassroomFieldView *classroomFieldView;
@property (strong, nonatomic) IBOutlet InfoFieldView *infoField;
@property (weak, nonatomic) IBOutlet UIButton *readButton;
@property (strong, nonatomic) IBOutlet StatsFieldView *statsField;
@property (strong, nonatomic) IBOutlet PagesFieldView *pagesField;
@property (strong, nonatomic) IBOutlet CompleteReadersField *completeReadersField;
@property (strong, nonatomic) IBOutlet CoverImageView *coverImageView;
@property (weak, nonatomic) IBOutlet UIButton *editLessonButton;
@end
