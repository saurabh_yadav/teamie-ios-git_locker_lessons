//
//  AuthorFieldView.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 3/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "AuthorFieldView.h"
#define FontSizeForAuthorLabel 9
@implementation AuthorFieldView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//- (void)configurePublishStatusLabelWithStatus:(PublishStatus)publishStatus
//{
//    switch (publishStatus) {
//        case Removal:
//            [self.publishStatusLabel removeFromSuperview];
//            break;
//        case Drafted:
//            [self publishStatusLabelText:@"Drafted" fontColor:[UIColor colorWithHex:@"#333333" alpha:1.0] backgroundColor:[UIColor colorWithHex:@"#f5dc9a" alpha:1.0]];
//            break;
//        case Published:
//            [self publishStatusLabelText:@"Published" fontColor:[UIColor colorWithHex:@"#ffffff" alpha:1.0] backgroundColor:[UIColor colorWithHex:@"#41A389" alpha:1.0]];
//            break;
//        default:
//            break;
//    }
//}
//
//- (void)publishStatusLabelText:(NSString *)text fontColor:(UIColor *)fontColor backgroundColor:(UIColor *)backgroundColor
//{
//    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:FontSizeForPublishStatus];
//    
//    CGSize expectedTextSize = [text sizeWithFont:font];
//    
//    CGSize expectedSize = CGSizeMake(expectedTextSize.width + 2 * PublishStatusLeftRightPaddings, expectedTextSize.height);
//    //Step 1, keep the origin, change the frame size to expected size
//    CGRect frame = self.publishStatusLabel.frame;
//    CGPoint oldOrigin = frame.origin;
//    CGFloat oldFrameWidth = frame.size.width;
//    CGFloat centerY = self.publishStatusLabel.center.y;
//    frame.size = expectedSize;
//    
//    
//    //Step 2, find the width offset
//    CGFloat newFrameWidth = frame.size.width;
//    CGFloat widthOffset = newFrameWidth - oldFrameWidth;
//    
//    //Step 3,move the origin left with the same width offset.
//    CGPoint newOrigin = CGPointMake(oldOrigin.x - widthOffset, oldOrigin.y);
//    frame.origin = newOrigin;
//    
//    NSLog(@"frame is %@",NSStringFromCGRect(frame));
//    //Step 4, configure the UIlabel
//    self.publishStatusLabel.font = font;
//    self.publishStatusLabel.text = text;
//    self.publishStatusLabel.textColor = fontColor;
//    self.publishStatusLabel.backgroundColor = backgroundColor;
//    
//    self.publishStatusLabel.frame = frame;
//    self.publishStatusLabel.layer.cornerRadius = 5.0;
//    self.publishStatusLabel.layer.masksToBounds = YES;
//    self.publishStatusLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
//    self.publishStatusLabel.textAlignment = NSTextAlignmentCenter;
//    
//    //Step 5,move the center back to the middle area
//    self.publishStatusLabel.center = CGPointMake(self.publishStatusLabel.center.x, centerY);
//    
//    NSLog(@"publish status label is %@",NSStringFromCGRect(self.publishStatusLabel.frame));
//}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
