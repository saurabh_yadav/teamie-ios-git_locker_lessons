//
//  QuestionPageView.h
//  TeamieApp5
//
//  Created by Raunak on 6/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionPageView : UIView

//@property (nonatomic,strong) UIScrollView* scrollView;
//- (id)initWithQuestionCellSize:(CGRect)frame;
- (UIView*)hitTest:(CGPoint)point withEvent:(UIEvent*)event;
- (id)initWithFrame:(CGRect)frame;

@end
