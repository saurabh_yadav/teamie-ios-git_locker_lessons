//
//  NewsfeedItemAction.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/29/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class NewsfeedItem;

@interface NewsfeedItemAction : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * access;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * href;
@property (nonatomic, retain) NSString * method;
@property (nonatomic, retain) NewsfeedItem *newsfeedItem;

@end
