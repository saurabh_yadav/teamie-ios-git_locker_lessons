//
//  SimpleUserMenu.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/20/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"

@interface TMEUserMenu : TMEModel

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSURL * href;
@property (nonatomic, retain) NSString * method;
@property (nonatomic, retain) NSDictionary * reports;

@end
