//
//  LPTableView.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 18/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMELesson.h"

#define RowHeight 49
#define Padding 10
@interface LPTableView : UITableView

- (id)initWithNumberOfLessonPages:(int)numberOfLessonPages index:(int)index;

@property (nonatomic,readonly) NSNumber *index;

@end
