//
//  MessageThreadViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/25/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "MessageThreadViewController.h"
#import "MessageItem.h"

@interface MessageThreadViewController ()

- (void)generateTableItems;

@end

@implementation MessageThreadViewController

@synthesize messageItems;

-(id)initWithItems:(NSArray*)items {
    if (self = [super init]) {
        //self.contentSizeForViewInPopover = TTNEWSFEEDSTYLEVAR(kDefaultMessagesPopoverSize);
        self.messageItems = items;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //self.navigationBarTintColor = [UIColor blackColor];
    //self.variableHeightRows = TRUE;

    [self generateTableItems];
    
    // add a reply button
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:TeamieLocalizedString(@"BTN_REPLY", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(replyButtonTapped:)];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)replyButtonTapped:(id)sender {
    
    // show the TTPostController for composing the reply to the message
    /*TTPostController* replyController = [[TTPostController alloc] init];
    replyController.delegate = self;
    replyController.textView.text = @"";
    replyController.title = TeamieLocalizedString(@"BTN_REPLY", nil);
    [replyController showInView:self.view animated:YES];*/
    
    //[[GANTracker sharedTracker] trackEvent:@"Messages" action:@"Messages Thread Reply Select" label:nil value:0 withError:nil];

}

#pragma mark - TTPostController delegate methods

/**
 * The user has posted text and an animation is about to show the text return to its origin.
 *
 * @return whether to dismiss the controller or wait for the user to call dismiss.
 */
/*- (BOOL)postController:(TTPostController*)postController willPostText:(NSString*)text {
    
    if (text.length <= 0) { // cannot post empty reply. Sorry!
        return NO;
    }
    
    if ([self.messageItems count] == 0) {   // there are no message items. What will u reply to? Dude!
        return NO;
    }
    
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_REPLY_IN_PROGRESS", nil) margin:10.f yOffset:10.f];
    
    // perform REST request to post the reply that was typed out
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%qi", [((MessageItem*)[self.messageItems objectAtIndex:0]).messageThread.tid longLongValue]], @"thread_id", text, @"message", nil];
    
    [self.apiCaller performRestRequest:TeamieUserMessageReplyRequest withParams:params];
    
    [[GANTracker sharedTracker] trackEvent:@"Messages" action:@"Messages Thread Reply Post" label:nil value:0 withError:nil];
    
    return YES;
}*/

/**
 * The text will animate towards a rectangle.
 *
 * @return the rect in screen coordinates where the text should animate towards.
 */
// - (CGRect)postController:(TTPostController*)postController willAnimateTowards:(CGRect)rect;

/**
 * The text has been posted.
 */
/*- (void)postController: (TTPostController*)postController
           didPostText: (NSString*)text
            withResult: (id)result;*/

/**
 * The controller was cancelled before posting.
 */
// - (void)postControllerDidCancel:(TTPostController*)postController;

#pragma mark - Teamie REST API Delegate methods

-(void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    
    [TeamieGlobals dismissActivityLabels];
    
    if (requestName == TeamieUserMessageReplyRequest) {
        if ([response isKindOfClass:[MessageItem class]]) {
            self.messageItems = [self.messageItems arrayByAddingObject:response];
            [self generateTableItems];
            
            NSIndexPath* indexPath = [NSIndexPath indexPathForRow:([self.messageItems count] - 1) inSection:0];
            
            // scroll the table to reveal the latest message that's added
            [self.tableView scrollToRowAtIndexPath:indexPath
                                  atScrollPosition:UITableViewScrollPositionTop
                                          animated:YES];
        }
    }
}

#pragma mark - Private methods

-(void)generateTableItems {
    
    /*NSMutableArray* tableItems = [[NSMutableArray alloc] init];
    
    if ([self.messageItems count] > 0) {
        // set the title as the subject of the current message
        self.navigationItem.title = ((MessageItem*)[self.messageItems objectAtIndex:0]).messageThread.subject;
        
        for (MessageItem* item in self.messageItems) {
            NSString* msgHtml = [TeamieGlobals encodeStringToXHTML:[NSString stringWithFormat:@"<div class=\"msgParticipants\">%@</div><div class=\"msgSubject\">%@</div><div class=\"dateText\">%@</div>", item.author.fullname, item.body, [TeamieGlobals timeIntervalWithStartTime:[item.time_stamp doubleValue] withEndTime:0]]];
            TTTableBulletinItem* messageEntry = [TTTableBulletinItem itemWithText:[TTStyledText textFromXHTML:msgHtml] imageURL:item.author.displayPic delegate:nil selector:nil];
            messageEntry.margin = UIEdgeInsetsMake(5, 5, 0, 5);
            [tableItems addObject:messageEntry];
        }
    }
    
    TeamieListDataSource* dataSource = [[TeamieListDataSource alloc] initWithItems:tableItems];
    self.dataSource = dataSource;*/
    
}

- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
