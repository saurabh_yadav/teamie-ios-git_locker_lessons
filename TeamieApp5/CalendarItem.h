//
//  CalendarItem.h
//  TeamieApp5
//
//  Created by Raunak on 14/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CalendarItem : NSObject

@property (nonatomic, retain) NSNumber* ciid;
@property (nonatomic, retain) NSNumber* cid;
@property (nonatomic, retain) NSString* calendar_name;
@property (nonatomic, retain) NSString* is_public;
@property (nonatomic, retain) NSString* type;
@property (nonatomic, retain) NSNumber* uid;
@property (nonatomic, retain) NSNumber* from_date_timestamp;
@property (nonatomic, retain) NSNumber* to_date_timestamp;
@property (nonatomic, retain) NSDate* from_date;
@property (nonatomic, retain) NSDate* to_date;
@property (nonatomic, retain) NSString* description;

@end

