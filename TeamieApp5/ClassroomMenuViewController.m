//
//  ClassroomMenuViewController.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 12/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "ClassroomMenuViewController.h"
#import "TMEClassroomMenu.h"
#import "LSViewController.h"
#import "PKRevealController.h"
#import "AssessmentListViewController.h"
#import "TMEClient.h"

@interface ClassroomMenuViewController ()

@property (nonatomic)TMEClassroomMenu *classroomMenu;

@end

@implementation ClassroomMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[TMEClient sharedClient]
     request:TeamieUserClassroomMenuRequest
     parameters:@{@"nid": self.classroomID}
     completion:nil success:^(NSDictionary *response) {
         self.classroomMenu = [TMEClassroomMenu parseDictionary:response];
         [self.tableView reloadData];
     } failure:nil];
}

#pragma -mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 5;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = [self titleForTableCellAtRowNumber:indexPath.row];
    cell.textLabel.textAlignment = NSTextAlignmentRight;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Lesson" bundle:nil];
    
    switch (indexPath.row) {
        case 0: {
            LSViewController *lessonListViewController = [[LSViewController alloc]initWithSubjectID:self.classroomID withNavTitle:self.classroomMenu.lessonTitle];
            
            //Defensive code
            NSAssert([self.revealController.frontViewController isKindOfClass:[UINavigationController class]], @"The front view controller should be a navigation controller");
            
            UINavigationController *nav =  (UINavigationController *)self.revealController.frontViewController;
            self.revealController.frontViewController.revealController.recognizesPanningOnFrontView = NO;
            [nav pushViewController:lessonListViewController animated:NO];
            [self.revealController showViewController:lessonListViewController animated:YES completion:nil];
        }
            break;
            
        case 1: {
//            AssessmentListViewController *assessmentListViewController = [[AssessmentListViewController alloc] initWithClassroomId:self.classroomID];
//            UINavigationController *nav =  (UINavigationController *)self.revealController.frontViewController;
//            self.revealController.frontViewController.revealController.recognizesPanningOnFrontView = NO;
//            [nav pushViewController:assessmentListViewController animated:NO];
//            UINavigationController* frontVC = [[UINavigationController alloc] initWithRootViewController:assessmentListViewController];
//            frontVC.navigationBar.translucent = NO;
//            [self.revealController setFrontViewController:frontVC];
//            [self.revealController showViewController:assessmentListViewController animated:YES completion:nil];
        }
            break;
            
        default:
            break;
    }
}

#pragma -mark Private Method
- (NSString *)titleForTableCellAtRowNumber:(NSInteger)rowNum
{
    switch (rowNum) {
        case 0:
            return self.classroomMenu.lessonTitle;
        case 1:
            return self.classroomMenu.quizTitle;
        case 2:
            return self.classroomMenu.gradebookTitle;
        case 3:
            return self.classroomMenu.memberTitle;
        case 4:
            return self.classroomMenu.leaderboardTitle;
        default:
            return @"";
            break;
    }
}

@end
