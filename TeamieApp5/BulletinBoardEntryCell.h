//
//  TMENotificationCell.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 11/9/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMENotification.h"

@interface BulletinBoardEntryCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *mainText;
@property (nonatomic, weak) IBOutlet UILabel *subText;
@property (nonatomic, weak) IBOutlet UIImageView *imageView;

@property (nonatomic, strong) NSMutableDictionary *options;

- (void)setCellData:(TMENotification *)bulletinEntry;
- (CGFloat)getCellHeight:(TMENotification *)bulletinEntry;

@end
