//
//  LessonPageAttachmentTableViewCell.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 28/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonPageAttachmentTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *icon;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
