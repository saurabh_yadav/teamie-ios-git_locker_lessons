//
//  TMEPollAttachmentCell.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 02/01/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEPollAttachmentCell.h"

#import "UIView+TMEEssentials.h"
#import <Masonry/Masonry.h>

@interface TMEPollAttachmentCell()

#pragma mark - Constraints

@property (nonatomic, weak) MASConstraint *rightViewWidthConstraint;
@property (nonatomic, weak) MASConstraint *labelWidthConstraint;
@property (nonatomic, weak) MASConstraint *labelLeftConstraint;

@end

@implementation TMEPollAttachmentCell

static const CGFloat kDefaultPadding = 10.0f;
static const CGFloat kCellHeight = 50.0f;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self addBorderToTop:YES bottom:NO left:NO right:NO];
        [self addRightView];
        [self addMainView];
        [self addConstraints];
    }
    return self;
}

- (void)addRightView {
    self.voteCountView = [self.contentView createAndAddSubView:UIView.class];
    [self.voteCountView addBorderToTop:NO bottom:NO left:YES right:NO];

    self.voteCountLabel = [self.voteCountView createAndAddSubView:UILabel.class];
    self.voteCountLabel.font = [TeamieGlobals appFontFor:@"regularFontWithSize16"];
}

- (void)addMainView {
    self.pollTitleView = [self.contentView createAndAddSubView:UIView.class];
    
    self.titleLabel = [self.pollTitleView createAndAddSubView:UILabel.class];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.numberOfLines = 2;
    [self.titleLabel sizeToFit];
    self.titleLabel.font = [TeamieGlobals appFontFor:@"regularFontWithSize13"];
}

- (void)addConstraints {
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    
    [self.voteCountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(self.contentView);
        make.height.equalTo(@(kCellHeight));
        self.rightViewWidthConstraint = make.width.equalTo(@(kCellHeight));
    }];
    [self.voteCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.voteCountView);
        make.top.equalTo(self.voteCountView).with.offset(12.0f);
    }];
    
    [self.pollTitleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.contentView);
        make.right.equalTo(self.voteCountView.mas_left);
        make.height.equalTo(self.contentView);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.pollTitleView).with.offset(kDefaultPadding);
        make.right.equalTo(self.pollTitleView).with.offset(-kDefaultPadding);
    }];
}

- (void)loadPollAttachment:(TMEPollOption *)pollOption withOptions:(TMEPollAttachment *)options {
    self.titleLabel.text = pollOption.title;
    [self.titleLabel sizeToFit];
    [self.titleLabel mas_updateConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
    }];
    
    if (pollOption.currentUserVote) {
        self.pollTitleView.backgroundColor = [TeamieUIGlobalColors pollOptionHighlightedLabelBackgroundColor];
    }
    
    self.voteCountLabel.text = pollOption.numberOfVotes.stringValue;
    
    if (!options.showPollResults) {
        self.rightViewWidthConstraint.offset = 0.f;
        self.voteCountView.hidden = YES;
    }
}

@end
