//
//  QuestionPageViewController.h
//  TeamieApp5
//
//  Created by Raunak on 6/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "QuestionPageView.h"
#import "QuestionCellFrontViewController.h"
#import "SubmissionDetails.h"
#import "SubmissionQuestion.h"

@interface QuestionPageViewController : UIViewController <UIScrollViewDelegate>

@property (nonatomic,strong) NSArray* questionViews;
@property (nonatomic,strong) SubmissionDetails* userDetails;
@property (nonatomic,strong) NSNumber* quizID;
@property (nonatomic,strong) NSArray* quizQuestions;
@property (nonatomic,strong) NSArray* filteredQuestions;

- (id)initWithUserSubmission:(SubmissionDetails*)details forAllQuestions:(NSArray*)questions andFilteredQuestions:(NSArray*)filteredQuestions quizID:(NSNumber*)quizID;
- (id)initWithUserSubmission:(SubmissionDetails *)details forQuestions:(NSArray *)questions quizID:(NSNumber*)quizID;
@end
