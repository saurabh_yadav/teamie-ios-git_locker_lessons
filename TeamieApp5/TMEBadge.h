//
//  TMEBadge.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEModel.h"

@interface TMEBadge : TMEModel

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSNumber *bid;
@property (nonatomic, strong) NSNumber *awardedBy;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSURL *href;

@end
