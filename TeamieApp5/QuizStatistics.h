//
//  QuizStatistics.h
//  TeamieApp5
//
//  Created by Raunak on 23/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface QuizStatistics : NSObject

@property (nonatomic,strong) NSNumber* totalScore;
@property (nonatomic,strong) NSNumber* maxScore;
@property (nonatomic,strong) NSNumber* minScore;
@property (nonatomic,strong) NSNumber* averageScore;
@property (nonatomic,strong) NSNumber* totalSubmissions;

@end
