//
//  LessonPageViewController.h
//  TeamieApp5
//
//  Created by Raunak on 23/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "LessonContentViewController.h"
#import "LessonPageToolBar.h"

@interface LessonPageViewController : UIViewController <UIPageViewControllerDataSource, UIPageViewControllerDelegate, UIGestureRecognizerDelegate,LessonContentDelegate>

@property (nonatomic,strong) UIPageViewController* pageController;
@property (nonatomic) NSInteger currentPage;
@property (nonatomic,strong) NSArray* lessonsList;
@property (nonatomic,strong) UIBarButtonItem* attachmentButton;
@property (nonatomic,strong) UIBarButtonItem* quizButton;
@property (strong, nonatomic) IBOutlet LessonPageToolBar *toolBar;
@property (strong, nonatomic) NSNumber *lessonID;
-(void) updateLessonStatusWithStatus :(BOOL)status;

@end
