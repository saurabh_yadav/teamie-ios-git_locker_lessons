//
//  UIView+AutoLayout.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 20/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIView(AutoLayout)

- (void)pinSubview:(UIView*)view withEdgeInsets:(UIEdgeInsets)edgeInsets flexibleWidth:(BOOL)isFlexibleWidth flexibleHeight:(BOOL)isFlexibleHeight;

- (NSArray*)verticallyPinSubviews:(NSArray*)subviews underTopView:(UIView*)topView bottomView:(UIView*)bottomView topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin leftRightMargin:(CGFloat)hMargin vDistance:(CGFloat)vDistance;

@end
