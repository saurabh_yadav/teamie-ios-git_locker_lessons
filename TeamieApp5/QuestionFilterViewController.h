//
//  QuestionFilterViewController.h
//  TeamieApp5
//
//  Created by Raunak on 12/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionFilterViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic,strong) UITableView* filterTable;
@property (nonatomic,strong) NSArray* questionPickerList;
@property (nonatomic,strong) NSDictionary* userPickerList;

- (NSArray*)getSelectedQIDs;
- (NSArray*)getSelectedUIDs;
- (void)selectAllUsers;

@end
