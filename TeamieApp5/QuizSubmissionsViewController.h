//
//  QuizSubmissionsViewController.h
//  TeamieApp5
//
//  Created by Raunak on 4/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollapseClickView.h"
#import "QuestionCellView.h"
#import "QuestionCellBackViewController.h"
#import "QuestionFilterTableViewController.h"
#import "SubmissionQuestion.h"
#import "SubmissionUser.h"
#import "QuizSubmissionSortViewController.h"

@interface QuizSubmissionsViewController : UIViewController <CollapseClickViewDelegate, UIPopoverControllerDelegate, QuizSortControllerDelegate, QuestionCellBackViewControllerDelegate>
@property (nonatomic, strong) NSNumber* qid;
@property (nonatomic, strong) NSString* quizTitle;
-(id)initWithQID:(NSNumber*)qid andTitle:(NSString*)title;
@end
