//
//  TeamieGlobals.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/5/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TeamieUIGlobals.h"

@class TTActivityLabel;

#define TMELocalize(key) [TeamieGlobals TMELocalize:key comment:nil]

// UI related global variables
#define UI_NORMAL_BORDER_RADIUS 3.0

//Client ID for google integration
extern NSString * const GOOGLE_CLIENT_ID;

//For AWS SDK integration
extern NSString * const SITE_PERMISSIONS_KEY;
extern NSString * const S3_KEY;
extern NSString * const WHITELIST_TERMS_KEY;
extern NSString * const WHITELIST_ORDER_KEY;
extern NSString * const SITE_INFO_KEY;

/*
 *  System Versioning Preprocessor Macros
 */
#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

// Number / count limits for stuff that appear within the app
static const NSUInteger NUMBER_OF_BULLETIN_BOARD_ITEMS = 20;

static const NSUInteger NUMBER_OF_NEWSFEED_ITEMS = 20;
static const NSUInteger NUMBER_OF_CALENDAR_EVENT_ITEMS = 20;

// Animation duration in app
static const CGFloat kAnimationDuration = 0.2;

typedef enum {
    
    TeamieLessonEntity = 0,
    TeamieQuizEntity,
    TeamieLeaderboardEntity,
    TeamieMembersEntity
    
} TeamieEntityType;

typedef enum {
    
    PlainBulletinNotification,
    ThoughtLikeNotification,
    ThoughtEchoNotification,
    ThoughtAnnouncementNotification,
    ThoughtCommentNotification,
    LessonPostNotification,
    QuizPostNotification,
    BadgeAwardNotification,
    ThoughtCommentActionNotification    // Up-vote, down-vote, mark as right notifications for a thought comment
    
} BulletinNotificationType;

static const int kGoogleAnalyticsImageReferenceValue = 0;
static const int kGoogleAnalyticsLinkReferenceValue  = 1;
static const int kGoogleAnalyticsVideoReferenceValue = 2;
static const int kGoogleAnalyticsFileReferenceValue = 3;
static const int kGoogleAnalyticsAudioReferenceValue = 4;
static const int kGoogleAnalyticsPollReferenceValue = 5;

static const int kGoogleAnalyticsLikeReferenceValue   = 0;
static const int kGoogleAnalyticsUnlikeReferenceValue = 1;
static const int kGoogleAnalyticsSameReferenceValue   = 2;

@interface TeamieGlobals : NSObject

+ (NSString *)TMELocalize:(NSString *)key comment:(NSString *)comment;

// returns a string used to identify the storyboard for the main bundle from app-plist.info file (irrespective of device)
+ (NSString*)storyboardIdentifier;

+ (NSString*)stringFromInteger:(NSInteger)number;

+ (NSString*)timeIntervalToText:(NSTimeInterval)seconds;

// where d1 and d2 are unix timestamps
+ (NSString*)timeIntervalWithStartTime:(NSTimeInterval)d1 withEndTime:(NSTimeInterval)d2;
// where d1 and d2 are NSDate objects
+ (NSString*)timeIntervalWithStartDate:(NSDate*)d1 withEndDate:(NSDate*)d2 brevity:(NSUInteger)brevLevel;

+ (void)addBackButtonToViewController:(UIViewController*)controller withCallback:(SEL)selector;

+ (void)addHUDTextOnlyLabel:(NSString *)labelText afterDelay:(NSTimeInterval)delay;

+ (void)addHUDIndeterminateLabel:(NSString*)labelText;

+ (void)addTSMessageInController:(UIViewController*)viewController title:(NSString*)title message:(NSString*)message type:(NSString*)type duration:(NSTimeInterval)duration withCallback:(id)callback;

+ (void)addTSMessageInController:(UIViewController *)viewController title:(NSString *)title message:(NSString *)message buttonCallback:(id)buttonCallback;

+ (void)addActivityLabelWithStyle:(int)style progress:(BOOL)progress withText:(NSString*)text atPosition:(NSInteger)position;

+ (void)dismissActivityLabels;

+ (void)dismissTSMessage;

+ (void)showErrorViewOnView:(UIView*)superview withFrame:(CGRect)frame withTitle:(NSString*)title subtitle:(NSString*)subtitle image:(UIImage*)image;

+ (void)dismissErrorViewsOnView:(UIView*)superview;

+ (BOOL)hasFourInchDisplay;

+ (UIButton*)createCheckboxButton:(CGRect)frame;

// if there is a url like 'www.theteamie.com/thought/732' this function extracts 732 if urlPrefix is given as 'thought/'
+ (NSString*)extractNumberFromURL:(NSString*)urlString prefix:(NSString*)urlPrefix;

+ (NSUInteger)extractNumberFromString:(NSString*)originalString;    // extracts the first number contained within the string

+ (void)performSelectionHighlightAnimation:(UIView*)view;

+ (void) moveControl:(id)uiControl forKeyboard:(NSNotification*)aNotification up:(BOOL)up distance:(CGFloat)distance inView:(UIView*)view;

+ (void)moveBottomChildOfView:(UIView*)view withBottomConstraint:(NSLayoutConstraint*)bottomConstraint andKeyboardState:(BOOL)showKeyboard andDefaultConstant:(CGFloat)defaultConstant andKeyboardInfo:(NSDictionary*)info;

+ (NSDictionary*)instituteDomainMappings;

+ (NSString*)encodeStringToXHTML:(NSString*)stringToEncode;

+ (void)showMailComposer:(NSDictionary *)params fromController:(id)controller;

+ (UIFont*)appFontFor:(NSString*)purpose;

+ (NSDictionary*)createFileOfType:(NSString*)fileType withData:(id)data andName:(NSString*)filename;

+ (void)cancelTeamieRestRequests;

+ (void)openInappBrowserWithURL:(NSURL *)URL fromResponder:(UIResponder *)responder;
+ (void)openInappBrowserWithURL:(NSURL *)URL fromResponder:(UIResponder *)responder withTitle:(NSString *)title;

+ (NSString *)encryptHmacSHA256:(NSString *)input key:(NSString *)key;

+ (NSDictionary *)prepareLoginParametersWithEmail:(NSString *)email site:(NSString *)site;

+ (NSString *)md5:(NSString *)input;

+ (NSString *)timeTillDate:(NSDate *)date;

@end
