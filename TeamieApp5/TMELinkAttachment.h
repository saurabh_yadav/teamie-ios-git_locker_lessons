//
//  TMELinkAttachment.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostAttachment.h"

@interface TMELinkAttachment : TMEPostAttachment

@property (nonatomic, strong) NSNumber *lid;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *html;

@end
