//
//  TMEVideoAttachment.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEVideoAttachment.h"

@implementation TMEVideoAttachment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"href_mp4" : @"playable.mp4",
    };
}

+ (NSValueTransformer *)fidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)href_mp4JSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

- (UIImage *)previewImage {
    return [TeamieUIGlobals defaultPicForPurpose:@"video"
                                        withSize:15
                                        andColor:[TeamieUIGlobalColors defaultTextColor]];
}

- (NSString *)displaySubTitle {
    return @"Video";
}

@end
