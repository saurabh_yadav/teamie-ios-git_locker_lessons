//
//  TeamieChartsViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 10/19/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TeamieChartsViewController.h"
#import "QuizStatView.h"
#import "TMEGradebook.h"
#import "TMEQuizSimple.h"
#import "TMEUserScore.h"
#import "TMEClient.h"
#import "StatViewItem.h"
#import "StatViewCell.h"
#import "JBChartView.h"

#define kTableCellVPadding 10.0f

@interface TeamieChartsViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray* tableItems;

-(void)initPlot;
-(void)configureHost;
-(void)configureGraph;
-(void)configureChart;
-(void)configureLegend;
-(void)displayQuizStats;

@end

/////////////////////////////////////////////////////////////////////////
/// A private class for the filter that a report will have
@implementation ReportFilter

@synthesize filterName, filterRequestType, filterTitle, filterURL;

- (id)initWithName:(NSString*)name requestType:(TMERequest)reportType {
    self = [super init];
    
    if (self) {
        self.filterName = name;
        self.filterRequestType = [NSNumber numberWithInteger:reportType];
    }
    
    return self;
}

- (id)initWithName:(NSString *)name requestType:(TMERequest)reportType title:(NSString*)title url:(NSString*)url {
    self = [super init];
    
    if (self) {
        self.filterName = name;
        self.filterTitle = title;
        self.filterRequestType = [NSNumber numberWithInteger:reportType];
        self.filterURL = url;
    }
    
    return self;
}

- (void)dealloc {
    self.filterName = nil;
    self.filterRequestType = nil;
}

@end
/////////////////////////////////////////////////////////////////////////

@implementation TeamieChartsViewController

#define REPORT_DESCRIPTION_HEIGHT 60.0
#define REPORT_DESCRIPTION_PADDING 12.0

static const NSString * kGradebookTextPositiveColor = @"#00b143";
static const NSString * kGradebookTextNegativeColor = @"#da2536";
static const NSString * kGradebookTextNeutralColor = @"#2492d7";

@synthesize hostView = _hostView, selectedTheme = _selectedTheme;
@synthesize studentsGradebook;
@synthesize quizStatView = _quizStatView;
@synthesize headerView = _headerView;
@synthesize graphAnnotation = _graphAnnotation;
@synthesize filterButton = _filterButton, reportInfoLabel = _reportInfoLabel;
@synthesize classroomID, plotNumbers = _plotNumbers, plotLabels = _plotLabels;

- (id)initWithReportType:(TMEReportType)reportType {
    self = [super init];
    
    if (self) {
        _reportType = reportType;
        _filterList = [[NSMutableArray alloc] init];
    }
    
    return self;
}

#pragma mark - View Lifecycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        _popoverClass = [UIPopoverController class];
    }
    else {
        _popoverClass = [WEPopoverController class];
    }

    if (_reportType == TeamieGradebookReport) {
        // if the classroom ID is known for retrieving the gradebook, then do so please!
        if (self.classroomID) {
            // make the request for obtaining the classroom gradebook
            [[TMEClient sharedClient]
             request:TeamieUserClassroomGradebookRequest
             parameters:@{@"nid": [NSString stringWithFormat:@"%qi", [self.classroomID longLongValue]]}
             completion:nil success:^(NSDictionary *response) {
                 // then populate the quizzes button with whatever is available
                 self.studentsGradebook = [TMEGradebook parseDictionary:response[@"gradebook"]];
                 
                 // if there are no quizzes, then display an empty message
                 if ([self.studentsGradebook.quizList count] <= 0) {
                     [self resetTableview];
                     [self showErrorViewWithTitle:TeamieLocalizedString(@"MSG_NO_DATA", nil) subtitle:TeamieLocalizedString(@"MSG_NO_QUIZZES", nil) image:nil];
                     return;
                 }
                 
                 // set the first quiz as the selected Quiz
                 [self.filterButton setTitle:((TMEQuizSimple *)[self.studentsGradebook.quizList objectAtIndex:0]).title forState:UIControlStateNormal];
                 _selectedFilterIndex = 0;
                 // Set the filter list as the list of all quizzes
                 _filterList = [[NSMutableArray alloc] initWithArray:self.studentsGradebook.quizList];
                 // display the Quiz stats based on the info loaded
                 [self displayQuizStats];
             } failure:nil];
        }
        
        // Add an export button to the navigation bar, meant to export the Gradebook data to a CSV file
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:TeamieLocalizedString(@"BTN_EXPORT", @"Export") style:UIBarButtonItemStyleBordered target:self action:@selector(exportButtonTapped:)];
    }
    else if (_reportType == TeamieDashboardReport) {
        if ([_filterList count] > 0) {  // if the filter list is set already
            // set the title of the button to be the name of the first filter
            [self.filterButton setTitle:((ReportFilter*)[_filterList objectAtIndex:0]).filterName forState:UIControlStateNormal];
            // if there are several requests to make perform the first one alone and stay put
            [[TMEClient sharedClient]
             request:[((ReportFilter *)[_filterList objectAtIndex:0]).filterRequestType intValue]
             parameters:nil
             completion:nil success:^(NSDictionary *response) {
                 [self parseCustomReport:response];
             } failure:nil];
        }
        else {  // if the filter list is not already set then make the reports menu request
//            [[TMEClient sharedClient]
//             request:TeamieUserCustomReportRequest
//             parameters:nil
//             makeURL:^NSString *(NSString *URL) {
//                 return _filterList.firstObject[@"filterURL"];
//             }
//             completion:nil
//             success:^(id response) {
//                 
//             }
//             failure:nil];
//            
            [[TMEClient sharedClient]
             request:TeamieUserCustomReportRequest
             parameters:nil
             completion:nil
             success:^(NSDictionary *response) {
                 [self setFilterList:response];
                 // if there is at least one ReportFilter
                 if ([_filterList count] >= 1) {
                     // make the REST request for the first report in the list
                     [[TMEClient sharedClient]
                      request:TeamieUserCustomReportRequest
                      parameters:nil
                      makeURL:^NSString *(NSString *URL) {
                          return [NSString stringWithFormat:URL, [[_filterList objectAtIndex:0] valueForKey:@"filterURL"]];
                      }
                      completion:nil success:^(NSDictionary *response) {
                          [self parseCustomReport:response];
                      }
                      failure:nil];
                     [self.filterButton setTitle:[[_filterList objectAtIndex:0] valueForKey:@"filterTitle"] forState:UIControlStateNormal];
                 }
             }
             failure:nil];
        }
    }
    
    // set the Header of the table as the headerView UIView (The headerView contains quizStatsView)
    self.tableView.tableHeaderView = self.headerView;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // Initialize a CoachMarksView for this view controller
    self.coachMarksView = [TeamieUIGlobals initializeCoachMarksView:NSStringFromClass([self class]) forView:self.navigationController.view forElements:nil];
}

- (void)viewDidUnload {
    self.graphAnnotation = nil;
    self.hostView = nil;
    self.selectedTheme = nil;
    self.quizStatView = nil;
    self.studentsGradebook = nil;
    self.filterButton = nil;
    self.classroomID = nil;
    self.plotLabels = nil;
    self.plotNumbers = nil;
    
    [self.wePopoverController dismissPopoverAnimated:NO];
    self.wePopoverController = nil;
    _filterList = nil;
    self.headerView = nil;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Do any additional setup after loading the view.
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHex:@"#BE202E" alpha:1.0]];
    if (_reportType == TeamieGradebookReport) {
        [self.navigationItem setTitle:TeamieLocalizedString(@"TITLE_GRADEBOOK", nil)];
    }
    else {
        [self.navigationItem setTitle:TeamieLocalizedString(@"TITLE_REPORTS", nil)];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [ARAnalytics pageView:@"/reports"];
}

- (void)dismissChartViewController:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - CPTPlotDataSource methods

// returns the number of records for making the plot
-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return [self.plotNumbers count];
}

// returns the data for the plot to be made
-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    if ([plot isKindOfClass:[CPTBarPlot class]]) {
        if (fieldEnum == CPTBarPlotFieldBarLocation)
        {
            // need to return the x-axis position of the bar
            return [NSDecimalNumber numberWithInteger:index];
        }
        else if (fieldEnum == CPTBarPlotFieldBarTip) {
            // need to return the y-axis position of the bar
            if (index < [self.plotNumbers count]) {
                // return the plot number at that particular index
                return [self.plotNumbers objectAtIndex:index];
            }
        }
        else {
            return [NSDecimalNumber zero];
        }
    }
    
    return [NSDecimalNumber zero];
}

-(CPTLayer *)dataLabelForPlot:(CPTPlot *)plot recordIndex:(NSUInteger)index {
    // check if the index is greater than the size of the plotLabels array. If yes, then return
    if (index >= [self.plotLabels count]) {
        return nil;
    }
    
    // 1 - Define label text style
    static CPTMutableTextStyle *labelText = nil;
    if (!labelText) {
        labelText= [[CPTMutableTextStyle alloc] init];
        labelText.color = [CPTColor grayColor];
        labelText.textAlignment = CPTTextAlignmentLeft;
    }
    
    // 2 - Create and return layer with label text
    return [[CPTTextLayer alloc] initWithText:[self.plotLabels objectAtIndex:index] style:labelText];
}

-(NSString *)legendTitleForPieChart:(CPTPieChart *)pieChart recordIndex:(NSUInteger)index {
    return @"";
}

#pragma mark - CPTBarPlotDelegate methods
-(void)barPlot:(CPTBarPlot *)plot barWasSelectedAtRecordIndex:(NSUInteger)index {
    // @Todo: Add some awesome code for stuff to be done when the bar graph is selected
    if (self.wePopoverController) {
        [self.wePopoverController dismissPopoverAnimated:YES];
        self.wePopoverController = nil;
    }
    
    // Retrieve the user info for the user whose bar was tapped
    if (index < [self.studentsGradebook.scoreList count]) {
        TMEUserScore* selectedScoreObject = [self.studentsGradebook.scoreList objectAtIndex:index];
        
        // set the label message as the user name
        NSString* labelMessage = selectedScoreObject.user.realName;

        if (_selectedFilterIndex < [self.studentsGradebook.quizList count]) {
            TMEQuizSimple * selectedQuiz = [self.studentsGradebook.quizList objectAtIndex:_selectedFilterIndex];
            NSPredicate* filterQuiz = [NSPredicate predicateWithFormat:@"(self.entity_id == %qi)", [selectedQuiz.qid longLongValue]];
            NSArray* filteredQuizScore = [selectedScoreObject.quizScores filteredArrayUsingPredicate:filterQuiz];
            if ([filteredQuizScore count] > 0) {
                labelMessage = [labelMessage stringByAppendingFormat:@"\n(Score: %@)", [[[filteredQuizScore objectAtIndex:0] valueForKey:@"score"] stringValue]];
            }
        }
        
        // display an HUDMessage with the username and his score
        [TeamieUIGlobals displayStatusMessage:labelMessage forPurpose:@"profile"];
    }
}

-(CPTFill*) barFillForBarPlot:(CPTBarPlot *)barPlot recordIndex:(NSUInteger)index {

    // check if the bar value is above or below average
    if ([[self.plotNumbers objectAtIndex:index] floatValue] > _avgPlotNumber) {
        return [CPTFill fillWithColor:[CPTColor colorWithCGColor:[UIColor colorWithHex:(NSString*)kGradebookTextPositiveColor alpha:1.0].CGColor]];
    }
    else if ([[self.plotNumbers objectAtIndex:index] floatValue] < _avgPlotNumber) {
        return [CPTFill fillWithColor:[CPTColor colorWithCGColor:[UIColor colorWithHex:(NSString*)kGradebookTextNegativeColor alpha:1.0].CGColor]];
    }
    else {
        return [CPTFill fillWithColor:[CPTColor colorWithCGColor:[UIColor colorWithHex:(NSString*)kGradebookTextNeutralColor alpha:1.0].CGColor]];
    }
}

#pragma mark - Gradebook behaviour

-(void)displayQuizStats {
    if (_selectedFilterIndex >= [self.studentsGradebook.quizList count]) {
#ifdef DEBUG
        NSLog(@"Error: The selected Quiz index is greater than the number of Quiz results available!");
#endif
    }
    
    // initialize a number formatter that might come in handy to display numbers
    NSNumberFormatter* numFormat = [[NSNumberFormatter alloc] init];
    [numFormat setPositiveFormat:@"0.#"];
    
    // empty the plotnumbers and plotlabels arrays
    [self.plotNumbers removeAllObjects];
    [self.plotLabels removeAllObjects];
    
    [self resetTableview];
    
    // Initialize some number formatter stuff
    // useful for converting strings to numbers and back and forth
    NSLocale *l_en = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US"];
    NSNumberFormatter* numForm = [[NSNumberFormatter alloc] init];
    [numForm setLocale:l_en];
    
    // obtain the currently selected Quiz
    TMEQuizSimple* selectedQuiz = [self.studentsGradebook.quizList objectAtIndex:_selectedFilterIndex];
    // set the Quiz Stat View items
    self.quizStatView.weightLabel.text = [selectedQuiz.weightage stringValue];
    self.quizStatView.highScoreLabel.text = [selectedQuiz.highScore stringValue];
    self.quizStatView.lowScoreLabel.text = [selectedQuiz.lowScore stringValue];
    self.quizStatView.avgScoreLabel.text = [selectedQuiz.avgScore stringValue];
    self.quizStatView.totalScoreLabel.text = [selectedQuiz.totalScore stringValue];
    
    // if there is only the scores of one user, then plot a graph of that user's score along with the high, low, average values
    if ([self.studentsGradebook.scoreList count] == 1) {
        [self.plotNumbers addObject:selectedQuiz.highScore];
        [self.plotLabels addObject:TeamieLocalizedString(@"LABEL_HIGH_SCORE", nil)];
        [self.plotNumbers addObject:selectedQuiz.lowScore];
        [self.plotLabels addObject:TeamieLocalizedString(@"LABEL_LOW_SCORE", nil)];
        [self.plotNumbers addObject:selectedQuiz.avgScore];
        [self.plotLabels addObject:TeamieLocalizedString(@"LABEL_AVG_SCORE", nil)];
        TMEUserScore* scoreItem = [self.studentsGradebook.scoreList objectAtIndex:0];
        
        NSString* myQuizScore, *myQuizWeightedScore;
        
        // get the quiz score for that particular quiz
        NSPredicate* filterQuiz = [NSPredicate predicateWithFormat:@"(self.entity_id == %qi)", [selectedQuiz.qid longLongValue]];
        NSArray* filteredQuizzes = [scoreItem.quizScores filteredArrayUsingPredicate:filterQuiz];
        
        // if scores are set then use the score numbers as labels
        if ([filteredQuizzes count] && [[[filteredQuizzes objectAtIndex:0] valueForKey:@"score_set"] boolValue]) {
            myQuizScore = [numFormat stringFromNumber:[[filteredQuizzes objectAtIndex:0] valueForKey:@"score"]];
            myQuizWeightedScore = [numFormat stringFromNumber:[[filteredQuizzes objectAtIndex:0] valueForKey:@"score_weighted"]];
        }
        else {  // display text saying scores not available
            myQuizScore = TeamieLocalizedString(@"NOT_AVAILABLE", nil);
            myQuizWeightedScore = TeamieLocalizedString(@"NOT_AVAILABLE", nil);
        }
        
        [self.plotNumbers addObject:[[filteredQuizzes objectAtIndex:0] valueForKey:@"score"]];
        [self.plotLabels addObject:scoreItem.user.realName];
        
        // add the gradebook Table Items for high score, low score and user score
       /* TTTeamieGradebookTableItem * gradebookItem1 = [TTTeamieGradebookTableItem itemWithText:TeamieLocalizedString(@"LABEL_HIGH_SCORE", nil) gradeText1:[numFormat stringFromNumber:selectedQuiz.highScore] gradeText2:[numFormat stringFromNumber:selectedQuiz.highScore] indicator:0 delegate:nil selector:nil];
        [tableItems addObject:gradebookItem1];

        TTTeamieGradebookTableItem * gradebookItem2 = [TTTeamieGradebookTableItem itemWithText:TeamieLocalizedString(@"LABEL_LOW_SCORE", nil) gradeText1:[numFormat stringFromNumber:selectedQuiz.lowScore] gradeText2:[numFormat stringFromNumber:selectedQuiz.lowScore] indicator:0 delegate:nil selector:nil];
        [tableItems addObject:gradebookItem2];
        TTTeamieGradebookTableItem * gradebookItem3 = [TTTeamieGradebookTableItem itemWithText:TeamieLocalizedString(@"LABEL_AVG_SCORE", nil) gradeText1:[numFormat stringFromNumber:selectedQuiz.avgScore] gradeText2:[numFormat stringFromNumber:selectedQuiz.avgScore] indicator:0 delegate:nil selector:nil];
        [tableItems addObject:gradebookItem3];
        TTTeamieGradebookTableItem * gradebookItem4 = [TTTeamieGradebookTableItem itemWithText:scoreItem.user.realName gradeText1:myQuizScore gradeText2:myQuizWeightedScore indicator:0 delegate:nil selector:nil];
        [tableItems addObject:gradebookItem4];*/
    }
    else {
        // loop through the scores of all the users and plot them
        for (TMEUserScore* scoreItem in self.studentsGradebook.scoreList) {
            
            NSString* quizScore, *weightedScore;
            // get the quiz score for that particular quiz
            NSPredicate* filterQuiz = [NSPredicate predicateWithFormat:@"(self.entity_id == %qi)", [selectedQuiz.qid longLongValue]];
            NSArray* filteredQuizzes = [scoreItem.quizScores filteredArrayUsingPredicate:filterQuiz];
            if ([filteredQuizzes count] <= 0) { // there is no info about the quiz score for this user
                quizScore = TeamieLocalizedString(@"NOT_AVAILABLE", nil);
                weightedScore = TeamieLocalizedString(@"NOT_AVAILABLE", nil);
            }
            else {
                // if the score_set attribute is set to true
                if ([[[filteredQuizzes objectAtIndex:0] valueForKey:@"score_set"] boolValue]) {
                    // add the quiz score to the plot numbers
                    [self.plotNumbers addObject:[[filteredQuizzes objectAtIndex:0] valueForKey:@"score"]];
                    // set the quiz score and weighted score labels
                    if ([[[filteredQuizzes objectAtIndex:0] valueForKey:@"score"] isKindOfClass:[NSNumber class]]) {
                        quizScore = [numFormat stringFromNumber:[[filteredQuizzes objectAtIndex:0] valueForKey:@"score"]];
                    }
                    else {
                        quizScore = [[filteredQuizzes objectAtIndex:0] valueForKey:@"score"];
                    }
                    if ([[[filteredQuizzes objectAtIndex:0] valueForKey:@"score_weighted"] isKindOfClass:[NSNumber class]]) {
                        weightedScore = [numFormat stringFromNumber:[[filteredQuizzes objectAtIndex:0] valueForKey:@"score_weighted"]];
                    }
                    else {
                        weightedScore = [[filteredQuizzes objectAtIndex:0] valueForKey:@"score_weighted"];
                    }
                }
                else {
                    // if the score_set property is false
                    [self.plotNumbers addObject:[NSDecimalNumber zero]];
                    quizScore = TeamieLocalizedString(@"NOT_AVAILABLE", nil);
                    weightedScore = TeamieLocalizedString(@"NOT_AVAILABLE", nil);
                }
            }
            
            // add the user name as the label for the graph plot value
            // [self.plotLabels addObject:scoreItem.user.realName];
            
            /*TTTeamieGradebookTableItem * gradebookItem = [TTTeamieGradebookTableItem itemWithText:scoreItem.user.realName gradeText1:quizScore gradeText2:weightedScore indicator:0 delegate:nil selector:nil];
            [tableItems addObject:gradebookItem];*/
        }
    }
    
    // set the average plot number, so that in the plot the bars are distinguished using colors
    _avgPlotNumber = [selectedQuiz.avgScore floatValue];
    
    // set the indicator for each gradebook table item: positive, negative or neutral
    /*NSUInteger counter = 0;
    for (NSNumber* num in self.plotNumbers) {
        NSInteger colorIndicator = ([num floatValue] > [selectedQuiz.avgScore floatValue]) ? 1 : (([num floatValue] < [selectedQuiz.avgScore floatValue]) ? -1 : 0);
        if (counter < [tableItems count]) {
            [[tableItems objectAtIndex:counter] setEdgeColorIndicator:[NSNumber numberWithInteger:colorIndicator]];
            counter++;
        }
    }*/
    // set the table items to be displayed
    [self.tableView reloadData];
    
    // initalize the graph plot to be drawn based on the values stored in self.plotNumbers
    [self initPlot];
}

#pragma mark - Chart behavior
-(void)initPlot {
    /*if (self.hostView.hostedGraph) {
        // if there is already a graph present then simply reload the data
        [self.hostView.hostedGraph reloadData];
        return;
    }*/
    self.hostView.hostedGraph = nil;
    self.hostView.allowPinchScaling = NO;
    [self configureHost];
    [self configureGraph];
    [self configureChart];
    [self configureLegend];
}

-(void)configureHost {
}

-(void)configureGraph {
    
    // 1 - Create and initialize graph
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.bounds];
    graph.plotAreaFrame.masksToBorder = NO;
    self.hostView.hostedGraph = graph;
    
    graph.paddingBottom = 0.0f;
    graph.paddingLeft  = 0.0f;
    graph.paddingTop    = 0.0f;
    graph.paddingRight  = 0.0f;
    graph.axisSet = nil;
    
    // 2 - Set up text style
    /*CPTMutableTextStyle *textStyle = [CPTMutableTextStyle textStyle];
    textStyle.color = [CPTColor grayColor];
    textStyle.fontName = @"Helvetica-Bold";
    textStyle.fontSize = 16.0f;*/
    
    // 3 - Configure title
    /*NSString *title = @"Student Gradebook";
    graph.title = title;
    graph.titleTextStyle = textStyle;
    graph.titlePlotAreaFrameAnchor = CPTRectAnchorTop;
    graph.titleDisplacement = CGPointMake(0.0f, -12.0f);*/
    graph.title = nil;
    
    // 4 - Set theme
    [graph applyTheme:self.selectedTheme];  // no need to initialize self.selectedTheme since lazy initialization takes place
    
    // 5 - Set up plot space
    CGFloat xMin = 0.0f;
    CGFloat xMax = (CGFloat)[self.plotNumbers count];
    CGFloat yMin = 0.0f;
    CGFloat yMax = [[self.plotNumbers valueForKeyPath:@"@max.floatValue"] floatValue];  // should determine dynamically based on max price
    // make the yMax 20% greater than the greatest value
    yMax *= 1.2f;
    
#ifdef DEBUG
    NSLog(@"We have an xMax of %f and a yMax of %f", xMax, yMax);
#endif
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xMin) length:CPTDecimalFromFloat(xMax)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yMin) length:CPTDecimalFromFloat(yMax)];
}

-(void)configureChart {
    
    // 1 - Get reference to graph
    CPTGraph *graph = self.hostView.hostedGraph;
    
    // 2 - Create chart
    CPTBarPlot * barChart = [CPTBarPlot tubularBarPlotWithColor:[CPTColor redColor] horizontalBars:NO];
    barChart.dataSource = self;
    barChart.delegate = self;
    barChart.identifier = graph.title;
    // 2 - Set up line style
    CPTMutableLineStyle *barLineStyle = [[CPTMutableLineStyle alloc] init];
    barLineStyle.lineColor = [CPTColor lightGrayColor];
    barLineStyle.lineWidth = 0.0f;
    barChart.barWidth = CPTDecimalFromDouble(0.9f);
    barChart.barOffset = CPTDecimalFromDouble(0.5f);
    barChart.lineStyle = barLineStyle;
    
    // barChart.labelRotation = 0.4;
    barChart.labelOffset = 2.5;
    
    /*CPTPieChart *pieChart = [[CPTPieChart alloc] init];
    pieChart.dataSource = self;
    pieChart.delegate = self;
    pieChart.pieRadius = (self.hostView.bounds.size.height * 0.7) / 2;
    pieChart.identifier = graph.title;
    pieChart.startAngle = M_PI_4;
    pieChart.sliceDirection = CPTPieDirectionClockwise;*/
    
    // 3 - Create gradient
    /*CPTGradient *overlayGradient = [[CPTGradient alloc] init];
    overlayGradient.gradientType = CPTGradientTypeAxial;
    overlayGradient = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.0] atPosition:0.9];
    overlayGradient = [overlayGradient addColorStop:[[CPTColor blackColor] colorWithAlphaComponent:0.4] atPosition:1.0];*/
    // pieChart.overlayFill = [CPTFill fillWithGradient:overlayGradient];
    
    // 4 - Add chart to graph
    // [graph addPlot:pieChart];
    [graph addPlot:barChart toPlotSpace:graph.defaultPlotSpace];
}

-(void)configureLegend {
}

//        else if ([response isKindOfClass:[NSDictionary class]]) {  // if the response is a dictionary
//            // iterate through all values in the dictionary looking for the 2 keys "count" and "text"
//            for (id itemKey in response) {
//                if ([[response valueForKey:itemKey] valueForKey:@"count"] && [[response valueForKey:itemKey] valueForKey:@"text"]) {
//                    StatViewItem * statViewItem = [StatViewItem itemWithText:[[response valueForKey:itemKey] valueForKey:@"text"] subtitle:nil statText:[[response valueForKey:itemKey] valueForKey:@"count"] statSubtext:nil barWidth:[NSDecimalNumber zero]];
//                    [self.tableItems addObject:statViewItem];
//                }
//            }
//        }
//
//        if (chartDescription) {
//            self.reportInfoLabel.text = chartDescription;
//        }
//        
//        // if there is no data to be displayed
//        if ([self.tableItems count] <= 0) {
//            [self showErrorViewWithTitle:TeamieLocalizedString(@"MSG_NO_DATA", nil) subtitle:TeamieLocalizedString(@"MSG_NO_DATA_SUBTITLE", nil) image:nil];
//            return;
//        }
//  
//        if ([self.tableItems count] >= 1) {
//            [self.tableView reloadData];
//            
//            [self.coachMarksView start];
//            // Set the coachMarks as shown
//            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"%@%@", COACH_MARKS_SHOWN_PREFIX, NSStringFromClass([self class])]];
//            [[NSUserDefaults standardUserDefaults] synchronize];

#pragma mark - UITableview methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.tableItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    id item = [self.tableItems objectAtIndex:indexPath.row];
    CGFloat height = kTableCellVPadding * 2;
    
    if ([item isKindOfClass:[StatViewItem class]]){
        StatViewItem* statViewItem = (StatViewItem*)item;
        if (statViewItem.text) {
            height += 44.0;
        }
    }
    
    return height;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"Cell";
    UITableViewCell* cell = nil;
    
    id tableItem = [self.tableItems objectAtIndex:indexPath.row];
    
    if ([tableItem isKindOfClass:[StatViewItem class]]){
        StatViewCell* statViewCell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (statViewCell == nil){
            statViewCell = [[StatViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                   reuseIdentifier:cellIdentifier];
        }
        
        [statViewCell setCellData:(StatViewItem*) tableItem];
        [statViewCell setSelectionStyle:UITableViewCellSelectionStyleNone];
        
        cell = statViewCell;
    }

    return cell;

}

- (void)resetTableview{
    [self dismissErrorView];
    
    [self.tableItems removeAllObjects];
    [self.tableView reloadData];
}

#pragma mark - Lazy Instantiation Methods
- (CPTGraphHostingView*)hostView {
    
    CGRect parentRect = self.headerView.frame;
    parentRect.size.height = 200.0;
    parentRect.origin.y = 150.0;    // to make space for the QuizStatView
    
    if (!_hostView) {
        _hostView = [[CPTGraphHostingView alloc] initWithFrame:parentRect];
        _hostView.allowPinchScaling = NO;
        [self.headerView addSubview:_hostView];
    }
    
    return _hostView;
}

- (CPTTheme*)selectedTheme {
    if (!_selectedTheme) {
        // set the selected Theme as a plain white theme
        _selectedTheme = [CPTTheme themeNamed:kCPTPlainWhiteTheme];
    }
    
    return _selectedTheme;
}

- (QuizStatView*)quizStatView {
    if (!_quizStatView) {
        _quizStatView = [[QuizStatView alloc] init];
        _quizStatView.frame = CGRectOffset(_quizStatView.frame, 0.0, 44.0);
        [self.headerView addSubview:_quizStatView];
    }
    
    return _quizStatView;
}

- (UIView*)headerView {
    if (!_headerView) {
        _headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 350)];
        if (_reportType == TeamieDashboardReport) {
            _headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 44);
        }
    }
    
    return _headerView;
}

- (UIButton*)filterButton {
    if (!_filterButton) {
        _filterButton = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, self.tableView.frame.size.width, 44.0)];
        [_filterButton setBackgroundColor:[UIColor clearColor]];
        [_filterButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [_filterButton.titleLabel setFont:[TeamieGlobals appFontFor:@"heading2"]];
        [self.headerView addSubview:_filterButton];
        
        // Add a bottomBorder to the filter button
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, _filterButton.frame.size.height - 1.0, _filterButton.frame.size.width, 1.0f);
        bottomBorder.backgroundColor = [UIColor darkGrayColor].CGColor;
        [_filterButton.layer addSublayer:bottomBorder];
        
        CGFloat arrowSize = 15.0;
        // Add a triangle pointing downwards to suggest it's a dropdown button
        CAShapeLayer *dropdownTriangle = [CAShapeLayer layer];
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, nil, 0, 0);
        CGPathAddLineToPoint(path, nil, arrowSize, 0);
        CGPathAddLineToPoint(path, nil, arrowSize / 2.0, arrowSize / 2.0);
        CGPathAddLineToPoint(path, nil, 0, 0);
        dropdownTriangle.path = path;
        dropdownTriangle.backgroundColor = [UIColor clearColor].CGColor;
        dropdownTriangle.fillColor = [UIColor darkGrayColor].CGColor;
        dropdownTriangle.strokeColor = [UIColor clearColor].CGColor;
        dropdownTriangle.frame = CGRectMake(_filterButton.frame.size.width - arrowSize - 20.0, (_filterButton.frame.size.height - arrowSize / 2.0) / 2, 40, 20);
        [_filterButton.layer addSublayer:dropdownTriangle];
        
        // add a delegate method to be called when the filter button is tapped
        [_filterButton addTarget:self action:@selector(filterButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    return _filterButton;
}

- (UILabel*)reportInfoLabel {
    
    // works with the assumption that reportInfoLabel will be instantiated only when needed
    if (!_reportInfoLabel) {
        _reportInfoLabel = [[UILabel alloc] initWithFrame:CGRectMake(REPORT_DESCRIPTION_PADDING, 44.0, self.headerView.frame.size.width - 2 * REPORT_DESCRIPTION_PADDING, REPORT_DESCRIPTION_HEIGHT)];
        [_reportInfoLabel setBackgroundColor:[UIColor clearColor]];
        [_reportInfoLabel setTextColor:[UIColor colorWithHex:@"#6f6f6f" alpha:1.0]];
        [_reportInfoLabel setFont:[TeamieGlobals appFontFor:@"numberStat-small"]];
        [_reportInfoLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_reportInfoLabel setNumberOfLines:3];
        
        // resize headerview to make space
        CGRect headerViewFrame = self.headerView.frame;
        headerViewFrame.size.height += REPORT_DESCRIPTION_HEIGHT;
        self.headerView.frame = headerViewFrame;
        [self.tableView setTableHeaderView:self.headerView];

        // add the label to the header view
        [self.headerView addSubview:_reportInfoLabel];
        
        // Add a bottomBorder to the label
        CALayer *bottomBorder = [CALayer layer];
        bottomBorder.frame = CGRectMake(0.0f, _reportInfoLabel.frame.size.height - 1.0, _reportInfoLabel.frame.size.width, 1.0f);
        bottomBorder.backgroundColor = [UIColor lightGrayColor].CGColor;
        [_reportInfoLabel.layer addSublayer:bottomBorder];
    }
    
    return _reportInfoLabel;
}

- (NSMutableArray*)tableItems{
    if (!_tableItems){
        _tableItems = [[NSMutableArray alloc]init];
    }
    return _tableItems;
}

- (NSMutableArray*)plotNumbers {
    if (!_plotNumbers) {
        _plotNumbers = [[NSMutableArray alloc] init];
    }
    return _plotNumbers;
}

- (NSMutableArray*)plotLabels {
    if (!_plotLabels) {
        _plotLabels = [[NSMutableArray alloc] init];
    }
    return _plotLabels;
}

#pragma mark - Action methods

- (void)filterButtonTapped:(id)sender {
    
   PopupMenuViewController *contentViewController = [[PopupMenuViewController alloc] initWithTableStyle:UITableViewStylePlain cellStyle:kCellStyleCheckmark selectedRowIndex:[NSNumber numberWithInteger: _selectedFilterIndex]];
    contentViewController.contentSizeForViewInPopover = CGSizeMake(250, 150);
    
    // populate the popover menu
    NSMutableArray * entries = [[NSMutableArray alloc] init];
    
    if (_reportType == TeamieDashboardReport) {
        entries = [_filterList valueForKey:@"filterTitle"];
    }
    else if (_reportType == TeamieGradebookReport) {
        entries = [_filterList valueForKey:@"title"];
    }
    
    [contentViewController setEntries:entries];
    contentViewController.menuDelegate = self;
    [contentViewController setNumMenuItems:entries.count];
    
    self.wePopoverController = [[_popoverClass alloc] initWithContentViewController:contentViewController];
    self.wePopoverController.delegate = self;
    
    self.wePopoverController.passthroughViews = [NSArray arrayWithObject:self.navigationController.navigationBar];
    [self.wePopoverController presentPopoverFromRect:self.filterButton.frame inView:self.view permittedArrowDirections:(UIPopoverArrowDirectionDown|UIPopoverArrowDirectionUp) animated:YES];
    // [self.wePopoverController presentPopoverFromBarButtonItem:self.newsfeedFilterButton permittedArrowDirections:(UIPopoverArrowDirectionDown|UIPopoverArrowDirectionUp) animated:YES];
}

- (void)exportButtonTapped:(id)sender {
    
    if (self.studentsGradebook) {
        NSArray* csvData = [self.studentsGradebook exportDataForCSV];
        NSDictionary* fileInfo = [TeamieGlobals createFileOfType:@"csv" withData:csvData andName:@"Data"];
        
//        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(nid == %qi)", [self.classroomID longLongValue]];
//        NSArray* matchingClassrooms = [self.apiCaller getStoredEntitiesOfClass:[Classroom class] withPredicate:predicate];
        
        NSString* mailSubject = nil;
        
//        if ([matchingClassrooms count] > 0) {
//            mailSubject = [NSString stringWithFormat:TeamieLocalizedString(@"MSG_EMAIL_EXPORT_GRADEBOOK_SUBJECT", @"[Teamie] '%@' Gradebook Exported Data"), ((Classroom*)[matchingClassrooms objectAtIndex:0]).name];
//        }
//        else {
            mailSubject = TeamieLocalizedString(@"MSG_EMAIL_EXPORT_GRADEBOOK_SUBJECT_2", @"[Teamie] Gradebook Exported Data");
//        }
        
        NSDictionary* mailInfo = [NSDictionary dictionaryWithObjectsAndKeys:[NSArray arrayWithObject:fileInfo], @"attachments",
                                  mailSubject, @"subject", TeamieLocalizedString(@"MSG_EMAIL_EXPORT_GRADEBOOK_BODY", @""), @"body", nil];
        [TeamieGlobals showMailComposer:mailInfo fromController:self];
    }
}

#pragma mark - Popover Menu Delegate method
- (void)menuDidExitWithData:(id)sender {
    
    // obtain the text of the filter that was selected
    id data = sender;
    // dismiss the popover
    [self.wePopoverController dismissPopoverAnimated:YES];
    
    if ([data isKindOfClass:[NSString class]]) {
        // get the index of the filter row with that string
        NSInteger currentIndex = 0;
        if (_reportType == TeamieDashboardReport) {
            for (ReportFilter* item in _filterList) {
                if ([item.filterTitle isEqualToString:data]) {
                    // set the filter button title as the currently selected filter
                    [self.filterButton setTitle:item.filterTitle forState:UIControlStateNormal];
                    break;
                }
                currentIndex++;
            }
        }
        else if (_reportType == TeamieGradebookReport) {
            for (TMEQuizSimple * item in _filterList) {
                if ([item.title isEqualToString:data]) {
                    // set the filter button title as the currently selected filter
                    [self.filterButton setTitle:item.title forState:UIControlStateNormal];
                    break;
                }
                currentIndex++;
            }
        }
        
        if (currentIndex == [_filterList count]) {
#ifdef DEBUG
            NSLog(@"Something really weird is going on here. The text of the filter that was tapped does not match the text of any of the filters!");
#endif
            return;
        }
        
        // if control reaches here then currentIndex holds the index of the filter to be applied to the report
        
        if (currentIndex == _selectedFilterIndex) {
            // the selected index is the already selected report
            // so do nothing
            return;
        }
        else {
            _selectedFilterIndex = currentIndex;
        }
        
        // empty the current table before loading the report with new stats
        [self resetTableview];
        
        // if the view controller is displaying dashboard reports then make REST request depending on the report requested
        if (_reportType == TeamieDashboardReport) {
            // perform the REST request required for that filter
            [[TMEClient sharedClient]
             request:TeamieUserCustomReportRequest
             parameters:nil
             makeURL:^NSString *(NSString *URL) {
                 return [NSString stringWithFormat:URL, [[_filterList objectAtIndex:_selectedFilterIndex] valueForKey:@"filterURL"]];
             } completion:nil success:^(NSDictionary *response) {
                 [self parseCustomReport:response];
             } failure:nil];
#ifdef DEBUG
            NSLog(@"Report request has been made with request URL %@", [[_filterList objectAtIndex:_selectedFilterIndex] valueForKey:@"filterURL"]);
#endif
            
            // remove the report description label
            [self.reportInfoLabel removeFromSuperview];
            self.reportInfoLabel = nil;
            
            // reset height of table header
            CGRect headerViewFrame = self.headerView.frame;
            headerViewFrame.size.height -= REPORT_DESCRIPTION_HEIGHT;
            self.headerView.frame = headerViewFrame;
            [self.tableView setTableHeaderView:self.headerView];
        }
        // if the view controller is displaying gradebook stats then display the stats for the selected Quiz
        else if (_reportType == TeamieGradebookReport) {
            [self displayQuizStats];
        }
    }
}

#pragma mark - WePopoverController Delegate methods

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    self.wePopoverController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

#pragma mark - MFMessageComposeViewControllerDelegate methods
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    switch (result) {
        case MFMailComposeResultCancelled:
            break;
        case MFMailComposeResultSaved:
            break;
        case MFMailComposeResultSent:
            break;
        case MFMailComposeResultFailed:
            break;
        default:
            break;
    }
    
    // dimiss the mail compose window
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Private

- (void)parseCustomReport:(NSDictionary *)response {
    // Process the data obtained to produce charts and tables
    [self resetTableview];
    CGFloat maxNumber = 0.0;
    NSNumber * barWidth;
    
    NSString* chartDescription = nil;
    
    if ([response isKindOfClass:[NSDictionary class]] && [response valueForKey:@"description"]) {
        chartDescription = [response valueForKey:@"description"];
        
        if ([response valueForKey:@"data"]) {
            response = [response valueForKey:@"data"];
        }
        else {
            // make response nil so that subsequent if conditions don't evaluate to true
            response = nil;
        }
    }

    TMERequest reportTypeRequested = [[[_filterList objectAtIndex:_selectedFilterIndex] valueForKey:@"filterRequestType"] integerValue];
    
    if (!chartDescription) {    // we gotta set chart description on our own baby!
        switch (reportTypeRequested) {
            case TeamieReportActiveClassroomsRequest:
                chartDescription = TeamieLocalizedString(@"DESC_ACTIVE_CLASSROOMS", nil);
                break;
            case TeamieReportActiveStudentsRequest:
                chartDescription = TeamieLocalizedString(@"DESC_ACTIVE_CLASSROOMS", nil);
                break;
            case TeamieReportLessonContributorsRequest:
                chartDescription = TeamieLocalizedString(@"DESC_LESSON_CONTRIBUTORS", nil);
                break;
            case TeamieReportQuizContributorsRequest:
                chartDescription = TeamieLocalizedString(@"DESC_QUIZ_CONTRIBUTORS", nil);
                break;
            case TeamieReportSiteUsageRequest:
                chartDescription = TeamieLocalizedString(@"DESC_SITE_USAGE", nil);
                break;
            case TeamieReportUserPointStatsRequest:
                chartDescription = TeamieLocalizedString(@"DESC_SITE_POINTS", nil);
                break;
            case TeamieReportUserPointsHistoryRequest:
                chartDescription = TeamieLocalizedString(@"DESC_USERPOINTS_HISTORY", nil);
                break;
            case TeamieReportMostViewedLessonsRequest:
                chartDescription = TeamieLocalizedString(@"DESC_MOST_VIEWED_LESSONS", nil);
                break;
            default:
                break;
        }
    }
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    
    for (NSDictionary * item in response) {
        StatViewItem * statViewItem;
        /////////////////////////
        // Active Classrooms report
        /////////////////////////
        
        if (reportTypeRequested == TeamieReportActiveClassroomsRequest) {
            maxNumber = (maxNumber <= 0.0) ? [(NSNumber*)[response valueForKeyPath:@"@max.points.floatValue"] floatValue] : maxNumber;
            barWidth = (maxNumber <= 0.0) ? [NSDecimalNumber zero] : [NSNumber numberWithFloat:[[item valueForKey:@"points"] floatValue] / maxNumber];
            
            statViewItem = [StatViewItem itemWithText:[[item valueForKey:@"node"] valueForKey:@"name"] subtitle:nil statText:[NSString stringWithFormat:@"%qi", [[item valueForKey:@"points"] longLongValue]]  statSubtext:@"points" barWidth:barWidth];
        }
        
        /////////////////////////
        // Active Students Report
        /////////////////////////
        else if (reportTypeRequested == TeamieReportActiveStudentsRequest) {
            maxNumber = (maxNumber <= 0.0) ? [(NSNumber*)[response valueForKeyPath:@"@max.points.floatValue"] floatValue] : maxNumber;
            barWidth = (maxNumber <= 0.0) ? [NSDecimalNumber zero] : [NSNumber numberWithFloat:[[item valueForKey:@"points"] floatValue] / maxNumber];
            
            statViewItem = [StatViewItem itemWithText:[[item valueForKey:@"user"] valueForKey:@"real_name"] subtitle:nil statText:[NSString stringWithFormat:@"%qi", [[item valueForKey:@"points"] longLongValue]] statSubtext:@"points" barWidth:barWidth];
        }
        
        /////////////////////////
        // Lesson Contributors report
        /////////////////////////
        else if (reportTypeRequested == TeamieReportLessonContributorsRequest) {
            maxNumber = (maxNumber <= 0.0) ? [(NSNumber*)[response valueForKeyPath:@"@max.lessons_created.floatValue"] floatValue] : maxNumber;
            barWidth = (maxNumber <= 0.0) ? [NSDecimalNumber zero] : [NSNumber numberWithFloat:[[item valueForKey:@"lessons_created"] floatValue] / maxNumber];
            
            statViewItem = [StatViewItem itemWithText:[[item valueForKey:@"user"] valueForKey:@"real_name"] subtitle:[[item valueForKey:@"user"] valueForKey:@"mail"] statText:[NSString stringWithFormat:@"%qi", [[item valueForKey:@"lessons_created"] longLongValue]] statSubtext:TeamieLocalizedString(@"$lesson[p]$", nil) barWidth:barWidth];
        }
        
        /////////////////////////
        // Quiz Contributors Report
        /////////////////////////
        
        else if (reportTypeRequested == TeamieReportQuizContributorsRequest) {
            maxNumber = (maxNumber <= 0.0) ? [(NSNumber*)[response valueForKeyPath:@"@max.quizzes_created.floatValue"] floatValue] : maxNumber;
            barWidth = (maxNumber <= 0.0) ? [NSDecimalNumber zero] : [NSNumber numberWithFloat:[[item valueForKey:@"quizzes_created"] floatValue] / maxNumber];
            
            statViewItem = [StatViewItem itemWithText:[[item objectForKey:@"user"] objectForKey:@"real_name"] subtitle:[[item objectForKey:@"user"] objectForKey:@"mail"] statText:[NSString stringWithFormat:@"%qi", [[item valueForKey:@"quizzes_created"] longLongValue]] statSubtext:TeamieLocalizedString(@"$quiz[p]$", nil) barWidth:barWidth];
        }
        
        /////////////////////////
        // User Points History
        /////////////////////////
        else if (reportTypeRequested == TeamieReportUserPointsHistoryRequest) {
            maxNumber = (maxNumber <= 0.0) ? [(NSNumber*)[response valueForKeyPath:@"@max.points.floatValue"] floatValue] : maxNumber;
            barWidth = (maxNumber <= 0.0) ? [NSDecimalNumber zero] : [NSNumber numberWithFloat:[[item valueForKey:@"points"] floatValue] / maxNumber];
            
            NSDate* date = [NSDate dateWithTimeIntervalSince1970:[[item valueForKey:@"unix_timestamp"] doubleValue]];
            NSString* dateString = [dateFormatter stringFromDate:date];
            
            statViewItem = [StatViewItem itemWithText:dateString subtitle:[item valueForKey:@"description"] statText:[item valueForKey:@"points"] statSubtext:[item valueForKey:@"status"] barWidth:barWidth];
        }
        /////////////////////////
        // Most Viewed Lessons
        /////////////////////////
        else if (reportTypeRequested == TeamieReportMostViewedLessonsRequest) {
            maxNumber = (maxNumber <= 0.0) ? [(NSNumber*)[response valueForKeyPath:@"@max.views.floatValue"] floatValue] : maxNumber;
            barWidth = (maxNumber <= 0.0) ? [NSDecimalNumber zero] : [NSNumber numberWithFloat:[[item valueForKey:@"views"] floatValue] / maxNumber];
            
            statViewItem = [StatViewItem itemWithText:[[item valueForKey:@"lesson"] valueForKey:@"name"] subtitle:nil statText:[item valueForKey:@"views"] statSubtext:@"views" barWidth:barWidth];
        }
        /////////////////////////
        // Site Userpoints Stats
        /////////////////////////
        else if (reportTypeRequested == TeamieReportUserPointStatsRequest) {
            maxNumber = (maxNumber <= 0.0) ? [(NSNumber*)[response valueForKeyPath:@"@max.points.floatValue"] floatValue] : maxNumber;
            barWidth = (maxNumber <= 0.0) ? [NSDecimalNumber zero] : [NSNumber numberWithFloat:[[item valueForKey:@"points"] floatValue] / maxNumber];
            
            statViewItem = [StatViewItem itemWithText:[item valueForKey:@"date"] subtitle:@"Total Teamie points earned on this day" statText:[item valueForKey:@"points"] statSubtext:@"points" barWidth:barWidth];
        }
        else {
            break;
        }
        [self.tableItems addObject:statViewItem];
    }
}

- (void)setFilterList:(NSDictionary *)response {
    for (NSDictionary* menuItem in response[@"menu"]) {
        if ([menuItem[@"name"] isEqualToString:@"report"] && menuItem[@"reports"] != nil) {
            // initialize the filterList variable
            _filterList = [[NSMutableArray alloc] init];
            // iterate through all the reports available and populate the _filterList variable
            for (id reportID in menuItem[@"reports"]) {
                if ([menuItem[@"reports"] objectForKey:reportID]) {
                    NSDictionary* reportInfo = [menuItem[@"reports"] objectForKey:reportID];
                    // if the report info has the href, name and title keys
                    if ([reportInfo valueForKey:@"href"] && [reportInfo valueForKey:@"name"] && [reportInfo valueForKey:@"title"]) {
                        TMERequest requestType = TeamieUserCustomReportRequest;
                        // To add a new report make sure you come here and make a change
                        if ([[reportInfo valueForKey:@"name"] isEqualToString:@"active-classroom"]) {
                            requestType = TeamieReportActiveClassroomsRequest;
                        }
                        else if ([[reportInfo valueForKey:@"name"] isEqualToString:@"userpoints-stats"]) {
                            requestType = TeamieReportActiveStudentsRequest;
                        }
                        else if ([[reportInfo valueForKey:@"name"] isEqualToString:@"lesson-contributors"]) {
                            requestType = TeamieReportLessonContributorsRequest;
                        }
                        else if ([[reportInfo valueForKey:@"name"] isEqualToString:@"quiz-contributors"]) {
                            requestType = TeamieReportQuizContributorsRequest;
                        }
                        else if ([[reportInfo valueForKey:@"name"] isEqualToString:@"site-usage-stats"]) {
                            requestType = TeamieReportSiteUsageRequest;
                        }
                        else if ([[reportInfo valueForKey:@"name"] isEqualToString:@"site-userpoints-stats"]) {
                            requestType = TeamieReportUserPointStatsRequest;
                        }
                        else if ([[reportInfo valueForKey:@"name"] isEqualToString:@"most-viewed-lesson"]) {
                            requestType = TeamieReportMostViewedLessonsRequest;
                        }
                        else if ([[reportInfo valueForKey:@"name"] isEqualToString:@"userpoints-history"]) {
                            requestType = TeamieReportUserPointsHistoryRequest;
                        }
                        ReportFilter* filter = [[ReportFilter alloc] initWithName:[reportInfo valueForKey:@"name"] requestType:requestType title:[reportInfo valueForKey:@"title"] url:[reportInfo valueForKey:@"href"]];
                        [_filterList addObject:filter];
                    }
                }//
            }
            break;
        }
    }
}

@end
