//
//  TeamieWebViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 9/20/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

@interface TeamieWebViewController : UIViewController

// property to be used when RevealController displays this view Controller
@property (strong, nonatomic) id parentRevealController;

// method that is called when the 'Close' button is pressed
- (void)closeButtonPressed:(id)sender;

@end
