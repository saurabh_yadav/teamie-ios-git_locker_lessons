//
//  QuizDefaultersListViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 10/09/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizDefaultersListViewController : UIViewController

- (id)initWithQuizId:(NSNumber*)qid;

@end
