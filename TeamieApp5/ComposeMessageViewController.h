//
//  ComposeMessageViewController.h
//  TeamieApp5
//
//  Created by Raunak on 21/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TITokenFieldView.h"
#import "MessageSubjectField.h"

@interface ComposeMessageViewController : UIViewController <TITokenFieldViewDelegate, UITextViewDelegate, UITextFieldDelegate, UIAlertViewDelegate>

@property (nonatomic,strong) TITokenFieldView * tokenFieldView;
@property (nonatomic,strong) MessageSubjectField* subjectView;
@property (nonatomic,strong) UITextView * messageView;
@property (nonatomic,strong) NSArray* users;

- (id)initWithRecipients:(NSArray*)recipients;

@end