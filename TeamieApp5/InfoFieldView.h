//
//  InfoFieldView.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 24/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InfoFieldView : UIView
@property (weak, nonatomic) IBOutlet UITableView *infoTable;

@end
