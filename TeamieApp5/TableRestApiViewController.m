//
//  TableRestApiViewController.m
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"
#import "TMEUser.h"
#import "TMEClient.h"

@implementation TableRestApiViewController

@synthesize coachMarksView;
@synthesize parentRevealController;

- (void)viewDidLoad {
    [super viewDidLoad];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveRefreshTokenNotification:)
                                                 name:@"refreshTokenNotification"
                                               object:nil];
    
    if (self.navigationController && self.navigationController.navigationBarHidden == NO) {
        // set the color of the navigation bar
        [self.navigationController.navigationBar setTintColor:[UIColor colorWithHex:@"#BE202E" alpha:1.0]];
    }
}

- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    //Cancel all requests for this viewController
    for (AFHTTPRequestOperation *operation in self.operationsSet) {
        [operation cancel];
    }
    
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - NSNotification methods

- (void)receiveRefreshTokenNotification:(NSNotification*)notification {
    // this method gets called after the refresh token generates a new access token
}

#pragma mark - UIView Rotation Methods

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)shouldAutorotate {
    return YES;
}

/*- (void)reachabilityChanged:(NSNotification*)notification {
 RKReachabilityObserver* observer = (RKReachabilityObserver*)[notification object];
 
 if ([observer isNetworkReachable]) {
 #ifdef DEBUG
 NSLog(@"We're online! %@", [self class]);
 #endif
 } else {
 #ifdef DEBUG
 NSLog(@"We've gone offline! %@", [self class]);
 #endif
 }
 }*/

#pragma mark - Public methods
- (void)showErrorViewWithTitle:(NSString*)title subtitle:(NSString*)subtitle image:(UIImage*)image{
    UIView* header = self.tableView.tableHeaderView;
    
    CGRect frame = CGRectMake(header.frame.origin.x, header.frame.origin.y + header.frame.size.height, header.bounds.size.width, self.tableView.bounds.size.height - header.frame.size.height - self.tableView.contentInset.top);

    [TeamieGlobals showErrorViewOnView:self.tableView
                             withFrame:frame
                             withTitle:title
                              subtitle:subtitle
                                 image:image];
}

- (void)dismissErrorView{
    [TeamieGlobals dismissErrorViewsOnView:self.tableView];
}

- (void)setParentRevealController:(id)controller {
    self->parentRevealController = controller;
}


@end
