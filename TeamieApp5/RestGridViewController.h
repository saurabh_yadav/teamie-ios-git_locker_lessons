//
//  RestGridViewController.h
//  TeamieApp5
//
//  Created by Teamie on 4/4/14.
//  Copyright (c) 2014 ETH. All rights reserved.
//

#import "AQGridViewController.h"
#import "TeamieRestAPI.h"

@interface RestGridViewController : AQGridViewController <RestApiDelegate>

@property (nonatomic, retain) TeamieRestAPI * apiCaller;

@property (nonatomic, retain) WSCoachMarksView* coachMarksView;

// to be used for maintaining a reference to the revealController that displays this view controller
// useful for adding slide menu icon button on the top navigation bar
@property (strong, nonatomic) id parentRevealController;

@end
