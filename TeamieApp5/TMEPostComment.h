//
//  TMEPostComment.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 5/16/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEUser.h"
#import "TMEPostAction.h"

typedef NS_ENUM(NSInteger, TMEPostCommentType) {
    TMEPostCommentTypeThought,
    TMEPostCommentTypeQuestion,
    TMEPostCommentTypeHomework,
};

@interface TMEPostComment : TMEModel

@property (nonatomic, strong) NSNumber * cid;
@property (nonatomic, strong) NSDate *created;
@property (nonatomic, strong) NSDate *updated;
@property (nonatomic, strong) NSString * message;
@property (nonatomic, strong) NSString * htmlMessage;
@property (nonatomic, strong) NSString * timestampText;
@property (nonatomic) BOOL isAnonymous;
@property (nonatomic, strong) TMEUser *author;
@property (nonatomic, strong) NSNumber *repliesCount;
@property (nonatomic, strong) NSNumber *likeCount;
@property (nonatomic, strong) NSNumber *voteCount;
@property (nonatomic, strong) NSNumber *isMarkedRight;
@property (nonatomic, strong) NSDictionary *actions;
@property (nonatomic, strong) NSArray *actionList;
@property (nonatomic, strong) NSArray* images;
@property (nonatomic, strong) NSArray* links;
@property (nonatomic, strong) NSArray* files;
@property (nonatomic, strong) NSArray* videos;
@property (nonatomic, strong) NSArray *resources;
@property (nonatomic, strong) NSArray *replies;
@property (nonatomic) TMEPostCommentType type;

@property (nonatomic, strong) NSArray *attachments;

- (TMEPostAction *)action:(NSString *)actionName;

@end
