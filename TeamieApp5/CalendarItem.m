//
//  CalendarItem.m
//  TeamieApp5
//
//  Created by Raunak on 14/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "CalendarItem.h"

@implementation CalendarItem

@synthesize ciid;
@synthesize cid;
@synthesize calendar_name;
@synthesize is_public;
@synthesize type;
@synthesize uid;
@synthesize from_date_timestamp;
@synthesize to_date_timestamp;
@synthesize from_date;
@synthesize to_date;
@synthesize description;

-(NSDate*)from_date {
//    return [NSDate dateWithTimeIntervalSince1970:([self.from_date_timestamp doubleValue] + 8*60*60)];
    NSTimeInterval systemTimeZoneOffset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    NSTimeInterval singaporeTimeZoneOffset = [[NSTimeZone timeZoneWithAbbreviation:@"SGT"] secondsFromGMT];
    double actualTimestamp = ([self.from_date_timestamp doubleValue] + 8*60*60) + (singaporeTimeZoneOffset - systemTimeZoneOffset);
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:actualTimestamp];
    return date;
}

-(NSDate*)to_date {
//    return [NSDate dateWithTimeIntervalSince1970:([self.to_date_timestamp doubleValue] + 8*60*60)];
    NSTimeInterval systemTimeZoneOffset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; // You could also use the systemTimeZone method
    NSTimeInterval singaporeTimeZoneOffset = [[NSTimeZone timeZoneWithAbbreviation:@"SGT"] secondsFromGMT];
    double actualTimestamp = ([self.to_date_timestamp doubleValue] + 8*60*60) + (singaporeTimeZoneOffset - systemTimeZoneOffset);
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:actualTimestamp];
    return date;
}

@end