//
//  LessonPageToolBar.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 25/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "LessonPageToolBar.h"

@implementation LessonPageToolBar

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
