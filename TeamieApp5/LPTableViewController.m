//
//  LPTableViewController.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 17/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "LPTableViewController.h"
#import "TeamieWebViewController.h"
#import "LessonPagesList.h"
#import "LessonPage.h"
#import "LPTableViewCell.h"
#import "PublishStatusLabel.h"

#define RowHeight 49
@interface LPTableViewController ()
@property LessonObject *lesson;
@property (readwrite) NSArray *lessonPages;
@end

@implementation LPTableViewController{
    BOOL shouldDisplayLabel;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithLesson:(LessonObject *)lesson{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Lesson" bundle:nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"LPTableViewController"];
    self.lesson = lesson;
    shouldDisplayLabel = YES;
    self.tableView.frame = CGRectMake(0, 0, 320, RowHeight * [self.lesson.pages intValue]);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self makeWebRequest];

    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (shouldDisplayLabel) {
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING", nil) margin:10.0 yOffset:10.0 afterDelay:3.0];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)makeWebRequest
{
    NSDictionary* dict = [NSDictionary dictionaryWithObject:self.lesson.nid forKey:@"lid"];
    [self.apiCaller performRestRequest:TeamieUserLessonPagesListRequest withParams:dict];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RowHeight;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.lessonPages.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.lessonPages.count != 0) {
        static NSString *cellIdentifier = @"Cell";
        LPTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[LPTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSDictionary *lessonPage = [self.lessonPages objectAtIndex:indexPath.row];
        
        cell.textLabel.text = [lessonPage valueForKey:@"title"];
        cell.textLabel.font = [UIFont systemFontOfSize:11];
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        PublishStatusLabel *label = [[PublishStatusLabel alloc]initWithFrame:CGRectMake(100, 10, 15, 15)];
        [label configurePublishStatusLabelWithStatus:Drafted];
        cell.accessoryView = label;
        
//        cell.accessoryView.backgroundColor = [UIColor yellowColor];
        
        UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
        separatorLineView.backgroundColor = [UIColor colorWithHex:@"#dddddd" alpha:1.0];
        [cell.contentView addSubview:separatorLineView];
        
        return cell;
    }
    
    //There is no lesson pages yet, we will return nil
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LessonPageViewController *lessonPageViewController = [[LessonPageViewController alloc]init];
    lessonPageViewController.lessonsList = self.lessonPages;
    
    lessonPageViewController.currentPage = indexPath.row;
    
    [self.delegate navigateToLessonPageViewController:lessonPageViewController];
}


#pragma mark - RestApiDelegate
- (void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response
{
    NSAssert([self.lesson.pages intValue] == [((LessonPagesList*)[response objectAtIndex:0]).lessonPages count], @"the page number obtained in the request for lesson object should be consistent with that obtained in the request for lesson page list object");
    [TeamieGlobals dismissActivityLabels];
    shouldDisplayLabel = NO;
    if ([((LessonPagesList*)[response objectAtIndex:0]).lessonPages count] == 0) {
        NSString* entityUrl = nil;
        NSString* baseUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"baseURL"];
        if (OAUTH_LOGIN_ENABLED) {
            entityUrl = [NSString stringWithFormat:@"%@/merge-session/?access_token=%@&redirect_url=%@", TEAMIE_DEFAULT_OAUTH_REST_URL, [[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"], [NSString stringWithFormat:@"node/%qi", [self.lesson.nid longLongValue]]];
        }
        else {
            entityUrl = [NSString stringWithFormat:@"%@/node/%qi", baseUrl, [self.lesson.nid longLongValue]];
        }
        TeamieWebViewController * webController = [[TeamieWebViewController alloc] init];
        //TODO
        /**********************************
         ***********TODO: openURL**********
         *********************************/
//        [webController openURL:[NSURL URLWithString:
        webController.modalPresentationStyle = UIModalPresentationFullScreen;
        webController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webController];
        [self presentViewController:navigationController animated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:NO];
        
    }
    
    else {
        self.lessonPages = ((LessonPagesList*)[response objectAtIndex:0]).lessonPages;
        [self.tableView reloadData];
        self.tableView.frame = CGRectMake(0, 0, 320, self.lessonPages.count * RowHeight);

    }
}

- (void)requestFailed:(NSString *)message forRequest:(TeamieRESTRequest)requestName
{
    [TeamieGlobals dismissActivityLabels];
    shouldDisplayLabel = NO;
}
@end
