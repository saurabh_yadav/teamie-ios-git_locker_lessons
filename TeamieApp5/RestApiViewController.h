//
//  RestApiViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/26/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//
//  Info:
//  Any UIViewController that wishes to make use of the REST API functions 
//  can simply inherit from this class.

#import <UIKit/UIKit.h>

@interface RestApiViewController : UIViewController <RestApiDelegate, RKRequestQueueDelegate, RKRequestDelegate>

@property (nonatomic, retain) TeamieRestAPI * apiCaller;

@property (nonatomic, retain) WSCoachMarksView* coachMarksView;

// to be used for maintaining a reference to the revealController that displays this view controller
// useful for adding slide menu icon button on the top navigation bar
@property (strong, nonatomic) id parentRevealController;

@end
