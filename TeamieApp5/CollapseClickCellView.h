//
//  CollapseClickCellView.h
//  TeamieApp5
//
//  Created by Raunak on 4/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ScoreBarView.h"
#import "ScoreBoxView.h"
#import "QuestionPageViewController.h"

#define kCCHeaderHeight 100

@interface CollapseClickCellView : UIView

@property (strong, nonatomic) IBOutlet ScoreBarView *scoreBarView;
@property (strong, nonatomic) IBOutlet UILabel *nameLabel;
@property (strong, nonatomic) IBOutlet UILabel *submittedLabel;
@property (strong, nonatomic) IBOutlet UILabel *completedLabel;
@property (strong, nonatomic) IBOutlet UIImageView *displayImageView;
@property (strong, nonatomic) IBOutlet UIButton *headerButton;
@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (strong, nonatomic) IBOutlet UILabel *lateLabel;
@property (strong, nonatomic) IBOutlet UILabel *gradedLabel;

@property (strong, nonatomic) IBOutlet UIView *contentView;
@property (strong, nonatomic) IBOutlet ScoreBoxView *scoreBoxView;

@property (nonatomic, assign) BOOL isClicked;
@property (nonatomic, assign) int index;

+ (CollapseClickCellView*)newCollapseClickCellWithDetails:(QuestionPageViewController*)qpVC maxScore:(NSNumber*)maxScore index:(int)index;

@end
