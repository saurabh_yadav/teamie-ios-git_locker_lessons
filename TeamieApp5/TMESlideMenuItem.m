//
//  TMESlideMenuItem.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 5/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMESlideMenuItem.h"

@implementation TMESlideMenuItem

+ (instancetype)item:(TMESlideMenuType)menu text:(NSString *)text icon:(NSString *)icon {
    TMESlideMenuItem *item = [TMESlideMenuItem new];
    item.menuType = menu;
    item.icon = icon;
    item.text = text;
    return item;
}

@end
