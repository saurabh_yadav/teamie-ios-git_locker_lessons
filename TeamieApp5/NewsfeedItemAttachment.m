//
//  NewsfeedItemAttachment.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/27/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "NewsfeedItemAttachment.h"
#import "NewsfeedItem.h"


@implementation NewsfeedItemAttachment

@dynamic desc;
@dynamic href;
@dynamic thumbnail;
@dynamic title;
@dynamic type;
@dynamic previewURL;
@dynamic iconURL;
@dynamic totalVotes;
@dynamic choiceCount;
@dynamic newsfeedItem;
@dynamic pollOptions;

- (NSString*)description {
    NSString* line = @"\n----------------------\n";
    NSString* debugMsg = [NSString stringWithFormat:@"Attached image / link: %@\nAttached thumbnail: %@\nAttached Title: %@\nDescription: %@\nAttached Poll numOptions: %lu with %i votes\n%@", self.href, self.thumbnail, self.title, self.desc, (unsigned long)[self.pollOptions count], [self.totalVotes intValue], line];
    
    return debugMsg;
}

@end
