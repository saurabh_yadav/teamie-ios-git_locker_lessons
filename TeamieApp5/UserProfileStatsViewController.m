//
//  UserProfileStatsViewController.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 8/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UserProfileStatsViewController.h"
#import "UserProfileStatsTableViewCell.h"
@interface UserProfileStatsViewController ()
@property (nonatomic) NSDictionary *userStats;
@property (nonatomic) NSMutableArray *statsData;
@end

@implementation UserProfileStatsViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithUserStats:(NSDictionary *)userStats
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"UserProfile" bundle:nil];
    self = (UserProfileStatsViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"UserProfileStatsViewController"];
    
    if (self) {
        self.userStats = userStats;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.statsData = [[NSMutableArray alloc]init];
    if (self.userStats) {
        for (id statKey in self.userStats) {
            NSDictionary* statInfo = [self.userStats valueForKey:statKey];
            
            if ([statInfo valueForKey:@"statistics"] && [[statInfo objectForKey:@"statistics"] isKindOfClass:[NSArray class]]) {
                for (NSString* statItemString in [statInfo objectForKey:@"statistics"]) {
                    NSNumber *statNo = [NSNumber numberWithInteger:[TeamieGlobals extractNumberFromString:statItemString]];
                    NSString* statLabel = [[[statItemString componentsSeparatedByString:@" "] subarrayWithRange:NSMakeRange(1, 2)] componentsJoinedByString:@" "];
                    NSDictionary *statsDic = @{@"number": statNo,
                                               @"label":statLabel,
                                               @"image":[self getIconForStatLabel:statLabel],
                                               };
                    [self.statsData addObject:statsDic];
                }
            }
        }
        [self.tableView reloadData];
        // Set content size of the profile tab content
    }
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.statsData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"UserProfileStatsTableViewCell";
    UserProfileStatsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (!cell) {
        cell = [[UserProfileStatsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.statsTitle = (UILabel *)[cell viewWithTag:102];
        cell.iconImage = (UIImageView *)[cell viewWithTag:101];
        cell.statsValue = (UILabel *)[cell viewWithTag:100];
        
    }
    NSDictionary *statsDic = [self.statsData objectAtIndex:indexPath.row];
    NSNumber *statNo = [statsDic objectForKey:@"number"];
    UIImage *iconImage = [statsDic objectForKey:@"image"];
    NSString *labelString = [statsDic objectForKey:@"label"];
    
    cell.statsTitle.text = labelString;
    cell.statsValue.text =  [NSString stringWithFormat:@"%d",[statNo intValue]];
    cell.iconImage.image = iconImage;
    
    return cell;
}

- (UIImage*)getIconForStatLabel:(NSString *)labelText {
    
    UIImage* sprite = [UIImage imageNamed:@"icons-profile-stats.png"];
    CGFloat leftLoc = 75.0;
    NSArray* stringsToLookFor = [[NSArray alloc] initWithObjects:@"thought", @"question", @"right", @"lesson", @"quiz", @"user", nil];
    
    NSInteger counter = 0;
    for (NSString* subString in stringsToLookFor) {
        if ([labelText rangeOfString:subString options:NSCaseInsensitiveSearch].location != NSNotFound) {
            switch (counter) {
                case 0:
                    leftLoc = 0.0;
                    break;
                case 1:
                    leftLoc = 175.0;
                    break;
                case 2:
                    leftLoc = 25.0;
                    break;
                case 3:
                    leftLoc = 50.0;
                    break;
                case 4:
                    leftLoc = 100.0;
                    break;
                case 5:
                    leftLoc = 200.0;
                    break;
                default:
                    leftLoc = 75.0;
                    break;
            }
            break;
        }
        counter++;
    }
    
    // Adapt the masking location to the scaling factor of the image being used
    leftLoc *= [sprite scale];
    
    // Step 2: Take a slice of the sprite (24 pixel square on the top left of the sprite)
    CGImageRef cgIcon = CGImageCreateWithImageInRect(sprite.CGImage, CGRectMake(leftLoc, 0.0, 25.0 * [sprite scale], 25.0 * [sprite scale]));
    
    // Step 3: Convert the icon to an UIImage and release the CGImage version
    UIImage *icon = [UIImage imageWithCGImage:cgIcon];
    
    return icon;
}
@end
