//
//  QuestionFileViewController.h
//  TeamieApp5
//
//  Created by Raunak on 30/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol QuestionFileControllerDelegate
- (void) questionFileControllerDidSelectOptionWithData:(NSDictionary*)option;
@end


@interface QuestionFileViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id<QuestionFileControllerDelegate> delegate;
@property (nonatomic, strong) NSArray* fileAttachments;
- (id)initWithFileAttachments:(NSArray*)fileAttachments;

@end
