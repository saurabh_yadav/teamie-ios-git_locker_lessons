//
//  UserMenu.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class URLParam, UserLoginData;

@interface UserMenu : NSManagedObject

@property (nonatomic, retain) NSString * category;
@property (nonatomic, retain) NSString * href;
@property (nonatomic, retain) NSString * method;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSSet *attributes;
@property (nonatomic, retain) UserLoginData *userObject;
@end

@interface UserMenu (CoreDataGeneratedAccessors)

- (void)addAttributesObject:(URLParam *)value;
- (void)removeAttributesObject:(URLParam *)value;
- (void)addAttributes:(NSSet *)values;
- (void)removeAttributes:(NSSet *)values;

@end
