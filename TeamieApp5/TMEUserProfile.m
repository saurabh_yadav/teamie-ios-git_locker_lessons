//
//  TMEUserProfile.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEUserProfile.h"
#import "TMEBadge.h"

@implementation TMEUserProfile

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"userStats": @"user-stats",
        @"otherInfo": @"other-info",
    };
}

+ (NSValueTransformer *)userJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEUser.class];
}

+ (NSValueTransformer *)badgesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEBadge.class];
}

@end
