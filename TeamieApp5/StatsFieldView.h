//
//  StatsFieldView.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 20/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface StatsFieldView : UIView
@property (weak, nonatomic) IBOutlet UILabel *readersLabel;
@property (weak, nonatomic) IBOutlet UIImageView *topOneImage;
@property (weak, nonatomic) IBOutlet UIImageView *topTwoImage;
@property (weak, nonatomic) IBOutlet UIImageView *topThreeImage;

@end
