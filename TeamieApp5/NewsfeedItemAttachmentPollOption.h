//
//  NewsfeedItemAttachmentPollOption.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/28/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class NewsfeedItemAttachment;

@interface NewsfeedItemAttachmentPollOption : NSManagedObject

@property (nonatomic, retain) NSNumber * currentUserVote;
@property (nonatomic, retain) NSNumber * numVotes;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * choiceKey;
@property (nonatomic, retain) NewsfeedItemAttachment *newsfeedItemAttachment;

- (NSNumber*)getFraction;

@end
