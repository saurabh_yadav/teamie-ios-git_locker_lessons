//
//  UserActions.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 11/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserActions : NSObject
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *icon;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *url;
@property (nonatomic, retain) NSString *type;

@end
