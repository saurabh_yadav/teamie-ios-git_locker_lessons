//
//  ThoughtImageViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/16/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "ThoughtImageViewController.h"

@implementation ThoughtImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    
    [super viewWillAppear:animated];
    
    // set the tint for the navigation bar
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHex:@"#BE202E" alpha:1.0]];
}

- (void)loadImageWithUrl:(NSString*)url WithCaption:(NSString*)caption WithTitle:(NSString*)title {
    
    /*self.photoSource = [[MockPhotoSource alloc]
                         initWithType:MockPhotoSourceNormal 
                         title:title photos:[NSArray arrayWithObject:[[MockPhoto alloc]
                                                              initWithURL:url 
                                                              smallURL:url 
                                                              size:CGSizeMake(0, 0)
                                                              caption:caption]] photos2:nil];*/
    
    [TeamieGlobals addBackButtonToViewController:self withCallback:@selector(dismissPhotoView:)];
}

- (void)dismissPhotoView:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
    
}

// Overriding TTPhotoViewController's method in order to hide the bottom toolbar
- (void)loadView {
    CGRect screenFrame = [UIScreen mainScreen].bounds;
    self.view = [[UIView alloc] initWithFrame:screenFrame];
    
    /*CGRect innerFrame = CGRectMake(0, 0,
                                   screenFrame.size.width, screenFrame.size.height);
    _innerView = [[UIView alloc] initWithFrame:innerFrame];
    _innerView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_innerView];
    
    _scrollView = [[TTScrollView alloc] initWithFrame:screenFrame];
    _scrollView.delegate = self;
    _scrollView.dataSource = self;
    _scrollView.rotateEnabled = NO;
    _scrollView.backgroundColor = [UIColor blackColor];
    _scrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight;
    [_innerView addSubview:_scrollView];
    
    _nextButton =
    [[UIBarButtonItem alloc] initWithImage:TTIMAGE(@"bundle://Three20.bundle/images/nextIcon.png")
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(nextAction)];
    _previousButton =
    [[UIBarButtonItem alloc] initWithImage:
     TTIMAGE(@"bundle://Three20.bundle/images/previousIcon.png")
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(previousAction)];
    
    UIBarButtonItem* playButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemPlay
                                                   target:self
                                                   action:@selector(playAction)];
    playButton.tag = 1;
    
    UIBarItem* space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                         UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    _toolbar = [[UIToolbar alloc] initWithFrame:
                CGRectMake(0, screenFrame.size.height - TT_ROW_HEIGHT,
                           screenFrame.size.width, TT_ROW_HEIGHT)];
    if (self.navigationBarStyle == UIBarStyleDefault) {
        _toolbar.tintColor = TTSTYLEVAR(toolbarTintColor);
    }
    
    _toolbar.barStyle = self.navigationBarStyle;
    _toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin;
    _toolbar.items = [NSArray arrayWithObjects:
                      space, _previousButton, space, _nextButton, space, nil];

    // Uncomment the line below to enable the toolbar for the photo viewer
    // [_innerView addSubview:_toolbar];*/
}

@end
