//
//  LessonPage.m
//  TeamieApp5
//
//  Created by Raunak on 23/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "TMELessonPage.h"
#import "TMELessonAction.h"
#import "TMEFileAttachment.h"
#import "TMELinkAttachment.h"
#import "TMEVideoAttachment.h"
#import "TMEResourceAttachment.h"
#import "TMEInlineQuizAttachment.h"

@implementation TMELessonPage

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"files": @"attachments.files",
        @"links": @"attachments.links",
        @"videos": @"attachments.videos",
        @"inline_quizzes": @"attachments.inline_quizzes",
        @"resources": @"attachments.resources",
    };
}

+ (NSValueTransformer *)authorJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEUser.class];
}

+ (NSValueTransformer *)actionsJSONTransformer {
    return [TMEModel listJSONTransformer:TMELessonAction.class];
}

+ (NSValueTransformer *)filesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEFileAttachment.class];
}

+ (NSValueTransformer *)linksJSONTransformer {
    return [TMEModel listJSONTransformer:TMELinkAttachment.class];
}

+ (NSValueTransformer *)videosJSONTransformer {
    return [TMEModel listJSONTransformer:TMEVideoAttachment.class];
}

+ (NSValueTransformer *)inline_quizzesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEInlineQuizAttachment.class];
}

+ (NSValueTransformer *)resourcesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEResourceAttachment.class];
}

@end
