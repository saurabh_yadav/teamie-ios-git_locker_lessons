//
//  ThoughtCommentReplyView.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 19/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "ThoughtCommentReplyView.h"
#import "UIView+AutoLayout.h"
#import "TeamieUIGlobalColors.h"
#import "ThoughtImageAttachmentView.h"
#import "ThoughtDefaultAttachmentView.h"
#import "TMEPostAttachment.h"

static const UIFont* kReplyFont;
static const UIFont* kReplyFontBold;
static const UIColor* kReplyTextColor;

static const NSString* kNibName = @"ThoughtCommentReplyView";

@interface ThoughtCommentReplyView() <UITextViewDelegate, ThoughtAttachmentViewDelegate>

@property (nonatomic, strong) TMEPostCommentReply* reply;

@property (nonatomic, strong) NSArray* imageAttachments;
@property (nonatomic, strong) NSArray* nonImageAttachments;

@property (nonatomic, strong) ThoughtAttachmentView* bottomAttachmentView;
@property (nonatomic, strong) NSLayoutConstraint* bottomAttachmentViewBottomConstraint;

@property (nonatomic) int prevMessageBodyWidth;

@end

@implementation ThoughtCommentReplyView

+ (void)initialize{
    UIFont* bodyFont = [TeamieGlobals appFontFor:@"thoughtBody"];
    UIFont* bodyFontBold = [TeamieGlobals appFontFor:@"thoughtBodyBold"];
    
    kReplyFont = [UIFont fontWithName:bodyFont.fontName size:bodyFont.pointSize * 0.9 * 0.9];
    kReplyFontBold = [UIFont fontWithName:bodyFontBold.fontName size:bodyFontBold.pointSize * 0.9 * 0.9];
    kReplyTextColor = [TeamieUIGlobalColors thoughtReplyTextColor];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithReply:(TMEPostCommentReply *)reply delegate:(id<ThoughtCommentReplyViewDelegate>)delegate{
    self = [[[NSBundle mainBundle] loadNibNamed:[kNibName copy] owner:nil options:nil] lastObject];
    if (self){
        self.delegate = delegate;
        
        [self setupLayout];
        [self loadReply:reply];
    }
    return self;
}

#pragma mark - Overrided methods
- (void)layoutSubviews{
    if (self.replyMessageBody.frame.size.width != self.prevMessageBodyWidth){
        self.prevMessageBodyWidth = self.replyMessageBody.frame.size.width;
    }
    
    [super layoutSubviews];
    
    if ([self.delegate respondsToSelector:@selector(replyViewDidLayoutSubviews:)]){
        [self.delegate performSelector:@selector(replyViewDidLayoutSubviews:) withObject:self];
    }
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor{
    [super setBackgroundColor:backgroundColor];
    
    self.replyMessageBody.backgroundColor = backgroundColor;
}

#pragma mark - Display reply
- (void)setupLayout{
    self.leftArrow.image = [TeamieUIGlobals defaultPicForPurpose:@"angle-right"
                                                  withSize:kReplyFont.pointSize * 1.2
                                                  andColor:[kReplyTextColor copy]];
    self.replyAuthorName.font = [kReplyFontBold copy];
    self.replyAuthorName.textColor = [kReplyTextColor copy];
    
    self.replyMeta.font = [kReplyFont copy];
    self.replyMeta.textColor = [kReplyTextColor copy];
    
    self.replyMessageBody.font = [kReplyFont copy];
    self.replyMessageBody.textColor = [kReplyTextColor copy];
    self.replyMessageBody.delegate = self;
    
    self.prevMessageBodyWidth = self.replyMessageBody.frame.size.width;
}

- (void)loadReply:(TMEPostCommentReply*)reply{
    self.reply = reply;
    
    [self loadReplyContent];
    
    if ([reply.attachments count] > 0){
        [self showAttachments];
    }
}

- (void)loadReplyContent{
    [self createAuthorImgView];
    
    self.replyAuthorName.text = self.reply.author.realName;
    self.replyMeta.text = self.reply.timestampText;
    
    if (self.reply.htmlMessage){
        self.replyMessageBody.attributedText = [self createMessageBodyText];
    }
    else{
        self.replyMessageBody.text = self.reply.message;
    }
    
}

- (void)createAuthorImgView{
    UIImage* img = nil;
    NSURL* imgURL = self.reply.author.user_profile_image.path;
    
    if (imgURL == nil){
        img = [TeamieUIGlobals defaultPicForPurpose:@"profile"
                                           withSize:2 * kReplyFont.pointSize
                                           andColor:[UIColor colorWithHex:@"#6F6F6F" alpha:1.0]];
        [self.replyAuthorImg setContentMode:UIViewContentModeCenter];
    }
    else{
        img = [UIImage imageWithData:[NSData dataWithContentsOfURL:imgURL]];
        [self.replyAuthorImg setContentMode:UIViewContentModeScaleAspectFill];
    }
    
    [self.replyAuthorImg setImage:img];
}

- (NSAttributedString*)createMessageBodyText{
    NSData* htmlData = [self.reply.htmlMessage dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* parseOptions = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                   NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)};
    NSDictionary* attributes = @{NSFontAttributeName:self.replyMessageBody.font,
                                 NSForegroundColorAttributeName:self.replyMessageBody.textColor,
                                 NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone)};
    
    NSMutableAttributedString* attText = [[NSMutableAttributedString alloc] initWithData:htmlData
                                                                                 options:parseOptions
                                                                      documentAttributes:nil
                                                                                   error:nil];
    [attText addAttributes:attributes range:NSMakeRange(0, [attText length])];
    
    return attText;
}

#pragma mark - Comment Attachments
- (void)showAttachments{
    if ([self.reply.attachments count] == 0){
        return;
    }
    
    self.imageAttachments = [self filterImageAttachments];
    NSMutableArray* nonImageAttachments = [NSMutableArray arrayWithArray:self.reply.attachments];
    [nonImageAttachments removeObjectsInArray:self.imageAttachments];
    self.nonImageAttachments = [nonImageAttachments copy];
    
    NSMutableArray* attachmentViews = [[NSMutableArray alloc]init];
    if ([self.imageAttachments count] > 0){
        [attachmentViews addObject:[self createImageAttachmentView]];
    }
    
    if ([self.nonImageAttachments count] > 0){
        [attachmentViews addObjectsFromArray:[self createNonImageAttachmentViews]];
    }
    
    [self layoutAttachmentViews:attachmentViews];
}

- (UIView*)createImageAttachmentView{
    ThoughtAttachmentView* imageAttachmentView = [[ThoughtImageAttachmentView alloc] initWithImageAttachments:self.imageAttachments delegate:self];
    
    self.bottomAttachmentView = imageAttachmentView;
    [self addSubview:imageAttachmentView];
    
    return imageAttachmentView;
}

- (NSArray*)createNonImageAttachmentViews{
    int attachmentCount = [self.nonImageAttachments count];
    NSMutableArray* viewArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < attachmentCount; i++){
        TMEPostAttachment* attachment = [self.nonImageAttachments objectAtIndex:i];
        
        ThoughtDefaultAttachmentView* attachmentView = [[ThoughtDefaultAttachmentView alloc] initWithAttachment:attachment];
        [self addSubview:attachmentView];
        
        // UI Style
        attachmentView.backgroundColor = [UIColor colorWithHex:@"#F4F5F6" alpha:1.0];
        attachmentView.layer.borderColor = [[UIColor colorWithHex:@"#EEEEEE" alpha:1.0] CGColor];
        attachmentView.layer.borderWidth = 1.0;
        attachmentView.layer.cornerRadius = 3.0;
        
        // Tap gesture
        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nonImageAttachmentDidTapped:)];
        attachmentView.tag = i;
        
        [attachmentView addGestureRecognizer:gesture];
        
        [viewArray addObject:attachmentView];
    }
    
    if ([viewArray count] > 0){
        self.bottomAttachmentView = [viewArray lastObject];
    }
    
    return viewArray;
}

- (void)layoutAttachmentViews:(NSArray*)views{
    [self removeConstraint:self.replyMessageBodyBottomConstraint];
    
    NSArray* constraints = [self verticallyPinSubviews:views
                                             underTopView:self.replyMessageBody
                                               bottomView:nil
                                                topMargin:0.0
                                             bottomMargin:8.0
                                          leftRightMargin:25.0
                                                vDistance:0.0];
    
    self.bottomAttachmentViewBottomConstraint = [constraints lastObject];
}

#pragma mark - UITextview delegate
//- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
//    [TeamieGlobals openInappBrowserWithURL:URL
//                        fromViewController:(UIViewController*)self.delegate];
//    return NO;
//}

#pragma mark - ThoughtAttachmentView delegate
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if ([self.delegate respondsToSelector:@selector(pushViewController:animated:)]){
        [self.delegate performSelector:@selector(pushViewController:animated:)
                            withObject:viewController
                            withObject:[NSNumber numberWithBool:animated]];
    }
}

#pragma mark - Target Action methods
- (void)nonImageAttachmentDidTapped:(UITapGestureRecognizer*)sender{
    int index = sender.view.tag;
    
    TMEPostAttachment* attachment = [self.nonImageAttachments objectAtIndex:index];
    if ([attachment respondsToSelector:@selector(href)]) {
        NSNumber *attachmentType;
//        if ([attachment.type isEqualToString:@"link"]) {
//            attachmentType = [NSNumber numberWithInt:kGoogleAnalyticsLinkReferenceValue];
//        }
//        else if ([attachment.type isEqualToString:@"video"]) {
//            attachmentType = [NSNumber numberWithInt:kGoogleAnalyticsVideoReferenceValue];
//        }
        [ARAnalytics event:@"tap_attachment" withProperties:@{@"category": @"full_post",
                                                              @"label": @"reply",
                                                              @"value": attachmentType}];
        
//        [TeamieGlobals openInappBrowserWithURL:[NSURL URLWithString:[attachment performSelector:@selector(href)]]
//                            fromViewController:(UIViewController*)self.delegate];
    }
}

#pragma mark - Other helper methods
- (NSArray*)filterImageAttachments{
    NSPredicate* predicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if (![evaluatedObject isKindOfClass:[TMEPostAttachment class]]){
            return NO;
        }
        
//        TMEPostAttachment* attachment = (TMEPostAttachment*)evaluatedObject;
//        if ([attachment.type isEqualToString:@"image"]){
//            return YES;
//        }
        
        return NO;
    }];
    
    return [self.reply.attachments filteredArrayUsingPredicate:predicate];
}

@end
