//
//  CalendarManager.h
//  TeamieApp5
//
//  Created by Raunak on 15/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <EventKit/EventKit.h>

@interface CalendarManager : NSObject

+(CalendarManager*)defaultCalendar;
- (BOOL)addDeadlineWithID:(NSNumber*)eventID title:(NSString*)title date:(NSDate*)date description:(NSString*)description;
- (BOOL)removeDeadlineWithID:(NSNumber*)eventID;

@end
