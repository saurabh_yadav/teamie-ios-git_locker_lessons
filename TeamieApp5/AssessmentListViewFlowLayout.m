//
//  AssessmentListViewFlowLayout.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 28/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "AssessmentListViewFlowLayout.h"

CGFloat topMargin    = 10.0f;
CGFloat leftMargin   = 10.0f;
CGFloat bottomMargin = 10.0f;
CGFloat rightMargin  = 10.0f;

@implementation AssessmentListViewFlowLayout

- (id)init {
 
    self = [super init];
    if (self) {
        self.itemSize                = CGSizeMake(290, 240);
        self.sectionInset            = UIEdgeInsetsMake(topMargin, leftMargin, bottomMargin, rightMargin);
        self.minimumInteritemSpacing = 10.0f;
        self.minimumLineSpacing      = 15.0f;
    }
    
    return self;
}

@end
