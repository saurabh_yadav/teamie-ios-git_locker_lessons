//
//  TMEAnalyticsController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 06/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TMEAnalyticsCustomDimension) {
    TMEAnalyticsClientDimension = 1,
    TMEAnalyticsUserRoleDimension = 2
};

@interface TMEAnalyticsController : NSObject

extern NSString * const kAppDynamicsMobileEUMKey;

/**
 * Less verbose function for adding an event.
 */
extern void TMETrackEvent(NSString *category, NSString *action, NSString *label);

+ (void)startTrackingOnGoogleAnalytics;

/**
 * Helper for tracking screens
 */
+ (void)trackScreen:(NSString *)screenName;

/**
 * Helper for tracking dimensions
 */
+ (void)trackDimension:(TMEAnalyticsCustomDimension)dimension value:(NSString *)value;

@end
