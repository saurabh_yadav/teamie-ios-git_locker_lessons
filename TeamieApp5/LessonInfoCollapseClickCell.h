//
//  LessonInfoCollapseClickCell.h
//  LS2
//
//  Created by Wei Wenbo on 3/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import "CollapseClickCell.h"
#define LessonInfoHeaderHeight 49
#define CellDistance 10
#define HeaderContentPaddings 1
@interface LessonInfoCollapseClickCell : CollapseClickCell

//Header
@property (weak, nonatomic) IBOutlet UIView *TitleView;
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *TitleButton;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;

//body
@property (weak, nonatomic) IBOutlet UIView *ContentView;

@end
