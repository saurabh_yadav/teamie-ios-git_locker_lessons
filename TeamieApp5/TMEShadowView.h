//
//  TMEShadowView.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMEShadowView : UIView

- (instancetype)init;

@end
