//
//  Leaderboard.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 12/10/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "Leaderboard.h"

@implementation Leaderboard

@synthesize uid;
@synthesize realName;
@synthesize profileImage;
@synthesize userPoints;
@synthesize nid;

@end
