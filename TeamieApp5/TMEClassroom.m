//
//  TMEClassroom.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TMEClassroom.h"

@implementation TMEClassroom

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
         @"freshThoughts": @"fresh_thoughts",
         @"name" : @"name",
         
    };
}

+ (NSValueTransformer *)nidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

- (BOOL)hasPermission:(NSString *)permission {
    if (_permissions == nil)
        return NO;
    else {
        for (NSString *p in _permissions.allKeys) {
            if ([_permissions[p] isEqualToString:@"1"]) {
                return YES;
            }
        }
        return NO;
    }
}

@end
