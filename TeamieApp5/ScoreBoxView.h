//
//  ScoreBoxView.h
//  TeamieApp5
//
//  Created by Raunak on 18/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreBoxView : UIView

@property (nonatomic, strong) UILabel* userScoreLabel;
@property (nonatomic, strong) UILabel* maxScoreLabel;

- (id)initWithFrame:(CGRect)frame userScore:(NSString*)userScore maxScore:(NSString*)maxScore;
@end
