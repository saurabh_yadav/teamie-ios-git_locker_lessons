//
//  TeamieBaseClient.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEBaseClient.h"
#import "TMEJSONResponseSerializerWithData.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "TeamieRevealController.h"
#import <ReactiveCocoa/RACEXTScope.h>

@implementation TMEBaseClient

#ifdef DEBUG
NSString * const TEAMIE_DEFAULT_OAUTH_REST_URL = @"https://api-qa.theteamie.com/app";
#else
NSString * const TEAMIE_DEFAULT_OAUTH_REST_URL = @"https://api.theteamie.com/app";
#endif

NSString * const TEAMIE_DEFAULT_REST_URL = @"https://instant.theteamie.com/api";
NSString * const TEAMIE_DEFAULT_API_VERSION_NUMBER = @"3.1";
NSString * const TEAMIE_DEFAULT_CLIENT = @"ios";
NSString * const TEAMIE_OAUTH_SSO_GRANT_TYPE = @"teamie_sso_extension";
NSString * const TEAMIE_DEFAULT_OAUTH_GRANT_TYPE = @"password";
NSString * const TEAMIE_DEFAULT_OAUTH_CLIENT_ID = @"ios-app";
NSString * const TEAMIE_DEFAULT_CLIENT_SITE = @"instant.theteamie.com";
NSString * const TEAMIE_DOMAIN_LIST_URL = @"https://theteamie.com";
NSString * const TEAMIE_DOMAIN_LIST_KEY = @"7465616d69652d73697465732d617661696c61626c65";

typedef NS_ENUM(NSInteger, TMERequestMethod) {
    GETMethod,
    POSTMethod,
    DELETEMethod,
    PUTMethod,
};

//Note, using URLs with a leading slash "/whatever...", will trim the path in the base URL specified. Not using a leading slash would append to after the path in the base URL.
NSString * const RequestURL[] = {
    [TeamieUserDomainRequest] = @"/get_sites.php",
    [TeamieUserPublicSiteInfoRequest] = @"site/public-info.json",
    [TeamieUserLoginRequest] = @"/oauth/token",
    [TeamieUserLogoutRequest] = @"user/logout.json",
    [TeamieUserBulletinBoardCountRequest] = @"bulletin-board.json",
    [TeamieUserBulletinBoardRequest] = @"bulletin-board.json",
    [TeamieUserMessagesRequest] = @"messages.json",
    [TeamieUserMarkNotificationReadRequest] = @"bulletin-board/read.json",
    [TeamieNewUserProfileRequest] = @"profile/",
    [TeamieUserNewsfeedRequest] = @"newsfeed.json",
    [TeamieUserClassroomNewsfeedRequest] = @"thought.json",
    [TeamieUserClassroomsRequest] = @"classroom.json",
    [TeamieUserSiteInfoRequest] = @"site.json",
    [TeamieUserDeviceRegisterRequest] = @"push_notifications.json",
    [TeamieUserDeviceUnregisterRequest] = @"push_notifications/%@.json",
    [TeamieUserLikeThoughtRequest] = @"thought/like.json",
    [TeamieUserLikeThoughtCommentRequest] = @"thought-comment/like.json",
    [TeamieUserSameThoughtRequest] = @"thought/same.json",
    [TeamieUserDeleteThoughtRequest] = @"thought/%@.json",
    [TeamieUserGetThoughtRequest] = @"thought/%@.json",
    [TeamieUserPostThoughtCommentRequest] = @"thought-comment.json",
    [TeamieUserPostThoughtCommentReplyRequest] = @"thought-comment/reply.json",
    [TeamieUserThoughtCommentVoteRequest] = @"thought-comment/vote.json",
    [TeamieUserThoughtCommentMarkRightRequest] = @"thought-comment/mark-right.json",
    [TeamieUserGetAllThoughtCommentsRequest] = @"thought/%@/comments.json",
    [TeamieUserGetAllThoughtCommentRepliesRequest] = @"thought-comment/%@/replies.json",
    [TeamieUserReportPost] = @"thought/report.json",
    [TeamieUserReportPostComment] = @"thought-comment/report.json",
    [TeamieUserClassroomMenuRequest] = @"classroom/user-menu.json",
    [TeamieUserLessonsRequest] = @"lesson.json",
    [TeamieUserLessonToggleStatusRequest] = @"lesson/toggle_status.json",
    [TeamieUserLessonDeleteRequest] = @"lesson/%@.json",
    [TeamieUserLessonRequest] = @"lesson/%@.json",
    [TeamieUserLessonUpdateRequest] = @"lesson/%@.json",
    [TeamieUserLessonPageRequest] = @"lesson-page/%@.json",
    [TeamieUserClassroomGradebookRequest] = @"/classroom/grade_book.json",
    [TeamieUserMenuRequest] = @"user-menu.json",
    [TeamieUserPostShareRequest] = @"thought.json",
    [TeamieUserFileUploadRequest] = @"file.json",
    [TeamieUserCustomReportRequest] = @"user-menu.json",
    [TeamieReportActiveClassroomsRequest]  = @"%@.json",
    [TeamieReportActiveStudentsRequest]  = @"%@.json",
    [TeamieReportLessonContributorsRequest]  = @"%@.json",
    [TeamieReportQuizContributorsRequest]  = @"%@.json",
    [TeamieReportSiteUsageRequest]  = @"%@.json",
    [TeamieReportMostViewedLessonsRequest]  = @"%@.json",
    [TeamieReportUserPointStatsRequest]  = @"%@.json",
    [TeamieReportUserPointsHistoryRequest]  = @"%@.json",
    [TeamieUserLockerLessonRequest] = @"locker/1/lesson.json",
//    [TeamieUserMenuForReportsRequest] = @"user-menu.json",
    [TeamieUserSeenPostRequest] = @"thought/seen.json",
    [TeamieUserQuizzesRequest] = @"quiz.json",
    [TeamieUserCalendarRequest] = @"calendar.json",
    [TeamieUserUpcomingEventsRequest] = @"calendarx.json",
    [DeletePostCommentRequest] = @"thought-comment/%@.json",
    [TeamieUserSubmitPollRequest] = @"thought/poll.json",
};

TMERequestMethod const RequestMethod[] = {
    [TeamieUserDomainRequest] = POSTMethod,
    [TeamieUserPublicSiteInfoRequest] = POSTMethod,
    [TeamieUserLoginRequest] = POSTMethod,
    [TeamieUserLogoutRequest] = POSTMethod,
    [TeamieUserBulletinBoardCountRequest] = GETMethod,
    [TeamieUserBulletinBoardRequest] = GETMethod,
    [TeamieUserMessagesRequest] = GETMethod,
    [TeamieUserMarkNotificationReadRequest] = POSTMethod,
    [TeamieNewUserProfileRequest] = GETMethod,
    [TeamieUserNewsfeedRequest] = GETMethod,
    [TeamieUserClassroomNewsfeedRequest] = GETMethod,
    [TeamieUserClassroomsRequest] = GETMethod,
    [TeamieUserSiteInfoRequest] = GETMethod,
    [TeamieUserDeviceRegisterRequest] = POSTMethod,
    [TeamieUserDeviceUnregisterRequest] = DELETEMethod,
    [TeamieUserLikeThoughtRequest] = POSTMethod,
    [TeamieUserLikeThoughtCommentRequest] = POSTMethod,
    [TeamieUserSameThoughtRequest] = POSTMethod,
    [TeamieUserDeleteThoughtRequest] = DELETEMethod,
    [TeamieUserGetThoughtRequest] = GETMethod,
    [TeamieUserPostThoughtCommentRequest] = POSTMethod,
    [TeamieUserPostThoughtCommentReplyRequest] = POSTMethod,
    [TeamieUserThoughtCommentVoteRequest] = POSTMethod,
    [TeamieUserThoughtCommentMarkRightRequest] = POSTMethod,
    [TeamieUserGetAllThoughtCommentsRequest] = GETMethod,
    [TeamieUserGetAllThoughtCommentRepliesRequest] = GETMethod,
    [TeamieUserReportPost] = POSTMethod,
    [TeamieUserReportPostComment] = POSTMethod,
    [TeamieUserClassroomMenuRequest] = POSTMethod,
    [TeamieUserLessonsRequest] = GETMethod,
    [TeamieUserLessonToggleStatusRequest] = POSTMethod,
    [TeamieUserLessonDeleteRequest] = DELETEMethod,
    [TeamieUserLessonRequest] = GETMethod,
    [TeamieUserLessonUpdateRequest] = PUTMethod,
    [TeamieUserLessonPageRequest] = GETMethod,
    [TeamieUserClassroomGradebookRequest] = POSTMethod,
    [TeamieUserMenuRequest] = GETMethod,
    [TeamieUserCustomReportRequest] = GETMethod,
    [TeamieUserPostShareRequest] = POSTMethod,
    [TeamieUserFileUploadRequest] = POSTMethod,
    [TeamieReportActiveClassroomsRequest] = GETMethod,
    [TeamieReportActiveStudentsRequest] = GETMethod,
    [TeamieReportLessonContributorsRequest] = GETMethod,
    [TeamieReportQuizContributorsRequest] = GETMethod,
    [TeamieReportSiteUsageRequest] = GETMethod,
    [TeamieReportMostViewedLessonsRequest] = GETMethod,
    [TeamieReportUserPointStatsRequest] = GETMethod,
    [TeamieReportUserPointsHistoryRequest] = GETMethod,
    [TeamieUserLockerLessonRequest] = GETMethod,
    [TeamieUserSeenPostRequest] = POSTMethod,
//    [TeamieUserMenuForReportsRequest] = GETMethod,
    [TeamieUserQuizzesRequest] = GETMethod,
    [TeamieUserCalendarRequest] = GETMethod,
    [TeamieUserUpcomingEventsRequest] = GETMethod,
    [DeletePostCommentRequest] = DELETEMethod,
    [TeamieUserSubmitPollRequest] = POSTMethod,
};

#pragma mark Network

- (AFHTTPRequestOperation *)request:(TMERequest)request
                         parameters:(NSDictionary *)parameters
                     loadingMessage:(NSString *)message
                            success:(void (^)(id response))successBlock
                            failure:(void (^)(NSError *error))failureBlock {
    return [self request:request
              parameters:parameters
                 makeURL:nil
          loadingMessage:message
              completion:nil
                 success:successBlock
                 failure:failureBlock];
}

- (AFHTTPRequestOperation *)request:(TMERequest)request
                         parameters:(NSDictionary *)parameters
                            makeURL:(NSString *(^)(NSString *URL))URLBlock
                     loadingMessage:(NSString *)message
                            success:(void (^)(id response))successBlock
                            failure:(void (^)(NSError *error))failureBlock {
    return [self request:request
              parameters:parameters
                 makeURL:URLBlock
          loadingMessage:message
              completion:nil
                 success:successBlock
                 failure:failureBlock];
}

- (AFHTTPRequestOperation *)request:(TMERequest)request
                         parameters:(NSDictionary *)parameters
                     loadingMessage:(NSString *)message
                         completion:(void (^)())completionBlock
                            success:(void (^)(id response))successBlock
                            failure:(void (^)(NSError *error))failureBlock {
    return [self request:request
              parameters:parameters
                 makeURL:nil
          loadingMessage:message
              completion:completionBlock
                 success:successBlock
                 failure:failureBlock];
}

- (AFHTTPRequestOperation *)request:(TMERequest)request
                         parameters:(NSDictionary *)parameters
                            makeURL:(NSString *(^)(NSString *URL))URLBlock
                     loadingMessage:(NSString *)message
                         completion:(void (^)())completionBlock
                            success:(void (^)(id response))successBlock
                            failure:(void (^)(NSError *error))failureBlock {
    
    NSString *URL = URLBlock? URLBlock(RequestURL[request]) : RequestURL[request];
    MBProgressHUD *HUD;
    if (message) {
        UIWindow *topWindow = [[[UIApplication sharedApplication].windows sortedArrayUsingComparator:^NSComparisonResult(UIWindow *win1, UIWindow *win2) {
            return win1.windowLevel - win2.windowLevel;
        }] lastObject];
        HUD = [MBProgressHUD showHUDAddedTo:topWindow animated:YES];
        HUD.animationType = MBProgressHUDAnimationFade;
        HUD.mode = MBProgressHUDModeIndeterminate;
        HUD.labelText = message;
    }
    
    void (^success)(AFHTTPRequestOperation *, NSDictionary *) = ^void(AFHTTPRequestOperation *operation, NSDictionary *responseObject) {
        if (successBlock) {
            successBlock(responseObject);
        }
        if (completionBlock) {
            completionBlock();
        }
        [HUD hide:YES];
    };
    void (^failure)(AFHTTPRequestOperation *, NSError *) = ^void(AFHTTPRequestOperation *operation, NSError *error) {
        if (error.code == NSURLErrorCannotFindHost || error.code == NSURLErrorCannotConnectToHost || error.code == NSURLErrorNotConnectedToInternet || error.code == NSURLErrorNetworkConnectionLost) {
            [TeamieGlobals addTSMessageInController:nil title:TMELocalize(@"error.no-internet") message:TMELocalize(@"error.no-internet-message") type:@"error" duration:0 withCallback:nil];
        }
        else if (failureBlock) {
            failureBlock(error);
        }
        if (completionBlock) {
            completionBlock();
        }
        [HUD hide:YES];
    };
    
    switch (RequestMethod[request]) {
        case GETMethod: return [self GET:URL parameters:parameters success:success failure:failure];
        case POSTMethod: return [self POST:URL parameters:parameters success:success failure:failure];
        case DELETEMethod: return [self DELETE:URL parameters:parameters success:success failure:failure];
        case PUTMethod: return [self PUT:URL parameters:parameters success:success failure:failure];
    }
}

@end