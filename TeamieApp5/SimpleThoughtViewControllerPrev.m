//
//  SimpleThoughtViewController.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 5/15/14.
//  Copyright (c) 2014 ETH. All rights reserved.
//

#define kAlertViewWillDelete 1
#define kAlertViewDidDelete 2

#import "SimpleThoughtViewControllerPrev.h"
#import "SimplePostAttachment.h"
#import "SimplePostAttachmentPollOption.h"
#import "SimplePostComment.h"
#import "SimplePostAction.h"

#import "ADVPercentProgressBar.h"
#import "AudioToolbox/AudioServices.h"

#import "TeamieStylesheet.h"

@interface SimpleThoughtViewControllerPrev ()

@property (strong, nonatomic) SimplePost* thoughtData;
@property (strong, nonatomic) NSNumber* thoughtID;

@end

@implementation SimpleThoughtViewControllerPrev{
    NSMutableArray* _pollBars;
    NSMutableArray* _pollLabels;
    NSMutableArray* _pollCheckboxes;
    NSMutableArray* _pollKeys;
    
    UITapGestureRecognizer* tapRecognizer;
    
    CGFloat thoughtContentNowX;
    CGFloat thoughtContentNowY;
}

- (id)initWithThoughtID:(NSNumber *)thoughtId delegate:(id)thoughtDelegate{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[TeamieGlobals storyboardIdentifier] bundle:nil];
    
    self = [storyboard instantiateViewControllerWithIdentifier:@"SimpleThoughtViewControllerPrev"];
    self.navigationItem.title = TeamieLocalizedString(@"$thought[s]", nil);
    
    if (self) {
        // make request for loading thought with given thought ID
        [self loadThoughtWithTid:thoughtId reload:YES];
        self.thoughtViewDelegate = thoughtDelegate;
    }
    
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    _pollBars = [[NSMutableArray alloc] init];
    _pollCheckboxes = [[NSMutableArray alloc] init];
    _pollLabels = [[NSMutableArray alloc] init];
    _pollKeys = [[NSMutableArray alloc] init];
    
    // if the thought data has been loaded, then display it
    if (self.thoughtData) {
        
        [self thoughtDataLoadSuccessful];
        
    }
    
    // if the current view controller is being shown as a modal
    if (self.presentingViewController) {
        [TeamieGlobals addBackButtonToViewController:self withCallback:@selector(dismissThoughtView:)];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self becomeFirstResponder];
    [self performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.01];
    
    if (self.thoughtID) {
        [[GANTracker sharedTracker] trackPageview:[NSString stringWithFormat:@"/thought/%qi", [self.thoughtID longLongValue]] withError:nil];
    }
    else {
        [[GANTracker sharedTracker] trackPageview:@"/thought/" withError:nil];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // if the view is going to appear & the thought data hasn't been loaded yet.
    if (!self.thoughtData) {
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_THOUGHT", nil) margin:10.f yOffset:10.f];
    }
    [self.view setNeedsDisplay];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)viewDidUnload
{
    [self setThoughtAuthorName:nil];
    [self setThoughtMessageBody:nil];
    [self setThoughtView:nil];
    [self setThoughtData:nil];
    [self setThoughtID:nil];
    [self setThoughtViewDelegate:nil];
    
    [self setThoughtAuthorDisplayPic:nil];
        
    [self setCommentStreamView:nil];
    [self setCommentEditor:nil];
    [self setBottomToolbar:nil];
    [self setCommentPostButton:nil];
    [self setThoughtTypeIndicator:nil];
    [self setThoughtTimestamp:nil];
    [self setThoughtClassroomName:nil];
    [self setThoughtInfoBar:nil];
    [self setCommentStreamHeader:nil];
    
    [super viewDidUnload];
}

- (void)loadThoughtWithTid:(NSNumber *)tid reload:(BOOL)shouldReload{
    [self.thoughtClassroomName setHidden:YES];
    
    NSDictionary* params;
    
    if (shouldReload) {
        params = [[NSDictionary alloc] initWithObjectsAndKeys:tid, @"tid", @"true", @"reload", nil];
    }
    else {
        params = [[NSDictionary alloc] initWithObjectsAndKeys:tid, @"tid", nil];
    }
    
    self.thoughtID = tid;
    self.thoughtData = nil;
    
    [self.apiCaller performRestRequest:TeamieUserGetThoughtRequest withParams:params];
    
    // Show a loading text, every time a request to API is made
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_THOUGHT", nil) margin:10.f yOffset:10.f];
}

- (void)thoughtRefreshed {
    [self loadThoughtWithTid:self.thoughtData.tid reload:YES];
    [self.commentStreamView reloadInputViews];
    NSLog(@"Refreshed Thought");
}

#pragma mark - TeamieRESTApi delegate methods
- (void)requestSuccessful:(TeamieRESTRequest)requestName {
    if (requestName == TeamieUserDeleteThoughtRequest) {
        // Set the thoughtData to nil, only then the dismissThoughtView of NewsfeedViewController will be invoked with nil parameter
        self.thoughtData = nil;
        [self dismissThoughtView:self];
        // TODO: Refresh Newsfeed after dismissing ThoughtViewController
    }
}

-(void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response{
    
    [TeamieGlobals dismissActivityLabels];
    
    if (requestName == TeamieUserGetThoughtRequest) {
        
        if ([response isKindOfClass:[SimplePost class]]) {
            self.thoughtData = response;
        }
        else if ([response isKindOfClass:[NSArray class]] && [[response objectAtIndex:0] isKindOfClass:[SimplePost class]]) {
            
            self.thoughtData = [response objectAtIndex:0];
        }
        else {
#ifdef DEBUG
            NSLog(@"Error: The response for Get Thought request is not of type NewsfeedItem");
#endif
        }
        
        if ([self isViewLoaded]) {  // if the view has finished loading...
            [self thoughtDataLoadSuccessful];
        }
    }
    else if (requestName == TeamieUserPostThoughtCommentRequest) {
        
        // clear the comment box text
        self.commentEditor.text = @"";
        [self.commentEditor resignFirstResponder];
        
        // if the response is an array of objects and the first object is a user comment
        if ([response isKindOfClass:[NSArray class]] && [[response objectAtIndex:0] isKindOfClass:[SimplePostComment class]]) {
            
            // add that comment to self.thoughtData
            SimplePostComment* newComment = [response objectAtIndex:0];
            NSMutableArray* newCommentArray = [NSMutableArray arrayWithArray:self.thoughtData.comments];
            [newCommentArray addObject:newComment];
            
            self.thoughtData.comments = newCommentArray;
            
            // increment self.thoughtData's comments count
            self.thoughtData.commentsCount = [NSNumber numberWithInteger:[self.thoughtData.commentsCount integerValue] + 1];
        }
        
        // layout the comments and scroll to the latest user comment
        [self layoutThoughtCommentsInFrame:self.commentStreamView.frame scrollToLatest:YES];
    }
    else if (requestName == TeamieUserSameThoughtRequest || requestName == TeamieUserLikeThoughtRequest) {
        
        UIBarButtonItem* actionButton = [self.navigationItem.rightBarButtonItems lastObject];
        NSString* currentTitle = actionButton.title;
        
        NSArray* defaultMenus = [TeamieGlobals defaultTeamieThoughtActionMenus];
        NSInteger titleIndex = [defaultMenus indexOfObject:currentTitle];
        NSString* newTitle = nil;
        
        if (titleIndex == 0) {
            newTitle = [defaultMenus objectAtIndex:1];
        }
        else if (titleIndex == 1) {
            newTitle = [defaultMenus objectAtIndex:0];
        }
        else if (titleIndex == 2) {
            newTitle = [defaultMenus objectAtIndex:3];
        }
        else if (titleIndex == 3) {
            newTitle = [defaultMenus objectAtIndex:2];
        }
        // set the new title to the button
        [actionButton setTitle:newTitle];
    }
    else if (requestName == TeamieUserPollThoughtRequest) {
        if (![response isKindOfClass:[NSArray class]]) {
            NSLog(@"Error: The response for thought poll POST request is not an array. Instead it is of class %@", NSStringFromClass([response class]));
            return;
        }
        
        // get the attachment object
        SimplePostAttachment* attachment = [response objectAtIndex:0];
        // if the current thought has any poll attachments remove them
        for (SimplePostAttachment* currentAttachment in self.thoughtData.attachment) {
            if ([currentAttachment.type isEqualToString:@"poll"]) {
                [TeamieRestAPI deleteEntityObject:currentAttachment];
                break;
            }
        }
        
        // sort the choice in the proper order
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"choiceKey" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray* finalSortedEntries = [attachment.pollOptions sortedArrayUsingDescriptors:sortDescriptors];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        
        for (NSUInteger i = 0; i < finalSortedEntries.count; i++) {
            // Update the poll option values
            SimplePostAttachmentPollOption* option = (SimplePostAttachmentPollOption*)[finalSortedEntries objectAtIndex:i];
            CGFloat fraction = [option.numVotes doubleValue] / [attachment.totalVotes doubleValue];
            
#ifdef DEBUG
            NSLog(@"Poll fraction for option %i updated to: %g", i, fraction);
#endif
            
            [((UILabel*)[_pollLabels objectAtIndex:i]) setText:option.title];
            [[_pollBars objectAtIndex:i] setProgress:fraction];
            [[_pollCheckboxes objectAtIndex:i] setSelected:[option.currentUserVote boolValue]];
        }
        
        [UIView commitAnimations];
    }
    else if (requestName == TeamieUserThoughtCommentVoteRequest) {
        [self loadThoughtWithTid:self.thoughtData.tid reload:YES];
    }
    else if (requestName == TeamieUserThoughtCommentMarkRightRequest) {
        [self loadThoughtWithTid:self.thoughtData.tid reload:YES];
        
    }
}

- (void)thoughtDataLoadSuccessful{
    [self prepareToDisplayThought];
    [self styleUIProperties];
    [self showThoughtContent];
    [self setupNotifications];
}

- (void)prepareToDisplayThought{
    // Add the thought action button to the navigation bar
    [self addThoughtActionButton];
    
    // Unset all poll options, so that in case thought is reloaded they're empty
    for (ADVPercentProgressBar* bar in _pollBars) {
        [bar removeFromSuperview];
    }
    for (UIButton* checkbox in _pollCheckboxes) {
        [checkbox removeFromSuperview];
    }
    for (UILabel* label in _pollLabels) {
        [label removeFromSuperview];
    }
    
    [_pollBars removeAllObjects];
    [_pollCheckboxes removeAllObjects];
    [_pollKeys removeAllObjects];
    [_pollLabels removeAllObjects];
    
    self.navigationItem.title = [self.thoughtData.type capitalizedString];
}

- (void)styleUIProperties{
    self.thoughtAuthorName.font = [TeamieStylesheet userNameFont];
    [self.thoughtAuthorDisplayPic.layer setCornerRadius:UI_PICTURE_BORDER_RADIUS];
    self.thoughtAuthorDisplayPic.layer.masksToBounds = YES;

    self.thoughtMessageBody.font = [TeamieStylesheet thoughtFont];
    self.thoughtMessageBody.textColor = [TeamieStylesheet textColor];
//    self.thoughtMessageBody.highlightedTextColor = [TeamieStylesheet highlightedTextColor];
    self.thoughtMessageBody.backgroundColor = [TeamieStylesheet backgroundTextColor];
    self.thoughtMessageBody.contentMode = UIViewContentModeLeft;
    self.thoughtMessageBody.textAlignment = NSTextAlignmentLeft;

    self.thoughtClassroomName.titleLabel.font = [TeamieStylesheet buttonFont];
    [self.thoughtClassroomName setHidden:NO];

    self.thoughtTimestamp.font = [TeamieStylesheet thoughtTimestampFont];
    self.thoughtTimestamp.textColor = [TeamieStylesheet timestampTextColor];
    self.thoughtTimestamp.highlightedTextColor = [UIColor whiteColor];
}

- (void)showThoughtContent{
    SimpleUser* author = self.thoughtData.author;
    
    NSURL* authorPicURL = [NSURL URLWithString:author.thumbnailImageURL];
    self.thoughtAuthorDisplayPic.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:authorPicURL]];
    
    self.thoughtAuthorName.text = author.realname;
    
    [self.thoughtTypeIndicator setImage:[UIImage imageNamed:([self.thoughtData.type isEqualToString:@"thought"] ? @"icon-thought-type1.png": @"icon-thought-type2.png")]];
    
    self.thoughtTimestamp.text = self.thoughtData.timestampText;
    
    [self.thoughtClassroomName setTitle:self.thoughtData.classroom.name
                               forState:UIControlStateNormal];
    
    self.thoughtMessageBody.text = self.thoughtData.msgText;
    self.thoughtMessageBody.editable = NO;
    self.thoughtMessageBody.dataDetectorTypes = UIDataDetectorTypeAll;
    [self sizeToFitAndDisableScroll:self.thoughtMessageBody];
    
    thoughtContentNowY = self.thoughtMessageBody.frame.origin.y + self.thoughtMessageBody.frame.size.height + UI_SMALL_MARGIN;
    thoughtContentNowX = self.thoughtInfoBar.frame.origin.x;
    
    [self showPoll];
    
    // Move the thought type indicator, label & button according to the nowY value
    CGRect tempFrame = self.thoughtInfoBar.frame;
    tempFrame.origin.y = thoughtContentNowY;
    self.thoughtInfoBar.frame = tempFrame;
    
    thoughtContentNowY += self.thoughtInfoBar.frame.size.height + UI_SMALL_MARGIN;
    
    // move the comment stream header just below the thoughtInfo bar
    tempFrame = self.commentStreamHeader.frame;
    tempFrame.origin.y = thoughtContentNowY;
    self.commentStreamHeader.frame = tempFrame;
    
    thoughtContentNowY += 15.0;
    
    // layout the comments of the thought
    CGRect commentFrame = CGRectMake(0.0, thoughtContentNowY, self.view.frame.size.width, self.thoughtView.bounds.size.height - thoughtContentNowY);
    [self layoutThoughtCommentsInFrame:commentFrame
                        scrollToLatest:NO];
    
    thoughtContentNowY += self.commentStreamView.frame.size.height + UI_SMALL_MARGIN;
    
    // set the UITextField delegate
    [self.commentEditor setDelegate:self];
}

- (void)showPoll{
    CGFloat totalWidth = self.thoughtView.frame.size.width;
    
    for (SimplePostAttachment* item in self.thoughtData.attachment) {
        if ([item.type isEqualToString:@"poll"]) {
            // sort the poll options according to choice_1, choice_2 etc.
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"choiceKey" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray* finalSortedEntries = [item.pollOptions sortedArrayUsingDescriptors:sortDescriptors];
            
            for (SimplePostAttachmentPollOption* pollOption in finalSortedEntries) {
                
                CGFloat tempX = thoughtContentNowX;
                CGFloat tempY = thoughtContentNowY;
                tempX += 30.0 + UI_SMALL_MARGIN;
                
                UILabel* pLabel = [[UILabel alloc] initWithFrame:CGRectMake(tempX, tempY, totalWidth - tempX, 15.0)];
                [pLabel setFont:[TeamieStylesheet thoughtPollFont]];
                [pLabel setTextColor:[TeamieStylesheet textColor]];
                [pLabel setText:pollOption.title];
                [pLabel setNumberOfLines:0];
                [pLabel sizeToFit];
                tempY += pLabel.frame.size.height;
                
                ADVPercentProgressBar* pBar = [[ADVPercentProgressBar alloc] initWithFrame:CGRectMake(tempX, tempY, [TeamieStylesheet kTableNewsfeedCellPollOptionWidth], [TeamieStylesheet kTableNewsfeedCellPollOptionHeight]) andProgressBarColor:ADVProgressBarBlack];
                CGFloat fraction = [pollOption.numVotes doubleValue] / [item.totalVotes doubleValue];
                [pBar setProgress:fraction];
                
                tempY += [TeamieStylesheet kTableNewsfeedCellPollOptionHeight] + UI_SMALL_MARGIN;
                
                UIButton* pButton = [TeamieGlobals createCheckboxButton:CGRectMake(thoughtContentNowX, thoughtContentNowY + (pLabel.frame.size.height + [TeamieStylesheet kTableNewsfeedCellPollOptionHeight] - 30.0) / 2.0, 30.0, 30.0)];
                [pButton addTarget:self action:@selector(thoughtPollOptionSelected:) forControlEvents:UIControlEventTouchUpInside];
                [pButton setSelected:[pollOption.currentUserVote boolValue]];
                
                thoughtContentNowY = tempY;
                
                [_pollBars addObject:pBar];
                [_pollLabels addObject:pLabel];
                [_pollCheckboxes addObject:pButton];
                [_pollKeys addObject:pollOption.choiceKey];
                
                [self.thoughtView addSubview:pLabel];
                [self.thoughtView addSubview:pBar];
                [self.thoughtView addSubview:pButton];
            }
        }
    }
}

- (void)layoutThoughtCommentsInFrame:(CGRect)viewFrame scrollToLatest:(BOOL)scrollAnimate {
    CGRect tempFrame;
    CGRect latestCommentFrame = CGRectZero;      // variable to store the frame of the latest comment
    
    // empty self.commentStreamView of all subviews before adding the comments
    NSArray* commentViews = [[self.commentStreamView subviews] copy];
    for (id commentItem in commentViews) {
        [commentItem removeFromSuperview];
    }
    
    // for each comment add the commentViews
    if (self.thoughtData.comments.count > 0) {
        
        // sort the user comments in ascending time order
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray* sortedResponse = [self.thoughtData.comments sortedArrayUsingDescriptors:sortDescriptors];
        
        CGFloat commentY = UI_SMALL_MARGIN;
        
        for (SimplePostComment* comment in sortedResponse) {
            UIView* commentView = [self createCommentView:comment thoughtType:self.thoughtData.type];
            tempFrame = commentView.frame;
            tempFrame.origin.x = UI_SMALL_MARGIN;
            tempFrame.origin.y = commentY;
            commentView.frame = tempFrame;
            [self.commentStreamView addSubview:commentView];   // add the comment to the screen view
            
            commentY += tempFrame.size.height + UI_SMALL_MARGIN;
            
            if (scrollAnimate) {
                latestCommentFrame = [commentView convertRect:commentView.bounds toView:self.thoughtView];
            }
        }
        
        // if the height of the comments view is more than the height provided
        if (commentY > viewFrame.size.height) {
            viewFrame.size.height = commentY;
        }
        
        self.commentStreamView.frame = viewFrame;
    }
    else {
        // To-do: Something when there are no comments
        if (viewFrame.size.height < self.commentStreamView.frame.size.height) {
            viewFrame.size.height = self.commentStreamView.frame.size.height;
        }
        self.commentStreamView.frame = viewFrame;
    }
    
    CGFloat thoughtViewHeight = self.commentStreamView.frame.size.height + self.commentStreamView.frame.origin.y + UI_SMALL_MARGIN;
    
    // adjust the contentsize of the scrollview according to new layout
    // Assuming that there is nothing that comes after commentStreamView
    self.thoughtView.contentSize = CGSizeMake(self.view.frame.size.width, thoughtViewHeight);
    
    // scroll to the latest comment
    if (scrollAnimate) {
        [self.thoughtView scrollRectToVisible:latestCommentFrame animated:YES];
    }
}

- (UIView*)createCommentView:(SimplePostComment*)comment thoughtType:(NSString *)thoughtType{
    CGFloat nowX = UI_SMALL_MARGIN;
    CGFloat nowY = UI_SMALL_MARGIN;
    CGFloat totalWidth = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?758.0:310.0;
    
    UIView* commentView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, totalWidth, 30.0)];
    
    // create the display pic
    UIImageView* commentAuthor = [[UIImageView alloc] initWithFrame:CGRectMake(nowX, nowY, 35.0, 35.0)];
    NSURL* authorPicURL = [NSURL URLWithString:comment.author.thumbnailImageURL];
    commentAuthor.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:authorPicURL]];
    commentAuthor.layer.cornerRadius = UI_PICTURE_BORDER_RADIUS;
    commentAuthor.layer.masksToBounds = YES;
    
    nowX = commentAuthor.frame.origin.x + commentAuthor.frame.size.width + UI_SMALL_MARGIN;
    
    UILabel* commentAuthorName = [[UILabel alloc] initWithFrame:CGRectMake(nowX, nowY, 200.0, 15.0)];
    commentAuthorName.text = comment.author.realname;
    [commentAuthorName setTextColor:[TeamieStylesheet textColor]];
    commentAuthorName.font = [TeamieStylesheet thoughtCommentAuthorNameFont];
    [commentAuthorName setBackgroundColor:[TeamieStylesheet backgroundTextColor]];
    
    nowX = totalWidth - UI_SMALL_MARGIN - 100.0;
    
    UILabel* commentTimestamp = [[UILabel alloc] initWithFrame:CGRectMake(nowX, nowY, 100.0, 15.0)];
    commentTimestamp.text = [TeamieGlobals timeIntervalWithStartTime:[comment.created doubleValue] withEndTime:0];
    commentTimestamp.textAlignment = NSTextAlignmentRight;
    [commentTimestamp setTextColor:[TeamieStylesheet timestampTextColor]];
    [commentTimestamp setFont:[TeamieStylesheet thoughtCommentTimestampFont]];
    [commentTimestamp setBackgroundColor:[UIColor clearColor]];
    
    nowX = commentAuthor.frame.origin.x + commentAuthor.frame.size.width + UI_SMALL_MARGIN;
    nowY = commentAuthorName.frame.size.height + UI_SMALL_MARGIN;
    
    // create the comment body label
    UITextView* commentBody = [[UITextView alloc] initWithFrame:CGRectMake(nowX, nowY, totalWidth - nowX - UI_SMALL_MARGIN, 10.0)];
    commentBody.font = [TeamieStylesheet thoughtCommentFont];
    commentBody.textColor = [TeamieStylesheet textColor];
    commentBody.backgroundColor = [TeamieStylesheet backgroundTextColor];
    commentBody.textAlignment = NSTextAlignmentLeft;
    commentBody.contentMode = UIViewContentModeLeft;
    // @TODO: Implement clickable comment username tag to open UserProfileViewController
    // @TODO: Implement clickable links too open TeamieWebViewController

    commentBody.text = comment.message;
    commentBody.editable = NO;
    commentBody.dataDetectorTypes = UIDataDetectorTypeAll;
    [self sizeToFitAndDisableScroll:commentBody];
    
    nowX = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?640.0:195.0;
    nowY += commentBody.frame.size.height + UI_SMALL_MARGIN;
    
    if ([thoughtType isEqualToString:@"question"]) {
        
        UIButton* commentMarkRight = [[UIButton alloc] initWithFrame:CGRectMake(nowX - UI_MEDIUM_LARGE_MARGIN, nowY, 20.0, 20.0)];
        if ([comment.markRightAccess integerValue] == 1) {
            if ([comment.markRightStatus integerValue] == 0) {
                [commentMarkRight setImage:[UIImage imageNamed:@"icon-thought-comment-mark-correct-neutral.png"] forState:UIControlStateNormal];
            }
            else {
                [commentMarkRight setImage:[UIImage imageNamed:@"icon-thought-comment-mark-correct.png"] forState:UIControlStateNormal];
            }
            [commentMarkRight addTarget:self action:@selector(markAsRight:) forControlEvents:UIControlEventTouchUpInside];
            commentMarkRight.tag = [comment.cid integerValue];
            [commentView addSubview:commentMarkRight];
        }
        
        nowX += UI_MEDIUM_MARGIN;
        UILabel* commentVoteCount = [[UILabel alloc] initWithFrame:CGRectMake(nowX, nowY + 2.5, 45.0, 15.0)];
        commentVoteCount.text = [comment.voteCount stringValue];
        if ([comment.voteCount integerValue] == 1) {
            commentVoteCount.text = [commentVoteCount.text stringByAppendingString:@" Vote"];
        }
        else {
            commentVoteCount.text = [commentVoteCount.text stringByAppendingString:@" Votes"];
        }
        [commentVoteCount setTextColor:[TeamieStylesheet textColor]];
        [commentVoteCount setFont:[TeamieStylesheet thoughtCommentVoteCountFont]];
        [commentVoteCount setBackgroundColor:[TeamieStylesheet backgroundTextColor]];
        nowX += commentVoteCount.frame.size.width + UI_SMALL_MARGIN;
        
        UIButton* commentVoteUp = [[UIButton alloc] initWithFrame:CGRectMake(nowX, nowY, 20.0, 20.0)];
        if ([comment.voteUpStatus integerValue] == 1) {
            [commentVoteUp setImage:[UIImage imageNamed:@"icon-thought-comment-vote-up.png"] forState:UIControlStateNormal];
        }
        else {
            [commentVoteUp setImage:[UIImage imageNamed:@"icon-thought-comment-vote-up-neutral.png"] forState:UIControlStateNormal];
        }
        commentVoteUp.tag = [comment.cid integerValue];
        [commentVoteUp addTarget:self action:@selector(upVote:) forControlEvents:UIControlEventTouchUpInside];
        [commentVoteUp setSelected:YES];
        
        nowX += commentVoteUp.frame.size.width + UI_MEDIUM_MARGIN;
        
        UIButton* commentVoteDown = [[UIButton alloc] initWithFrame:CGRectMake(nowX, nowY, 20.0, 20.0)];
        if ([comment.voteDownStatus integerValue] == 1) {
            [commentVoteDown setImage:[UIImage imageNamed:@"icon-thought-comment-vote-down.png"] forState:UIControlStateNormal];
        }
        else {
            [commentVoteDown setImage:[UIImage imageNamed:@"icon-thought-comment-vote-down-neutral.png"] forState:UIControlStateNormal];
        }
        commentVoteDown.tag = [comment.cid integerValue];
        [commentVoteDown addTarget:self action:@selector(downVote:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([comment.author.uid integerValue] != [self.apiCaller.currentUser.uid integerValue]) {
            [commentView addSubview:commentVoteUp];
            [commentView addSubview:commentVoteDown];
        }
        [commentView addSubview:commentVoteCount];
    }
    
    nowY += commentTimestamp.frame.size.height + UI_SMALL_MARGIN;
    commentView.frame = CGRectMake(0, 0, totalWidth, nowY);
    
    if ([thoughtType isEqualToString:@"question"]) {
        if ([comment.isCorrect integerValue] >= 1) {
            UIView* commentEdge = [[UIView alloc] initWithFrame:CGRectMake(commentView.frame.origin.x -5.0, commentView.frame.origin.y, 5.0, commentView.frame.size.height)];
            [commentEdge setBackgroundColor:[UIColor colorWithHex:@"#00b143" alpha:1.0]];
            [commentView addSubview:commentEdge];
        }
    }
    
    // add to the commentView subview
    [commentView addSubview:commentAuthor];
    [commentView addSubview:commentAuthorName];
    [commentView addSubview:commentBody];
    [commentView addSubview:commentTimestamp];

    return commentView;
}

- (void)setupNotifications{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                            action:@selector(didTapAnywhere:)];
}

- (void)addThoughtActionButton {
    
    UIBarButtonItem* moreActionsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(showThoughtActionSheet:)];
    [moreActionsButton setTintColor:[UIColor colorWithHex:@"#d94c5a" alpha:1.0]];    
    
    NSPredicate* filterPredicate = [NSPredicate predicateWithFormat:@"(name == 'like' || name == 'same') && access == 1"];
    NSArray* filteredActions = [self.thoughtData.actions filteredArrayUsingPredicate:filterPredicate];
    
    if ([filteredActions count] <= 0) {
        [self.navigationController.navigationBar.topItem setRightBarButtonItem:moreActionsButton animated:YES];
        return;
    }
    
    SimplePostAction* thoughtAction = [filteredActions objectAtIndex:0];
    
    NSArray* defaultMenus = [TeamieGlobals defaultTeamieThoughtActionMenus];
    
    // Determine the thought action button accordingly
    NSString* buttonTitle = [thoughtAction.name isEqualToString:@"like"] ? ([thoughtAction.status boolValue] ? [defaultMenus objectAtIndex:1] : [defaultMenus objectAtIndex:0]) : ([thoughtAction.status boolValue] ? [defaultMenus objectAtIndex:3] : [defaultMenus objectAtIndex:2]);
    
    // set the title of the thought action button
    UIBarButtonItem* actionButton = [[UIBarButtonItem alloc] initWithTitle:buttonTitle style:UIBarButtonItemStyleBordered target:self action:@selector(thoughtActionSelected:)];
    [actionButton setTintColor:[UIColor colorWithHex:@"#d94c5a" alpha:1.0]];
    
    [self.navigationController.navigationBar.topItem setRightBarButtonItems:[NSArray arrayWithObjects:moreActionsButton, actionButton, nil] animated:YES];
    
}

- (void)showThoughtActionSheet:(id)sender {
    // The app supports only delete action now. So check if the user has Delete access to the thought.
    NSPredicate* filterPredicate = [NSPredicate predicateWithFormat:@"name == 'delete-thought' && access == 1"];
    
    NSArray* thoughtActionsData = self.thoughtData.actions;
    NSArray* filteredActions = [thoughtActionsData filteredArrayUsingPredicate:filterPredicate];
    NSLog(@"Filtered Actions: %@", filteredActions);
    
    NSString* actionSheetTitle = TeamieLocalizedString(@"TITLE_THOUGHT_ACTIONS", @"Thought Actions"); //Action Sheet Title
    NSString* deleteAction; //Action Sheet Button Titles
    NSString* refreshAction = TeamieLocalizedString(@"BTN_REFRESH", @"Refresh");
    NSString* editAction;
    NSString* reportAction;
    NSString* followAction;
    NSString* cancelAction = TeamieLocalizedString(@"BTN_CANCEL", @"Cancel");
    
    int count = 0;
    NSLog(@"filtered Actions: %d", [filteredActions count]);
    while (count < [filteredActions count]) {
        SimplePostAction* thoughtAction = [filteredActions objectAtIndex:count];
        
        if ([thoughtAction.name isEqualToString:@"edit-thought"] && [thoughtAction.access boolValue]) {
            NSLog(@"Edit Action: Yes");
            editAction = @"Edit";
        }
        if ([thoughtAction.name isEqualToString:@"delete-thought"] && [thoughtAction.access boolValue]) {
            deleteAction = @"Delete";
        }
        if ([thoughtAction.name isEqualToString:@"report"] && [thoughtAction.access boolValue]) {
            reportAction = @"Report";
        }
        if ([thoughtAction.name isEqualToString:@"follow"] && [thoughtAction.access boolValue]) {
            followAction = @"Follow";
        }
        count++;
    }
    
    NSLog(@"Edit Action= %@", editAction);
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelAction
                                  destructiveButtonTitle:deleteAction
                                  otherButtonTitles:refreshAction, followAction, reportAction, nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if  ([buttonTitle isEqualToString:@"Delete"]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:TeamieLocalizedString(@"Are you sure you want to delete this $thought[s]$?", nil) delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
        alertView.tag = kAlertViewWillDelete;
        [alertView show];
        //After Delete button is pressed Thought Delete Request is sent
    }
    if ([buttonTitle isEqualToString:@"Refresh"]) {
        NSLog(@"Refresh action pressed");
        [self thoughtRefreshed];
    }
    if ([buttonTitle isEqualToString:@"Edit"]) {
        NSLog(@"Edit pressed");
    }
    if ([buttonTitle isEqualToString:@"Follow"]) {
        NSLog(@"Follow pressed");
    }
    if ([buttonTitle isEqualToString:@"Report"]) {
        NSLog(@"Report pressed");
    }
    if ([buttonTitle isEqualToString:@"Cancel"]) {
        NSLog(@"Cancel pressed --> Cancel ActionSheet");
    }
}

#pragma mark - Alert view delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
        if (alertView.tag == kAlertViewWillDelete) {
            return;
        }
    }
    else {
        NSLog(@"Deleting Thought");
        [self.apiCaller performRestRequest:TeamieUserDeleteThoughtRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:self.thoughtData.tid, @"tid", nil]];
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_THOUGHT_DELETE_PROGRESS", @"Deleting thought...") margin:10.0f yOffset:10.0f];
    }
    
}

#pragma mark - Public API methods
-(void)dismissThoughtView:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Presenting view controller: %@ %@", [self.presentingViewController class], [self.navigationController.presentingViewController class]);
    
    if ([self.thoughtViewDelegate respondsToSelector:@selector(didDismissThoughtView:)]) {
        // pass the thought data object back to the presenting view controller so it would update its view
        [self.thoughtViewDelegate performSelector:@selector(didDismissThoughtView:) withObject:self.thoughtData];
    }
    
    [[GANTracker sharedTracker] trackEvent:@"Thought View" action:@"Thought View Cancel" label:nil value:(int)[self.thoughtData.tid longLongValue] withError:nil];
}

- (void)thoughtActionSelected:(id)sender {
    
    if (![sender isKindOfClass:[UIBarButtonItem class]]) {
#ifdef DEBUG
        NSLog(@"Error: Thought action button is not a UIBarButtonItem");
#endif
        return;
    }
    
    NSString* actionName = ((UIBarButtonItem*)sender).title;
    NSArray* thoughtActionMenus = [TeamieGlobals defaultTeamieThoughtActionMenus];
    
    NSInteger actionIndex = [thoughtActionMenus indexOfObject:actionName];
    
    if (actionIndex == NSNotFound) {
#ifdef DEBUG
        NSLog(@"Error: Thought action menu '%@' could not be found in the default menu list", actionName);
#endif
        return;
    }
    else {
        TeamieRESTRequest requestToMake;
        
        if (actionIndex == 0 || actionIndex == 1) {     // Like or unlike
            requestToMake = TeamieUserLikeThoughtRequest;
        }
        else if (actionIndex == 2 || actionIndex == 3) {    // Echo or unecho
            requestToMake = TeamieUserSameThoughtRequest;
        }
        else {
#ifdef DEBUG
            NSLog(@"Error: This request (%@) is not yet supported!", actionName);
#endif
            return;
        }
        
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_PROCESSING", nil) margin:10.f yOffset:10.f];
        
        [self.apiCaller performRestRequest:requestToMake withParams:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%qi", [self.thoughtData.tid longLongValue]], @"tid", nil]];
    }
    
    [[GANTracker sharedTracker] trackEvent:@"Thought View" action:@"Thought Action" label:actionName value:(int)[self.thoughtData.tid longLongValue] withError:nil];
}

- (void)thoughtPollOptionSelected:(id)sender {
    
    UIButton* selectedButton = (UIButton*)sender;
    BOOL checked = ![selectedButton isSelected];
    [selectedButton setSelected:checked];
    
    NSUInteger pollIndex = [_pollCheckboxes indexOfObject:selectedButton];
    if (pollIndex == NSNotFound) {
        return;
    }
    
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_PROCESSING", nil) margin:10.f yOffset:10.f];
    NSString* choiceKey = [NSString stringWithFormat:@"%d", [TeamieGlobals extractNumberFromString:[_pollKeys objectAtIndex:pollIndex]]];
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat:@"%qi", [self.thoughtData.tid longLongValue]], @"tid", @"1", @"num_choices", choiceKey, @"choices[1][choice]", (checked ? @"1" : @"0"), @"choices[1][value]", nil];
    
    [self.apiCaller performRestRequest:TeamieUserPollThoughtRequest withParams:params];
    
    [[GANTracker sharedTracker] trackEvent:@"Thought View" action:@"Thought Poll Selected" label:nil value:(int)[self.thoughtData.tid longLongValue] withError:nil];
}

#pragma mark - IBAction methods
- (IBAction)postThoughtComment:(id)sender {
    // disable the send button
    [sender setEnabled:NO];
    
    // show an activity label
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_COMMENT_IN_PROGRESS", nil) margin:10.f yOffset:10.f];
    
    [self.apiCaller performRestRequest:TeamieUserPostThoughtCommentRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%qi", [self.thoughtID longLongValue]], @"tid", self.commentEditor.text, @"message", nil]];
    
    [[GANTracker sharedTracker] trackEvent:@"Thought View" action:@"Thought Comment Post" label:nil value:(int)[self.thoughtID longLongValue] withError:nil];
}

- (IBAction)textfieldEditingChanged:(id)sender {
    [self.commentPostButton setEnabled:([self.commentEditor.text length] > 0)];
}

#pragma mark Comment Action methods
- (void)upVote:(UIButton *)sender {
    NSInteger commentID = sender.tag;
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:commentID], @"cid", @"up", @"action", nil];
    [self.apiCaller performRestRequest:TeamieUserThoughtCommentVoteRequest withParams:params];
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_COMMENTS", nil) margin:10.f yOffset:10.f];
}

- (void)downVote:(UIButton *)sender {
    NSInteger commentID = sender.tag;
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:commentID], @"cid", @"down", @"action", nil];
    [self.apiCaller performRestRequest:TeamieUserThoughtCommentVoteRequest withParams:params];
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_COMMENTS", nil) margin:10.f yOffset:10.f];
}

- (void)markAsRight:(UIButton *)sender {
    NSInteger commentID = sender.tag;
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:commentID], @"cid", nil];
    [self.apiCaller performRestRequest:TeamieUserThoughtCommentMarkRightRequest withParams:params];
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_COMMENTS", nil) margin:10.f yOffset:10.f];
}

#pragma mark - Keyboard Delegate methods
- (void)keyboardWillShow:(NSNotification *)note {
    [self.thoughtView addGestureRecognizer:tapRecognizer];
    
    // move the comment editor according to the keyboard
    [TeamieGlobals moveControl:self.bottomToolbar forKeyboard:note up:YES distance:0 inView:self.view];
    
}

- (void)keyboardWillHide:(NSNotification *)note {
    [self.thoughtView removeGestureRecognizer:tapRecognizer];
    [TeamieGlobals moveControl:self.bottomToolbar forKeyboard:note up:NO distance:0 inView:self.view];
}

- (void)didTapAnywhere:(UITapGestureRecognizer *)recognizer {
    [self.commentEditor resignFirstResponder];
}

#pragma mark Motion detection delegates
- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    
    if (event.subtype == UIEventSubtypeMotionShake) {
        // If device is shaked, capture the Motion Shake Event and do something
        NSLog(@"Device Shaked");
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);    //Vibrate Device
     }
    
    if ([super respondsToSelector:@selector(motionEnded:withEvent:)]) {
        [super motionEnded:motion withEvent:event];
    }
}

#pragma mark Helper methods
- (void)sizeToFitAndDisableScroll:(UITextView*)textView{
    [textView sizeToFit];
    
    CGRect frame = textView.frame;
    frame.size = textView.contentSize;
    textView.frame = frame;
    
    textView.scrollEnabled = NO;
}

@end
