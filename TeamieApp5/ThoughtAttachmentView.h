//
//  ThoughtAttachmentView.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 16/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ThoughtAttachmentView;

@protocol ThoughtAttachmentViewDelegate <NSObject>

@optional
- (void)pushViewController:(UIViewController*)viewController animated:(BOOL)animated;

@end

@interface ThoughtAttachmentView : UIView

@property (nonatomic, weak) id<ThoughtAttachmentViewDelegate> delegate;

@end
