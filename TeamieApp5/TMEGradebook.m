//
//  Gradebook.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TMEGradebook.h"
#import "TMEUserScore.h"
#import "TMEQuizSimple.h"

@implementation TMEGradebook

@synthesize quizList;
@synthesize scoreList;

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"quizList": @"entities",
        @"scoreList": @"users",
    };
}

+ (NSValueTransformer *)quizListJSONTransformer {
    return [TMEModel listJSONTransformer:TMEQuizSimple.class];
}

+ (NSValueTransformer *)scoreListJSONTransformer {
    return [TMEModel listJSONTransformer:TMEUserScore.class];
}

- (NSArray*)exportDataForCSV {
    if ([self.scoreList count] <= 0) {
        return nil;
    }
    
    NSMutableArray* data = [[NSMutableArray alloc] init];
    
    [data addObject:@"NAME"];
    // add titles of every quiz
    for (TMEQuizSimple* quiz in self.quizList) {
        // the quiz titles better be in uppercase since this is the column title
        [data addObject:[NSString stringWithFormat:@"%@ (Total: %f)", [quiz.title uppercaseString], [quiz.totalScore floatValue]]];
    }
    
    for (TMEUserScore* scoreItem in self.scoreList) {
        
        // the \n is prefixed to indicate the beginning of a new line
        [data addObject:[NSString stringWithFormat:@"\n%@", scoreItem.user.realName]];
        
        for (TMEQuizSimple* quiz in self.quizList) {
            // add this user's quiz score info
            NSPredicate* filterQuiz = [NSPredicate predicateWithFormat:@"(self.entity_id == %qi)", [quiz.qid longLongValue]];
            NSArray* filteredQuizzes = [scoreItem.quizScores filteredArrayUsingPredicate:filterQuiz];
            
            if (([filteredQuizzes count] > 0) && ([[[filteredQuizzes objectAtIndex:0] valueForKey:@"score_set"] boolValue])) {
                [data addObject:[NSString stringWithFormat:@"%f", [[[filteredQuizzes objectAtIndex:0] valueForKey:@"score"] doubleValue]]];
            }
            else {
                [data addObject:TMELocalize(@"error.unavailable")];
            }
        }
    }
    
    for (NSInteger i = 0; i < 3; i++) {
        NSString* nameLabel = (i == 0) ? @"Highest Score" : ((i == 1) ? @"Lowest Score" : @"Avg Score");
        [data addObject:[NSString stringWithFormat:@"\n%@", nameLabel]];
        
        for (TMEQuizSimple* quiz in self.quizList) {
            NSNumber* scoreNumber = (i == 0) ? quiz.highScore : ((i == 1) ? quiz.lowScore : quiz.avgScore);
            [data addObject:[NSString stringWithFormat:@"%f", [scoreNumber doubleValue]]];
        }
    }
    
    return data;
}

@end
