//
//  PollItemCell.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 23/05/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ShareBoxPollOptions.h"

// we will use this delegate method to persist data after changing it
@protocol PollItemCellDelegate
-(void)cellItemDidUpdate;
@end

@interface PollItemCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;

@property (weak,nonatomic) id<PollItemCellDelegate> itemDelegate;

@property(strong,nonatomic) ShareBoxPollOptions *currentItem;

-(void)customInit;
-(void)updateCell;


@end
