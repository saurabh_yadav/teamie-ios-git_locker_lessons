//
//  AuthorFieldView.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 3/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublishStatusLabel.h"
#import "UILabelWithPadding.h"
//typedef enum{
//    Removal = 0,
//    Drafted,
//    Published,
//    PublishStatusCount
//}PublishStatus;

@interface AuthorFieldView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *authorImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabelWithPadding *PublishStatusLabelAuthor;


@end
