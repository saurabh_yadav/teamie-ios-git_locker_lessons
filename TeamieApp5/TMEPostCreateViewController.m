//
//  TMEPostCreateViewController.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 5/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostCreateViewController.h"
#import "TMEPostCreateViewModel.h"
#import "TMEPostCreateAttachment.h"
#import "TeamieUIGlobalColors.h"
#import "TMEUser.h"
#import "TMEPost.h"
#import "TMEClassroom.h"
#import "TMEClient.h"
#import "TeamieRevealController.h"
#import "UIView+TMEEssentials.h"
#import "UIImage+TMEColor.h"
#import "TMENewsfeedViewController.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>

#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import <AWSiOSSDKv2/S3.h>
#import <AWSiOSSDKv2/AWSCore.h>
#import <AWSCognitoSync/Cognito.h>

@interface TMEPostCreateViewController()

// MARK: Will hold the state of this view
@property (nonatomic, strong) TMEPostCreateViewModel *viewModel;

// MARK: SubViews
@property (nonatomic, weak) UIScrollView *scrollView;
@property (nonatomic, weak) UIView *superView;
@property (nonatomic, weak) UIButton *thoughtButton;
@property (nonatomic, weak) UIButton *questionButton;
@property (nonatomic, weak) UIButton *homeworkButton;
@property (nonatomic, weak) UIButton *classroomsButton;
@property (nonatomic, weak) UIButton *attachmentButton;
@property (nonatomic, weak) UIView *attachmentsView;
@property (nonatomic, weak) UIButton *attachmentsBar;
@property (nonatomic, weak) UITableView *attachmentsList;
@property (nonatomic, weak) UIView *deadlineView;
@property (nonatomic, weak) UIButton *deadlineButton;
@property (nonatomic, weak) UIButton *allDayButton;
@property (nonatomic, weak) UIButton *anonymousButton;
@property (nonatomic, weak) SZTextView *postTextView;
@property (nonatomic, weak) UIView *settingsView;
@property (nonatomic, weak) UIButton *announcementButton;
@property (nonatomic, weak) UIButton *emailAnnouncementButton;
@property (nonatomic, weak) UIButton *notificationAnnouncementButton;
@property (nonatomic, weak) UIButton *restrictCommentsButton;
@property (nonatomic, weak) UIDatePicker *deadlinePicker;
@property (nonatomic, strong) NSString *linkCopied;
// MARK: Constraints
@property (nonatomic, weak) MASConstraint *attachmentButtonWidth;
@property (nonatomic, weak) MASConstraint *deadlineViewHeight;
@property (nonatomic, weak) MASConstraint *notificationAnnouncementButtonWidth;
@property (nonatomic, weak) MASConstraint *emailAnnouncementButtonWidth;
@property (nonatomic, weak) MASConstraint *attachmentsViewHeight;

// MARK: Global Variables
@property (nonatomic, getter=areConstraintsAdded) BOOL constraintsAdded;
@property (nonatomic, strong) TMEPostCreateClassroomsViewController *classroomsListView;
@property (nonatomic, strong) UIImagePickerController *imagePicker;
@property (nonatomic, strong) ALAssetsLibrary *assetsLibrary;
@property (nonatomic, strong) NSMutableArray *mediaUploads;
@property (nonatomic, strong) NSMutableSet *mediaTitles;
@property (nonatomic, strong) NSMutableSet *links;
@property (nonatomic, strong) AWSS3TransferManagerUploadRequest *request;
@property (nonatomic, strong) MBProgressHUD *statusHUD;
@property (nonatomic) BOOL imageAttached;
//For sending refresh and delete actions
@property (nonatomic, weak) TMENewsfeedViewController *presentingNewsfeed;

@end

@implementation TMEPostCreateViewController

//String constants
static NSString * const POST_PLACEHOLDER = @"What would you like to say?";
static NSString * const ATTACH_IMAGE = @"Choose Existing Photo";
static NSString * const ATTACH_VIDEO = @"Choose Existing Video";
static NSString * const ATTACH_VIDEO_FROM_CAMERA = @"Record Video";
static NSString * const ATTACH_IMAGE_FROM_CAMERA = @"Take Photo";
static NSString * const ATTACH_AUDIO_FROM_MIC    = @"Record Audio";
static NSString * const CANCEL = @"Cancel";

//Layout constants
static UIEdgeInsets DEFAULT_PADDING;
static CGFloat DEFAULT_SIZE = 25.0f;
static CGFloat DEFAULT_WIDTH_WITH_PADDING;
static CGFloat DEFAULT_HEIGHT_WITH_PADDING;
static CGFloat AUTHOR_IMAGE_SIZE = 60.0f;
static CGFloat ANIMATION_DURATION = 0.3f;
static CGFloat ATTACHMENT_ROW_HEIGHT = 43.5f;
static CGFloat MIN_TEXT_VIEW_HEIGHT = 250.0f;

//Other constants
static UIImage *AUTHOR_IMAGE;
static UIImage *ANONYMOUS_IMAGE;
static NSInteger const ATTACHMENTS_TAG = 1;
static NSInteger const MAX_ATTACHMENTS = 10;
static NSInteger attachment_video_camera_index;
static NSInteger attachment_image_camera_index;
static NSInteger attachment_audio_mic_index;
static NSInteger attachment_image_index;
static NSInteger attachment_video_index;

//Global variables

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    if (self) {
        //Initializing properties
        self.viewModel = [TMEPostCreateViewModel new];
        self.constraintsAdded = NO;
        self.deadlineButton.selected = NO;
        self.attachmentsBar.selected = NO;
        self.classroomsListView = [[TMEPostCreateClassroomsViewController alloc] initWithDelegate:self];
        self.imagePicker = [UIImagePickerController new];
        self.imagePicker.delegate = self;
        self.assetsLibrary = [ALAssetsLibrary new];
        self.mediaUploads = [NSMutableArray new];
        self.mediaTitles = [NSMutableSet new];
        self.links = [NSMutableSet new];

        //Initializing constants and other variables
        DEFAULT_PADDING = UIEdgeInsetsMake(8, 8, 8, 8);
        DEFAULT_WIDTH_WITH_PADDING = DEFAULT_SIZE + DEFAULT_PADDING.left + DEFAULT_PADDING.right;
        DEFAULT_HEIGHT_WITH_PADDING = DEFAULT_SIZE + DEFAULT_PADDING.top + DEFAULT_PADDING.bottom;
        AUTHOR_IMAGE = [[TMEUser currentUser] picture];
        ANONYMOUS_IMAGE = [TeamieUIGlobals defaultPicForPurpose:@"profile" withSize:AUTHOR_IMAGE_SIZE];
        attachment_video_camera_index = -1;
        attachment_image_camera_index = -1;
        attachment_image_index = -1;
        attachment_video_index = -1;
    }
    return self;
}

- (instancetype)initWithClassroom:(TMEClassroom *)classroom {
    self = [self init];
    if (self) {
        self.viewModel.selectedClassrooms = @[classroom];
        [self.classroomsListView.selectedClassrooms addObject:classroom];
    }
    return self;
}

- (instancetype)initWithCopiedLink:(NSString *)link{
    self = [self init];
    
    if (self) {
        self.linkCopied = link;
    }
    return self;
}

- (void)loadView {
    [super loadView];
    self.view = [UIView new];
    self.view.backgroundColor = [UIColor whiteColor];
    self.scrollView = [self.view createAndAddSubView:UIScrollView.class];
    self.scrollView.userInteractionEnabled = YES;
    self.superView = [self.scrollView createAndAddSubView:UIView.class];
    self.superView.userInteractionEnabled = YES;
    self.presentingNewsfeed = (TMENewsfeedViewController *)self.navigationController.viewControllers[self.navigationController.viewControllers.count - 2];
    
    [self setupNavigationBar];
    [self setupClassroomsButton];
    [self setupAttachmentButton];
    [self setupAttachmentsView];
    [self setupDeadlineView];
    [self setupAnonymousButton];
    [self setupPostTextView];
    [self setupSettingsView];
    
    [self setupKeyboard];
    
    [self hideInvalidViews];
    
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];
    [self bindViewModel];

    // Adding this here will add it to the top of the view
    self.statusHUD = [[MBProgressHUD alloc] initWithView:self.view];
    [self.superView addSubview:self.statusHUD];
    
    //Deactivate reveal controller here
    ((TeamieRevealController *)([[UIApplication sharedApplication] delegate]).window.rootViewController).frontViewController.revealController.recognizesPanningOnFrontView = NO;
}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    //Fix insets, >= iOS 8 and < iOS 8
    if ([self.attachmentsList respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.attachmentsList setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.attachmentsList respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.attachmentsList setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (void)updateViewConstraints {
    // Uses the boolean haveConstraintsBeenAdded to ensure that this is called only once
    if (!self.areConstraintsAdded) {
        [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.view);
        }];
        [self.superView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(self.view);
            make.edges.equalTo(self.scrollView);
            make.bottom.equalTo(self.view);
        }];
        [self.classroomsButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self.superView);
            make.height.equalTo(@(DEFAULT_HEIGHT_WITH_PADDING));
            
        }];
        [self.attachmentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.classroomsButton.mas_right);
            make.top.right.equalTo(self.superView);
            make.height.equalTo(@(DEFAULT_HEIGHT_WITH_PADDING));
            self.attachmentButtonWidth = make.width.equalTo(@(DEFAULT_WIDTH_WITH_PADDING));
        }];
        [self.attachmentsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.superView);
            make.top.equalTo(self.classroomsButton.mas_bottom);
            self.attachmentsViewHeight = make.height.equalTo(@(DEFAULT_HEIGHT_WITH_PADDING));
        }];
        [self.attachmentsBar mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.top.equalTo(self.attachmentsView);
            make.height.equalTo(@(DEFAULT_HEIGHT_WITH_PADDING));
        }];
        [self.attachmentsList mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.attachmentsView);
            make.top.equalTo(self.attachmentsBar.mas_bottom);
        }];
        
        [self.deadlineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.superView);
            make.top.equalTo(self.attachmentsView.mas_bottom);
            self.deadlineViewHeight = make.height.equalTo(@(DEFAULT_HEIGHT_WITH_PADDING));
        }];
        [self.deadlineButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.equalTo(self.deadlineView);
            make.height.equalTo(@(DEFAULT_HEIGHT_WITH_PADDING));
        }];
        [self.allDayButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.right.equalTo(self.deadlineView);
            make.width.equalTo(@(DEFAULT_WIDTH_WITH_PADDING));
            make.height.equalTo(self.deadlineButton);
            make.left.equalTo(self.deadlineButton.mas_right);
        }];
        [self.deadlinePicker mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.deadlineView);
            make.centerX.equalTo(self.deadlineView);
            make.left.greaterThanOrEqualTo(self.deadlineView);
            make.right.lessThanOrEqualTo(self.deadlineView);
            make.top.equalTo(self.deadlineButton.mas_bottom);
        }];
        [self.anonymousButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.superView);
            make.top.equalTo(self.deadlineView.mas_bottom);
            make.height.equalTo(@(AUTHOR_IMAGE_SIZE + 6));
            make.width.equalTo(@(AUTHOR_IMAGE_SIZE + 6));
        }];

        [self.postTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.anonymousButton.mas_right);
            make.right.equalTo(self.superView);
            make.top.equalTo(self.deadlineView.mas_bottom);
            make.height.greaterThanOrEqualTo(@(MIN_TEXT_VIEW_HEIGHT));
        }];
        [self.settingsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.superView);
            make.top.equalTo(self.postTextView.mas_bottom).with.offset(DEFAULT_PADDING.bottom);
            make.height.equalTo(@(DEFAULT_HEIGHT_WITH_PADDING));
        }];
        
        [self.announcementButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self.settingsView);
            make.width.equalTo(@(DEFAULT_WIDTH_WITH_PADDING));
            make.left.equalTo(self.settingsView.mas_left);
        }];
        
        [self.notificationAnnouncementButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self.settingsView);
            make.left.equalTo(self.announcementButton.mas_right);
            self.notificationAnnouncementButtonWidth = make.width.equalTo(@(DEFAULT_WIDTH_WITH_PADDING));
        }];

        [self.emailAnnouncementButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self.settingsView);
            make.left.equalTo(self.notificationAnnouncementButton.mas_right);
            self.emailAnnouncementButtonWidth = make.width.equalTo(@(DEFAULT_WIDTH_WITH_PADDING));
        }];

        [self.restrictCommentsButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self.settingsView);
            make.width.equalTo(@(DEFAULT_WIDTH_WITH_PADDING));
            make.left.equalTo(self.emailAnnouncementButton.mas_right).with.offset(DEFAULT_PADDING.left);
        }];

        self.constraintsAdded = YES;
    }
    [super updateViewConstraints];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/create/post"];
}

#pragma mark - Layout

- (void)setupNavigationBar {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"close"
                                                                                        withSize:DEFAULT_SIZE
                                                                                        andColor:[UIColor whiteColor]]
                                             style:UIBarButtonItemStylePlain
                                             target:self action:@selector(backButtonClicked)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"send"
                                                                                        withSize:DEFAULT_SIZE
                                                                                        andColor:[UIColor whiteColor]]
                                             style:UIBarButtonItemStylePlain
                                             target:self action:@selector(sendButtonClicked)];
    self.navigationItem.titleView = [self createPostTypeSwitch];
}

- (UIView *)createPostTypeSwitch {
    UIView *postTypeView = [UIView new];
    
    NSDictionary *sitePermissions = [[NSUserDefaults standardUserDefaults] objectForKey:SITE_PERMISSIONS_KEY];
    if ([sitePermissions[@"create homework thought"] boolValue]) {
        postTypeView.frame = CGRectMake(0, 0, DEFAULT_WIDTH_WITH_PADDING * 2 + DEFAULT_SIZE, DEFAULT_SIZE);
    }
    else {
        postTypeView.frame = CGRectMake(0, 0, DEFAULT_WIDTH_WITH_PADDING + DEFAULT_SIZE, DEFAULT_SIZE);
    }
    
    self.thoughtButton = [self createPostTypeButton:[postTypeView createAndAddSubView:UIButton.class] withType:@"thought"];
    self.thoughtButton.frame = CGRectMake(0, 0, DEFAULT_SIZE, DEFAULT_SIZE);
    
    self.questionButton = [self createPostTypeButton:[postTypeView createAndAddSubView:UIButton.class] withType:@"question"];
    self.questionButton.frame = CGRectMake(DEFAULT_SIZE + (DEFAULT_PADDING.left + DEFAULT_PADDING.right), 0, DEFAULT_SIZE, DEFAULT_SIZE);

    if ([sitePermissions[@"create homework thought"] boolValue]) {
        self.homeworkButton = [self createPostTypeButton:[postTypeView createAndAddSubView:UIButton.class] withType:@"homework"];
        self.homeworkButton.frame = CGRectMake((DEFAULT_SIZE + (DEFAULT_PADDING.left + DEFAULT_PADDING.right)) * 2, 0, DEFAULT_SIZE, DEFAULT_SIZE);
    }
    
    return postTypeView;
}

- (void)setupClassroomsButton {
    self.classroomsButton = [self.superView createAndAddSubView:UIButton.class];
    self.classroomsButton.titleEdgeInsets = DEFAULT_PADDING;
    self.classroomsButton.titleLabel.font = [TeamieGlobals appFontFor:@"actionButtonText"];
    [self.classroomsButton setTitleColor:[TeamieUIGlobalColors buttonTitleColor] forState:UIControlStateNormal];
    self.classroomsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
}

- (void)setupAttachmentButton {
    self.attachmentButton = [self.superView createAndAddSubView:UIButton.class];
    [self.attachmentButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"attachment"
                                                            withSize:DEFAULT_SIZE
                                                           andColor:[UIColor grayColor]]
                           forState:UIControlStateNormal];
    [self.attachmentButton addBorderToTop:NO bottom:NO left:YES right:NO];
}

- (void)setupAttachmentsView {
    self.attachmentsView = [self.superView createAndAddSubView:UIView.class];

    self.attachmentsBar = [self.attachmentsView createAndAddSubView:UIButton.class];
    self.attachmentsBar.titleLabel.font = [TeamieGlobals appFontFor:@"actionButton"];
    [self.attachmentsBar setTitleColor:[TeamieUIGlobalColors buttonTitleColor] forState:UIControlStateNormal];
    [self.attachmentsBar setTitle:self.viewModel.attachmentsText forState:UIControlStateNormal];
    self.attachmentsBar.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.attachmentsBar.titleEdgeInsets = DEFAULT_PADDING;
    [self.attachmentsBar addBorderToTop:YES bottom:NO left:NO right:NO];

    self.attachmentsList = [self.attachmentsView createAndAddSubView:UITableView.class];
    self.attachmentsList.dataSource = self;
    self.attachmentsList.delegate = self;
    self.attachmentsList.allowsSelection = YES;
    self.attachmentsList.estimatedRowHeight = ATTACHMENT_ROW_HEIGHT;
    [self.attachmentsList addBorderToTop:YES bottom:NO left:NO right:NO];
}

- (void)setupDeadlineView {
    self.deadlineView = [self.superView createAndAddSubView:UIView.class];
    [self.deadlineView addBorderToTop:YES bottom:NO left:NO right:NO];

    self.deadlineButton = [self.deadlineView createAndAddSubView:UIButton.class];
    self.deadlineButton.titleLabel.font = [TeamieGlobals appFontFor:@"deadlineText"];
    [self.deadlineButton setTitleColor:[TeamieUIGlobalColors buttonTitleColor] forState:UIControlStateNormal];
    [self.deadlineButton setAttributedTitle:self.viewModel.deadlineText forState:UIControlStateNormal];
    self.deadlineButton.backgroundColor = [TeamieUIGlobalColors secondaryYellowColor];
    self.deadlineButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.deadlineButton.titleEdgeInsets = DEFAULT_PADDING;
    
    self.allDayButton = [self.deadlineView createAndAddSubView:UIButton.class];
    self.allDayButton.titleLabel.font = [TeamieGlobals appFontFor:@"allDayText"];
    self.allDayButton.titleLabel.numberOfLines = 0;
    self.allDayButton.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.allDayButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.allDayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    [self.allDayButton setTitleColor:[TeamieUIGlobalColors buttonTitleColor] forState:UIControlStateNormal];
    [self.allDayButton setTitle:@"All\nDay" forState:UIControlStateNormal];
    [self.allDayButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors secondaryYellowColor]]
                                 forState:UIControlStateNormal];
    [self.allDayButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors secondaryGreenColor]]
                                 forState:UIControlStateSelected];

    UIDatePicker *datePicker = [UIDatePicker new];
    [self.deadlineView insertSubview:datePicker atIndex:0];
    self.deadlinePicker = datePicker;
    [self.deadlinePicker addBorderToTop:YES bottom:NO left:NO right:NO];
}

- (void)setupAnonymousButton {
    self.anonymousButton = [self.superView createAndAddSubView:UIButton.class];
    [self.anonymousButton setImage:AUTHOR_IMAGE forState:UIControlStateNormal];
    [self.anonymousButton setImage:ANONYMOUS_IMAGE forState:UIControlStateSelected];
    [self.anonymousButton addBorderToTop:YES bottom:NO left:NO right:NO];
}

- (void)setupPostTextView {
    self.postTextView = [self.superView createAndAddSubView:SZTextView.class];
    self.postTextView.font = [TeamieGlobals appFontFor:@"postBody"];

    UIEdgeInsets customInsets = DEFAULT_PADDING;
    customInsets.left = -2;
    self.postTextView.textContainerInset = customInsets;
    self.postTextView.dataDetectorTypes = UIDataDetectorTypeLink;
    if (self.linkCopied) {
        self.postTextView.text = self.linkCopied;
        [self.postTextView becomeFirstResponder];
    }
    self.postTextView.placeholder = POST_PLACEHOLDER;
    [self.postTextView addBorderToTop:YES bottom:NO left:NO right:NO];
}

- (void)setupSettingsView {
    self.settingsView = [self.superView createAndAddSubView:UIView.class];
    [self.settingsView addBorderToTop:YES bottom:NO left:NO right:NO];
    
    self.announcementButton = [self createSettingsButton:[self.settingsView createAndAddSubView:UIButton.class]
                                                     withType:@"announcement"];

    self.notificationAnnouncementButton = [self createSettingsButton:[self.settingsView createAndAddSubView:UIButton.class]
                                                     withType:@"bulletin"];
    
    self.emailAnnouncementButton = [self createSettingsButton:[self.settingsView createAndAddSubView:UIButton.class]
                                                     withType:@"message"];
    [self.emailAnnouncementButton addBorderToTop:NO bottom:NO left:NO right:YES];
    
    self.restrictCommentsButton = [self.settingsView createAndAddSubView:UIButton.class];
    self.restrictCommentsButton.contentMode = UIViewContentModeScaleAspectFill;
    self.restrictCommentsButton.imageEdgeInsets = DEFAULT_PADDING;
    [self.restrictCommentsButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"unlock"
                                                                       withSize:DEFAULT_SIZE]
                                 forState:UIControlStateNormal];
    [self.restrictCommentsButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"lock"
                                                                       withSize:DEFAULT_SIZE
                                                                       andColor:[TeamieUIGlobalColors primaryGreenColor]]
                                 forState:UIControlStateSelected];
}

- (void)hideInvalidViews {
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        [self.view layoutIfNeeded];
        self.attachmentButtonWidth.offset(0);
        self.attachmentButton.hidden = YES;
        [self.view layoutIfNeeded];
    }
    
    //Hide settings view if cannot post announcement or restrict comments in any classroom
    BOOL hideSettingsView = YES;
    for (TMEClassroom *classroom in [TMEClassroom getFromUserDefaults]) {
        if ([classroom.permissions[@"send thought as announcement"] boolValue] ||
            [classroom.permissions[@"limit access to thought comments"] boolValue]) {
            hideSettingsView = NO;
            break;
        }
    }
    self.settingsView.hidden = hideSettingsView;
}

#pragma mark - Keyboard events

- (void)setupKeyboard {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChange:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(detectURLAttachments)];
    [self.view addGestureRecognizer:tap];
}

- (void)keyboardWillChange:(NSNotification *) notification{
    NSDictionary *info = [notification userInfo];
    CGFloat startY = CGRectGetMinY([info[UIKeyboardFrameBeginUserInfoKey] CGRectValue]);
    CGFloat endY = CGRectGetMinY([info[UIKeyboardFrameEndUserInfoKey] CGRectValue]);
    CGRect rect = self.view.frame;
    rect.size.height += endY - startY;
    self.view.frame = rect;
}

#pragma mark - View Model bindings

- (void)bindViewModel {
    //using with @strongify(self) this makes sure that self isn't retained in the blocks
    //this is declared in RACEXTScope.h
    @weakify(self)
    
    //Block for toggling button selected state
    void (^toggleSelected)(UIButton *) = ^(UIButton *button) {
        button.selected ^= YES;
    };
    
    //One way binding from the viewModel
    RAC(self.deadlinePicker, date) = RACObserve(self.viewModel, deadline);
    RAC(self.thoughtButton, selected) = [RACObserve(self.viewModel, postType) map:^id(NSNumber *value) {
        return @([value isEqualToNumber:@(TMEPostThought)]);
    }];
    RAC(self.questionButton, selected) = [RACObserve(self.viewModel, postType) map:^id(NSNumber *value) {
        return @([value isEqualToNumber:@(TMEPostQuestion)]);
    }];
    RAC(self.homeworkButton, selected) = [RACObserve(self.viewModel, postType) map:^id(NSNumber *value) {
        return @([value isEqualToNumber:@(TMEPostHomework)]);
    }];
    
    //Hide announcement options if announcement is not selected
    RACSignal *announcement = RACObserve(self.viewModel, announcement);
    [announcement subscribeNext:^(NSNumber *announcement) {
        @strongify(self)
        [self.view layoutIfNeeded];
        CGFloat width = [announcement boolValue]? DEFAULT_WIDTH_WITH_PADDING: 0.0f;
        self.notificationAnnouncementButtonWidth.offset(width);
        self.emailAnnouncementButtonWidth.offset(width);
        
        if ([announcement boolValue]) {
            self.notificationAnnouncementButton.hidden = NO;
            self.emailAnnouncementButton.hidden = NO;
        }
        [UIView animateWithDuration:ANIMATION_DURATION animations:^{
            self.notificationAnnouncementButton.alpha = [announcement doubleValue];
            self.emailAnnouncementButton.alpha = [announcement doubleValue];
            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            if (![announcement boolValue]) {
                self.notificationAnnouncementButton.hidden = NO;
                self.emailAnnouncementButton.hidden = NO;
            }
        }];
    }];
    
    //RACChannelTo provides way binding between the view and viewModel, initial value taken from viewModel
    
    RACChannelTo(self.allDayButton, selected) = RACChannelTo(self.viewModel, allDay);
    [[[self.allDayButton rac_signalForControlEvents:UIControlEventTouchUpInside]
     doNext:toggleSelected]
     subscribeNext:^(UIButton *button) {
         @strongify(self)
         self.deadlinePicker.datePickerMode = button.selected? UIDatePickerModeDate: UIDatePickerModeDateAndTime;
         [self.deadlineButton setAttributedTitle:self.viewModel.deadlineText forState:UIControlStateNormal];
     }];
    
    RACChannelTo(self.anonymousButton, selected) = RACChannelTo(self.viewModel, anonymous);
    [[[self.anonymousButton rac_signalForControlEvents:UIControlEventTouchUpInside]
      doNext:toggleSelected]
      subscribeNext:^(UIButton *x) {
          [self statusMessage:x.selected? @"Will post anonymously":
           [NSString stringWithFormat:@"Will post as %@", [TMEUser currentUser].realName]];
      }];

    RACChannelTo(self.announcementButton, selected) = RACChannelTo(self.viewModel, announcement);
    [[[self.announcementButton rac_signalForControlEvents:UIControlEventTouchUpInside]
      doNext:toggleSelected]
      subscribeNext:^(UIButton *x) {
          [self statusMessage:x.selected? @"Will post as an announcement": @"Announcement disabled"];
      }];
 
    RACChannelTo(self.notificationAnnouncementButton, selected) = RACChannelTo(self.viewModel, notificationAnnouncement);
    [[[self.notificationAnnouncementButton rac_signalForControlEvents:UIControlEventTouchUpInside]
      doNext:toggleSelected]
      subscribeNext:^(UIButton *x) {
          [self statusMessage:x.selected? @"Will post announcement with bulletin board notification":
           @"Announcement will not have bulletin board notification"];
      }];
    
    RACChannelTo(self.emailAnnouncementButton, selected) = RACChannelTo(self.viewModel, emailAnnouncement);
    [[[self.emailAnnouncementButton rac_signalForControlEvents:UIControlEventTouchUpInside]
      doNext:toggleSelected]
      subscribeNext:^(UIButton *x) {
          [self statusMessage:x.selected? @"Will post announcement with email notification":
           @"Announcement will not have email notification"];
      }];
    
    RACChannelTo(self.restrictCommentsButton, selected) = RACChannelTo(self.viewModel, restrictComments);
    [[[self.restrictCommentsButton rac_signalForControlEvents:UIControlEventTouchUpInside]
      doNext:toggleSelected]
      subscribeNext:^(UIButton *x) {
          [self statusMessage:x.selected? @"Comments are locked": @"Comments will not be locked"];
      }];
    
    //One way binding to the view model
    
    [[self.attachmentButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        //this creates a reference to self that when used with @weakify(self)
        //makes sure self isn't retained
        @strongify(self)
        if (self.viewModel.attachments.count < MAX_ATTACHMENTS) {
            [self showAttachmentChoices];
        }
        else {
            [self errorMessage:[NSString stringWithFormat:@"Maximum allowed attachments are %ld", (long)MAX_ATTACHMENTS]];
        }
    }];

    //Switching buttons
    [[self.thoughtButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        self.viewModel.postType = TMEPostThought;
    }];
    [[self.questionButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        self.viewModel.postType = TMEPostQuestion;
    }];
    [[self.homeworkButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        @strongify(self)
        self.viewModel.postType = TMEPostHomework;
    }];
    
    //Deadline, datepicker showing and hiding
    RACSignal *showDeadline = [RACObserve(self.viewModel, postType) map:^id(NSNumber *value) {
        return @([value intValue] == TMEPostHomework);
    }];
    [showDeadline subscribeNext:^(NSNumber *isHomework) {
        //On any change, unselect deadlineButton
        self.deadlineButton.selected = NO;
    }];
    RAC(self.deadlineButton, hidden) = [showDeadline not];
    RAC(self.allDayButton, hidden) = [showDeadline not];
    [[self.deadlineButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:toggleSelected];
    [[RACSignal combineLatest:@[showDeadline, RACObserve(self.deadlineButton, selected)]
     reduce:^(NSNumber *showDeadlineVal, NSNumber *showPicker){
         @strongify(self)
         return @([showDeadlineVal intValue] * (DEFAULT_HEIGHT_WITH_PADDING +
                                           ([showPicker boolValue]? self.deadlinePicker.frame.size.height: 0.0f)));
     }]
     subscribeNext:^(NSNumber *height) {
         @strongify(self)
         [self.view layoutIfNeeded];
         self.deadlineViewHeight.offset([height doubleValue]);
         
         BOOL showPicker = self.deadlineButton.selected;
         if (showPicker) {
             self.deadlinePicker.hidden = NO;
         }
         [UIView animateWithDuration:ANIMATION_DURATION animations:^{
             self.deadlinePicker.alpha = showPicker;
             [self.view layoutIfNeeded];
         } completion:^(BOOL finished) {
             if (!showPicker) {
                 self.deadlinePicker.hidden = YES;
             }
         }];
     }];
    
    //Updating deadlinebutton label on changing deadline
    [[[self.deadlinePicker rac_signalForControlEvents:UIControlEventValueChanged]
      doNext:^(id x) {
          @strongify(self)
          self.viewModel.deadline = self.deadlinePicker.date;
      }]
      subscribeNext:^(id x) {
          @strongify(self)
          [self.deadlineButton setAttributedTitle:self.viewModel.deadlineText forState:UIControlStateNormal];
      }];
    
    //Update post text
    [self.postTextView.rac_textSignal subscribe:RACChannelTo(self.viewModel, postContentText)];
    
    //Classrooms Button
    [[self.classroomsButton rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:^(id x) {
        [self.navigationController pushViewController:self.classroomsListView animated:YES];
    }];
    
    [RACObserve(self.viewModel, selectedClassrooms)
     subscribeNext:^(id x) {
         [self.classroomsButton setAttributedTitle:self.viewModel.classroomsText forState:UIControlStateNormal];
     }];
    
    //Attachments
    RACSignal *showAttachments = [[RACObserve(self.viewModel, attachments)
                                   doNext:^(id x) {
                                       @strongify(self)
                                       [self.attachmentsList reloadData];
                                   }]
                                   map:^id(NSMutableArray *value) {
                                       return @(value.count > 0);
                                   }];
    RAC(self.attachmentsBar, hidden) = [showAttachments not];
    [RACObserve(self.viewModel, attachments) subscribeNext:^(id x) {
        @strongify(self)
        [self.attachmentsBar setTitle:self.viewModel.attachmentsText forState:UIControlStateNormal];
    }];
    
    [[self.attachmentsBar rac_signalForControlEvents:UIControlEventTouchUpInside] subscribeNext:toggleSelected];
    [[RACSignal
      combineLatest:@[showAttachments, RACObserve(self.attachmentsBar, selected)]
      reduce:^(NSNumber *showAttachmentsBar, NSNumber *showAttachmentsList){
          @strongify(self)
          return @([showAttachmentsBar intValue] * (DEFAULT_HEIGHT_WITH_PADDING +
             ([showAttachmentsList boolValue]? ATTACHMENT_ROW_HEIGHT * self.viewModel.attachments.count - 1.0f : 0.0f)));
          //Subtracting 1 to remove the bottom seperator
      }]
      subscribeNext:^(NSNumber *height) {
          @strongify(self)
          [self.view layoutIfNeeded];
          self.attachmentsViewHeight.offset([height doubleValue]);
          if (height == 0) {
              self.attachmentsBar.selected = NO;
          }
          BOOL showList = self.attachmentsBar.selected;
          if (showList) {
              self.attachmentsList.hidden = NO;
          }
          [UIView animateWithDuration:ANIMATION_DURATION animations:^{
              [self.view layoutIfNeeded];
          } completion:^(BOOL finished) {
              if (!showList) {
                  self.attachmentsList.hidden = YES;
              }
          }];
     }];
    
    //Status messages
}

#pragma mark - Actions

- (void)backButtonClicked {
    UIAlertView *confirmDiscard = [[UIAlertView alloc] initWithTitle:@"Discard Post"
                                                             message:@"Are you sure you want to discard this post?"
                                                            delegate:self
                                                   cancelButtonTitle:@"No"
                                                   otherButtonTitles:@"Yes", nil];
    [confirmDiscard show];
}

- (void)sendButtonClicked {
    if ([self validateSend]) {
        self.statusHUD.mode = MBProgressHUDModeIndeterminate;
        dispatch_async(dispatch_get_main_queue(),^ {
            [self.statusHUD show:YES];
        });
        [self detectURLAttachments];
        
        //There are attachments to be uploaded
        if (self.mediaTitles.count > 0) {
            [self makeAWSRequests];
        }
        else {
            [self uploadImage];
        }
    }
}

- (void)showAttachmentChoices {
    UIActionSheet *attachmentChoicePopup = [[UIActionSheet alloc] initWithTitle:TMELocalize(@"message.new-attachment")
                                                                       delegate:self
                                                              cancelButtonTitle:nil
                                                         destructiveButtonTitle:nil
                                                              otherButtonTitles:nil];
    attachment_image_camera_index = [attachmentChoicePopup addButtonWithTitle:ATTACH_IMAGE_FROM_CAMERA];
    attachment_video_camera_index = [attachmentChoicePopup addButtonWithTitle:ATTACH_VIDEO_FROM_CAMERA];
    attachment_audio_mic_index = [attachmentChoicePopup addButtonWithTitle:ATTACH_AUDIO_FROM_MIC];
    attachment_image_index = [attachmentChoicePopup addButtonWithTitle:ATTACH_IMAGE];
    attachment_video_index = [attachmentChoicePopup addButtonWithTitle:ATTACH_VIDEO];
    attachmentChoicePopup.cancelButtonIndex = [attachmentChoicePopup addButtonWithTitle:CANCEL];

    attachmentChoicePopup.tag = ATTACHMENTS_TAG;
    [attachmentChoicePopup showFromRect:self.attachmentButton.frame
                                 inView:self.superView
                               animated:YES];
}

#pragma mark - UITextViewDelegate

//TODO: Is this working? don't think so
- (void)textViewDidChange:(UITextView *)textView{
    void (^scrollToCaret)() = ^() {
        CGRect rect = [self.postTextView caretRectForPosition:self.postTextView.selectedTextRange.end];
        rect.size.height += self.postTextView.textContainerInset.bottom;
        [self.postTextView scrollRectToVisible:rect animated:NO];
    };
    if ([textView.text hasSuffix:@"\n"]) {
        [CATransaction setCompletionBlock:^{
            scrollToCaret();
        }];
    } else {
        scrollToCaret();
    }
}

#pragma mark - UIAlertViewDelegate (Cancel Post)

// This alert view is shown when the user clicks on close (top left)
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == [alertView cancelButtonIndex]) {
        //Cancelled, nothing to be done
        TMETrackEvent(@"post_share", @"tap_cancel", @"post");
    }
    else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UIActionSheetDelegate (Choose Attachment)

// This action sheet is shown when a user clicks on add attachment
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    switch (actionSheet.tag) {
        case ATTACHMENTS_TAG:
            if (buttonIndex == actionSheet.cancelButtonIndex) {
                //Do nothing
            }
            else if (buttonIndex == attachment_video_camera_index) {
                if (![self deviceHasCamera]) {
                    [self errorMessage:TMELocalize(@"error.no-camera")];
                }
                else {
                    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    self.imagePicker.mediaTypes = @[(NSString *)kUTTypeMovie];
                    self.imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModeVideo;
                    [self showPicker];
                }
            }
            else if (buttonIndex == attachment_image_camera_index) {
                if (![self deviceHasCamera]) {
                    [self errorMessage:TMELocalize(@"error.no-camera")];
                }
                else if (self.imageAttached) {
                    [self errorMessage:TMELocalize(@"error.only-one-image")];
                }
                else {
                    self.imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
                    self.imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
                    self.imagePicker.cameraCaptureMode = UIImagePickerControllerCameraCaptureModePhoto;
                    [self showPicker];
                }
            }
            else if (buttonIndex == attachment_audio_mic_index) {
                // Return if file
                TMEPostCreateAudioAttachmentViewController *audioVC = [TMEPostCreateAudioAttachmentViewController new];
                audioVC.delegate = self;
                [self presentViewController:audioVC animated:YES completion:nil];
            }
            else if (buttonIndex == attachment_image_index) {
                if (![self deviceHasImages]) {
                    [self errorMessage:TMELocalize(@"error.no-images-library")];
                }
                else if (self.imageAttached) {
                    [self errorMessage:TMELocalize(@"error.only-one-image")];
                }
                else {
                    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    //Limit to images only
                    self.imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
                    [self showPicker];
                }
            }
            else if (buttonIndex == attachment_video_index) {
                if (![self deviceHasVideos]) {
                    [self errorMessage:TMELocalize(@"error.no-videos-library")];
                }
                else {
                    self.imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    //Limit to videos only
                    self.imagePicker.mediaTypes = @[(NSString *)kUTTypeMovie];
                    [self showPicker];
                }
            }
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker
didFinishPickingMediaWithInfo:(NSDictionary *)info {
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        NSString *mediaType = info[UIImagePickerControllerMediaType];
        if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
            [self.assetsLibrary writeImageToSavedPhotosAlbum:[info[UIImagePickerControllerOriginalImage] CGImage]
                                                 orientation:(ALAssetOrientation)[info[UIImagePickerControllerOriginalImage] imageOrientation]
                                             completionBlock:^(NSURL *assetURL, NSError *error) {
                                                 [self createImageAttachment:assetURL
                                                                   withImage:info[UIImagePickerControllerOriginalImage]];
                                             }];
        }
        else if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) {
            if ([self.assetsLibrary videoAtPathIsCompatibleWithSavedPhotosAlbum:info[UIImagePickerControllerMediaURL]]) {
                [self.assetsLibrary
                 writeVideoAtPathToSavedPhotosAlbum:info[UIImagePickerControllerMediaURL]
                 completionBlock:^(NSURL *assetURL, NSError *error) {
                     [self createVideoAttachmentWithAsset:assetURL
                                                  andfile:info[UIImagePickerControllerMediaURL]];
                 }];
            }
        }
    } else if (picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        if ([picker.mediaTypes containsObject:(NSString *)kUTTypeImage]) {
            [self createImageAttachment:info[UIImagePickerControllerReferenceURL]
                              withImage:info[UIImagePickerControllerOriginalImage]];
        } else if ([picker.mediaTypes containsObject:(NSString *)kUTTypeMovie]) {
            [self createVideoAttachmentWithAsset:info[UIImagePickerControllerReferenceURL]
                                         andfile:info[UIImagePickerControllerMediaURL]];
        }
    }
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - TMEPostCreateAudioAttachmentViewControllerDelegate

- (void)didCompleteRecordingAudioWithFilePath:(NSURL *)path andAudioLength:(CGFloat)time {
    if (path) {
        [self createAudioAttachmentWithFileURL:path andTime:time];
    }
}

#pragma mark - TMEPostCreateClassroomsViewDelegate (Classrooms)

- (void)didFinishPickingClassrooms:(NSArray *)classrooms {
    self.viewModel.selectedClassrooms = classrooms;
    
}

#pragma mark - UITableViewDatasource (Attachments)

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.viewModel.attachments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //For such a small number of cells, dequeueing will be slower than creating one here.
    UITableViewCell *cell = [UITableViewCell new];
    TMEPostCreateAttachment *attachment = (TMEPostCreateAttachment *)self.viewModel.attachments[indexPath.row];
    cell.textLabel.text = attachment.title;
    cell.imageView.image = attachment.thumbnail;
    cell.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    //Constrain image to 33 x 33
    CGSize itemSize = CGSizeMake(33, 33);
    UIGraphicsBeginImageContextWithOptions(itemSize, NO, UIScreen.mainScreen.scale);
    CGRect imageRect = CGRectMake(0.0, 1.0, itemSize.width, itemSize.height);
    [cell.imageView.image drawInRect:imageRect];
    cell.imageView.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();

    return cell;
}

// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [tableView beginUpdates];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];

        //If it is a media object, the user should be able to add it again
        TMEPostCreateAttachment *attachment = self.viewModel.attachments[indexPath.row];
        if ([attachment isKindOfClass:TMEPostCreateMediaAttachment.class]) {
            [self.mediaTitles removeObject:attachment.title];
            if ([attachment isKindOfClass:TMEPostCreateImageAttachment.class]) {
                self.imageAttached = NO;
            }
        }
        
        if ([attachment isKindOfClass:TMEPostCreateImageAttachment.class]) {
            TMETrackEvent(@"post_share", @"remove_attachment", @"image");
        }
        else if ([attachment isKindOfClass:TMEPostCreateVideoAttachment.class]) {
            TMETrackEvent(@"post_share", @"remove_attachment", @"video");
        }
        else if ([attachment isKindOfClass:TMEPostCreateLinkAttachment.class]) {
            TMETrackEvent(@"post_share", @"remove_attachment", @"link");
        }
        
        //Force it to fire KVO
        [[self.viewModel mutableArrayValueForKey:@"attachments"] removeObjectAtIndex:indexPath.row];
        [tableView endUpdates];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

#pragma mark - UITableViewDelegate (Attachments)

//Set Seperator insets to zero
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    //For < iOS 8
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    //For >= iOS7
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ATTACHMENT_ROW_HEIGHT;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TMEPostCreateAttachment *attachment = self.viewModel.attachments[indexPath.row];
    if ([attachment isKindOfClass:TMEPostCreateVideoAttachment.class]) {
        TMEPostCreateVideoAttachment *video = (TMEPostCreateVideoAttachment *)attachment;
        MPMoviePlayerViewController *moviePlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:video.fileURL];
        [self presentMoviePlayerViewControllerAnimated:moviePlayer];
    }
    else if ([attachment isKindOfClass:TMEPostCreateAudioAttachment.class]) {
        TMEPostCreateAudioAttachment *audio = (TMEPostCreateAudioAttachment *)attachment;
        MPMoviePlayerViewController *audioPlayer = [[MPMoviePlayerViewController alloc] initWithContentURL:audio.fileURL];
        [self presentMoviePlayerViewControllerAnimated:audioPlayer];
    }
}

#pragma mark - Layout Helpers

- (UIButton *)createPostTypeButton:(UIButton *)button withType:(NSString *)type {
    button.contentMode = UIViewContentModeScaleAspectFill;
    button.imageEdgeInsets = UIEdgeInsetsZero;
    [button setImage:[TeamieUIGlobals defaultPicForPurpose:type
                                                  withSize:DEFAULT_SIZE
                                                  andColor:[UIColor whiteColor]]
            forState:UIControlStateNormal];
    [button setImage:[TeamieUIGlobals defaultPicForPurpose:type
                                                  withSize:DEFAULT_SIZE
                                                  andColor:[TeamieUIGlobalColors secondaryYellowColor]]
            forState:UIControlStateSelected];
    return button;
}

- (UIButton *)createSettingsButton:(UIButton *)settingsButton withType:(NSString *)type {
    settingsButton.contentMode = UIViewContentModeScaleAspectFill;
    settingsButton.imageEdgeInsets = DEFAULT_PADDING;
    [settingsButton setImage:[TeamieUIGlobals defaultPicForPurpose:type
                                                              withSize:DEFAULT_SIZE]
                        forState:UIControlStateNormal];
    [settingsButton setImage:[TeamieUIGlobals defaultPicForPurpose:type
                                                              withSize:DEFAULT_SIZE
                                                              andColor:[TeamieUIGlobalColors primaryGreenColor]]
                        forState:UIControlStateSelected];
    return settingsButton;
}

- (void)statusMessage:(NSString *)message {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.superView addSubview:hud];
    hud.animationType = MBProgressHUDAnimationFade;
    hud.detailsLabelText = message;
    hud.mode = MBProgressHUDModeText;
    [hud show:YES];
    [hud hide:YES afterDelay:1];
}

- (void)errorMessage:(NSString *)message {
    MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self.view];
    [self.superView addSubview:hud];
    hud.animationType = MBProgressHUDAnimationFade;
    hud.labelText = @"Error";
    hud.detailsLabelText = message;
    hud.mode = MBProgressHUDModeText;
    [hud show:YES];
    [hud hide:YES afterDelay:2];
}


#pragma mark - Media Helpers

- (BOOL)deviceHasCamera {
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
}

- (BOOL)deviceHasImages {
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]
    && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary]
        containsObject:(NSString *)kUTTypeImage];
}

- (BOOL)deviceHasVideos {
    return [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]
    && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary]
        containsObject:(NSString *)kUTTypeMovie];
}

- (void)detectURLAttachments {
    [self.postTextView endEditing:YES];
    NSDataDetector *detect = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink error:nil];
    NSArray *matches = [detect matchesInString:self.viewModel.postContentText
                                       options:0
                                         range:NSMakeRange(0, self.viewModel.postContentText.length)];
    for (NSTextCheckingResult *result in matches) {
        NSString *urlString = [[result URL] absoluteString];
        if (![self.links containsObject:urlString] && self.viewModel.attachments.count < MAX_ATTACHMENTS) {
            [self.links addObject:urlString];
            TMEPostCreateLinkAttachment *attachment = [TMEPostCreateLinkAttachment new];
            attachment.url = urlString;
            [[self.viewModel mutableArrayValueForKey:@"attachments"] addObject:attachment];
            TMETrackEvent(@"post_share", @"add_link_attachment", nil);
        }
    }
}

- (void)createImageAttachment:(NSURL *)url withImage:(UIImage *)image {
    @weakify(self)
    [self.assetsLibrary assetForURL:url resultBlock:^(ALAsset *asset) {
        ALAssetRepresentation *mediaRep = [asset defaultRepresentation];
        TMEPostCreateImageAttachment *attachment = [TMEPostCreateImageAttachment new];
        attachment.image = image;
        attachment.name = mediaRep.filename;
        attachment.fileSize = @(mediaRep.size);
        attachment.fileMime = (__bridge_transfer NSString*)UTTypeCopyPreferredTagWithClass ((__bridge CFStringRef)mediaRep.UTI, kUTTagClassMIMEType);
        attachment.fileURL = url;
        
        @strongify(self)
        self.imageAttached = YES;
        if ([self.mediaTitles containsObject:attachment.title]) {
            [self statusMessage:@"Already added!"];
        }
        else {
            [self.mediaTitles addObject:attachment.title];
            //Force it to fire KVO
            [[self.viewModel mutableArrayValueForKey:@"attachments"] addObject:attachment];
            TMETrackEvent(@"post_share", @"add_image_attachment", nil);
        }
    } failureBlock:^(NSError *error) {
        @strongify(self)
        [TeamieGlobals addTSMessageInController:self
                                          title:@"Attachment Failure"
                                        message:@"There was an error attaching the selected image!"
                                           type:@"error"
                                       duration:0
                                   withCallback:nil];
    }];
}

- (void)createVideoAttachmentWithAsset:(NSURL *)assetURL andfile:(NSURL *)fileURL {
    @weakify(self)
    [self.assetsLibrary assetForURL:assetURL resultBlock:^(ALAsset *asset) {
        ALAssetRepresentation *mediaRep = [asset defaultRepresentation];
        TMEPostCreateVideoAttachment *attachment = [TMEPostCreateVideoAttachment new];

        attachment.name = mediaRep.filename;
        attachment.fileSize = @(mediaRep.size);
        attachment.fileMime = (__bridge_transfer NSString *)UTTypeCopyPreferredTagWithClass((__bridge CFStringRef)mediaRep.UTI, kUTTagClassMIMEType);
        attachment.fileURL = fileURL;
        
        @strongify(self)
        if ([self.mediaTitles containsObject:attachment.title]) {
            [self statusMessage:@"Already added!"];
        }
        else {
            [self.mediaTitles addObject:attachment.title];
            //Force it to fire KVO
            [[self.viewModel mutableArrayValueForKey:@"attachments"] addObject:attachment];
            TMETrackEvent(@"post_share", @"add_video_attachment", nil);
        }
    } failureBlock:^(NSError *error) {
        @strongify(self)
        [self errorMessage:@"There was an error attaching the selected video!"];
    }];
}

- (void)createAudioAttachmentWithFileURL:(NSURL *)fileURL andTime:(CGFloat)time {
    TMEPostCreateAudioAttachment *attachment = [TMEPostCreateAudioAttachment new];
    NSInteger seconds = (int)time%60;
    NSInteger minutes = time/60;
    
    NSPredicate *predicate  = [NSPredicate predicateWithFormat:@"SELF.fileURL.absoluteString CONTAINS[cd] %@", @"AudioRecording"];
    NSArray *filteredArray = [self.viewModel.attachments filteredArrayUsingPredicate:predicate];
    
    if (minutes == 0) {
        attachment.name = [NSString stringWithFormat:@"Audio Recording %lu: %lds", filteredArray.count + 1, (long)seconds];
    } else {
        attachment.name = [NSString stringWithFormat:@"Audio Recording %lu: %ldm %lds", filteredArray.count + 1, (long)minutes, (long)seconds];
    }
    attachment.fileMime = @"audio/mp4";
    attachment.fileURL = fileURL;
    
    [self.mediaTitles addObject:attachment.title];
    //Force it to fire KVO
    [[self.viewModel mutableArrayValueForKey:@"attachments"] addObject:attachment];
    TMETrackEvent(@"post_share", @"add_audio_attachment", nil);
}

- (void)showPicker {
    [self presentViewController:self.imagePicker animated:YES completion:nil];
}

#pragma mark - Validation

- (BOOL)validateSend {
    if (self.viewModel.postContentText.length == 0) {
        if(self.viewModel.attachments.count == 0){
            [self errorMessage:@"Post message cannot be empty!"];
            return NO;
        }
        else{
            return YES;
        }
    }
    if (self.viewModel.selectedClassrooms.count == 0) {
        [self errorMessage:@"Please select a classroom!"];
        return NO;
    }
    for (TMEClassroom *classroom in self.viewModel.selectedClassrooms) {
        if (![classroom.permissions[@"create thought"] boolValue]) {
            [self errorMessage:[NSString stringWithFormat:@"You do not have permission to post in %@", classroom.name]];
            return NO;
        }
    }
    if (self.viewModel.anonymous) {
        for (TMEClassroom *classroom in self.viewModel.selectedClassrooms) {
            if (![classroom.permissions[@"create thought anonymously"] boolValue]) {
                [self errorMessage:[NSString stringWithFormat:@"You cannot post anonymously in %@", classroom.name]];
                return NO;
            }
        }
    }
    if (self.viewModel.announcement) {
        for (TMEClassroom *classroom in self.viewModel.selectedClassrooms) {
            if (![classroom.permissions[@"send thought as announcement"] boolValue]) {
                [self errorMessage:[NSString stringWithFormat:@"You cannot post an announcement in %@", classroom.name]];
                return NO;
            }
        }
        if (!self.viewModel.notificationAnnouncement && !self.viewModel.emailAnnouncement) {
            [self errorMessage:@"Please choose atleast one announcement mode!"];
        }
    }
    if (self.viewModel.restrictComments) {
        for (TMEClassroom *classroom in self.viewModel.selectedClassrooms) {
            if (![classroom.permissions[@"limit access to thought comments"] boolValue]) {
                [self errorMessage:[NSString stringWithFormat:@"You cannot restrict comments in %@", classroom.name]];
                return NO;
            }
        }
    }
    
    return YES;
}

#pragma mark - Network Request

- (void)makeAWSRequests {
    self.statusHUD.labelText = @"Uploading attachments...";
    NSDictionary *bucketDetails = [[NSUserDefaults standardUserDefaults] dictionaryForKey:S3_KEY];
    
    AWSCognitoCredentialsProvider *credentialsProvider = [AWSCognitoCredentialsProvider
                                                          credentialsWithRegionType:AWSRegionUSEast1
                                                          accountId:@"529176168733"
                                                          identityPoolId:@"us-east-1:a010b45d-3108-4102-809b-6b1c02ccb9a7"
                                                          unauthRoleArn:@"arn:aws:iam::529176168733:role/Cognito_teamiemobileappsUnauth_DefaultRole"
                                                          authRoleArn:@"arn:aws:iam::529176168733:role/Cognito_teamiemobileappsAuth_DefaultRole"];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = [AWSServiceConfiguration configurationWithRegion:AWSRegionAPSoutheast1
                                                                                                         credentialsProvider:credentialsProvider];
    
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
    for (TMEPostCreateAttachment *attachment in self.viewModel.attachments) {
        if ([attachment isKindOfClass:TMEPostCreateVideoAttachment.class] || [attachment isKindOfClass:TMEPostCreateAudioAttachment.class]) {
            TMEPostCreateMediaAttachment *media = (TMEPostCreateMediaAttachment *)attachment;
            NSString *path = bucketDetails[@"directories"][@"thought_video_attachment"];
            if ([attachment isKindOfClass:TMEPostCreateAudioAttachment.class]) {
                path = bucketDetails[@"directories"][@"thought_audio_attachment"];
            }
            //Remove leading slash if any
            if ([path hasPrefix:@"/"] && path.length > 1) {
                path = [path substringFromIndex:1];
            }
            
            NSString *requestKey = [NSString stringWithFormat:@"%@%@", path, media.hashedName];
            
            AWSS3TransferManagerUploadRequest *request = [AWSS3TransferManagerUploadRequest new];
            request.bucket = bucketDetails[@"bucket_name"];
            request.key = requestKey;
            request.body = media.fileURL;
            request.ACL = AWSS3BucketCannedACLPublicRead;
            
            media.url = [NSString stringWithFormat:@"s3://%@", requestKey];
            
            [self.mediaUploads addObject:[transferManager upload:request]];
        }
    }
    if (self.mediaUploads.count > 0) {
        [[BFTask taskForCompletionOfAllTasks:self.mediaUploads]
         continueWithExecutor:[BFExecutor mainThreadExecutor]
         withBlock:^id(BFTask *task) {
             if (task.error != nil) {
                 NSLog(@"%@", task.error);
             }
             else {
                 [self uploadImage];
             }
             return nil;
         }];
    }
    else {
        [self uploadImage];
    }
}

- (void)uploadImage {
    if (self.imageAttached) {
        for (TMEPostCreateAttachment *attachment in self.viewModel.attachments) {
            if ([attachment isKindOfClass:TMEPostCreateImageAttachment.class]) {
                TMEPostCreateImageAttachment *imageAttachment = (TMEPostCreateImageAttachment *)attachment;
                NSData *newImageData = [UIImageJPEGRepresentation(imageAttachment.image, 0.5)
                                        base64EncodedDataWithOptions:NSDataBase64Encoding76CharacterLineLength];
                NSString *imageDataBase64String = [NSString stringWithUTF8String:[newImageData bytes]];
                [[TMEClient sharedClient]
                 request:TeamieUserFileUploadRequest
                 parameters:@{@"file[filename]": imageAttachment.hashedName,
                              @"file[file]": imageDataBase64String}
                 loadingMessage:nil
                 success:^(NSDictionary *response) {
                     imageAttachment.url = [NSString stringWithFormat:@"public://%@", imageAttachment.hashedName];
                     imageAttachment.fid = @([(NSString *)response[@"fid"] integerValue]);
                     [self post];
                 } failure:^(NSError *error) {
                     [self.statusHUD hide:YES];
                     if (![error.userInfo[TMEJSONResponseErrorKey] isEqual:[NSNull null]]) {
                         [self errorMessage:error.userInfo[TMEJSONResponseErrorKey][@"error_description"]?:TMELocalize(@"error.internal-error")];
                     }
                     else {
                         [self errorMessage:TMELocalize(@"error.internal-error")];
                     }
                 }];
                break;
            }
        }
    }
    else {
        [self post];
    }
}

- (void)post {
    self.navigationItem.rightBarButtonItem.enabled = NO;
    self.statusHUD.labelText = @"Posting ...";
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"nids"] = [[self.viewModel.selectedClassrooms.rac_sequence map:^NSNumber *(TMEClassroom *classroom) {
        return classroom.nid;
    }] array];
    switch (self.viewModel.postType) {
        case TMEPostThought:
            parameters[@"type"] = @"thought";
            break;
        case TMEPostQuestion:
            parameters[@"type"] = @"question";
            break;
        case TMEPostHomework:
            parameters[@"type"] = @"homework";
            break;
    }
    parameters[@"message"] = self.viewModel.postContentText;
    parameters[@"is_announcement"] = @(self.viewModel.announcement);
    if (self.viewModel.announcement) {
        NSMutableArray *sendMethods = [NSMutableArray new];
        if (self.viewModel.notificationAnnouncement) {
            [sendMethods addObject:@"notification"];
        }
        if (self.viewModel.emailAnnouncement) {
            [sendMethods addObject:@"mail"];
        }
        parameters[@"announcement_send_methods"] = sendMethods;
    }
    parameters[@"is_anonymous"] = @(self.viewModel.anonymous);
    parameters[@"is_comments_restricted"] = @(self.viewModel.restrictComments);
    if (self.viewModel.postType == TMEPostHomework) {
        parameters[@"deadline"] = @((long long)[self.viewModel.deadline timeIntervalSince1970]);
        parameters[@"is_all_day"] = @(self.viewModel.allDay);
    }
    if (self.viewModel.attachments.count > 0) {
        NSMutableArray *attachmentTypes = [NSMutableArray new];
        NSArray *videos = [[[self.viewModel.attachments.rac_sequence filter:^BOOL(TMEPostCreateAttachment *attachment) {
            return [attachment isKindOfClass:TMEPostCreateVideoAttachment.class];
        }] map:^TMEPostCreateVideoAttachment *(TMEPostCreateAttachment *value) {
            return (TMEPostCreateVideoAttachment *)value;
        }] array];
        NSArray *audios = [[[self.viewModel.attachments.rac_sequence filter:^BOOL(TMEPostCreateAttachment *attachment) {
            return [attachment isKindOfClass:TMEPostCreateAudioAttachment.class];
        }] map:^TMEPostCreateAudioAttachment *(TMEPostCreateAttachment *value) {
            return (TMEPostCreateAudioAttachment *)value;
        }] array];
        if (videos.count > 0) {
            NSMutableDictionary *videoDict = [NSMutableDictionary new];
            videoDict[@"type"] = @"video";
            videoDict[@"data"] = [MTLJSONAdapter JSONArrayFromModels:videos];
            [attachmentTypes addObject:videoDict];
        }
        if (audios.count > 0) {
            NSMutableDictionary *audioDict = [NSMutableDictionary new];
            audioDict[@"type"] = @"audio";
            audioDict[@"data"] = [MTLJSONAdapter JSONArrayFromModels:audios];
            [attachmentTypes addObject:audioDict];
        }
        NSArray *images = [[[self.viewModel.attachments.rac_sequence filter:^BOOL(TMEPostCreateAttachment *attachment) {
            return [attachment isKindOfClass:TMEPostCreateImageAttachment.class];
        }] map:^TMEPostCreateImageAttachment *(TMEPostCreateAttachment *value) {
            return (TMEPostCreateImageAttachment *)value;
        }] array];
        if (images.count > 0) {
            NSMutableDictionary *imageDict = [NSMutableDictionary new];
            imageDict[@"type"] = @"image";
            imageDict[@"data"] = [MTLJSONAdapter JSONArrayFromModels:images];
            [attachmentTypes addObject:imageDict];
        }
        NSArray *links = [[[self.viewModel.attachments.rac_sequence filter:^BOOL(TMEPostCreateAttachment *attachment) {
            return [attachment isKindOfClass:TMEPostCreateLinkAttachment.class];
        }] map:^TMEPostCreateLinkAttachment *(TMEPostCreateAttachment *value) {
            return (TMEPostCreateLinkAttachment *)value;
        }] array];
        if (links.count > 0) {
            NSMutableDictionary *linkDict = [NSMutableDictionary new];
            linkDict[@"type"] = @"link";
            linkDict[@"data"] = [MTLJSONAdapter JSONArrayFromModels:links];
            [attachmentTypes addObject:linkDict];
        }
        
        //Convert to JSON String
        NSError *error;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:attachmentTypes
                                                           options:0
                                                             error:&error];
        parameters[@"attachments"] = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    
    [self trackPostDetails:parameters];
    
    [[TMEClient sharedClient]
     request:TeamieUserPostShareRequest
     parameters:parameters
     loadingMessage:nil
     success:^(id response) {
         self.statusHUD.mode = MBProgressHUDModeText;
         self.statusHUD.labelText = @"Post success!";
         [self.statusHUD hide:YES afterDelay:2];
         TMETrackEvent(@"post_share", @"create_success", @"post");
         
         //Close after a couple of seconds
         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2 * NSEC_PER_SEC),
                        dispatch_get_main_queue(),
                        ^{
                            if ([self.navigationController.viewControllers[self.navigationController.viewControllers.count - 2] isKindOfClass:TMENewsfeedViewController.class]) {
                              [self.presentingNewsfeed refresh];
                            }
                            [self.navigationController popViewControllerAnimated:YES];
                        });
     } failure:^(NSError *error) {
         [self.statusHUD hide:YES];
         if (![error.userInfo[TMEJSONResponseErrorKey] isEqual:[NSNull null]]) {
             [self errorMessage:error.userInfo[TMEJSONResponseErrorKey][@"error_description"]?:TMELocalize(@"error.internal-error")];
         }
         else {
             [self errorMessage:TMELocalize(@"error.internal-error")];
         }
            self.navigationItem.rightBarButtonItem.enabled = YES;
     }];
}

#pragma - Google Analytics Tracking
- (void)trackPostDetails:(NSDictionary *)parameters {
    if (self.viewModel.anonymous) {
        TMETrackEvent(@"post_share", @"set_anonymous", @"post");
    }
    
    if (self.viewModel.announcement) {
        if (self.viewModel.notificationAnnouncement && self.viewModel.emailAnnouncement) {
            TMETrackEvent(@"post_share", @"set_announcement", @"both");
        }
        else if (self.viewModel.notificationAnnouncement) {
            TMETrackEvent(@"post_share", @"set_announcement", @"notification");
        }
        else if (self.viewModel.emailAnnouncement) {
            TMETrackEvent(@"post_share", @"set_announcement", @"email");
        }
    }
    
    if (self.viewModel.restrictComments) {
        TMETrackEvent(@"post_share", @"set_comment_restriction", nil);
    }
    TMETrackEvent(@"post_share", @"set_post_type", parameters[@"type"]);
    TMETrackEvent(@"post_share", @"tap_send", @"post");
}

@end
