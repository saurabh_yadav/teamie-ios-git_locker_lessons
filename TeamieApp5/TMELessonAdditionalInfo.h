//
//  AdditionalInfoField.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 10/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"

@interface TMELessonAdditionalInfo : TMEModel

@property (nonatomic,retain) NSNumber *tid;
@property (nonatomic,retain) NSString *title;
@property (nonatomic,retain) NSString *label;

@end
