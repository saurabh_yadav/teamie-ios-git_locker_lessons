//
//  QuizSubmissionsViewController.m
//  TeamieApp5
//
//  Created by Raunak on 4/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuizSubmissionsViewController.h"
#import "QuestionPageViewController.h"
#import "SubmissionAnswer.h"
#import "SubmissionDetails.h"
#import "QuizStatistics.h"
#import "QuizSubmissions.h"
#import "QuizStatsViewController.h"
#import "QuizDefaultersListViewController.h"
#import <TapkuLibrary/TKEmptyView.h>

@interface QuizSubmissionsViewController () {
    QuizSortType selectedSortType;
    QuizSortOrder selectedSortOrder;
    NSInteger selectedQuestionsCount;
    NSMutableArray* barButtons;
    BOOL shouldLoadDefaulters;
}
@property (nonatomic,strong) CollapseClickView* myCollapseClickView;
@property (nonatomic,strong) UIToolbar* optionsBar;
@property (nonatomic,strong) UIBarButtonItem* filterButton;
@property (nonatomic,strong) UISegmentedControl* segmentControl;
@property (nonatomic,strong) QuestionFilterTableViewController* filterVC;
@property (nonatomic,strong) UIPopoverController* filterPopover;
@property (nonatomic,strong) UIPopoverController* sortPopover;
@property (nonatomic,strong) NSMutableArray* submissionsVC;
@property (nonatomic,strong) QuizSubmissions* quizSubmissionsObject;
@property (nonatomic,strong) QuizSubmissions* savedSubmissionsObject;
@end

@implementation QuizSubmissionsViewController

@synthesize myCollapseClickView;

#pragma mark - View Lifecycle Methods

-(id)initWithQID:(NSNumber*)qid andTitle:(NSString *)title {
    self = [super init];
    if (self) {
        _qid = qid;
        _quizTitle = title;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.qid) {
        [self.apiCaller performRestRequest:TeamieUserQuizSubmissionRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:self.qid, @"quiz_id", @"10", @"items_per_page", nil]];

        
        dispatch_queue_t requestQueue = dispatch_queue_create("restRequestQueue",NULL);
        dispatch_async(requestQueue, ^{
            [self.apiCaller performRestRequest:TeamieUserQuizDefaultersList withParams:[NSDictionary dictionaryWithObjectsAndKeys:@"defaulters", @"type", self.qid, @"quiz_id", nil]];
        });

        if (!self.quizTitle) {
            self.quizTitle = TeamieLocalizedString(@"$quiz[s]$ Submissions", nil);
        }
        self.navigationItem.title = self.quizTitle;
        self.view.backgroundColor = [UIColor whiteColor];
        
        shouldLoadDefaulters = NO;
        barButtons = [[NSMutableArray alloc] init];
        
        self.filterButton = [[UIBarButtonItem alloc] initWithTitle:@"Filter" style:UIBarButtonItemStyleBordered target:self action:@selector(filterButtonPressed:)];
        UIBarButtonItem* sortButton = [[UIBarButtonItem alloc] initWithTitle:@"Sort" style:UIBarButtonItemStyleBordered target:self action:@selector(sortButtonPressed:)];
        UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem* segment = [[UIBarButtonItem alloc] initWithCustomView:self.segmentControl];
        self.optionsBar.items = [NSArray arrayWithObjects:segment, flexibleSpace, sortButton, self.filterButton, nil];
        
        self.filterVC = [[QuestionFilterTableViewController alloc] init];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userFilterChanged:) name:@"UserFilterChanged" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionFilterChanged:) name:@"QuestionFilterChanged" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userScoreChanged:) name:@"UserScoreChanged" object:nil];
        selectedSortType = kSubmissionTime;
        selectedSortOrder = kAscending;
    }
    else {
        //Display empty view
        TKEmptyView* view = [[TKEmptyView alloc] initWithFrame:self.view.frame emptyViewImage:TKEmptyViewImageSearch title:TeamieLocalizedString(@"Unable to load $quiz[s]$ Submissions", nil) subtitle:@""];
        [self.view addSubview:view];
    }
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TeamieGlobals addHUDTextOnlyLabel:@"Loading" margin:10.0 yOffset:10.0];
}

- (void)viewDidUnload {
    [self setMyCollapseClickView:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UserFilterChanged" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"QuestionFilterChanged" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"UserScoreChanged" object:nil];
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [TeamieGlobals cancelTeamieRestRequests];
    [super viewWillDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - REST Api Methods

- (void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {

    if (requestName == TeamieUserQuizSubmissionRequest) {
        
        if ([response isKindOfClass:[NSArray class]] && [response count]) {
            // Check if self.quizSubmissionsObject is already populated. If yes, then replace only stats and add on to existing details.
            
            if (!self.quizSubmissionsObject) {
                self.quizSubmissionsObject = [response objectAtIndex:0];
            }
            else if ([[response objectAtIndex:0] isKindOfClass:[QuizSubmissions class]]) {
                self.quizSubmissionsObject.stats = ((QuizSubmissions*)[response objectAtIndex:0]).stats;
                self.quizSubmissionsObject.details = [self.quizSubmissionsObject.details arrayByAddingObjectsFromArray:((QuizSubmissions*)[response objectAtIndex:0]).details];
            }
            else {  // This else block should never get executed. Could imply there's something wrong with mapping.
                return;
            }
            
            if (self.quizSubmissionsObject.questions != nil && self.quizSubmissionsObject.stats != nil && self.quizSubmissionsObject.details != nil) {
                // Clear out the submissions views out there currently.
                self.submissionsVC = [[NSMutableArray alloc] init];
                
                for (SubmissionDetails* details in self.quizSubmissionsObject.details) {
                    QuestionPageViewController* qpVC = [[QuestionPageViewController alloc] initWithUserSubmission:details forQuestions:self.quizSubmissionsObject.questions quizID:self.qid];
                    [self.submissionsVC addObject:qpVC];
                }
                
                if ([self.submissionsVC count] > 1) {
                    [self.view addSubview:self.optionsBar];
                }
                
                UILabel* loadedItems = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 15)];
                loadedItems.text = [NSString stringWithFormat:@"%d/%@", [self.submissionsVC count], self.quizSubmissionsObject.stats.totalSubmissions];
                [loadedItems setTextColor:[UIColor whiteColor]];
                [loadedItems setBackgroundColor:[UIColor clearColor]];
                UIBarButtonItem* loadedItemsButton = [[UIBarButtonItem alloc] initWithCustomView:loadedItems];
                UIBarButtonItem* statButton = [[UIBarButtonItem alloc] initWithTitle:@"Stats" style:UIBarButtonItemStyleBordered target:self action:@selector(statButtonPressed:)];
                UIBarButtonItem* defaultersButton = [[UIBarButtonItem alloc] initWithTitle:@"Defaulters" style:UIBarButtonItemStyleBordered target:self action:@selector(defaulterButtonPressed:)];
                
                UIActivityIndicatorView *ai = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
                UIBarButtonItem* activityIndicator = [[UIBarButtonItem alloc] initWithCustomView:ai];
                [ai startAnimating];
                
                barButtons = [NSMutableArray arrayWithObjects:activityIndicator, loadedItemsButton, statButton, nil];
                if (shouldLoadDefaulters) {
                    [barButtons addObject:defaultersButton];
                }
                
                self.navigationItem.rightBarButtonItems = barButtons;
                selectedQuestionsCount = [self.quizSubmissionsObject.questions count];
                self.filterVC.questionPickerList = [self getDistinctQuestions];
                self.filterVC.userPickerList = [self getDistinctUsers];
                [self.view addSubview:self.myCollapseClickView];
                [self sortSubmissions];
                //            [self.view setNeedsDisplay];
                //            [self.view bringSubviewToFront:self.myCollapseClickView];
                
                if ([self.submissionsVC count] < [self.quizSubmissionsObject.stats.totalSubmissions intValue] && [self.submissionsVC count]%10==0) {
                    dispatch_queue_t requestQueue = dispatch_queue_create("restRequestQueue",NULL);
                    dispatch_async(requestQueue, ^{
                        [self.apiCaller performRestRequest:TeamieUserQuizSubmissionRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:self.qid, @"quiz_id", [NSString stringWithFormat:@"%d", ([self.submissionsVC count]/10)], @"page", @"10", @"items_per_page", nil]];
                    });
                }
                
                if ([self.submissionsVC count] >= [self.quizSubmissionsObject.stats.totalSubmissions intValue] || [self.submissionsVC count] % 10 != 0) {
                    [ai stopAnimating];
                    [ai removeFromSuperview];
                }
                
            }
            else {
                [self requestFailed:@"Invalid Response" forRequest:requestName];
            }
        }
        else {
            [self requestFailed:@"Invalid Response" forRequest:requestName];
        }
    }

    if (requestName == TeamieUserQuizDefaultersList) {
        shouldLoadDefaulters = YES;
    }
}

- (void)requestFailed:(NSString *)message forRequest:(TeamieRESTRequest)requestName {
    [TeamieGlobals dismissActivityLabels];
    TKEmptyView* view = [[TKEmptyView alloc] initWithFrame:self.view.frame emptyViewImage:TKEmptyViewImageSearch title:TeamieLocalizedString(@"Unable to load $quiz[s]$ Submissions", nil) subtitle:@""];
    [self.view addSubview:view];
}

#pragma mark - QuizSortController Delegate Methods

- (void)quizSortControllerDidSelectType:(QuizSortType)type order:(QuizSortOrder)order {
    [TeamieGlobals addHUDTextOnlyLabel:@"Loading" margin:10.0 yOffset:10.0];
    selectedSortType = type;
    selectedSortOrder = order;
    [self performSelector:@selector(updateUserInterface) withObject:nil afterDelay:0.5];
}

#pragma mark - QuizFilterController Delegate Methods

- (void)userFilterChanged:(NSNotification*)notification {
    NSMutableArray* userVC = [NSMutableArray array];
    NSArray* selectedUIDs = [[notification userInfo] objectForKey:@"filterArray"];
    if ([selectedUIDs count] != [self.quizSubmissionsObject.details count]) {
        [self.filterButton setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor yellowColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
        self.segmentControl.selectedSegmentIndex = 0;
    }
    else if ([selectedUIDs count] == [self.quizSubmissionsObject.details count] && selectedQuestionsCount == [self.quizSubmissionsObject.questions count]) {
        [self.filterButton setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
    }
    for (SubmissionDetails* details in self.quizSubmissionsObject.details) {
        if ([selectedUIDs containsObject:details.user.uid]) {
            QuestionPageViewController* qpVC2 = [[QuestionPageViewController alloc] initWithUserSubmission:details forAllQuestions:self.quizSubmissionsObject.questions andFilteredQuestions:[self.filterVC getSelectedQIDs] quizID:self.qid];
            [userVC addObject:qpVC2];
        }
    }
    self.submissionsVC = nil;
    self.submissionsVC = userVC;
    [self sortSubmissions];
}

- (void)questionFilterChanged:(NSNotification*)notification {
    NSArray* selectedQIDs = [[notification userInfo] objectForKey:@"filterArray"];
    for (QuestionPageViewController* qpVC in self.submissionsVC) {
        qpVC.filteredQuestions = selectedQIDs;
    }
    selectedQuestionsCount = [selectedQIDs count];
    if ([selectedQIDs count] != [self.quizSubmissionsObject.questions count]) {
        [self.filterButton setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor yellowColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
    }
    else if ([selectedQIDs count] == [self.quizSubmissionsObject.questions count] && [[self.filterVC getSelectedUIDs] count] == [self.quizSubmissionsObject.details count])  {
        [self.filterButton setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
    }
    [TeamieGlobals dismissActivityLabels];
}

- (void)updateUserInterface {
    [self sortSubmissions];
}

- (void)userScoreChangedWithResponse:(QuizSubmissions *)newSubmissionObject {
    self.savedSubmissionsObject = newSubmissionObject;
}

- (void)viewDidExit {
    if (self.savedSubmissionsObject) {
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING", nil) margin:10.0 yOffset:10.0];
        
        self.quizSubmissionsObject.stats = self.savedSubmissionsObject.stats;
        NSMutableArray* newDetailsArray = [NSMutableArray array];

        SubmissionDetails* newDetails = [self.savedSubmissionsObject.details objectAtIndex:0];
        for (SubmissionDetails* details in self.quizSubmissionsObject.details) {
            if ([newDetails.user.uid isEqualToNumber:details.user.uid]) {
                [newDetailsArray addObject:newDetails];
            }
            else {
                [newDetailsArray addObject:details];
            }
        }
        
        self.quizSubmissionsObject.details = newDetailsArray;
        self.submissionsVC = [[NSMutableArray alloc] init];
        
        for (SubmissionDetails* details in self.quizSubmissionsObject.details) {
            QuestionPageViewController* qpVC = [[QuestionPageViewController alloc] initWithUserSubmission:details forQuestions:self.quizSubmissionsObject.questions quizID:self.qid];
            [self.submissionsVC addObject:qpVC];
        }
        self.savedSubmissionsObject = nil;
        [self performSelector:@selector(updateUserInterface) withObject:nil afterDelay:0.5];
    }
}

#pragma mark - UIPopoverController Delegate Methods

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    if ([popoverController isEqual:self.filterPopover]) {
        self.filterPopover = nil;
    }
    else if ([popoverController isEqual:self.sortPopover]) {
        self.sortPopover = nil;
    }
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    return YES;
}

#pragma mark - Private Methods

- (NSArray*)getDistinctQuestions {
    NSMutableArray* arr = [NSMutableArray array];
    for (SubmissionQuestion* question in self.quizSubmissionsObject.questions) {
        [arr addObject:[NSNumber numberWithInt:[self.quizSubmissionsObject.questions indexOfObject:question] + 1]];
    }
    return arr;
}

- (NSDictionary*)getDistinctUsers {
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    for (QuestionPageViewController* qpVC in self.submissionsVC) {
        SubmissionUser* user = qpVC.userDetails.user;
        [dict setObject:user.realName forKey:user.uid];
    }
    return dict;
}

- (void)statButtonPressed:(id)sender {
    QuizStatsViewController  * qsVC = [[QuizStatsViewController alloc] initWithQuizStatistics:self.quizSubmissionsObject.stats andTitle:self.quizTitle];
    qsVC.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentModalViewController:qsVC animated:YES];
}

- (void)defaulterButtonPressed:(id)sender {
    QuizDefaultersListViewController* qdVC = [[QuizDefaultersListViewController alloc] initWithQuizId:_qid];
    qdVC.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentModalViewController:qdVC animated:YES];
}

- (void)sortButtonPressed:(id)sender {
    if (self.filterPopover) {
        [self.filterPopover dismissPopoverAnimated:NO];
        self.filterPopover = nil;
    }
    
    UIBarButtonItem* button = sender;
    if (!self.sortPopover) {
        QuizSubmissionSortViewController* sortVC = [[QuizSubmissionSortViewController alloc] init];
        sortVC.selectedType = selectedSortType;
        sortVC.selectedOrder = selectedSortOrder;
        sortVC.delegate = self;
        self.sortPopover = [[UIPopoverController alloc] initWithContentViewController:sortVC];
        self.sortPopover.delegate = self;
        [self.sortPopover presentPopoverFromBarButtonItem:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else {
        [self.sortPopover dismissPopoverAnimated:YES];
        self.sortPopover = nil;
    }
}

- (void)filterButtonPressed:(id)sender {
    UIBarButtonItem* button = sender;
    
    if (self.sortPopover) {
        [self.sortPopover dismissPopoverAnimated:NO];
        self.sortPopover = nil;
    }
    
    if (!self.filterPopover) {
        self.filterPopover = [[UIPopoverController alloc] initWithContentViewController:self.filterVC];
        self.filterPopover.delegate = self;
        [self.filterPopover presentPopoverFromBarButtonItem:button permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else {
        [self.filterPopover dismissPopoverAnimated:YES];
        self.filterPopover = nil;
    }
}

- (void)segmentValueChanged:(id)sender {
    [TeamieGlobals addHUDTextOnlyLabel:@"Loading" margin:10.0 yOffset:10.0];
    NSMutableArray* userVC = [NSMutableArray array];
    switch (self.segmentControl.selectedSegmentIndex) {
        case 0:
            for (SubmissionDetails* details in self.quizSubmissionsObject.details) {                    QuestionPageViewController* qpVC2 = [[QuestionPageViewController alloc] initWithUserSubmission:details forAllQuestions:self.quizSubmissionsObject.questions andFilteredQuestions:[self.filterVC getSelectedQIDs] quizID:self.qid];
                [userVC addObject:qpVC2];
            }
            break;
        case 1:
            for (SubmissionDetails* details in self.quizSubmissionsObject.details) {
                if (details.isGraded) {
                    QuestionPageViewController* qpVC2 = [[QuestionPageViewController alloc] initWithUserSubmission:details forAllQuestions:self.quizSubmissionsObject.questions andFilteredQuestions:[self.filterVC getSelectedQIDs] quizID:self.qid];
                    [userVC addObject:qpVC2];
                }
            }
            [self.filterVC selectAllUsers];
            if (selectedQuestionsCount == [self.quizSubmissionsObject.questions count]) {
                [self.filterButton setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
            }
            break;
        case 2:
            for (SubmissionDetails* details in self.quizSubmissionsObject.details) {
                if (!details.isGraded) {
                    QuestionPageViewController* qpVC2 = [[QuestionPageViewController alloc] initWithUserSubmission:details forAllQuestions:self.quizSubmissionsObject.questions andFilteredQuestions:[self.filterVC getSelectedQIDs] quizID:self.qid];
                    [userVC addObject:qpVC2];
                }
            }
            [self.filterVC selectAllUsers];
            if (selectedQuestionsCount == [self.quizSubmissionsObject.questions count]) {
                [self.filterButton setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
            }
            break;
        case 3:
            for (SubmissionDetails* details in self.quizSubmissionsObject.details) {
                if (details.isDelayed) {
                    QuestionPageViewController* qpVC2 = [[QuestionPageViewController alloc] initWithUserSubmission:details forAllQuestions:self.quizSubmissionsObject.questions andFilteredQuestions:[self.filterVC getSelectedQIDs] quizID:self.qid];
                    [userVC addObject:qpVC2];
                }
            }
            [self.filterVC selectAllUsers];
            if (selectedQuestionsCount == [self.quizSubmissionsObject.questions count]) {
                [self.filterButton setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:UITextAttributeTextColor] forState:UIControlStateNormal];
            }
            break;
        default:
            break;
    }
    self.submissionsVC = nil;
    self.submissionsVC = userVC;
    [self performSelector:@selector(updateUserInterface) withObject:nil afterDelay:0.5];
}

- (void)sortSubmissions {
    for (QuestionPageViewController* qpVC in self.submissionsVC) {
        [self addChildViewController:qpVC];
        [qpVC didMoveToParentViewController:self];
    }
    switch (selectedSortType) {
        case kName:
            if (selectedSortOrder == kAscending) {
                [self.submissionsVC sortUsingComparator:^NSComparisonResult (id obj1, id obj2) {
                    return [((QuestionPageViewController*)obj1).userDetails.user.realName localizedCaseInsensitiveCompare:((QuestionPageViewController*)obj2).userDetails.user.realName];
                }];
            }
            else {
                [self.submissionsVC sortUsingComparator:^NSComparisonResult (id obj1, id obj2) {
                    return [((QuestionPageViewController*)obj2).userDetails.user.realName localizedCaseInsensitiveCompare:((QuestionPageViewController*)obj1).userDetails.user.realName];
                }];
            }
            break;
        case kScore:
            if (selectedSortOrder == kAscending) {
                [self.submissionsVC sortUsingComparator:^NSComparisonResult (id obj1, id obj2) {
                    return [((QuestionPageViewController*)obj1).userDetails.totalScore compare:((QuestionPageViewController*)obj2).userDetails.totalScore];
                }];
            }
            else {
                [self.submissionsVC sortUsingComparator:^NSComparisonResult (id obj1, id obj2) {
                    return [((QuestionPageViewController*)obj2).userDetails.totalScore compare:((QuestionPageViewController*)obj1).userDetails.totalScore];
                }];
            }
            break;
        case kSubmissionTime:
            if (selectedSortOrder == kAscending) {
                [self.submissionsVC sortUsingComparator:^NSComparisonResult (id obj1, id obj2) {
                    return [((QuestionPageViewController*)obj1).userDetails.submittedTime compare:((QuestionPageViewController*)obj2).userDetails.submittedTime];
                }];
            }
            else {
                [self.submissionsVC sortUsingComparator:^NSComparisonResult (id obj1, id obj2) {
                    return [((QuestionPageViewController*)obj2).userDetails.submittedTime compare:((QuestionPageViewController*)obj1).userDetails.submittedTime];
                }];
            }
            break;
        case kCompletionTime:
            if (selectedSortOrder == kAscending) {
                [self.submissionsVC sortUsingComparator:^NSComparisonResult (id obj1, id obj2) {
                    return [((QuestionPageViewController*)obj1).userDetails.completionTime compare:((QuestionPageViewController*)obj2).userDetails.completionTime];
                }];
            }
            else {
                [self.submissionsVC sortUsingComparator:^NSComparisonResult (id obj1, id obj2) {
                    return [((QuestionPageViewController*)obj2).userDetails.completionTime compare:((QuestionPageViewController*)obj1).userDetails.completionTime];
                }];
            }
            break;
        default:
            break;
    }
    [self.myCollapseClickView reloadCollapseClick];
}

#pragma mark - Collapse Click Delegate Methods

- (int)numberOfCellsForCollapseClick {
    return [self.submissionsVC count];
}

- (QuestionPageViewController*)detailsForCollapseClickAtIndex:(int)index {
    return (QuestionPageViewController*)[self.submissionsVC objectAtIndex:index];
}

-(NSNumber*)maxScoreForCollapseClickAtIndex:(int)index {
    return self.quizSubmissionsObject.stats.totalScore;
}

- (void)didFinishLoad {
    [TeamieGlobals dismissActivityLabels];
}

#pragma mark - Lazy Instantiation Methods

- (CollapseClickView*)myCollapseClickView {
    if (!myCollapseClickView) {
        myCollapseClickView = [[CollapseClickView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y + 44.0, self.view.bounds.size.width, self.view.bounds.size.height - 44.0)];
        myCollapseClickView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        myCollapseClickView.CollapseClickDelegate = self;
    }
    return myCollapseClickView;
}

- (UIToolbar*) optionsBar {
    if (!_optionsBar) {
        _optionsBar = [[UIToolbar alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, 44.0)];
        _optionsBar.barStyle = UIBarStyleDefault;
    }
    return _optionsBar;
}

- (UISegmentedControl*)segmentControl {
    if (!_segmentControl) {
        _segmentControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"All", @"Graded", @"Not Graded", @"Late", nil]];
        _segmentControl.segmentedControlStyle = UISegmentedControlStyleBar;
        _segmentControl.selectedSegmentIndex = 0;
        [_segmentControl addTarget:self action:@selector(segmentValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentControl;
}

@end