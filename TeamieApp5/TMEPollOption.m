//
//  TMEPollOption.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 05/01/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEPollOption.h"

@implementation TMEPollOption

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"choiceID": @"choice_id",
             @"numberOfVotes": @"num_votes",
             @"currentUserVote": @"current_user_vote"
             };
}

+ (NSValueTransformer *)numberOfVotesJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

@end
