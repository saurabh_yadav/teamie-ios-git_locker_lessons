//
//  TeamieBaseClient.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "AFHTTPRequestOperationManager.h"
#import "TMEJSONResponseSerializerWithData.h"

//Abstract class, do not use without subclassing. Each subclass is for a different
//base URL
//Subclasses should define + (instancetype)sharedClient; and call
//initWithBaseURL with the baseURL they wish to use
@interface TMEBaseClient : AFHTTPRequestOperationManager

extern NSString * const TEAMIE_DEFAULT_REST_URL;
extern NSString * const TEAMIE_DEFAULT_OAUTH_REST_URL;
extern NSString * const TEAMIE_DEFAULT_API_VERSION_NUMBER;
extern NSString * const TEAMIE_DEFAULT_CLIENT;
extern NSString * const TEAMIE_OAUTH_SSO_GRANT_TYPE;
extern NSString * const TEAMIE_DEFAULT_OAUTH_GRANT_TYPE;
extern NSString * const TEAMIE_DEFAULT_OAUTH_CLIENT_ID;
extern NSString * const TEAMIE_DEFAULT_CLIENT_SITE;
extern NSString * const TEAMIE_DOMAIN_LIST_URL;
extern NSString * const TEAMIE_DOMAIN_LIST_KEY;

typedef NS_ENUM(NSInteger, TMERequest){
    TeamieUserDomainRequest,
    TeamieUserPublicSiteInfoRequest,
    TeamieUserLoginRequest,
    TeamieUserLogoutRequest,
    TeamieUserBulletinBoardCountRequest,
    TeamieUserMarkNotificationReadRequest,
    TeamieUserBulletinBoardRequest,
    TeamieUserMessagesRequest,
    TeamieNewUserProfileRequest,
    TeamieUserNewsfeedRequest,
    TeamieUserClassroomNewsfeedRequest,
    TeamieUserClassroomsRequest,
    TeamieUserSiteInfoRequest,              // this request is made after the user logs into the app
    TeamieUserDeviceRegisterRequest,
    TeamieUserDeviceUnregisterRequest,          // a request to unregsiter a device token for the user
    TeamieUserLikeThoughtRequest,               // a request to like / unlike a thought
    TeamieUserLikeThoughtCommentRequest,
    TeamieUserSameThoughtRequest,               // a request to say I hav the same question
    TeamieUserDeleteThoughtRequest,             // a request to delete a thought/question in the Newsfeed VC/Thought VC
    DeletePostCommentRequest,
    TeamieUserGetThoughtRequest,                // a request to get the details of a thought given tid
    TeamieUserPostThoughtCommentRequest,        // a request to post a comment for a given thought
    TeamieUserPostThoughtCommentReplyRequest,
    TeamieUserThoughtCommentVoteRequest,         // a request to vote up/down on a comment
    TeamieUserThoughtCommentMarkRightRequest,   // a request to mark a thought as correct
    TeamieUserGetAllThoughtCommentsRequest,     // a request to get a page of comment of a thought
    TeamieUserGetAllThoughtCommentRepliesRequest,   // a request to get a page of reply of a thought comment
    TeamieUserReportPost,
    TeamieUserReportPostComment,
    TeamieUserClassroomMenuRequest,
    TeamieUserLessonsRequest,
    TeamieUserLessonToggleStatusRequest,
    TeamieUserLessonDeleteRequest,
    TeamieUserLessonRequest,
    TeamieUserLessonUpdateRequest,
    TeamieUserLessonPageRequest,
    TeamieUserClassroomGradebookRequest,    // a request to get the gradebook of a classroom for a user
    TeamieUserMenuRequest,
    TeamieUserPostShareRequest,
    TeamieUserFileUploadRequest,
    // Requests for getting dashboard statistics
    TeamieUserCustomReportRequest,
    TeamieReportActiveClassroomsRequest,
    TeamieReportActiveStudentsRequest,
    TeamieReportLessonContributorsRequest,
    TeamieReportQuizContributorsRequest,
    TeamieReportSiteUsageRequest,
    TeamieReportMostViewedLessonsRequest,
    TeamieReportUserPointStatsRequest,
    TeamieReportUserPointsHistoryRequest,
    TeamieUserQuizzesRequest,                   // a request to get all quizzes in classroom
    TeamieUserMenuForReportsRequest,
    TeamieUserCalendarRequest,
    TeamieUserUpcomingEventsRequest,
    TeamieUserSubmitPollRequest,
    TeamieUserSeenPostRequest,
    
    //    TeamieUserPostThoughtRequest,
    //    TeamieUserMenuForNewClassroom,
    //    TeamieUserProfileRequest,
    //    TeamieUserNewsfeedFilterRequest,
    //    TeamieUserMessagesRequest,                  // a request to get all messages of current user
    //    TeamieUserMessagesCountRequest,
    //    TeamieUserMessageThreadRequest,             // a request to retrieve messages of a given message thread
    //    TeamieUserMessageReplyRequest,              // a request to post a reply to a message thread
    //    TeamieUserMessageCreateRequest,             // a request to create a new message thread
    //    TeamieUserLessonsRequest,                   // a request to get all lessons of classroom

    //    TeamieUserPollThoughtRequest,               // a request to set a poll option of a thought
    //    TeamieUserDeviceRegisterRequest,            // a request to register a device token for the user
    //    TeamieUserConnectionsRequest,               // a request to get all the connections of current user
    //    TeamieUserAllConnectionsRequest,
    //    TeamieUserConnectionsByUIDRequest,
    //    TeamieUserMergeSessionRequest,
    //    TeamieUserLeaderboardRequest,           // a request to get the leaderboard of a current classroom
    //    TeamieUserMembersRequest,                // a request to get the members of the current classroom
    //    TeamieUserGetThoughtCommentRequest,            // a request to get a particular comment with cid
    //    TeamieUserActivityHistoryRequest,           // a request to get user's userpoint history for a given no. of days in the past
    
    //
    //    TeamieUserCalendarRequest,
    //    TeamieUserLessonPagesListRequest,
    //    TeamieUserQuizSubmissionRequest,
        TeamieUserLockerLessonRequest,
    //    TeamieUserLockerQuizRequest,
    //    TeamieUserQuizSaveScoreRequest,
    //    TeamieUserQuizDefaultersList,
};

/**
 @param request An TMERequest value indicating which request. This is attached to the URL which is to be used and the request method (GET, POST etc)
 @param parameters A dictionary of values to be passed
 @param makeURL A block which takes in the default (usually template) string and returns the relative path (without leading slash)
 @param completion A block which is executed after the success or failure block, even if they are empty
 @param success A block which is executed if the request is successful. The response object contains the result of the JSON response parsed into a NSDictionary or NSArray
 @param failure A block which should be executed if the request fails. The failure is logged by default
 */
- (AFHTTPRequestOperation *)request:(TMERequest)request
                         parameters:(NSDictionary *)parameters
                            makeURL:(NSString *(^)(NSString *URL))URLBlock
                     loadingMessage:(NSString *)message
                            success:(void (^)(id response))successBlock
                            failure:(void (^)(NSError *error))failureBlock;

/**
 This uses the default URL for the request without a makeURL block.
 @see request:parameters:makeURL:completion:success:failure:
 */
- (AFHTTPRequestOperation *)request:(TMERequest)request
                         parameters:(NSDictionary *)parameters
                     loadingMessage:(NSString *)message
                            success:(void (^)(id response))successBlock
                            failure:(void (^)(NSError *error))failureBlock;

- (AFHTTPRequestOperation *)request:(TMERequest)request
                         parameters:(NSDictionary *)parameters
                     loadingMessage:(NSString *)message
                         completion:(void (^)())completionBlock
                            success:(void (^)(id response))successBlock
                            failure:(void (^)(NSError *error))failureBlock;

- (AFHTTPRequestOperation *)request:(TMERequest)request
                         parameters:(NSDictionary *)parameters
                            makeURL:(NSString *(^)(NSString *URL))URLBlock
                     loadingMessage:(NSString *)message
                         completion:(void (^)())completionBlock
                            success:(void (^)(id response))successBlock
                            failure:(void (^)(NSError *error))failureBlock;


@end
