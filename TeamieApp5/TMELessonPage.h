//
//  LessonPage.h
//  TeamieApp5
//
//  Created by Raunak on 23/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"
#import "TMEUser.h"

@interface TMELessonPage : TMEModel

@property (nonatomic, strong) NSNumber *nid;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *body;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) TMEUser *author;
@property (nonatomic, strong) NSArray *actions;
@property (nonatomic, strong) NSArray *files;
@property (nonatomic, strong) NSArray *links;
@property (nonatomic, strong) NSArray *videos;
@property (nonatomic, strong) NSArray *inline_quizzes;
@property (nonatomic, strong) NSArray *resources;

@end
