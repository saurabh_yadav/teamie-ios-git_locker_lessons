//
//  TMELinkAttachment.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMELinkAttachment.h"

@implementation TMELinkAttachment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
         @"desc": @"description",
    };
}

+ (NSValueTransformer *)lidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

- (UIImage *)previewImage {
    return [TeamieUIGlobals defaultPicForPurpose:@"link"
                                        withSize:15
                                        andColor:[TeamieUIGlobalColors defaultTextColor]];
}

- (NSString *)displaySubTitle {
    return [self.href absoluteString];
}

@end
