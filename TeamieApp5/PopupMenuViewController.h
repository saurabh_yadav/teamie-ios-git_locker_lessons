//
//  PopupMenuViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 2/14/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"

typedef enum{
    kCellStyleCheckmark,
    kCellStylePlain
} CellStyle;

@protocol PopoverMenuDelegate <NSObject>
- (void)menuDidExitWithData:(id)data;
@end

@interface PopupMenuViewController : TableRestApiViewController {
    @private
    NSInteger _maxMenuItems;
}

@property (nonatomic, weak) id<PopoverMenuDelegate> menuDelegate;

@property (nonatomic, strong) NSArray* entries;

- (id)initWithTableStyle:(UITableViewStyle)style cellStyle:(CellStyle)cellStyle selectedRowIndex:(NSNumber*)selectedRowIndex;

- (void)setNumMenuItems:(NSInteger)maxMenuItems;

@end
