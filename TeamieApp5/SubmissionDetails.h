//
//  SubmissionDetails.h
//  TeamieApp5
//
//  Created by Raunak on 23/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SubmissionUser.h"
#import "SubmissionAnswer.h"

@interface SubmissionDetails : NSObject

@property (nonatomic,strong) SubmissionUser* user;
@property (nonatomic,strong) NSNumber* attemptNumber;
@property (nonatomic,strong) NSNumber* submittedTime;
@property (nonatomic) BOOL isDelayed;
@property (nonatomic,strong) NSNumber* completionTime;
@property (nonatomic,strong) NSNumber* totalScore;
@property (nonatomic) BOOL isGraded;
@property (nonatomic,strong) NSArray* submissionAnswers;

@end
