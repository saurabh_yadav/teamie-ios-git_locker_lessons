//
//  TMEPostView.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 17/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TMEPost.h"

typedef NS_ENUM(NSInteger, TMEPostViewMode) {
    TMEPostViewModeNewsfeed,
    TMEPostViewModeClassroom,
    TMEPostViewModeFull,
};

@class TMEPostView;

@protocol TMEPostViewDelegate

- (void)showPostMoreActionsSheet:(TMEPostView *)postView;
- (void)commentButtonClicked:(TMEPost *)post;
- (void)removePost:(TMEPost *)post;
-(void) updateTheRowForIndex: (TMEPost *)post withOption :(BOOL)showMore;
- (void)updatePostViewHeightWithUpdatedPost:(TMEPost *)post;
-(void) updateTheRowForIndex: (TMEPost *)post withHeight:(CGFloat)webViewHeight;

@end

@interface TMEPostView : UIView

extern const CGFloat kTextPadding;

@property (nonatomic, weak) UIButton *commentsCountButton;
@property (nonatomic, weak, readonly) UIButton *moreButton;
@property (nonatomic, weak) UIViewController<TMEPostViewDelegate> *delegate;
@property (nonatomic) BOOL showMore;

- (instancetype)initWithMode:(TMEPostViewMode)mode;
- (instancetype)initWithMode:(TMEPostViewMode)mode andShowOption:(BOOL)showMore andShowOnPostID:(NSMutableDictionary *)showMoreforPostID;
- (void)loadPost:(TMEPost *)post;
- (BOOL)hasMoreActions;
- (UIActionSheet *)moreActionsSheet;

@end
