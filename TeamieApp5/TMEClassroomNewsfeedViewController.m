//
//  TMEClassroomNewsfeedViewController.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 18/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEClassroomNewsfeedViewController.h"

#import "TMEClient.h"
#import "TMEPost.h"
#import "TMEPostCreateViewController.h"
#import "ClassroomHeaderView.h"
#import "TMEShadowView.h"
#import "LSViewController.h"
#import "AssessmentListViewController.h"

@interface TMEClassroomNewsfeedViewController() <ClassroomHeaderProtocol>

@property (nonatomic, strong, readwrite) TMEClassroom *classroom;

@end

@implementation TMEClassroomNewsfeedViewController

#pragma mark - Lifecycle

+ (TMEClassroomNewsfeedViewController *)newsfeedForClassroom:(TMEClassroom *)classroom {
    TMEClassroomNewsfeedViewController *newsfeed = [super newsfeed];
    newsfeed.classroom = classroom;
    newsfeed.mode = TMEPostViewModeClassroom;
    return newsfeed;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = self.classroom.name;
    [self addHeaderView];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/classroom/*"];
}

#pragma mark - Superclass Overrides

- (TMERequest)requestName {
    return TeamieUserClassroomNewsfeedRequest;
}

- (NSDictionary *)requestParameters {
    return @{@"nid": self.classroom.nid};
}

- (void)openPostShareView {
    [self.navigationController pushViewController:[[TMEPostCreateViewController alloc] initWithClassroom:self.classroom]
                                         animated:YES];
}

#pragma mark - Helper

- (void)addHeaderView {
    ClassroomHeaderView *headerView = [[[NSBundle mainBundle] loadNibNamed:@"ClassroomHeaderView" owner:self options:nil] firstObject];
    [headerView styleView];
    headerView.protocol = self;
    
    TMEShadowView *shadow = [TMEShadowView new];
    [shadow addSubview:headerView];

    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 60)];
    self.tableView.tableHeaderView.backgroundColor = [TeamieUIGlobalColors baseBgColor];
    [self.tableView.tableHeaderView addSubview:shadow];
    
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(shadow);
    }];
    [shadow mas_makeConstraints:^(MASConstraintMaker *make){
        make.edges.equalTo(self.tableView.tableHeaderView).with.insets(UIEdgeInsetsMake(13, 13, 0, 13));
    }];
}

#pragma mark - ClassroomHeaderProtocol

- (void)didTapOnClassroomSupplementaryViewButton:(UIButton *)sender {
    UIViewController *controller;
    if (sender.tag == 0) {
        controller = [[LSViewController alloc] initWithSubjectID:self.classroom.nid
                                                    withNavTitle:TMELocalize(@"menu.lessons")];
        ((LSViewController *)controller).classroomName = self.classroom.name;
    }
    else if (sender.tag == 1) {
        controller = [[AssessmentListViewController alloc] initWithClassroomId:self.classroom.nid
                                                             andClassroomTitle:self.classroom.name];
    }
    [self.navigationController pushViewController:controller
                                         animated:YES];
}

@end
