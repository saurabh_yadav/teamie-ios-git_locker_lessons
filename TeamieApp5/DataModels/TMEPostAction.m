//
//  TMEPostAction.m
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostAction.h"

@implementation TMEPostAction

+ (NSValueTransformer *)accessJSONTransformer {
    return [TMEModel boolJSONTransformer];
}

+ (NSValueTransformer *)statusJSONTransformer {
    return [TMEModel boolJSONTransformer];
}

@end
