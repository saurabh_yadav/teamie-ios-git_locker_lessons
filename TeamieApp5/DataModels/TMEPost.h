//
//  TMEPost.h
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEUser.h"
#import "TMEClassroom.h"
#import "TMEPostAction.h"
#import "TMEPollAttachment.h"
#import "TMEModel.h"

typedef NS_ENUM(NSInteger, TMEPostType) {
    TMEPostThought,
    TMEPostQuestion,
    TMEPostHomework
};

typedef NS_ENUM(NSInteger, TMEPostStatus) {
    TMEPostStatusDraft = 0,
    TMEPostStatusPublished,
    TMEPostStatusPinned,
    TMEPostStatusUnpublished
};

@interface TMEPost : TMEModel

@property (nonatomic, strong) NSNumber* tid;
@property (nonatomic, strong) NSString* msgHtml;
@property (nonatomic, strong) NSString* msgText;
@property (nonatomic, strong) NSString* timestampText;
@property (nonatomic, strong) NSAttributedString* richTextHtml;
@property (nonatomic) BOOL isAnnouncement;
@property (nonatomic) BOOL isAnonymous;
@property (nonatomic) TMEPostType type;
@property (nonatomic) TMEPostStatus status;
@property (nonatomic, strong) NSDate* created;
@property (nonatomic, strong) NSDate* updated;
@property (nonatomic, strong) NSNumber* commentsCount;
@property (nonatomic, strong) NSDictionary *actions;
@property (nonatomic, strong) NSArray *actionList;
@property (nonatomic, strong) TMEUser *author;
@property (nonatomic, strong) NSArray* comments;
@property (nonatomic, strong) NSArray* images;
@property (nonatomic, strong) NSArray* links;
@property (nonatomic, strong) NSArray* files;
@property (nonatomic, strong) NSArray* videos;
@property (nonatomic, strong) NSArray* audios;
@property (nonatomic, strong) NSArray* resources;
@property (nonatomic, strong) TMEPollAttachment* poll;
@property (nonatomic, strong) TMEClassroom *classroom;

@property (nonatomic, strong) NSArray *attachments;
@property (nonatomic, strong) NSMutableAttributedString *mainText;
@property (nonatomic, strong) NSMutableAttributedString *richText;

//Defined here for compatibility
@property (nonatomic, strong) NSNumber *likesCount;
@property (nonatomic, strong) NSNumber *echosCount;

+ (NSValueTransformer *)typeJSONTransformer;
- (NSString *)typeString;
- (BOOL)isNew;

@end