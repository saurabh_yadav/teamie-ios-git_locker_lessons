//
//  SimpleClassroom.h
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleClassroom : NSObject

@property (nonatomic, strong) NSNumber* nid;
@property (nonatomic, strong) NSString* name;

@end
