//
//  TMEPost.h
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPost.h"
#import "TMEThought.h"
#import "TMEQuestion.h"
#import "TMEHomework.h"
#import "TMEPostComment.h"
#import "TMEImageAttachment.h"
#import "TMEFileAttachment.h"
#import "TMEVideoAttachment.h"
#import "TMEAudioAttachment.h"
#import "TMEResourceAttachment.h"
#import "TMELinkAttachment.h"

@implementation TMEPost

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
         @"msgHtml": @"message.html",
         @"msgText": @"message.text",
         @"timestampText": @"timestamp.time.text",
         @"isAnnouncement": @"is_announcement",
         @"isAnonymous": @"is_anonymous",
         @"commentsCount": @"total_comments",
         @"actionList": @"action",
         @"author": @"user",
         @"classroom": @"timestamp.classroom",
         @"comments": @"comments.comment",
         @"images": @"attachment.images",
         @"links": @"attachment.links",
         @"files": @"attachment.files",
         @"videos": @"attachment.videos",
         @"audios": @"attachment.audios",
         @"resources": @"attachment.resource",
         @"richTextHtml": @"attachment.richtext.html",
         @"poll": @"attachment.poll",
         @"attachments": NSNull.null,
         @"likesCount": NSNull.null,
         @"echosCount": NSNull.null,
    };
}

+ (NSValueTransformer *)tidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)createdJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)updatedJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)commentsJSONTransformer {
    return [TMEModel listJSONTransformer:TMEPostComment.class];
}

+ (NSValueTransformer *)imagesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEImageAttachment.class];
}

+ (NSValueTransformer *)linksJSONTransformer {
    return [TMEModel listJSONTransformer:TMELinkAttachment.class];
}

+ (NSValueTransformer *)filesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEFileAttachment.class];
}

+ (NSValueTransformer *)videosJSONTransformer {
    return [TMEModel listJSONTransformer:TMEVideoAttachment.class];
}

+ (NSValueTransformer *)audiosJSONTransformer {
    return [TMEModel listJSONTransformer:TMEAudioAttachment.class];
}

+ (NSValueTransformer *)resourcesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEResourceAttachment.class];
}

+ (NSValueTransformer *)pollJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEPollAttachment.class];
}

+ (NSValueTransformer *)authorJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEUser.class];
}

+ (NSValueTransformer *)classroomJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEClassroom.class];
}

+ (NSValueTransformer *)actionListJSONTransformer {
    return [TMEModel dictionaryListJSONTransformer];
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{
       @"thought": @(TMEPostThought),
       @"question": @(TMEPostQuestion),
       @"homework": @(TMEPostHomework),
    }];
}

+ (Class)classForParsingJSONDictionary:(NSDictionary *)JSONDictionary {
    NSString *type = JSONDictionary[@"type"];
            
    if ([type isEqualToString:@"thought"]) {
        return TMEThought.class;
    }
    else if ([type isEqualToString:@"question"]) {
        return TMEQuestion.class;
    }
    else if ([type isEqualToString:@"homework"]) {
        return TMEHomework.class;
    }
    
    NSAssert(NO, @"No matching class for the post '%@'.", JSONDictionary);
    return self;
}


- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self) {
        NSMutableDictionary *mutableActions = [[NSMutableDictionary alloc] initWithCapacity:self.actionList.count];
        for (NSDictionary *actionDict in self.actionList) {
            TMEPostAction *action = [TMEPostAction parseDictionary:actionDict];
            mutableActions[action.name] = action;
        }
        self.actions = [mutableActions copy];
        
        NSMutableArray *attachments = [NSMutableArray new];
//        [attachments addObjectsFromArray:self.images];
        [attachments addObjectsFromArray:self.links];
        [attachments addObjectsFromArray:self.files];
        [attachments addObjectsFromArray:self.videos];
        [attachments addObjectsFromArray:self.audios];
        self.attachments = attachments;
    }
    return self;
}

- (NSString *)typeString {
    return [[TMEPost typeJSONTransformer] reverseTransformedValue:@(self.type)];
}

- (NSMutableAttributedString *)mainText {
    if (_mainText == nil) {
        //iOS 6 and below will show plain text
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
            _mainText = [[NSMutableAttributedString alloc]
                                           initWithData:[self.msgHtml dataUsingEncoding:NSUTF8StringEncoding]
                                           options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                     NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                           documentAttributes:nil
                                           error:nil];
        }
        else {
            _mainText = [[NSMutableAttributedString alloc] initWithString:self.msgText];
        }
        [_mainText addAttribute:NSFontAttributeName
                          value:[TeamieGlobals appFontFor:@"postBody"]
                          range:NSMakeRange(0, _mainText.length)];
    }
    return _mainText;
}

- (NSAttributedString *) richText{
    if (_richText == nil) {
        _richText = [self.richTextHtml mutableCopy];
    }
    return _richText;
}

- (BOOL)isNew {
    if (![self.author.uid isEqualToNumber:[TMEUser currentUser].uid]) {
        TMEPostAction *seen = self.actions[@"seen"];
        if (seen) {
            return !seen.status;
        }
    }
    return NO;
}


@end