//
//  SimpleUser.h
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SimpleUser : NSObject

@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSNumber* uid;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* realname;
@property (nonatomic, strong) NSString* thumbnailImageURL;

@end
