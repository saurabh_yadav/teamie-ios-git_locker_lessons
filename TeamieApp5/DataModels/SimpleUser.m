//
//  SimpleUser.m
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "SimpleUser.h"

@implementation SimpleUser

@synthesize username, uid, email, realname, thumbnailImageURL;


- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.uid forKey:@"uid"];
    [encoder encodeObject:self.username forKey:@"username"];
    [encoder encodeObject:self.email forKey:@"email"];
    [encoder encodeObject:self.realname forKey:@"realname"];
    [encoder encodeObject:self.thumbnailImageURL forKey:@"thumbnailImageURL"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.uid = [decoder decodeObjectForKey:@"uid"];
        self.username = [decoder decodeObjectForKey:@"username"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.realname = [decoder decodeObjectForKey:@"realname"];
        self.email = [decoder decodeObjectForKey:@"email"];
        self.thumbnailImageURL = [decoder decodeObjectForKey:@"thumbnailImageURL"];
    }
    return self;
}

- (void)saveUserObject:(SimpleUser *)object key:(NSString *)key {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:object];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:key];
    [defaults synchronize];
}

- (SimpleUser *)loadUserObjectWithKey:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:key];
    SimpleUser *object = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
    return object;
}

@end
