//
//  TMEPostAction.h
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"

@interface TMEPostAction : TMEModel

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* title;
@property (nonatomic) BOOL access;
@property (nonatomic) BOOL status;
@property (nonatomic, strong) NSString* href;
@property (nonatomic, strong) NSString* method;
@property (nonatomic, strong) NSArray* attributes;

@end
