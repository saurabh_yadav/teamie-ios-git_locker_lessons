//
//  ScoreBoxView.m
//  TeamieApp5
//
//  Created by Raunak on 18/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ScoreBoxView.h"

@interface ScoreBoxView ()  {
    UIView* scoreLine;
}

@end

@implementation ScoreBoxView

- (id)initWithFrame:(CGRect)frame
{
    return [self initWithFrame:frame userScore:@"0" maxScore:@"0"];
}

- (id)initWithFrame:(CGRect)frame userScore:(NSString*)userScore maxScore:(NSString*)maxScore {
    self = [super initWithFrame:frame];
    if (self) {
        _userScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(frame), (CGRectGetHeight(frame)/2))];
        scoreLine = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_userScoreLabel.frame), CGRectGetWidth(frame), 1)];
        _maxScoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(scoreLine.frame), CGRectGetWidth(frame), (CGRectGetHeight(frame)/2 - 1))];
        
        [self addSubview:self.userScoreLabel];
        [self addSubview:scoreLine];
        [self addSubview:self.maxScoreLabel];
        
        _userScoreLabel.text = userScore;
        _maxScoreLabel.text = maxScore;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    return [self initWithFrame:[self frame]];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code    
    self.userScoreLabel.frame = CGRectMake(0, 0, CGRectGetWidth(self.frame), (CGRectGetHeight(self.frame)/2));
    self.userScoreLabel.backgroundColor = [UIColor colorWithHex:@"#eeeeee" alpha:1.0];
    self.userScoreLabel.textAlignment = NSTextAlignmentCenter;
    self.userScoreLabel.textColor = [UIColor blackColor];
    self.userScoreLabel.font = [TeamieGlobals appFontFor:@"boldFontWithSize18"];
    
    scoreLine.frame = CGRectMake(0, CGRectGetMaxY(self.userScoreLabel.frame), CGRectGetWidth(self.frame), 1);
    scoreLine.backgroundColor = [UIColor blackColor];
    
    self.maxScoreLabel.frame = CGRectMake(0, CGRectGetMaxY(scoreLine.frame), CGRectGetWidth(self.frame), (CGRectGetHeight(self.frame)/2 - 1));
    self.maxScoreLabel.backgroundColor = [UIColor colorWithHex:@"#eeeeee" alpha:1.0];
    self.maxScoreLabel.textAlignment = NSTextAlignmentCenter;
    self.maxScoreLabel.textColor = [UIColor blackColor];
    self.maxScoreLabel.font = [TeamieGlobals appFontFor:@"boldFontWithSize18"];
}


@end
