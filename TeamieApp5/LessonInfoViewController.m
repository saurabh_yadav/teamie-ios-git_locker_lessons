//
//  LessonInfoViewController.m
//  LS2
//
//  Created by Wei Wenbo on 3/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import "LessonInfoViewController.h"
#import "UIImageView+webCache.h"
#import "InfoViewCell.h"
#import "LessonPageViewController.h"
#import "LessonInfoCollapseClickCell.h"
#import "PageStatsLabel.h"
#import "NSString+FontAwesome.h"
#import <FontAwesomeIconFactory/NIKFontAwesomeIconFactory.h>
#import <FontAwesomeIconFactory/NIKFontAwesomeIconFactory+iOS.h>
#import "TMELessonAdditionalInfo.h"
#import "TMEClassroom.h"
#import "UserActions.h"
#import "TMELessonPageShort.h"
#import "TMELesson.h"
#import "NSMutableArray+PagesUtilityButtons.h"
#import "PKRevealController+Hide.h"
#import "NSData+Base64.h"
#import "UserActions.h"
#import "TMEClient.h"
#import "UserProfileViewController.h"

#define RowHeightBenchMark 45
#define InfoTitleLabelHeightBenchMark 25
#define InfoTitleValueLabelHeightBenchMark 25
#define InfoCellMargin 15
#define InfoCellLabelDistance 100
#define FontSizeForInfoTitleLabel 13
#define FontSizeForInfoTitleValueLabel 13
#define FontSizeForGoalField 13
#define FontSizeForClassroomField 11
#define FontSizeForPagesField 11
#define FontSizeForCompleteUserNumber 11
#define FontSizeForCompleteRemainingString 11

static const CGSize constrainedInfoTitleSize = {100, MAXFLOAT};
static const CGSize constrainedInfoTitileValueSize = {100,MAXFLOAT};

static NSString *UnknownDate = @"Unknown";
static NSString *NotIndicatedInfo = @"Not indicated";

@interface LessonInfoViewController ()
@property TMELesson *lesson;
@property NSArray *additionalInfoFields;
@property NSMutableArray *photos;
@property NSNumber *lessonID;
@property NSNumber *authorID;

@property BOOL has_edit_access;
@property BOOL has_delete_access;
@property BOOL has_toggle_access;
@property NSIndexPath *indexPathOfDeletedCell;
@property UIButton *imageButton;
@property UIImage *imageToReplace;
@property PagesTableViewCell *currentCellEnabled;
@property int currentCellEnableButtonIndex;
@property PageStatsLabel *pageReadStatsLabel;
@property UIView *defaultCoverImageView;
@property (strong, nonatomic) NSDictionary *tempLesson;
@end

@implementation LessonInfoViewController

static NSInteger viewActionIndex;
static NSInteger uploadActionIndex;
static NSInteger toggleStatusActionIndex;
static const NSInteger coverImageActionSheetTag = 1;
static const NSInteger lessonEditActionSheetTag = 2;

- (id)initWithLesson:(NSNumber *)lessonID
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Lesson" bundle:nil];
    self = [storyBoard instantiateViewControllerWithIdentifier:@"LessonInfoViewController"];
    
    if (self) {
        self.lessonID = lessonID;
//        self.lesson.lesson_pages = lessonPages;
    }
    
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [[NSBundle mainBundle] loadNibNamed:@"LessonInfoViewController" owner:self options:nil];
        // Custom initialization
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //    self.lessonHeaderView.authorImageView
    //    self.lessonHeaderView.deadline.text
    //    self.lessonHeaderView.classroomInfo.text
    
    
    //    self.infoField.infoTable.delegate = self;
    //    self.infoField.infoTable.dataSource = self;
    //    self.infoField.infoTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    //
    //
    //    self.classroomFieldView.classroomTable.delegate = self;
    //    self.classroomFieldView.classroomTable.dataSource = self;
    //    self.classroomFieldView.classroomTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    //
    //    self.pagesField.pagesTableView.delegate = self;
    //    self.pagesField.pagesTableView.dataSource = self;
    //    self.pagesField.pagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //
    //    self.myCollapseClick.CollapseClickDelegate = self;
    //
    //
    //    [self.coverImageView.coverImage setImageWithURL:self.lesson.coverImageURL];
    //    [self.coverImageView.button addTarget:self action:@selector(coverImageClicked) forControlEvents:UIControlEventTouchUpInside];
    //
    //
    //    [self.myCollapseClick reloadCollapseClick];
    //    self.myCollapseClick.backgroundColor = [UIColor colorWithHex:@"#cccccc" alpha:1];
    //    [self.myCollapseClick openCollapseClickCellAtIndex:0 animated:NO];
    //
    //
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"back2" withSize:25 andColor:[UIColor whiteColor]] style:UIBarButtonItemStyleDone target:self action:@selector(backButtonPressed)];
    
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.backBarButtonItem = nil;
    [self.editLessonButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"ellipsis" withSize:25 andColor:[UIColor whiteColor]] forState:UIControlStateNormal] ;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];
    
    [[TMEClient sharedClient]
     request:TeamieUserLessonRequest
     parameters:nil
     makeURL:^NSString *(NSString *URL) {
         return [NSString stringWithFormat:URL, self.lessonID];
     }
     loadingMessage:TMELocalize(@"message.loading")
     success:^(NSDictionary *response) {
         [[NSBundle mainBundle] loadNibNamed:@"MyView" owner:self options:nil];
         [[NSBundle mainBundle] loadNibNamed:@"AuthorField" owner:self options:nil];
         [[NSBundle mainBundle] loadNibNamed:@"CompleteReadersField" owner:self options:nil];
         
         self.lesson = [TMELesson parseDictionary:[response valueForKey:@"lesson"]];
         self.tempLesson = [response valueForKey:@"lesson"];
         [self processAccess:self.lesson.actions];
         [self processLessonResponse];
     } failure:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/lesson/*"];
    [self.editLessonButton setHidden:YES];
    [self.readButton setHidden:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collapse Click Delegate

// Required Methods
-(NSUInteger)numberOfCellsForCollapseClick {
    if(self.has_edit_access){
        return menuSectionCount;
    }
    else{
        return menuSectionCount - 1 ;
    }
}

-(NSString *)titleForCollapseClickAtIndex:(int)index {
    
    switch (index) {
        case GoalSection:
            return @"Learning Goals";
        case LessonInfoSection:
            return @"Other Info";
        case ClassroomSection:
            return @"Classrooms";
        case LessonStatsSection:
            return @"Statistics";
        case LessonPagesSection:
            return [NSString stringWithFormat:@"%lu pages", (unsigned long)self.lesson.lesson_pages.count];
        default:
            return @"";
            break;
    }
}

-(UIView *)viewForCollapseClickContentViewAtIndex:(int)index {
    CGFloat oldHeight;
    CGFloat newHeight;
    CGFloat heightOffset;
    CGRect frame;
    switch (index) {
        case GoalSection:
        {
            //single method for setting the height of the goal field frame on iPAD AND iPHONE was not working,
            // the workaround i found for now , was to use two different methods for ipad and another one for iPHONE
            NSString *goalString;
            UIFont *font = [UIFont systemFontOfSize:11.0f];
            self.goalField.goalText.font = font;
            self.goalField.goalText.center = CGPointMake(self.goalField.frame.size.width / 2, self.goalField.frame.size.height / 2);
            [self.goalField.goalText setFont:[TeamieGlobals appFontFor:@"regularFontWithSize11"]];

            
            
            if ([self.lesson.desc length] > 0) {
                self.goalField.goalText.attributedText = [[NSAttributedString alloc] initWithData:[self.lesson.desc dataUsingEncoding:NSUTF8StringEncoding]
                                                                                          options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                                    NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                                                               documentAttributes:nil error:nil];
                
                goalString = self.lesson.desc;
            }
            else{
                self.goalField.goalText.attributedText = [[NSAttributedString alloc] initWithString:NotIndicatedInfo];
                goalString = @"Not Indicated";
            }
            self.goalField.goalText.editable = NO;
            self.goalField.goalText.scrollEnabled = NO;
            if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
                CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width, FLT_MAX);
                CGSize newHeight = [goalString sizeWithFont:font constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingHead];
                CGRect frameRect = CGRectMake(0, 0, self.view.frame.size.width, newHeight.height);
                frameRect.size.height = newHeight.height + 36;
                self.goalField.frame = frameRect;
                
                self.goalField.goalText.backgroundColor = [UIColor colorWithHex:@"#f1f2f3" alpha:1.0];
                CGFloat fixedWidth = self.goalField.frame.size.width;
                CGSize newSize = [self.goalField sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
                CGRect newFrame = self.goalField.frame;
                newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
                
                
                self.goalField.frame = newFrame;
                return self.goalField;
            }
            else{
                oldHeight = self.goalField.goalText.frame.size.height;
                [self.goalField.goalText sizeToFit];
                newHeight = self.goalField.goalText.frame.size.height;
                
                heightOffset = newHeight - oldHeight;
                
                frame = self.goalField.frame;
                frame.size = CGSizeMake(CGRectGetWidth(self.view.bounds), frame.size.height + heightOffset);
                self.goalField.frame = frame;
                
                self.goalField.goalText.backgroundColor = [UIColor colorWithHex:@"#f1f2f3" alpha:1.0];
                return self.goalField;
            }
        }
        case LessonInfoSection:
        {
            
            oldHeight = self.infoField.infoTable.frame.size.height;
            [self.infoField.infoTable sizeToFit];
            newHeight = self.infoField.infoTable.frame.size.height;
            
            heightOffset = newHeight - oldHeight;
            
            frame = self.infoField.frame;
            frame.size = CGSizeMake(CGRectGetWidth(self.view.bounds), frame.size.height + heightOffset);
            self.infoField.frame = frame;
            
//            self.infoField.infoTable.backgroundColor = [UIColor colorWithHex:@"#f1f2f3" alpha:1.0];
            return self.infoField;
        }
        case ClassroomSection:
        {
            oldHeight = self.classroomFieldView.classroomTable.frame.size.height;
            [self.classroomFieldView.classroomTable sizeToFit];
            newHeight = self.classroomFieldView.classroomTable.frame.size.height;
            
            heightOffset = newHeight - oldHeight;
            
            frame = self.classroomFieldView.frame;
            frame.size = CGSizeMake(CGRectGetWidth(self.view.bounds), frame.size.height + heightOffset);
            self.classroomFieldView.frame = frame;
            return self.classroomFieldView;
        }
        case LessonStatsSection:
        {
            if (_has_edit_access) {
//            self.statsField.readersLabel.text = [NSString stringWithFormat:@"%@ readers",self.lesson.num_reader ];
//            
//            self.statsField.backgroundColor = [UIColor colorWithHex:@"#f1f2f3" alpha:1.0];
//            return self.statsField;
                [self configureCompleteReadersField];
                return self.completeReadersField;
            }
            else{
                [self.completeReadersField setHidden:YES];
                return nil;
            }
        }
        case LessonPagesSection:
        {
            oldHeight = self.pagesField.pagesTableView.frame.size.height;
            [self.pagesField.pagesTableView sizeToFit];
            newHeight = self.pagesField.pagesTableView.frame.size.height;
            
            heightOffset = newHeight - oldHeight;
            frame = self.pagesField.frame;
            frame.size = CGSizeMake(CGRectGetWidth(self.view.bounds), frame.size.height + heightOffset);
            self.pagesField.frame = frame;
            return self.pagesField;
        default:
            return nil;
        }
    }

}
// Optional Methods

-(UIColor *)colorForCollapseClickTitleViewAtIndex:(int)index {
//    return [UIColor colorWithRed:223/255.0f green:47/255.0f blue:51/255.0f alpha:1.0];
    return [UIColor colorWithWhite:1.0 alpha:1.0];
}

-(UIColor *)colorForTitleLabelAtIndex:(int)index {
    return [UIColor colorWithHex:@"#333333" alpha:1.0];
}

-(UIColor *)colorForTitleArrowAtIndex:(int)index {
    return [UIColor blackColor];
}

- (UIColor *)colorForLessonInfo:(int)index{
    return [UIColor whiteColor];
}

-(void)didClickCollapseClickCellAtIndex:(int)index isNowOpen:(BOOL)open {
}

- (void)showLessonPagesReadStats
{
    if (self.has_edit_access == NO) {
        int index = LessonPagesSection;
        LessonInfoCollapseClickCell *cell =[self.myCollapseClick.dataArray objectAtIndex:index];
        
        
        UIView *titleView = cell.TitleView;
        
        self.pageReadStatsLabel = [[PageStatsLabel alloc]initWithFrame:CGRectMake(200, 10, 100, titleView.frame.size.height - 20)];
        
        [self updatePagesStatsLabel];
        
        [titleView addSubview:self.pageReadStatsLabel];
    }
}

- (UIView *)viewForAuthorField
{
    if (self.lesson.author.user_profile_image.path) {
        [self.authorField.authorImageView sd_setImageWithURL:self.lesson.author.user_profile_image.path];
    }
    else{
        self.authorField.authorImageView.image = [TeamieUIGlobals defaultPicForPurpose:@"profile" withSize:25 andColor:[UIColor blueColor]];
    }
    if (self.lesson.author.uid) {
        self.authorID = self.lesson.author.uid;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapDetectedOnAuthorImage)];
        singleTap.numberOfTapsRequired = 1;
        [self.authorField.authorImageView setUserInteractionEnabled:YES];
        [self.authorField.authorImageView addGestureRecognizer:singleTap];
        self.authorField.nameLabel.text = self.lesson.author.realName;
    }
    if (self.has_edit_access == YES) {
        
        [self.editLessonButton setHidden:NO];
        if ([self.lesson.status boolValue] == YES) {
            [self.authorField.PublishStatusLabelAuthor configurePublishStatusLabelWithStatus:PublishStatusPublished];
        }else{
            [self.authorField.PublishStatusLabelAuthor configurePublishStatusLabelWithStatus:PublishStatusDrafted];
        }
        [self.authorField.PublishStatusLabelAuthor sizeToFit];
    }else{
        [self.authorField.PublishStatusLabelAuthor removeFromSuperview];
        [self.editLessonButton removeFromSuperview];
        [self.editLessonButton setHidden:YES];
    }
    return self.authorField;
}

- (UIView *)viewForCoverImage
{
    if(self.lesson.coverImageURL!=nil){
        return self.coverImageView;
    }
    else{
        self.coverImageView.backgroundColor = [UIColor grayColor];
        return self.coverImageView;
    }
}

- (void)configureCompleteReadersField
{
    UILabel *label = self.completeReadersField.textLabel;
    
    int userNum = 3;
    NSString *numberString = [NSString stringWithFormat:@"%@ users ", self.lesson.num_reader];
    NSString *remainingString = (userNum == 1)? @"has read lesson completely":@"have read lesson completely";
    
    UIFont *userNumberFont = [TeamieGlobals appFontFor:@"notificationsBadgeText"];
    UIFont *remainingStringFont = [TeamieGlobals appFontFor:@"remainingStringFont"];
    
    UIColor *userNumberColor = [UIColor colorWithHex:@"#6f6f6f" alpha:1.0];
    UIColor *remainingStringColor = [UIColor colorWithHex:@"#6f6f6f" alpha:1.0];
//    NSDictionary * fontMapping = [NSDictionary dictionaryWithKeysAndObjects:userNumberFont,numberString,remainingStringFont,remainingString, nil];  //an NSDictionary of NSString => UIColor pairs
    NSDictionary *fontMapping = @{numberString: userNumberFont,
                                  remainingString: remainingStringFont};
    NSDictionary *colorMapping = @{numberString: userNumberColor,
                                   remainingString: remainingStringColor};
    NSArray *wordOrderArray = [NSArray arrayWithObjects:numberString,remainingString, nil];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@""];
    for (NSString * word in wordOrderArray) {
        UIFont * font = [fontMapping objectForKey:word];
        UIColor *color = [colorMapping objectForKey:word];
//        NSDictionary * attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
        NSDictionary * attributes = @{NSFontAttributeName: font,
                                      NSForegroundColorAttributeName:color};
        NSAttributedString * subString = [[NSAttributedString alloc] initWithString:word attributes:attributes];
        [string appendAttributedString:subString];
    }
    
    CGPoint center = label.center;
    CGRect expectedFrame = [string boundingRectWithSize:CGSizeMake(320, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];

    label.attributedText = string;
    CGRect frame = label.frame;
    frame.size = expectedFrame.size;
    
    label.frame = frame;
    
    label.center = center;
    
    self.completeReadersField.backgroundColor = [UIColor colorWithHex:@"#f1f2f3" alpha:1.0];
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.infoField.infoTable) {
        return self.additionalInfoFields.count;
    }
    
    if (tableView == self.classroomFieldView.classroomTable) {
        return self.lesson.classrooms.count;
    }
    if (tableView == self.pagesField.pagesTableView) {
        return self.lesson.lesson_pages.count;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.infoField.infoTable) {
        
        NSDictionary *field = [self.additionalInfoFields objectAtIndex:indexPath.row];
        NSString *titleText = [field objectForKey:@"key"];
        NSString *titleValueText = [field objectForKey:@"value"];
        
        CGSize expectedTitleSize = [titleText sizeWithFont:[TeamieGlobals appFontFor:@"tableTitleRegularFont"] constrainedToSize:constrainedInfoTitleSize lineBreakMode:NSLineBreakByWordWrapping];
        
        CGSize expectedTitleValueSize = [titleValueText sizeWithFont:[TeamieGlobals appFontFor:@"tableTitleRegularFont"] constrainedToSize:constrainedInfoTitileValueSize lineBreakMode:NSLineBreakByWordWrapping];
        
        CGFloat titleHeightOffset = expectedTitleSize.height - 24;
        CGFloat titleValueHeightOffset = expectedTitleValueSize.height - 24;
        
        CGFloat offset = (titleHeightOffset-titleValueHeightOffset >= 0)? titleHeightOffset:titleValueHeightOffset;
        
        offset = (offset>0)? offset: 0;
        return RowHeightBenchMark + offset;
    }
    return RowHeightBenchMark;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *cellIdentifier = @"Cell";
    
    if (tableView == self.infoField.infoTable) {
        InfoViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[InfoViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
            
            cell.title = [[UILabel alloc]init];
            cell.titleValue = [[UILabel alloc]initWithFrame:CGRectMake(0, InfoCellMargin, self.view.frame.size.width - InfoCellMargin, 0)];
            [cell addSubview:cell.title];
            [cell addSubview:cell.titleValue];
        }
        
        //TODO: check the validation of index path
        NSDictionary *field = [self.additionalInfoFields objectAtIndex:indexPath.row];
        NSString *titleText = [field objectForKey:@"key"];
        NSString *titleValueText = [field objectForKey:@"value"];
        cell.title.text = titleText;
        cell.title.font = [TeamieGlobals appFontFor:@"tableTitleRegularFont"];
        cell.title.lineBreakMode = NSLineBreakByWordWrapping;
        cell.title.numberOfLines = 0;
        CGSize expectedTitleSize = [titleText sizeWithFont:[TeamieGlobals appFontFor:@"tableTitleRegularFont"] constrainedToSize:constrainedInfoTitleSize lineBreakMode:NSLineBreakByWordWrapping];
        cell.title.frame = CGRectMake(InfoCellMargin, InfoCellMargin, expectedTitleSize.width, expectedTitleSize.height);
        
        //Step 1, keep the origin, change the frame size to expected size
        CGSize expectedTitleValueSize = [titleValueText sizeWithFont:[TeamieGlobals appFontFor:@"tableTitleRegularFont"] constrainedToSize:constrainedInfoTitileValueSize lineBreakMode:NSLineBreakByWordWrapping];
        CGRect frame = cell.titleValue.frame;
        CGPoint oldOrigin = frame.origin;
        CGFloat oldFrameWidth = frame.size.width;
        frame.size = expectedTitleValueSize;
        
        
        //Step 2, find the width offset
        CGFloat newFrameWidth = frame.size.width;
        CGFloat widthOffset = newFrameWidth - oldFrameWidth;
        
        //Step 3,move the origin left with the same width offset.
        CGPoint newOrigin = CGPointMake(oldOrigin.x - widthOffset, oldOrigin.y);
        frame.origin = newOrigin;
        
        //Step 4, configure the label
        cell.titleValue.text = titleValueText;
        cell.titleValue.font = [TeamieGlobals appFontFor:@"tableTitleRegularFont"];
        cell.titleValue.lineBreakMode = NSLineBreakByWordWrapping;
        cell.titleValue.numberOfLines = 0;
        
        cell.titleValue.frame = frame;

        //Step 5
//        cell.titleValue.center = CGPointMake(cell.titleValue.center.x, centerY);

        
        if (indexPath.row != 0) {
            UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 1)];
            separatorLineView.backgroundColor = [UIColor colorWithHex:@"#dddddd" alpha:1.0];
            [cell.contentView addSubview:separatorLineView];
        }
        
        return cell;

    }else if (tableView == self.classroomFieldView.classroomTable){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        TMEClassroom *classroom = [self.lesson.classrooms objectAtIndex:indexPath.row];
        cell.textLabel.text = classroom.name;
        cell.textLabel.font = [TeamieGlobals appFontFor:@"remainingStringFont"];
        
//        NIKFontAwesomeIcon iconName = 0xf000 + indexPath.row;
//        NIKFontAwesomeIconFactory* factory = [[NIKFontAwesomeIconFactory alloc] init];
//        factory.colors = @[[UIColor blueColor]];
//        factory.size = 25;
//        cell.imageView.image = [factory createImageForIcon:iconName];
//        cell.textLabel.text = [NSString stringWithFormat:@"this is line %d",indexPath.row + 2];
        if (indexPath.row != 0) {
            UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 1)];
            separatorLineView.backgroundColor = [UIColor colorWithHex:@"#dddddd" alpha:1.0];
            [cell.contentView addSubview:separatorLineView];
        }
        return cell;
    }else if (tableView == self.pagesField.pagesTableView){
        PagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        if (cell == nil) {
            cell = [[PagesTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        TMELessonPageShort *lessonPage = [self.lesson.lesson_pages objectAtIndex:indexPath.row];
        
        // Add utility buttons
        NSMutableArray *rightUtilityButtons = [NSMutableArray new];
        
        //    [rightUtilityButtons sw_addUtilityButtonWithColor:
        //     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
        //                                                title:@"More"];
        //    [rightUtilityButtons sw_addUtilityButtonWithColor:
        //     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
        //                                                title:@"Delete"];
        
        
        BOOL edit_access  = NO;
        BOOL delete_access = NO;
        for (UserActions *action in lessonPage.actions) {
            if ([action.name isEqualToString:@"edit"]) {
                edit_access = YES;
            }else if ([action.name isEqualToString:@"delete"]){
                delete_access = YES;
            }
        }
        
        if (edit_access == YES) {
            NSString *hexColor;
            if ([lessonPage.status boolValue] == YES) {
                hexColor = publishedColorHex;
            }else{
                hexColor = draftedColorHex;
            }
            [rightUtilityButtons addPagesUtilityButtonWithColor:[UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0] icon:@"icon-ok" titleColor:[UIColor colorWithHex:hexColor alpha:1.0]] ;
        }
        
        if (delete_access == YES) {
            [rightUtilityButtons addPagesUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] icon:@"icon-remove" titleColor:[UIColor whiteColor]];
        }
        
        cell.rightUtilityButtons = rightUtilityButtons;
        cell.delegate = self;
        
        cell.textLabel.text = lessonPage.title;
        cell.textLabel.font = [TeamieGlobals appFontFor:@"regularFontWithSize14"];
//        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        
        if (indexPath.row != 0) {
            UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 1)];
            separatorLineView.backgroundColor = [UIColor colorWithHex:@"#dddddd" alpha:1.0];
            [cell.contentView addSubview:separatorLineView];
        }
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   cell.backgroundColor = [UIColor colorWithHex:@"#f1f2f3" alpha:1.0];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.pagesField.pagesTableView) {
        LessonPageViewController *lessonPageViewController = [[LessonPageViewController alloc]init];
        lessonPageViewController.lessonsList = self.lesson.lesson_pages;
        
        lessonPageViewController.currentPage = indexPath.row;
        
        lessonPageViewController.lessonID = self.lessonID;
        
        [self.navigationController pushViewController:lessonPageViewController animated:YES];
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Private Methods
- (void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

//- (void)makeWebRequest
//{
//    NSDictionary* dict = [NSDictionary dictionaryWithObject:self.lesson.nid forKey:@"lid"];
//    [self.apiCaller performRestRequest:TeamieUserLessonPagesListRequest withParams:dict];
//}

- (NSArray *)additionalInfoOfLesson:(TMELesson *)lesson
{
    NSMutableArray *additionalInfo = [[NSMutableArray alloc]init];
    int counterForOtherInfo = 0;
    NSMutableArray *keyObject, *valueObject;
    keyObject = [NSMutableArray array];
    valueObject = [NSMutableArray array];
    if(lesson.tags){
        
        for (TMELessonAdditionalInfo *additionalInfoFieldObject in lesson.tags) {
            
            NSMutableDictionary *field = [[NSMutableDictionary alloc]init];
            if (additionalInfoFieldObject.label != nil && counterForOtherInfo < 1) {
                [field setObject:additionalInfoFieldObject.label forKey:@"key"];
                [keyObject addObject:additionalInfoFieldObject.label];
            }
            else{
                [field setObject:NotIndicatedInfo forKey:@"key"];
                [keyObject addObject:@"NotIndicatedInfo"];
            }
            if (additionalInfoFieldObject.title != nil) {
                [field setObject:@"nil" forKey:@"value"];
                [valueObject addObject:additionalInfoFieldObject.title];
            }else{
                [field setObject:NotIndicatedInfo forKey:@"value"];
            }
            counterForOtherInfo += 1;
        }
        NSString *valueString = [valueObject componentsJoinedByString:@", "];
        NSMutableDictionary *newField = [[NSMutableDictionary alloc] init];
        [newField setObject:[keyObject firstObject] forKey:@"key"];
        [newField setObject:valueString forKey:@"value"];
//        [newField setValue:valueString forKey:[keyObject firstObject]];
        [additionalInfo addObject:newField];
    }
    counterForOtherInfo = 0;
    [keyObject removeAllObjects];
    [valueObject removeAllObjects];
    if(lesson.campuses){
        
        for (TMELessonAdditionalInfo *additionalInfoFieldObject in lesson.campuses) {
            
            NSMutableDictionary *field = [[NSMutableDictionary alloc]init];
            if (additionalInfoFieldObject.label != nil && counterForOtherInfo < 1) {
                [field setObject:additionalInfoFieldObject.label forKey:@"key"];
                [keyObject addObject:additionalInfoFieldObject.label];
            }
            else{
                [field setObject:NotIndicatedInfo forKey:@"key"];
                [keyObject addObject:@"NotIndicatedInfo"];
            }
            if (additionalInfoFieldObject.title != nil) {
                [field setObject:@"nil" forKey:@"value"];
                [valueObject addObject:additionalInfoFieldObject.title];
            }else{
                [field setObject:NotIndicatedInfo forKey:@"value"];
            }
            counterForOtherInfo += 1;
        }
        NSString *valueString = [valueObject componentsJoinedByString:@", "];
        NSMutableDictionary *newField = [[NSMutableDictionary alloc] init];
        [newField setObject:[keyObject firstObject] forKey:@"key"];
        [newField setObject:valueString forKey:@"value"];
        //        [newField setValue:valueString forKey:[keyObject firstObject]];
        [additionalInfo addObject:newField];
    }
    counterForOtherInfo = 0;
    [keyObject removeAllObjects];
    [valueObject removeAllObjects];

    if(lesson.subjects){
        
        for (TMELessonAdditionalInfo *additionalInfoFieldObject in lesson.subjects) {
            
            NSMutableDictionary *field = [[NSMutableDictionary alloc]init];
            if (additionalInfoFieldObject.label != nil && counterForOtherInfo < 1) {
                [field setObject:additionalInfoFieldObject.label forKey:@"key"];
                [keyObject addObject:additionalInfoFieldObject.label];
            }
            else{
                [field setObject:NotIndicatedInfo forKey:@"key"];
                [keyObject addObject:@"NotIndicatedInfo"];
            }
            if (additionalInfoFieldObject.title != nil) {
                [field setObject:additionalInfoFieldObject.title forKey:@"value"];
                [valueObject addObject:additionalInfoFieldObject.title];
            }else{
                [field setObject:NotIndicatedInfo forKey:@"value"];
            }
            counterForOtherInfo += 1;
        }
        NSString *valueString = [valueObject componentsJoinedByString:@", "];
        NSMutableDictionary *newField = [[NSMutableDictionary alloc] init];
        [newField setObject:[keyObject firstObject] forKey:@"key"];
        [newField setObject:valueString forKey:@"value"];
        //        [newField setValue:valueString forKey:[keyObject firstObject]];
        [additionalInfo addObject:newField];
    }

    counterForOtherInfo = 0;
    [keyObject removeAllObjects];
    [valueObject removeAllObjects];
    
    if(lesson.levels){
        
        for (TMELessonAdditionalInfo *additionalInfoFieldObject in lesson.levels) {
            
            NSMutableDictionary *field = [[NSMutableDictionary alloc]init];
            if (additionalInfoFieldObject.label != nil && counterForOtherInfo < 1) {
                [field setObject:additionalInfoFieldObject.label forKey:@"key"];
                [keyObject addObject:additionalInfoFieldObject.label];
            }
            else{
                [field setObject:NotIndicatedInfo forKey:@"key"];
                [keyObject addObject:@"NotIndicatedInfo"];
            }
            if (additionalInfoFieldObject.title != nil) {
                [field setObject:@"nil" forKey:@"value"];
                [valueObject addObject:additionalInfoFieldObject.title];
            }else{
                [field setObject:NotIndicatedInfo forKey:@"value"];
            }
            counterForOtherInfo += 1;
        }
        NSString *valueString = [valueObject componentsJoinedByString:@", "];
        NSMutableDictionary *newField = [[NSMutableDictionary alloc] init];
        [newField setObject:[keyObject firstObject] forKey:@"key"];
        [newField setObject:valueString forKey:@"value"];
        //        [newField setValue:valueString forKey:[keyObject firstObject]];
        [additionalInfo addObject:newField];
    }
    
    //set up publish date and deadline
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MMM d, yyyy"];
    
    NSString *publishDateString;
    if ([dateFormatter stringFromDate:lesson.publishDate]) {
        publishDateString = [dateFormatter stringFromDate:lesson.publishDate];
    }else{
        publishDateString = UnknownDate;
    }
    
    NSString *deadlineString;
    if ([dateFormatter stringFromDate:lesson.deadline]) {
        deadlineString = [dateFormatter stringFromDate:lesson.deadline];
    }else{
        deadlineString = UnknownDate;
    }

    NSMutableDictionary *publishDateField = [[NSMutableDictionary alloc]init];
    [publishDateField setObject:@"Publish Date" forKey:@"key"];
    [publishDateField setObject:publishDateString forKey:@"value"];
    [additionalInfo addObject:publishDateField];
    
    NSMutableDictionary *deadlineDateField = [[NSMutableDictionary alloc]init];
    [deadlineDateField setObject:@"To Date" forKey:@"key"];
    [deadlineDateField setObject:deadlineString forKey:@"value"];
    [additionalInfo addObject:deadlineDateField];
    
    return additionalInfo;
}

- (void)coverImageClicked {
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil
                                                       delegate:self
                                              cancelButtonTitle:nil
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:nil];
    viewActionIndex = [popup addButtonWithTitle:@"View Image"];
    uploadActionIndex = self.has_edit_access ? [popup addButtonWithTitle:@"Upload A New Photo"] : -1;
    popup.cancelButtonIndex = [popup addButtonWithTitle:TMELocalize(@"button.cancel")];
    popup.tag = coverImageActionSheetTag;
    [popup showFromRect:self.coverImageView.frame inView:self.view animated:YES];
}

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (popup.tag == coverImageActionSheetTag) {
        if (buttonIndex == popup.cancelButtonIndex){
            NSLog(@"event cancelled");
        }
        else if (buttonIndex == viewActionIndex){
            // Create array of MWPhoto objects
            self.photos = [NSMutableArray array];
            if (self.lesson.coverImageURL != nil) {
                [self.photos addObject:[MWPhoto photoWithURL:self.lesson.coverImageURL]];
            }
            else{
                
            }
            
            
            // Create browser (must be done each time photo browser is
            // displayed. Photo browser objects cannot be re-used)
            MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];

            // Set options
            browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
            browser.displayNavArrows = NO; // Whether to display left and right nav arrows on toolbar (defaults to NO)
            browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
            browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
            browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
            browser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
            browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
            browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
            
            // Optionally set the current visible photo before displaying
            [browser setCurrentPhotoIndex:1];
            
            // Present
            [self.navigationController pushViewController:browser animated:YES];
            
            // Manipulate
            [browser showNextPhotoAnimated:YES];
            [browser showPreviousPhotoAnimated:YES];
            [browser setCurrentPhotoIndex:10];
        
        }
        else if (buttonIndex == uploadActionIndex) {
            UIImagePickerController * picker = [[UIImagePickerController alloc] init];
            picker.delegate=self;
            [picker setSourceType:(UIImagePickerControllerSourceTypePhotoLibrary)];
            [self presentViewController:picker animated:YES completion:Nil];
        }
    }
    else if (popup.tag == lessonEditActionSheetTag) {
        if (buttonIndex == 0) {
            NSLog(@"You pressed button at index 0");
            [[TMEClient sharedClient]
             request:TeamieUserLessonToggleStatusRequest
             parameters:@{@"lesson_id":self.lesson.nid}
             makeURL:^NSString *(NSString *URL) {
                 return [NSString stringWithFormat:URL, self.lessonID];
             }
             loadingMessage:TMELocalize(@"message.processing")
             success:^(NSDictionary *response) {
                 [[TMEClient sharedClient]
                  request:TeamieUserLessonRequest
                  parameters:nil
                  makeURL:^NSString *(NSString *URL) {
                      return [NSString stringWithFormat:URL, self.lessonID];
                  }
                  loadingMessage:TMELocalize(@"message.processing")
                  success:^(NSDictionary *response) {
                      self.lesson = [TMELesson parseDictionary:response[@"lesson"]];
                      LessonPageViewController *lessonPageViewController = [[LessonPageViewController alloc]init];
                      lessonPageViewController.lessonsList = self.lesson.lesson_pages;
                      lessonPageViewController.lessonID = self.lessonID;
                      [self.viewForAuthorField setNeedsDisplay];
                  }
                  failure:nil];
                 
             }
             failure:nil];
        }
    }
}

//-(void)imagePickerController:(UIImagePickerController *)picker
//      didFinishPickingImage : (UIImage *)image
//                 editingInfo:(NSDictionary *)editingInfo
-(void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    UIImage *image = [info valueForKey:UIImagePickerControllerOriginalImage];
    self.imageToReplace = image;
    NSData *newImageData = [UIImagePNGRepresentation(image) base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];
    NSString *imageDataBase64String = [NSString stringWithUTF8String:[newImageData bytes]];
//    NSString *imageDataBase64String = [NSString stringWithFormat:@"%@.png", imageDataBase64Strings];
   
    //User current time stamp as the image name
//    NSString *imageName =  [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    
    NSMutableDictionary *lessonParams = [[NSMutableDictionary alloc]init];
    
//    NSMutableDictionary *dict = [[NSMutableDictionary alloc]init];
    
  //  [dict setObject:imageName forKey:@"filename"];
    //[dict setObject:imageDataBase64String forKey:@"file"];
//    [lessonParams setObject:dict forKey:@"image"];
//    [lessonParams setObject:@"new titlessssssss" forKey:@"title"];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    [lessonParams setObject:imageDataBase64String forKey:@"image[file]"];
    [lessonParams setObject:dateString forKey:@"image[filename]"];

    [picker dismissViewControllerAnimated:YES completion:nil];
    [[TMEClient sharedClient]
     request:TeamieUserLessonUpdateRequest
     parameters:lessonParams
     makeURL:^NSString *(NSString *URL) {
         return [NSString stringWithFormat:URL, self.lesson.nid];
     }
     loadingMessage:TMELocalize(@"message.loading")
     success:^(NSDictionary *response) {
         if (self.imageToReplace) {
             [self.coverImageView.coverImage setImage:self.imageToReplace];
         }
     } failure:nil];
}
#pragma mark - IBAction
- (IBAction)startReading:(id)sender {
    if([[self.tempLesson objectForKey:@"next"] isKindOfClass:[NSDictionary class]]){
        NSURL *nextURLPath = [[self.tempLesson objectForKey:@"next"] valueForKey:@"href"];
        
        if(nextURLPath){
            NSString *lastPath = [nextURLPath  lastPathComponent];
            
            LessonPageViewController *lessonPageViewController = [[LessonPageViewController alloc]init];
            lessonPageViewController.lessonsList = self.lesson.lesson_pages;
            lessonPageViewController.lessonID = self.lessonID;
            
            NSInteger currentPage = 0;
            
            for (int i=0; i<self.lesson.lesson_pages.count; i++) {
                TMELessonPageShort *lessonPage = [self.lesson.lesson_pages objectAtIndex:i];
                NSNumber *lessonID = lessonPage.nid;
                if ([lastPath isEqualToString:[NSString stringWithFormat:@"%d",[lessonID intValue]]]) {
                    currentPage = i;
                }
            }
            
            lessonPageViewController.currentPage = currentPage;
            
            [self.navigationController pushViewController:lessonPageViewController animated:YES];
        }
    }
    else{
        //        LessonPageViewController *lessonPageViewController = [[LessonPageViewController alloc] init];
        
        
    }
    

}
- (IBAction)editLessonButtonPressed:(id)sender {
    if (self.has_edit_access) {
        UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:nil
                                                           delegate:self
                                                  cancelButtonTitle:nil
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:nil];
        toggleStatusActionIndex = [popup addButtonWithTitle:[self.lesson.status boolValue]? TMELocalize(@"button.unpublish-lesson") : TMELocalize(@"button.publish-lesson")];
        popup.cancelButtonIndex = [popup addButtonWithTitle:TMELocalize(@"button.cancel")];
        popup.tag = lessonEditActionSheetTag;
        [popup showFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
    }
}

- (void)swipeableTableViewCell:(PagesTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [self.pagesField.pagesTableView indexPathForCell:cell];
    TMELessonPageShort *lessonPage = [self.lesson.lesson_pages objectAtIndex:indexPath.row];
    
    BOOL edit_access  = NO;
    BOOL delete_access = NO;
    for (UserActions *action in lessonPage.actions) {
        if ([action.name isEqualToString:@"edit"]) {
            edit_access = YES;
        }else if ([action.name isEqualToString:@"delete"]){
            delete_access = YES;
        }
    }
    
    BOOL enableClicked = NO;
    BOOL deleteClicked = NO;
    if (edit_access == YES && delete_access == YES) {
        if (index == 0) {
            enableClicked = YES;
        }else if (index == 1){
            deleteClicked = YES;
        }
    }else if(edit_access == YES){
        if (index == 0) {
            enableClicked = YES;
        }
    }else if (delete_access == YES){
        if (index == 0) {
            delete_access = YES;
        }
    }
    
    if (enableClicked == YES) {
        
        self.currentCellEnabled = cell;
        self.currentCellEnableButtonIndex = (int)index;
        
        [cell hideUtilityButtonsAnimated:YES];
        
        NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
        [params setValue:[NSString stringWithFormat:@"%qi", [lessonPage.nid longLongValue]] forKey:@"lesson_id"];

        [[TMEClient sharedClient]
         request:TeamieUserLessonToggleStatusRequest
         parameters:params
         loadingMessage:TMELocalize(@"message.loading")
         success:^(NSDictionary *response) {
             NSIndexPath *indexPath = [self.pagesField.pagesTableView indexPathForCell:self.currentCellEnabled];
             TMELessonPageShort *lessonPage = [self.lesson.lesson_pages objectAtIndex:indexPath.row];
             if ([lessonPage.status boolValue] == YES) {
                 [(NSMutableArray *)self.currentCellEnabled.rightUtilityButtons setButtonTitleColorAtIndex:self.currentCellEnableButtonIndex color:[UIColor colorWithHex:draftedColorHex alpha:1.0]];
             }else{
                 [(NSMutableArray *)self.currentCellEnabled.rightUtilityButtons setButtonTitleColorAtIndex:self.currentCellEnableButtonIndex color:[UIColor colorWithHex:publishedColorHex alpha:1.0]];
             }
         } failure:nil];
    }else if (deleteClicked == YES){
        self.indexPathOfDeletedCell = indexPath;
        [[TMEClient sharedClient]
         request:TeamieUserLessonDeleteRequest
         parameters:nil
         makeURL:^NSString *(NSString *URL) {
             return [NSString stringWithFormat:URL, lessonPage.nid];
         }
         loadingMessage:TMELocalize(@"message.loading")
         success:^(NSDictionary *response) {
             NSMutableArray *lesson_pages = [NSMutableArray arrayWithArray:self.lesson.lesson_pages];
             [lesson_pages removeObjectAtIndex:self.indexPathOfDeletedCell.row];
             self.lesson.lesson_pages = lesson_pages;
             
             [self.myCollapseClick changeToTitle:[NSString stringWithFormat:@"%lu pages",(unsigned long)self.lesson.lesson_pages.count] cellIndex:LessonPagesSection];
             [self.pagesField.pagesTableView deleteRowsAtIndexPaths:@[self.indexPathOfDeletedCell] withRowAnimation:UITableViewRowAnimationLeft];
             
             [self.pagesField.pagesTableView reloadData];
             dispatch_async(dispatch_get_main_queue(), ^{
                 [self updatePagesField];
                 [self updatePagesStatsLabel];
                 [self.myCollapseClick reframeCell:LessonPagesSection withContent:self.pagesField];
             });
         } failure:nil];
    }
}

- (void)updatePagesField
{
    [self.pagesField.pagesTableView sizeToFit];
    
    CGRect frame = self.pagesField.frame;
    frame.size = self.pagesField.pagesTableView.frame.size;
    self.pagesField.frame = frame;
}

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < self.photos.count)
        return [self.photos objectAtIndex:index];
    return nil;
}

- (void)updatePagesStatsLabel
{
    UIFont *font = [TeamieGlobals appFontFor:@"AppFontForPurposeCellLabelText"];
    NSString *pageReadStatsString = [NSString stringWithFormat:@"%@ pages read",self.lesson.num_pages_read_by_current_user];
    CGSize expectedTextSize = [pageReadStatsString sizeWithFont:font];
    
    CGSize expectedSize = CGSizeMake(expectedTextSize.width + 2 * PageStatsLeftRightPaddings, expectedTextSize.height);
    
    //Step 1, keep the origin, change the frame size to expected size
    CGRect frame = self.pageReadStatsLabel.frame;
    CGPoint oldOrigin = frame.origin;
    CGFloat oldFrameWidth = frame.size.width;
    CGFloat centerY = self.pageReadStatsLabel.center.y;
    frame.size = expectedSize;
    
    
    //Step 2, find the width offset
    CGFloat newFrameWidth = frame.size.width;
    CGFloat widthOffset = newFrameWidth - oldFrameWidth;
    
    //Step 3,move the origin left with the same width offset.
    CGPoint newOrigin = CGPointMake(oldOrigin.x - widthOffset, oldOrigin.y);
    frame.origin = newOrigin;
    
    
    //Step 4, configure the UIlabel
    self.pageReadStatsLabel.font = font;
    self.pageReadStatsLabel.text = pageReadStatsString;
    self.pageReadStatsLabel.textColor = [UIColor colorWithHex:@"#ffffff" alpha:1.0];
    
    self.pageReadStatsLabel.frame = frame;
    self.pageReadStatsLabel.layer.cornerRadius = 5.0;
    self.pageReadStatsLabel.layer.masksToBounds = YES;
    self.pageReadStatsLabel.backgroundColor = [UIColor colorWithHex:@"#5f6f8c" alpha:1.0];
    self.pageReadStatsLabel.baselineAdjustment = UIBaselineAdjustmentAlignBaselines;
    self.pageReadStatsLabel.textAlignment = NSTextAlignmentCenter;
    //    pageReadStatsLabel.textAlignment = nste
    
    //Step 5,move the center back to the middle area
    self.pageReadStatsLabel.center = CGPointMake(self.pageReadStatsLabel.center.x, centerY);
}

- (void)processAccess:(NSArray *)actions
{
    self.has_edit_access = NO;
    self.has_delete_access = NO;
    self.has_toggle_access = NO;
    for (UserActions *action in actions) {
        if ([action.name isEqualToString:@"edit"]) {
            self.has_edit_access = YES;
        }else if ([action.name isEqualToString:@"delete"]){
            self.has_delete_access = YES;
        }else if ([action.name isEqualToString:@"toggle"]){
            self.has_toggle_access = YES;
        }
    }
}

- (void)processLessonResponse {
    self.additionalInfoFields = [self additionalInfoOfLesson:self.lesson];
    
    self.infoField.infoTable.delegate = self;
    self.infoField.infoTable.dataSource = self;
    self.infoField.infoTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    
    self.classroomFieldView.classroomTable.delegate = self;
    self.classroomFieldView.classroomTable.dataSource = self;
    self.classroomFieldView.classroomTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.pagesField.pagesTableView.delegate = self;
    self.pagesField.pagesTableView.dataSource = self;
    self.pagesField.pagesTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.myCollapseClick.CollapseClickDelegate = self;
    
    
    [self.coverImageView.coverImage sd_setImageWithURL:self.lesson.coverImageURL];
    [self.coverImageView.button addTarget:self action:@selector(coverImageClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    [self.myCollapseClick reloadCollapseClick];
    self.myCollapseClick.backgroundColor = [UIColor colorWithHex:@"#cccccc" alpha:1];
    
    
    self.navigationItem.title = self.lesson.title;
    //TO DO Increase the size of the pages title name here that appear in the lessons landing page collapse click menu
    [self.readButton setHidden:NO];
    self.readButton.frame = CGRectMake(0, (self.view.bounds.size.height) - 48, CGRectGetWidth(self.view.frame), 48);
    self.readButton.titleLabel.font = [UIFont fontWithName:@"FontAwesome" size:15];
    NSString *title = [NSString stringWithFormat:@"Read %@",[NSString fontAwesomeIconStringForIconIdentifier:@"icon-arrow-right"]];
    [self.readButton setTitle:title forState:UIControlStateNormal];
    [self.readButton setTintColor:[UIColor colorWithHex:@"#ffffff" alpha:1.0]];
    if ([self.lesson.lesson_pages count] < 1) {
        [self.readButton setBackgroundColor:[UIColor colorWithHex:@"#96A1B2" alpha:1.0]];
    }else{
        [self.readButton setBackgroundColor:[UIColor colorWithHex:@"#5f6f8c" alpha:1.0]];
    }
    self.readButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
}

-(void)tapDetectedOnAuthorImage{
    NSLog(@"single Tap on imageview");
    UIViewController *frontViewController;
    frontViewController = [[UserProfileViewController alloc] initWithUserID: [self.authorID integerValue]];
    [self.navigationController pushViewController:frontViewController animated:YES];
}


@end
