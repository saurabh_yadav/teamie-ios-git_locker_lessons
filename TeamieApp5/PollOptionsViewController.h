//
//  PollOptionsViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 23/05/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PollItemCell.h"
#import "NewPollItemCell.h"

@protocol PollOptionsViewControllerDelegate
- (void)pollOptionsViewControllerDidDismiss;
@end

@interface PollOptionsViewController : UIViewController <UITableViewDelegate,UIScrollViewDelegate,UITableViewDataSource,NSFetchedResultsControllerDelegate,PollItemCellDelegate>

// Main table view controller , containing items
@property (weak, nonatomic) IBOutlet UITableView *tableView;
// Tableview-s Navigationbar
@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;
// tableviews navigationItem
@property (weak, nonatomic) IBOutlet UINavigationItem *tableNavigationItem;

@property (nonatomic,strong) UIPopoverController *popover;

@property (nonatomic,weak) id<PollOptionsViewControllerDelegate> delegate;

// set/unset editing to tableview
-(IBAction)editButtonAction:(id)sender;
- (IBAction)dismissPollModal:(id)sender;
- (IBAction)handleLongPress:(id)sender;


//Core Data ObjectContext
@property (nonatomic,strong) NSManagedObjectContext *context;
//Item Entity
@property (nonatomic,strong) NSEntityDescription *entity;
//fetched results for Item entities
@property (strong,nonatomic) NSFetchedResultsController *fetchedResultsController;

//first cell , using this cell we can add new items ,
@property (strong,nonatomic) NewPollItemCell *addItemCell;


@end
