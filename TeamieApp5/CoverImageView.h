//
//  CoverImageView.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 24/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoverImageView : UIView
@property (weak, nonatomic) IBOutlet UIButton *button;
@property (weak, nonatomic) IBOutlet UIImageView *coverImage;

@end
