//
//  SiteInfo.h
//  TeamieApp5
//
//  Created by Raunak on 24/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"

@interface TMESiteInfo : TMEModel

@property (nonatomic,strong) NSString* baseURL;
@property (nonatomic,strong) NSString* siteName;
@property (nonatomic,strong) NSString* siteLogoURL;
@property (nonatomic, strong) NSString* siteSlogan;
@property (nonatomic, strong) NSString* siteSubSlogan;
@property (nonatomic, strong) NSString* isTeamieLoginSupported;
@property (nonatomic, strong) NSString* isGoogleLoginSupported;
@property (nonatomic, strong) NSString* isTeamieRegisterSupported;

@end