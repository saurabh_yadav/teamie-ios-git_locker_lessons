//
//  UserBadgesViewController.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 9/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserBadgesViewController : UICollectionViewController

- (id)initWithBadges:(NSArray *)badges;
@end
