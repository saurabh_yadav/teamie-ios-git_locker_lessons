//
//  TeamieUserBadge.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 7/1/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "TeamieUserBadge.h"

@implementation TeamieUserBadge

@synthesize bid, name, awardedByUser, badgeImageURL, awardedOn;

- (NSString*)description {
    return [NSString stringWithFormat:@"BID: %qi \nName: %@ \nAwarded By User: %qi \nImage: %@ \nOn: %@ \n", [self.bid longLongValue], self.name, [self.awardedByUser longLongValue], self.badgeImageURL, self.awardedOn];
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.bid forKey:@"bid"];
    [encoder encodeObject:self.name forKey:@"name"];
    [encoder encodeObject:self.awardedByUser forKey:@"awardedByUser"];
    [encoder encodeObject:self.badgeImageURL forKey:@"badgeImageURL"];
    [encoder encodeObject:self.awardedOn forKey:@"awardedOn"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.bid = [decoder decodeObjectForKey:@"bid"];
        self.name = [decoder decodeObjectForKey:@"name"];
        self.awardedByUser = [decoder decodeObjectForKey:@"awardedByUser"];
        self.badgeImageURL = [decoder decodeObjectForKey:@"badgeImageURL"];
        self.awardedOn = [decoder decodeObjectForKey:@"awardedOn"];
    }
    return self;
}

@end
