//
//  CalendarManager.m
//  TeamieApp5
//
//  Created by Raunak on 15/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "CalendarManager.h"

@interface CalendarManager (){
    BOOL syncAllowed;
}

@property (nonatomic, strong) EKEventStore* store;
@property (nonatomic, strong) EKCalendar* calendar;
@property (nonatomic, strong) NSString* calendarIdentifier;

@end

@implementation CalendarManager

static CalendarManager* manager = nil;

+(CalendarManager *)defaultCalendar {
    @synchronized([CalendarManager class]) {
        if (!manager) {
            manager = [[self alloc] init];
        }
        return manager;
    }
    return nil;
}

+(id)alloc {
    @synchronized([CalendarManager class]) {
        manager = [super alloc];
        return manager;
    }
    return nil;
}

-(id)init {
    self = [super init];
    if (self) {
        _store = [[EKEventStore alloc] init];
        syncAllowed = YES;
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")) {
            [_store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError* error) {
                syncAllowed = granted;
            }];
        }
        if ([[NSUserDefaults standardUserDefaults] objectForKey:@"calendar_id"]) {
            _calendarIdentifier = [[NSUserDefaults standardUserDefaults] objectForKey:@"calendar_id"];
        }
    }
    return self;
}

- (BOOL)addDeadlineWithID:(NSNumber *)eventID title:(NSString *)title date:(NSDate *)date description:(NSString *)description {
    if (syncAllowed) {
        EKEvent* newEvent = [EKEvent eventWithEventStore:self.store];
        newEvent.title = title;
        newEvent.notes = description;
        newEvent.startDate = date;
        newEvent.endDate = date;
        newEvent.calendar = self.calendar;
        [self.store saveEvent:newEvent span:EKSpanThisEvent error:nil];
    }
}

@end
