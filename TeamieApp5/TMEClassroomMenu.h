//
//  ClassroomMenu.h
//  TeamieApp5
//
//  Created by Raunak on 24/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"

@interface TMEClassroomMenu : TMEModel

@property (nonatomic,strong) NSString* lessonTitle;
@property (nonatomic,strong) NSString* lessonHref;
@property (nonatomic,strong) NSString* quizTitle;
@property (nonatomic,strong) NSString* quizHref;
@property (nonatomic,strong) NSString* gradebookTitle;
@property (nonatomic,strong) NSString* gradebookHref;
@property (nonatomic,strong) NSString* leaderboardTitle;
@property (nonatomic,strong) NSString* leaderboardHref;
@property (nonatomic,strong) NSString* memberTitle;
@property (nonatomic,strong) NSString* memberHref;

@end
