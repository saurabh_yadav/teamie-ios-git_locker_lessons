//
//  MessageThread.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/25/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "MessageThread.h"
#import "MessageItem.h"
#import "UserProfile.h"


@implementation MessageThread

@dynamic is_new;
@dynamic lastUpdated;
@dynamic subject;
@dynamic tid;
@dynamic messageItems;
@dynamic participants;

@end
