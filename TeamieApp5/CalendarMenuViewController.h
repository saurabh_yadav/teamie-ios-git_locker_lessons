//
//  CalendarMenuViewController.h
//  TeamieApp5
//
//  Created by Raunak on 16/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"

@protocol CalendarMenuDelegate
- (void) menuDidSelectObjectWithData:(id)data;
- (void) menuDidDeselectObjectWithData:(id)data;
@end

@interface CalendarMenuViewController : TableRestApiViewController
@property (nonatomic, strong) id<CalendarMenuDelegate> menuDelegate;
- (void)setNumOfMenuItems:(NSInteger)maxMenuItems;

@end
