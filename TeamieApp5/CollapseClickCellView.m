//
//  CollapseClickCellView.m
//  TeamieApp5
//
//  Created by Raunak on 4/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "CollapseClickCellView.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImage-Extensions.h"

@implementation CollapseClickCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (CollapseClickCellView*)newCollapseClickCellWithDetails:(QuestionPageViewController *)qpVC maxScore:(NSNumber *)maxScore index:(int)index {
    
    NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"CollapseClickCellView" owner:nil options:nil];
    CollapseClickCellView *cell = [[CollapseClickCellView alloc] initWithFrame:CGRectMake(10, 0, 748, kCCHeaderHeight)];
    cell = [views objectAtIndex:0];
    cell.layer.cornerRadius = 10.0;
    
    // Initialization Here
    cell.nameLabel.text = qpVC.userDetails.user.realName;
    [cell.nameLabel sizeToFit];
    
    cell.lateLabel.frame = CGRectMake(CGRectGetMaxX(cell.nameLabel.frame) + 10.0, CGRectGetMinY(cell.lateLabel.frame), CGRectGetWidth(cell.lateLabel.frame), CGRectGetHeight(cell.lateLabel.frame));
    cell.lateLabel.layer.cornerRadius = 4.0;
    cell.lateLabel.hidden = !qpVC.userDetails.isDelayed;
    
    // check for divide by zero error
    cell.scoreBarView.barRatio = ([maxScore intValue] == 0) ? 0 : [qpVC.userDetails.totalScore doubleValue] / [maxScore doubleValue];
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MMM dd, yyyy"];

    cell.submittedLabel.text = [NSString stringWithFormat:@"Submitted on %@ (%@)", [dateFormat stringFromDate:[NSDate dateWithTimeIntervalSince1970:[qpVC.userDetails.submittedTime doubleValue]]], [TeamieGlobals timeIntervalWithStartTime:[qpVC.userDetails.submittedTime doubleValue] withEndTime:0]];
    
    cell.completedLabel.text = [NSString stringWithFormat:@"Completed in %@, %@ attempt", [TeamieGlobals timeIntervalToText:[qpVC.userDetails.completionTime doubleValue]], [TeamieGlobals stringFromInteger:[qpVC.userDetails.attemptNumber integerValue]]];
    cell.completedLabel.adjustsFontSizeToFitWidth = YES;
    
    UIImage* img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:qpVC.userDetails.user.displayPic]]];
    cell.displayImageView.image = [img imageByScalingProportionallyToSize:CGSizeMake(65, 65)];
    cell.displayImageView.clipsToBounds = YES;
    cell.displayImageView.layer.cornerRadius = 5.0;
    cell.displayImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    cell.displayImageView.layer.borderWidth = 1.0;
    
    cell.scoreBoxView.userScoreLabel.text = [qpVC.userDetails.totalScore stringValue];
    cell.scoreBoxView.maxScoreLabel.text = [maxScore stringValue];
    cell.scoreBoxView.clipsToBounds = YES;
    cell.scoreBoxView.layer.cornerRadius = 8.0;
    cell.scoreBoxView.layer.borderColor = [UIColor blackColor].CGColor;
    cell.scoreBoxView.layer.borderWidth = 0.5;
    
    cell.gradedLabel.layer.cornerRadius = 4.0;
//    cell.gradedLabel.hidden = YES;
    cell.gradedLabel.backgroundColor = [UIColor grayColor];
    cell.gradedLabel.text = @"GRADED";
    
    for (SubmissionAnswer* submissionAnswer in qpVC.userDetails.submissionAnswers) {
        if (!submissionAnswer.isGraded) {
//            cell.gradedLabel.hidden = NO;
            cell.gradedLabel.backgroundColor = [UIColor blueColor];
            cell.gradedLabel.text = @"YET TO GRADE";
        }
    }
    
    cell.index = index;
    cell.headerButton.tag = index;
//    cell.contentView.frame = CGRectMake(10, cell.contentView.frame.origin.y, CGRectGetWidth(cell.frame) - 20, qpVC.view.frame.size.height);
    cell.contentView.frame = CGRectMake(10, cell.contentView.frame.origin.y, CGRectGetWidth(cell.frame) - 20, 420);
//    [cell.contentView addSubview:qpVC.view];
    
    return cell;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
