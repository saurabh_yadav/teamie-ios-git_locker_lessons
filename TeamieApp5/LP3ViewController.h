//
//  LP3ViewController.h
//  LS2
//
//  Created by Wei Wenbo on 11/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LP3ViewController : UIViewController<UIWebViewDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property IBOutlet UIWebView *webview;
@end
