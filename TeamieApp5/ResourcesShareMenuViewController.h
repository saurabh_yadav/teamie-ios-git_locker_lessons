//
//  ResourcesShareMenuViewController.h
//  TeamieApp5
//
//  Created by Raunak on 19/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

typedef enum {
    kShare = 0,
    kOpenIn
} ShareMenuOptions;

@protocol ShareMenuControllerDelegate <NSObject>
- (void) shareMenuDidSelectDeleteOption:(ShareMenuOptions)option;

@optional
- (BOOL) shareMenuShouldDisplayOpenInOption;

@end


@interface ResourcesShareMenuViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) id<ShareMenuControllerDelegate> delegate;
@end
