//
//  ShareBoxViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/16/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PopupMenuViewController.h"
#import "WEPopoverController.h"
#import "DrawScribbleViewController.h"
#import "PollOptionsViewController.h"

@protocol ShareBoxControllerDelegate
-(void)shareBoxDidExit:(NSNumber*)thoughtPostStatus;
@end

@interface ShareBoxViewController : UIViewController <UIPickerViewDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UIPopoverControllerDelegate, PopoverMenuDelegate, WEPopoverControllerDelegate, PollOptionsViewControllerDelegate, WSCoachMarksViewDelegate> {
    BOOL _isThoughtPosted;
    CGRect connectionPopoverInitFrame;
    Class popoverClass;
    BOOL _isThoughtAnonymous;
}

@property (weak, nonatomic) IBOutlet UITextView *thoughtTextView;
@property (weak, nonatomic) IBOutlet UIPickerView *classroomSelector;
@property (weak, nonatomic) IBOutlet UIButton *selectedClassroom;
@property (weak, nonatomic) IBOutlet UISegmentedControl *thoughtQuestionFlag;
@property (weak, nonatomic) IBOutlet UIImageView *userDisplayPic;
@property (weak, nonatomic) IBOutlet UILabel *thoughtQuestionFlagLabel;
@property (weak, nonatomic) IBOutlet UIToolbar *thoughtActionsBar;
@property (weak, nonatomic) IBOutlet UIToolbar *topActionsBar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *thoughtCameraOption;

@property (nonatomic, retain) NSMutableArray * classroomList;
@property (nonatomic, retain) NSNumber * defaultClassroom;
@property (nonatomic, retain) NSString * attachmentImageName;
@property (nonatomic, retain) NSData * attachmentImageData;
@property (nonatomic, retain) NSString* attachmentImageBase64String;
@property (nonatomic, retain) NSString * attachmentLink;
@property (nonatomic, retain) NSArray * attachmentPollOptions;
@property (nonatomic, retain) NSData* attachmentFileData;
@property (nonatomic, retain) NSString* attachmentFileName;
@property (nonatomic, retain) NSString* attachmentFileBase64String;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *postThoughtButton;

// attachment Preview pane outlets
@property (weak, nonatomic) IBOutlet UIView *attachmentPreview;
@property (weak, nonatomic) IBOutlet UIImageView *attachmentPreviewThumbnail;
@property (weak, nonatomic) IBOutlet UILabel *attachmentPreviewTitle;
@property (weak, nonatomic) IBOutlet UILabel *attachmentPreviewMeta;

- (IBAction)classroomSelectorPressed:(id)sender;
- (IBAction)postThought:(id)sender;
- (IBAction)thoughtOptionSelected:(UIBarButtonItem*)sender;
- (void)attachmentPreviewSelected:(UITapGestureRecognizer*)sender;
- (IBAction)dismissShareBox:(id)sender;
- (IBAction)thoughtQuestionFlagValueChanged:(id)sender;

- (void)updateDefaultClassroom:(NSNumber*)classID;
- (void)setImageData:(UIImage *)viewContextInImage;
- (void)getListOfObjectsInContext:(NSString*)inEntity;

@property (weak, nonatomic) id<ShareBoxControllerDelegate> shareboxControllerDelegate;

@property (strong, nonatomic) UIProgressView* requestProgress;

@property (strong, nonatomic) WEPopoverController * wePopoverController;

@property (nonatomic,strong) NSManagedObjectContext *context;
@property (nonatomic,strong) NSEntityDescription *entity;

@end
