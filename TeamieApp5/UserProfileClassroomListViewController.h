//
//  UserProfileClassroomListViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 08/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileClassroomListViewController : UITableViewController

- (id)initWithClassrooms:(NSArray *)classrooms;

@end
