//
//  ResourcesImageViewController.h
//  TeamieApp5
//
//  Created by Raunak on 16/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ThoughtImageViewController.h"
#import "WEPopoverController.h"
#import "ResourcesShareMenuViewController.h"

@class ResourcesImageViewController;

@protocol ResourcesImageControllerDelegate
- (void)resourcesImageControllerDidSelectShareOption:(ResourcesImageViewController*)imgController;
@end

@interface ResourcesImageViewController : ThoughtImageViewController <UIDocumentInteractionControllerDelegate, ShareMenuControllerDelegate, WEPopoverControllerDelegate>

@property (nonatomic, weak) id<ResourcesImageControllerDelegate> delegate;
@property (nonatomic, strong) NSURL* filePath;
- (void)loadImageWithUrl:(NSString*)url WithCaption:(NSString*)caption WithTitle:(NSString*)title WithThoughtID:(NSNumber*)tid WithFilePath:(NSURL*)url;

@end
