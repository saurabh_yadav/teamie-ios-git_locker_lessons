//
//  TeamieAlternateClient.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEAlternateClient.h"

@implementation TMEAlternateClient

+ (instancetype)sharedClient {
    static dispatch_once_t onceToken;
    static TMEAlternateClient *sharedClient = nil;
    dispatch_once(&onceToken, ^{
        sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:TEAMIE_DOMAIN_LIST_URL]];
    });
    return sharedClient;
}

@end