//
//  TMEResourceAttachment.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEResourceAttachment.h"

@implementation TMEResourceAttachment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
         @"resourceMapId": @"resource_map_id",
     };
}

+ (NSValueTransformer *)resourceMapIdJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

- (UIImage *)previewImage {
    return [TeamieUIGlobals defaultPicForPurpose:@"resource"
                                        withSize:15
                                        andColor:[TeamieUIGlobalColors defaultTextColor]];
}

@end
