//
//  UserCalendarViewController.h
//  TeamieApp5
//
//  Created by Raunak on 14/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "WEPopoverController.h"
#import "CalendarMenuViewController.h"

@interface UserCalendarViewController : UIViewController <TKCalendarDayViewDelegate, TKCalendarDayViewDataSource, TKCalendarMonthViewDelegate, TKCalendarMonthViewDataSource, WEPopoverControllerDelegate, CalendarMenuDelegate, UIActionSheetDelegate>

//Stores all the events to be displayed to the user
@property (nonatomic, retain) NSArray* calendarItems;

@end

