//
//  ClassroomMenu.m
//  TeamieApp5
//
//  Created by Raunak on 24/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "TMEClassroomMenu.h"

@implementation TMEClassroomMenu

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"lessonTitle": @"menu.lesson.title",
        @"lessonHref": @"menu.lesson.href",
        @"quizTitle": @"menu.quiz.title",
        @"quizHref": @"menu.quiz.href",
        @"gradebookTitle": @"menu.gradebook.title",
        @"gradebookHref": @"menu.gradebook.href",
        @"leaderboardTitle": @"menu.leaderboard.title",
        @"leaderboardHref": @"menu.leaderboard.href",
        @"memberTitle": @"menu.member.title",
        @"memberHref": @"menu.member.href",
    };
}

@end
