//
//  QuizSimpleObject.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"
#import "TMEUser.h"

@interface TMEQuizSimple : TMEModel

#pragma mark - Public Properties

@property (nonatomic, retain) NSNumber * qid;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, strong) NSString * webUrl;
@property (nonatomic, strong) NSString * authorName;

@property (nonatomic, retain) NSDate * publishDate;

@property (nonatomic, retain) NSDate   * deadlineDate;
@property (nonatomic, retain) NSString * deadlineTimezone;

@property (nonatomic, retain) NSNumber * totalPeople;
@property (nonatomic, retain) NSNumber * quizTakers;
@property (nonatomic, retain) NSNumber * isGraded;
@property (nonatomic, retain) NSNumber * hasAccess;

@property (nonatomic, strong) NSArray * classrooms;

@property (nonatomic, strong) NSNumber * numberOfQuestions;
@property (nonatomic, retain) NSNumber * weightage;
@property (nonatomic, strong) NSNumber * maximumAttempts;
@property (nonatomic, strong) NSNumber * duration;
@property (nonatomic, retain) NSNumber * numVotes;

@property (nonatomic, retain) NSNumber * numberOfAttempts;

@property (nonatomic, retain) NSNumber * canSubmitLate;
@property (nonatomic, retain) NSNumber * lateTimeAllowed;

@property (nonatomic, retain) NSNumber * totalScore;
@property (nonatomic, retain) NSNumber * highScore;
@property (nonatomic, retain) NSNumber * lowScore;
@property (nonatomic, retain) NSNumber * avgScore;

#pragma mark - Public Methods

/**
 *  Method to generate a custom date format string
 *
 *  @return "`MMM dd HH:MM`" format string
 */
- (NSString *)deadLineDateString;

/**
 *  Publish state of Quiz
 *
 *  @return `YES` if quiz is in draft state
 */
- (BOOL)isDraft;

/**
 *  Returns the text for the classroom label
 *
 *  @return "`Current Classroom Title` + `classroom.count` more"
 */
- (NSString *)classroomsAsStringForClassroomId:(NSNumber *)nid;

/**
 *  Returns the text for the number of questions detail label
 *
 *  @return "`numberOfQuestions` questions". If only 1 question then "1 question"
 */
- (NSString *)numberOfQuestionsString;

/**
 *  Returns text for marks detail label
 *
 *  @return "`totalScore` marks". If no maximum marks then "0 marks"
 */
- (NSString *)marksString;

/**
 *  Converts time in seconds and adds "minutes" at the end
 *
 *  @return "`x` minutes"
 */
- (NSString *)durationString;

/**
 *  Generates attempt count string
 *
 *  @return "`maximumAttempts` attempts" for teacher, "`numberOfAttempts`/`maximumAttempts` attempts" for student
 */
- (NSString *)attemptCountString;

/**
 *  Generates submission label text for submissions made in a quiz
 *
 *  @return "`quizTakers`/`totalPeople` submitted"
 */
- (NSString *)submissionsMadeString;

@end
