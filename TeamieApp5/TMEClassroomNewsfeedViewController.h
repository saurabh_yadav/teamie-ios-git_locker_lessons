//
//  TMEClassroomNewsfeedViewController.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 18/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMENewsfeedViewController.h"

@interface TMEClassroomNewsfeedViewController : TMENewsfeedViewController

@property (nonatomic, strong, readonly) TMEClassroom *classroom;

+ (TMEClassroomNewsfeedViewController *)newsfeedForClassroom:(TMEClassroom *)classroom;

@end
