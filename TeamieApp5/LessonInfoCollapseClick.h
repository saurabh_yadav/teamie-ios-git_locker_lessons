//
//  LessonInfoCollapseClick.h
//  LS2
//
//  Created by Wei Wenbo on 3/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import "CollapseClick.h"
@protocol LessonInfoCollapseClickDelegate<CollapseClickDelegate>

@required
- (void)showLessonPagesReadStats;
- (UIView *)viewForAuthorField;
- (UIView *)viewForCoverImage;
@optional

@end
@interface LessonInfoCollapseClick : CollapseClick
@property (weak) id<LessonInfoCollapseClickDelegate>CollapseClickDelegate;
@property (strong, nonatomic) UIImageView *headerImageView;
@property (strong, nonatomic) UIView *completeReadersField;

- (void)reframeCell:(int)index withContent:(UIView *)content;
- (void)changeToTitle:(NSString *)title cellIndex:(int)index;
@end
