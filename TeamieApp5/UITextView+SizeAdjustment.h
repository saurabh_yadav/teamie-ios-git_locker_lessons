//
//  UITextView+SizeAdjustment.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 5/23/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

@protocol UITextViewWithSizeAdjustmentDelegate <UITextViewDelegate>

@optional
- (void)textviewDidChangeHeight:(UITextView*)textView;

@end

@interface UITextView (SizeAdjustment)

- (void)fitWithMaxNumberOfLines:(int)maxNumLines;

- (void)reloadText;

@end
