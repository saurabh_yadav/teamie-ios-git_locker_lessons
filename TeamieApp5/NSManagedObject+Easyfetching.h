//
//  NSManagedObject+Easyfetching.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/27/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//
//  Class containing helper Core Data methods that make adding, retrieving, updating & deleting records easier

#import "TeamieAppDelegate.h"

#import <CoreData/NSManagedObjectContext.h>
#import <CoreData/NSManagedObject.h>
#import <CoreData/NSEntityDescription.h>
#import <CoreData/NSFetchRequest.h>

@interface NSManagedObject (EasyFetching)

+ (NSEntityDescription *)entityDescriptionInContext:(NSManagedObjectContext *)context;

+ (NSArray *)findAllObjects;
+ (NSArray *)findAllObjectsInContext:(NSManagedObjectContext *)context;

+ (NSArray *)findAllObjectsWithPredicate:(id)predicate;
+ (NSArray *)findAllObjectsWithPredicate:(id)predicate inContext:(NSManagedObjectContext *)context;

+ (NSManagedObject*)findFirstObjectWithPredicate:(id)predicate;
+ (NSManagedObject*)findFirstObjectWithPredicate:(id)predicate inContext:(NSManagedObjectContext*)context;

+ (NSInteger)updateAllObjectsWithPredicate:(id)predicate withValues:(NSDictionary*)values inContext:(NSManagedObjectContext*)context;
+ (NSInteger)updateAllObjectsWithPredicate:(id)predicate withValues:(NSDictionary*)values;

+ (NSEntityDescription*)newObjectWithValues:(NSDictionary*)values;
+ (NSEntityDescription*)newObjectWithValues:(NSDictionary *)values inContext:(NSManagedObjectContext*)context;
+ (NSEntityDescription*)newObjectWithValues:(NSDictionary *)values forEntity:(NSString*)entityName;
+ (NSEntityDescription*)newObjectWithValues:(NSDictionary *)values forEntity:(NSString*)entityName inContext:(NSManagedObjectContext*)context;

+ (void)deleteAllObjects;
+ (void)deleteAllObjectsInContext:(NSManagedObjectContext *)context;

// Methods to save the current context
- (void)saveObject;
- (void)saveObjectInContext:(NSManagedObjectContext *)context;

// Methods to delete current object
- (void)deleteObject;
- (void)deleteObjectInContext:(NSManagedObjectContext *)context;

- (NSManagedObject*)cloneAttributes;
- (NSManagedObject*)cloneAttributesInContext:(NSManagedObjectContext*)context;

@end
