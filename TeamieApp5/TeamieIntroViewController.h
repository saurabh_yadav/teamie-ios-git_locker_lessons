//
//  TeamieIntroViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 27/11/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EAIntroView.h"

@interface TeamieIntroViewController : UIViewController <EAIntroDelegate>

- (void)showIntroWithCrossDissolve;

@end
