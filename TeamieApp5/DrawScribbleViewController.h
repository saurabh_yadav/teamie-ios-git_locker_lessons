//
//  DrawScribbleViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 13/02/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SmoothLineView.h"
#import "InfColorPicker.h"
#import "ShareBoxViewController.h"
#import "WEPopoverController.h"

@interface DrawScribbleViewController : UIViewController <InfColorPickerControllerDelegate, WEPopoverControllerDelegate, UIPopoverControllerDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate, UIActionSheetDelegate>
{
    SmoothLineView* myDrawing;
    Class popoverClass;
}

@property (nonatomic,retain) UIColor *curColor;
@property (nonatomic, weak) id delegate;

// IBOutlets to refer items on the DrawScribbleViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *clearScreenButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *undoLastButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *redoLastButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *colorPickerButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *eraserButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *attachToThoughtButton;
@property (weak, nonatomic) IBOutlet UIView *drawingView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *loadImageButton;

@property (strong, nonatomic) WEPopoverController * wePopoverController;
@property (strong, nonatomic) UIPopoverController* popoverController;
@property (strong, nonatomic) SmoothLineView* myDrawing;

// IBActions to manage the button events
- (IBAction)clearScreenAction:(id)sender;
- (IBAction)undoLastAction:(id)sender;
- (IBAction)redoLastAction:(id)sender;
- (IBAction)colorPickerAction:(id)sender;
- (IBAction)eraserAction:(id)sender;
- (IBAction)attachToThoughtAction:(id)sender;
- (IBAction)cancelAction:(id)sender;
- (IBAction)loadImageAction:(id)sender;

- (void)eraserButtonToggle;
- (void)penButtonToggle;

-(void) setUndoButtonEnable:(NSNumber*)isEnable;
-(void) setRedoButtonEnable:(NSNumber*)isEnable;
-(void) setClearButtonEnable:(NSNumber*)isEnable;
-(void) setEraserButtonEnable:(NSNumber*)isEnable;
-(void) setAttachToThoughtEnable:(NSNumber*)isEnable;

@end
