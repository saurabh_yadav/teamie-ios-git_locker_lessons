//
//  NSMutableArray+PagesUtilityButtons.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 21/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (PagesUtilityButtons)
- (void)addPagesUtilityButtonWithColor:(UIColor *)color icon:(NSString *)icon titleColor:(UIColor *)titleColor;
- (void)setButtonTitleColorAtIndex:(int)index color:(UIColor *)titleColor;
@end
