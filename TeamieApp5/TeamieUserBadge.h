//
//  TeamieUserBadge.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 7/1/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamieUserBadge : NSObject

@property (nonatomic, copy) NSNumber* bid;
@property (nonatomic, copy) NSString* name;
@property (nonatomic, copy) NSNumber* awardedByUser;
@property (nonatomic, copy) NSDate* awardedOn;
@property (nonatomic, copy) NSString* badgeImageURL;

@end
