//
//  TMEPostAttachmentView.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostAttachmentView.h"
#import "TMEImageAttachment.h"
#import "TMEVideoAttachment.h"

#import "UIView+TMEEssentials.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import <Masonry/Masonry.h>

@interface TMEPostAttachmentView()

@property (nonatomic, weak) UIView<TMEPostAttachmentViewDelegate> *delegate;
@property (nonatomic, weak) TMEPostAttachment *attachment;

@end

@implementation TMEPostAttachmentView

static const CGFloat kDefaultPadding = 10.0f;
static const CGFloat kDefaultImagePadding = 15.0f;

- (instancetype)initWithDelegate:(UIView<TMEPostAttachmentViewDelegate> *)delegate
                      attachment:(TMEPostAttachment *)attachment {
    self = [super init];
    if (self) {
        self.delegate = delegate;
        self.attachment = attachment;
        
        [self createSubViews];
        [self loadAttachment];
        [self loadConstraints];
        
        [self addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                           action:@selector(tap)]];
    }
    return self;
}

- (void)createSubViews {
    self.mainTextLabel = [self createAndAddSubView:UILabel.class];
    self.mainTextLabel.font = [TeamieGlobals appFontFor:@"postAttachmentTitle"];
    self.mainTextLabel.textColor = [TeamieUIGlobalColors defaultTextColor];
    
    self.subTextLabel = [self createAndAddSubView:UILabel.class];
    self.subTextLabel.font = [TeamieGlobals appFontFor:@"postAttachmentSubTitle"];
    self.subTextLabel.textColor = [TeamieUIGlobalColors defaultTextColor];
    
    self.previewImageView = [self createAndAddSubView:UIImageView.class];
    self.previewImageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)loadConstraints {
    [self.previewImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).with.offset(kDefaultImagePadding);
        make.top.equalTo(self).with.offset(kDefaultImagePadding);
        make.bottom.equalTo(self).with.offset(-(kDefaultImagePadding));
        make.width.height.equalTo(@(15.0f));
    }];
    [self.mainTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.previewImageView.mas_right).with.offset(kDefaultImagePadding);
        make.top.equalTo(self).with.offset(kDefaultPadding);
        make.right.equalTo(self).with.offset(-kDefaultPadding);
    }];
    [self.subTextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.previewImageView.mas_right).with.offset(kDefaultImagePadding);
        make.top.equalTo(self.mainTextLabel.mas_bottom);
        make.right.equalTo(self).with.offset(-kDefaultPadding);
    }];
}

- (void)loadAttachment {
    if ([self.attachment isKindOfClass:TMEImageAttachment.class]) {
        [self.previewImageView sd_setImageWithURL:((TMEImageAttachment *)self.attachment).thumbnail
                                           placeholderImage:self.attachment.previewImage];
    }
    else {
        self.previewImageView.image = self.attachment.previewImage;
    }

    if ([self.attachment isKindOfClass:TMEVideoAttachment.class] && ((TMEVideoAttachment *)self.attachment).href_mp4) {
        self.href = ((TMEVideoAttachment *)self.attachment).href_mp4;
    }
    else {
        self.href = self.attachment.href;
    }

    self.mainTextLabel.text = self.attachment.title;
    self.subTextLabel.text = self.attachment.displaySubTitle;
}

- (void)tap {
    [self.delegate trackAttachmentClick:self.attachment.attachmentType];
    if (self.href) {
        [TeamieGlobals openInappBrowserWithURL:self.href fromResponder:self];
    }
}

@end
