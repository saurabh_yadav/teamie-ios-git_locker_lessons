//
//  UserProfileViewController.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 7/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UserProfileViewController.h"
#import "UserProfileStatsViewController.h"
#import "UserProfileClassroomListViewController.h"
#import "UserProfileStatsTableViewCell.h"
#import "TMEUser.h"
#import "UserProfileBadgeView.h"
#import "TeamieUserBadge.h"
#import "UIImageView+WebCache.h"
#import "TMEClient.h"
#import "TMEBadge.h"
#import <Masonry/Masonry.h>
#import "TMEClassroomAlt.h"
#import "UIView+TMEEssentials.h"

@interface UserProfileViewController ()
@property (nonatomic) NSNumber *uid;
@property (nonatomic) NSDictionary *profileData;
@property (nonatomic) TMEUserProfile *userData;
@property (nonatomic, weak) UIViewController *parentRevealController;

@end

@implementation UserProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithUserID:(NSInteger)uid
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"UserProfile" bundle:nil];
    
    self = (UserProfileViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    
    if (self) {
        self.uid = [NSNumber numberWithInteger:uid];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.presentingViewController) {
        [TeamieGlobals addBackButtonToViewController:self withCallback:@selector(popNavigationItemAnimated:)];
    }
    
    if (self.uid) {
        [[TMEClient sharedClient]
         getUserProfile:nil
         uid:self.uid
         loadingMessage:TMELocalize(@"message.loading")
         success:^(TMEUserProfile *userProfile) {
             self.userData = userProfile;
             self.bagesViewController = [[UserBadgesViewController alloc]initWithBadges:userProfile.badges];
             self.myCollapseClick.CollapseClickDelegate = self;
             [self.myCollapseClick reloadCollapseClick];
             self.myCollapseClick.backgroundColor = [UIColor colorWithHex:@"#cccccc" alpha:1];
             [self updateData];
        } failure:nil];
    }
}

- (void)updateViewConstraints {
    [super updateViewConstraints];
    
    [self.myCollapseClick mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.bottom.and.right.equalTo(self.view);
        make.top.equalTo(self.view).with.offset(50.0);
        make.height.equalTo(self.view).offset(-50.0);
    }];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/profile/*"];
}

#pragma mark - Private Methods

- (void)addUserView {
    // TODO: Temp User Profile Header substitute
    UIView *userView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 50.0)];
    UIImageView *userImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50.0, 50.0)];
    UILabel *userName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(userImage.frame) + 5.0, CGRectGetMinY(self.view.frame), 100.0, 20.0)];
    UILabel *userPoints = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.view.frame) - 100.0, CGRectGetMinY(self.view.frame), 100.0, 20.0)];
    [userView addBorderToTop:NO bottom:YES left:NO right:NO];
    
    [userImage sd_setImageWithURL:self.userData.user.user_profile_image.path];
    userName.text = self.userData.user.realName;
    userName.font = [TeamieGlobals appFontFor:@"regularFontWithSize14"];
    userPoints.text = [NSString stringWithFormat:@"%@ %@", self.userData.user.userPoints, TMELocalize(@"message.points")];
    userPoints.font = [TeamieGlobals appFontFor:@"regularFontWithSize14"];
    [userView addSubview:userImage];
    [userView addSubview:userName];
    [userView addSubview:userPoints];
    [self.view addSubview:userView];
    
    [userView mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.top.right.equalTo(self.view);
        make.height.equalTo(@50);
    }];
    
    [userImage mas_makeConstraints:^(MASConstraintMaker *make){
        make.bottom.left.top.equalTo(userView);
        make.width.height.equalTo(@50);
    }];
    
    [userName mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(userImage.mas_right).with.offset(10.0);
        make.centerY.equalTo(userView.mas_centerY);
        make.height.equalTo(@20);
    }];

    [userPoints mas_makeConstraints:^(MASConstraintMaker *make){
        make.centerY.equalTo(userView.mas_centerY);
        make.trailing.equalTo(userView.mas_right).with.offset(-10.0);
        make.height.equalTo(@20);
    }];

}

#pragma mark - Collapse Click Delegate

// Required Methods
-(NSUInteger)numberOfCellsForCollapseClick {
//    return UserProfileSectionCount;
    return 3;
    // TODO: Temporarily disabled 2 sections as data is not available in the web service response
}

-(NSString *)titleForCollapseClickAtIndex:(int)index {
    switch (index) {
        case StatsSection:
            return @"Stats";
            break;
        case BadgesSection:
            return @"Badges";
            break;
        case ClassroomsSection:
            return @"Classrooms";
            break;
        case InfoSection:
            return @"Info";
        default:
            return @"";
            break;
    }
}

-(UIView *)viewForCollapseClickContentViewAtIndex:(int)index {
    
    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    view.backgroundColor = [UIColor grayColor];
    UserProfileStatsViewController *ups = [[UserProfileStatsViewController alloc]initWithUserStats:self.userData.userStats];
    
    NSMutableArray *classrooms = [NSMutableArray array];
    for (TMEClassroomAlt *classroom in _userData.user.classrooms) {
        [classrooms addObject:classroom.name];
    }
    UserProfileClassroomListViewController *classroomList = [[UserProfileClassroomListViewController alloc] initWithClassrooms:classrooms];

    UIView *badgesView = [[UIView alloc]init];
    NSArray *badges = self.userData.badges;
    
    CGFloat left = 0;
    CGFloat top =0;
    for (int i=0; i<self.userData.badges.count; i++) {
        // Temp hack without auto layout
        left = (i % ((UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?6:3)) * (10 + 100);
        top = (i / ((UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?6:3)) * (10 + 110);
        TMEBadge *badge = [badges objectAtIndex:i];
        
        
        UserProfileBadgeView* badgeView = [[UserProfileBadgeView alloc] init];
    
        [badgeView.badgeImageView sd_setImageWithURL:badge.href];
        [badgeView.badgeTitleLabel setText:badge.name];
        
        // set the position of the badge view
        badgeView.frame = CGRectMake(left, top, 100, 100);
        
        [badgesView addSubview:badgeView];
    }
    
    badgesView.frame = CGRectMake(0, 0, self.view.frame.size.width, top + 100);
    
    switch (index) {
        case StatsSection:
            [self addChildViewController:ups];
            
            ups.tableView.scrollEnabled = NO;
            CGRect frame = ups.tableView.frame;
            frame.size = CGSizeMake(frame.size.width, UserProfileStatsTableCellHeight * [self numberOfStatsCells]);
            ups.tableView.frame = frame;
            return ups.tableView;
            break;
        case BadgesSection:
            return badgesView;
        case ClassroomsSection: {
            [self addChildViewController:classroomList];

            classroomList.tableView.scrollEnabled = NO;
            CGRect frame = classroomList.tableView.frame;
            frame.size = CGSizeMake(frame.size.width, 44 * classrooms.count);
            frame.origin = CGPointMake(0, 0);
            classroomList.tableView.frame = frame;
            return classroomList.tableView;
            break;
        }
        case InfoSection: {
//            NSArray *personalData = _userData.personal.allKeys;
            return view;
        }
        default:
            return view;
            break;
    }
}
// Optional Methods

-(UIColor *)colorForCollapseClickTitleViewAtIndex:(int)index {
    //    return [UIColor colorWithRed:223/255.0f green:47/255.0f blue:51/255.0f alpha:1.0];
    return [UIColor colorWithWhite:1.0 alpha:1.0];
}

-(UIColor *)colorForTitleLabelAtIndex:(int)index {
    return [UIColor colorWithHex:@"#333333" alpha:1.0];
}

-(UIColor *)colorForTitleArrowAtIndex:(int)index {
    //    return [UIColor colorWithWhite:0.0 alpha:0.25];
    return [UIColor blackColor];
}

- (UIColor *)colorForLessonInfo:(int)index{
    return [UIColor whiteColor];
}

-(void)didClickCollapseClickCellAtIndex:(int)index isNowOpen:(BOOL)open {

}

- (NSInteger)numberOfStatsCells
{
    int count = 0;
    for (id statKey in self.userData.userStats) {
        NSDictionary* statInfo = [self.userData.userStats valueForKey:statKey];
        
        if ([statInfo valueForKey:@"statistics"] && [[statInfo objectForKey:@"statistics"] isKindOfClass:[NSArray class]]) {
            count += [(NSArray *)[statInfo objectForKey:@"statistics"] count];
        }
    }
    
    return count;

}

- (void)updateData {
    self.navigationItem.title = self.userData.user.realName;
    [self addUserView];
}

- (void)dismissUserProfile {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setParentRevealController:(UIViewController *)parent {
    _parentRevealController = parent;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//if (self.userData.badges) {
//    NSInteger counter = 0;
//    
//    for (TeamieUserBadge* badge in self.userData.badges) {
//        UserProfileBadgeView* badgeView = [[UserProfileBadgeView alloc] init];
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            [badgeView setFrame:CGRectMake((counter % 7) * (PROFILE_BADGES_WIDTH + 12.0) + 12.0, floor(counter / 7.0) * (PROFILE_BADGES_HEIGHT + 6.0) + 12.0, PROFILE_BADGES_WIDTH, PROFILE_BADGES_HEIGHT)];
//        }
//        else {
//            [badgeView setFrame:CGRectMake((counter % 3) * (PROFILE_BADGES_WIDTH + 12.0) + 12.0, floor(counter / 3.0) * (PROFILE_BADGES_HEIGHT + 6.0) + 12.0, PROFILE_BADGES_WIDTH, PROFILE_BADGES_HEIGHT)];
//        }
//        
//        // Set the badge image and title
//        [badgeView.badgeImageView setImage:[[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:badge.badgeImageURL]]]];
//        [badgeView.badgeTitleLabel setText:badge.name];
//        
//        // Add the badge to the tab content
//        [self.profileTabContent addSubview:badgeView];
//        
//        valuesAdded = YES;
//        
//        counter++;
//    }
//    
//    // Set content size of the profile tab content
//    [self.profileTabContent setContentSize:CGSizeMake(self.profileTabContent.frame.size.width, (PROFILE_BADGES_HEIGHT + 6.0) * ceil(counter / 3.0) + 12.0)];
//}

@end
