//
//  TMECollectionViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 02/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMECollectionViewController.h"
#import <Masonry/Masonry.h>

@interface TMECollectionViewController ()

@end

@implementation TMECollectionViewController

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Public Methods

- (void)viewForEmptyListWithTitleText:(NSString *)title subtitleText:(NSString *)subtitle andImage:(UIImage *)image {
    // TODO: Add a call to action button in next release
    UIView *containerView = [[UIView alloc] initWithFrame:self.emptyView.frame];
    UIImageView *emptyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMidX(containerView.frame), 0, 100.0, 100.0)];
    UILabel *titleText = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(containerView.frame), CGRectGetMaxY(emptyImageView.frame) + 5.0, CGRectGetWidth(containerView.frame), 20.0)];
    UILabel *subtitleText = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(containerView.frame), CGRectGetMaxY(titleText.frame) + 5.0, CGRectGetWidth(containerView.frame), 20.0)];
    
    [self.emptyView addSubview:containerView];
    
    [containerView addSubview:emptyImageView];
    [containerView addSubview:titleText];
    [containerView addSubview:subtitleText];
    
    [emptyImageView mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.height.equalTo(@100);
        make.top.centerX.equalTo(containerView);
    }];
    
    [titleText mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(containerView);
        make.height.equalTo(@20);
        make.top.equalTo(emptyImageView.mas_bottom).with.offset(10.0);
        make.centerX.equalTo(emptyImageView);
    }];
    
    [subtitleText mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(containerView);
        make.height.greaterThanOrEqualTo(@20);
        make.top.equalTo(titleText.mas_bottom).with.offset(5.0);
        make.centerX.equalTo(emptyImageView);
    }];
    
    titleText.font = [TeamieGlobals appFontFor:@"emptyListTitleText"];
    subtitleText.font = [TeamieGlobals appFontFor:@"emptyListSubtitleText"];
    titleText.textAlignment = NSTextAlignmentCenter;
    subtitleText.textAlignment = NSTextAlignmentCenter;
    subtitleText.numberOfLines = 0;
    
    emptyImageView.image = image?image:[TeamieUIGlobals defaultPicForPurpose:@"emptylist" withSize:100.0];
    titleText.text = title?title:@"No Data";
    subtitleText.text = subtitle?subtitle:@"You should probably do some activity";
    
    [self.view addSubview:self.emptyView];
    
    [containerView mas_updateConstraints:^(MASConstraintMaker *make){
        CGSize size = CGSizeMake(CGRectGetWidth(self.view.bounds), CGRectGetHeight(emptyImageView.frame) + CGRectGetHeight(titleText.frame) + CGRectGetHeight(subtitleText.frame));
        make.width.equalTo(@(size.width));
        make.height.equalTo(@(size.height));
        make.center.equalTo(self.view);
    }];
}

- (UIView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[UIView alloc] initWithFrame:self.view.bounds];
        _emptyView.backgroundColor = [TeamieUIGlobalColors genericBackgroundColor];
    }
    return _emptyView;
}

@end
