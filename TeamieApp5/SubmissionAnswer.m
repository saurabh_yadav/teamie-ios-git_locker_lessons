//
//  SubmissionAnswer.m
//  TeamieApp5
//
//  Created by Raunak on 23/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "SubmissionAnswer.h"

@implementation SubmissionAnswer

@synthesize aid;
@synthesize qid;
@synthesize isCorrect;
@synthesize currentScore;
@synthesize answerHtml;
@synthesize answerFiles;
@synthesize answerFilePath;
@synthesize answerSummary;

// Make answer Files always return an array with answer file path
// A temporary fix as long as file submissions remains a single file thingy.
- (NSArray*)answerFiles {
    return [NSArray arrayWithObject:[NSDictionary dictionaryWithObject:self.answerFilePath forKey:@"href"]];
}

- (NSString*)answerSummary {
    NSString* answerText;
    
    if (self.answerFilePath) {
        answerText = @"1 File submitted. Tap on 'View Submission' button below to view & grade it.";
    }
    else if (self.answerHtml) {
        answerText = self.answerHtml;
    }
    else {
        answerText = @"(No Answer Available)";
    }
    
    return answerText;
}

@end
