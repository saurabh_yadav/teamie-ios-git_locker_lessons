//
//  ErrorView.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 10/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "ErrorView.h"

static const CGFloat kVPadding1 = 30.0f;
static const CGFloat kVPadding2 = 10.0f;
static const CGFloat kVPadding3 = 15.0f;
static const CGFloat kHPadding  = 10.0f;

@interface ErrorView()

@property (nonatomic, strong) UIImageView* imageView;
@property (nonatomic, strong) UILabel* titleView;
@property (nonatomic, strong) UILabel* subtitleView;

@end

@implementation ErrorView

- (id)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame title:(NSString*)title subtitle:(NSString*)subtitle image:(UIImage*)image {
	self = [self initWithFrame:frame];
    
    if (self) {
        self.titleView.text = title;
        self.subtitleView.text = subtitle;
        self.imageView.image = image;
    }
    
    return self;
}

- (void)initialize{
    self.imageView = [[UIImageView alloc] init];
    self.imageView.contentMode = UIViewContentModeCenter;
    [self addSubview:_imageView];
    
    self.titleView = [[UILabel alloc] init];
    self.titleView.backgroundColor = [UIColor clearColor];
    self.titleView.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleView];
    
    self.subtitleView = [[UILabel alloc] init];
    self.subtitleView.backgroundColor = [UIColor clearColor];
    self.subtitleView.textAlignment = NSTextAlignmentCenter;
    self.subtitleView.numberOfLines = 0;
    [self addSubview:_subtitleView];
}

- (void)layoutSubviews {
    CGSize subtitileSize = [self.subtitleView sizeThatFits:CGSizeMake(self.frame.size.width - kHPadding*2, 0)];
    self.subtitleView.frame = CGRectMake(0, 0, subtitileSize.width, subtitileSize.height);
    [self.titleView sizeToFit];
    [self.imageView sizeToFit];
    
    CGFloat maxHeight = self.imageView.frame.size.height + self.titleView.frame.size.height + self.subtitleView.frame.size.height
    + kVPadding1 + kVPadding2;
    BOOL canShowImage = self.imageView.image && self.frame.size.height > maxHeight;
    
    CGFloat totalHeight = 0.0f;
    
    if (canShowImage) {
        totalHeight += self.imageView.frame.size.height;
    }
    if (self.titleView.text.length) {
        totalHeight += (totalHeight ? kVPadding1 : 0) + self.titleView.frame.size.height;
    }
    if (self.subtitleView.text.length) {
        totalHeight += (totalHeight ? kVPadding2 : 0) + self.subtitleView.frame.size.height;
    }
    
    CGFloat top = floor(self.frame.size.height/2 - totalHeight/2);
    
    if (canShowImage) {
        CGPoint origin = CGPointMake(floor(self.frame.size.width/2 - self.imageView.frame.size.width/2), top);
        self.imageView.frame = CGRectMake(origin.x, origin.y, self.imageView.frame.size.width, self.imageView.frame.size.height);
        self.imageView.hidden = NO;
        top += self.imageView.frame.size.height + kVPadding1;
        
    } else {
        self.imageView.hidden = YES;
    }
    if (self.titleView.text.length) {
        CGPoint origin = CGPointMake(floor(self.frame.size.width/2 - self.titleView.frame.size.width/2), top);
        self.titleView.frame = CGRectMake(origin.x, origin.y, self.titleView.frame.size.width, self.titleView.frame.size.height);
        top += self.titleView.frame.size.height + kVPadding2;
    }
    if (self.subtitleView.text.length) {
        CGPoint origin = CGPointMake(floor(self.frame.size.width/2 - self.subtitleView.frame.size.width/2), top);
        self.subtitleView.frame = CGRectMake(origin.x, origin.y, self.subtitleView.frame.size.width, self.subtitleView.frame.size.height);
        top += self.subtitleView.frame.size.height + kVPadding3;
    }
}

@end
