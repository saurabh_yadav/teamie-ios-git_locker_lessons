//
//  NSMutableArray+LPTOCUtilityButtons.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 22/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+FontAwesome.h"
@interface NSMutableArray (LPTOCUtilityButtons)
- (void)lptoc_addUtilityButtonWithColor:(UIColor *)color icon:(NSString *)icon;
@end
