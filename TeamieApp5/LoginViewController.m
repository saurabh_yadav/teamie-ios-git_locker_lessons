//
//  LoginViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/20/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "LoginViewController.h"
#import <GoogleOpenSource/GoogleOpenSource.h>
#import <GooglePlus/GooglePlus.h>

#import "TMEClient.h"
#import "TeamieAppDelegate.h"
#import "TMESiteInfo.h"
#import "TMEUser.h"

#import <SDWebImage/UIImageView+WebCache.h>
#import "NIKFontAwesomeIconFactory.h"
#import "NIKFontAwesomeIconFactory+iOS.h"
#import <Masonry/Masonry.h>

@implementation LoginViewController

@synthesize logoImageView;
@synthesize usernameTextField;
@synthesize passwordTextField;
@synthesize forgotPasswordButton;
@synthesize loginButton, registerButton;
@synthesize instituteURL, siteinfo;
@synthesize googleSigninButton;

// Init the UIViewController from the Storyboard
- (id)initFromStoryboard {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[TeamieGlobals storyboardIdentifier] bundle:nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    return self;
}

#pragma mark - View lifecycle

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.constraintsAdded = NO;
    
    [self.sloganLabel setFont:[TeamieGlobals appFontFor:@"slogan"]];
    [self.subsloganLabel setFont:[TeamieGlobals appFontFor:@"subslogan"]];
    [self.usernameTextField setFont:[TeamieGlobals appFontFor:@"inputText"]];
    [self.passwordTextField setFont:[TeamieGlobals appFontFor:@"inputText"]];
    [self.loginButton.titleLabel setFont:[TeamieGlobals appFontFor:@"actionButton"]];
    [self.registerButton.titleLabel setFont:[TeamieGlobals appFontFor:@"textLink"]];
    
    [self.usernameTextField setTextColor:[TeamieUIGlobalColors inputFieldTextColor]];
    [self.passwordTextField setTextColor:[TeamieUIGlobalColors inputFieldTextColor]];
    self.usernameTextField.layer.borderWidth = self.passwordTextField.layer.borderWidth = 1.0f;
    self.usernameTextField.layer.borderColor = self.passwordTextField.layer.borderColor = [TeamieUIGlobalColors inputFieldBorderColor].CGColor;
    
    UIBezierPath *maskPath1, *maskPath2;
    maskPath1 = [UIBezierPath bezierPathWithRoundedRect:self.usernameTextField.bounds
                                     byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
                                           cornerRadii:CGSizeMake(UI_NORMAL_BORDER_RADIUS, UI_NORMAL_BORDER_RADIUS)];
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = self.usernameTextField.bounds;
    maskLayer1.path = maskPath1.CGPath;
    self.usernameTextField.layer.mask = maskLayer1;
    
    maskPath2 = [UIBezierPath bezierPathWithRoundedRect:self.usernameTextField.bounds
                                    byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                          cornerRadii:CGSizeMake(UI_NORMAL_BORDER_RADIUS, UI_NORMAL_BORDER_RADIUS)];
    CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
    maskLayer2.frame = self.passwordTextField.bounds;
    maskLayer2.path = maskPath2.CGPath;
    self.passwordTextField.layer.mask = maskLayer2;
    
    self.usernameTextField.layer.masksToBounds = YES;
    self.usernameTextField.layer.masksToBounds = YES;
    usernameTextField.delegate = self;
    passwordTextField.delegate = self;
    
    [self.loginButton setBackgroundColor:[TeamieUIGlobalColors primaryActionColor]];
    self.loginButton.layer.cornerRadius = UI_NORMAL_BORDER_RADIUS;
    
    // Add notification observers for the keyboard that pops up
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    
    _tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
    
    NIKFontAwesomeIconFactory *factory = [NIKFontAwesomeIconFactory buttonIconFactory];
    [self.backButton setImage:[factory createImageForIcon:NIKFontAwesomeIconArrowLeft] forState:UIControlStateNormal];
    [self.backButton setTitle:@"" forState:UIControlStateNormal];
    
    [self googleSignInSetup];
}

- (void)viewDidUnload {
    [self setUsernameTextField:nil];
    [self setPasswordTextField:nil];
    [self setLoginButton:nil];
    [self setRegisterButton:nil];
    [self setAppVersion:nil];
    [self setLogoImageView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/login"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    self.navigationController.navigationBarHidden = YES;
    
    // Set the site info based on the info passed on.
    if ([self.siteinfo.siteLogoURL length]) {
        [self.customerLogoImage sd_setImageWithURL:[NSURL URLWithString:self.siteinfo.siteLogoURL] placeholderImage:[UIImage imageNamed:@"teamie-logo.png"]];
    }
    if ([self.siteinfo.siteSlogan length]) {
        [self.sloganLabel setText:self.siteinfo.siteSlogan];
    }
    if ([self.siteinfo.siteSubSlogan length]) {
        [self.subsloganLabel setText:self.siteinfo.siteSubSlogan];
    }
    else {
        [self.subsloganLabel setText:self.siteinfo.siteName];
    }
    
#ifdef DEBUG
    self.appVersion.text = [NSString stringWithFormat:@"Version: %@ (%@)",
                            [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"],
                            [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"]];
#else
    self.appVersion.text = [NSString stringWithFormat:@"Version: %@",
                            [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
#endif

    // Removing login form stuff based on the current institute's settings
    // Removing instead of hiding since hidden views still take space
    if (![siteinfo.isTeamieRegisterSupported boolValue]) {
        [registerButton removeFromSuperview];
    }
    if ((![siteinfo.isTeamieLoginSupported boolValue]) && siteinfo.isTeamieLoginSupported != nil) {
        [registerButton removeFromSuperview];
        [loginButton removeFromSuperview];
        [forgotPasswordButton removeFromSuperview];
        [usernameTextField removeFromSuperview];
        [passwordTextField removeFromSuperview];
        googleSigninButton.autoresizingMask = UIViewAutoresizingNone;
        googleSigninButton.frame = CGRectMake(20, 190, 280, 40);
        googleSigninButton.center = self.loginButton.center;

    }
    if (![siteinfo.isGoogleLoginSupported boolValue]) {
        [googleSigninButton removeFromSuperview];
        [usernameTextField becomeFirstResponder];
    }
}

- (void)updateViewConstraints {
    if (!self.areConstraintsAdded) {
        [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.height.equalTo(@44);
            make.top.left.equalTo(self.view).with.insets(UIEdgeInsetsMake(10.0, 10.0, 0, 0));
        }];

        [self.customerLogoImage mas_makeConstraints:^(MASConstraintMaker *make){
            make.centerX.equalTo(self.view);
            make.width.equalTo(@210);
            make.height.equalTo(@85);
            make.top.equalTo(self.view).with.offset(15.0);
        }];
        
        [self.sloganLabel mas_makeConstraints:^(MASConstraintMaker *make){
            make.centerX.equalTo(self.view);
            make.top.equalTo(self.customerLogoImage.mas_bottom).with.offset(5.0);
            make.width.equalTo(@250.0);
            make.height.equalTo(@25);
        }];
        
        [self.subsloganLabel mas_makeConstraints:^(MASConstraintMaker *make){
            make.centerX.equalTo(self.view);
            make.top.equalTo(self.sloganLabel.mas_bottom).with.offset(5.0);
            make.width.equalTo(@280);
            make.height.equalTo(@35);
        }];
        
        if (siteinfo.isTeamieLoginSupported.boolValue) {
            [self.usernameTextField mas_makeConstraints:^(MASConstraintMaker *make){
                make.centerX.equalTo(self.view);
                make.top.equalTo(self.sloganLabel.mas_bottom).with.offset(90.0);
                make.height.equalTo(@40);
                make.width.equalTo(@280);
            }];
            
            [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make){
                make.centerX.equalTo(self.view);
                make.top.equalTo(self.usernameTextField.mas_bottom);
                make.width.height.equalTo(self.usernameTextField);
            }];
            
            [self.loginButton mas_makeConstraints:^(MASConstraintMaker *make){
                make.centerX.equalTo(self.view);
                make.top.equalTo(self.passwordTextField.mas_bottom).with.offset(20.0);
                make.width.height.equalTo(self.usernameTextField);
            }];
            
            [self.forgotPasswordButton mas_makeConstraints:^(MASConstraintMaker *make){
                make.right.equalTo(self.loginButton);
                make.top.equalTo(self.loginButton.mas_bottom).with.offset(10.0);
            }];
            
            if (siteinfo.isTeamieRegisterSupported.boolValue) {
                [self.registerButton mas_makeConstraints:^(MASConstraintMaker *make){
                    make.left.equalTo(self.loginButton);
                    make.top.equalTo(self.loginButton.mas_bottom).with.offset(10.0);
                }];
            }
        }
        
        if (siteinfo.isGoogleLoginSupported.boolValue) {
            [self.googleSigninButton mas_makeConstraints:^(MASConstraintMaker *make){
                make.centerX.equalTo(self.view);
                make.height.equalTo(@45);
                make.width.equalTo(@280);
                if (siteinfo.isTeamieLoginSupported.boolValue) {
                    make.top.equalTo(self.loginButton.mas_bottom).with.offset(40);
                }
                else {
                    make.top.equalTo(self.customerLogoImage.mas_bottom).with.offset(70);
                }
            }];
        }
        
        [self.appVersion mas_makeConstraints:^(MASConstraintMaker *make){
            make.bottom.right.equalTo(self.view);
        }];

        self.constraintsAdded = YES;
    }
    [super updateViewConstraints];
}

#pragma mark - Button click events

- (IBAction)loginButtonClicked:(id)sender {
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
    
    NSString * username = [usernameTextField text];
    NSString * password = [passwordTextField text];
    
    if (username.length == 0 && password.length == 0) {
        [self showError:TMELocalize(@"error.no-username-password")];
        return;
    }
    else if (username.length == 0) {
        [self showError:TMELocalize(@"error.no-username")];
        return;
    }
    else if (password.length == 0) {
        [self showError:TMELocalize(@"error.no-password")];
        return;
    }
    
    [self sendTeamieLoginRequest:@{@"username": username, @"password": password, @"institute": self.instituteURL}];
}

- (IBAction)registerButtonClicked:(id)sender {
    [TMEAnalyticsController trackScreen:@"/register"];

    // open the Instant registration page on Safari app
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@/user/register", self.instituteURL, nil]]];
}

- (IBAction)forgotPasswordClicked:(id)sender {
    // open the Instant registration page on Safari app
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@/user/password", self.instituteURL, nil]]];
    //[[[GAI sharedInstance] defaultTracker] set:kGAIScreenName value:@"/user/password"];
    //[[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createScreenView] build]];
}

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark Keyboard Notification methods
- (void)keyboardWillShow:(NSNotification *)note {
    [TeamieGlobals dismissTSMessage];
    // create a transparent view to be added to the view controller to detect taps outside of inputs & button
    _viewToTapOn = [[UIView alloc] initWithFrame:self.view.bounds];
    // add gesture recognizer to the view
    [_viewToTapOn addGestureRecognizer:_tapGestureRecognizer]; 
    [self.view addSubview:_viewToTapOn];
    [self.view bringSubviewToFront:_viewToTapOn];
    
    // bring the login button and text fields to the front so that tapping on them does not dismiss the keyboard
    [self.view bringSubviewToFront:self.logoImageView];
    [self.view bringSubviewToFront:self.loginButton];
    [self.view bringSubviewToFront:self.usernameTextField];
    [self.view bringSubviewToFront:self.passwordTextField];
    [self.view bringSubviewToFront:self.backButton];
    [self.view bringSubviewToFront:self.registerButton];
    [self.view bringSubviewToFront:self.forgotPasswordButton];
    
    /*if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [TeamieGlobals moveControl:self.logoImageView forKeyboard:note up:YES distance:20.0 inView:self.view];
        [TeamieGlobals moveControl:self.loginButton forKeyboard:note up:YES distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
        [TeamieGlobals moveControl:self.usernameTextField forKeyboard:note up:YES distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
        [TeamieGlobals moveControl:self.passwordTextField forKeyboard:note up:YES distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
        //[TeamieGlobals moveControl:self.instituteTextField forKeyboard:note up:YES distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
        //[TeamieGlobals moveControl:self.institueFieldHelpText forKeyboard:note up:YES distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
        
        if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
            [TeamieGlobals moveControl:self.loginButton forKeyboard:note up:YES distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT - 25.0 inView:self.view];
            [TeamieGlobals moveControl:self.usernameTextField forKeyboard:note up:YES distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT - 25.0 inView:self.view];
            [TeamieGlobals moveControl:self.passwordTextField forKeyboard:note up:YES distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT - 25.0 inView:self.view];
            //[TeamieGlobals moveControl:self.instituteTextField forKeyboard:note up:YES distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT - 25.0 inView:self.view];
        }
    }*/
}

- (void)keyboardWillHide:(NSNotification *)note {
    
    [_viewToTapOn removeGestureRecognizer:_tapGestureRecognizer];
    [_viewToTapOn removeFromSuperview];
    _viewToTapOn = nil;
    
    /*if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [TeamieGlobals moveControl:self.logoImageView forKeyboard:note up:NO distance:20.0 inView:self.view];
        [TeamieGlobals moveControl:self.loginButton forKeyboard:note up:NO distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
        [TeamieGlobals moveControl:self.usernameTextField forKeyboard:note up:NO distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
        [TeamieGlobals moveControl:self.passwordTextField forKeyboard:note up:NO distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
        //[TeamieGlobals moveControl:self.instituteTextField forKeyboard:note up:NO distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
//        [TeamieGlobals moveControl:self.institueFieldHelpText forKeyboard:note up:NO distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT inView:self.view];
        
        if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
            [TeamieGlobals moveControl:self.loginButton forKeyboard:note up:NO distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT - 25.0 inView:self.view];
            [TeamieGlobals moveControl:self.usernameTextField forKeyboard:note up:NO distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT - 25.0 inView:self.view];
            [TeamieGlobals moveControl:self.passwordTextField forKeyboard:note up:NO distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT - 25.0 inView:self.view];
            //[TeamieGlobals moveControl:self.instituteTextField forKeyboard:note up:NO distance:LOGIN_ITEMS_SCREEN_DISPLACEMENT - 25.0 inView:self.view];
        }

    }    */
}

- (void)didTapAnywhere:(UITapGestureRecognizer *)recognizer {
    [self.usernameTextField resignFirstResponder];
    [self.passwordTextField resignFirstResponder];
}

#pragma mark Lazy Initialization methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if (textField == usernameTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    if (textField == self.passwordTextField) {
        [textField resignFirstResponder];
    }
    return YES;
}

#pragma mark GPPSignInDelegateMethods

- (void)finishedWithAuth: (GTMOAuth2Authentication *)auth
                   error: (NSError *) error {
    if (!error) {
        [self sendSSORequest:[GPPSignIn sharedInstance].authentication.userEmail];
    }
}

- (void)didDisconnectWithError:(NSError *)error {
    //Called after [GPPSignIn disconnect] is called
    //TODO: Anything to do here?
}


#pragma mark Private methods

- (void)sendSSORequest:(NSString *)email {
    [self sendTeamieLoginRequest:[TeamieGlobals prepareLoginParametersWithEmail:email site:self.instituteURL]];
}

- (void)googleSignInSetup {
    googleSigninButton.style = kGPPSignInButtonStyleWide;

    GPPSignIn *signIn = [GPPSignIn sharedInstance];

    signIn.clientID = GOOGLE_CLIENT_ID;

    //We need only email
    signIn.shouldFetchGoogleUserEmail = YES;
    signIn.scopes = @[kGTLAuthScopePlusUserinfoEmail];

    signIn.delegate = self;
}

- (void)sendTeamieLoginRequest:(NSDictionary *)params {
    [[TMEClient sharedClient] login:params
        success:^() {
          [self.usernameTextField resignFirstResponder];
          [self.passwordTextField resignFirstResponder];
          self.usernameTextField.text = nil;
          self.passwordTextField.text = nil;

          NSLog(@"Opening up the Newsfeed screen...");
          [((TeamieAppDelegate *)[[UIApplication sharedApplication] delegate]) performSelector:@selector(transitionToHomeScreen)];
      }
      failure:^(NSError *error) {
          [self showError:error.userInfo[TMEJSONResponseErrorKey][@"error_description"]];
      }];
}

- (void)showError:(NSString *)message {
    [TeamieGlobals addTSMessageInController:self title:TMELocalize(@"error.login-failed") message:message type:@"error" duration:0 withCallback:nil];
}

@end
