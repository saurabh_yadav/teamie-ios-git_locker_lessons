//
//  TMEUser.h
//  Classmates
//
//  Created by Anuj Rajput on 23/08/12.
//  Copyright (c) 2012 Anuj Rajput. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TMEModel.h"
#import "TMEImage.h"

@interface TMEUser : TMEModel

@property (nonatomic, strong) NSNumber *uid;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *realName;
@property (nonatomic, strong) NSNumber *userPoints;
@property (nonatomic, strong) NSArray *classrooms;
@property (nonatomic, strong) NSArray *roles;
@property (nonatomic, strong) TMEImage *user_profile_image;

+ (TMEUser *)currentUser;
+ (void)setCurrentUser:(TMEUser *)currentUser;

- (UIImage *)picture;

@end
