//
//  Lesson.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEUser.h"
#import "TMEModel.h"

@interface TMELessonOverview : TMEModel

@property (nonatomic, retain) NSString * web_url;
@property (nonatomic, retain) NSString * href;
@property (nonatomic, retain) NSNumber * nid;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSNumber * pages;
@property (nonatomic, retain) TMEUser *author;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSDate * publishDate;
@property (nonatomic, retain) NSDate * deadline;
@property (nonatomic, retain) NSNumber * numVotes;
@property (nonatomic, retain) NSNumber * num_pages_read_by_current_user;
@property (nonatomic, retain) NSArray * tags;
//@property (nonatomic, retain) NSArray * subjects;
//@property (nonatomic, retain) NSArray * campuses;
//@property (nonatomic, retain) NSArray * levels;
@property (nonatomic, retain) NSArray * classrooms;
@property (nonatomic, retain) NSURL *coverImageURL;
@property (nonatomic, retain) NSNumber * num_reader;
@property (nonatomic, retain) NSArray * actions;
@property (nonatomic, retain) NSArray *lesson_pages;
@property (nonatomic, retain) NSArray * topReaders;
@property (nonatomic, retain) NSString *type;

@end
