//
//  UserLoginData.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/27/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "UserLoginData.h"
#import "TMEClassroom.h"
#import "UserMenu.h"
#import "UserProfile.h"

@implementation UserLoginData

@dynamic isLoggedIn;
@dynamic lastLoginTime;
@dynamic sessid;
@dynamic session_name;
@dynamic uid;
@dynamic baseURL;
@dynamic userBulletin;
@dynamic userClassroom;
@dynamic userMenu;
@dynamic userProfile;

@end
