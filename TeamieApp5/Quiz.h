//
//  Quiz.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 10/23/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@interface Quiz : NSManagedObject

@property (nonatomic, retain) NSNumber * deadlineDate;
@property (nonatomic, retain) NSNumber * numVotes;
@property (nonatomic, retain) NSNumber * publishDate;
@property (nonatomic, retain) NSNumber * qid;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@end
