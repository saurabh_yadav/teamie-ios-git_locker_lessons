//
//  TeamieListViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/12/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"

@interface TeamieListViewController : TableRestApiViewController {
    @private
    TeamieEntityType _entityType;
    NSNumber* _entityId;
    id _loadedEntries;  // a list of the loaded entities
    BOOL _loadMore;
    NSInteger _currentPage;
}

@property (weak, nonatomic) IBOutlet UINavigationBar *navigationBar;

// Init methods
- (id)initWithEntity:(TeamieEntityType)entityType withId:(NSNumber*)entityId;
- (id)initWithEntity:(TeamieEntityType)entityType;
@end
