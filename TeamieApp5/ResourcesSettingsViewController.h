//
//  ResourcesSettingsViewController.h
//  TeamieApp5
//
//  Created by Raunak on 16/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kCurrentAccount = 0,
    kOtherAccounts
} SettingsDeleteOptions;

@protocol SettingsControllerDelegate
- (void) settingsControllerDidSelectDeleteOption:(SettingsDeleteOptions)option;
- (BOOL) settingsControllerShouldDisplayCurrentAccountOption;
- (BOOL) settingsControllerShouldDisplayOtherAccountsOptions;
@end

@interface ResourcesSettingsViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) id<SettingsControllerDelegate> delegate;

@end
