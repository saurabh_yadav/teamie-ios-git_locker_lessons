//
//  UserScore.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEUser.h"
#import "TMEModel.h"

@interface TMEUserScore : TMEModel

@property (nonatomic, retain) TMEUser* user;

@property (nonatomic, retain) NSNumber* finalScore;

@property (nonatomic, retain) NSString* grade;

@property (nonatomic, retain) NSNumber* isScoreSet;

// An array of NSDictionary items, wherein each NSDictionary instance is the score of a particular user in a particular quiz.
/* The JSON response for quiz score in a particular quiz looks something like this:
     {
     "score": 1,
     "score_weighted": 33.33,
     "score_set": true,
     "entity_type": "quizng",
     "entity_id": 244
     },
 */
@property (nonatomic, retain) NSArray* quizScores;

@end
