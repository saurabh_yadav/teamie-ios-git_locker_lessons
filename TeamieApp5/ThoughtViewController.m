//
//  ThoughtViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/8/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#define kAlertViewWillDelete 1
#define kAlertViewDidDelete 2


#import "ThoughtViewController.h"
#import "NewsfeedItemComment.h"

#import "ADVPercentProgressBar.h"

#import "AudioToolbox/AudioServices.h"

#import "TeamieWebViewController.h"

// Private methods declaration
@interface ThoughtViewController()

- (void)thoughtDataLoadSuccessful;
- (void)layoutThoughtCommentsInFrame:(CGRect)tempFrame scrollToLatest:(BOOL)scrollAnimate;
- (UIView*)createCommentView:(NewsfeedItemComment*)comment thoughtType:(NSString*)thoughtType;
- (void)addThoughtActionButton;
- (void)thoughtRefreshed;

@end

// Class implementation
@implementation ThoughtViewController

@synthesize thoughtAuthorDisplayPic;
@synthesize thoughtAuthorName;
@synthesize thoughtMessageBody;
@synthesize thoughtView;
@synthesize thoughtTypeIndicator;
@synthesize thoughtTimestamp;
@synthesize thoughtClassroomName;
@synthesize thoughtInfoBar;
@synthesize commentStreamView;
@synthesize commentEditor;
@synthesize commentPostButton;
@synthesize commentStreamHeader;
@synthesize bottomToolbar;
@synthesize thoughtData;
@synthesize thoughtID = _thoughtID;
@synthesize commentStreamController = _commentStreamController;
@synthesize thoughtViewDelegate;

- (id)initWithThoughtID:(NSNumber*)thoughtId delegate:(id)thoughtDelegate {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[TeamieGlobals storyboardIdentifier] bundle:nil];
    
    self = [storyboard instantiateViewControllerWithIdentifier:@"ThoughtViewController"];
    self.navigationItem.title = TeamieLocalizedString(@"$thought[s]", nil);
    
    if (self) {
        // make request for loading thought with given thought ID
        [self loadThoughtWithTid:thoughtId reload:YES];
        self.thoughtViewDelegate = thoughtDelegate;
    }
    
    return self;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    // add the commentStreamController as a childViewController to self
    [self addChildViewController:self.commentStreamController];
    [self.commentStreamController didMoveToParentViewController:self];
    
    // initialize the private mutable arrays
    _pollBars = [[NSMutableArray alloc] init];
    _pollCheckboxes = [[NSMutableArray alloc] init];
    _pollLabels = [[NSMutableArray alloc] init];
    _pollKeys = [[NSMutableArray alloc] init];
    
    if (self.thoughtData) { // if the thought data has been loaded, then display it
        
        [self thoughtDataLoadSuccessful];
    
    }
    
    // if the current view controller is being shown as a modal
    if (self.presentingViewController) {
        [TeamieGlobals addBackButtonToViewController:self withCallback:@selector(dismissThoughtView:)];
    }
    
    // Answered by good gentleman over here http://stackoverflow.com/questions/8771176/ttnavigator-not-pushing-onto-navigation-stack
    /*TTNavigator* navigator = [TTNavigator navigator];
    navigator.delegate = self;
    navigator.window = [UIApplication sharedApplication].delegate.window;
    
    TTURLMap* map = navigator.URLMap;
    [map from:@"*" toViewController:[TTWebController class]];*/

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self becomeFirstResponder];
    [self performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0.01];

    if (self.thoughtID) {
        //[[GANTracker sharedTracker] trackPageview:[NSString stringWithFormat:@"/thought/%qi", [self.thoughtID longLongValue]] withError:nil];
    }
    else {
        //[[GANTracker sharedTracker] trackPageview:@"/thought/" withError:nil];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [self resignFirstResponder];
    [super viewDidDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
        
    // if the view is going to appear & the thought data hasn't been loaded yet.
    if (!self.thoughtData) {
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_THOUGHT", nil) margin:10.f yOffset:10.f];
    }
    [self.view setNeedsDisplay];
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)viewDidUnload
{
    [self setThoughtAuthorName:nil];
    [self setThoughtMessageBody:nil];
    [self setThoughtView:nil];
    [self setThoughtData:nil];
    [self setThoughtID:nil];
    [self setThoughtViewDelegate:nil];
    
    [self setThoughtAuthorDisplayPic:nil];
    
    [self.commentStreamController willMoveToParentViewController:self];
    [self.commentStreamController removeFromParentViewController];
    [self.commentStreamController viewDidUnload];
    self.commentStreamController = nil;
    
    [self setCommentStreamView:nil];
    [self setCommentEditor:nil];
    [self setBottomToolbar:nil];
    [self setCommentPostButton:nil];
    [self setThoughtTypeIndicator:nil];
    [self setThoughtTimestamp:nil];
    [self setThoughtClassroomName:nil];
    [self setThoughtInfoBar:nil];
    [self setCommentStreamHeader:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)loadThoughtWithTid:(NSNumber *)tid reload:(BOOL)shouldReload {
    
    [self.thoughtClassroomName setHidden:YES];
    
    NSDictionary* params;
    
    if (shouldReload) {
        params = [[NSDictionary alloc] initWithObjectsAndKeys:tid, @"tid", @"true", @"reload", nil];
    }
    else {
        params = [[NSDictionary alloc] initWithObjectsAndKeys:tid, @"tid", nil];
    }
    
    self.thoughtID = tid;
    self.thoughtData = nil; // unset the existing thoughtData so that the "loading" message would be displayed in viewWillAppear:
    
    [self.apiCaller performRestRequest:TeamieUserGetThoughtRequest withParams:params];
    
    // Show a loading text, every time a request to API is made
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_THOUGHT", nil) margin:10.f yOffset:10.f];
}

- (void)thoughtRefreshed {
    [self loadThoughtWithTid:thoughtData.tid reload:YES];
    [self.commentStreamView reloadInputViews];
}

#pragma mark - TeamieRESTApi delegate methods

- (void)requestSuccessful:(TeamieRESTRequest)requestName {
    if (requestName == TeamieUserDeleteThoughtRequest) {
        
        [self.thoughtData deleteObjectInContext:[RKObjectManager sharedManager].objectStore.primaryManagedObjectContext];
        
        // Set the thoughtData to nil, only then the dismissThoughtView of NewsfeedViewController will be invoked with nil parameter
        self.thoughtData = nil;
        [self dismissThoughtView:self];
        // TODO: Refresh Newsfeed after dismissing ThoughtViewController
    }
}

-(void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    
    [TeamieGlobals dismissActivityLabels];
    
    if (requestName == TeamieUserGetThoughtRequest) {
        
        if ([response isKindOfClass:[NewsfeedItem class]]) {
            // if the response is an object of type newsfeed Item then directly assign it to thoughtData
            self.thoughtData = response;
        }
        else if ([response isKindOfClass:[NSArray class]] && [[response objectAtIndex:0] isKindOfClass:[NewsfeedItem class]]) {
            // if the response is an object of type NSArray whose first object is a NewsfeedItem, then assign that first object to thoughtData
            self.thoughtData = [response objectAtIndex:0];
        }
        else {
#ifdef DEBUG
            NSLog(@"Error: The response for Get Thought request is not of type NewsfeedItem");
#endif
        }
        
        if ([self isViewLoaded]) {  // if the view has finished loading...
            [self thoughtDataLoadSuccessful];
        }
    }
    else if (requestName == TeamieUserPostThoughtCommentRequest) {
        
        // clear the comment box text
        self.commentEditor.text = @"";
        [self.commentEditor resignFirstResponder];
        
        // if the response is an array of objects and the first object is a user comment
        if ([response isKindOfClass:[NSArray class]] && [[response objectAtIndex:0] isKindOfClass:[NewsfeedItemComment class]]) {
            // add that comment to self.thoughtData
            [self.thoughtData addCommentsObject:[response objectAtIndex:0]];
            // increment self.thoughtData's comments count
            self.thoughtData.countComment = [NSNumber numberWithInteger:[self.thoughtData.countComment integerValue] + 1];
        }
        
        // need to save the thought data otherwise database changes won't be permanent
        [self.thoughtData saveObjectInContext:[RKObjectManager sharedManager].objectStore.primaryManagedObjectContext];
        
        // layout the comments and scroll to the latest user comment
        [self layoutThoughtCommentsInFrame:self.commentStreamView.frame scrollToLatest:YES];
    }
    else if (requestName == TeamieUserSameThoughtRequest || requestName == TeamieUserLikeThoughtRequest) {
        
        UIBarButtonItem* actionButton = [self.navigationItem.rightBarButtonItems lastObject];
        NSString* currentTitle = actionButton.title;
        
        NSArray* defaultMenus = [TeamieGlobals defaultTeamieThoughtActionMenus];
        NSInteger titleIndex = [defaultMenus indexOfObject:currentTitle];
        NSString* newTitle = nil;
        
        if (titleIndex == 0) {
            newTitle = [defaultMenus objectAtIndex:1];
        }
        else if (titleIndex == 1) {
            newTitle = [defaultMenus objectAtIndex:0];
        }
        else if (titleIndex == 2) {
            newTitle = [defaultMenus objectAtIndex:3];
        }
        else if (titleIndex == 3) {
            newTitle = [defaultMenus objectAtIndex:2];
        }
        // set the new title to the button
        [actionButton setTitle:newTitle];
    }
    else if (requestName == TeamieUserPollThoughtRequest) {
        if (![response isKindOfClass:[NSArray class]]) {
            return;
        }
        
        // get the attachment object
        NewsfeedItemAttachment* attachment = [response objectAtIndex:0];
        // if the current thought has any poll attachments remove them
        for (NewsfeedItemAttachment* currentAttachment in self.thoughtData.attachment) {
            if ([currentAttachment.type isEqualToString:@"poll"]) {
                [TeamieRestAPI deleteEntityObject:currentAttachment];
                break;
            }
        }
        // now add the current newsfeed attachment object
        [self.thoughtData addAttachmentObject:attachment];
        // save the entity in the shared object manager object context
        [self.thoughtData saveObjectInContext:[RKObjectManager sharedManager].objectStore.primaryManagedObjectContext];
        
        // sort the choice in the proper order
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"choiceKey" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray* finalSortedEntries = [attachment.pollOptions sortedArrayUsingDescriptors:sortDescriptors];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        
        for (NSUInteger i = 0; i < finalSortedEntries.count; i++) {
            // Update the poll option values
            NewsfeedItemAttachmentPollOption* option = (NewsfeedItemAttachmentPollOption*)[finalSortedEntries objectAtIndex:i];
            CGFloat fraction = [option.numVotes doubleValue] / [attachment.totalVotes doubleValue];
            
#ifdef DEBUG
            NSLog(@"Poll fraction for option %i updated to: %g", i, fraction);
#endif
            
            [((UILabel*)[_pollLabels objectAtIndex:i]) setText:option.title];
//            [[_pollBars objectAtIndex:i] setProgress:fraction];
            [[_pollCheckboxes objectAtIndex:i] setSelected:[option.currentUserVote boolValue]];
        }
        
        [UIView commitAnimations];
    }
    else if (requestName == TeamieUserThoughtCommentVoteRequest) {
        [self loadThoughtWithTid:thoughtData.tid reload:YES];
    }
    else if (requestName == TeamieUserThoughtCommentMarkRightRequest) {
        [self loadThoughtWithTid:thoughtData.tid reload:YES];
    }
}

#pragma mark - Private methods

-(void)thoughtDataLoadSuccessful {
    
    // Add the thought action button to the navigation bar
    [self addThoughtActionButton];
    
    // Unset all poll options, so that in case thought is reloaded they're empty
    for (ADVPercentProgressBar* bar in _pollBars) {
        [bar removeFromSuperview];
    }
    for (UIButton* checkbox in _pollCheckboxes) {
        [checkbox removeFromSuperview];
    }
    for (UILabel* label in _pollLabels) {
        [label removeFromSuperview];
    }
    [_pollBars removeAllObjects];
    [_pollCheckboxes removeAllObjects];
    [_pollKeys removeAllObjects];
    [_pollLabels removeAllObjects];
    
    self.navigationItem.title = [self.thoughtData.type capitalizedString];
    
    ////////////////////////////////////////
    // set Display properties of UI elements
    ////////////////////////////////////////
    /*self.thoughtAuthorName.font = TTNEWSFEEDSTYLEVAR(userNameFont);
    [self.thoughtAuthorDisplayPic.layer setCornerRadius:UI_PICTURE_BORDER_RADIUS];
    self.thoughtAuthorDisplayPic.layer.masksToBounds = YES;

    self.thoughtMessageBody.font = TTNEWSFEEDSTYLEVAR(thoughtFont);
    self.thoughtMessageBody.textColor = TTNEWSFEEDSTYLEVAR(textColor);
    self.thoughtMessageBody.highlightedTextColor = TTNEWSFEEDSTYLEVAR(highlightedTextColor);
    self.thoughtMessageBody.backgroundColor = TTNEWSFEEDSTYLEVAR(backgroundTextColor);
    self.thoughtMessageBody.textAlignment = UITextAlignmentLeft;
    self.thoughtMessageBody.contentMode = UIViewContentModeLeft;
    
    self.thoughtClassroomName.titleLabel.font = TTNEWSFEEDSTYLEVAR(buttonFont);
    [self.thoughtClassroomName setHidden:NO];
    
    self.thoughtTimestamp.font = TTNEWSFEEDSTYLEVAR(thoughtTimestampFont);
    self.thoughtTimestamp.textColor = TTNEWSFEEDSTYLEVAR(timestampTextColor);
    self.thoughtTimestamp.highlightedTextColor = [UIColor whiteColor];*/
    
    ////////////////////////////////////////
    // set the data value & layout
    ////////////////////////////////////////

    self.thoughtAuthorName.text = self.thoughtData.author.fullname;
    
    //[self.thoughtAuthorDisplayPic setUrlPath:self.thoughtData.author.displayPic];

    CGFloat nowX = 0.0;
    CGFloat nowY = 0.0;
    CGFloat totalWidth = self.thoughtView.frame.size.width;
    
    // Make sure the thought body text is encoded as proper HTML before being printed on screen.
    //self.thoughtMessageBody.text = [TTStyledText textFromXHTML:[TeamieGlobals encodeStringToXHTML:self.thoughtData.htmlmessage]];
    [self.thoughtMessageBody sizeToFit];
    
    nowY = self.thoughtMessageBody.frame.origin.y + self.thoughtMessageBody.frame.size.height + UI_SMALL_MARGIN;
    nowX = self.thoughtInfoBar.frame.origin.x;
    
    [self.thoughtTypeIndicator setImage:[UIImage imageNamed:([self.thoughtData.type isEqualToString:@"thought"] ? @"icon-thought-type1.png": @"icon-thought-type2.png")]];
    [self.thoughtTimestamp setText:self.thoughtData.timestampText];
    [self.thoughtClassroomName setTitle:self.thoughtData.classrooms.name forState:UIControlStateNormal];
    
    // if the thought has poll attached, then display the poll options
    for (NewsfeedItemAttachment* item in self.thoughtData.attachment) {
        if ([item.type isEqualToString:@"poll"]) {
            // sort the poll options according to choice_1, choice_2 etc.
            NSSortDescriptor *sortDescriptor;
            sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"choiceKey" ascending:YES];
            NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
            NSArray* finalSortedEntries = [item.pollOptions sortedArrayUsingDescriptors:sortDescriptors];
            
            /*for (NewsfeedItemAttachmentPollOption* pollOption in finalSortedEntries) {
                
                CGFloat tempX = nowX;
                CGFloat tempY = nowY;
                tempX += 30.0 + UI_SMALL_MARGIN;
                
                UILabel* pLabel = [[UILabel alloc] initWithFrame:CGRectMake(tempX, tempY, totalWidth - tempX, 15.0)];
                [pLabel setFont:TTNEWSFEEDSTYLEVAR(thoughtPollFont)];
                [pLabel setTextColor:TTNEWSFEEDSTYLEVAR(textColor)];
                [pLabel setText:pollOption.title];
                [pLabel setNumberOfLines:0];
                [pLabel sizeToFit];
                tempY += pLabel.frame.size.height;
                
                ADVPercentProgressBar* pBar = [[ADVPercentProgressBar alloc] initWithFrame:CGRectMake(tempX, tempY, TTNEWSFEEDSTYLEVAR(kTableNewsfeedCellPollOptionWidth), TTNEWSFEEDSTYLEVAR(kTableNewsfeedCellPollOptionHeight)) andProgressBarColor:ADVProgressBarBlack];
                CGFloat fraction = [pollOption.numVotes doubleValue] / [item.totalVotes doubleValue];
                [pBar setProgress:fraction];
                
                tempY += TTNEWSFEEDSTYLEVAR(kTableNewsfeedCellPollOptionHeight) + UI_SMALL_MARGIN;
                
                UIButton* pButton = [TeamieGlobals createCheckboxButton:CGRectMake(nowX, nowY + (pLabel.frame.size.height + TTNEWSFEEDSTYLEVAR(kTableNewsfeedCellPollOptionHeight) - 30.0) / 2.0, 30.0, 30.0)];
                [pButton addTarget:self action:@selector(thoughtPollOptionSelected:) forControlEvents:UIControlEventTouchUpInside];
                [pButton setSelected:[pollOption.currentUserVote boolValue]];
                
                nowY = tempY;
                
                [_pollBars addObject:pBar];
                [_pollLabels addObject:pLabel];
                [_pollCheckboxes addObject:pButton];
                [_pollKeys addObject:pollOption.choiceKey];
                
                [self.thoughtView addSubview:pLabel];
                [self.thoughtView addSubview:pBar];
                [self.thoughtView addSubview:pButton];
            }*/
        }
    }
    
    // Move the thought type indicator, label & button according to the nowY value
    CGRect tempFrame = self.thoughtInfoBar.frame;
    tempFrame.origin.y = nowY;
    self.thoughtInfoBar.frame = tempFrame;
    
    nowY += self.thoughtInfoBar.frame.size.height + UI_SMALL_MARGIN;
    
    // move the comment stream header just below the thoughtInfo bar
    tempFrame = self.commentStreamHeader.frame;
    tempFrame.origin.y = nowY;
    self.commentStreamHeader.frame = tempFrame;
    
    nowY += 15.0;

    // layout the comments of the thought
    [self layoutThoughtCommentsInFrame:CGRectMake(0.0, nowY, self.view.frame.size.width, self.thoughtView.bounds.size.height - nowY) scrollToLatest:NO];
    
    nowY += self.commentStreamView.frame.size.height + UI_SMALL_MARGIN;
    
    // set the UITextField delegate    
    [self.commentEditor setDelegate:self];
    
    // Add notification observers for the keyboard that pops up
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    // instantiate a tap recognizer
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                            action:@selector(didTapAnywhere:)];
}

- (void)layoutThoughtCommentsInFrame:(CGRect)viewFrame scrollToLatest:(BOOL)scrollAnimate {
    
    CGRect tempFrame;
    CGRect latestCommentFrame = CGRectZero;      // variable to store the frame of the latest comment
    
    // empty self.commentStreamView of all subviews before adding the comments
    NSArray* commentViews = [[self.commentStreamView subviews] copy];
    for (id commentItem in commentViews) {
        [commentItem removeFromSuperview];
    }
    
    // for each comment add the commentViews
    if (self.thoughtData.comments.count > 0) {
        
        // sort the user comments in ascending time order
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"created" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray* sortedResponse = [[self.thoughtData.comments allObjects] sortedArrayUsingDescriptors:sortDescriptors];
        
        CGFloat commentY = UI_SMALL_MARGIN;
        
        for (NewsfeedItemComment* comment in sortedResponse) {
            UIView* commentView = [self createCommentView:comment thoughtType:thoughtData.type];
            tempFrame = commentView.frame;
            tempFrame.origin.x = UI_SMALL_MARGIN;
            tempFrame.origin.y = commentY;
            commentView.frame = tempFrame;
            [self.commentStreamView addSubview:commentView];     // add the comment to the screen view
            
            commentY += tempFrame.size.height + UI_SMALL_MARGIN;
            
            if (scrollAnimate) {
                latestCommentFrame = [commentView convertRect:commentView.bounds toView:self.thoughtView];
                // latestCommentFrame.origin.y += viewFrame.origin.y;
            }
        }
        
        if (commentY > viewFrame.size.height) { // if the height of the comments view is more than the height provided
            viewFrame.size.height = commentY;
        }
    
        self.commentStreamView.frame = viewFrame;
    }
    else {
        // To-do: Something when there are no comments
        if (viewFrame.size.height < self.commentStreamView.frame.size.height) {
            viewFrame.size.height = self.commentStreamView.frame.size.height;
        }
        self.commentStreamView.frame = viewFrame;
    }
    
    CGFloat thoughtViewHeight = self.commentStreamView.frame.size.height + self.commentStreamView.frame.origin.y + UI_SMALL_MARGIN;
    
    // adjust the contentsize of the scrollview according to new layout
    // Assuming that there is nothing that comes after commentStreamView
    self.thoughtView.contentSize = CGSizeMake(self.view.frame.size.width, thoughtViewHeight);
    
    // scroll to the latest comment
    if (scrollAnimate) {
        [self.thoughtView scrollRectToVisible:latestCommentFrame animated:YES];
    }
}

- (UIView*)createCommentView:(NewsfeedItemComment*)comment thoughtType:(NSString *)thoughtType{
    
    CGFloat nowX = UI_SMALL_MARGIN;
    CGFloat nowY = UI_SMALL_MARGIN;
    CGFloat totalWidth = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?758.0:310.0;
    
    UIView* commentView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, totalWidth, 30.0)];
    
    // create the display pic
    /*TTImageView* commentAuthor = [[TTImageView alloc] initWithFrame:CGRectMake(nowX, nowY, 35.0, 35.0)];
    [commentAuthor setUrlPath:comment.author.displayPic];
    [commentAuthor.layer setCornerRadius:UI_PICTURE_BORDER_RADIUS];
    [commentAuthor.layer setMasksToBounds:YES];
    
    nowX = commentAuthor.frame.origin.x + commentAuthor.frame.size.width + UI_SMALL_MARGIN;

    UILabel* commentAuthorName = [[UILabel alloc] initWithFrame:CGRectMake(nowX, nowY, 200.0, 15.0)];
    commentAuthorName.text = comment.author.fullname;
    [commentAuthorName setTextColor:TTNEWSFEEDSTYLEVAR(textColor)];
    commentAuthorName.font = TTNEWSFEEDSTYLEVAR(thoughtCommentAuthorNameFont);
    [commentAuthorName setBackgroundColor:TTNEWSFEEDSTYLEVAR(backgroundTextColor)];
    
    nowX = totalWidth - UI_SMALL_MARGIN - 100.0;
    
    UILabel* commentTimestamp = [[UILabel alloc] initWithFrame:CGRectMake(nowX, nowY, 100.0, 15.0)];
    commentTimestamp.text = [TeamieGlobals timeIntervalWithStartTime:[comment.created doubleValue] withEndTime:0];
    commentTimestamp.textAlignment = UITextAlignmentRight;
    [commentTimestamp setTextColor:TTNEWSFEEDSTYLEVAR(timestampTextColor)];
    [commentTimestamp setFont:TTNEWSFEEDSTYLEVAR(thoughtCommentTimestampFont)];
    [commentTimestamp setBackgroundColor:[UIColor clearColor]];
    
    nowX = commentAuthor.frame.origin.x + commentAuthor.frame.size.width + UI_SMALL_MARGIN;
    nowY = commentAuthorName.frame.size.height + UI_SMALL_MARGIN;
    
    // create the comment body label
    TTStyledTextLabel* commentBody = [[TTStyledTextLabel alloc] initWithFrame:CGRectMake(nowX, nowY, totalWidth - nowX - UI_SMALL_MARGIN, 10.0)];
    commentBody.font = TTNEWSFEEDSTYLEVAR(thoughtCommentFont);
    commentBody.textColor = TTNEWSFEEDSTYLEVAR(textColor);
    commentBody.highlightedTextColor = TTNEWSFEEDSTYLEVAR(highlightedTextColor);
    commentBody.backgroundColor = TTNEWSFEEDSTYLEVAR(backgroundTextColor);
    commentBody.textAlignment = UITextAlignmentLeft;
    commentBody.contentMode = UIViewContentModeLeft;
    // @TODO: Implement clickable comment username tag to open UserProfileViewController
    // @TODO: Implement clickable links too open TeamieWebViewController
    // It's necessary to use the encodeStringToXHTML to ensure that the string is in proper XHTML format. Otherwise won't get printed.
    commentBody.text = [TTStyledText textFromXHTML:[TeamieGlobals encodeStringToXHTML:comment.htmlMessage] lineBreaks:YES URLs:YES];
    [commentBody sizeToFit];
    
    nowX = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)?640.0:195.0;
    nowY += commentBody.frame.size.height + UI_SMALL_MARGIN;
    
    if ([thoughtType isEqualToString:@"question"]) {
        
        UIButton* commentMarkRight = [[UIButton alloc] initWithFrame:CGRectMake(nowX - UI_MEDIUM_LARGE_MARGIN, nowY, 20.0, 20.0)];
        if ([comment.markRightAccess integerValue] == 1) {
            if ([comment.markRightStatus integerValue] == 0) {
                [commentMarkRight setImage:[UIImage imageNamed:@"icon-thought-comment-mark-correct-neutral.png"] forState:UIControlStateNormal];
            }
            else {
                [commentMarkRight setImage:[UIImage imageNamed:@"icon-thought-comment-mark-correct.png"] forState:UIControlStateNormal];
            }
            [commentMarkRight addTarget:self action:@selector(markAsRight:) forControlEvents:UIControlEventTouchUpInside];
            commentMarkRight.tag = [comment.cid integerValue];
            [commentView addSubview:commentMarkRight];
        }
        
        nowX += UI_MEDIUM_MARGIN;
        UILabel* commentVoteCount = [[UILabel alloc] initWithFrame:CGRectMake(nowX, nowY + 2.5, 45.0, 15.0)];
        commentVoteCount.text = [comment.voteCount stringValue];
        if ([comment.voteCount integerValue] == 1) {
            commentVoteCount.text = [commentVoteCount.text stringByAppendingString:@" Vote"];
        }
        else {
            commentVoteCount.text = [commentVoteCount.text stringByAppendingString:@" Votes"];
        }
        [commentVoteCount setTextColor:TTNEWSFEEDSTYLEVAR(textColor)];
        [commentVoteCount setFont:TTNEWSFEEDSTYLEVAR(thoughtCommentVoteCountFont)];
        [commentVoteCount setBackgroundColor:TTNEWSFEEDSTYLEVAR(backgroundTextColor)];
        nowX += commentVoteCount.frame.size.width + UI_SMALL_MARGIN;
        
        UIButton* commentVoteUp = [[UIButton alloc] initWithFrame:CGRectMake(nowX, nowY, 20.0, 20.0)];
        if ([comment.voteUpStatus integerValue] == 1) {
            [commentVoteUp setImage:[UIImage imageNamed:@"icon-thought-comment-vote-up.png"] forState:UIControlStateNormal];
        }
        else {
            [commentVoteUp setImage:[UIImage imageNamed:@"icon-thought-comment-vote-up-neutral.png"] forState:UIControlStateNormal];
        }
        commentVoteUp.tag = [comment.cid integerValue];
        [commentVoteUp addTarget:self action:@selector(upVote:) forControlEvents:UIControlEventTouchUpInside];
        [commentVoteUp setSelected:YES];
        
        nowX += commentVoteUp.frame.size.width + UI_MEDIUM_MARGIN;
        
        UIButton* commentVoteDown = [[UIButton alloc] initWithFrame:CGRectMake(nowX, nowY, 20.0, 20.0)];
        if ([comment.voteDownStatus integerValue] == 1) {
            [commentVoteDown setImage:[UIImage imageNamed:@"icon-thought-comment-vote-down.png"] forState:UIControlStateNormal];
        }
        else {
            [commentVoteDown setImage:[UIImage imageNamed:@"icon-thought-comment-vote-down-neutral.png"] forState:UIControlStateNormal];
        }
        commentVoteDown.tag = [comment.cid integerValue];
        [commentVoteDown addTarget:self action:@selector(downVote:) forControlEvents:UIControlEventTouchUpInside];

        if ([comment.author.uid integerValue] != [self.apiCaller.currentUser.uid integerValue]) {
            [commentView addSubview:commentVoteUp];
            [commentView addSubview:commentVoteDown];
        }
        [commentView addSubview:commentVoteCount];
    }
    
    nowY += commentTimestamp.frame.size.height + UI_SMALL_MARGIN;
    commentView.frame = CGRectMake(0, 0, totalWidth, nowY);
    
    if ([thoughtType isEqualToString:@"question"]) {
        if ([comment.isCorrect integerValue] >= 1) {
            UIView* commentEdge = [[UIView alloc] initWithFrame:CGRectMake(commentView.frame.origin.x -5.0, commentView.frame.origin.y, 5.0, commentView.frame.size.height)];
            [commentEdge setBackgroundColor:[UIColor colorWithHex:@"#00b143" alpha:1.0]];
            [commentView addSubview:commentEdge];
        }
    }

    // add to the commentView subview
    [commentView addSubview:commentAuthor];
    [commentView addSubview:commentAuthorName];
    [commentView addSubview:commentBody];
    [commentView addSubview:commentTimestamp];*/
    
    return commentView;
}

- (void)addThoughtActionButton {
    
    UIBarButtonItem* moreActionsButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(showThoughtActionSheet:)];
    [moreActionsButton setTintColor:[UIColor colorWithHex:@"#d94c5a" alpha:1.0]];
//    [self.navigationController.navigationBar.topItem setRightBarButtonItem:moreActionsButton];

    
    NSPredicate* filterPredicate = [NSPredicate predicateWithFormat:@"(name == 'like' || name == 'same') && access == 1"];
    NSSet* filteredActions = [self.thoughtData.actions filteredSetUsingPredicate:filterPredicate];
    
    if ([filteredActions count] <= 0) {
        [self.navigationController.navigationBar.topItem setRightBarButtonItem:moreActionsButton animated:YES];
        return;
    }
    
    NewsfeedItemAction* thoughtAction = [filteredActions anyObject];

    NSArray* defaultMenus = [TeamieGlobals defaultTeamieThoughtActionMenus];
    
    // Determine the thought action button accordingly
    NSString* buttonTitle = [thoughtAction.name isEqualToString:@"like"] ? ([thoughtAction.status boolValue] ? [defaultMenus objectAtIndex:1] : [defaultMenus objectAtIndex:0]) : ([thoughtAction.status boolValue] ? [defaultMenus objectAtIndex:3] : [defaultMenus objectAtIndex:2]);
        
    // set the title of the thought action button
    UIBarButtonItem* actionButton = [[UIBarButtonItem alloc] initWithTitle:buttonTitle style:UIBarButtonItemStyleBordered target:self action:@selector(thoughtActionSelected:)];
    [actionButton setTintColor:[UIColor colorWithHex:@"#d94c5a" alpha:1.0]];
    
    [self.navigationController.navigationBar.topItem setRightBarButtonItems:[NSArray arrayWithObjects:moreActionsButton, actionButton, nil] animated:YES];
    
}

- (void)showThoughtActionSheet:(id)sender {
    // The app supports only delete action now. So check if the user has Delete access to the thought.
    NSPredicate* filterPredicate = [NSPredicate predicateWithFormat:@"name == 'delete-thought' && access == 1"];
    
    NSArray* thoughtActionsData = [self.thoughtData.actions allObjects];
    NSArray* filteredActions = [thoughtActionsData filteredArrayUsingPredicate:filterPredicate];

    NSString* actionSheetTitle = TeamieLocalizedString(@"TITLE_THOUGHT_ACTIONS", @"Thought Actions"); //Action Sheet Title
    NSString* deleteAction; //Action Sheet Button Titles
    NSString* refreshAction = TeamieLocalizedString(@"BTN_REFRESH", @"Refresh");
    NSString* editAction;
    NSString* reportAction;
    NSString* followAction;
    NSString* cancelAction = TeamieLocalizedString(@"BTN_CANCEL", @"Cancel");
    
    int count = 0;
    while (count < [filteredActions count]) {
        NewsfeedItemAction* thoughtAction = [filteredActions objectAtIndex:count];
        
        if ([thoughtAction.name isEqualToString:@"edit-thought"] && [thoughtAction.access boolValue]) {
            editAction = @"Edit";
        }
        if ([thoughtAction.name isEqualToString:@"delete-thought"] && [thoughtAction.access boolValue]) {
            deleteAction = @"Delete";
        }
        if ([thoughtAction.name isEqualToString:@"report"] && [thoughtAction.access boolValue]) {
            reportAction = @"Report";
        }
        if ([thoughtAction.name isEqualToString:@"follow"] && [thoughtAction.access boolValue]) {
            followAction = @"Follow";
        }
        count++;
    }
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelAction
                                  destructiveButtonTitle:deleteAction
//                                  Removed editAction for now, edit attachment not yet implemented
//                                  otherButtonTitles:refreshAction, editAction, followAction, reportAction, nil];
                                  otherButtonTitles:refreshAction, followAction, reportAction, nil];
    [actionSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    //Get the name of the current pressed button
    NSString *buttonTitle = [actionSheet buttonTitleAtIndex:buttonIndex];
    if  ([buttonTitle isEqualToString:@"Delete"]) {
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:@"Are you sure?" message:TeamieLocalizedString(@"Are you sure you want to delete this $thought[s]$?", nil) delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
        alertView.tag = kAlertViewWillDelete;
        [alertView show];
        //After Delete button is pressed Thought Delete Request is sent
    }
    if ([buttonTitle isEqualToString:@"Refresh"]) {
        [self thoughtRefreshed];
    }
}

#pragma mark - Alert view delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == [alertView cancelButtonIndex]) {
        if (alertView.tag == kAlertViewWillDelete) {
            return;
        }
    }
    else {
        [self.apiCaller performRestRequest:TeamieUserDeleteThoughtRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:thoughtData.tid, @"tid", nil]];
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_THOUGHT_DELETE_PROGRESS", @"Deleting thought...") margin:10.0f yOffset:10.0f];
    }
    
}


#pragma mark - Lazy instantiation methods
/*-(TTTableViewController*)commentStreamController {
    
    if (_commentStreamController == nil) {
        _commentStreamController = [[TTTableViewController alloc] initWithStyle:UITableViewStylePlain];
    }
    return _commentStreamController;
    
}*/

-(NSNumber*)thoughtID {
    if (_thoughtID == nil) {
        _thoughtID = [[NSNumber alloc] init];
    }
    return _thoughtID;
}

#pragma mark - Public API methods

-(void)dismissThoughtView:(id)sender {
    
    [self dismissModalViewControllerAnimated:YES];
    if ([self.thoughtViewDelegate respondsToSelector:@selector(didDismissThoughtView:)]) {
        // pass the thought data object back to the presenting view controller so it would update its view
        [self.thoughtViewDelegate performSelector:@selector(didDismissThoughtView:) withObject:self.thoughtData];
    }
    
    //[[GANTracker sharedTracker] trackEvent:@"Thought View" action:@"Thought View Cancel" label:nil value:[self.thoughtData.tid longLongValue] withError:nil];
}

- (void)thoughtActionSelected:(id)sender {
    
    if (![sender isKindOfClass:[UIBarButtonItem class]]) {
#ifdef DEBUG
        NSLog(@"Error: Thought action button is not a UIBarButtonItem");
#endif
        return;
    }
    
    NSString* actionName = ((UIBarButtonItem*)sender).title;
    NSArray* thoughtActionMenus = [TeamieGlobals defaultTeamieThoughtActionMenus];
    
    NSInteger actionIndex = [thoughtActionMenus indexOfObject:actionName];
    
    if (actionIndex == NSNotFound) {
#ifdef DEBUG
        NSLog(@"Error: Thought action menu '%@' could not be found in the default menu list", actionName);
#endif
        return;
    }
    else {
        TeamieRESTRequest requestToMake;
        
        if (actionIndex == 0 || actionIndex == 1) {     // Like or unlike
            requestToMake = TeamieUserLikeThoughtRequest;
        }
        else if (actionIndex == 2 || actionIndex == 3) {    // Echo or unecho
            requestToMake = TeamieUserSameThoughtRequest;
        }
        else {
#ifdef DEBUG
            NSLog(@"Error: This request (%@) is not yet supported!", actionName);
#endif
            return;
        }
        
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_PROCESSING", nil) margin:10.f yOffset:10.f];
        
        [self.apiCaller performRestRequest:requestToMake withParams:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%qi", [self.thoughtData.tid longLongValue]], @"tid", nil]];
    }
    
    //[[GANTracker sharedTracker] trackEvent:@"Thought View" action:@"Thought Action" label:actionName value:[self.thoughtData.tid longLongValue] withError:nil];
}

- (void)thoughtPollOptionSelected:(id)sender {

    UIButton* selectedButton = (UIButton*)sender;
    BOOL checked = ![selectedButton isSelected];
    [selectedButton setSelected:checked];
    
    NSUInteger pollIndex = [_pollCheckboxes indexOfObject:selectedButton];
    if (pollIndex == NSNotFound) {
        return;
    }
    
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_PROCESSING", nil) margin:10.f yOffset:10.f];
    NSString* choiceKey = [NSString stringWithFormat:@"%d", [TeamieGlobals extractNumberFromString:[_pollKeys objectAtIndex:pollIndex]]];
    
    NSDictionary* params = [NSDictionary dictionaryWithObjectsAndKeys: [NSString stringWithFormat:@"%qi", [self.thoughtData.tid longLongValue]], @"tid", @"1", @"num_choices", choiceKey, @"choices[1][choice]", (checked ? @"1" : @"0"), @"choices[1][value]", nil];
    
    [self.apiCaller performRestRequest:TeamieUserPollThoughtRequest withParams:params];
    
    //[[GANTracker sharedTracker] trackEvent:@"Thought View" action:@"Thought Poll Selected" label:nil value:[self.thoughtData.tid longLongValue] withError:nil];
}

#pragma mark - IBAction methods

- (IBAction)postThoughtComment:(id)sender {
    
    // disable the send button
    [sender setEnabled:NO];
    
    // show an activity label
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_COMMENT_IN_PROGRESS", nil) margin:10.f yOffset:10.f];
    
    [self.apiCaller performRestRequest:TeamieUserPostThoughtCommentRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%qi", [self.thoughtID longLongValue]], @"tid", self.commentEditor.text, @"message", nil]];
    
    //[[GANTracker sharedTracker] trackEvent:@"Thought View" action:@"Thought Comment Post" label:nil value:[self.thoughtID longLongValue] withError:nil];
}

- (IBAction)textfieldEditingChanged:(id)sender {
    
    [self.commentPostButton setEnabled:([self.commentEditor.text length] > 0)];
    
}

#pragma mark - Keyboard Delegate methods
- (void)keyboardWillShow:(NSNotification *)note {
    
    [self.thoughtView addGestureRecognizer:tapRecognizer]; 
    
    // move the comment editor according to the keyboard
    [TeamieGlobals moveControl:self.bottomToolbar forKeyboard:note up:YES distance:0 inView:self.view];
    
}

- (void)keyboardWillHide:(NSNotification *)note {
    
    [self.thoughtView removeGestureRecognizer:tapRecognizer];
    [TeamieGlobals moveControl:self.bottomToolbar forKeyboard:note up:NO distance:0 inView:self.view];
}

- (void)didTapAnywhere:(UITapGestureRecognizer *)recognizer {
    
    [self.commentEditor resignFirstResponder];
    
}

#pragma mark TTModelViewController methods
//- (id<UITableViewDelegate>)createDelegate {
//    return [[TTTableViewDragRefreshDelegate alloc] initWithController:self];
//}

#pragma mark Comment Action methods
- (void)upVote:(UIButton *)sender {
    NSInteger commentID = sender.tag;
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:commentID], @"cid", @"up", @"action", nil];
    [self.apiCaller performRestRequest:TeamieUserThoughtCommentVoteRequest withParams:params];
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_COMMENTS", nil) margin:10.f yOffset:10.f];
}

- (void)downVote:(UIButton *)sender {
    NSInteger commentID = sender.tag;
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:commentID], @"cid", @"down", @"action", nil];
    [self.apiCaller performRestRequest:TeamieUserThoughtCommentVoteRequest withParams:params];
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_COMMENTS", nil) margin:10.f yOffset:10.f];
}

- (void)markAsRight:(UIButton *)sender {
    NSInteger commentID = sender.tag;
    NSDictionary* params = [[NSDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithInteger:commentID], @"cid", nil];
    [self.apiCaller performRestRequest:TeamieUserThoughtCommentMarkRightRequest withParams:params];
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_COMMENTS", nil) margin:10.f yOffset:10.f];
}


#pragma mark Delegate methods to navigate to WebView from TTStyledText
/*- (BOOL)navigator:(TTBaseNavigator *)navigator shouldOpenURL:(NSURL *) URL {
    // setup a webcontroller with the URL and push it
    TeamieWebViewController * webController = [[TeamieWebViewController alloc] init];
    [webController openURL:URL];
    webController.modalPresentationStyle = UIModalPresentationFullScreen;
    webController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webController];
    [self presentViewController:navigationController animated:YES completion:nil];
    return NO;
}*/

#pragma mark Motion detection delegates

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    
    if (event.subtype == UIEventSubtypeMotionShake) {
        // If device is shaked, capture the Motion Shake Event and do something
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);    //Vibrate Device
        
        // find all local newsfeed items
        NSArray* existingNewsfeedItems = [NewsfeedItem findAllSortedBy:@"timestamp" ascending:NO inContext:[RKObjectManager sharedManager].objectStore.primaryManagedObjectContext];
        
        if ([existingNewsfeedItems count] >= 1) {
            // randomIndex for generating a random Index from the NewsfeedObjects count
            NSUInteger randomIndex = arc4random() % [existingNewsfeedItems count];
            NewsfeedItem *thoughtItem = [existingNewsfeedItems objectAtIndex:randomIndex];
            [self loadThoughtWithTid:[thoughtItem tid] reload:YES];
        }
    }
    
    if ([super respondsToSelector:@selector(motionEnded:withEvent:)]) {
        [super motionEnded:motion withEvent:event];
    }
}

// TODO: Implement keyboardDidShow delegate methods
#pragma mark Text field delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    thoughtView.frame = CGRectMake(thoughtView.frame.origin.x, thoughtView.frame.origin.y, thoughtView.frame.size.width, thoughtView.frame.size.height-215); //original setup
//    [self.thoughtView scrollRectToVisible:self.commentStreamView.frame animated:YES];
}

-(void)textFieldDidEndEditing:(UITextField *)textField {
    thoughtView.frame = CGRectMake(thoughtView.frame.origin.x, thoughtView.frame.origin.y, thoughtView.frame.size.width, thoughtView.frame.size.height+215); //original setup
//    [self.thoughtView scrollRectToVisible:self.commentStreamView.frame animated:YES];
}

@end