//
//  NSData+TMEConversion.m
//  TeamieApp5
//
//  Obtained from http://stackoverflow.com/a/9084784/1106264
//  Created by Nalin Ilango on 24/2/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "NSData+TMEConversion.h"

@implementation NSData (TMEConversion)

- (NSString *)hexadecimalString {
    /* Returns hexadecimal string of NSData. Empty string if data is empty.*/
    
    const unsigned char *dataBuffer = (const unsigned char *)[self bytes];
    
    if (!dataBuffer)
        return [NSString string];
    
    NSUInteger          dataLength  = [self length];
    NSMutableString     *hexString  = [NSMutableString stringWithCapacity:(dataLength * 2)];
    
    for (int i = 0; i < dataLength; ++i)
        [hexString appendString:[NSString stringWithFormat:@"%02lx", (unsigned long)dataBuffer[i]]];
    
    return [NSString stringWithString:hexString];
}

@end
