//
//  TeamieChartsViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 10/19/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"

#import "Popover/WEPopoverController.h"
#import "PopupMenuViewController.h"
#import "TMEClient.h"
#import <MessageUI/MessageUI.h>

@class QuizStatView, TMEGradebook;

// An Enum listing the different types of reports that the TeamieChartsViewController can display.
// Depending on the type of report this viewController will make REST requests to obtain the data to display
// For eg: If report type is TeamieGradebookReport, then the viewController will make a gradebook request and display the stats accordingly
// So to use this viewController it's better to use the initWithReportType: method
typedef NS_ENUM(NSInteger, TMEReportType){
    TeamieGradebookReport,
    TeamieDashboardReport
};

/////////////////////////////////////////////////////////////////////////
/// A private class for storing info about the type of filters that can be applied to a report

@interface ReportFilter : NSObject

@property (nonatomic, strong) NSString * filterName;
@property (nonatomic, strong) NSString * filterTitle;
@property (nonatomic, strong) NSString * filterURL;
@property (nonatomic, strong) NSNumber * filterRequestType;

- (id)initWithName:(NSString*)name requestType:(TMERequest)reportType;
- (id)initWithName:(NSString *)name requestType:(TMERequest)reportType title:(NSString*)title url:(NSString*)url;

@end

/////////////////////////////////////////////////////////////////////////

@interface TeamieChartsViewController : TableRestApiViewController <CPTPlotDataSource, CPTBarPlotDataSource, CPTBarPlotDelegate, PopoverMenuDelegate, WEPopoverControllerDelegate, MFMailComposeViewControllerDelegate> {
    TMEReportType _reportType;
    NSMutableArray * _filterList;
    NSInteger _selectedFilterIndex;
    Class _popoverClass;
    CGFloat _avgPlotNumber; // stores the average of all the plot numbers
}

// the container View for the graph
@property (nonatomic, strong) CPTGraphHostingView *hostView;
// the annotation for the bar graph to be displayed
@property (nonatomic, strong) CPTPlotSpaceAnnotation *graphAnnotation;
// the theme for the chart to be displayed
@property (nonatomic, strong) CPTTheme *selectedTheme;
// A Quiz Stat view for showing summary of a given quiz
@property (nonatomic, strong) QuizStatView * quizStatView;
// A UIView to contain both the hostViwe and the quizStatView
@property (nonatomic, strong) UIView * headerView;

// An object to store the current user's gradebook in the current classroom
@property (nonatomic, strong) TMEGradebook* studentsGradebook;

// A data member to store the classroom ID of the class for which the gradebook is being viewed
// This is needed for making the Gradebook request when the VC has finished loading.
@property (nonatomic, strong) NSNumber* classroomID;

// objects for storing the plot details
@property (nonatomic, strong) NSMutableArray* plotNumbers;
@property (nonatomic, strong) NSMutableArray* plotLabels;

// A button which when tapped displays a popover with the filter options for the current report
@property (nonatomic, strong) UIButton * filterButton;

// A UILabel to display some description about a report
@property (nonatomic, strong) UILabel * reportInfoLabel;

// a pointer to the popoverController that displays the list of filters for the current report
@property (nonatomic, strong) WEPopoverController * wePopoverController;

// the method recommended for initializing this viewController
- (id)initWithReportType:(TMEReportType)reportType;

// the dismiss method used to dismiss the viewcontroller when presented modally
- (void)dismissChartViewController:(id)sender;

// the delegate method to be called when the filter button is tapped
- (void)filterButtonTapped:(id)sender;

// the callback method invoked when the 'Export' button is tapped, for exporting data to different file formats
- (void)exportButtonTapped:(id)sender;

@end
