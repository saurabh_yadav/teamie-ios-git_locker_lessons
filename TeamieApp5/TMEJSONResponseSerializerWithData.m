//
//  TMEJSONResponseSerializerWithData.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 6/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEJSONResponseSerializerWithData.h"

@implementation TMEJSONResponseSerializerWithData

NSString * const TMEJSONResponseErrorKey = @"error_details";

- (id)responseObjectForResponse:(NSURLResponse *)response
                           data:(NSData *)data
                          error:(NSError *__autoreleasing *)error {
    id JSONObject = [super responseObjectForResponse:response data:data error:error];
    if (*error && (*error).userInfo) {
        NSMutableDictionary *userInfo = [(*error).userInfo mutableCopy];
        if (data) {
            userInfo[TMEJSONResponseErrorKey] = [NSJSONSerialization JSONObjectWithData:data
                                                                                options:NSJSONReadingMutableContainers
                                                                                  error:error]?: NSNull.null;
        }
        NSError *newError = [NSError errorWithDomain:(*error).domain code:(*error).code userInfo:userInfo];
        (*error) = newError;
    }
    
    return (JSONObject);
}

@end
