//
//  LSCollapseClickCell.h
//  CollapseClickDemo
//
//  Created by Wei Wenbo on 28/5/14.
//  Copyright (c) 2014 Ben Gordon. All rights reserved.
//

#import "CollapseClickCell.h"
#import "PageStatsLabel.h"
#import "TMELesson.h"
#import "PublishStatusLabel.h"
#import "UILabelWithPadding.h"

#define LSLessonHeaderHeight 112
#define LSCollapseHeaderHeight 49
#define LSPaddings 10
//static UIFont *dateLabelFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:9];
@interface LSCollapseClickCell : CollapseClickCell

//ContainerView
@property (weak, nonatomic) IBOutlet UIView *containerView;

//Lesson Info Header
@property (weak, nonatomic) IBOutlet UIView *LessonInfo;
@property (weak, nonatomic) IBOutlet UIImageView *authorImageView;
@property (weak, nonatomic) IBOutlet UILabel *lessonTitle;
@property (weak, nonatomic) IBOutlet UILabel *classroomInfo;
@property (weak, nonatomic) IBOutlet UILabel *deadline;
@property (weak, nonatomic) IBOutlet UILabel *publishDate;
@property (strong, nonatomic) NSArray *classroomLocker;

// Header
@property (weak, nonatomic) IBOutlet PageStatsLabel *readStats;
@property (weak, nonatomic) IBOutlet UIView *TitleView;
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *TitleButton;
@property (weak, nonatomic) IBOutlet UIButton *lessonHeaderButton;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UIButton *authorImageButton;
@property (weak, nonatomic) IBOutlet UIView *authorImageContentView;


// Body
@property (weak, nonatomic) IBOutlet UIView *ContentView;
@property (weak, nonatomic) IBOutlet UILabelWithPadding *publishStatusLabelLessons;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImage;

+ (LSCollapseClickCell *)newCollapseClickCellWithTitle:(NSString *)collapseTitle index:(int)index content:(UIView *)content lesson:(TMELesson *)lesson classroomID:(NSNumber *)classroomID;


@end
