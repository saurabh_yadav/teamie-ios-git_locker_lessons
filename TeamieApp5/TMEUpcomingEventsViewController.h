//
//  TMEUpcomingEventsViewController.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TeamieRevealController.h"
#import "TMEListViewController.h"

@interface TMEUpcomingEventsViewController : TMEListViewController <TMERevealDelegate>

@property (nonatomic, weak) UIViewController *parentRevealController;

@end
