//
//  ResourcesShareMenuViewController.m
//  TeamieApp5
//
//  Created by Raunak on 19/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ResourcesShareMenuViewController.h"

@interface ResourcesShareMenuViewController ()

@property (nonatomic, strong) UITableView* tableView;

@end

@implementation ResourcesShareMenuViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    if ([self.delegate respondsToSelector:@selector(shareMenuShouldDisplayOpenInOption)] && ![self.delegate shareMenuShouldDisplayOpenInOption]) {
        self.contentSizeForViewInPopover = CGSizeMake(200, 65);
    }
    else {
        self.contentSizeForViewInPopover = CGSizeMake(200, 110);
    }
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.scrollEnabled = NO;
    self.tableView.bounces = NO;
    [self.view addSubview:self.tableView];
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count;
    if ([self.delegate respondsToSelector:@selector(shareMenuShouldDisplayOpenInOption)]) {
        count = [self.delegate shareMenuShouldDisplayOpenInOption] + 1;
    }
    else {
        count = 2;
    }
    self.tableView.frame = CGRectMake(CGRectGetMinX(self.tableView.frame), CGRectGetMinY(self.tableView.frame), CGRectGetWidth(self.tableView.frame), (count * 45) + 20.0);
    return count;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"menuCellIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menuCellIdentifier"];
    }
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Share";
            break;
        case 1:
            cell.textLabel.text = @"Open In..";
            break;
        default:
            break;
    }
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.delegate shareMenuDidSelectDeleteOption:((ShareMenuOptions)indexPath.row)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
