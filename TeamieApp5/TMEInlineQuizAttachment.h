//
//  TMEInlineQuizAttachment.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostAttachment.h"

@interface TMEInlineQuizAttachment : TMEPostAttachment

@property (nonatomic, strong) NSNumber *qid;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSURL *href;
@property (nonatomic) BOOL is_quiz_taken;

@end
