//
//  TeamieClient.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEBaseClient.h"
#import "TMEUserProfile.h"

@interface TMEClient : TMEBaseClient

extern NSString * const kBaseURL;

//The authorization token to be sent in the header for each request
//Use nil to unset this value
@property (nonatomic, strong) NSString *authorizationToken;

+ (instancetype)sharedClient;

- (BOOL)isLoggedIn;

- (void)getUserProfile:(NSDictionary *)parameters
                   uid:(NSNumber *)uid
        loadingMessage:(NSString *)message
               success:(void (^)(TMEUserProfile *user))successBlock
               failure:(void (^)(NSError *error))failureBlock;

- (void)login:(NSDictionary *)parameters
      success:(void (^)())successBlock
      failure:(void (^)(NSError *error))failureBlock;

- (void)logout:(void (^)())successBlock;

@end
