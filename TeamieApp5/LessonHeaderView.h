//
//  LessonHeaderView.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 30/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LessonHeaderView : UIView

//Lesson Info Header
@property (weak, nonatomic) IBOutlet UIImageView *authorImageView;
@property (weak, nonatomic) IBOutlet UILabel *lessonTitle;
@property (weak, nonatomic) IBOutlet UILabel *classroomInfo;
@property (weak, nonatomic) IBOutlet UILabel *deadline;
@end
