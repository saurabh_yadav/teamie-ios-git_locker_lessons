//
//  PKRevealController+Hide.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 23/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "PKRevealController.h"

@interface PKRevealController (Hide)
- (void)hideStatusBar:(BOOL)hide;
@end
