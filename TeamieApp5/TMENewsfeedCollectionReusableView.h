//
//  TMENewsfeedCollectionReusableView.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 06/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMENewsfeedCollectionReusableView : UICollectionReusableView

@property (strong, nonatomic) IBOutlet UIButton *lessonsButton;
@property (strong, nonatomic) IBOutlet UIButton *assessmentsButton;

- (void)styleView;

@end
