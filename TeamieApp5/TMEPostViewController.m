//
//  TMEPostViewController.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostViewController.h"

#import "TMEPostView.h"
#import "UIView+TMEEssentials.h"
#import "TMEPostComment.h"
#import "TMEPostCommentView.h"
#import "TMEClient.h"
#import "TMENewsfeedViewController.h"

#import <Masonry/Masonry.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import <SZTextView/SZTextView.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SDWebImage/UIButton+WebCache.h>

@interface TMEPostViewController()
<TMEPostViewDelegate, TMEPostCommentViewDelegate, UITextViewDelegate>

#pragma mark - Subviews

@property (nonatomic, weak) UIScrollView *scrollView;
@property (nonatomic, weak) UIView *superView;

@property (nonatomic, weak) TMEPostView *postView;
@property (nonatomic, weak) UIView *commentsView;

@property (nonatomic, weak) UIView *commentBarView;
@property (nonatomic, weak) UIButton *commentAuthorButton;
@property (nonatomic, weak) SZTextView *commentTextView;
@property (nonatomic, weak) UIButton *commentSubmitButton;

@property (nonatomic, weak) MASConstraint *textViewHeightConstraint;

@property (nonatomic, weak) MBProgressHUD *loadingProgressHUD;

@property (nonatomic, strong) UIActionSheet *postActionSheet;

#pragma mark - Public Readonly

@property (nonatomic, strong, readwrite) NSNumber *postID;
@property (nonatomic, strong, readwrite) NSNumber *commentID;
@property (nonatomic, strong, readwrite) NSNumber *replyID;
@property (nonatomic, strong, readwrite) TMEPost *post;

//If null, we are commenting on the post
@property (nonatomic, strong) NSNumber *commentToReplyOn;

//If set to true, will open with comment box
@property (nonatomic) BOOL openCommentBox;

//For handling keyboard height
@property (nonatomic) CGFloat keyboardHeight;

//For sending refresh and delete actions
@property (nonatomic, weak) UIViewController<TMEPostViewDelegate> *presentingNewsfeed;

@end

@implementation TMEPostViewController

#pragma mark - Layout Constants

static const CGFloat kDefaultSize = 25.0f;
static const CGFloat kDefaultOffset = 10.0f;

#pragma mark - Initializers

+ (instancetype)postWithId:(NSNumber *)postID {
    TMEPostViewController *postViewController = [TMEPostViewController new];
    postViewController.postID = postID;
    return postViewController;
}

+ (instancetype)postWithId:(NSNumber *)postID openComment:(BOOL)open {
    TMEPostViewController *postViewController = [TMEPostViewController postWithId:postID];
    postViewController.openCommentBox = open;
    return postViewController;
}

+ (instancetype)postWithId:(NSNumber *)postID atComment:(NSNumber *)commentID {
    TMEPostViewController *postViewController = [TMEPostViewController postWithId:postID];
    postViewController.commentID = commentID;
    //TODO: Scroll to comment
    return postViewController;
}

+ (instancetype)postWithId:(NSNumber *)postID atReply:(NSNumber *)replyID {
    TMEPostViewController *postViewController = [TMEPostViewController postWithId:postID];
    postViewController.replyID = replyID;
    //TODO: Scroll to reply
    return postViewController;
}

#pragma mark - Lifecycle

- (void)loadView {
    [super loadView];
    
    self.view = [UIView new];
    self.view.backgroundColor = [TeamieUIGlobalColors baseBgColor];
    self.scrollView = [self.view createAndAddSubView:UIScrollView.class];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    // prevents the scroll view from swallowing up the touch event of child buttons
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapGesture];
    
    self.superView = [self.scrollView createAndAddSubView:UIView.class];
    self.superView.backgroundColor = [UIColor whiteColor];
    TMEPostView *postView = [[TMEPostView alloc] initWithMode:TMEPostViewModeFull];
    postView.delegate = self;
    [self.superView addSubview:postView];
    self.postView = postView;
    
    self.presentingNewsfeed = self.navigationController.viewControllers[self.navigationController.viewControllers.count - 2];

    [[TMEClient sharedClient]
     request:TeamieUserGetThoughtRequest
     parameters:nil
     makeURL:^NSString *(NSString *URL) {
         return [NSString stringWithFormat:URL, self.postID];
     }
     loadingMessage:TMELocalize(@"message.loading-post")
     success:^(NSDictionary *response) {
         self.post = [TMEPost parseDictionary:response[@"thought"][0]];
         [self prepareNavigationBar];
         [self.postView loadPost:self.post];
         
         [self addCommentsView];
         [self addCommentBar];

         [self addConstraints];
         
         if ([self.postView hasMoreActions]) {
             self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                                       initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"ellipsis"
                                                                                                  withSize:kDefaultSize
                                                                                                  andColor:[UIColor whiteColor]]
                                                       style:UIBarButtonItemStylePlain
                                                       target:self
                                                       action:@selector(moreButtonClicked)];
         }
         if (self.openCommentBox) {
             [self commentButtonClicked:nil];
         }
     } failure:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidChange:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc]
                                             initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"back"
                                                                                        withSize:kDefaultSize
                                                                                        andColor:[UIColor whiteColor]]
                                             style:UIBarButtonItemStylePlain
                                             target:self
                                             action:@selector(backButtonClicked)];
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/thought/*"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.postActionSheet.window) {
        [self.postActionSheet dismissWithClickedButtonIndex:self.postActionSheet.cancelButtonIndex animated:animated];
    }
}

#pragma mark - Constraints

- (void)addConstraints {
    //Scroll view
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
    }];
    [self.superView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.width.equalTo(self.scrollView);
    }];
    [self.postView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.superView);
    }];
    [self.commentsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.postView.mas_bottom);
        make.left.right.bottom.equalTo(self.superView);
    }];
    
    //Comment bar view, outside the scroll view
    [self.commentBarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scrollView.mas_bottom);
        make.bottom.left.right.equalTo(self.view);
    }];
    [self.commentAuthorButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self.commentBarView);
        make.width.height.equalTo(@(kDefaultSize + 2 * kDefaultOffset));
    }];
    [self.commentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.commentBarView);
        make.bottom.equalTo(self.commentBarView);
        make.left.equalTo(self.commentAuthorButton.mas_right);
        self.textViewHeightConstraint = make.height.equalTo(@(kDefaultSize + 2 * kDefaultOffset));
    }];
    [self.commentSubmitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.equalTo(self.commentBarView);
        make.left.equalTo(self.commentTextView.mas_right);
        make.width.height.equalTo(@(kDefaultSize + 2 * kDefaultOffset));
    }];
}

#pragma mark - Adding Subviews

- (void)prepareNavigationBar {
    NSString *title = [[TMEPost typeJSONTransformer] reverseTransformedValue:@(self.post.type)];
    self.navigationItem.title = [title stringByReplacingCharactersInRange:NSMakeRange(0,1)
                                                               withString:[[title substringToIndex:1] capitalizedString]];

}

- (void)addCommentsView {
    self.commentsView = [self.superView createAndAddSubView:UIView.class];
    [self.commentsView addBorderToTop:NO bottom:YES left:NO right:NO];
    self.commentsView.translatesAutoresizingMaskIntoConstraints = NO;
    [self loadComments];
}

- (void)loadComments {
    [self.commentsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    __block TMEPostCommentView *prevComment = nil;
    
    @weakify(self)
    [self.post.comments enumerateObjectsUsingBlock:^(TMEPostComment *comment, NSUInteger idx, BOOL *stop) {
        @strongify(self)
        TMEPostCommentView *commentView = [TMEPostCommentView viewWithComment:comment delegate:self];
        [commentView addBorderToTop:YES bottom:NO left:NO right:NO];
        [self.commentsView addSubview:commentView];
        
        [commentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.commentsView);
            make.top.equalTo((prevComment)? prevComment.mas_bottom : self.commentsView.mas_top);
            if (idx == self.post.comments.count - 1) {
                make.bottom.equalTo(self.commentsView);
            }
        }];
        prevComment = commentView;
    }];
}

- (void)addCommentBar {
    self.commentBarView = [self.view createAndAddSubView:UIView.class];
    self.commentBarView.backgroundColor = [UIColor colorWithHex:@"#EEEEEE" alpha:1];
    [self.commentBarView addBorderToTop:YES bottom:NO left:NO right:NO];
    
    self.commentAuthorButton = [self.commentBarView createAndAddSubView:UIButton.class];
    self.commentAuthorButton.contentMode = UIViewContentModeScaleAspectFill;
    [self.commentAuthorButton sd_setImageWithURL:[TMEUser currentUser].user_profile_image.path forState:UIControlStateNormal];
    [self.commentAuthorButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"profile" withSize:30] forState:UIControlStateSelected];
    [self.commentAuthorButton addTarget:self action:@selector(toggleAnonymousComment) forControlEvents:UIControlEventTouchUpInside];
    
    self.commentTextView = [self.commentBarView createAndAddSubView:SZTextView.class];
    self.commentTextView.placeholder = TMELocalize(@"message.leave-comment");
    self.commentTextView.font = [TeamieGlobals appFontFor:@"postNewComment"];
    self.commentTextView.contentInset = UIEdgeInsetsMake(5, 0, 0, 0);
    self.commentTextView.editable = YES;
    self.commentTextView.delegate = self;
    
    self.commentSubmitButton = [self.commentBarView createAndAddSubView:UIButton.class];
    self.commentSubmitButton.enabled = NO;
    [self.commentSubmitButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"send"
                                        withSize:25]
                              forState:UIControlStateNormal];
    [self.commentSubmitButton addTarget:self action:@selector(submitComment) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Actions

- (void)backButtonClicked {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)moreButtonClicked {
    [self showPostMoreActionsSheet:self.postView];
}

- (void)toggleAnonymousComment {
    for (TMEClassroom *classroom in [TMEClassroom getFromUserDefaults]) {
        if ([self.post.classroom.nid isEqualToNumber:classroom.nid]) {
            if ([classroom.permissions[@"create thought comment anonymously"] boolValue]) {
                self.commentAuthorButton.selected ^= YES;
            }
        }
    }
}

- (void)submitComment {
    if (self.commentToReplyOn == nil) {
        TMETrackEvent(@"full_post", @"tap_send", @"comment");
        if (self.commentAuthorButton.selected) {
            TMETrackEvent(@"full_post", @"set_anonymous", @"comment");
        }
        
        [[TMEClient sharedClient]
         request:TeamieUserPostThoughtCommentRequest
         parameters:@{@"tid":self.post.tid,
                      @"message":self.commentTextView.text,
                      @"is_anonymous": @(self.commentAuthorButton.selected)}
         loadingMessage:TMELocalize(@"message.posting-comment")
         success:^(NSDictionary *response) {
             TMETrackEvent(@"full_post", @"create_success", @"comment");
             [self refreshView];
         } failure:nil];
    }
    else {
        TMETrackEvent(@"full_post", @"tap_send", @"reply");
        if (self.commentAuthorButton.selected) {
            TMETrackEvent(@"full_post", @"set_anonymous", @"reply");
        }
        
        [[TMEClient sharedClient]
         request:TeamieUserPostThoughtCommentReplyRequest
         parameters:@{@"tid":self.post.tid, @"cid": self.commentToReplyOn, @"message": self.commentTextView.text, @"is_anonymous": @(self.commentAuthorButton.selected)}
         loadingMessage:TMELocalize(@"message.posting-reply")
         success:^(id response) {
             TMETrackEvent(@"full_post", @"create_success", @"reply");
             [self refreshView];
         } failure:nil];
    }
}

#pragma mark - TMEPostViewDelegate

- (void)showPostMoreActionsSheet:(TMEPostView *)postView {
    self.postActionSheet = [postView moreActionsSheet];
    [self.postActionSheet showFromBarButtonItem:self.navigationItem.rightBarButtonItem
                                              animated:YES];
}

- (void)removePost:(TMEPost *)post {
    [self.presentingNewsfeed removePost:post];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)commentButtonClicked:(TMEPost *)post {
    self.commentToReplyOn = nil;
    self.commentTextView.placeholder = TMELocalize(@"message.leave-comment");
    [self showKeyboard];
}

- (void)updatePostViewHeightWithUpdatedPost:(TMEPost *)post {
    [self updateViewConstraints];
}

#pragma mark - TMEPostCommentViewDelegate

- (void)showPostCommentMoreActionsSheet:(TMEPostCommentView *)commentView {
    [[commentView moreActionsSheet] showFromRect:[commentView.moreButton convertRect:commentView.moreButton.bounds toView:self.view]
                                          inView:self.view
                                        animated:YES];
}

- (void)replyButtonClicked:(NSNumber *)commentID {
    self.commentToReplyOn = commentID;
    self.commentTextView.placeholder = TMELocalize(@"message.leave-reply");
    [self showKeyboard];
}

- (void)deleteComment:(TMEPostComment *)comment {
    NSMutableArray *temp = [self.post.comments mutableCopy];
    [temp removeObject:comment];
    self.post.comments = [temp copy];
    self.post.commentsCount = @(self.post.commentsCount.intValue - 1);
    [self.postView.commentsCountButton setTitle:[self.post.commentsCount stringValue]
                                       forState:UIControlStateNormal];
    [self loadComments];
}

- (void)deleteReply:(TMEPostCommentReply *)reply inComment:(TMEPostComment *)comment {
    NSMutableArray *temp = [comment.replies mutableCopy];
    [temp removeObject:reply];
    comment.replies = [temp copy];
    comment.repliesCount = @(comment.repliesCount.intValue - 1);
    [self loadComments];
}

#pragma mark - UITextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    [self.view layoutIfNeeded];
    self.textViewHeightConstraint.offset(self.commentTextView.contentSize.height > kDefaultSize + 2 * kDefaultOffset?
                                         self.commentTextView.contentSize.height : kDefaultSize + 2 * kDefaultOffset);
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:nil];
    self.commentSubmitButton.enabled = textView.text.length > 0;
}

#pragma mark - Keyboard events

- (void)keyboardDidChange:(NSNotification *) notification{
    NSDictionary *info = [notification userInfo];
    CGFloat startY = CGRectGetMinY([info[UIKeyboardFrameBeginUserInfoKey] CGRectValue]);
    CGFloat endY = CGRectGetMinY([info[UIKeyboardFrameEndUserInfoKey] CGRectValue]);
    CGRect rect = self.view.frame;
    rect.size.height += endY - startY;
    self.view.frame = rect;
}

- (void)showKeyboard {
    [self.commentTextView becomeFirstResponder];
}

- (void)hideKeyboard {
    [self.view endEditing:YES];
}

#pragma mark - Helper

- (void)refreshView {
    NSMutableArray *viewControllers = [[self.navigationController viewControllers] mutableCopy];
    [viewControllers removeLastObject];
    [viewControllers addObject:[TMEPostViewController postWithId:self.post.tid]];
    [self.navigationController setViewControllers:[viewControllers copy] animated:NO];
}

@end
