//
//  UserProfileStatItemView.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 12/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MSLabel.h"

@interface UserProfileStatItemView : UIView

@property (weak, nonatomic) IBOutlet UILabel *statItemNumberLabel;
@property (weak, nonatomic) IBOutlet UIImageView *statItemIconView;
@property (weak, nonatomic) IBOutlet MSLabel *statItemTextLabel;

@end
