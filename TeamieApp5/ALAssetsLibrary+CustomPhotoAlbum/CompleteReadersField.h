//
//  CompleteReadersField.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 4/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CompleteReadersField : UIView
@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@end
