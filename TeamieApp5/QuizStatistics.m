//
//  QuizStatistics.m
//  TeamieApp5
//
//  Created by Raunak on 23/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuizStatistics.h"

@implementation QuizStatistics

@synthesize totalScore;
@synthesize maxScore;
@synthesize minScore;
@synthesize averageScore;
@synthesize totalSubmissions;

@end
