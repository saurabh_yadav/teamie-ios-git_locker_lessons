//
//  TMEUpcomingEventsViewController.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEUpcomingEventsViewController.h"

#import "TMEEventCell.h"
#import "TMEEvent.h"
#import "TMEClient.h"
#import "UIView+TMEEssentials.h"
#import "UIScrollView+SVInfiniteScrolling.h"

@interface TMEUpcomingEventsViewController()

@property (nonatomic, strong) NSArray *events;

@end

BOOL loadMore;
NSMutableArray *consolidateEvents;
UIRefreshControl *refreshControl;

@implementation TMEUpcomingEventsViewController

static NSString * const kReuseIdentifier = @"event";

#pragma mark - LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    loadMore = NO;
    consolidateEvents = [[NSMutableArray alloc] init];
    
    [self.tableView registerClass:TMEEventCell.class forCellReuseIdentifier:kReuseIdentifier];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.rowHeight = 70.0f;
    self.tableView.allowsSelection = NO;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshEvents) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    [self sendEventRequest:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.backgroundColor = [TeamieUIGlobalColors baseBgColor];
    self.navigationItem.title = TMELocalize(@"menu.calendar");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/calendar"];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return consolidateEvents.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TMEEventCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kReuseIdentifier
                                                              forIndexPath:indexPath];
    [cell loadEvent:consolidateEvents[indexPath.row]];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (loadMore) {
        if (indexPath.row == consolidateEvents.count - 1) {
            self.tableView.infiniteScrollingView.enabled = YES;
            __unsafe_unretained typeof(self) weakSelf = self;
            [weakSelf.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf sendEventRequest:YES];
            }];
        }
    }
}

#pragma mark - Private Methods

- (void)refreshEvents {
    [self sendEventRequest:NO];
}

- (void)sendEventRequest:(BOOL)withParameters {
    NSDictionary *params = [NSDictionary dictionary];
    NSString *loadingIndicator = TMELocalize(@"message.loading-events");
    if (!withParameters) {
        loadMore = YES;
        params = @{@"items_per_page": @(NUMBER_OF_CALENDAR_EVENT_ITEMS)};
        [consolidateEvents removeAllObjects];
    }
    else if (loadMore) {
        params = @{@"page": [NSNumber numberWithInteger:consolidateEvents.count/NUMBER_OF_CALENDAR_EVENT_ITEMS + 1],
                   @"items_per_page": @(NUMBER_OF_CALENDAR_EVENT_ITEMS)};
        loadingIndicator = nil;
    }
    
    [[TMEClient sharedClient]
     request:TeamieUserUpcomingEventsRequest
     parameters:nil
     loadingMessage:loadingIndicator
     completion:^{
         [self.tableView.infiniteScrollingView stopAnimating];
         [refreshControl endRefreshing];
         if (!consolidateEvents.count) {
             [self viewForEmptyListWithTitleText:nil subtitleText:nil andImage:nil];
         }
     } success:^(id response) {
         self.events = [TMEEvent parseList:response[@"calendar_items"]];
         loadMore = (self.events.count && self.events.count % NUMBER_OF_CALENDAR_EVENT_ITEMS == 0)?YES:NO;
         [consolidateEvents addObjectsFromArray:self.events];
         [self.tableView reloadData];
     } failure:nil];
}

@end
