//
//  TMEPostCreateAudioAttachmentViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 25/03/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEPostCreateAudioAttachmentViewController.h"
#import "TMEVoiceRecording.h"
#import "UIView+TMEEssentials.h"

#import <Masonry/Masonry.h>
#import <MZTimerLabel/MZTimerLabel.h>

#pragma mark - Globals
#define IMAGE_MIC_NORMAL    [UIImage imageNamed:@"record_0.png"]        // White Mic
#define IMAGE_MIC_WAVE      [UIImage imageNamed:@"record_1.png"]        // Red Mic

#pragma mark UIImageGrayscale

@interface UIImage (Grayscale)

/**
 *  This method is used to display a partial image with Progress
 *  http://stackoverflow.com/questions/1298867/convert-image-to-grayscale
 */
- (UIImage *)partialImageWithPercentage:(float)percentage
                               vertical:(BOOL)vertical
                          grayscaleRest:(BOOL)grayscaleRest;

@end

@implementation UIImage (Grayscale)

- (UIImage *)partialImageWithPercentage:(float)percentage vertical:(BOOL)vertical grayscaleRest:(BOOL)grayscaleRest {
    const int ALPHA = 0;
    const int RED = 1;
    const int GREEN = 2;
    const int BLUE = 3;
    
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, self.size.width * self.scale, self.size.height * self.scale);
    
    int width = imageRect.size.width;
    int height = imageRect.size.height;
    
    // the pixels will be painted to this array
    uint32_t *pixels = (uint32_t *) malloc(width * height * sizeof(uint32_t));
    
    // clear the pixels so any transparency is preserved
    memset(pixels, 0, width * height * sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    // create a context with RGBA pixels
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width * sizeof(uint32_t), colorSpace,
                                                 kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    
    // paint the bitmap to our context which will fill in the pixels array
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), [self CGImage]);
    
    int x_origin = vertical ? 0 : width * percentage;
    int y_to = vertical ? height * (1.f -percentage) : height;
    
    for(int y = 0; y < y_to; y++) {
        for(int x = x_origin; x < width; x++) {
            uint8_t *rgbaPixel = (uint8_t *) &pixels[y * width + x];
            
            if (grayscaleRest) {
                // convert to grayscale using recommended method: http://en.wikipedia.org/wiki/Grayscale#Converting_color_to_grayscale
                uint32_t gray = 0.3 * rgbaPixel[RED] + 0.59 * rgbaPixel[GREEN] + 0.11 * rgbaPixel[BLUE];
                
                // set the pixels to gray
                rgbaPixel[RED] = gray;
                rgbaPixel[GREEN] = gray;
                rgbaPixel[BLUE] = gray;
            }
            else {
                rgbaPixel[ALPHA] = 0;
                rgbaPixel[RED] = 0;
                rgbaPixel[GREEN] = 0;
                rgbaPixel[BLUE] = 0;
            }
        }
    }
    
    // create a new CGImageRef from our context with the modified pixels
    CGImageRef image = CGBitmapContextCreateImage(context);
    
    // we're done with the context, color space, and pixels
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    free(pixels);
    
    // make a new UIImage to return
    UIImage *resultUIImage = [UIImage imageWithCGImage:image
                                                 scale:self.scale
                                           orientation:UIImageOrientationUp];
    
    // we're done with image now too
    CGImageRelease(image);
    
    return resultUIImage;
}

@end

#pragma mark TMEProgressImageView

/**
 *  Class to handle the vertical progressive mic animation
 */
@interface TMEProgressImageView : UIImageView {
    UIImage * _originalImage;
    BOOL      _internalUpdating;
}

@property (nonatomic) float progress;
@property (nonatomic) BOOL  hasGrayscaleBackground;

@property (nonatomic, getter = isVerticalProgress) BOOL verticalProgress;

@end

@interface TMEProgressImageView ()

@property(nonatomic,retain) UIImage * originalImage;

- (void)commonInit;
- (void)updateDrawing;

@end

@implementation TMEProgressImageView

@synthesize progress               = _progress;
@synthesize hasGrayscaleBackground = _hasGrayscaleBackground;
@synthesize verticalProgress       = _verticalProgress;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    return self;
}

- (void)commonInit {
    _progress = 0.f;
    _hasGrayscaleBackground = YES;
    _verticalProgress = YES;
    _originalImage = self.image;
}

#pragma mark - Custom Accessor

- (void)setImage:(UIImage *)image {
    [super setImage:image];
    
    if (!_internalUpdating) {
        self.originalImage = image;
        [self updateDrawing];
    }
    
    _internalUpdating = NO;
}

- (void)setProgress:(float)progress {
    _progress = MIN(MAX(0.f, progress * 10.f), 1.f);
    [self updateDrawing];
}

- (void)setHasGrayscaleBackground:(BOOL)hasGrayscaleBackground {
    _hasGrayscaleBackground = hasGrayscaleBackground;
    [self updateDrawing];
}

- (void)setVerticalProgress:(BOOL)verticalProgress {
    _verticalProgress = verticalProgress;
    [self updateDrawing];
}

#pragma mark - drawing

- (void)updateDrawing {
    _internalUpdating = YES;
    // Update the image to show a partial image which is dependant on the progress value
    self.image = [_originalImage partialImageWithPercentage:_progress vertical:_verticalProgress grayscaleRest:_hasGrayscaleBackground];
}

@end

@interface TMEPostCreateAudioAttachmentViewController () <MZTimerLabelDelegate>

@property (nonatomic, strong) UIButton             * backButton;
@property (nonatomic, strong) MZTimerLabel         * timerLabel;
@property (nonatomic, strong) UIImageView          * micNormalImageView;
@property (nonatomic, strong) TMEProgressImageView * dynamicProgress;
@property (nonatomic, strong) UIButton             * recordButton;
@property (nonatomic, strong) TMEVoiceRecording    * voice;                 // Voice Object
@property (nonatomic        ) BOOL                 shouldRecord;

@end

@implementation TMEPostCreateAudioAttachmentViewController

#pragma mark - Lifecycle

- (instancetype)init {
    self = [super init];
    
    if (self) {
        self.view.backgroundColor = UIColor.whiteColor;
        self.voice = [TMEVoiceRecording new];
        self.progress = 0.f;
        self.shouldRecord = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createMainUI];
    
    // Listening to notification. This fires when application goes in the background. When the app goes in the background or off screen, the recording ends and attaches to the post
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(recordEnd) name:@"stopAudioRecording" object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"stopAudioRecording" object:nil];
}

- (void)createMainUI {
    // Back Button
    self.backButton = [self.view createAndAddSubView:UIButton.class];
    [self.backButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"close" withSize:25.f] forState:UIControlStateNormal];
    [self.backButton addTarget:self action:@selector(dismissViewController) forControlEvents:UIControlEventTouchUpInside];
    
    // Timer Label
    self.timerLabel = [MZTimerLabel new];
    self.timerLabel.timerType = MZTimerLabelTypeStopWatch;
    self.timerLabel.textColor = [TeamieUIGlobalColors grayColor];
    self.timerLabel.font = [UIFont fontWithName:@"OpenSans" size:65.f];
    self.timerLabel.timeFormat = @"mm:ss";
    self.timerLabel.text = @"00:00";
    self.timerLabel.textAlignment = NSTextAlignmentCenter;
    self.timerLabel.delegate = self;
    [self.view addSubview:self.timerLabel];
    
    // Microphone Image: Grey
    self.micNormalImageView = [UIImageView new];
    self.micNormalImageView.image = IMAGE_MIC_NORMAL;
    [self.view addSubview:self.micNormalImageView];
    
    // Overlay Microphone Image: Red
    self.dynamicProgress = [TMEProgressImageView new];
    self.dynamicProgress.image = IMAGE_MIC_WAVE;
    self.dynamicProgress.progress = 0;
    self.dynamicProgress.hasGrayscaleBackground = NO;
    self.dynamicProgress.verticalProgress = YES;
    [self.view addSubview:self.dynamicProgress];
    
    // Circular Record Button
    self.recordButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.recordButton.backgroundColor = UIColor.redColor;
    self.recordButton.layer.cornerRadius = 50.f;
    self.recordButton.titleLabel.font = [UIFont fontWithName:@"OpenSans" size:20.f];
    [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    [self.recordButton setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [self.recordButton addTarget:self action:@selector(recordToggle) forControlEvents:UIControlEventTouchUpInside];
    [self.recordButton addTarget:self action:@selector(recordCancel) forControlEvents:UIControlEventTouchUpOutside];
    [self.view addSubview:self.recordButton];
    
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.height.equalTo(@25);
        make.left.top.equalTo(@10);
    }];
    [self.timerLabel mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(@170);
        make.height.equalTo(@80);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).with.offset(35.f);
    }];
    [self.micNormalImageView mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(@102);
        make.height.equalTo(@148);
        make.center.equalTo(self.view);
    }];
    [self.dynamicProgress mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(@102);
        make.height.equalTo(@148);
        make.center.equalTo(self.view);
    }];
    [self.recordButton mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.height.equalTo(@100);
        make.centerX.equalTo(self.view);
        make.bottom.equalTo(self.view.mas_bottom).with.offset(-35.f);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Custom Accessors

- (void)setProgress:(CGFloat)progress {
    if (self.dynamicProgress) {
        self.dynamicProgress.progress = progress;
    }
}

- (CGFloat)progress {
    if (self.dynamicProgress)
        return _dynamicProgress.progress;
    
    return 0.f;
}

#pragma mark - Private Methods

- (void)recordToggle {
    if (_shouldRecord) {
        [self recordStart];
        _shouldRecord = NO;
    }
    else {
        [self recordEnd];
        _shouldRecord = YES;
    }
}

- (void)recordCancel {
    [self.timerLabel reset];
    [self.voice cancelled];
    self.recordButton.layer.cornerRadius = 50.f;
    
    UIAlertView * alert =
    [[UIAlertView alloc] initWithTitle:@"Recording Cancelled"
                               message:nil
                              delegate:nil
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil, nil];
    [alert show];
}

- (void)recordStart {
    [self.recordButton setTitle:@"Stop" forState:UIControlStateNormal];
    self.recordButton.layer.cornerRadius = 5.f;
    [self.timerLabel start];
    
    // TODO: Decide on Filename
    // Start to record in the App's Home Directory with name "MySound.m4a"
    // Delegate sends the parent object to the child
    [self.voice startRecordWithPath:[NSString stringWithFormat:@"%@/Documents/AudioRecording-%f.m4a", NSHomeDirectory(), [NSDate timeIntervalSinceReferenceDate]] delegate:self];
}

- (void)recordEnd {
    [self.recordButton setTitle:@"Record" forState:UIControlStateNormal];
    self.recordButton.layer.cornerRadius = 50.f;
    [self.timerLabel pause];
    
    // Stop to record, dismiss the view controller and attach the file to 'Create Post' view
    [self.voice stopRecordWithCompletionBlock:^{
        if (self.timerLabel.getTimeCounted > 0.0f) {
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"file://%@", self.voice.recordPath]];
            [self.delegate didCompleteRecordingAudioWithFilePath:url andAudioLength:self.timerLabel.getTimeCounted];
            [self dismissViewController];
        }
    }];
}

- (void)dismissViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - MZTimerLabelDelegate

- (void)timerLabel:(MZTimerLabel *)timerLabel countingTo:(NSTimeInterval)time timertype:(MZTimerLabelType)timerType {
    // Limit the recording time to 2 hours
    if (time >= 7200.0) {
        [self recordEnd];
    }
}

@end