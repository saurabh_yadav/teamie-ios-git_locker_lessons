//
//  TMEPostAttachmentView.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TMEPostAttachment.h"

@protocol TMEPostAttachmentViewDelegate <NSObject>

- (void)trackAttachmentClick:(NSString *)attachmentType;

@end

@interface TMEPostAttachmentView : UIView

@property (nonatomic, weak) UILabel *mainTextLabel;
@property (nonatomic, weak) UILabel *subTextLabel;
@property (nonatomic, weak) UIImageView *previewImageView;

@property (nonatomic, strong) NSURL *href;

- (instancetype)initWithDelegate:(UIView<TMEPostAttachmentViewDelegate> *)delegate
                      attachment:(TMEPostAttachment *)attachment;

@end 
