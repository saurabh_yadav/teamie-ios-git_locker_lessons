//
//  TMELessonPageShort.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEModel.h"
#import "TMEUser.h"

@interface TMELessonPageShort : TMEModel

@property (nonatomic, strong) NSNumber *nid;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *status;
@property (nonatomic, strong) TMEUser *author;
@property (nonatomic, strong) NSArray *actions;
@property (nonatomic, strong) NSString *is_Read;
@end
