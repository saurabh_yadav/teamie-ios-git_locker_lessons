//
//  AssesmentCollectionViewCell.h
//  CollectionView
//
//  Created by Anuj Rajput on 15/10/14.
//
//

#import <UIKit/UIKit.h>
#import "TMEQuizSimple.h"

/**
 *  Subclass of UICollectionViewCell to define a custom view for Assessment Listing View
 */
@interface AssessmentCollectionViewCell : UICollectionViewCell

/**
 *  Set Assessment Collection Cell View Data
 */
- (void)setCellData:(TMEQuizSimple *)quiz withClassroomId:(NSNumber *)nid;

@end
