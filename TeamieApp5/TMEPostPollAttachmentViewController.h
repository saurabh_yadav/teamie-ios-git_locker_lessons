//
//  TMEPostPollAttachmentViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 02/01/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEPollAttachment.h"

@protocol TMEPollAttachmentDelegate

- (void)updatePollAttachmentViewHeightWithPoll:(TMEPollAttachment *)poll;

@end

@interface TMEPostPollAttachmentViewController : UITableViewController

@property (nonatomic, strong) NSObject <TMEPollAttachmentDelegate> *delegate;

- (instancetype)initWithPoll:(TMEPollAttachment *)poll postID:(NSNumber *)tid;

@end
