//
//  DrawScribbleViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 13/02/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "DrawScribbleViewController.h"

@implementation DrawScribbleViewController

@synthesize clearScreenButton;
@synthesize undoLastButton;
@synthesize redoLastButton;
@synthesize colorPickerButton;
@synthesize eraserButton;
@synthesize attachToThoughtButton;
@synthesize loadImageButton;
@synthesize drawingView;
@synthesize curColor;
@synthesize delegate;
@synthesize wePopoverController;
@synthesize popoverController;
@synthesize myDrawing;

- (void)viewDidLoad {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        popoverClass = [UIPopoverController class];
    }
    else {
        popoverClass = [WEPopoverController class];
    }
    
    [self.view addSubview:self.myDrawing];       // add this view to the view controller
    self.curColor = [UIColor blackColor];   // set initial brush color to black
    [self becomeFirstResponder];
}

- (void)viewDidUnload {
    [self setClearScreenButton:nil];
    [self setUndoLastButton:nil];
    [self setRedoLastButton:nil];
    [self setColorPickerButton:nil];
    [self setAttachToThoughtButton:nil];
    [self setDrawingView:nil];
    [self setUndoLastButton:nil];
    [self setLoadImageButton:nil];
    [super viewDidUnload];
}

- (void)applyPickedColor:(InfColorPickerController*) picker {

    float red, green, blue, alpha;
    self.curColor = picker.resultColor;
    [self.curColor getRed:&red green:&green blue:&blue alpha:&alpha];
    
    [self.myDrawing setColor:red g:green b:blue a:alpha];
}


#pragma mark UIPopoverControllerDelegate methods

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popVC {
    if ([popVC.contentViewController isKindOfClass:[InfColorPickerController class]]) {
        InfColorPickerController* picker = (InfColorPickerController*) popVC.contentViewController;
        [self applyPickedColor:picker];
    }
    
    if (popVC == self.wePopoverController) {
        self.wePopoverController = nil;
    }
}

- (BOOL)dismissActivePopover {
    if (self.wePopoverController) {
        [self.wePopoverController dismissPopoverAnimated:YES];
        [self popoverControllerDidDismissPopover:self.wePopoverController];
        
        return YES;
    }
    
    return NO;
}

- (void)showPopover:(WEPopoverController*) popover from:(id) sender {
    popover.delegate = self;
    
//    if ([sender isKindOfClass:[UIBarButtonItem class]]) {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        CGRect btnRect = [((UIView*)sender) convertRect:((UIView*)sender).frame toView:self.view];
        
        [popover presentPopoverFromRect:btnRect inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else {
        [popover presentPopoverFromBarButtonItem:sender
                        permittedArrowDirections:UIPopoverArrowDirectionAny
                                        animated:YES];
    }
}

#pragma mark InfHSBColorPickerControllerDelegate methods

- (void)colorPickerControllerDidChangeColor:(InfColorPickerController *)picker {
    [self applyPickedColor:picker];
}

- (void)colorPickerControllerDidFinish:(InfColorPickerController *)picker {
    [self applyPickedColor:picker];
    [wePopoverController dismissPopoverAnimated:YES];
}

#pragma mark Button Click Event

- (IBAction)undoLastAction:(id)sender {
    
    [self.myDrawing undoButtonClicked];
}

- (IBAction)redoLastAction:(id)sender {

    [self.myDrawing redoButtonClicked];
}

- (IBAction)clearScreenAction:(id)sender {
    [TeamieUIGlobals showAlertWithTitle:@"Delete Drawing?" message:@"Are you sure you want to delete the drawing you've made so far?" delegate:self tag:1 cancelButton:@"No" otherTitles:@"Yes", nil];
}

- (IBAction)eraserAction:(id)sender {

    int drawStep = [self.myDrawing eraserButtonClicked];
    if (drawStep == DRAW) {
        [self eraserButtonToggle];
    }
    else {
        [self penButtonToggle];
    }
}

- (IBAction)colorPickerAction:(id)sender {
    
    if ([self dismissActivePopover]) {  // If the popover is already visible, then dismiss it buddy!
        return;
    }
    
    InfColorPickerController* picker = [InfColorPickerController colorPickerViewController];
    
    picker.sourceColor = self.curColor;
    picker.delegate = self;
    
    self.wePopoverController = [[popoverClass alloc] initWithContentViewController:picker];
    [self showPopover:self.wePopoverController from:sender];
}

- (IBAction)attachToThoughtAction:(id)sender {
    
    [self.myDrawing save2AlbumButtonClicked];
    UIImage* attachedImage = [self.myDrawing attachImageToThought];
    
    if ([self.delegate respondsToSelector:@selector(setImageData:)])
    {
        [self.delegate performSelector:@selector(setImageData:) withObject:attachedImage];
    }
    [self dismissModalViewControllerAnimated:YES];
}

- (IBAction)cancelAction:(id)sender {
    // Show an alert view confirming with the user if he's sure to leave
    [TeamieUIGlobals showAlertWithTitle:@"Cancel?" message:@"Are you sure you want to cancel? You will lose the drawing you've made!" delegate:self tag:2 cancelButton:@"No" otherTitles:@"Yes", nil];
}

- (IBAction)loadImageAction:(id)sender {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:TeamieLocalizedString(@"ATTACH_IMAGE_OPTIONS", nil) delegate:self cancelButtonTitle:TeamieLocalizedString(@"BTN_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:TeamieLocalizedString(@"BTN_IMAGE_FROM_GALLERY", nil), TeamieLocalizedString(@"BTN_NEW_IMAGE", nil), nil];
    [actionSheet showInView:self.view];
}

- (void)eraserButtonToggle {
    [eraserButton setImage:[UIImage imageNamed:@"icon-draw-eraser.png"]];
}

- (void)penButtonToggle {
    [eraserButton setImage:[UIImage imageNamed:@"icon-drawscribble.png"]];
}

#pragma mark toolbarDelegate

- (void) setUndoButtonEnable:(NSNumber*)isEnable {

    [self.undoLastButton setEnabled:[isEnable boolValue]];
}

- (void)setRedoButtonEnable:(NSNumber*)isEnable {
    
    [self.redoLastButton setEnabled:[isEnable boolValue]];
}
- (void) setClearButtonEnable:(NSNumber*)isEnable {
    
    [self.clearScreenButton setEnabled:[isEnable boolValue]];
}

- (void) setEraserButtonEnable:(NSNumber*)isEnable {
    
    [eraserButton setEnabled:[isEnable boolValue]];
}

- (void) setAttachToThoughtEnable:(NSNumber*)isEnable {

    [attachToThoughtButton setEnabled:[isEnable boolValue]];
}

- (void) setLoadImageButtonEnable:(NSNumber*)isEnable {
    
    [loadImageButton setEnabled:[isEnable boolValue]];
}

#pragma mark Device Orientation Delegate

// shouldAutorotateToInterfaceOrientation depreciated in iOS 6
- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;  // Disable Landscape Orientation TODO: Sometime later, maybe
}

#pragma mark Hidden Easter Egg

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    if (event.subtype == UIEventSubtypeMotionShake) {
        [self drawRect:drawingView.frame];
    }
}

- (void)drawRect:(CGRect)rect {
    
    //// Color Declarations
    UIColor* black = [UIColor colorWithRed: 0.112 green: 0.1 blue: 0.101 alpha: 1];
    UIColor* red = [UIColor colorWithRed: 0.682 green: 0.061 blue: 0.14 alpha: 1];
    
    //// Teamie Logo
    
    //// Bezier 1 Drawing
    UIBezierPath* bezier1Path = [UIBezierPath bezierPath];
    [bezier1Path moveToPoint: CGPointMake(162.14, 218.18)];
    [bezier1Path addCurveToPoint: CGPointMake(159.94, 217.11) controlPoint1: CGPointMake(161.52, 217.59) controlPoint2: CGPointMake(160.63, 217.58)];
    [bezier1Path addCurveToPoint: CGPointMake(154.95, 208.83) controlPoint1: CGPointMake(156.98, 215.12) controlPoint2: CGPointMake(155.66, 212.16)];
    [bezier1Path addCurveToPoint: CGPointMake(154.65, 205.5) controlPoint1: CGPointMake(154.71, 207.73) controlPoint2: CGPointMake(154.65, 206.62)];
    [bezier1Path addCurveToPoint: CGPointMake(154.77, 186.2) controlPoint1: CGPointMake(154.67, 199.06) controlPoint2: CGPointMake(154.36, 192.61)];
    [bezier1Path addCurveToPoint: CGPointMake(172.53, 160.29) controlPoint1: CGPointMake(155.53, 174.14) controlPoint2: CGPointMake(161.85, 165.66)];
    [bezier1Path addCurveToPoint: CGPointMake(198.6, 165) controlPoint1: CGPointMake(182.05, 157.28) controlPoint2: CGPointMake(190.86, 158.04)];
    [bezier1Path addCurveToPoint: CGPointMake(208.98, 185.27) controlPoint1: CGPointMake(204.65, 170.45) controlPoint2: CGPointMake(208.19, 177.15)];
    [bezier1Path addCurveToPoint: CGPointMake(209.18, 185.78) controlPoint1: CGPointMake(209, 185.46) controlPoint2: CGPointMake(208.94, 185.68)];
    [bezier1Path addCurveToPoint: CGPointMake(209.18, 207.86) controlPoint1: CGPointMake(209.18, 193.14) controlPoint2: CGPointMake(209.18, 200.5)];
    [bezier1Path addCurveToPoint: CGPointMake(208.79, 208.9) controlPoint1: CGPointMake(208.73, 208.08) controlPoint2: CGPointMake(208.87, 208.56)];
    [bezier1Path addCurveToPoint: CGPointMake(201.26, 218.18) controlPoint1: CGPointMake(207.83, 213.26) controlPoint2: CGPointMake(205.89, 216.81)];
    [bezier1Path addCurveToPoint: CGPointMake(199.58, 218.18) controlPoint1: CGPointMake(200.7, 218.18) controlPoint2: CGPointMake(200.14, 218.18)];
    [bezier1Path addCurveToPoint: CGPointMake(195.82, 216.03) controlPoint1: CGPointMake(198.27, 217.56) controlPoint2: CGPointMake(196.88, 217.12)];
    [bezier1Path addCurveToPoint: CGPointMake(191.92, 205.9) controlPoint1: CGPointMake(193.07, 213.2) controlPoint2: CGPointMake(191.97, 209.72)];
    [bezier1Path addCurveToPoint: CGPointMake(191.91, 188.28) controlPoint1: CGPointMake(191.84, 200.03) controlPoint2: CGPointMake(191.87, 194.15)];
    [bezier1Path addCurveToPoint: CGPointMake(186.11, 177.75) controlPoint1: CGPointMake(191.94, 183.67) controlPoint2: CGPointMake(189.77, 180.29)];
    [bezier1Path addCurveToPoint: CGPointMake(181.24, 176.57) controlPoint1: CGPointMake(184.68, 176.75) controlPoint2: CGPointMake(183.03, 176.26)];
    [bezier1Path addCurveToPoint: CGPointMake(171.88, 188.04) controlPoint1: CGPointMake(176.15, 177.43) controlPoint2: CGPointMake(171.89, 182.63)];
    [bezier1Path addCurveToPoint: CGPointMake(171.87, 205.54) controlPoint1: CGPointMake(171.86, 193.87) controlPoint2: CGPointMake(171.89, 199.71)];
    [bezier1Path addCurveToPoint: CGPointMake(163.82, 218.18) controlPoint1: CGPointMake(171.84, 212.01) controlPoint2: CGPointMake(169.3, 215.97)];
    [bezier1Path addCurveToPoint: CGPointMake(162.14, 218.18) controlPoint1: CGPointMake(163.26, 218.18) controlPoint2: CGPointMake(162.7, 218.18)];
    [bezier1Path closePath];
    bezier1Path.miterLimit = 4;
    
    bezier1Path.usesEvenOddFillRule = YES;
    
    [red setFill];
    [bezier1Path stroke];
    [bezier1Path fill];
        
    //// Bezier 2 Drawing
    UIBezierPath* bezier2Path = [UIBezierPath bezierPath];
    [bezier2Path moveToPoint: CGPointMake(116.3, 184.58)];
    [bezier2Path addCurveToPoint: CGPointMake(117.23, 180.59) controlPoint1: CGPointMake(116.88, 183.31) controlPoint2: CGPointMake(116.87, 181.91)];
    [bezier2Path addCurveToPoint: CGPointMake(135.47, 159.75) controlPoint1: CGPointMake(119.93, 170.67) controlPoint2: CGPointMake(126.06, 163.76)];
    [bezier2Path addCurveToPoint: CGPointMake(144.8, 158.7) controlPoint1: CGPointMake(138.45, 158.48) controlPoint2: CGPointMake(141.63, 158.56)];
    [bezier2Path addCurveToPoint: CGPointMake(161.01, 165.74) controlPoint1: CGPointMake(151.1, 158.98) controlPoint2: CGPointMake(156.47, 161.37)];
    [bezier2Path addCurveToPoint: CGPointMake(161.17, 167.25) controlPoint1: CGPointMake(161.59, 166.3) controlPoint2: CGPointMake(161.7, 166.57)];
    [bezier2Path addCurveToPoint: CGPointMake(153.37, 183.64) controlPoint1: CGPointMake(157.34, 172.12) controlPoint2: CGPointMake(154.65, 177.55)];
    [bezier2Path addCurveToPoint: CGPointMake(152.91, 185.43) controlPoint1: CGPointMake(153.25, 184.17) controlPoint2: CGPointMake(153.1, 184.7)];
    [bezier2Path addCurveToPoint: CGPointMake(147.09, 177.58) controlPoint1: CGPointMake(152.05, 181.86) controlPoint2: CGPointMake(150.13, 179.29)];
    [bezier2Path addCurveToPoint: CGPointMake(138.6, 178.24) controlPoint1: CGPointMake(144.11, 175.9) controlPoint2: CGPointMake(141.27, 176.16)];
    [bezier2Path addCurveToPoint: CGPointMake(133.52, 188.44) controlPoint1: CGPointMake(135.3, 180.82) controlPoint2: CGPointMake(133.47, 184.13)];
    [bezier2Path addCurveToPoint: CGPointMake(133.54, 205.84) controlPoint1: CGPointMake(133.59, 194.24) controlPoint2: CGPointMake(133.55, 200.04)];
    [bezier2Path addCurveToPoint: CGPointMake(131.88, 212.89) controlPoint1: CGPointMake(133.53, 208.31) controlPoint2: CGPointMake(132.95, 210.67)];
    [bezier2Path addCurveToPoint: CGPointMake(125.66, 218.18) controlPoint1: CGPointMake(130.58, 215.57) controlPoint2: CGPointMake(128.56, 217.39)];
    [bezier2Path addCurveToPoint: CGPointMake(123.98, 218.18) controlPoint1: CGPointMake(125.1, 218.18) controlPoint2: CGPointMake(124.54, 218.18)];
    [bezier2Path addCurveToPoint: CGPointMake(121.35, 216.94) controlPoint1: CGPointMake(123.16, 217.64) controlPoint2: CGPointMake(122.16, 217.52)];
    [bezier2Path addCurveToPoint: CGPointMake(117, 210.42) controlPoint1: CGPointMake(119.07, 215.3) controlPoint2: CGPointMake(117.84, 213)];
    [bezier2Path addCurveToPoint: CGPointMake(116.3, 208.1) controlPoint1: CGPointMake(116.75, 209.65) controlPoint2: CGPointMake(116.85, 208.77)];
    [bezier2Path addCurveToPoint: CGPointMake(116.3, 184.58) controlPoint1: CGPointMake(116.3, 200.26) controlPoint2: CGPointMake(116.3, 192.42)];
    [bezier2Path closePath];
    bezier2Path.miterLimit = 4;
    
    bezier2Path.usesEvenOddFillRule = YES;
    
    [red setFill];
    [bezier2Path stroke];
    [bezier2Path fill];
    
    
    //// Bezier 3 Drawing
    UIBezierPath* bezier3Path = [UIBezierPath bezierPath];
    [bezier3Path moveToPoint: CGPointMake(184.7, 137.3)];
    [bezier3Path addCurveToPoint: CGPointMake(190.61, 141.22) controlPoint1: CGPointMake(187.05, 138.03) controlPoint2: CGPointMake(189.15, 139.13)];
    [bezier3Path addCurveToPoint: CGPointMake(190.29, 152.57) controlPoint1: CGPointMake(193.04, 144.71) controlPoint2: CGPointMake(192.93, 149.21)];
    [bezier3Path addCurveToPoint: CGPointMake(179.54, 155.54) controlPoint1: CGPointMake(187.77, 155.78) controlPoint2: CGPointMake(183.39, 157)];
    [bezier3Path addCurveToPoint: CGPointMake(173.45, 145.83) controlPoint1: CGPointMake(175.56, 154.03) controlPoint2: CGPointMake(173.12, 150.15)];
    [bezier3Path addCurveToPoint: CGPointMake(180.84, 137.5) controlPoint1: CGPointMake(173.76, 141.82) controlPoint2: CGPointMake(176.75, 138.45)];
    [bezier3Path addCurveToPoint: CGPointMake(181.1, 137.3) controlPoint1: CGPointMake(180.94, 137.48) controlPoint2: CGPointMake(181.01, 137.37)];
    [bezier3Path addCurveToPoint: CGPointMake(184.7, 137.3) controlPoint1: CGPointMake(182.3, 137.3) controlPoint2: CGPointMake(183.5, 137.3)];
    [bezier3Path closePath];
    bezier3Path.miterLimit = 4;
    
    bezier3Path.usesEvenOddFillRule = YES;
    
    [black setFill];
    [bezier3Path stroke];
    [bezier3Path fill];
    
    
    //// Bezier 4 Drawing
    UIBezierPath* bezier4Path = [UIBezierPath bezierPath];
    [bezier4Path moveToPoint: CGPointMake(142.7, 137.3)];
    [bezier4Path addCurveToPoint: CGPointMake(148.79, 141.38) controlPoint1: CGPointMake(145.17, 138.01) controlPoint2: CGPointMake(147.29, 139.2)];
    [bezier4Path addCurveToPoint: CGPointMake(142.32, 156.05) controlPoint1: CGPointMake(152.7, 147.08) controlPoint2: CGPointMake(149.08, 155.19)];
    [bezier4Path addCurveToPoint: CGPointMake(131.68, 148.58) controlPoint1: CGPointMake(136.85, 156.74) controlPoint2: CGPointMake(132.55, 153.02)];
    [bezier4Path addCurveToPoint: CGPointMake(139.17, 137.45) controlPoint1: CGPointMake(130.53, 142.69) controlPoint2: CGPointMake(134.87, 138)];
    [bezier4Path addCurveToPoint: CGPointMake(139.34, 137.3) controlPoint1: CGPointMake(139.23, 137.44) controlPoint2: CGPointMake(139.28, 137.35)];
    [bezier4Path addCurveToPoint: CGPointMake(142.7, 137.3) controlPoint1: CGPointMake(140.46, 137.3) controlPoint2: CGPointMake(141.58, 137.3)];
    [bezier4Path closePath];
    bezier4Path.miterLimit = 4;
    
    bezier4Path.usesEvenOddFillRule = YES;
    
    [black setFill];
    [bezier4Path stroke];
    [bezier4Path fill];
    
    [self.view setNeedsDisplay];
}

#pragma mark - Alert View Delegate Method

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 1) {   // Clear the drawing button alert
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:0 animated:YES];
        }
        else if (buttonIndex == 1) {
            // Clear the beautiful drawing made by the user
            [self.myDrawing clearButtonClicked];
            [self.myDrawing removeFromSuperview];
            self.myDrawing = nil;
            [self.view addSubview:self.myDrawing];
        }
    }
    else if (alertView.tag == 2) {  // Cancel the drawing button alert
        if (buttonIndex == 0) {
            [alertView dismissWithClickedButtonIndex:0 animated:YES];
        }
        else {
            // Dismiss the Draw Scribble view controller
            [self dismissModalViewControllerAnimated:YES];
        }
    }
}

#pragma mark - Action Sheet Delegate Method

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)index {
    if (index != actionSheet.cancelButtonIndex) { // user did not press Cancel
        // Create image picker controller
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
        
        switch (index) {
            case 0: {
#ifdef DEBUG
                NSLog(@"Open image gallery...");
#endif
                // Set source to the photo library
                imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
            }
            break;
            case 1: {
#ifdef DEBUG
                NSLog(@"Open camera for taking a pic...");
#endif
                
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO) {
#ifdef DEBUG
                    NSLog(@"Sorry! Camera is not available on this device!");
#endif
                    return;
                }
                
                // Set source to the camera of the device
                imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                imagePicker.allowsEditing = YES;
            }
            break;
            default:
#ifdef DEBUG
                NSLog(@"Action sheet button index not implemented.");
#endif
            return;
            break;
        }
        
        // Delegate is self
        if (index == 0 || index == 1) {
            
            imagePicker.delegate = self;
            
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                // Show image picker
                [self presentModalViewController:imagePicker animated:YES];
            }
            else {
                // dismiss and dealloc a popover controller if it's already there!
                [self.wePopoverController dismissPopoverAnimated:YES];
                self.wePopoverController = nil;
                
                self.wePopoverController = [[popoverClass alloc] initWithContentViewController:imagePicker];
                self.wePopoverController.delegate = self;
                [self showPopover:self.wePopoverController from:self.loadImageButton];
            }
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // dismiss the popovers no matter what
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.wePopoverController dismissPopoverAnimated:YES];
        self.wePopoverController = nil;
    }
    else {
        [self dismissModalViewControllerAnimated:YES];
    }
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera || picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        // Access the uncropped image from info dictionary
        UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        if (image) {   // if the image was captured using the camera
            // apply some rotate transformations so that the image appears properly on the web
            image = [self scaleAndRotateImage:image];
            // save the cropped & scaled image to the Camera Roll or user device album
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        }
        else {
            image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }
        
        UIGraphicsBeginImageContext(self.myDrawing.frame.size);
        [image drawInRect:self.myDrawing.bounds];
        UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        self.myDrawing.backgroundColor = [UIColor colorWithPatternImage:newImage];
    }
    else {
#ifdef DEBUG
        NSLog(@"The Image picker sourceType was not camera or photo library. Aborting...");
#endif
    }
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    // dismiss the popovers no matter what
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.wePopoverController dismissPopoverAnimated:YES];
        self.wePopoverController = nil;
    }
    else {
        [self dismissModalViewControllerAnimated:YES];
    }
}

// Method to scale and rotate the image so that it appears properly on the web
// Code from: http://discussions.apple.com/thread.jspa?messageID=7949889
- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

#pragma mark - Lazy Instantiation Methods

- (SmoothLineView*)myDrawing {
    if (!myDrawing) {
        myDrawing = [[SmoothLineView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y + 44.0, self.view.bounds.size.width, self.view.bounds.size.height - 88.0)];
        myDrawing.delegate = self;
    }
    return myDrawing;
}



@end
