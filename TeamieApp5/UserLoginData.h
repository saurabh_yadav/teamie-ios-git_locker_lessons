//
//  UserLoginData.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/27/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class TMENotification, TMEClassroom, UserMenu, UserProfile;

@interface UserLoginData : NSManagedObject

@property (nonatomic, retain) NSNumber * isLoggedIn;
@property (nonatomic, retain) NSNumber * lastLoginTime;
@property (nonatomic, retain) NSString * sessid;
@property (nonatomic, retain) NSString * session_name;
@property (nonatomic, retain) NSNumber * uid;
@property (nonatomic, retain) NSString * baseURL;
@property (nonatomic, retain) NSSet *userBulletin;
@property (nonatomic, retain) NSSet *userClassroom;
@property (nonatomic, retain) NSSet *userMenu;
@property (nonatomic, retain) UserProfile *userProfile;
@end

@interface UserLoginData (CoreDataGeneratedAccessors)

- (void)addUserBulletinObject:(TMENotification *)value;
- (void)removeUserBulletinObject:(TMENotification *)value;
- (void)addUserBulletin:(NSSet *)values;
- (void)removeUserBulletin:(NSSet *)values;

- (void)addUserClassroomObject:(TMEClassroom *)value;
- (void)removeUserClassroomObject:(TMEClassroom *)value;
- (void)addUserClassroom:(NSSet *)values;
- (void)removeUserClassroom:(NSSet *)values;

- (void)addUserMenuObject:(UserMenu *)value;
- (void)removeUserMenuObject:(UserMenu *)value;
- (void)addUserMenu:(NSSet *)values;
- (void)removeUserMenu:(NSSet *)values;

@end
