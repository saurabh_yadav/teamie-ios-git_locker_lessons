//
//  StatViewCell.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 9/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StatViewItem.h"

@interface StatViewCell : UITableViewCell

- (void)setCellData:(StatViewItem*)cellData;

@end
