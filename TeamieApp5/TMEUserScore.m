//
//  UserScore.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TMEUserScore.h"

@implementation TMEUserScore

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"finalScore": @"final_score.score",
        @"grade": @"final_score.grade",
        @"isScoreSet": @"final_score.score_set",
        @"quizScores": @"scores"
    };
};

+ (NSValueTransformer *)finalScoreJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)userJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEUser.class];
}

@end
