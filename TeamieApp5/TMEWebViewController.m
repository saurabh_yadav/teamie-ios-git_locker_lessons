//
//  TMEWebViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 05/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEWebViewController.h"
#import "TMECLient.h"

@implementation TMEWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webViewController.webView.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Class Methods

+ (void)openWebPageViewerWithURL:(NSURL *)URL andResponder:(UIResponder *)responder withTitle:(NSString *)title {
    if ([URL.absoluteString rangeOfString:[[NSUserDefaults standardUserDefaults] stringForKey:kBaseURL]].location != NSNotFound) {
        URL = [self getMergeSessionURL:URL];
    }
    
    //Find the view controller
    while (![responder isKindOfClass:UIViewController.class]) {
        responder = [responder nextResponder];
    }
    
    TMEWebViewController *webController = [[TMEWebViewController alloc] initWithURL:URL];
    webController.title = title;
    [(UIViewController *)responder presentViewController:webController
                                                animated:YES
                                              completion:nil];
}

// Generate URL for merge session request so that user is logged onto website within the in-app webview
+ (NSURL *)getMergeSessionURL:(NSURL *)URL {
    if (SYSTEM_VERSION_LESS_THAN(@"8")) {
        NSString *destinationURLString = [NSString stringWithFormat:@"%@?display_mode=minimal", [URL absoluteString]];
        NSString *mergeSessionURLString = [NSString stringWithFormat:@"%@?access_token=%@&redirect_url=%@",
                                           [[[TMEClient sharedClient].baseURL URLByAppendingPathComponent:@"merge-session"] absoluteString],
                                           [TMEClient sharedClient].authorizationToken,
                                           [destinationURLString stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]]];
        
        return [NSURL URLWithString:mergeSessionURLString];
    }
    else {
        //Set the display mode to be minimal
        NSURLComponents *finalURL = [NSURLComponents componentsWithURL:URL resolvingAgainstBaseURL:NO];
        finalURL.queryItems = @[[NSURLQueryItem queryItemWithName:@"display_mode" value:@"minimal"]];
        
        NSURLComponents *mergeURL = [NSURLComponents componentsWithURL:[[TMEClient sharedClient].baseURL URLByAppendingPathComponent:@"merge-session"]
                                               resolvingAgainstBaseURL:NO];
        mergeURL.queryItems = @[[NSURLQueryItem queryItemWithName:@"access_token" value:[TMEClient sharedClient].authorizationToken],
                                [NSURLQueryItem queryItemWithName:@"redirect_url" value:finalURL.string]];
        return mergeURL.URL;
    }
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [TeamieGlobals addHUDIndeterminateLabel:TMELocalize(@"message.loading")];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [TeamieGlobals dismissActivityLabels];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [TeamieGlobals dismissActivityLabels];
}

@end
