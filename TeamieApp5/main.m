//
//  main.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/16/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <ADEUMInstrumentation/ADEUMInstrumentation.h>

#import "TeamieAppDelegate.h"

int main(int argc, char *argv[])
{
    [ADEumInstrumentation initWithKey:kAppDynamicsMobileEUMKey];
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TeamieAppDelegate class]));
    }
}