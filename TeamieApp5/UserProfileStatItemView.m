//
//  UserProfileStatItemView.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 12/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "UserProfileStatItemView.h"

@implementation UserProfileStatItemView

- (id)init {
    // load the view from the XIB file
    self = [[[NSBundle mainBundle] loadNibNamed:@"UserProfileStatItemView" owner:self options:nil] objectAtIndex:0];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"UserProfileStatItemView" owner:self options:nil] objectAtIndex:0];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
    }
    return self;
}

@end
