//
//  DashedLineView.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 28/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "DashedLineView.h"

@implementation DashedLineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect {
    
    // Solid Line
//    CGContextRef context = UIGraphicsGetCurrentContext();
    
//    CGContextSetLineWidth(context, 5.0);
//    
//    CGContextSetStrokeColorWithColor(context, [UIColor blueColor].CGColor);
//    
//    CGContextMoveToPoint(context, 10, 200);
//    
//    CGContextAddQuadCurveToPoint(context, 150, 10, 300, 200);
//    
//    CGContextStrokePath(context);
    
    // Dashed Line
//    CGContextSetStrokeColorWithColor(context, [UIColor redColor].CGColor);
//    
//    CGFloat dashArray[] = {2,2};
//    
//    CGContextSetLineDash(context, 3, dashArray, 4);
//    
//    CGContextMoveToPoint(context, 10, 200);
//    
//    CGContextAddQuadCurveToPoint(context, 150, 10, 300, 200);
//    
//    CGContextStrokePath(context);
    
    CGContextRef context =UIGraphicsGetCurrentContext();
    CGContextBeginPath(context);
    CGContextSetLineWidth(context, 2.0);
    CGContextSetStrokeColorWithColor(context, [UIColor colorWithHex:@"#cccccc" alpha:1.0].CGColor);
    CGFloat lengths[] = {3,3};
    CGContextSetLineDash(context, 0, lengths,2);
    CGContextMoveToPoint(context, 5, 0);
    CGContextAddLineToPoint(context, self.frame.size.width,0);
    CGContextStrokePath(context);
    CGContextClosePath(context);
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
