//
//  Leaderboard.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 12/10/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Leaderboard : NSObject

@property (nonatomic, retain) NSNumber* uid;
@property (nonatomic, retain) NSString* realName;
@property (nonatomic, retain) NSString* profileImage;
@property (nonatomic, retain) NSNumber* userPoints;
@property (nonatomic, retain) NSNumber* nid;

@property (nonatomic, retain) NSArray* users;

@end