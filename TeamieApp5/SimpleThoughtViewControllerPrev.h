//
//  SimpleThoughtViewController.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 5/15/14.
//  Copyright (c) 2014 ETH. All rights reserved.
//

#import "RestApiViewController.h"
#import "SimplePost.h"

@protocol ThoughtViewControllerDelegate <NSObject>

@optional
- (void)didDismissThoughtView:(SimplePost*)thoughtData;

@end

@interface SimpleThoughtViewControllerPrev : RestApiViewController <UITextFieldDelegate, UIActionSheetDelegate>

// IBOutlets
@property (weak, nonatomic) IBOutlet UIImageView *thoughtAuthorDisplayPic;
@property (weak, nonatomic) IBOutlet UILabel *thoughtAuthorName;
@property (weak, nonatomic) IBOutlet UIImageView *thoughtTypeIndicator;
@property (weak, nonatomic) IBOutlet UILabel *thoughtTimestamp;
@property (weak, nonatomic) IBOutlet UIButton *thoughtClassroomName;
@property (weak, nonatomic) IBOutlet UITextView *thoughtMessageBody;
@property (weak, nonatomic) IBOutlet UIView *thoughtInfoBar;
@property (weak, nonatomic) IBOutlet UIScrollView *thoughtView;

@property (weak, nonatomic) IBOutlet UIImageView *commentStreamHeader;
@property (weak, nonatomic) IBOutlet UIView *commentStreamView;
@property (weak, nonatomic) IBOutlet UIToolbar *bottomToolbar;
@property (weak, nonatomic) IBOutlet UITextField *commentEditor;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *commentPostButton;

// Data properties
@property (strong, nonatomic) id<ThoughtViewControllerDelegate> thoughtViewDelegate;
// the delegate view controller that must be notified when the thought view controller is dismissed.

// Public API methods
- (id)initWithThoughtID:(NSNumber*)thoughtId delegate:(id)thoughtDelegate;
// initialize a thought view controller with a given thought ID and delegate

// IBActions
- (IBAction)postThoughtComment:(id)sender;
- (IBAction)textfieldEditingChanged:(id)sender;

// Notification methods
- (void)keyboardWillShow:(NSNotification*)note;
- (void)keyboardWillHide:(NSNotification*)note;
- (void)didTapAnywhere:(UITapGestureRecognizer*)recognizer;

// Comment Actions
- (void)upVote:(id)sender;
- (void)downVote:(id)sender;
- (void)markAsRight:(id)sender;

@end
