//
//  QuestionCellViewController.m
//  TeamieApp5
//
//  Created by Raunak on 7/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuestionCellFrontViewController.h"

@interface QuestionCellFrontViewController ()
@end

@implementation QuestionCellFrontViewController

- (id)initWithSubmissionQuestion:(SubmissionQuestion*)question andAnswer:(SubmissionAnswer *)answer forQuestionNumber:(NSNumber *)qNo {
    self = [super init];
    if (self) {
        self.question = question;
        self.answer = answer;
        self.questionNumber = qNo;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Check if there were any files submitted for the question. If yes, then show that as text.
    QuestionCellView *qcv = [QuestionCellView createQuestionCellWithType:self.question.questionType score:self.answer.currentScore maxScore:self.question.maxScore question:self.question.questionText answer:self.answer.answerSummary questionNumber:self.questionNumber isGraded:self.answer.isGraded];
    [qcv.submissionButton addTarget:self action:@selector(frontFlipButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    self.view = qcv;
    
}

- (void)frontFlipButtonPressed:(id)sender {
    QuestionCellBackViewController* vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"QuestionCellBackViewController"];
    vc.question = self.question;
    vc.answer = self.answer;
    [self presentModalViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
