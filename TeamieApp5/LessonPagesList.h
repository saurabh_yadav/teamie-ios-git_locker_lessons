//
//  LessonPagesList.h
//  TeamieApp5
//
//  Created by Raunak on 10/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LessonPagesList : NSObject

@property (nonatomic,strong) NSString* title;
@property (nonatomic,strong) NSString* descriptionTitle;
@property (nonatomic,strong) NSString* descriptionValue;
@property (nonatomic,strong) NSArray* lessonPages;

@end
