//
//  QuestionCellView.h
//  TeamieApp5
//
//  Created by Raunak on 5/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PaddedUILabel.h"
#import "ScoreBarView.h"

typedef enum {
    kFillInTheBlanks,
    kMultipleChoice,
    kMatchTheFollowing,
    kOpenEnded
}QuestionCellType ;

@interface QuestionCellView : UIView
@property (strong, nonatomic) IBOutlet PaddedUILabel *typeLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;
@property (strong, nonatomic) IBOutlet UILabel *maxScoreLabel;
@property (strong, nonatomic) IBOutlet UIWebView *questionView;
@property (strong, nonatomic) IBOutlet UILabel *questionNumberLabel;
@property (strong, nonatomic) IBOutlet ScoreBarView *scoreBar;
@property (strong, nonatomic) IBOutlet UIButton *submissionButton;
@property (strong, nonatomic) IBOutlet UILabel* gradedLabel;

+(QuestionCellView*)createQuestionCellWithType:(NSString*)type score:(NSNumber*)score maxScore:(NSNumber*)maxScore question:(NSString*)question answer:(NSString*)answer questionNumber:(NSNumber*)qNumber isGraded:(BOOL)isGraded;
@end
