//
//  Quiz.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 10/23/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "Quiz.h"


@implementation Quiz

@dynamic deadlineDate;
@dynamic numVotes;
@dynamic publishDate;
@dynamic qid;
@dynamic title;
@dynamic type;

@end
