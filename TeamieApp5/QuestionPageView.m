//
//  QuestionPageView.m
//  TeamieApp5
//
//  Created by Raunak on 6/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuestionPageView.h"

@implementation QuestionPageView

//- (id)initWithQuestionCellSize:(CGRect)frame
//{
//    self = [super initWithFrame:CGRectMake(0, 0, 748, frame.size.height)];
//    CGRect temp = self.frame;
//    if (self) {
//        self.clipsToBounds = YES;
////        self.userInteractionEnabled = YES;
//        temp = CGRectMake(self.center.x - (frame.size.width/2)-10, 0, frame.size.width+20, frame.size.height);
//        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.center.x - (frame.size.width/2)-10, 0, frame.size.width+20, frame.size.height)];
//        _scrollView.clipsToBounds = NO;
//        _scrollView.pagingEnabled = YES;
////        _scrollView.userInteractionEnabled = YES;
//        [self addSubview:_scrollView];
//        // Initialization code
//    }
//    return self;
//}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.clipsToBounds = YES;
    }
    return self;
}
//
//- (void)setScrollView:(UIScrollView *)scrollView {
//    _scrollView = [[UIScrollView alloc] init];
//    _scrollView = scrollView;
//    [self addSubview:_scrollView];
//}

-(UIView*)hitTest:(CGPoint)point withEvent:(UIEvent*)event
{
    UIView* child = nil;
    if ((child = [super hitTest:point withEvent:event]) == self) {
    	for (UIView* v in self.subviews) {
            if ([v isKindOfClass:[UIScrollView class]]) {
                return v;
            }
        }
    }
    return child;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
