//
//  ClassmatesViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 20/09/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "ClassmatesViewController.h"
#import "UserProfileViewController.h"

@implementation ClassmatesViewController

-(void)viewDidLoad
{
    [super viewDidLoad];
    //Perform REST Request for retrieving the user's connections
    // set the items_per_page parameter to 0, so that all connections are loaded
    [self.apiCaller performRestRequest:TeamieUserConnectionsRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:@"0", @"items_per_page", nil]];
    
    self.navigationItem.title = TeamieLocalizedString(@"TITLE_CONNECTIONS", nil);
    //_tableView.delegate = self;
}

-(void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response
{
    if(![response isKindOfClass:[NSArray class]])
        return;
    
    NSArray *TableItems = [[NSMutableArray alloc] init];
    NSMutableArray *tableObjects = [NSMutableArray array];
    
    /*for(UserProfile *userObject in response)
    {
        if (![tableObjects containsObject:userObject]) {
            TTTeamieUserProfileItem *imageItem = [TTTeamieUserProfileItem itemWithText:userObject.fullname subtitle:userObject.email imageURL:userObject.displayPic profileData:userObject delegate:nil selector:nil];
            [((NSMutableArray*)TableItems) addObject:imageItem];
            [tableObjects addObject:userObject];
        }
    }*/
    
    [((NSMutableArray*)TableItems) sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
		return [[self sortStringForObject:obj1] localizedCaseInsensitiveCompare:[self sortStringForObject:obj2]];
	}];
    
    self.users = TableItems;
    //TeamieListDataSource *dataSource = [[TeamieListDataSource alloc] initWithItems:TableItems];
    //self.dataSource = dataSource;
}

- (NSString *)sortStringForObject:(id)object {
    /*TTTeamieUserProfileItem* user = (TTTeamieUserProfileItem*)object;
    if ([user.text isEqualToString:@""]) {
        return user.subtitle;
    }
    return user.text;*/
}

#pragma mark - User Profile View

- (void)tableView:(UITableView*)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*UserProfileViewController* classmatesView;
    TeamieUser *user = ((TTTeamieUserProfileItem*)[self.users objectAtIndex:indexPath.row]).profileData;
    classmatesView = [[UserProfileViewController alloc] initWithUserID:[user.uid longLongValue]];
    [self.navigationController pushViewController:classmatesView animated:YES];*/
}

@end
