//
//  PollItemCell.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 23/05/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "PollItemCell.h"

@implementation PollItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

// custom initialization

-(void)customInit {
    // set background
    UIImage *bg = nil;
    bg = [UIImage imageNamed:@"ItemBackground"];
    UIImageView *bgView = [[UIImageView alloc] initWithImage:bg];
    self.backgroundView = bgView;
    // update cell according Item object
    [self updateCell];
    
}

// update cell's views with ShareBoxPollOption object
-(void)updateCell {
    self.label.text = self.currentItem.title;
}



@end
