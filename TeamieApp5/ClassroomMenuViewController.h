//
//  ClassroomMenuViewController.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 12/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"

@interface ClassroomMenuViewController : TableRestApiViewController

@property (nonatomic) NSNumber *classroomID;

@end
