//
//  UserProfile.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/25/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class MessageItem, MessageThread, NewsfeedItem, NewsfeedItemComment, UserLoginData;

@interface UserProfile : NSManagedObject

@property (nonatomic, retain) NSString * displayPic;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSString * fullname;
@property (nonatomic, retain) NSString * profileURL;
@property (nonatomic, retain) NSNumber * uid;
@property (nonatomic, retain) NSSet *commentObject;
@property (nonatomic, retain) NSSet *newsfeedItemObjects;
@property (nonatomic, retain) UserLoginData *userObject;
@property (nonatomic, retain) NSSet *messageThreads;
@property (nonatomic, retain) NSSet *messageItemsWritten;
@end

@interface UserProfile (CoreDataGeneratedAccessors)

- (void)addCommentObjectObject:(NewsfeedItemComment *)value;
- (void)removeCommentObjectObject:(NewsfeedItemComment *)value;
- (void)addCommentObject:(NSSet *)values;
- (void)removeCommentObject:(NSSet *)values;

- (void)addNewsfeedItemObjectsObject:(NewsfeedItem *)value;
- (void)removeNewsfeedItemObjectsObject:(NewsfeedItem *)value;
- (void)addNewsfeedItemObjects:(NSSet *)values;
- (void)removeNewsfeedItemObjects:(NSSet *)values;

- (void)addMessageThreadsObject:(MessageThread *)value;
- (void)removeMessageThreadsObject:(MessageThread *)value;
- (void)addMessageThreads:(NSSet *)values;
- (void)removeMessageThreads:(NSSet *)values;

- (void)addMessageItemsWrittenObject:(MessageItem *)value;
- (void)removeMessageItemsWrittenObject:(MessageItem *)value;
- (void)addMessageItemsWritten:(NSSet *)values;
- (void)removeMessageItemsWritten:(NSSet *)values;

@end
