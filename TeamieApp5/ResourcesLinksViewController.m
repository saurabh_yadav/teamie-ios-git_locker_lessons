//
//  ResourcesLinksViewController.m
//  TeamieApp5
//
//  Created by Raunak on 20/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ResourcesLinksViewController.h"

@interface ResourcesLinksViewController ()
@property (nonatomic, strong) NSNumber* tid;
@property (nonatomic, strong) WEPopoverController* menuController;
@end

@implementation ResourcesLinksViewController

- (void)openURL:(NSURL*)url WithThoughtID:(NSNumber*)tid {
    //[super openURL:url];
    self.linkURL = url;
    self.tid = tid;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.navigationBarTintColor = [UIColor colorWithHex:@"#BE202E" alpha:1.0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(menuButtonPressed:)];
}

- (void)menuButtonPressed:(id)sender {
    if(!self.menuController) {
        ResourcesShareMenuViewController* contentViewController = [[ResourcesShareMenuViewController alloc] init];
        contentViewController.delegate = self;
        
        Class popoverClass;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            popoverClass = [UIPopoverController class];
        }
        else {
            popoverClass = [WEPopoverController class];
        }
        
        self.menuController = [[popoverClass alloc] initWithContentViewController:contentViewController];
        self.menuController.delegate = self;
        [self.menuController presentPopoverFromBarButtonItem:(UIBarButtonItem*)sender permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown) animated:YES];
    }
    else {
        [self.menuController dismissPopoverAnimated:YES];
        self.menuController= nil;
    }
}

- (void) shareMenuDidSelectDeleteOption:(ShareMenuOptions)option {
    
    [self.menuController dismissPopoverAnimated:YES];
    self.menuController= nil;
    
    switch (option) {
        case kShare:
            [self.delegate resourcesLinksControllerDidSelectShareOption:self];
            break;
        case kOpenIn:
            break;
        default:
            break;
    }
}

- (BOOL)shareMenuShouldDisplayOpenInOption {
    return NO;
}

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    self.menuController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
