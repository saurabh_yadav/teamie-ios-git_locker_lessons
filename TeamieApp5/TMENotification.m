//
//  TMENotification.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TMENotification.h"
#import "UserLoginData.h"
#import "TMEUser.h"

@implementation TMENotification

@synthesize mainText;
@synthesize notificationType;
@synthesize subText;
@synthesize entityID;

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"readStatus": @"read_status"};
}

- (instancetype) initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self) {
        [self setMainText];
        [self setSubText];
        [self setNotificationTypeAndEntityID];
    }
    return self;
}

-(UIImage *)postImage {
    NSString *purpose;
    switch (notificationType) {
        case PlainBulletinNotification:
            purpose = @"link";
            break;
        case ThoughtLikeNotification:
            purpose = @"like";
            break;
        case ThoughtEchoNotification:
            purpose = @"thought";
            break;
        case ThoughtAnnouncementNotification:
            purpose = @"announcement";
            break;
        case ThoughtCommentNotification:
            purpose = @"comment";
            break;
        case ThoughtCommentActionNotification:
            purpose = @"comment";
            break;
        case LessonPostNotification:
            purpose = @"lesson";
            break;
        case QuizPostNotification:
            purpose = @"quiz";
            break;
        case BadgeAwardNotification:
            return [[UIImage alloc] initWithData:
                    [NSData dataWithContentsOfURL:[TMEUser currentUser].user_profile_image.path]];
    }

    return [TeamieUIGlobals defaultPicForPurpose:purpose];
}

#pragma mark private

- (void)setMainText {
    NSDictionary *options = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)};
    NSData *data = [self.body dataUsingEncoding:NSUTF8StringEncoding];
    NSAttributedString *bodyText = [[NSAttributedString alloc] initWithData:data options:options documentAttributes:nil error:nil];

    mainText = [[NSMutableAttributedString alloc] initWithString:[bodyText string]];
    [bodyText enumerateAttributesInRange:(NSRange){0, [bodyText length]}
                             options:NSAttributedStringEnumerationReverse
                          usingBlock:^(NSDictionary *attrs, NSRange range, BOOL *stop) {
        if ([[attrs objectForKey:@"NSUnderline"] isEqual:@1]) {
            [mainText addAttribute:NSFontAttributeName value:[TeamieGlobals appFontFor:@"notificationTextHighlight"] range:range];
        } else {
            [mainText addAttribute:NSFontAttributeName value:[TeamieGlobals appFontFor:@"notificationText"] range:range];
        }
    }];

}

- (void)setSubText {
    NSDate *timestamp = [NSDate dateWithTimeIntervalSince1970:[self.sent doubleValue]];
    subText = [TeamieGlobals timeIntervalWithStartDate:timestamp withEndDate:[NSDate date] brevity:0];
}

// sets the type of bulletin board notification based on the text of the notification
- (void)setNotificationTypeAndEntityID {
    if (([self.body rangeOfString:@"liked"].location != NSNotFound || [self.body rangeOfString:@"likes"].location != NSNotFound) && [self.body rangeOfString:@"thought"].location != NSNotFound) {
        notificationType = ThoughtLikeNotification;
        entityID = [TeamieGlobals extractNumberFromURL:self.body prefix:@"thought"];
    }
    else if ([self.body rangeOfString:@"echoed"].location != NSNotFound && [self.body rangeOfString:@"question"].location != NSNotFound) {
        notificationType = ThoughtEchoNotification;
        entityID = [TeamieGlobals extractNumberFromURL:self.body prefix:@"thought"];
    }
    else if (([self.body rangeOfString:@"commented"].location != NSNotFound) || ([self.body rangeOfString:@"responded"].location != NSNotFound) || ([self.body rangeOfString:@"mentioned"].location != NSNotFound)) {
        notificationType = ThoughtCommentNotification;
        entityID = [TeamieGlobals extractNumberFromURL:self.body prefix:@"thought"];
    }
    else if ([self.body rangeOfString:@"announcement"].location != NSNotFound) {
        notificationType = ThoughtAnnouncementNotification;
        entityID = [TeamieGlobals extractNumberFromURL:self.body prefix:@"thought"];
    }
    else if ([[self.body lowercaseString] rangeOfString:@"lesson"].location != NSNotFound) {
        notificationType = LessonPostNotification;
        entityID = [TeamieGlobals extractNumberFromURL:self.body prefix:@"node"];
    }
    else if ([self.body rangeOfString:@"quiz"].location != NSNotFound) {
        notificationType = QuizPostNotification;
        entityID = [TeamieGlobals extractNumberFromURL:self.body prefix:@"quiz"];
    }
    else if ([self.body rangeOfString:@"awarded"].location != NSNotFound && [self.body rangeOfString:@"badge"].location != NSNotFound) {
        notificationType = BadgeAwardNotification;
        entityID = nil;
    }
    else if (([self.body rangeOfString:@"marked as right"].location != NSNotFound && [self.body rangeOfString:@"answer"].location != NSNotFound && [self.body rangeOfString:@"question"].location != NSNotFound) || ([self.body rangeOfString:@"reported"].location != NSNotFound)) {
        // if thought comment has been marked as right or thought has been reported
        notificationType = ThoughtCommentActionNotification;
        entityID = [TeamieGlobals extractNumberFromURL:self.body prefix:@"thought"];
    }
    else {
        notificationType = PlainBulletinNotification;
        entityID = nil;
    }
}

@end
