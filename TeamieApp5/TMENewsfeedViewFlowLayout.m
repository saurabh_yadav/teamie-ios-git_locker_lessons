//
//  TMENewsfeedViewFlowLayout.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 04/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMENewsfeedViewFlowLayout.h"

@implementation TMENewsfeedViewFlowLayout

- (id)init {
    
    CGFloat topMargin    = 10.0f;
    CGFloat leftMargin   = 10.0f;
    CGFloat bottomMargin = 10.0f;
    CGFloat rightMargin  = 5.0f;

    self = [super init];
    if (self) {
        self.itemSize                = CGSizeMake(290, 240);
        self.sectionInset            = UIEdgeInsetsMake(topMargin, leftMargin, bottomMargin, rightMargin);
        self.minimumInteritemSpacing = 5.0f;
        self.minimumLineSpacing      = 5.0f;
    }
    
    return self;
}

// Shamelessly borrowed from nice guy over here http://stackoverflow.com/a/14542269/1781918

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray* layoutAttributes = [super layoutAttributesForElementsInRect:rect];
    for (UICollectionViewLayoutAttributes* attribute in layoutAttributes) {
        if (nil == attribute.representedElementKind) {
            NSIndexPath* indexPath = attribute.indexPath;
            attribute.frame = [self layoutAttributesForItemAtIndexPath:indexPath].frame;
        }
    }
    return layoutAttributes;
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewLayoutAttributes* layoutAttributes = [super layoutAttributesForItemAtIndexPath:indexPath];
    
    if (indexPath.item == 0 || indexPath.item == 1) // degenerate case 1, first item of section
        return layoutAttributes;
    
    NSIndexPath* previousIndexPath = [NSIndexPath indexPathForItem:indexPath.item-2 inSection:indexPath.section];
    
    CGRect previousFrame = [self layoutAttributesForItemAtIndexPath:previousIndexPath].frame;
    CGFloat rightPrev = previousFrame.origin.y + previousFrame.size.height + 10;
    if (layoutAttributes.frame.origin.y <= rightPrev) // degenerate case 2, first item of line
        return layoutAttributes;
    
    CGRect updatedFrame = layoutAttributes.frame;
    updatedFrame.origin.y = rightPrev;
    layoutAttributes.frame = updatedFrame;

    return layoutAttributes;
}

@end
