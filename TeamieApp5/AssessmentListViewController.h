//
//  AssessmentListViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 27/10/14.
//  Copyright (c) 2014 ETH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMECollectionViewController.h"

@interface AssessmentListViewController : TMECollectionViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

/**
 *  Custom initializer method to initialize the list with object of a particular classroom
 *
 *  @param nid nid of a node (classroom)
 *
 *  @return self
 */
- (id)initWithClassroomId:(NSNumber *)nid andClassroomTitle:(NSString *)classroomName;

@end
