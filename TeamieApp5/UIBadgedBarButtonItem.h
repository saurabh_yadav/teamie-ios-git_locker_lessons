//
//  TeamieBadgedBarButtonItem.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 9/9/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBadgedBarButtonItem : UIBarButtonItem

- (id) initWithTargetAndImage:(id)target action:(SEL)action forControlEvents:(UIControlEvents)event image:(UIImage *)image;

- (void) updateBadgeValue:(int)value;
- (void) updateBadgeValue:(int)value refresh:(BOOL)refresh;

@end
