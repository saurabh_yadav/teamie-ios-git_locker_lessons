//
//  QuestionPageViewController.m
//  TeamieApp5
//
//  Created by Raunak on 6/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuestionPageViewController.h"
#import "QuestionCellView.h"
#import "QuizSubmissionsViewController.h"

@interface QuestionPageViewController () 
@property (nonatomic,strong) UIScrollView* scrollView;
@end

@implementation QuestionPageViewController

- (id)initWithUserSubmission:(SubmissionDetails*)details forAllQuestions:(NSArray *)questions andFilteredQuestions:(NSArray *)filteredQuestions quizID:(NSNumber *)quizID{
    self = [super init];
    if (self) {
        self.userDetails = details;
        self.quizQuestions = questions;
        self.filteredQuestions = filteredQuestions;
        self.quizID = quizID;
    }
    return self;
}

- (id)initWithUserSubmission:(SubmissionDetails *)details forQuestions:(NSArray *)questions quizID:(NSNumber *)quizID {
    NSMutableArray* arr = [NSMutableArray array];
    for (int i = 1; i <= [questions count]; i++) {
        [arr addObject:[NSNumber numberWithInt:i]];
    }
    return [self initWithUserSubmission:details forAllQuestions:questions andFilteredQuestions:arr quizID:quizID];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
//    [self setUpObjects];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(questionFilterChanged:) name:@"QuestionFilterChanged" object:nil];
	// Do any additional setup after loading the view.
    CGRect frame = CGRectMake(0, 0, 350, 400);
    QuestionPageView* v = [[QuestionPageView alloc] initWithFrame:CGRectMake(0, 0, 728, frame.size.height + 20)];
    self.view = v;
        
    UIScrollView *myScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(self.view.center.x - (frame.size.width/2) - 30, 10, frame.size.width + 60, frame.size.height)];
    myScrollView.showsHorizontalScrollIndicator = NO;
    myScrollView.showsVerticalScrollIndicator = NO;
    myScrollView.scrollsToTop = NO;
    myScrollView.bounces = YES;
    myScrollView.clipsToBounds = NO;
//    myScrollView.pagingEnabled = YES;
//    myScrollView.delaysContentTouches = YES;
    myScrollView.delegate = self;

    [self.view addSubview:myScrollView];
//    [self loadScrollView:myScrollView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpObjects];
    for (UIView* v in self.view.subviews) {
        if ([v isKindOfClass:[UIScrollView class]]) {
            [self loadScrollView:((UIScrollView*)v)];
        }
    }
}

- (void)questionFilterChanged:(NSNotification*)notification {
    NSMutableArray* array = [[NSMutableArray alloc] init];
    NSArray* selectedQIDs = [[notification userInfo] objectForKey:@"filterArray"];
    for (SubmissionQuestion* question in self.quizQuestions) {
        for (SubmissionAnswer* answer in self.userDetails.submissionAnswers) {
            if ([answer.qid isEqualToNumber:question.qid]) {
                if ([selectedQIDs containsObject:[NSNumber numberWithInt:[self.quizQuestions indexOfObject:question] + 1]]) {
                    QuestionCellView *qcv = [QuestionCellView createQuestionCellWithType:question.questionType score:answer.currentScore maxScore:question.maxScore question:question.questionText answer:answer.answerHtml questionNumber:[NSNumber numberWithInt:[self.quizQuestions indexOfObject:question] + 1] isGraded:answer.isGraded];
                    qcv.submissionButton.tag = [self.quizQuestions indexOfObject:question];
                    [qcv.submissionButton addTarget:self action:@selector(frontFlipButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                    [array addObject:qcv];
                }
            }
        }
    }
    self.questionViews = nil;
    self.questionViews = array;
    self.filteredQuestions = selectedQIDs;
    UIScrollView* myScrollView;
    for (UIView* v in self.view.subviews) {
        if ([v isKindOfClass:[UIScrollView class]]) {
            myScrollView = (UIScrollView*)v;
        }
    }
    [self loadScrollView:myScrollView];
}

- (void)loadScrollView:(UIScrollView*)scrollView {
    for (UIView* v in scrollView.subviews) {
        [v removeFromSuperview];
    }
    scrollView.contentSize = CGSizeMake(CGRectGetWidth(scrollView.frame) * self.questionViews.count, CGRectGetHeight(scrollView.frame));
    int page = 0;
    for (QuestionCellView* questionView in self.questionViews) {
        CGRect frame = scrollView.frame;
        frame.origin.x = CGRectGetWidth(frame) * page + 30;
        frame.origin.y = 0;
        frame.size = questionView.frame.size;
        questionView.frame = frame;
//        [self addChildViewController:questionView];
        [scrollView addSubview:questionView];
//        [questionView didMoveToParentViewController:self];
        page++;
    }
    [TeamieGlobals dismissActivityLabels];
}

- (void)loadScrollView:(UIScrollView*)scrollView withPage:(NSUInteger)page
{
    if (page >= self.questionViews.count)
        return;
    QuestionCellView* questionView = [self.questionViews objectAtIndex:page];
    CGRect frame = scrollView.frame;
    frame.origin.x = CGRectGetWidth(frame) * page + 30;
    frame.origin.y = 0;
    frame.size = questionView.frame.size;
    questionView.frame = frame;
        
    [scrollView addSubview:questionView];
}

- (void)setUpObjects {
    NSMutableArray* array = [[NSMutableArray alloc] init];    
    for (SubmissionQuestion* question in self.quizQuestions) {
        for (SubmissionAnswer* answer in self.userDetails.submissionAnswers) {
            if ([answer.qid isEqualToNumber:question.qid]) {
                if ([self.filteredQuestions containsObject:[NSNumber numberWithInt:[self.quizQuestions indexOfObject:question] + 1]]) {
                    QuestionCellView *qcv = [QuestionCellView createQuestionCellWithType:question.questionType score:answer.currentScore maxScore:question.maxScore question:question.questionText answer:answer.answerSummary questionNumber:[NSNumber numberWithInt:[self.quizQuestions indexOfObject:question] + 1] isGraded:answer.isGraded];
                    qcv.submissionButton.tag = [self.quizQuestions indexOfObject:question];
                    [qcv.submissionButton addTarget:self action:@selector(frontFlipButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                    [array addObject:qcv];
                }
            }
        }
    }
    self.questionViews = array;
}

- (void)frontFlipButtonPressed:(id)sender {
    UIButton* button = (UIButton*)sender;
    QuestionCellBackViewController* vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"QuestionCellBackViewController"];
    SubmissionQuestion* question = [self.quizQuestions objectAtIndex:button.tag];
    
    for (SubmissionAnswer* answer in self.userDetails.submissionAnswers) {
        if ([answer.qid isEqualToNumber:question.qid]) {
            vc.question = question;
            vc.answer = answer;
            vc.questionNumber = [NSNumber numberWithInteger:button.tag];
            vc.quizID = self.quizID;
            vc.userID = self.userDetails.user.uid;
            if ([self.parentViewController isKindOfClass:[QuizSubmissionsViewController class]]) {
                vc.delegate = (QuizSubmissionsViewController*)self.parentViewController;
            }
        }
    }
    [self presentModalViewController:vc animated:YES];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated {
    for (UIView* v in self.view.subviews) {
        if ([v isKindOfClass:[UIScrollView class]]) {
            for (UIView* subview in v.subviews) {
                [subview removeFromSuperview];
            }
        }
    }

//    self.quizQuestions = nil;
    self.questionViews = nil;
//    self.quizID = nil;
//    self.userDetails = nil;
//    self.filteredQuestions = nil;
    [super viewDidDisappear:animated];
}

- (void)viewDidUnload {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"QuestionFilterChanged" object:nil];
}

@end
