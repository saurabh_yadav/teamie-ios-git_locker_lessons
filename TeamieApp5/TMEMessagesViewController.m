//
//  TMEMessagesViewController.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 29/6/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEMessagesViewController.h"

#import "TeamieRevealController.h"
#import "TMEWebViewController.h"

@interface TMEMessagesViewController() <TMERevealDelegate, UIWebViewDelegate>

@end

@implementation TMEMessagesViewController

- (void)loadView {
    [super loadView];
    self.view = [[UIWebView alloc] init];
    [(UIWebView *)self.view setDelegate:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = TMELocalize(@"menu.messages");
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [(UIWebView *)self.view loadRequest:[NSURLRequest requestWithURL:[TMEWebViewController getMergeSessionURL:[NSURL URLWithString:@"/messages"]]]];
}

#pragma mark - UIWebViewDelegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [TeamieGlobals addHUDIndeterminateLabel:TMELocalize(@"message.loading")];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [TeamieGlobals dismissActivityLabels];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [TeamieGlobals dismissActivityLabels];
}

@end
