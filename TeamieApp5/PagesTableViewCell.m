//
//  PagesTableViewCell.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 21/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "PagesTableViewCell.h"
@interface SWTableViewCell()
@property (nonatomic, strong) UIScrollView *cellScrollView;
@end
@implementation PagesTableViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.cellScrollView.showsHorizontalScrollIndicator = NO;
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
