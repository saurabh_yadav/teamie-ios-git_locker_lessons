//
//  TMEPostCreateViewController.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 5/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostCreateClassroomsViewController.h"
#import "TMEPostCreateAudioAttachmentViewController.h"
#import "TMEModel.h"

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

#import <SZTextView/SZTextView.h>
#import <Masonry/Masonry.h>

@interface TMEPostCreateViewController : UIViewController
<UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIAlertViewDelegate, TMEPostCreateClassroomsViewDelegate, UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, TMEPostCreateAudioAttachmentDelegate>

- (instancetype)initWithClassroom:(TMEClassroom *)classroom;
- (instancetype)initWithCopiedLink:(NSString *)link;
@end