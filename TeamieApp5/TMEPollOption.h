//
//  TMEPollOption.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 05/01/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEModel.h"

@interface TMEPollOption : TMEModel

@property (nonatomic, strong) NSNumber *choiceID;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSNumber *numberOfVotes;
@property (nonatomic) BOOL currentUserVote;

@end
