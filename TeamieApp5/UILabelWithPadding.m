//
//  UILabelWithPadding.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 22/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UILabelWithPadding.h"

/**
 *  Status Labels Left Margin
 */
CGFloat kStatusLabelLeftMarginn  = 5.0;

/**
 *  Status Labels Right Margin
 */
CGFloat kStatusLabelRightMarginn = 5.0;

/**
 *  Status Labels Corner Radius
 */
CGFloat kLabelCornerRadiuss      = 2.0;


@implementation UILabelWithPadding

- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect {
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];
    self.edgeInsets = UIEdgeInsetsMake(0, kStatusLabelLeftMarginn, 0, kStatusLabelRightMarginn);
    self.font = [TeamieGlobals appFontFor:@"AppFontForPurposeCellLabelText"];
    self.layer.cornerRadius = kLabelCornerRadiuss;
    self.layer.masksToBounds = YES;
    self.textAlignment = NSTextAlignmentCenter;
}

- (CGSize)intrinsicContentSize {
    CGSize size = [super intrinsicContentSize];
    size.width  += self.edgeInsets.left + self.edgeInsets.right;
    size.height += self.edgeInsets.top + self.edgeInsets.bottom;
    return size;
}

#pragma mark - Methods

- (void)configurePublishStatusLabelWithStatus:(PublishStatus)publishStatus {
    switch (publishStatus) {
        case PublishStatusRemoval: {
            [self removeFromSuperview];
        }
            break;
        case PublishStatusDrafted: {
            [self publishStatusLabelText:@"Draft" textColor:[TeamieUIGlobalColors publishStatusDraftLabelTextColor] backgroundColor:[TeamieUIGlobalColors publishStatusDraftLabelBackGroundColor]];
        }
            break;
        case PublishStatusPublished: {
            [self publishStatusLabelText:@"Published" textColor: [TeamieUIGlobalColors publishStatusPublishedLabelTextColor] backgroundColor:[TeamieUIGlobalColors publishStatusPublishedLabelBackGroundColor]];
        }
            break;
        default:
            break;
    }
}

- (void)publishStatusLabelText:(NSString *)text textColor:(UIColor *)textColor backgroundColor:(UIColor *)backgroundColor {
    self.text = text;
    self.font = [TeamieGlobals appFontFor:@"AppFontForPurposeCellLabelText"];
    self.textColor = textColor;
    self.backgroundColor = backgroundColor;
    self.textAlignment = NSTextAlignmentCenter;
}

@end
