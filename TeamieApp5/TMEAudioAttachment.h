//
//  TMEAudioAttachment.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 17/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostAttachment.h"

@interface TMEAudioAttachment : TMEPostAttachment

@property (nonatomic, strong) NSNumber *fid;
@property (nonatomic, strong) NSString *filemime;
@property (nonatomic, strong) NSString *icon;

@end
