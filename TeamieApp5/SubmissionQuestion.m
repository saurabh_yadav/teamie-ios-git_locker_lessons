//
//  SubmissionQuestion.m
//  TeamieApp5
//
//  Created by Raunak on 14/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "SubmissionQuestion.h"

@implementation SubmissionQuestion

@synthesize qid;
@synthesize questionType;
@synthesize questionText;
@synthesize maxScore;
@synthesize correctAnswerText;

@end
