//
//  TeamieClient.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEClient.h"
#import "TeamieAppDelegate.h"

#import <GooglePlus/GooglePlus.h>

@implementation TMEClient

static NSString * const kAuthorizationHeader = @"Authorization";
static NSString * const kAuthorizationFormat = @"Bearer %@";
static NSString * const kAccessToken = @"access_token_v3";

#pragma mark - Singleton

NSString * const kBaseURL = @"baseURL";

+ (instancetype)sharedClient {
    static dispatch_once_t onceToken;
    static TMEClient *sharedClient = nil;
    dispatch_once(&onceToken, ^{
        sharedClient = [[self alloc] initWithBaseURL:[NSURL URLWithString:TEAMIE_DEFAULT_OAUTH_REST_URL]];
        
        sharedClient.authorizationToken = [[NSUserDefaults standardUserDefaults] stringForKey:kAccessToken];
        
        //TODO: Change this to use HTTPS (setAllowInvalidCertificates - none, setPinnedCertificates, setSSLPinningMode)
        sharedClient.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        
        //Custom response serializer to parse response body on error
        sharedClient.responseSerializer = [TMEJSONResponseSerializerWithData new];
        
        //Custom headers sent with each request
        [sharedClient.requestSerializer setValue:TEAMIE_DEFAULT_API_VERSION_NUMBER
                              forHTTPHeaderField:@"X-Teamie-Api-Version"];
        [sharedClient.requestSerializer setValue:TEAMIE_DEFAULT_CLIENT
                              forHTTPHeaderField:@"X-Teamie-Client"];
    });
    return sharedClient;
}

#pragma mark - Authorization

- (void)setAuthorizationToken:(NSString *)token {
    [self.requestSerializer setValue:[NSString stringWithFormat:kAuthorizationFormat, token] forHTTPHeaderField:kAuthorizationHeader];
    _authorizationToken = token;
    if (token == nil) {
        [self.requestSerializer clearAuthorizationHeader];
    }
}

- (BOOL)isLoggedIn {
    return self.authorizationToken != nil;
}

- (AFHTTPRequestOperation *)request:(TMERequest)request
                         parameters:(NSDictionary *)parameters
                            makeURL:(NSString *(^)(NSString *URL))URLBlock
                     loadingMessage:(NSString *)message
                         completion:(void (^)())completionBlock
                            success:(void (^)(id reponse))successBlock
                            failure:(void (^)(NSError *error))failureBlock {
    AFHTTPRequestOperation *operation = [super
                                         request:request
                                         parameters:parameters
                                         makeURL:URLBlock
                                         loadingMessage:message
                                         completion:completionBlock
                                         success:successBlock
                                         failure:failureBlock];
    if (self.authorizationToken) {
        //Redirect clears the Authorization header, this is to add it back
        [operation setRedirectResponseBlock:^NSURLRequest *(NSURLConnection *connection, NSURLRequest *request, NSURLResponse *redirectResponse) {
            if (request.allHTTPHeaderFields[kAuthorizationHeader]) {
                return request;
            }
            NSMutableURLRequest *authorizedRequest = [request mutableCopy];
            [authorizedRequest setValue:[NSString stringWithFormat:kAuthorizationFormat, self.authorizationToken] forHTTPHeaderField:kAuthorizationHeader];
            return [authorizedRequest copy];
         }];
    }
    return operation;
}

#pragma mark - Custom Network

- (void)getUserProfile:(NSDictionary *)parameters
                   uid:(NSNumber *)uid
        loadingMessage:(NSString *)message
               success:(void (^)(TMEUserProfile *user))successBlock
               failure:(void (^)(NSError *error))failureBlock {
    [self
     request:TeamieNewUserProfileRequest
     parameters:nil
     makeURL:^NSString *(NSString *URL) {
         return [NSString stringWithFormat:@"%@%@.json", URL, uid];
     }
     loadingMessage:(NSString *)message
     success:^(NSDictionary *response) {
         TMEUserProfile *userProfile = [TMEUserProfile parseDictionary:response];
        
         // whenever user profile info of the current user is loaded, update the cached current user object.
         if ([userProfile.user.uid isEqualToNumber:TMEUser.currentUser.uid]) {
             [TMEUser setCurrentUser:userProfile.user];
         }
         successBlock(userProfile);
     }
     failure:failureBlock];
}

- (void)login:(NSDictionary *)parameters
      success:(void (^)())successBlock
      failure:(void (^)(NSError *))failureBlock {
    NSMutableDictionary *modifiedParameters = [NSMutableDictionary dictionaryWithDictionary:parameters];
    modifiedParameters[@"client_id"] = modifiedParameters[@"client_id"]?:TEAMIE_DEFAULT_OAUTH_CLIENT_ID;
    modifiedParameters[@"grant_type"] = modifiedParameters[@"grant_type"]?:TEAMIE_DEFAULT_OAUTH_GRANT_TYPE;
    NSString * institutePlainURL = [[[[parameters valueForKey:@"institute"]
                                      stringByReplacingOccurrencesOfString:@"http://" withString:@""]
                                     stringByReplacingOccurrencesOfString:@"https://" withString:@""]
                                    stringByReplacingOccurrencesOfString:@"/api" withString:@""];
    
    // if the institute URL happens to be empty then make it the default client site URL
    if ([institutePlainURL length] == 0) {
        institutePlainURL = TEAMIE_DEFAULT_CLIENT_SITE;
    }
    
    // set the site parameter
    modifiedParameters[@"site"] = institutePlainURL;
    
    // save the site URL as base url for later use. For eg: When opening lessons & quizzes in-app
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"https://%@", institutePlainURL] forKey:kBaseURL];
    
    [self
     request:TeamieUserLoginRequest
     parameters:modifiedParameters
     loadingMessage:TMELocalize(@"message.verify-account")
     success:^(NSDictionary *response) {
         NSNumber * userID = [NSNumber numberWithLongLong:[response[@"user"][@"uid"] longLongValue]];
         [[NSUserDefaults standardUserDefaults] setObject:userID forKey:@"current_user_uid"];
         if (userID) {
             [TMEUser setCurrentUser:[TMEUser parseDictionary:response[@"user"][@"user"]]];
         }
         
         [[NSUserDefaults standardUserDefaults] setObject:response[@"access_token"] forKey:kAccessToken];
         self.authorizationToken = response[@"access_token"];
         [[NSUserDefaults standardUserDefaults] setObject:response[@"refresh_token"] forKey:@"refresh_token"];

         //if user is logged in and the APNS Device token is set, then contact server to save the token
         if ([[TMEClient sharedClient] isLoggedIn]) {
             NSString *deviceToken = [[NSUserDefaults standardUserDefaults] stringForKey:@"apnsDeviceToken"];
             if (deviceToken) {
                 [self
                  request:TeamieUserDeviceRegisterRequest
                  parameters:@{@"token":deviceToken, @"type":@"ios"}
                  loadingMessage:nil
                  success:nil
                  failure:nil];
             }
         }
         
         NSArray *roles = response[@"user_roles"];
         NSString *role = @"other";
         if (roles) {
             if ([roles containsObject:@"site administrator"] || [roles containsObject:@"school administrator"]) {
                 role = @"admin";
             }
             else if ([roles containsObject:@"teacher"]) {
                 role = @"teacher";
             }
             else if ([roles containsObject:@"student"]) {
                 role = @"student";
             }
         }

         [TMEAnalyticsController trackDimension:TMEAnalyticsClientDimension value:institutePlainURL];
         [TMEAnalyticsController trackDimension:TMEAnalyticsUserRoleDimension value:role];

         if (successBlock) {
             successBlock();
         }
     } failure:failureBlock];
}

- (void)logout:(void (^)())successBlock {
    __block NSString* apnsDeviceToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"apnsDeviceToken"];
    
    UIWindow *topWindow = [[[UIApplication sharedApplication].windows sortedArrayUsingComparator:^NSComparisonResult(UIWindow *win1, UIWindow *win2) {
        return win1.windowLevel - win2.windowLevel;
    }] lastObject];
    MBProgressHUD *HUD = [MBProgressHUD showHUDAddedTo:topWindow animated:YES];
    HUD.animationType = MBProgressHUDAnimationFade;
    HUD.mode = MBProgressHUDModeIndeterminate;
    HUD.labelText = TMELocalize(@"message.logging-out");
    
    void (^logoutBlock)() = ^void() {
        [self request:TeamieUserLogoutRequest parameters:nil loadingMessage:nil success:nil failure:nil];

        //Remove all information of the user, including access token
        [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:[NSBundle mainBundle].bundleIdentifier];
        self.authorizationToken = nil;

        //Set this back, this is not supposed to be deleted
        [[NSUserDefaults standardUserDefaults] setObject:apnsDeviceToken forKey:@"apnsDeviceToken"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        //Sign out from google
        [[GPPSignIn sharedInstance] signOut];

        [HUD hide:YES];
        if (successBlock) {
            successBlock();
        }
    };
    
    if (apnsDeviceToken) {
        // call the web service to unset the device token for push notifications
        [self
         request:TeamieUserDeviceUnregisterRequest
         parameters:nil
         makeURL:^NSString *(NSString *URL) {
             return [NSString stringWithFormat:URL, apnsDeviceToken];
         }
         loadingMessage:nil
         completion:logoutBlock
         success:nil
         failure:nil];
    }
    else {
        logoutBlock();
    }
}

@end

