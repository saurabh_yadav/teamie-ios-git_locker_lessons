//
//  AdditionalInfoField.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 10/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMELessonAdditionalInfo.h"

@implementation TMELessonAdditionalInfo

+ (NSValueTransformer *)tidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

@end
