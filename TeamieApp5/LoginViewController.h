//
//  LoginViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/20/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GooglePlus/GooglePlus.h>

@class TMESiteInfo;
@class GPPSignInButton;

@interface LoginViewController : UIViewController <UITextFieldDelegate, GPPSignInDelegate> {

    UITapGestureRecognizer* _tapGestureRecognizer;
    UIView* _viewToTapOn;
}

- (IBAction)loginButtonClicked:(id)sender;
- (IBAction)registerButtonClicked:(id)sender;
- (IBAction)forgotPasswordClicked:(id)sender;
- (IBAction)backButtonClicked:(id)sender;
- (id)initFromStoryboard;

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *appVersion;

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton* registerButton;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet GPPSignInButton *googleSigninButton;

// Custom UI elements
@property (weak, nonatomic) IBOutlet UILabel *sloganLabel;
@property (weak, nonatomic) IBOutlet UILabel *subsloganLabel;
@property (weak, nonatomic) IBOutlet UIImageView *customerLogoImage;

@property (strong, nonatomic) NSString* instituteURL;
@property (strong, nonatomic) TMESiteInfo* siteinfo;

@property (nonatomic, getter=areConstraintsAdded) BOOL constraintsAdded;

@end
