//
//  SubmissionQuestion.h
//  TeamieApp5
//
//  Created by Raunak on 14/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubmissionQuestion : NSObject

@property (nonatomic,strong) NSNumber* qid;
@property (nonatomic,strong) NSString* questionType;
@property (nonatomic,strong) NSString* questionText;
@property (nonatomic,strong) NSNumber* maxScore;
@property (nonatomic,strong) NSString* correctAnswerText;

@end
