//
//  TeamieModel.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "Mantle.h"

@interface TMEModel : MTLModel <MTLJSONSerializing>

/**
 Reversible transformer between a string and a NSNumber
 */
+ (NSValueTransformer *)numberJSONTransformer;

/**
 Reversible transformer between any object and a BOOL
 */
+ (NSValueTransformer *)boolJSONTransformer;

/**
 The given object is either an array of dictionaries or a dictionary of dictionaries, this enumerates over the object and returns an NSArray of class objects
 @param modelClass should be a subclass of TMEModel
 */
+ (NSValueTransformer *)listJSONTransformer:(Class)modelClass;

+ (NSValueTransformer *)dictionaryListJSONTransformer;

/**
 Reversible transformer between a string timestamp and a NSDate object
 */
+ (NSValueTransformer *)dateJSONTransformer;

/**
 Reversible transformer between a string timezone and a NSTimestamp object
 */
+ (NSValueTransformer *)timezoneJSONTransformer;

/**
 Maps the given dictionary to the current class as defined by the mapping in that class
 */
+ (instancetype)parseDictionary:(NSDictionary *)jsonObject;

/**
 The given object is either an array of dictionaries or a dictionary of dictionaries, this enumerates over the object and returns an NSArray of class objects
 */
+ (NSArray *)parseList:(id)jsonObjects;

+ (void)storeToUserDefaults:(NSArray *)objects;

+ (NSArray *)getFromUserDefaults;

+ (void)deleteFromUserDefaults;

@end
