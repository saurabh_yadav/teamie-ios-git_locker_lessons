//
//  ResourceWebViewController.h
//  TeamieApp5
//
//  Created by Raunak on 16/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "WEPopoverController.h"
#import "ResourcesShareMenuViewController.h"

@class ResourceWebViewController;

@protocol ResourcesWebControllerDelegate
- (void)resourcesWebControllerDidSelectShareOption:(ResourceWebViewController*)webController;
@end

@interface ResourceWebViewController : UIViewController <UIDocumentInteractionControllerDelegate, ShareMenuControllerDelegate, WEPopoverControllerDelegate>

@property (nonatomic, weak) id<ResourcesWebControllerDelegate> delegate;
@property (nonatomic, strong) NSURL* fileURL;
- (void)loadFileWithUrl:(NSURL*)url WithTitle:(NSString*)title WithThoughtID:(NSNumber*)tid;

@end
