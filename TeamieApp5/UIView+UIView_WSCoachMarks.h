//
//  UIView+UIView_WSCoachMarks.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/27/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (UIView_WSCoachMarks)

- (CGRect)rectToMarkInView:(UIView *)view;
- (CGRect)rectToMarkInView:(UIView *)view withInset:(UIEdgeInsets )inset;

@end
