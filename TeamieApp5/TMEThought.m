//
//  TMEThought.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 24/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEThought.h"

@implementation TMEThought

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    mapping[@"likesCount"] = @"statistics.like.count";
    return mapping;
}

@end
