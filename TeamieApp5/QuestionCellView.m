//
//  QuestionCellView.m
//  TeamieApp5
//
//  Created by Raunak on 5/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuestionCellView.h"

@implementation QuestionCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(QuestionCellView*)createQuestionCellWithType:(NSString*)type score:(NSNumber*)score maxScore:(NSNumber*)maxScore question:(NSString*)question answer:(NSString*)answer questionNumber:(NSNumber*)qNumber isGraded:(BOOL)isGraded {
    
    NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"QuestionCellView" owner:nil options:nil];
    QuestionCellView *cell = [[QuestionCellView alloc] initWithFrame:CGRectMake(0, 0, 350, 400)];
    cell = [views objectAtIndex:0];
    
//    switch (type) {
//        case kFillInTheBlanks:
//            cell.typeLabel.text = @"Fill In The Blanks";
//            break;
//        case kMultipleChoice:
//            cell.typeLabel.text = @"Multiple Choice";
//            break;
//        case kMatchTheFollowing:
//            cell.typeLabel.text = @"Match The Following";
//            break;
//        case kOpenEnded:
//            cell.typeLabel.text = @"Open Ended";
//            break;
//            
//        default:
//            break;
//    }
    cell.clipsToBounds = YES;
    cell.layer.cornerRadius = 8.0;
    cell.typeLabel.text = type;
    cell.typeLabel.edgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    cell.scoreLabel.text = [score stringValue];
    cell.maxScoreLabel.text = [maxScore stringValue];
    cell.questionNumberLabel.text = [qNumber stringValue];
    
    cell.scoreLabel.layer.borderColor = [UIColor blackColor].CGColor;
    cell.scoreLabel.layer.borderWidth = 0.8;
    cell.scoreLabel.backgroundColor = [UIColor colorWithHex:@"#eeeeee" alpha:1.0];
    cell.maxScoreLabel.layer.borderColor = [UIColor blackColor].CGColor;
    cell.maxScoreLabel.layer.borderWidth = 0.8;
    cell.maxScoreLabel.backgroundColor = [UIColor colorWithHex:@"#eeeeee" alpha:1.0];

    cell.questionNumberLabel.layer.borderColor = [UIColor blackColor].CGColor;
    cell.questionNumberLabel.layer.borderWidth = 1.0;
    cell.typeLabel.layer.borderColor = [UIColor blackColor].CGColor;
    cell.typeLabel.layer.borderWidth = 1.0;

    [cell.questionView loadHTMLString:[[NSString stringWithFormat:@"<strong>Question:</strong></br></br>%@", question] stringByAppendingFormat:@"</br></br><strong>Answer:</strong></br></br>%@", answer] baseURL:nil];
    cell.questionView.scrollView.bounces = NO;
    cell.questionView.layer.borderColor = [UIColor blackColor].CGColor;
    cell.questionView.layer.borderWidth = 1.0;
//    cell.questionView.layer.cornerRadius = 4.0;
    
    cell.layer.borderColor = [UIColor blackColor].CGColor;
    cell.layer.borderWidth = 2.0;
    
    cell.scoreBar.barRatio = [score floatValue]/[maxScore floatValue];
    cell.scoreBar.layer.borderColor = [UIColor blackColor].CGColor;
    cell.scoreBar.layer.borderWidth = 0.8;
    
    cell.submissionButton.layer.borderColor = [UIColor blackColor].CGColor;
    cell.submissionButton.layer.borderWidth = 1.0;
    cell.submissionButton.backgroundColor = [UIColor colorWithHex:@"#eeeeee" alpha:1.0];
    cell.submissionButton.layer.cornerRadius = 5.0;
    if (![type isEqualToString:@"Open ended"]) {
        cell.questionView.frame = CGRectMake(CGRectGetMinX(cell.questionView.frame), CGRectGetMinY(cell.questionView.frame), CGRectGetWidth(cell.questionView.frame), CGRectGetMaxY(cell.submissionButton.frame) - CGRectGetMinY(cell.questionView.frame));
        cell.submissionButton.hidden = YES;
    }
    
    cell.gradedLabel.layer.cornerRadius = 4.0;
    cell.gradedLabel.hidden = isGraded;

    return cell;
}



@end
