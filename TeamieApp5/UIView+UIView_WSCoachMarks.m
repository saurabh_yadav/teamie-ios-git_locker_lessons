//
//  UIView+UIView_WSCoachMarks.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/27/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "UIView+UIView_WSCoachMarks.h"

@implementation UIView (UIView_WSCoachMarks)

- (CGRect)rectToMarkInView:(UIView *)view {
    return [self rectToMarkInView:view withInset:UIEdgeInsetsZero];
}

- (CGRect)rectToMarkInView:(UIView *)view withInset:(UIEdgeInsets )inset {
    return UIEdgeInsetsInsetRect([self convertRect:self.bounds toView:view], inset);
}

@end
