//
//  LessonContentViewController.m
//  TeamieApp5
//
//  Created by Raunak on 23/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "LessonContentViewController.h"
#import "LessonPageViewController.h"
#import "LessonAttachmentButtonView.h"
#import "PKRevealController+Hide.h"
#import "LessonPageAttachmentTableViewCell.h"
#import "NSString+FontAwesome.h"
#import "DashedLineView.h"
#import "TMEClient.h"
#import "TMELessonAction.h"
#import "TMEFileAttachment.h"
#import "TMELinkAttachment.h"
#import "TMEVideoAttachment.h"
#import "TMEWebViewController.h"

#define LessonPageAttachmentsCellHeight 45
@interface WebViewLinkController : UIViewController
@property (nonatomic,strong) NSURLRequest* requestURL;
@property (nonatomic,strong) UIWebView* linkWebView;
@property (nonatomic,strong) UIToolbar* toolbar;
@property (nonatomic,strong) UIBarButtonItem* backButton;
@end

//TODO: What is this? Why is this needed? Use TMEWebViewController instead.

@implementation WebViewLinkController
@synthesize requestURL;
@synthesize linkWebView;
@synthesize toolbar;
@synthesize backButton;

- (id)initWithRequestURL:(NSURLRequest*)url {
    self = [super init];
    if (self) {
        // Modify the URL to perform a merge-session request whenever it is opened.
        requestURL = [NSURLRequest requestWithURL:[TMEWebViewController getMergeSessionURL:url.URL]];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = NO;
    
    self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    self.toolbar.tintColor = [UIColor colorWithHex:@"#BE202E" alpha:1.0];
    self.backButton = [[UIBarButtonItem alloc] initWithTitle:TMELocalize(@"button.back") style:UIBarButtonItemStyleBordered target:self action:@selector(backButtonPressed)];
    NSArray* toolbarArray = [NSArray arrayWithObject:self.backButton];
    self.toolbar.items = toolbarArray;
    
    self.linkWebView = [[UIWebView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, 44, self.view.frame.size.width,  self.view.frame.size.height - 44)];

    [self.view addSubview:self.linkWebView];
    [self.view addSubview:self.toolbar];
    [self.linkWebView loadRequest:self.requestURL];
    
    
//    NSString *jsCommand = [NSString stringWithFormat:@"document.body.style.zoom = 0.5;"];
//    [self.linkWebView stringByEvaluatingJavaScriptFromString:jsCommand];
    
}

- (void)backButtonPressed {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

@interface LessonContentViewController ()  {
    BOOL shouldDisplayLabel;
    CGFloat lastContentOffsetY;
    BOOL isToobarAndStatusBarHidden;
}
@property (nonatomic,strong) UIWebView* webView;
@property (nonatomic,strong) UIView* container;
@property (nonatomic,strong) UIScrollView *scrollView;
@property (nonatomic) BOOL didLoadContent;
@property (nonatomic,strong) NSMutableArray *attachments;
@end

@implementation LessonContentViewController

@synthesize lessonID;
@synthesize lessonPage;
@synthesize webView;
@synthesize container;
@synthesize didLoadContent;

- (id)initWithLessonID:(NSNumber*)lid {
    self = [super init];
    if (self) {
        // Custom initialization
        lessonID = lid;
        didLoadContent = NO;
        shouldDisplayLabel = YES;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view.
    if (!self.didLoadContent) {
        [[TMEClient sharedClient]
         request:TeamieUserLessonPageRequest
         parameters:nil
         makeURL:^NSString *(NSString *URL) {
             return [NSString stringWithFormat:URL, self.lessonID];
         }
         loadingMessage:TMELocalize(@"message.loading")
         success:^(NSDictionary *response) {
             self.lessonPage = [TMELessonPage parseDictionary:response];
             self.didLoadContent = YES;
             if (self.lessonPage.body != nil && self.lessonPage.body != (NSString*)[NSNull null] ) {
                 NSString *cssString = @"<style type='text/css'>img {max-width: 99% !important; width: auto; height: auto;}p{word-wrap:break-word;} </style>";
                 NSString* titleString = [NSString stringWithFormat:@"<h3 style='border-bottom: dashed 1px #ccc;padding-bottom: 12px;padding-left: 12px;text-align:center;padding-right: 12px;'>%@</h3>", self.lessonPage.title];
                 NSString* newHtml = [[cssString stringByAppendingString:titleString] stringByAppendingString:self.lessonPage.body];
                 
                 //To prevent zoomed in page because of table, scale to fit and enable scrolling if the page has a table
                 self.webView.scalesPageToFit = self.webView.scrollView.scrollEnabled = [self doesPageContainTable:newHtml];

                 [self.webView stringByEvaluatingJavaScriptFromString:@"document.body.style.zoom = 4.0;"];
                 [self.webView loadHTMLString:newHtml
                                      baseURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] stringForKey:kBaseURL]]];
             }
             
             [self processAttachments];
             [self updateToolbar];
         } failure:nil];
    }
    
//    self.webView = [[UIWebView alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height - 88)];
//    self.webView.delegate = self;
//    [self.view addSubview:self.webView];
    
    self.scrollView = [[UIScrollView alloc]initWithFrame:self.view.bounds];
    [self.view addSubview:self.scrollView];
    
    self.container = [[UIView alloc]initWithFrame:self.view.bounds];
    [self.scrollView addSubview:self.container];
    
    self.webView = [[UIWebView alloc]initWithFrame:self.container.bounds];
    self.webView.delegate = self;
    self.webView.scrollView.scrollEnabled = NO;
    [self.container addSubview:self.webView];
    
    self.webView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleTap:)];
    tapGesture.numberOfTapsRequired = 1;
    [self.webView addGestureRecognizer:tapGesture];
    tapGesture.delegate = self;
    
    self.scrollView.delegate = self;
    isToobarAndStatusBarHidden = NO;
    lastContentOffsetY = 0;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self showToolBarAndStatusBar];
    [self updateToolbar];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (didLoadContent) {
        ((LessonPageViewController*)self.parentViewController.parentViewController).attachmentButton.enabled = !([self.lessonPage.files count] == 0 && [self.lessonPage.links count] == 0);
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self showToolBarAndStatusBar];
}

- (void)webViewDidFinishLoad:(UIWebView *)myWebview {
    [TeamieGlobals dismissActivityLabels];
    shouldDisplayLabel = NO;
    
    //to ensure all the modifications only happens after the webview is fully loaded
    if (myWebview.loading == YES) {
        return;
    }
    
    //Set the webview's size to be the same as its content size
    
//    CGSize contentSize = myWebview.scrollView.contentSize;
//    CGSize viewSize = self.view.bounds.size;
//    
//    float rw = viewSize.width / contentSize.width;
//    myWebview.scrollView.minimumZoomScale = rw;
//
//    myWebview.scrollView.maximumZoomScale = rw;
//
//    myWebview.scrollView.zoomScale = rw;
    
    CGRect frame = self.webView.frame;
    frame.size = self.webView.scrollView.contentSize;
    self.webView.frame = frame;
    
    //Set the container to the new size
    CGRect cframe = self.container.frame;
    cframe.size = self.webView.scrollView.contentSize;
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, cframe.size.height, self.view.frame.size.width, LessonPageAttachmentsCellHeight * self.attachments.count + 30)];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.scrollEnabled = NO;
    
    [self.container addSubview:tableView];
    cframe.size.height += tableView.frame.size.height;
    self.container.frame = cframe;
    
    //set the outter scrollview contentSize
    self.scrollView.contentSize = self.container.frame.size;
    self.scrollView.contentInset = UIEdgeInsetsMake(20, 0, 44, 0);
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [TeamieGlobals dismissActivityLabels];
    shouldDisplayLabel = NO;
}

- (BOOL) webView:(UIWebView *)webView2 shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
        if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        WebViewLinkController* webViewController = [[WebViewLinkController alloc] initWithRequestURL:request];
        [self presentViewController:webViewController animated:YES completion:nil];
        return NO;
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return YES;
}
- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return YES;
}

//- (void)displayAttachmentsForEvent:(UIEvent *)event {
//    if(!self.attachmentsController) {
//        LessonAttachmentsViewController* contentViewController = [[LessonAttachmentsViewController alloc] initWithFileAttachments:self.lessonPage.attachedFiles linkAttachments:self.lessonPage.attachedLinks];
//        contentViewController.delegate = self;
//        
//        Class popoverClass;
//        CGFloat maxWidth, maxHeight;
//        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
//            popoverClass = [UIPopoverController class];
//            maxWidth = 350;
//            maxHeight = 600;
//        }
//        else {
//            popoverClass = [WEPopoverController class];
//            maxWidth = 280;
//            maxHeight = 360;
//        }
//        
//        self.attachmentsController = [[popoverClass alloc] initWithContentViewController:contentViewController];
//        
//        NSInteger count = [self.lessonPage.attachedFiles count] + [self.lessonPage.attachedLinks count];
//        CGFloat height = count * 44;
//        if ([self.lessonPage.attachedFiles count] > 0) {
//            height += 20.0;
//        }
//        if ([self.lessonPage.attachedLinks count] > 0) {
//            height += 20.0;
//        }
//        if (height > maxHeight) {
//            height = maxHeight;
//        }        
//        [self.attachmentsController setPopoverContentSize:(CGSizeMake(maxWidth, height))];
//        self.attachmentsController.delegate = self;
//        [self.attachmentsController presentPopoverFromRect:[[event.allTouches anyObject] view].frame inView:((LessonPageViewController*)self.parentViewController.parentViewController).toolbar permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
//    }
//    else {
//        [self.attachmentsController dismissPopoverAnimated:YES];
//        self.attachmentsController= nil;
//    }
//}

- (void)lessonAttachmentsControllerDidSelectOptionWithData:(NSDictionary *)option {
    // Use the preview URL preferably. If no preview URL is available then use the default URL.
    NSString* urlToOpen = [option objectForKey:@"preview"];
    if ((!urlToOpen) || [[option valueForKey:@"href"] rangeOfString:@"youtube"].location != NSNotFound) { // If it's a youtube URL then use the href instead of preview.
        urlToOpen = [option valueForKey:@"href"];
    }
    
    WebViewLinkController* webViewController = [[WebViewLinkController alloc] initWithRequestURL:[NSURLRequest requestWithURL:[NSURL URLWithString:urlToOpen]]];
    [self presentViewController:webViewController animated:YES completion:nil];
}

- (void)attachmentFilesButtonPressed:(UIButton *)button
{
    TMEFileAttachment *option = self.lessonPage.files[button.tag];
    NSString* urlToOpen = option.preview;
    WebViewLinkController* webViewController = [[WebViewLinkController alloc] initWithRequestURL:[NSURLRequest requestWithURL:[NSURL URLWithString:urlToOpen]]];
    [self presentViewController:webViewController animated:YES completion:nil];
//    tempDict = [self.fileAttachments objectAtIndex:button.tag];
//    cell.textLabel.text = [[NSURL URLWithString:(NSString*)[tempDict objectForKey:@"href"]] lastPathComponent];
//    tempDict = [self.linkAttachments objectAtIndex:indexPath.row];
//    cell.textLabel.text = [tempDict objectForKey:@"title"];
//    cell.detailTextLabel.text = [tempDict objectForKey:@"description"];
}

- (void)attachmentLinksButtonPressed:(UIButton *)button
{
    TMELinkAttachment *option = self.lessonPage.links[button.tag];
    WebViewLinkController* webViewController = [[WebViewLinkController alloc] initWithRequestURL:[NSURLRequest requestWithURL:option.href]];
    [self presentViewController:webViewController animated:YES completion:nil];
}

#pragma mark - Gesture Callback functions
- (void)handleTap:(id)sender
{
    [self toggleToobarAndStatusBar];
    isToobarAndStatusBarHidden = isToobarAndStatusBarHidden ^ YES;//toggle the boolean value
}

#pragma mark - UIScrollViewDelegate Methods
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    //If the lesson page is moving around the "bouncing area"
    if (lastContentOffsetY <= 0 && scrollView.contentOffset.y <= 0) {
        if (isToobarAndStatusBarHidden == YES) {
            [self showToolBarAndStatusBar];
            isToobarAndStatusBarHidden = NO;
        }
        return;
    }
    
    //if the page is scrolled down to the bottom of the page, then the tool bar and status
    //bar should be shown
    CGFloat height = scrollView.frame.size.height;
    
    CGFloat contentYoffset = scrollView.contentOffset.y;
    
    CGFloat distanceFromBottom = scrollView.contentSize.height - contentYoffset;
    
    if(distanceFromBottom <= height)
    {
        if (isToobarAndStatusBarHidden == YES) {
            [self showToolBarAndStatusBar];
            isToobarAndStatusBarHidden = NO;
        }
        
        return;
    }
    
    
    /*
     If user scrolling down to a certain distance such as 10 in this case,
     then we hide toolbar and status bar if it is scrolling up, then we show the toolbar
     and status bar 
     */
    if (lastContentOffsetY + 10 < scrollView.contentOffset.y) {
        if (isToobarAndStatusBarHidden == NO) {
            [self hideToolBarAndStatusBar];
            isToobarAndStatusBarHidden = YES;
        }
    }else if (lastContentOffsetY > scrollView.contentOffset.y){
        if (isToobarAndStatusBarHidden == YES) {
            [self showToolBarAndStatusBar];
            isToobarAndStatusBarHidden = NO;
        }
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView
{
    lastContentOffsetY = scrollView.contentOffset.y;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    lastContentOffsetY = scrollView.contentOffset.y;
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    lastContentOffsetY = scrollView.contentOffset.y;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    lastContentOffsetY = scrollView.contentOffset.y;
}

- (void)toggleToobarAndStatusBar
{
    if (isToobarAndStatusBarHidden == YES) {
        [self showToolBarAndStatusBar];
    }else{
        [self hideToolBarAndStatusBar];
    }
}

- (void)hideToolBarAndStatusBar
{
    [self.delegate hideToolBar];
    [self.revealController hideStatusBar:YES];
}

- (void)showToolBarAndStatusBar
{
    [self.delegate showToolBar];
    [self.revealController hideStatusBar:NO];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.attachments.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    
    LessonPageAttachmentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LessonPageAttachmentTableViewCell" owner:self options:nil];
        
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell = (LessonPageAttachmentTableViewCell *)currentObject;
                break;
            }
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSDictionary *dict = [self.attachments objectAtIndex:indexPath.row];
    
    cell.icon.image = [TeamieUIGlobals defaultPicForPurpose:dict[@"icon"] withSize:15 andColor:[TeamieUIGlobalColors defaultTextColor]];
    cell.icon.contentMode = UIViewContentModeCenter;
    NSString *attachmentTitle = [self truncateName:[dict objectForKey:@"title"]];
    cell.titleLabel.frame = CGRectMake(42, 11, self.view.frame.size.width, 21);
    
    cell.titleLabel.preferredMaxLayoutWidth = 400.0f;
    cell.titleLabel.text = attachmentTitle;
    
    DashedLineView *dashedLine = [[DashedLineView alloc]initWithFrame:CGRectMake(0, LessonPageAttachmentsCellHeight, self.view.frame.size.width, 1)];
    dashedLine.backgroundColor = [UIColor clearColor];
    [cell addSubview:dashedLine];
    
    if (indexPath.row == 0) {
        DashedLineView *lastDashedLine = [[DashedLineView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 1)];
        lastDashedLine.backgroundColor = [UIColor clearColor];
        [cell addSubview:lastDashedLine];
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return LessonPageAttachmentsCellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *dict = self.attachments[indexPath.row];
    [TeamieGlobals openInappBrowserWithURL:[NSURL URLWithString:dict[@"href"]] fromResponder:self withTitle:dict[@"title"]];
}

#pragma mark - Private Methods
- (void)processAttachments
{
    self.attachments = [[NSMutableArray alloc]init];
    
    for (TMEFileAttachment *file in self.lessonPage.files) {
        [self.attachments addObject:@{
          @"icon":@"file",
          @"title":file.title,
          @"href":[file.href absoluteString]
          }];
    }
    
    for (TMELinkAttachment *link in self.lessonPage.links) {
        [self.attachments addObject:@{
          @"icon":@"link",
          @"title":link.title,
          @"href":[link.href absoluteString]
          }];
    }
    
    for (TMEVideoAttachment *video in self.lessonPage.videos) {
        [self.attachments addObject:@{
          @"icon":@"video",
          @"title":video.title,
          @"href":[video.href_mp4?:video.href performSelector:@selector(absoluteString)],
          }];
    }
}

- (void)processAccess
{
    NSMutableDictionary *accessDict = [[NSMutableDictionary alloc]init];
    [accessDict setObject:[NSNumber numberWithBool:NO] forKey:@"edit_access"];
    [accessDict setObject:[NSNumber numberWithBool:NO] forKey:@"delete_access"];
    [accessDict setObject:[NSNumber numberWithBool:NO] forKey:@"toggle_access"];

    for (TMELessonAction *action in self.lessonPage.actions) {
        NSString *actionName = action.name;
        if ([actionName isEqualToString:@"edit"]) {
            [accessDict setObject:[NSNumber numberWithBool:YES] forKey:@"edit_access"];
        }else if ([actionName isEqualToString:@"delete"]) {
            [accessDict setObject:[NSNumber numberWithBool:YES] forKey:@"delete_access"];
        }else if ([actionName isEqualToString:@"toggle"]) {
            [accessDict setObject:[NSNumber numberWithBool:YES] forKey:@"toggle_access"];
        }
    }
    
    [self.delegate setAccess:accessDict];
}

- (void)updateEnableButtonColor
{
    NSString *colorHex;
    if ([self.lessonPage.status boolValue] == YES) {
        colorHex = publishedColorHex;
    }else{
        colorHex = draftedColorHex;
    }
    [self.delegate setEnableButtonColor:colorHex];
}

- (void)updatePublishStatusLabel
{
    PublishStatus publishStatus;
    if ([self.lessonPage.status boolValue] == YES) {
        publishStatus = PublishStatusPublished;
    }
    else{
        publishStatus = PublishStatusDrafted;
    }
    
    [self.delegate setPublishStatus:publishStatus];

}

- (void) updateToolbar
{
    [self processAccess];
    [self updateEnableButtonColor];
//    [self updatePublishStatusLabel];
}

- (void)displayAttachmentsForEvent:(UIEvent*)event {
    // TODO: Hey you just saw you. And this is crazy. But here's my suggestion. So write me maybe.
}

- (BOOL)doesPageContainTable:(NSString *)html {
    NSError *error;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"<\\s*table[^>]*>"
                                                                           options:NSRegularExpressionDotMatchesLineSeparators|NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    NSRange firstMatch = [regex rangeOfFirstMatchInString:html
                                                  options:0
                                                    range:NSMakeRange(0, html.length)];
    return firstMatch.location != NSNotFound;
}

-(NSString *) truncateName:(NSString *)StringToTruncate{
    NSString *origString;
    NSUInteger originalStrinLength = StringToTruncate.length;
    NSUInteger truncationPoint;
    NSUInteger firstPartLength;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        truncationPoint = 70;
        firstPartLength = 33;
    }
    else{
        truncationPoint = 36;
        firstPartLength = 14;
    }
    if([StringToTruncate length]>truncationPoint)
    {
        NSString *firstString = [StringToTruncate substringToIndex:firstPartLength];
        NSString *secondString = [StringToTruncate substringFromIndex:originalStrinLength-firstPartLength];
        origString = [NSString stringWithFormat:@"%@...%@",firstString, secondString];
        return origString;
    }
    else{
        return StringToTruncate;
    }
}

@end
