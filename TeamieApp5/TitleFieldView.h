//
//  TitleFieldView.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 24/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleFieldView : UIView
@property (weak, nonatomic) IBOutlet UITextView *titleTextView;

@end
