//
//  QuizSimpleObject.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TMEQuizSimple.h"
#import "TMEClassroomAlt.h"

BOOL isDraft;

@implementation TMEQuizSimple

#pragma mark - Mappings

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"qid": @"qid",
        @"title": @"title",
        @"type": @"type",
        @"webUrl": @"web_url",
        @"numVotes": @"votes.five-star.count",
        @"deadlineDate": @"deadline.till",
        @"deadlineTimezone": @"deadline.timezone",
        @"publishDate": @"published.on",
        @"totalPeople": @"submission.total_people",
        @"quizTakers": @"submission.quiz_takers",
        @"hasAccess": @"submission.access",
        @"isGraded": @"submission.graded",
        @"classrooms": @"classrooms",
        @"authorName": @"user.real_name",
        @"numberOfQuestions": @"data.num_questions",
        @"weightage": @"data.weightage",
        @"maximumAttempts": @"data.max_attempts",
        @"duration": @"data.duration",
        @"canSubmitLate": @"data.late_submission.allowed",
        @"lateTimeAllowed": @"data.late_submission.time_allowed",
        @"numberOfAttempts": @"submission_status.num_attempts",
        @"totalScore": @"data.max_score",
    };
}

#pragma mark - Mapping Transformations

+ (NSValueTransformer *)qidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)deadlineDateJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)publishDateJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)classroomsJSONTransformer {
    return [TMEModel listJSONTransformer:TMEClassroomAlt.class];
}

+ (NSValueTransformer *)weightageJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)maximumAttemptsJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)durationJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)canSubmitLateJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)lateTimeAllowedJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

#pragma mark - Private Methods

- (NSString *)deadLineDateString {
    if (_deadlineDate) {
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        format.dateFormat       = @"MMM dd HH:mm";
        return [format stringFromDate:self.deadlineDate];
    }
    else {
        return @"";
    }
}

- (BOOL)isDraft {
    if (_publishDate) {
        isDraft = NO;
    }
    else {
        isDraft = YES;
    }
    return isDraft;
}

- (NSString *)type {
    if (_type) {
        if ([_type isEqualToString:@"assignment"]) {
            _type = TMELocalize(@"assessment.type.online");
        }
        else if ([_type isEqualToString:@"assignment_clone"]) {
            _type = TMELocalize(@"assessment.type.assignment");
        }
        else if ([_type isEqualToString:@"offline"]) {
            _type = TMELocalize(@"assessment.type.offline");
        }
    }
    return _type;
}

- (NSString *)classroomsAsStringForClassroomId:(NSNumber *)nid {
    NSMutableString *classroomLabelString = [[NSMutableString alloc] init];
    if (_classrooms.count > 1) {
        for (TMEClassroom *classroom in _classrooms) {
            NSNumber *classroomId = classroom.nid;
            if (classroomId.longLongValue == nid.longLongValue) {
                NSString *primaryTitle = classroom.name;
                [classroomLabelString appendString:primaryTitle];
            }
        }
        NSString *secondaryTitle = [NSString stringWithFormat:@" + %u more", _classrooms.count - 1];
        [classroomLabelString appendString:secondaryTitle];
        
        return classroomLabelString;
    }
    else {
        TMEClassroom *classroom = _classrooms.firstObject;
        return classroom.name;
    }
}

- (NSString *)numberOfQuestionsString {
    if ([_type isEqualToString:TMELocalize(@"assessment.type.offline")]) {
        // if quiz type is offline, there are not questions specified
        return @"0 questions";
    }
    else if (_numberOfQuestions.longValue > 1) {
        return [NSString stringWithFormat:@"%@ questions", _numberOfQuestions];
    }
    else {
        return @"1 question";
    }
}

- (NSString *)marksString {
    if (_totalScore) {
        return [NSString stringWithFormat:@"%@ %@", _totalScore, TMELocalize(@"assessment.score.plural")];
    }
    else if ([_totalScore  isEqual:@1]) {
        return [NSString stringWithFormat:@"%@ %@", _totalScore, TMELocalize(@"assessment.score.singular")];
    }
    else {
        return [NSString stringWithFormat:@"0 %@", TMELocalize(@"assessment.score.plural")];
    }
}

- (NSString *)durationString {
    if (!_duration) {
        NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
        [f setNumberStyle:NSNumberFormatterNoStyle];
        NSNumber * timeInSeconds = _duration;
        NSNumber *timeInMinutes = [NSNumber numberWithInteger:([timeInSeconds integerValue] / 60)];
        NSString *timeString = [NSString stringWithFormat:@"%@ minutes", timeInMinutes];
        
        return timeString;
    }
    else {
        return @"No time limit";
    }
}

- (NSString *)attemptCountString {
    if ([_maximumAttempts isEqualToNumber:@-1]) {
        return @"unlimited attempts";
    }
    
    NSString *attempt = @"attempt";
    if ([_maximumAttempts integerValue] > 1) {
        attempt = @"attempts";
    }
    
    if (_hasAccess.boolValue) {
        if (_maximumAttempts) {
            return [NSString stringWithFormat:@"%@ %@", _maximumAttempts, attempt];
        }
        else {
            return attempt;
        }
    }
    else {
        if (_maximumAttempts) {
            return [NSString stringWithFormat:@"%@/%@ %@", _numberOfAttempts, _maximumAttempts, attempt];
        }
        else {
            return attempt;
        }
    }
}

- (NSString *)submissionsMadeString {
    return [NSString stringWithFormat:@"%@/%@ submitted", _quizTakers, _totalPeople];
}

@end
