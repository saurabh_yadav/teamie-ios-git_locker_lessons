//
//  TMEPostViewController.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TMEPost.h"

@interface TMEPostViewController : UIViewController

@property (nonatomic, strong, readonly) NSNumber *postID;
@property (nonatomic, strong, readonly) NSNumber *commentID;
@property (nonatomic, strong, readonly) NSNumber *replyID;
@property (nonatomic, strong, readonly) TMEPost *post;

+ (instancetype)postWithId:(NSNumber *)postID;
+ (instancetype)postWithId:(NSNumber *)postID openComment:(BOOL)open;
+ (instancetype)postWithId:(NSNumber *)postID atComment:(NSNumber *)commentID;
+ (instancetype)postWithId:(NSNumber *)postID atReply:(NSNumber *)replyID;

@end
