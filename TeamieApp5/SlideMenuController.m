//
//  SlideMenuController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/6/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "SlideMenuController.h"
#import "TMEClient.h"
#import "TMEClassroom.h"
#import "TMESlideMenuCell.h"
#import "TMEUser.h"
#import "TMESiteInfo.h"
#import "TMESlideMenuItem.h"
#import "TMENewsfeedViewController.h"
#import "TMEClassroomNewsfeedViewController.h"
#import "UserProfileViewController.h"
#import "BulletinBoardViewController.h"
#import "TMEUpcomingEventsViewController.h"
#import "TeamieAppDelegate.h"
#import "LSViewController.h"
#import "TMEMessagesViewController.h"

#import "PKRevealController.h"
#import "UIImage-Extensions.h"
#import "NSData+Base64.h"
#import "UIView+TMEEssentials.h"

@interface SlideMenuController()

@property (nonatomic, strong) NSNumber *unreadMessagesCount;

@end

@implementation SlideMenuController

static NSString * const kCellIdentifier = @"Cell";
static NSInteger kLogoutTag = 1;
static NSInteger kSwitchUserTag = 2;

#pragma mark - View Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.tableView registerClass:TMESlideMenuCell.class forCellReuseIdentifier:kCellIdentifier];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.menuSections = [[NSMutableArray alloc] initWithArray:@[@"", TMELocalize(@"menu.classrooms"), TMELocalize(@"menu.settings")]];
    self.menuItems = [[NSMutableArray alloc] initWithArray:@[@[],@[],@[]]];
    self.userClassrooms = [NSMutableArray new];
    self.unreadMessagesCount = @0;

    //This line is to ensure there is no freak case where there are classrooms left over from the last session
    [TMEClassroom deleteFromUserDefaults];
    
    //Has no network dependency
    [self loadSettingsSection];
    
    //Loads the header and first section after calling site info
    [self loadSiteInfo];
    
    //Loads the classroom after getting classroom requests
    [self refreshClassrooms];
    
    //Reloads the first section after loading profile for current user
    [self loadUserProfile];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    //Set the count for the notifications
    TMESlideMenuItem *notificationsItem = (TMESlideMenuItem *)self.menuItems[0][2];
    NSNumber *unreadCount = ((TeamieRevealController *)self.revealController).unreadNotificationsCount;
    notificationsItem.badgeText = [unreadCount intValue] > 0? [unreadCount stringValue] : @"";

    [self refreshClassrooms];
    [self refreshMessagesCount];
}

#pragma mark - Alert Delegate methods
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView.tag == kLogoutTag) {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            // User cancelled the logout request
        }
        else { // The user definitely wants to logout
            [[TMEClient sharedClient] logout:^{
                [((TeamieAppDelegate *)[UIApplication sharedApplication].delegate) transitionToLoginScreen];
            }];
        }
    }
    else if (alertView.tag == kSwitchUserTag) {
        if (buttonIndex == [alertView cancelButtonIndex]) {
            // User cancelled the switch user request
        }
        else {
            NSDictionary *siteInfo = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"site_info"];
            [[TMEClient sharedClient]
             login:[TeamieGlobals prepareLoginParametersWithEmail:[alertView textFieldAtIndex:0].text
                                                             site:[[NSURL alloc] initWithString:siteInfo[@"base_url"]].host]
             success:^() {
                 [(TeamieAppDelegate *)[[UIApplication sharedApplication] delegate] transitionToHomeScreen];
             }
             failure:^(NSError *error) {
                 [TeamieGlobals addTSMessageInController:self
                                                   title:TMELocalize(@"error.login-failed")
                                                 message:error.userInfo[TMEJSONResponseErrorKey][@"error_description"]
                                                    type:@"error"
                                                duration:0
                                            withCallback:nil];
             }];
        }
    }
}

#pragma mark - NSNotification methods

- (void)receiveRefreshTokenNotification:(NSNotification*)notification {
    // this method gets called after the refresh token generates a new access token
    [self refreshClassrooms];
}

#pragma mark - UITableViewDataSource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TMESlideMenuCell* cell = [self.tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    [cell setData:self.menuItems[indexPath.section][indexPath.row]];
    if (indexPath.row > 0) {
        [cell addBorderToTop:YES bottom:NO left:NO right:NO width:0.5f];
    }

    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.menuSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.menuItems[section] count];
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return self.menuSections[section];
}

- (UIView*)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return nil; //First section has no header
    }
    
    UIView * headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 20.0)];
    [headerView setBackgroundColor:[TeamieUIGlobalColors navigationBarBackgroundColor]];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(11.0, 1.0, self.tableView.bounds.size.width, 20.0)];
    label.text = [self.tableView.dataSource tableView:tableView titleForHeaderInSection:section];
    label.textColor = [TeamieUIGlobalColors navigationBarTextColor];
    label.font = [TeamieGlobals appFontFor:@"tableviewSectionHeader"];
    label.backgroundColor = [UIColor clearColor];
    [headerView addSubview:label];
    
    return headerView;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return section == 0? 0 : 22; //First section zero height
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TMESlideMenuItem *item = self.menuItems[indexPath.section][indexPath.row];
    
    UIViewController *currentViewController = ((UINavigationController *)self.revealController.frontViewController).topViewController;
    UIViewController *frontViewController;
    switch (item.menuType) {
        case TMESlideMenuTypeProfile: {
            TMETrackEvent(@"user_menu", @"tap", @"user_profile");
            if ([currentViewController isMemberOfClass:UserProfileViewController.class]) {
                [self.revealController showViewController:self.revealController.frontViewController];
                return;
            }
            frontViewController = [[UserProfileViewController alloc] initWithUserID:[[TMEUser currentUser].uid integerValue]];
            break;
        }
        case TMESlideMenuTypeNewsfeed: {
            TMETrackEvent(@"user_menu", @"tap", @"newsfeed");
            if ([currentViewController isMemberOfClass:TMENewsfeedViewController.class]) {
                [self.revealController showViewController:self.revealController.frontViewController];
                return;
            }
            frontViewController = [TMENewsfeedViewController newsfeed];
            break;
        }
        case TMESlideMenuTypeNotifications: {
            TMETrackEvent(@"user_menu", @"tap", @"notifications");
            if ([currentViewController isMemberOfClass:BulletinBoardViewController.class]) {
                [self.revealController showViewController:self.revealController.frontViewController];
                return;
            }
            frontViewController = [[BulletinBoardViewController alloc] init];
            break;
        }
        case TMESlideMenuTypeUpcomingEvents: {
            TMETrackEvent(@"user_menu", @"tap", @"calendar");
            if ([currentViewController isMemberOfClass:TMEUpcomingEventsViewController.class]) {
                [self.revealController showViewController:self.revealController.frontViewController];
                return;
            }
            frontViewController = [TMEUpcomingEventsViewController new];
            break;
        }
        case TMESlideMenuTypeMessages: {
            TMETrackEvent(@"user_menu", @"tap", @"messages");
            if ([currentViewController isMemberOfClass:TMEMessagesViewController.class]) {
                [self.revealController showViewController:self.revealController.frontViewController];
                return;
            }
            frontViewController = [TMEMessagesViewController new];
            break;
        }
        case TMESlideMenuTypeLockerLessons: {
            if ([currentViewController isMemberOfClass:LSViewController.class]) {
                [self.revealController showViewController:self.revealController.frontViewController];
                return;
            }
            frontViewController = [[LSViewController alloc] initWithtitle:TMELocalize(@"menu.locker_lessons")];
            break;
        }
        case TMESlideMenuTypeClassroomNewsfeed: {
            TMETrackEvent(@"user_menu", @"tap", @"classroom");
            if ([currentViewController isMemberOfClass:TMEClassroomNewsfeedViewController.class] &&
                [((TMEClassroomNewsfeedViewController *)currentViewController).classroom isEqual:item.data]) {
                [self.revealController showViewController:self.revealController.frontViewController];
                return;
            }
            frontViewController = [TMEClassroomNewsfeedViewController newsfeedForClassroom:(TMEClassroom *)item.data];
            break;
        }
        case TMESlideMenuTypeSwitchUser: {
            UIAlertView *switchAlert = [[UIAlertView alloc]
                                        initWithTitle:TMELocalize(@"message.enter-email")
                                        message:nil
                                        delegate:self
                                        cancelButtonTitle:TMELocalize(@"button.cancel")
                                        otherButtonTitles:TMELocalize(@"button.ok"), nil];
            switchAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
            switchAlert.tag = kSwitchUserTag;
            [switchAlert show];
            return;
        }
        case TMESlideMenuTypeLogout: {
            UIAlertView *logoutAlert = [[UIAlertView alloc]
                                        initWithTitle:TMELocalize(@"message.confirm-action")
                                        message:TMELocalize(@"message.confirm-logout-action")
                                        delegate:self
                                        cancelButtonTitle:TMELocalize(@"button.sorry")
                                        otherButtonTitles:TMELocalize(@"button.logout"), nil];
            logoutAlert.tag = kLogoutTag;
            [logoutAlert show];
            return;
        }
        case TMESlideMenuTypeNull: {
            return;
        }
    }
    UINavigationController* frontNavigationViewController = [[UINavigationController alloc] initWithRootViewController:frontViewController];
    frontNavigationViewController.navigationBar.translucent = NO;
    self.revealController.frontViewController = frontNavigationViewController;
    [self.revealController showViewController:frontViewController animated:YES completion:nil];
}

#pragma mark - LoadItems

- (void)setLogoHeader {
    NSDictionary *info = [[NSUserDefaults standardUserDefaults] dictionaryForKey:SITE_INFO_KEY];
    if (info == nil) return;
    
    UIView* infoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 120)];
    UIImageView* logoView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 240, 80)];
    logoView.image = [[UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:info[@"logo_path"]]]] imageByScalingProportionallyToSize:CGSizeMake(240, 80)];
    
    UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(10, 90, 240, 20)];
    if (info[@"site_name"] != nil) {
        label.text = info[@"site_name"];
    }
    else if ([info[@"base_url"] rangeOfString:@"https://"].location != NSNotFound) {
        NSRange range = [info[@"base_url"] rangeOfString:@"https://"];
        label.text = [info[@"base_url"] substringFromIndex:(range.location + range.length)];
    }
    else if ([info[@"base_url"] rangeOfString:@"http://"].location != NSNotFound) {
        NSRange range = [info[@"base_url"] rangeOfString:@"http://"];
        label.text = [info[@"base_url"] substringFromIndex:(range.location + range.length)];
    }
    else {
        label.text = info[@"base_url"];
    }
    
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 8/[UIFont labelFontSize];
    label.textAlignment = NSTextAlignmentCenter;
    
    [infoView addSubview:logoView];
    [infoView addSubview:label];
    
    self.tableView.tableHeaderView = infoView;
    [self.tableView reloadData];
}

- (void)loadFirstSection {
    TMESlideMenuItem *messagesItem = [TMESlideMenuItem item:TMESlideMenuTypeMessages text:TMELocalize(@"menu.messages") icon:@"message"];
    messagesItem.badgeText = [self.unreadMessagesCount integerValue] > 0 ? [self.unreadMessagesCount stringValue] : nil;

    NSMutableArray *items = [NSMutableArray arrayWithArray:@[[TMESlideMenuItem item:TMESlideMenuTypeProfile text:TMELocalize(@"menu.profile") icon:@"profile"],
                                                             [TMESlideMenuItem item:TMESlideMenuTypeNewsfeed text:TMELocalize(@"menu.newsfeed") icon:@"thought"],
                                                             [TMESlideMenuItem item:TMESlideMenuTypeNotifications text:TMELocalize(@"menu.notifications") icon:@"bulletin"],
                                                             messagesItem,
                                                             [TMESlideMenuItem item:TMESlideMenuTypeUpcomingEvents text:TMELocalize(@"menu.calendar") icon:@"calendar"],
                                                             [TMESlideMenuItem item:TMESlideMenuTypeLockerLessons text:TMELocalize(@"menu.locker_lessons") icon:@"lesson"]]];
    
    if ([TMEUser currentUser].uid) {
        ((TMESlideMenuItem *)items[0]).URL = [TMEUser currentUser].user_profile_image.path;
        ((TMESlideMenuItem *)items[0]).text = [TMEUser currentUser].realName;
    }
    
    self.menuItems[0] = items;
    [self.tableView reloadData];
}

- (void)loadClassroomsSection {
    // Sort the classrooms in alphabetical order
    NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
    NSArray* sortedResponse = [[TMEClassroom getFromUserDefaults] sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
    
    //At the start, set the classrooms section to have one 'Loading ...' item
    self.menuItems[1] = @[[TMESlideMenuItem item:TMESlideMenuTypeNull text:@"Loading ..." icon:@"info"]];
    
    // Put in the new values of classrooms into the array
    NSMutableArray* classroomEntries = [NSMutableArray new];
    for (TMEClassroom* classroom in sortedResponse) {        // add classrooms one by one
        NSString *badgeText;
        
        if ([classroom.freshThoughts integerValue] > 0) {
            badgeText = [classroom.freshThoughts stringValue];
        }
        NSString *truncatedName = [self truncateName:classroom.name];
        TMESlideMenuItem *item = [TMESlideMenuItem item:TMESlideMenuTypeClassroomNewsfeed text:truncatedName icon:@"classroom"];
        item.badgeText = badgeText;
        item.data = classroom;

        // Add the textItem to an array, which will finally be added to the table
        [classroomEntries addObject:item];
    }
    
    if ([classroomEntries count] == 0) {
        [classroomEntries addObject:[TMESlideMenuItem item:TMESlideMenuTypeNull text:@"No classrooms to be displayed" icon:@"info"]];
    }
    self.menuItems[1] = [classroomEntries copy];
    [self.tableView reloadData];
}

- (void)loadSettingsSection {
//IMPORTANT: This switch user feature is only for debug use now. DO NOT remove this condition
#if DEBUG
    self.menuItems[2] = @[[TMESlideMenuItem item:TMESlideMenuTypeSwitchUser text:TMELocalize(@"menu.switch-user") icon:@"switch_user"],
                          [TMESlideMenuItem item:TMESlideMenuTypeLogout text:TMELocalize(@"menu.logout") icon:@"logout"]];
#else
    self.menuItems[2] = @[[TMESlideMenuItem item:TMESlideMenuTypeLogout text:TMELocalize(@"menu.logout") icon:@"logout"]];
#endif
}

#pragma mark - Network Requests
- (void)refreshClassrooms {
    [[TMEClient sharedClient]
     request:TeamieUserClassroomsRequest
     parameters:@{@"show_permissions": @YES}
     loadingMessage:nil
     success:^(NSDictionary *response) {
         [TMEClassroom storeToUserDefaults:[TMEClassroom parseList:response[@"classroom"]?:response]];
         [self loadClassroomsSection];
     } failure:nil];
}

- (void)refreshMessagesCount {
    [[TMEClient sharedClient]
     request:TeamieUserMessagesRequest
     parameters:@{@"items_per_page": @0}
     loadingMessage:nil
     success:^(NSDictionary *response) {
         self.unreadMessagesCount = response[@"unread"];
         [self loadFirstSection];
     } failure:nil];
}

- (void)loadUserProfile {
    [[TMEClient sharedClient]
     getUserProfile:nil
     uid:[TMEUser currentUser].uid
     loadingMessage:nil
     success:^(TMEUserProfile *user) {
         [self loadFirstSection];
     } failure:nil];
}

- (void)loadSiteInfo {
    [[TMEClient sharedClient]
     request:TeamieUserSiteInfoRequest
     parameters:nil
     loadingMessage:nil
     success:^(NSDictionary *response) {
         if (response[@"whitelist_terms"]) {
             [[NSUserDefaults standardUserDefaults] setObject:response[@"whitelist_terms"] forKey:WHITELIST_TERMS_KEY];
         }
         if (response[@"amazon_s3"]) {
             [[NSUserDefaults standardUserDefaults] setObject:response[@"amazon_s3"] forKey:S3_KEY];
         }
         if (response[@"site_info"]) {
             [[NSUserDefaults standardUserDefaults] setObject:response[@"site_info"] forKey:SITE_INFO_KEY];
         }
         self.menuSections = [[NSMutableArray alloc] initWithArray:@[@"", TMELocalize(@"menu.classrooms"), TMELocalize(@"menu.settings")]];
         [self loadFirstSection];
         [self setLogoHeader];
     } failure:nil];
}

#pragma mark - Misc.

-(NSString *) truncateName:(NSString *)StringToTruncate{
    NSString *origString;
    NSUInteger originalStrinLength = StringToTruncate.length;
    
    if([StringToTruncate length]>30)
    {
        NSString *firstString = [StringToTruncate substringToIndex:14];
        NSString *secondString = [StringToTruncate substringFromIndex:originalStrinLength-13];
        origString = [NSString stringWithFormat:@"%@...%@",firstString, secondString];
        return origString;
    }
    else{
        return StringToTruncate;
    }
}

@end
