//
//  LessonInfoCollapseClickCell.m
//  LS2
//
//  Created by Wei Wenbo on 3/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import "LessonInfoCollapseClickCell.h"
@interface LessonInfoCollapseClickCell()
@end
@implementation LessonInfoCollapseClickCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (LessonInfoCollapseClickCell *)newCollapseClickCellWithTitle:(NSString *)title index:(int)index content:(UIView *)content {
    NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"LessonInfoCollapseClickCell" owner:nil options:nil];
    CGFloat width = (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad)?768:320;
    LessonInfoCollapseClickCell *cell = [[LessonInfoCollapseClickCell alloc] initWithFrame:CGRectMake(0, 0, width, LessonInfoHeaderHeight)];

    cell = [views objectAtIndex:0];
    
    // Initialization Here
    cell.TitleLabel.text = title;
    cell.index = index;
    cell.TitleButton.tag = index;
    cell.ContentView.frame = CGRectMake(cell.ContentView.frame.origin.x, cell.ContentView.frame.origin.y, cell.ContentView.frame.size.width, content.frame.size.height);

    [cell.ContentView addSubview:content];

    return cell;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
