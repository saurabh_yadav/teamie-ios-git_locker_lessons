//
//  BulletinBoardViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 2/7/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TeamieRevealController.h"
#import "BulletinBoardEntryCell.h"
#import "PullRefreshTableViewController.h"
#import "TMEListViewController.h"

@interface BulletinBoardViewController : TMEListViewController <TMERevealDelegate>

@property (nonatomic, strong) NSArray *bulletinBoardEntries;

@end
