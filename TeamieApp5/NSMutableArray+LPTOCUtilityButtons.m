//
//  NSMutableArray+LPTOCUtilityButtons.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 22/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "NSMutableArray+LPTOCUtilityButtons.h"

@implementation NSMutableArray (LPTOCUtilityButtons)
- (void)lptoc_addUtilityButtonWithColor:(UIColor *)color icon:(NSString *)icon
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = color;
//    [button setTitle:[NSString fontAwesomeIconStringForIconIdentifier:icon] forState:UIControlStateNormal];
//    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [button setAttributedTitle:attributedString forState:UIControlStateNormal];
//    [button setTitle:[NSString fontAwesomeIconStringForIconIdentifier:@"icon-ok"] forState:UIControlStateNormal];
    UIImage *image = [TeamieUIGlobals defaultPicForPurpose:icon];
    [button setImage:image forState:UIControlStateNormal];
    [self addObject:button];
}
@end
