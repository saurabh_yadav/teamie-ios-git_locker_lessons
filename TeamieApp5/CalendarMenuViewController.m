//
//  CalendarMenuViewController.m
//  TeamieApp5
//
//  Created by Raunak on 16/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "CalendarMenuViewController.h"

@interface CalendarMenuViewController ()
@property (nonatomic) NSInteger maxMenuItems;
@end

@implementation CalendarMenuViewController

- (id)initWithStyle:(UITableViewStyle)style {
    if (self = [super initWithStyle:style]) {
        self.contentSizeForViewInPopover = CGSizeMake(175, 130);
        self.tableView.scrollEnabled = NO;
        self.tableView.bounces = NO;
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    /*if (self.maxMenuItems == 0 ) {
        TTErrorView* errorView = [[TTErrorView alloc] initWithTitle:@"No Calendars"
                                                           subtitle:nil
                                                              image:nil];
        errorView.backgroundColor = _tableView.backgroundColor;
        self.errorView = errorView;
    }*/
}


- (void)setNumOfMenuItems:(NSInteger)maxMenuItems {
    
    self.maxMenuItems = maxMenuItems;
    
    if (self.maxMenuItems > 6) {
        self.contentSizeForViewInPopover = CGSizeMake(self.contentSizeForViewInPopover.width, 6 * 42.0 + 4.0);
        self.tableView.scrollEnabled = YES;
    }
    else if (self.maxMenuItems == 0){
        self.contentSizeForViewInPopover = CGSizeMake(self.contentSizeForViewInPopover.width, 84.0);
    }
    else {
        self.contentSizeForViewInPopover = CGSizeMake(self.contentSizeForViewInPopover.width, maxMenuItems * 42.0 + 4.0);
    }
}

- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath {
    /*if([object isKindOfClass:[TTTeamieCalendarItem class]]) {
        TTTeamieCalendarItemCell *selectedCell = (TTTeamieCalendarItemCell *)[self.tableView cellForRowAtIndexPath:indexPath];
        if (selectedCell.item.state == CheckmarkChecked) {
            selectedCell.item.state = CheckmarkNone;
            [self.menuDelegate menuDidDeselectObjectWithData:selectedCell.item];
        }
        else {
            selectedCell.item.state = CheckmarkChecked;
            [self.menuDelegate menuDidSelectObjectWithData:selectedCell.item];
        }
        [self.tableView reloadData];
    }*/
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
