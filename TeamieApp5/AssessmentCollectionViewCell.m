//
//  AssesmentCollectionViewCell.m
//  CollectionView
//
//  Created by Anuj Rajput on 15/10/14.
//
//

#import <QuartzCore/QuartzCore.h>
#import "AssessmentCollectionViewCell.h"
#import "TeamieGlobals.h"
#import "TeamieUIGlobalColors.h"
#import "UILabelWithPadding.h"
#import "UIView+TMEEssentials.h"
#import <Masonry/Masonry.h>

// MARK: Constants
/**
 *  Cell Corner Radius
 */
CGFloat kCellCornerRadius       = 8.0;

/**
 *  Status Labels Corner Radius
 */
CGFloat kLabelCornerRadius      = 2.0;

/**
 *  Submission Label Left Margin
 */
CGFloat kLabelLeftMargin        = 10.5;

/**
 *  Status Labels Left Margin
 */
CGFloat kStatusLabelLeftMargin  = 4.0;

/**
 *  Status Labels Right Margin
 */
CGFloat kStatusLabelRightMargin = 4.0;


@interface AssessmentCollectionViewCell ()

// MARK: Cell Subviews
@property (weak, nonatomic) IBOutlet UIView             *assessmentHeaderView;
@property (weak, nonatomic) IBOutlet UILabel            *questionCountLabel;
@property (weak, nonatomic) IBOutlet UILabel            *totalMarksLabel;
@property (weak, nonatomic) IBOutlet UILabel            *assessmentDurationLabel;
@property (weak, nonatomic) IBOutlet UILabel            *attemptCountLabel;
@property (weak, nonatomic) IBOutlet UILabelWithPadding *submissionLabel;

// MARK: assessmentHeaderView Subviews
@property (weak, nonatomic) IBOutlet UILabel            *assessmentTitle;
@property (weak, nonatomic) IBOutlet UILabel            *assessmentDeadline;
@property (weak, nonatomic) IBOutlet UILabelWithPadding *assessmentPublishState;
@property (weak, nonatomic) IBOutlet UILabelWithPadding *assessmentType;
@property (weak, nonatomic) IBOutlet UILabelWithPadding *assessmentClassrooms;

// MARK: Auto Layout Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingToAssessmentType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *quizPublishStateWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *submissionCountLabelHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *trailingToSubmissionCount;

@end

@implementation AssessmentCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    [self styleCellItems];
}

- (void)styleCellItems {

    // MARK: Cell Header View
    self.assessmentHeaderView.backgroundColor = [TeamieUIGlobalColors headerViewBackgroundColor];
    [self.assessmentHeaderView addBorderToTop:NO bottom:YES left:NO right:NO];
    
    // MARK: assessmentHeaderView Subviews
    self.assessmentTitle.font      = [TeamieGlobals appFontFor:@"AppFontForPurposeCellTitleText"];
    self.assessmentTitle.textColor = [TeamieUIGlobalColors headerTitleLabelTextColor];
    
    self.assessmentDeadline.font      = [TeamieGlobals appFontFor:@"AppFontForPurposeCellSubtitleText"];
    self.assessmentDeadline.textColor = [TeamieUIGlobalColors headerSubtitleLabelTextColor];
    
    self.quizPublishStateWidth.constant             = kStatusLabelLeftMargin + CGRectGetWidth(self.assessmentPublishState.frame) + kStatusLabelRightMargin;
    self.assessmentPublishState.font                = [TeamieGlobals appFontFor:@"AppFontForPurposeCellLabelText"];
    self.assessmentPublishState.textColor           = [TeamieUIGlobalColors warningLabelTextColor];
    self.assessmentPublishState.backgroundColor     = [TeamieUIGlobalColors warningLabelBackgroundColor];
    self.assessmentPublishState.layer.cornerRadius  = kLabelCornerRadius;
    self.assessmentPublishState.layer.masksToBounds = YES;
    self.assessmentPublishState.textAlignment       = NSTextAlignmentCenter;
    self.assessmentPublishState.edgeInsets          = UIEdgeInsetsMake(0.0, kStatusLabelLeftMargin, 0.0, kStatusLabelRightMargin);
    
    self.assessmentType.font                = [TeamieGlobals appFontFor:@"AppFontForPurposeCellLabelText"];
    self.assessmentType.textColor           = [TeamieUIGlobalColors primaryLabelTextColor];
    self.assessmentType.backgroundColor     = [TeamieUIGlobalColors primaryLabelBackgroundColor];
    self.assessmentType.layer.cornerRadius  = kLabelCornerRadius;
    self.assessmentType.layer.masksToBounds = YES;
    self.assessmentType.textAlignment       = NSTextAlignmentCenter;
    self.assessmentType.edgeInsets          = UIEdgeInsetsMake(0.0, kStatusLabelLeftMargin, 0.0, kStatusLabelRightMargin);
    
    self.assessmentClassrooms.font                = [TeamieGlobals appFontFor:@"AppFontForPurposeCellLabelText"];
    self.assessmentClassrooms.textColor           = [TeamieUIGlobalColors infoLabelTextColor];
    self.assessmentClassrooms.backgroundColor     = [TeamieUIGlobalColors infoLabelBackgroundColor];
    self.assessmentClassrooms.layer.cornerRadius  = kLabelCornerRadius;
    self.assessmentClassrooms.layer.masksToBounds = YES;
    self.assessmentClassrooms.textAlignment       = NSTextAlignmentCenter;
    self.assessmentClassrooms.edgeInsets          = UIEdgeInsetsMake(0.0, kStatusLabelLeftMargin, 0.0, kStatusLabelRightMargin);
    
    // MARK: Assessment Statistics Labels
    self.questionCountLabel.backgroundColor   = [TeamieUIGlobalColors detailLabelBackgroundColor];
    self.questionCountLabel.textColor         = [TeamieUIGlobalColors detailLabelTitleTextColor];
    [self.questionCountLabel mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(self).dividedBy(2.0);
    }];
    [self.questionCountLabel addBorderToTop:NO bottom:YES left:NO right:YES width:2.0];

    self.totalMarksLabel.backgroundColor   = [TeamieUIGlobalColors detailLabelBackgroundColor];
    self.totalMarksLabel.textColor         = [TeamieUIGlobalColors detailLabelTitleTextColor];
    [self.totalMarksLabel mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(self.questionCountLabel);
        make.left.equalTo(self.questionCountLabel.mas_right);
    }];
    [self.totalMarksLabel addBorderToTop:NO bottom:YES left:NO right:NO width:2.0];

    self.assessmentDurationLabel.backgroundColor   = [TeamieUIGlobalColors detailLabelBackgroundColor];
    self.assessmentDurationLabel.textColor         = [TeamieUIGlobalColors detailLabelTitleTextColor];
    [self.assessmentDurationLabel addBorderToTop:NO bottom:NO left:NO right:YES width:2.0];

    self.attemptCountLabel.backgroundColor   = [TeamieUIGlobalColors detailLabelBackgroundColor];
    self.attemptCountLabel.textColor         = [TeamieUIGlobalColors detailLabelTitleTextColor];
    [self.attemptCountLabel mas_makeConstraints:^(MASConstraintMaker *make){
        make.left.equalTo(self.assessmentDurationLabel.mas_right);
    }];
    [self.attemptCountLabel addBorderToTop:NO bottom:NO left:NO right:NO width:2.0];
  
    self.submissionLabel.backgroundColor     = TeamieUIGlobalColors.detailLabelBackgroundColor;
    self.submissionLabel.textColor           = TeamieUIGlobalColors.detailLabelTitleNumberColor;
    self.submissionLabel.edgeInsets          = UIEdgeInsetsMake(0.0, kLabelLeftMargin, 0.0, 0.0);
    [self.submissionLabel mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(self);
    }];
    [self.submissionLabel addBorderToTop:YES bottom:NO left:NO right:NO width:2.0];
    
    /** Xcode 6 on iOS 7 hot fix **/
    if (SYSTEM_VERSION_LESS_THAN(@"8.0")) {
        self.contentView.frame = self.bounds;
        self.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    }
    // MARK: Cell Corner Radius & Cell Drop Shadow
    self.layer.cornerRadius  = kCellCornerRadius;
    self.layer.shadowOffset  = CGSizeMake(0, 1);
    self.layer.shadowRadius  = 4;
    self.layer.shadowOpacity = 1;
    self.layer.shadowColor   = [TeamieUIGlobalColors cellShadowColor].CGColor;
    UIBezierPath *path       = [UIBezierPath bezierPathWithRect:self.bounds];
    self.layer.shadowPath    = path.CGPath;
}

#pragma mark Private Methods

- (void)setCellData:(TMEQuizSimple *)quiz withClassroomId:(NSNumber *)nid {
    // MARK: Set the textual data
    self.assessmentTitle.text         = quiz.title;
    self.assessmentDeadline.text      = quiz.deadLineDateString;
    self.assessmentType.text          = quiz.type;
    self.assessmentClassrooms.text    = [quiz classroomsAsStringForClassroomId:nid];
    self.questionCountLabel.text      = quiz.numberOfQuestionsString;
    self.totalMarksLabel.text         = quiz.marksString;
    self.assessmentDurationLabel.text = quiz.durationString;
    self.attemptCountLabel.text       = quiz.attemptCountString;
    self.submissionLabel.text         = quiz.submissionsMadeString;
    
    // MARK: Remove Draft status label and shift other labels to the left
//    if (!quiz.isDraft) {
//        [self adjustStatusLabelConstraints];
    self.assessmentPublishState.hidden = quiz.isDraft ? NO : YES;
    self.quizPublishStateWidth.constant = quiz.isDraft ? kStatusLabelLeftMargin + CGRectGetWidth(self.assessmentPublishState.frame) + kStatusLabelRightMargin : 0.f;
    self.leadingToAssessmentType.constant = quiz.isDraft ? 5.f : 0.f;
    [self layoutIfNeeded];
//    }
    
    // MARK: Remove Submission count detail label and reduce the cell size if a user doesn't access to view submissions
    if (!quiz.hasAccess) {
        [self adjustDetailLabelConstraints];
    }
}

- (void)adjustStatusLabelConstraints {
    // Empty the text
    self.assessmentPublishState.text      = @"";
    // Hide the label
    self.assessmentPublishState.hidden    = YES;
    // Zero the label width
    self.quizPublishStateWidth.constant   = 0.f;
    // Zero the trailing space (or leading space from the next label)
    self.leadingToAssessmentType.constant = 0.f;
    // Update the constraints
    [self layoutIfNeeded];
}

- (void)adjustDetailLabelConstraints {
    // Empty the text
    self.submissionLabel.text                = @"";
    // Hide the label
    self.submissionLabel.hidden              = YES;
    // Zero label height
    self.submissionCountLabelHeight.constant = 0.f;
    // Zero top space
    self.trailingToSubmissionCount.constant  = 0.f;
    // Update the constraints
    [self layoutIfNeeded];
}

@end
