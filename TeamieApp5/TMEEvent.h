//
//  TMEEvent.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEModel.h"

typedef NS_ENUM(NSInteger, TMEEventType) {
    TMEEventDefault,
    TMEEventLesson,
    TMEEventQuiz,
    TMEEventHomework,
    TMEEventVideoConference,
};

@interface TMEEvent : TMEModel

- (NSString *)eventTypeName;

@property (nonatomic, strong) NSNumber *eventID;
@property (nonatomic, strong) NSNumber *calendarID;
@property (nonatomic, strong) NSString *calendarName;
@property (nonatomic) TMEEventType type;
@property (nonatomic, strong) NSNumber *userID;
@property (nonatomic, strong) NSDate *from;
@property (nonatomic, strong) NSDate *to;
@property (nonatomic) BOOL allDay;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *desc;

@end
