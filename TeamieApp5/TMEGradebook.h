//
//  Gradebook.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"

@interface TMEGradebook : TMEModel

// This property stores an array of QuizSimpleObject objects that contain info about each quiz for this gradebook.
@property (nonatomic, retain) NSArray * quizList;
// This property stores an array of UserScore objects that contain info about each user's score in each quiz for this gradebook.
@property (nonatomic, retain) NSArray * scoreList;

// A Public API method to export the Gradebook data as an array of values so it can easily be put into a CSV file.
- (NSArray*)exportDataForCSV;

@end
