//
//  UserProfileClassroomListViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 08/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UserProfileClassroomListViewController.h"

@interface UserProfileClassroomListViewController ()

@property (nonatomic, retain) NSArray *userClassrooms;

@end

@implementation UserProfileClassroomListViewController

- (id)initWithClassrooms:(NSArray *)classrooms
{
    self = [super init];
    if (self) {
        self.userClassrooms = classrooms;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.userClassrooms.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"UserProfileClassroomListViewCell";
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    
    NSString *classroom = self.userClassrooms[indexPath.row];
    
    if (cell) {
        cell.textLabel.text = classroom;
        cell.textLabel.font = [TeamieGlobals appFontFor:@"userProfileSectionContentText"];
    }
    
    return cell;
}

@end
