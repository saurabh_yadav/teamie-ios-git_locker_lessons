//
//  InstituteSelectViewController.m
//  TeamieApp5
//
//  Created by Teamie on 16/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "InstituteSelectViewController.h"
#import "TMESiteInfo.h"
#import "TMEAlternateClient.h"
#import "TMEUser.h"
#import "TMEClient.h"

#import <Masonry/Masonry.h>

@interface InstituteSelectViewController ()

@property (nonatomic, strong) AutocompletionTableView *autoCompleter;

@end

@implementation InstituteSelectViewController

@synthesize instituteTextField = _instituteTextField;
@synthesize autoCompleter;


// Init the UIViewController from the Storyboard
- (id)initFromStoryboard {
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:[TeamieGlobals storyboardIdentifier] bundle:nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"InstituteSelectViewController"];
    return self;
    
}

#pragma mark - View Lifecycle Methods

- (void)viewDidLoad {
    [super viewDidLoad];

    self.constraintsAdded = NO;
    [self.navigationController setNavigationBarHidden:YES];
    [self.progressIndicator setHidden:YES];
    
    if ([[TMEClient sharedClient] isLoggedIn]) {
        [[[UIApplication sharedApplication] delegate] performSelector:@selector(transitionToHomeScreen)];
        return;
    }
    
    _domainMappings = [TeamieGlobals instituteDomainMappings];
    
    [[TMEAlternateClient sharedClient]
     request:TeamieUserDomainRequest
     parameters:@{@"key":TEAMIE_DOMAIN_LIST_KEY}
     loadingMessage:TMELocalize(@"message.loading")
     success:^(NSDictionary *response) {
         NSMutableDictionary* domainMappings = [[NSMutableDictionary alloc] init];
         NSLog(@"%@", response[@"sites"]);
         for (NSString *siteName in response[@"sites"]) {
             domainMappings[siteName] = [response[@"sites"][siteName] stringByAppendingString:@"/api"];
         }
         _domainMappings = domainMappings;
     } failure:nil];

    // Set the fonts for all controls
    [self.sloganLabel setFont:[TeamieGlobals appFontFor:@"slogan"]];
    [self.subsloganLabel setFont:[TeamieGlobals appFontFor:@"subslogan"]];
    [self.instituteTextField setFont:[TeamieGlobals appFontFor:@"inputText"]];
    
    self.instituteTextField.layer.cornerRadius = UI_NORMAL_BORDER_RADIUS;
    self.instituteTextField.layer.borderColor = [TeamieUIGlobalColors inputFieldBorderColor].CGColor;
    self.instituteTextField.layer.borderWidth = 1.0f;
    self.instituteTextField.layer.masksToBounds = YES;
    
    self.instituteTextField.delegate = self;
    [self.instituteTextField addTarget:self.autoCompleter action:@selector(textFieldValueChanged:) forControlEvents:UIControlEventEditingChanged];
    [self.instituteTextField addTarget:self.autoCompleter action:@selector(textFieldDidEndEditing:) forControlEvents:UIControlEventEditingDidEnd];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    [self.instituteTextField setText:@""];
    [self.instituteTextField becomeFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/choose-institute"];
}

- (void)viewDidUnload {
    [self setInstituteTextField:nil];
}

- (void)updateViewConstraints {
    if (!self.areConstraintsAdded) {
        [self.logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.top.equalTo(@50);
            make.width.height.equalTo(@60);
        }];
        
        [self.sloganLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.top.equalTo(self.logoImageView.mas_bottom).with.offset(25);
        }];
        
        [self.subsloganLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.top.equalTo(self.sloganLabel.mas_bottom).with.offset(15);
            make.height.equalTo(@35);
        }];
        
        [self.instituteTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.top.equalTo(self.subsloganLabel.mas_bottom).with.offset(35);
            make.width.equalTo(@240);
            make.height.equalTo(@40);
        }];
        
        [self.progressIndicator mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.view);
            make.top.equalTo(self.instituteTextField.mas_bottom).with.offset(10);
        }];
        
        self.constraintsAdded = YES;
    }
    [super updateViewConstraints];
}

#pragma mark - Autocomplete Methods

- (AutocompletionTableView *)autoCompleter
{
    if (!autoCompleter)
    {
        NSMutableDictionary *options = [NSMutableDictionary dictionaryWithCapacity:2];
        [options setValue:[NSNumber numberWithBool:NO] forKey:ACOCaseSensitive];
        [options setValue:nil forKey:ACOUseSourceFont];
        
        autoCompleter = [[AutocompletionTableView alloc] initWithTextField:self.instituteTextField inViewController:self withOptions:options];
        autoCompleter.autoCompleteDelegate = self;
        autoCompleter.suggestionsDictionary = [_domainMappings allKeys];
    }
    return autoCompleter;
}

- (void) autoCompletion:(AutocompletionTableView*) completer didSelectAutoCompleteSuggestionWithIndex:(NSInteger) index{
    // Get the institute URL
    
    NSString * institute = self.instituteTextField.text;
    // Assign a default value for _siteinfo object
    _siteinfo = [[TMESiteInfo alloc] init];
    _siteinfo.siteName = institute;
    
    if (([institute length] > 0) && ([[_domainMappings allKeys] indexOfObject:institute] == NSNotFound)) {
        // if the institute field is a string that does not contain "theteamie.com" or "teamieapp.com" in it also.
        if ([institute rangeOfString:@"theteamie.com"].location == NSNotFound && [institute rangeOfString:@"teamieapp.com"].location == NSNotFound) {
            return;
        }
    }
    if ([institute rangeOfString:@"theteamie.com"].location == NSNotFound && [institute rangeOfString:@"teamieapp.com"].location == NSNotFound) {
        _instituteURL = [_domainMappings valueForKey:institute];
    }
    else {
        _instituteURL = institute;
    }
    
    // Trim the Site URL to contain only the URL without other stuff.
    _instituteURL = [[[_instituteURL stringByReplacingOccurrencesOfString:@"https://" withString:@""] stringByReplacingOccurrencesOfString:@"http://" withString:@""] stringByReplacingOccurrencesOfString:@"/api" withString:@""];
    
    // Make request to load public info about the selected site.
    [self showProgressIndicator];
    [[TMEClient sharedClient]
     request:TeamieUserPublicSiteInfoRequest
     parameters:@{@"key": TEAMIE_DOMAIN_LIST_KEY, @"site": _instituteURL}
     loadingMessage:nil
     success:^(NSDictionary *response) {
         _siteinfo = [TMESiteInfo parseDictionary:response];
         [self stopProgressIndicator];
         [self performSegueWithIdentifier:@"loginForInstitute" sender:self];
     } failure:^(NSError *error) {
         [self stopProgressIndicator];
     }];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"loginForInstitute"]) {
        if ([segue.destinationViewController respondsToSelector:@selector(setInstituteURL:)]) {
            [segue.destinationViewController performSelector:@selector(setInstituteURL:) withObject:_instituteURL];
        }
        if ([segue.destinationViewController respondsToSelector:@selector(setSiteinfo:)]) {
            [segue.destinationViewController performSelector:@selector(setSiteinfo:) withObject:_siteinfo];
        }
    }
}

- (NSArray*) autoCompletion:(AutocompletionTableView*) completer suggestionsFor:(NSString*) string{
    // with the prodided string, build a new array with suggestions - from DB, from a service, etc.
    return [_domainMappings allKeys];
}

#pragma mark Keyboard Notification methods
- (void)keyboardWillShow:(NSNotification *)note {
    [TeamieGlobals dismissTSMessage];

    [self.view bringSubviewToFront:self.autoCompleter];
    [self.view bringSubviewToFront:self.instituteTextField];
}

#pragma mark Private
- (void)showProgressIndicator {
    [self.progressIndicator setHidden:NO];
    [self.progressIndicator startAnimating];
}

- (void)stopProgressIndicator {
    [TeamieGlobals dismissActivityLabels];
    [self.progressIndicator stopAnimating];
    [self.progressIndicator setHidden:YES];
}

@end
