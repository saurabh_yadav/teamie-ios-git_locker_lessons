//
//  InstituteSelectViewController.h
//  TeamieApp5
//
//  Created by Teamie on 16/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AutocompletionTableView.h"

@class TMESiteInfo;

@interface InstituteSelectViewController : UIViewController <UITextFieldDelegate, AutocompletionTableViewDelegate> {
    NSDictionary* _domainMappings;
    NSString* _instituteURL;
    TMESiteInfo* _siteinfo;
}

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *sloganLabel;
@property (weak, nonatomic) IBOutlet UILabel *subsloganLabel;

@property (weak, nonatomic) IBOutlet UITextField *instituteTextField;
@property (weak, nonatomic) IBOutlet UILabel *appVersion;

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *progressIndicator;

@property (nonatomic, getter=areConstraintsAdded) BOOL constraintsAdded;

- (id)initFromStoryboard;

@end
