//
//  TMECardTableViewCell.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMECardTableViewCell.h"
#import "UIView+TMEEssentials.h"
#import "TMEShadowView.h"

#import <Masonry/Masonry.h>

@interface TMECardTableViewCell()

@property (nonatomic, weak) UIView *shadowView;

@end

@implementation TMECardTableViewCell

const CGFloat kDefaultCardPadding = 13;
static UIEdgeInsets kDefaultInsets;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor = [TeamieUIGlobalColors baseBgColor];

        self.shadowView = [self.contentView createAndAddSubView:TMEShadowView.class];
        self.shadowView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return self;
}

- (void)setCardView:(UIView *)cardView {
    [self.shadowView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    _cardView = cardView;
    [self.shadowView addSubview:self.cardView];
    self.cardView.layer.masksToBounds = YES;
    self.cardView.layer.cornerRadius = 4.0f;
}

- (void)updateConstraints {
    kDefaultInsets = UIEdgeInsetsMake(kDefaultCardPadding, kDefaultCardPadding, 0, kDefaultCardPadding);
    [self.cardView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.shadowView);
    }];
    [self.shadowView mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).with.insets(kDefaultInsets);
    }];
    [super updateConstraints];
}

@end
