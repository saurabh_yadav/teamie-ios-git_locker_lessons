//
//  URLParam.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "URLParam.h"
#import "UserMenu.h"


@implementation URLParam

@dynamic isOptional;
@dynamic name;
@dynamic value;
@dynamic paramMenu;

@end
