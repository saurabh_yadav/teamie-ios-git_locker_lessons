//
//  TeamieUIGlobalColors.m
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TeamieUIGlobalColors.h"

@implementation TeamieUIGlobalColors

+ (UIColor*)primaryRedColor {
    return [UIColor colorWithHex:@"#be202e" alpha:1.0];
}
+ (UIColor*)secondaryRedColor {
    return [UIColor colorWithHex:@"#F25246" alpha:1.0];
}
+ (UIColor*)primaryBlueColor {
    return [UIColor colorWithHex:@"#659CB4" alpha:1.0];
}
+ (UIColor*)secondaryBlueColor {
    return [UIColor colorWithHex:@"#2F445C" alpha:1.0];
}
+ (UIColor*)primaryGreenColor {
    return [UIColor colorWithHex:@"#296950" alpha:1.0];
}
+ (UIColor*)secondaryGreenColor {
    return [UIColor colorWithHex:@"#5A915E" alpha:1.0];
}

+ (UIColor*)navigationBarBackgroundColor {
#ifdef DEBUG
    return [UIColor colorWithHex:@"#2F445C" alpha:1.0];
#else
    return [UIColor colorWithHex:@"#70615E" alpha:1.0];
#endif
}

+ (UIColor*)navigationBarTextColor {
    return [UIColor whiteColor];
}

+ (UIColor*)toolbarBackgroundColor{
    return [UIColor colorWithHex:@"#DDDDDD" alpha:1.0];
}

+ (UIColor*)toolbarTextColor{
    return [UIColor colorWithHex:@"#555555" alpha:1.0];
}

+ (UIColor*)inputFieldBorderColor {
    return [UIColor colorWithHex:@"#CCCCCC" alpha:1.0];
}
+ (UIColor*)inputFieldTextColor {
    return [UIColor colorWithHex:@"#555555" alpha:1.0];
}

+ (UIColor*)tableViewCellBorderColor {
    return [UIColor colorWithHex:@"#cccccc" alpha:1.0];
}

+ (UIColor*)defaultTextColor{
    return [UIColor colorWithHex:@"#333333" alpha:1.0];
}
+ (UIColor*)buttonTitleColor{
    return [UIColor colorWithHex:@"#333333" alpha:1.0];
}
+ (UIColor*)thoughtCommentTextColor{
    return [UIColor colorWithHex:@"#6F6F6F" alpha:1.0];
}
+ (UIColor*)thoughtReplyTextColor{
    return [UIColor colorWithHex:@"#6F6F6F" alpha:1.0];
}
+ (UIColor*)postCommentBackgroundColor{
    return [UIColor colorWithHex:@"#f4f5f6" alpha:1.0];
}
+ (UIColor*)postCommentReplyBackgroundColor {
    return [UIColor colorWithHex:@"#e7eaef" alpha:1.0];
}
+ (UIColor*)borderColor{
    return [UIColor colorWithHex:@"#CCCCCC" alpha:1.0];
}

+ (UIColor*)primaryActionColor {
    return [UIColor colorWithHex:@"#5F6F8C" alpha:1.0];
}

+ (UIColor*)tableViewIconColor {
    return [UIColor colorWithHex:@"#6f6f6f" alpha:1.0];
}

+ (UIColor *)grayColor {
    return [UIColor colorWithHex:@"#6f6f6f" alpha:1.0];
}

+ (UIColor*)genericBackgroundColor {
    return [UIColor colorWithHex:@"#CCCCCC" alpha:1.0];
}

+ (UIColor *)baseBgColor {
    return [UIColor colorWithHex:@"#CCCCCC" alpha:1.0];
}

+ (UIColor *)supplementaryViewElementTextColor {
    return [UIColor colorWithHex:@"#333333" alpha:1.0];
}

+ (UIColor *)headerViewBackgroundColor {
    return [UIColor colorWithHex:@"#E7EAEF" alpha:1.0];
}

+ (UIColor *)headerTitleLabelTextColor {
    return [UIColor colorWithHex:@"#333333" alpha:1.0];
}

+ (UIColor *)headerSubtitleLabelTextColor {
    return [UIColor colorWithHex:@"#333333" alpha:1.0];
}

+ (UIColor *)warningLabelTextColor {
    return [UIColor colorWithHex:@"#3C3C3C" alpha:1.0];
}

+ (UIColor *)warningLabelBackgroundColor {
    return [UIColor colorWithHex:@"#F5DC9A" alpha:1.0];
}

+ (UIColor *)primaryLabelTextColor {
    return [UIColor colorWithHex:@"#FFFFFF" alpha:1.0];
}

+ (UIColor *)primaryLabelBackgroundColor {
    return [UIColor colorWithHex:@"#5F6F8C" alpha:1.0];
}

+ (UIColor *)infoLabelTextColor {
    return [UIColor colorWithHex:@"#3C3C3C" alpha:1.0];
}

+ (UIColor *)infoLabelBackgroundColor {
    return [UIColor colorWithHex:@"#CCCCCC" alpha:1.0];
}

+ (UIColor *)detailLabelTitleTextColor {
    return [UIColor colorWithHex:@"#6F6F6F" alpha:1.0];
}

+ (UIColor *)detailLabelTitleNumberColor {
    return [UIColor colorWithHex:@"#333333" alpha:1.0];
}

+ (UIColor *)detailLabelBackgroundColor {
    return [UIColor colorWithHex:@"#FFFFFF" alpha:1.0];
}

+ (UIColor *)subviewBorderColor {
    return [UIColor colorWithHex:@"#DDDDDD" alpha:1.0];
}

+ (UIColor *)cellShadowColor {
    return [UIColor colorWithHex:@"#A2A2A2" alpha:1.0];
}

+ (UIColor *)cellContainerBackgroundColor {
    return [UIColor colorWithHex:@"#CCCCCC" alpha:1.0];
}

+ (UIColor *)notificationBadgeBackgroundColor {
    return [UIColor colorWithHex:@"#BE202E" alpha:1.0];
}

+ (UIColor *)notificationBadgeTextColor {
    return [UIColor colorWithHex:@"#FFFFFF" alpha:1.0];
}

+ (UIColor *)secondaryYellowColor {
    return [UIColor colorWithHex:@"#F5DC9A" alpha:1.0];
}

+ (UIColor *)publishStatusDraftLabelTextColor{
    return [UIColor colorWithHex:@"#333333" alpha:1.0];
}
+ (UIColor *)publishStatusPublishedLabelTextColor{
    return [UIColor colorWithHex:@"#ffffff" alpha:1.0];
}
+ (UIColor *)publishStatusDraftLabelBackGroundColor{
    return [UIColor colorWithHex:@"#f5dc9a" alpha:1.0];
}
+ (UIColor *)publishStatusPublishedLabelBackGroundColor{
    return [UIColor colorWithHex:@"#41A389" alpha:1.0];
}

+ (UIColor *)pollOptionHighlightedLabelBackgroundColor {
    return [UIColor colorWithHex:@"#CFE7E4" alpha:1.0];
}

+ (UIColor *)pollOptionSubmitButtonColor {
    return [UIColor colorWithHex:@"#41A389" alpha:1.0];
}

+ (UIColor *)disabledColor {
    return [UIColor colorWithHex:@"#E8E8E8" alpha:1.0];
}

@end
