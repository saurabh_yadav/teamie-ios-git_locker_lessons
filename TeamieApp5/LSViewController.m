//
//  LSViewController.m
//  LS2
//
//  Created by Wei Wenbo on 2/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import "LSViewController.h"
#import "LessonInfoViewController.h"
#import "TMELesson.h"
#import "TMELessonOverview.h"
#import "PKRevealController.h"
#import "LPTableView.h"
#import "LPTableViewCell.h"
#import "TMELessonPageShort.h"
#import "LessonPageViewController.h"
#import "TMEClient.h"
#import "SVModalWebViewController.h"
#import "LessonPageViewController.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "UserProfileViewController.h"
#import "TMEClassroomAlt.h"
#import "NSString+FontAwesome.h"
#import "Masonry.h"
#import "TMEListViewController.h"

@interface LSViewController () <TMERevealDelegate>
@property NSArray *lessons;
@property NSMutableArray* consolidateLessons;
@property NSMutableArray *lpTableViewControllers;
@property NSString *navTitle;
@property NSString *currSysVer;
@property NSMutableArray *lockerClassroomID;
@property (nonatomic, weak) UIViewController *parentRevealController;
@end

@implementation LSViewController{
    BOOL shouldDisplayLabel;
    BOOL loadMore;
    UIRefreshControl *refreshControl;
}

- (id)initWithSubjectID:(NSNumber *)subjectID withNavTitle:(NSString *)title
{
    // init the viewController from the storyboard
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Lesson" bundle:nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"LSViewController"];
    
    if (self != nil) {
        _subjectID = subjectID;
        shouldDisplayLabel = YES;
        _navTitle = title;
    }
    
    return self;
}

-(id)initWithtitle:(NSString *)title{
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Lesson" bundle:nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"LSViewController"];
    
    if (self != nil) {
        shouldDisplayLabel = YES;
        _navTitle = title;
        _lockerClassroomID = [NSMutableArray new];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.revealController.frontViewController.revealController.recognizesPanningOnFrontView = NO;
    
    loadMore = NO;
    self.consolidateLessons = [NSMutableArray array];
    [self makeWebRequestsWithParameters:NO];
    self.view.backgroundColor = [TeamieUIGlobalColors baseBgColor];

    if(_subjectID){
        if([self.classroomName length] > 7){
            self.navigationItem.title = [NSString stringWithFormat:@"%@... : %@",
                                         [self.classroomName substringToIndex:7], TMELocalize(@"menu.lessons")];
        }
        else{
            self.navigationItem.title = [NSString stringWithFormat:@"%@ : %@", self.classroomName, TMELocalize(@"menu.lessons")];
        }
        UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"back2" withSize:25 andColor:[UIColor whiteColor]] style:UIBarButtonItemStyleDone target:self action:@selector(backButtonPressed)];
        self.navigationItem.leftBarButtonItem = backButton;
        [self.navigationItem.leftBarButtonItem setTintColor:[UIColor whiteColor]];
        [self.navigationItem.rightBarButtonItem setTintColor:[UIColor whiteColor]];
        
        
        self.navigationItem.hidesBackButton = YES;
        self.navigationItem.backBarButtonItem = nil;

        
    }
    
    else{
        self.navigationItem.title = TMELocalize(@"menu.locker_lessons");
        
    }
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.backBarButtonItem = nil;
    
        // TODO: Temp fix for refreshing lessons
        UIBarButtonItem *refreshButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(refreshLessons)];

        self.navigationItem.rightBarButtonItem = refreshButton;
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    
    [refreshControl addTarget:self action:@selector(refreshLessons) forControlEvents:UIControlEventValueChanged];
    [myCollapseClick addSubview:refreshControl];
    myCollapseClick.delegate = self;
    self.currSysVer = [[UIDevice currentDevice] systemVersion];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/lessons"];
}

#pragma mark - Collapse Click Delegate

// Required Methods
-(int)numberOfCellsForCollapseClick
{
    return self.consolidateLessons.count;
}

-(NSString *)titleForCollapseClickAtIndex:(int)index
{
    TMELesson *lesson = [self.consolidateLessons objectAtIndex:index];
    
    return [NSString stringWithFormat:@"%@ pages",lesson.pages];
}

-(UIView *)viewForCollapseClickContentViewAtIndex:(int)index
{
    TMELesson *lesson = [self.consolidateLessons objectAtIndex:index];
    if (lesson.lesson_pages.count == 0) {
        return nil;
    }
    LessonPageViewController *lessonPageTableViewController = [[LessonPageViewController alloc] init];
//    lessonPageTableViewController.delegate = self;
    

    [self.lpTableViewControllers insertObject:lessonPageTableViewController atIndex:index];
//    
//    [self addChildViewController:lessonPageTableViewController];
//    [lessonPageTableViewController.tableView setScrollEnabled:NO];
//    //    [lessonPageTableViewController.tableView setSeparatorInset:UIEdgeInsetsZero];
//    //    CGRect frame = lessonPageTableViewController.tableView.frame;
//    //    frame.size.height -= 47;
//    //    lessonPageTableViewController.tableView.frame = frame;
//    
//    return lessonPageTableViewController.tableView;
    
    LPTableView *tableView = [[LPTableView alloc]initWithNumberOfLessonPages:(int)lesson.lesson_pages.count index:index];
    
    tableView.dataSource = self;
    tableView.delegate = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.scrollEnabled = NO;
    
    
    return tableView;
}


// Optional Methods

-(UIColor *)colorForCollapseClickTitleViewAtIndex:(int)index
{
    return [UIColor whiteColor];
}


-(UIColor *)colorForTitleLabelAtIndex:(int)index
{
    return [UIColor colorWithWhite:0 alpha:0.85];
}

-(UIColor *)colorForTitleArrowAtIndex:(int)index
{
    return [UIColor colorWithWhite:0.0 alpha:1.0];
}

- (UIColor *)colorForLessonInfo:(int)index
{
    return [UIColor whiteColor];
}


-(void)didClickCollapseClickCellAtIndex:(int)index isNowOpen:(BOOL)open
{
    NSLog(@"breaking here");
}

- (void)didClickedLessonHeaderButton:(int)index
{
    TMELesson *lesson = [self.consolidateLessons objectAtIndex:index];
    LessonInfoViewController *lessonInfoViewController;
    if([lesson.type isEqualToString:@"scorm"]) {
        [TeamieGlobals openInappBrowserWithURL:lesson.web_url fromResponder:self withTitle:lesson.title];
    }
    else{
        lessonInfoViewController = [[LessonInfoViewController alloc]initWithLesson:lesson.nid];
        [self.navigationController pushViewController: lessonInfoViewController animated:YES];
    }
}

- (void) didclickAuthorImageButton:(int) index{
    NSLog(@"m in lsvc in didclick image button method");
    UIViewController *frontViewController;
    NSDictionary *fetchAuthorDetails = [self.lessons objectAtIndex:index];
    NSString *authorUID;
    authorUID = [[fetchAuthorDetails valueForKey:@"author"] valueForKey:@"uid"];
    frontViewController = [[UserProfileViewController alloc] initWithUserID: [authorUID integerValue]];
    [self.navigationController pushViewController:frontViewController animated:YES];
}
#pragma mark - LSCollapseClickDelegate

- (TMELesson *)lesson:(int)index
{
    TMELesson *lesson = [self.consolidateLessons objectAtIndex:index];
    return lesson;
}

- (NSNumber *)classroomIDforIndex:(int)index
{
    if(_subjectID){
        return _subjectID;
    }
    else{
        return [_lockerClassroomID objectAtIndex:index];
    }
    
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    float scrollViewHeight = scrollView.frame.size.height;
    float scrollContentSizeHeight = scrollView.contentSize.height;
    float scrollOffset = scrollView.contentOffset.y;
    if (scrollOffset + scrollViewHeight - 50 > scrollContentSizeHeight) {
        if (loadMore) {
            [self makeWebRequestsWithParameters:YES];
            loadMore = NO;
        }
    }
}

#pragma mark - Private Methods
- (void)refreshLessons{
    [self makeWebRequestsWithParameters:NO];
}

- (void)makeWebRequestsWithParameters:(BOOL)withParameters
{
    if (_subjectID != nil) {
        NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
        if (!withParameters) {
            params = [[NSMutableDictionary alloc] initWithDictionary:@{@"items_per_page": @20}];
            [self.consolidateLessons removeAllObjects];
        }
        else if (loadMore) {
            float currentPageCount = self.consolidateLessons.count/20;
            float nextPage = ceilf(currentPageCount);
            
            params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page": [NSNumber numberWithInteger:nextPage + 1], @"items_per_page": @20}];
        }

        [params setValue:[NSString stringWithFormat:@"%qi", [_subjectID longLongValue]] forKey:@"nid"];
        [[TMEClient sharedClient]
         request:TeamieUserLessonsRequest
         parameters:params
         loadingMessage:TMELocalize(@"message.loading")
         completion:^{
             [TeamieGlobals dismissActivityLabels];
             shouldDisplayLabel = NO;
             if (!self.consolidateLessons.count) {
//                 [self viewForEmptyListWithTitleText:nil subtitleText:nil andImage:nil];
             }
         }success:^(NSDictionary *response) {
             _lessons = [TMELesson parseList:response[@"lessons"]];
             if(_lessons.count == 0){
                 UIView *emptyViewforLessons = [self uIViewForEmptyListWithTitleText:nil subtitleText:nil andImage:nil];
                 [self.view addSubview:emptyViewforLessons];
             }
             else {
                 [self removeEmptyView];
             }
             [self.consolidateLessons addObjectsFromArray:_lessons];
             if (self.consolidateLessons.count % 20 == 0) {
                 loadMore = YES;
             }
             else {
                 loadMore = NO;
             }
             self.lpTableViewControllers = [[NSMutableArray alloc]initWithCapacity:self.consolidateLessons.count];
             
             for (int i=0; i<self.consolidateLessons.count; i++) {
                 [self.lpTableViewControllers addObject:[NSNull null]];
             }
             
             myCollapseClick.backgroundColor = [UIColor colorWithHex:@"#cccccc" alpha:1];
             myCollapseClick.CollapseClickDelegate = self;
             [myCollapseClick reloadCollapseClick];
             [myCollapseClick.infiniteScrollingView stopAnimating];
             [refreshControl endRefreshing];
             [TeamieGlobals dismissActivityLabels];
         } failure:^(NSError *error) {
             [TeamieGlobals dismissActivityLabels];
         }];
        }
    else{
        NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
        NSString *currentUser = (NSString *)[TMEUser currentUser].uid;
        if (!withParameters) {
            params = [[NSMutableDictionary alloc] initWithDictionary:@{@"items_per_page": @20,
                                                                       @"uid":currentUser,
                                                                       @"filter":@"all"
                                                                       }];
            [self.consolidateLessons removeAllObjects];
            [_lockerClassroomID removeAllObjects];
        }
        else if (loadMore) {
            float currentPageCount = self.consolidateLessons.count/20;
            float nextPage = ceilf(currentPageCount);
            
            params = [[NSMutableDictionary alloc] initWithDictionary:@{@"page": [NSNumber numberWithInteger:nextPage + 1], @"items_per_page": @20,
                                                                       @"uid":currentUser,
                                                                       @"filter":@"all"
                                                                       }];
        }
//        [params setValue:@"1" forKey:@"page"];
        [[TMEClient sharedClient]
         request:TeamieUserLockerLessonRequest
         parameters:params
         loadingMessage:TMELocalize(@"message.loading")
         completion:^{
             [TeamieGlobals dismissActivityLabels];
             shouldDisplayLabel = NO;
             if (!self.consolidateLessons.count) {
//                 [self viewForEmptyListWithTitleText:nil subtitleText:nil andImage:nil];
             }
         }
         success:^(NSDictionary *response) {
             _lessons = [TMELesson parseList:response[@"lessons"]];
             [self.consolidateLessons addObjectsFromArray:_lessons];
             if (self.consolidateLessons.count % 20 == 0) {
                 loadMore = YES;
             }
             else {
                 loadMore = NO;
             }
             for (TMELesson *lesson in _lessons) {
                 for (TMEClassroomAlt* classroom in lesson.classrooms) {
                     [_lockerClassroomID addObject:classroom.nid];
                 }
             }
             
             myCollapseClick.backgroundColor = [UIColor colorWithHex:@"#cccccc" alpha:1];
             myCollapseClick.CollapseClickDelegate = self;
             [myCollapseClick reloadCollapseClick];
             [myCollapseClick.infiniteScrollingView stopAnimating];
             [refreshControl endRefreshing];
             [TeamieGlobals dismissActivityLabels];
         } failure:^(NSError *error) {
             [TeamieGlobals dismissActivityLabels];
         }];
         }
}

- (void)backButtonPressed
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RowHeight;
}

- (void)tableView:(LPTableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    TMELesson *lesson = [self.consolidateLessons objectAtIndex:[tableView.index intValue]];
    LessonPageViewController *lessonPageViewController = [[LessonPageViewController alloc]init];
    lessonPageViewController.lessonsList = lesson.lesson_pages;
    lessonPageViewController.currentPage = indexPath.row;
    [self.navigationController pushViewController:lessonPageViewController animated:YES];
}

#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(LPTableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    TMELesson *lesson = [self.consolidateLessons objectAtIndex:[tableView.index intValue]];
    
    return lesson.lesson_pages.count;
}

- (LPTableViewCell *)tableView:(LPTableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *cellIdentifier = @"LPTableViewCell";
    LPTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        NSArray* topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"LPTableViewCell" owner:self options:nil];
        
        for (id currentObject in topLevelObjects) {
            if ([currentObject isKindOfClass:[UITableViewCell class]]) {
                cell = (LPTableViewCell *)currentObject;
                break;
            }
        }
    }
    
    TMELesson *lesson = [self.consolidateLessons objectAtIndex:[tableView.index intValue]];
    TMELessonPageShort *lessonPage = [lesson.lesson_pages objectAtIndex:indexPath.row];
    
    UIFont *titleFont = [TeamieGlobals appFontFor:@"regularFontWithSize14"];
    CGSize expectedSize = [lessonPage.title sizeWithFont:titleFont];
    CGPoint oldCenter = cell.titleLabel.center;
    
    CGRect frame = cell.titleLabel.frame;
    frame.size = expectedSize;
    
    cell.titleLabel.font = titleFont;
    cell.titleLabel.text = lessonPage.title;
    cell.titleLabel.frame = frame;
    CGPoint newCenter = CGPointMake(cell.titleLabel.center.x, oldCenter.y);
    cell.titleLabel.center = newCenter;
    if(!lessonPage.status){
        [cell.PublishStatusLabel configurePublishStatusLabelWithStatus:PublishStatusDrafted];
    }
    else{
        if (lessonPage.is_Read.boolValue == 1) {
            [cell.PublishStatusLabel removeFromSuperview];
            UIImage *tick = [TeamieUIGlobals defaultPicForPurpose:@"checkmark" withSize:10 andColor:[UIColor blackColor]];
            cell.tickImageView.image = tick;
            cell.tickImageView.contentMode = UIViewContentModeScaleAspectFill;
        }
        else{
            [cell.PublishStatusLabel removeFromSuperview];
        }
    }
    
    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 1)];
    separatorLineView.backgroundColor = [UIColor colorWithHex:@"#dddddd" alpha:1.0];
    [cell.contentView addSubview:separatorLineView];
    
    return cell;
}

- (UIView *)uIViewForEmptyListWithTitleText:(NSString *)title subtitleText:(NSString *)subtitle andImage:(UIImage *)image {
    // TODO: Add a call to action button in next release
    NSArray *viewsToRemove = [self.view subviews];
    for (UIView *tempView in viewsToRemove) {
        [tempView removeFromSuperview];
    }
    UIView *containerView = [[UIView alloc] initWithFrame:self.emptyView.frame];
    UIImageView *emptyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMidX(containerView.frame), 0, 100.0, 100.0)];
    UILabel *titleText = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(containerView.frame), CGRectGetMaxY(emptyImageView.frame) + 5.0, CGRectGetWidth(containerView.frame), 20.0)];
    UILabel *subtitleText = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMidX(containerView.frame), CGRectGetMaxY(titleText.frame) + 5.0, CGRectGetWidth(containerView.frame), 20.0)];
    
    [self.emptyView addSubview:containerView];
    
    [containerView addSubview:emptyImageView];
    [containerView addSubview:titleText];
    [containerView addSubview:subtitleText];
    
    [emptyImageView mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.height.equalTo(@100);
        make.top.centerX.equalTo(containerView);
    }];
    
    [titleText mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(containerView);
        make.height.equalTo(@20);
        make.top.equalTo(emptyImageView.mas_bottom).with.offset(10.0);
        make.centerX.equalTo(emptyImageView);
    }];
    
    [subtitleText mas_makeConstraints:^(MASConstraintMaker *make){
        make.width.equalTo(containerView);
        make.height.greaterThanOrEqualTo(@20);
        make.top.equalTo(titleText.mas_bottom).with.offset(5.0);
        make.centerX.equalTo(emptyImageView);
    }];
    
    titleText.font = [TeamieGlobals appFontFor:@"emptyListTitleText"];
    subtitleText.font = [TeamieGlobals appFontFor:@"emptyListSubtitleText"];
    titleText.textAlignment = NSTextAlignmentCenter;
    subtitleText.textAlignment = NSTextAlignmentCenter;
    subtitleText.numberOfLines = 0;
    
    emptyImageView.image = image?image:[TeamieUIGlobals defaultPicForPurpose:@"emptylist" withSize:100.0];
    titleText.text = title?title:@"No Data";
    subtitleText.text = subtitle?subtitle:@"You should probably do some activity";
    
    [self.view insertSubview:self.emptyView atIndex:0];
    
    [containerView mas_updateConstraints:^(MASConstraintMaker *make){
        CGSize size = CGSizeMake(CGRectGetWidth(self.view.bounds), CGRectGetHeight(emptyImageView.frame) + CGRectGetHeight(titleText.frame) + CGRectGetHeight(subtitleText.frame));
        make.width.equalTo(@(size.width));
        make.height.equalTo(@(size.height));
        make.center.equalTo(self.view);
    }];
    return containerView;
}

- (void)removeEmptyView {
    [self.emptyView removeFromSuperview];
}

- (UIView *)emptyView {
    if (!_emptyView) {
        _emptyView = [[UIView alloc] initWithFrame:self.view.bounds];
        _emptyView.backgroundColor = [TeamieUIGlobalColors genericBackgroundColor];
    }
    return _emptyView;
}


@end
