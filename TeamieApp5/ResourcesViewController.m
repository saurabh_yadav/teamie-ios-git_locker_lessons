//
//  ResourcesViewController.m
//  TeamieApp5
//
//  Created by Raunak on 12/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ResourcesViewController.h"
#import "TeamieWebViewController.h"
#import "TSMessage.h"
#import "UIImage-Extensions.h"
#import <TapkuLibrary/TKEmptyView.h>
#import <AFNetworking/AFHTTPRequestOperation.h>

@interface ResourcesTableViewCell : UITableViewCell
@end

@implementation ResourcesTableViewCell

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(0, 0, 60, 50);
    self.imageView.contentMode = UIViewContentModeCenter;
    self.textLabel.frame = CGRectMake(CGRectGetMaxX(self.imageView.frame) + 5.0, CGRectGetMinY(self.textLabel.frame), CGRectGetWidth(self.textLabel.frame), CGRectGetHeight(self.textLabel.frame));
    self.detailTextLabel.frame = CGRectMake(CGRectGetMaxX(self.imageView.frame) + 5.0, CGRectGetMinY(self.detailTextLabel.frame), CGRectGetWidth(self.detailTextLabel.frame), CGRectGetHeight(self.detailTextLabel.frame));
}

@end

@interface ResourcesViewController () {
    BOOL imagesListHidden;
    BOOL linksListHidden;
    BOOL filesListHidden;
    BOOL didFinishLoad;
}
@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) NSMutableArray* resourcesList;
@property (nonatomic, strong) NSMutableArray* downloadingList;
@property (nonatomic, strong) NSMutableArray* resourcesMapping;
@property (nonatomic, strong) NSMutableArray* imagesList;
@property (nonatomic, strong) NSMutableArray* linksList;
@property (nonatomic, strong) NSMutableArray* filesList;
@property (nonatomic, strong) UIBarButtonItem* editButton;
@property (nonatomic, strong) UIBarButtonItem* settingsButton;
@property (nonatomic, strong) TKEmptyView* emptyView;
@property (nonatomic, strong) UISegmentedControl* segmentedControl;
@property (nonatomic, strong) WEPopoverController* settingsController;
@end

@implementation ResourcesViewController

#define ATTACHMENT_LINK_TITLE @"title"
#define ATTACHMENT_LINK_DESCRIPTION @"desc"
#define ATTACHMENT_HREF @"href"
#define ATTACHMENT_IMAGE_THUMBNAIL @"thumbnail"
#define ATTACHMENT_TYPE @"type"
#define ATTACHMENT_FILE_ICON @"iconURL"
#define ATTACHMENT_FILE_PREVIEW @"previewURL"
#define ATTACHMENT_THOUGHT_MESSAGE @"thoughtMessage"
#define ATTACHMENT_THOUGHT_AUTHOR @"thoughtAuthorName"
#define ATTACHMENT_THOUGHT_CLASSROOM @"thoughtClassroomName"
#define ATTACHMENT_THOUGHT_ID @"tid"

static ResourcesViewController* controller = nil;

#pragma mark - Singleton Methods

+ (ResourcesViewController*)sharedController {
    @synchronized([ResourcesViewController class]) {
        if (!controller) {
            controller = [[self alloc] init];
        }
        return controller;
    }
    return nil;
}

+ (id)alloc {
    @synchronized([ResourcesViewController class]) {
        NSAssert(controller == nil, @"Attempted to allocate a second instance of a singleton.");
        controller = [super alloc];
        return controller;
    }
    return nil;
}

- (id)init {
    self = [super init];
    if (self) {
        _resourcesList = [NSMutableArray array];
        _resourcesMapping = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Public Methods

- (void)resetSharedController {
    self.resourcesList = nil;
    self.resourcesList = [NSMutableArray array];
    self.resourcesMapping = nil;
    self.resourcesMapping = [NSMutableArray array];
}

- (AddResourceStatus)addResource:(NewsfeedItemAttachment*)itemAttachment {
    [self loadFromFile];
    if (itemAttachment != nil) {
        
        NSDictionary* dict = [NSMutableDictionary dictionary];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.title == nil ? @"" : itemAttachment.title) forKey:ATTACHMENT_LINK_TITLE];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.desc == nil ? @"" : itemAttachment.desc) forKey:ATTACHMENT_LINK_DESCRIPTION];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.href == nil ? @"" : itemAttachment.href) forKey:ATTACHMENT_HREF];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.thumbnail == nil ? @"" : itemAttachment.thumbnail) forKey:ATTACHMENT_IMAGE_THUMBNAIL];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.type == nil ? @"" : itemAttachment.type) forKey:ATTACHMENT_TYPE];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.iconURL == nil ? @"" : itemAttachment.iconURL) forKey:ATTACHMENT_FILE_ICON];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.previewURL == nil ? @"" : itemAttachment.previewURL) forKey:ATTACHMENT_FILE_PREVIEW];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.newsfeedItem.message == nil ? @"" : itemAttachment.newsfeedItem.message) forKey:ATTACHMENT_THOUGHT_MESSAGE];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.newsfeedItem.author.fullname == nil ? @"" :  itemAttachment.newsfeedItem.author.fullname) forKey:ATTACHMENT_THOUGHT_AUTHOR];
        [((NSMutableDictionary*)dict) setObject:(itemAttachment.newsfeedItem.classrooms.name == nil ? @"" : itemAttachment.newsfeedItem.classrooms.name) forKey:ATTACHMENT_THOUGHT_CLASSROOM];
        [((NSMutableDictionary*)dict) setObject:itemAttachment.newsfeedItem.tid forKey:ATTACHMENT_THOUGHT_ID];
        
        if (![self.resourcesList containsObject:dict]) {
            if ([itemAttachment.type isEqualToString:@"image"] || [itemAttachment.type isEqualToString:@"file"]) {
                if (![self.downloadingList containsObject:dict]) {
                    [self.downloadingList addObject:dict];
                    [self downloadResource:dict];
                    return kDownloadInProgress;
                }
                else {
                    return kDownloadInProgress;
                }
            }
            else {
                [self.resourcesList addObject:dict];
                [self.resourcesMapping addObject:[NSNull null]];
                [self saveToFile];
                return kDownloadSuccessful;
            }
        }
    }
    return kDownloadFailed;
}

#pragma mark - View Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.title = TeamieLocalizedString(@"MENU_NAME_RESOURCES", nil);
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:self.editButton, self.settingsButton, nil];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.segmentedControl];    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.emptyView];
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"current_user_uid"]) {
        [[NSUserDefaults standardUserDefaults] setObject:self.apiCaller.currentUser.uid forKey:@"current_user_uid"];
    }
    
    imagesListHidden = NO;
    linksListHidden = NO;
    filesListHidden = NO;
    didFinishLoad = NO;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setTintColor:[UIColor colorWithHex:@"#BE202E" alpha:1.0]];
    if (!didFinishLoad) {
        [self performSelectorInBackground:@selector(loadViewController) withObject:nil];
    }
}

- (void)loadViewController {
    [self loadFromFile];
    [self repopulateLists];
    self.tableView.editing = NO;
    [self resizeTableView];
    [self reload];
    [self performSelectorOnMainThread:@selector(viewLoaded) withObject:nil waitUntilDone:NO];
}

- (void)viewLoaded {
    didFinishLoad = YES;
}

- (void)viewDidDisappear:(BOOL)animated {
    [self saveToFile];
    self.downloadingList = nil;
//    self.imagesList = nil;
//    self.linksList = nil;
//    self.filesList = nil;
    self.editButton = nil;
    didFinishLoad = NO;
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Delegate Methods

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView
           editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        NSDictionary* dict;
        switch (indexPath.section) {
            case 0:
                dict = [self.imagesList objectAtIndex:indexPath.row];
                break;
            case 1:
                dict = [self.linksList objectAtIndex:indexPath.row];
                break;
            case 2:
                dict = [self.filesList objectAtIndex:indexPath.row];
                break;
            default:
                break;
        }
        NSString* fileName = [self.resourcesMapping objectAtIndex:[self.resourcesList indexOfObject:dict]];
        NSError* error = nil;
        if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], fileName]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], fileName] error:&error];
        }
        [self.resourcesMapping removeObjectAtIndex:[self.resourcesList indexOfObject:dict]];
        [self.resourcesList removeObject:dict];
        [self repopulateLists];
        [self saveToFile];
        [self reload];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger count = 0;
    [self resizeTableView];
    switch (section) {
        case 0:
            count = imagesListHidden ? 0 : [self.imagesList count];
            break;
        case 1:
            count = linksListHidden ? 0 : [self.linksList count];
            break;
        case 2:
            count = filesListHidden ? 0 : [self.filesList count];
            break;            
        default:
            break;
    }
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return ([self.imagesList count] > 0 && !imagesListHidden ? 20.0 : 0);
            break;
        case 1:
            return ([self.linksList count] > 0 && !linksListHidden ? 20.0 : 0);
            break;
        case 2:
            return ([self.filesList count] > 0 && !filesListHidden ? 20.0 : 0);
            break;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50.0;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return ([self.imagesList count] > 0 && !imagesListHidden ? @"Images" : nil);
            break;
        case 1:
            return ([self.linksList count] > 0 && !linksListHidden ? @"Links" : nil);
            break;
        case 2:
            return ([self.filesList count] > 0 && !filesListHidden ? @"Files" : nil);
            break;
        default:
            return nil;
            break;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"AttachmentCellIdentifier"];
    if (cell == nil) {
        cell = [[ResourcesTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"AttachmentCellIdentifier"];
    }
    NSString* fileName; 
    switch (indexPath.section) {
        case 0:
            cell.textLabel.text = [self stripTags:[((NSDictionary*)[self.imagesList objectAtIndex:indexPath.row]) objectForKey:ATTACHMENT_THOUGHT_MESSAGE]];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"By %@, %@",[((NSDictionary*)[self.imagesList objectAtIndex:indexPath.row]) objectForKey:ATTACHMENT_THOUGHT_AUTHOR], [((NSDictionary*)[self.imagesList objectAtIndex:indexPath.row]) objectForKey:ATTACHMENT_THOUGHT_CLASSROOM]];
            fileName = [self.resourcesMapping objectAtIndex:[self.resourcesList indexOfObject:[self.imagesList objectAtIndex:indexPath.row]]];
            cell.imageView.image = [[UIImage imageWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], fileName]] imageByScalingProportionallyToSize:CGSizeMake(60.0, 50.0)];
            break;
        case 1:
            cell.textLabel.text = [((NSDictionary*)[self.linksList objectAtIndex:indexPath.row]) objectForKey:ATTACHMENT_LINK_TITLE];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"By %@, %@",[((NSDictionary*)[self.linksList objectAtIndex:indexPath.row]) objectForKey:ATTACHMENT_THOUGHT_AUTHOR], [((NSDictionary*)[self.linksList objectAtIndex:indexPath.row]) objectForKey:ATTACHMENT_THOUGHT_CLASSROOM]];
            cell.imageView.image = [[UIImage imageNamed:@"icon-attached-link.png"] imageByScalingProportionallyToSize:CGSizeMake(60.0, 50.0)];
            break;
        case 2:
            cell.textLabel.text = [self stripTags:[((NSDictionary*)[self.filesList objectAtIndex:indexPath.row]) objectForKey:ATTACHMENT_THOUGHT_MESSAGE]];
            cell.detailTextLabel.text = [NSString stringWithFormat:@"By %@, %@",[((NSDictionary*)[self.filesList objectAtIndex:indexPath.row]) objectForKey:ATTACHMENT_THOUGHT_AUTHOR], [((NSDictionary*)[self.filesList objectAtIndex:indexPath.row]) objectForKey:ATTACHMENT_THOUGHT_CLASSROOM]];
            if (!(cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString: [((NSDictionary*)[self.filesList objectAtIndex:indexPath.row]) objectForKey:@"iconURL"]]]])) {
                cell.imageView.image = [TeamieUIGlobals defaultPicForPurpose:@"file" withSize:20.0 andColor:[UIColor blackColor]];
            }
            break;            
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray* arr;
    switch (indexPath.section) {
        case 0:
            arr = self.imagesList;
            break;
        case 1:
            arr = self.linksList;
            break;
        case 2:
            arr = self.filesList;
            break;
        default:
            break;
    }
    NSDictionary* attachment = (NSDictionary*)[arr objectAtIndex:indexPath.row];
    NSURL* url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], [self.resourcesMapping objectAtIndex:[self.resourcesList indexOfObject:attachment]]]];
    NSString* urlString = [url absoluteString];
    if ([[attachment objectForKey:ATTACHMENT_TYPE] isEqualToString:@"image"] && [attachment objectForKey:ATTACHMENT_HREF]) {
        // open the attached image
        // Open up the image view controller, pass the attachment info along
        ResourcesImageViewController* vc = [[ResourcesImageViewController alloc] init];
        [vc loadImageWithUrl:urlString WithCaption:[NSString stringWithFormat:@"%@: %@", [attachment objectForKey:ATTACHMENT_THOUGHT_AUTHOR], [self stripTags:[attachment objectForKey:ATTACHMENT_THOUGHT_MESSAGE]]] WithTitle:[NSString stringWithFormat:@"Shared by %@", [attachment objectForKey:ATTACHMENT_THOUGHT_AUTHOR]] WithThoughtID:[attachment objectForKey:ATTACHMENT_THOUGHT_ID] WithFilePath:url];
        vc.delegate = self;
        UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
        [self presentViewController:navigationController animated:YES completion:nil];
    }
    else {
        NSString* linkToOpen = nil;
        if ([[attachment objectForKey:ATTACHMENT_TYPE] isEqualToString:@"link"] && [attachment objectForKey:ATTACHMENT_HREF]) {
            // the thought's attached link was pressed
            linkToOpen = [attachment objectForKey:ATTACHMENT_HREF];
            // Open up the link inside an in-app browser
            ResourcesLinksViewController* webController = [[ResourcesLinksViewController alloc] init];
            [webController openURL:[NSURL URLWithString:linkToOpen] WithThoughtID:[attachment objectForKey:ATTACHMENT_THOUGHT_ID]];
            webController.delegate = self;
            webController.modalPresentationStyle = UIModalPresentationFullScreen;
            webController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webController];
            [self presentViewController:navigationController animated:YES completion:nil];
        }
        else if ([[attachment objectForKey:ATTACHMENT_TYPE] isEqualToString:@"file"] && [attachment objectForKey:ATTACHMENT_FILE_PREVIEW]) {
            ResourceWebViewController* vc = [[ResourceWebViewController alloc] init];
            [vc loadFileWithUrl:url WithTitle:[NSString stringWithFormat:@"Shared by %@", [attachment objectForKey:ATTACHMENT_THOUGHT_AUTHOR]]  WithThoughtID:[attachment objectForKey:ATTACHMENT_THOUGHT_ID]];
            vc.delegate = self;
            UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:vc];
            [self presentModalViewController:navigationController animated:YES];

        }
    }    
}

#pragma mark - Private Methods

- (void)reload {
    [self.tableView reloadData];
    self.emptyView.hidden = [self.resourcesList count] > 0;
    self.navigationItem.rightBarButtonItem.enabled = self.emptyView.hidden;
}

- (void)downloadResource:(NSDictionary*)dict {
    NSString* fileName = [NSString stringWithFormat:@"%@-%@-%f.%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"current_user_uid"], [dict objectForKey:ATTACHMENT_THOUGHT_ID], [[NSDate date] timeIntervalSince1970], [((NSString*)[dict objectForKey:ATTACHMENT_HREF]) pathExtension]];
    NSURLRequest* request = [NSURLRequest  requestWithURL:[NSURL URLWithString:[dict objectForKey:ATTACHMENT_HREF]]];
    AFHTTPRequestOperation* operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.outputStream = [NSOutputStream outputStreamToFileAtPath:[NSString stringWithFormat:@"%@/%@",[self getPathToDocumentsDirectory], fileName] append:NO];
    
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.resourcesList addObject:dict];
        [self.resourcesMapping addObject:fileName];
        [self.downloadingList removeObject:dict];
        [self saveToFile];
        [self repopulateLists];
        [self.tableView reloadData];
        self.emptyView.hidden = [self.resourcesList count] > 0;
        self.navigationItem.rightBarButtonItem.enabled = self.emptyView.hidden;
        
        [TSMessage dismissActiveNotification];
        [TeamieUIGlobals displayStatusMessage:@"Download Successful" forPurpose:@"ok"];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [TSMessage dismissActiveNotification];
        [TeamieUIGlobals displayStatusMessage:@"Download Failed" forPurpose:@"error"];
        if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], fileName]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], fileName] error:&error];
        }
    }];
    
    [operation start];
}

- (void)editButtonPressed:(id)sender {
    self.tableView.editing = !self.tableView.isEditing;
}

- (void)settingsButtonPressed:(id)sender {
    if(!self.settingsController) {
        ResourcesSettingsViewController* contentViewController = [[ResourcesSettingsViewController alloc] init];
        contentViewController.delegate = self;

        Class popoverClass;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            popoverClass = [UIPopoverController class];
        }
        else {
            popoverClass = [WEPopoverController class];
        }
        
        self.settingsController = [[popoverClass alloc] initWithContentViewController:contentViewController];
        self.settingsController.delegate = self;
        [self.settingsController presentPopoverFromBarButtonItem:(UIBarButtonItem*)sender permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown) animated:YES];
    }
    else {
        [self.settingsController dismissPopoverAnimated:YES];
        self.settingsController = nil;
    }

}

- (void)repopulateLists {
    NSMutableArray* arr;
    [self.imagesList removeAllObjects];
    [self.linksList removeAllObjects];
    [self.filesList removeAllObjects];
    for (NSDictionary* dict in self.resourcesList) {
        if ([[dict objectForKey:ATTACHMENT_TYPE] isEqualToString:@"image"]) {
            arr = self.imagesList;
        }
        else if ([[dict objectForKey:ATTACHMENT_TYPE] isEqualToString:@"link"]) {
            arr = self.linksList;
        }
        if ([[dict objectForKey:ATTACHMENT_TYPE] isEqualToString:@"file"]) {
            arr = self.filesList;
        }
        if (![arr containsObject:dict]) {
            [arr addObject:dict];
        }
    }
    [self updateSegmentControl];
}

- (void)updateSegmentControl {
    NSInteger index = self.segmentedControl.selectedSegmentIndex;
    if (index == 1 && [self.imagesList count] == 0) {
        self.segmentedControl .selectedSegmentIndex = 0;
        [self.segmentedControl sendActionsForControlEvents:UIControlEventValueChanged];
    }
    if (index == 2 && [self.linksList count] == 0) {
        self.segmentedControl.selectedSegmentIndex = 0;
        [self.segmentedControl sendActionsForControlEvents:UIControlEventValueChanged];
    }
    if (index == 3 && [self.filesList count] == 0) {
        self.segmentedControl.selectedSegmentIndex = 0;
        [self.segmentedControl sendActionsForControlEvents:UIControlEventValueChanged];
    }
    
    [self.segmentedControl setEnabled:([self.imagesList count] > 0) ? 1 : 0 forSegmentAtIndex:1];
    [self.segmentedControl setEnabled:([self.linksList count] > 0) ? 1 : 0 forSegmentAtIndex:2];
    [self.segmentedControl setEnabled:([self.filesList count] > 0) ? 1 : 0 forSegmentAtIndex:3];
}

- (void)resizeTableView {
    NSInteger count;
    if (!(imagesListHidden || linksListHidden || filesListHidden)) {
        count = [self.resourcesList count];
    }
    else if (!imagesListHidden) {
        count = [self.imagesList count];
    }
    else if (!linksListHidden) {
        count = [self.linksList count];
    }
    else {
        count = [self.filesList count];
    }
    CGRect frame = CGRectMake(self.tableView.frame.origin.x, self.tableView.frame.origin.y, self.tableView.frame.size.width, (CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.segmentedControl.frame) - 10.0) > (count * 50.0) + 60.0 ? (count * 50.0) + 60.0 : (CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.segmentedControl.frame) - 10.0)); 
    self.tableView.frame = frame;
}

- (void)segmentValueChanged:(id)sender {
    switch (self.segmentedControl.selectedSegmentIndex) {
        case 0:
            imagesListHidden = NO;
            linksListHidden = NO;
            filesListHidden = NO;
            break;
        case 1:
            imagesListHidden = NO;
            linksListHidden = YES;
            filesListHidden = YES;
            break;
        case 2:
            imagesListHidden = YES;
            linksListHidden = NO;
            filesListHidden = YES;
            break;
        case 3:
            imagesListHidden = YES;
            linksListHidden = YES;
            filesListHidden = NO;
            break;            
        default:
            break;
    }
    [self.tableView reloadData];
    
}

- (NSString*)stripTags:(NSString*)message {
    NSString* strippedMessage;
    
    // Removing [<email address>] from User Tag
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[.+?\\]" options:NSRegularExpressionCaseInsensitive error:nil];
    strippedMessage = [regex stringByReplacingMatchesInString:message options:0 range:NSMakeRange(0, [message length]) withTemplate:@""];
    
    // Removing "@" from User Tag
    strippedMessage = [strippedMessage stringByReplacingOccurrencesOfString:@"@" withString:@""];
    return strippedMessage;
}

#pragma mark - File Operations Methods

- (void)saveToFile {
    if ([self.resourcesList count] == 0 && [self.resourcesMapping count] == 0) {
        NSString *documentsPath = [self getPathToDocumentsDirectory];
        NSError* error = nil;
        if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/Resources-%@", documentsPath, [[NSUserDefaults standardUserDefaults] objectForKey:@"current_user_uid"]]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/Resources-%@", documentsPath, [[NSUserDefaults standardUserDefaults] objectForKey:@"current_user_uid"]] error:&error];
        }
    }
    else {
        NSString *documentsPath = [self getPathToDocumentsDirectory];
        NSMutableData *data  = [[NSMutableData alloc] init];
        NSKeyedArchiver* archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
        [archiver encodeObject:self.resourcesList forKey:@"resources_list"];
        [archiver encodeObject:self.resourcesMapping forKey:@"resources_mapping"];
        [archiver finishEncoding];
        [data writeToFile:[NSString stringWithFormat:@"%@/Resources-%@", documentsPath, [[NSUserDefaults standardUserDefaults] objectForKey:@"current_user_uid"]] atomically:YES];
    }
}

- (void)loadFromFile {
    NSString* documentsPath = [self getPathToDocumentsDirectory];
    NSData* data = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/Resources-%@", documentsPath, [[NSUserDefaults standardUserDefaults] objectForKey:@"current_user_uid"]]];
    NSKeyedUnarchiver* unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSMutableArray* arr = [unarchiver decodeObjectForKey:@"resources_list"];
    NSMutableArray* arr2 = [unarchiver decodeObjectForKey:@"resources_mapping"];
    [unarchiver finishDecoding];
    
    if (arr != nil && [arr count] > 0) {
        self.resourcesList = arr;
    }
    if (arr2 != nil && [arr2 count] > 0) {
        self.resourcesMapping = arr2;
    }
}

-(NSString*)getPathToDocumentsDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    return documentsPath;
}

#pragma mark - Resource Display Delegate Methods

- (void)resourcesImageControllerDidSelectShareOption:(ResourcesImageViewController*)imageController {
    [self dismissModalViewControllerAnimated:NO];
    [[NSUserDefaults standardUserDefaults] setObject:[NSData dataWithContentsOfURL:imageController.filePath] forKey:@"cache_thought_image"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"ShareBoxNavigationController"];
        [self presentViewController:vc animated:NO completion:nil];
    }
    else {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"ShareBoxViewController"];
        [self presentModalViewController:vc animated:NO];
    }
}

- (void)resourcesWebControllerDidSelectShareOption:(ResourceWebViewController *)webController {
    [self dismissModalViewControllerAnimated:NO];
    [[NSUserDefaults standardUserDefaults] setObject:[NSData dataWithContentsOfURL:webController.fileURL] forKey:@"cache_thought_file"];
    [[NSUserDefaults standardUserDefaults] setObject:[[webController.fileURL absoluteString] pathExtension] forKey:@"cache_thought_file_extension"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"ShareBoxNavigationController"];
        [self presentModalViewController:vc animated:NO];
    }
    else {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"ShareBoxViewController"];
        [self presentModalViewController:vc animated:NO];
    }
}

- (void)resourcesLinksControllerDidSelectShareOption:(ResourcesLinksViewController *)linksController {
    [self dismissModalViewControllerAnimated:NO];
    [[NSUserDefaults standardUserDefaults] setObject:[linksController.linkURL absoluteString] forKey:@"cache_thought_attached_link"];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPhone" bundle:nil] instantiateViewControllerWithIdentifier:@"ShareBoxNavigationController"];
        [self presentModalViewController:vc animated:NO];
    }
    else {
        UIViewController* vc = [[UIStoryboard storyboardWithName:@"MainStoryboard_iPad" bundle:nil] instantiateViewControllerWithIdentifier:@"ShareBoxViewController"];
        [self presentModalViewController:vc animated:NO];
    }
}

#pragma mark - ResourceSettingsController Delegate Methods

- (void)settingsControllerDidSelectDeleteOption:(SettingsDeleteOptions)option {
    [self.settingsController dismissPopoverAnimated:YES];
    self.settingsController = nil;
    if (option == kCurrentAccount) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Delete Resources" message:@"Are you sure you want to delete all resources from your current account?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
        alert.tag = kCurrentAccount;
        [alert show];
    }
    else if (option == kOtherAccounts) {
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Delete Resources" message:@"Are you sure you want to delete all resources from your other accounts?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Yes", nil];
        alert.tag = kOtherAccounts;
        [alert show];
    }
}

- (BOOL)settingsControllerShouldDisplayOtherAccountsOptions {
    NSPredicate* pred = [NSPredicate predicateWithFormat:@"self beginswith %@", @"Resources"];
    NSArray* filteredContents = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self getPathToDocumentsDirectory] error:nil] filteredArrayUsingPredicate:pred];
    
    if (filteredContents && [filteredContents count] > 0) {
        NSString* currentUID = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"current_user_uid"]];
        for (NSString* file in filteredContents) {
            NSString* fileName = [file lastPathComponent];
            NSString* uid = [[fileName componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"-"]] objectAtIndex:1];
            if (![uid isEqualToString:currentUID]) {
                return YES;
            }
        }
    }
    return NO;
}

- (BOOL)settingsControllerShouldDisplayCurrentAccountOption {
    return [self.resourcesList count] > 0;
}

#pragma mark - UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    }
    else {
        if (alertView.tag == kCurrentAccount) {
            [self deleteAllResources];
        }
        else {
            [self deleteOtherAccountResources];
        }
    }
}

#pragma mark - WEPopoverController Delegate Methods

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    self.settingsController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

#pragma mark - Resource Settings Helper Methods

- (void)deleteAllResources {
    for (NSDictionary* dict in self.resourcesList) {
        NSString* fileName = [self.resourcesMapping objectAtIndex:[self.resourcesList indexOfObject:dict]];
        NSError* error = nil;
        if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], fileName]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], fileName] error:&error];
        }
    }
    [self.resourcesMapping removeAllObjects];
    [self.resourcesList removeAllObjects];
    [self repopulateLists];
    [self saveToFile];
    [self reload];
}

- (void)deleteOtherAccountResources {
    NSPredicate* pred = [NSPredicate predicateWithFormat:@"self beginswith %@", @"Resources"];
    NSArray* filteredContents = [[[NSFileManager defaultManager] contentsOfDirectoryAtPath:[self getPathToDocumentsDirectory] error:nil] filteredArrayUsingPredicate:pred];
    if (filteredContents && [filteredContents count] > 0) {
        NSString* currentUID = [NSString stringWithFormat:@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"current_user_uid"]];
        for (NSString* file in filteredContents) {
            NSString* fileName = [file lastPathComponent];
            NSString* uid = [[fileName componentsSeparatedByCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"-"]] objectAtIndex:1];
            if (![uid isEqualToString:currentUID]) {
                [self deleteResourcesInFile:fileName];
            }
        }
    }
    [TeamieUIGlobals displayStatusMessage:@"Resources Deleted Successfully" forPurpose:@"ok"];
}

- (BOOL)deleteResourcesInFile:(NSString*)fileName {
    NSString* documentsPath = [self getPathToDocumentsDirectory];
    NSData* data = [NSData dataWithContentsOfFile:[NSString stringWithFormat:@"%@/%@", documentsPath, fileName]];
    NSKeyedUnarchiver* unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSMutableArray* tempResourcesMapping = [unarchiver decodeObjectForKey:@"resources_mapping"];
    [unarchiver finishDecoding];
    
    NSError* error = nil;
    for (NSString* filePath in tempResourcesMapping) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], filePath]]) {
            [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], filePath] error:&error];
        }
    }
    if ([[NSFileManager defaultManager] fileExistsAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], fileName]]) {
        [[NSFileManager defaultManager] removeItemAtPath:[NSString stringWithFormat:@"%@/%@", [self getPathToDocumentsDirectory], fileName] error:&error];
    }
    return (error == nil);
}

#pragma mark - Lazy Instantiation Methods

- (NSMutableArray*)downloadingList {
    if (!_downloadingList) {
        _downloadingList = [NSMutableArray array];
    }
    return _downloadingList;
}

- (NSMutableArray*)imagesList {
    if (!_imagesList) {
        _imagesList = [NSMutableArray array];
    }
    return _imagesList;
}

- (NSMutableArray*)linksList {
    if (!_linksList) {
        _linksList = [NSMutableArray array];
    }
    return _linksList;
}

- (NSMutableArray*)filesList {
    if (!_filesList) {
        _filesList = [NSMutableArray array];
    }
    return _filesList;
}

- (UISegmentedControl*)segmentedControl {
    if (!_segmentedControl) {
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"All", @"Images", @"Links", @"Files", nil]];
        _segmentedControl.frame = CGRectMake(CGRectGetMinX(self.view.bounds) + 5.0, CGRectGetMinY(self.view.bounds) + 5.0, CGRectGetWidth(self.view.bounds) - 10.0, 30.0);
        _segmentedControl.segmentedControlStyle = UISegmentedControlStyleBar;
        _segmentedControl.selectedSegmentIndex = 0;
        _segmentedControl.tintColor = [UIColor colorWithHex:@"#BE202E" alpha:1.0];
        [_segmentedControl addTarget:self action:@selector(segmentValueChanged:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentedControl;
}

- (UITableView*)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.view.bounds), CGRectGetMaxY(self.segmentedControl.frame) + 5.0, CGRectGetWidth(self.view.bounds), CGRectGetHeight(self.view.bounds) - CGRectGetHeight(self.segmentedControl.frame) - 10.0) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.bounces = NO;
    }
    return _tableView;
}

- (TKEmptyView*)emptyView {
    if (!_emptyView) {
        _emptyView = [[TKEmptyView alloc] initWithFrame:self.view.bounds emptyViewImage:TKEmptyViewImageSearch title:@"No Resources" subtitle:nil];
        _emptyView.hidden = YES;
    }
    return _emptyView;
}

- (UIBarButtonItem*)editButton {
    if (!_editButton) {
        _editButton = [[UIBarButtonItem alloc] initWithTitle:TeamieLocalizedString(@"BTN_EDIT", nil) style:UIBarButtonItemStylePlain target:self action:@selector(editButtonPressed:)];
    }
    return _editButton;
}

- (UIBarButtonItem*)settingsButton {
    if (!_settingsButton) {
        _settingsButton = [[UIBarButtonItem alloc] initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"settings" withSize:20.0 andColor:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(settingsButtonPressed:)];
    }
    return _settingsButton;
}

@end