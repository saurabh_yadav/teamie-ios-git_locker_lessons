//
//  ClassmatesViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 20/09/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"

@interface ClassmatesViewController : TableRestApiViewController <UITableViewDelegate>

@property (nonatomic, retain) NSArray* users;

@end
