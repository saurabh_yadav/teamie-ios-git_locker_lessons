//
//  TMESlideMenuItem.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 5/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, TMESlideMenuType) {
    TMESlideMenuTypeProfile,
    TMESlideMenuTypeNewsfeed,
    TMESlideMenuTypeNotifications,
    TMESlideMenuTypeUpcomingEvents,
    TMESlideMenuTypeMessages,
    TMESlideMenuTypeClassroomNewsfeed,
    TMESlideMenuTypeSwitchUser,
    TMESlideMenuTypeLogout,
    TMESlideMenuTypeLockerLessons,
    TMESlideMenuTypeNull
};

@interface TMESlideMenuItem : NSObject

@property (nonatomic) TMESlideMenuType menuType;
@property (nonatomic, strong) id data;
@property (nonatomic, strong) NSString *badgeText;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSURL *URL;

+ (instancetype)item:(TMESlideMenuType)menu text:(NSString *)text icon:(NSString *)icon;

@end
