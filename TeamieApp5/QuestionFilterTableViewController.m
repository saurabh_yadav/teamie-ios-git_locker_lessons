//
//  QuestionFilterTableViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 13/12/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuestionFilterTableViewController.h"
#import <objc/runtime.h>

@interface QuestionFilterTableViewController ()

@property (nonatomic,strong) NSMutableArray* selectedQuestions;
@property (nonatomic,strong) NSMutableArray* selectedUsers;
@property (nonatomic,strong) NSMutableDictionary* userMapping;
@property (nonatomic,strong) NSMutableDictionary* questionMapping;

@end

@implementation QuestionFilterTableViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.contentSizeForViewInPopover = CGSizeMake(350, 400);
    self.view.frame = CGRectMake(0, 0, 350, 400);
    
    // Somehow Lazy Initialization stopped working suddenly
    if (!_questionTable) {
        _questionTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 350, 200) style:UITableViewStylePlain];
        _questionTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _questionTable.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _questionTable.dataSource = self;
        _questionTable.delegate = self;
        _questionTable.bounces = NO;
        [_questionTable reloadData];
    }
    if (!_userTable) {
        _userTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 200, 350, 200) style:UITableViewStylePlain];
        _userTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _userTable.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
        _userTable.dataSource = self;
        _userTable.delegate = self;
        _userTable.bounces = NO;
        [_userTable reloadData];
    }

    [self.view addSubview:_questionTable];
    [self.view addSubview:_userTable];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _questionTable) {
        return ([self.questionPickerList count] + 1);
    }
    else if (tableView == _userTable) {
        return ([self.userPickerList count] + 1);
    }
    else {
        return 0;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (tableView == _questionTable) {
        return @"By Question";
    }
    else if (tableView == _userTable) {
        return @"By Submitter";
    }
    else {
        return @"";
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setBounds:CGRectMake(0, 0, tableView.frame.size.width - 20, 44)];
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleSelection:)];
        singleTapGestureRecognizer.numberOfTapsRequired = 1;
        [cell addGestureRecognizer:singleTapGestureRecognizer];
        
        if (tableView == _questionTable) {
            objc_setAssociatedObject(cell, "cellArray", self.selectedQuestions, OBJC_ASSOCIATION_ASSIGN);
        }
        else if (tableView == _userTable) {
            objc_setAssociatedObject(cell, "cellArray", self.selectedUsers, OBJC_ASSOCIATION_ASSIGN);
        }
        objc_setAssociatedObject(cell, "cellPicker", tableView, OBJC_ASSOCIATION_ASSIGN);
    }
    
    if (tableView == _questionTable) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"All Questions";
        }
        else {
            cell.textLabel.text = [NSString stringWithFormat:@"Question %@", ((NSNumber*)[self.questionPickerList objectAtIndex:indexPath.row - 1])];
        }
        
        if ([self.selectedQuestions indexOfObject:[NSNumber numberWithInt:indexPath.row]] != NSNotFound) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
    }
    else if (tableView == _userTable) {
        if (indexPath.row == 0) {
            cell.textLabel.text = @"All Users";
        }
        else {
            cell.textLabel.text = [[self.userPickerList allValues] objectAtIndex:indexPath.row - 1];
        }
        
        if ([self.selectedUsers indexOfObject:[NSNumber numberWithInt:indexPath.row]] != NSNotFound) {
            [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
        } else {
            [cell setAccessoryType:UITableViewCellAccessoryNone];
        }
    }
    
    cell.tag = indexPath.row;
    return cell;
}

- (void)toggleSelection:(UITapGestureRecognizer *)recognizer {
    NSNumber *row = [NSNumber numberWithInt:recognizer.view.tag];
    NSMutableArray* arr = (NSMutableArray*)objc_getAssociatedObject(recognizer.view, "cellArray");
    UITableView* tableView = (UITableView*)objc_getAssociatedObject(recognizer.view, "cellPicker");
    NSUInteger index = [arr indexOfObject:row];
    
    if ([row isEqualToNumber:[NSNumber numberWithInt:0]]) {
        if (index != NSNotFound) {
            [arr removeAllObjects];
        }
        else {
            for (int i = 0; i < [tableView numberOfRowsInSection:0]; i++) {
                if (![arr containsObject:[NSNumber numberWithInt:i]]) {
                    [arr addObject:[NSNumber numberWithInt:i]];
                }
            }
        }
    }
    else {
        if (index != NSNotFound) {
            [arr removeObjectAtIndex:index];
            if ([arr count] == [tableView numberOfRowsInSection:0] - 1) {
                [arr removeObjectAtIndex:[arr indexOfObject:[NSNumber numberWithInt:0]]];
            }
        }
        else {
            [arr addObject:row];
            if ([arr count] == [tableView numberOfRowsInSection:0] - 1) {
                [arr addObject:[NSNumber numberWithInt:0]];
            }
        }
    }
    [tableView reloadData];
    [TeamieGlobals addHUDTextOnlyLabel:@"Loading" margin:10.0 yOffset:10.0];
    [self performSelector:@selector(updateDisplayForPicker:) withObject:tableView afterDelay:0.5];
}

- (void)updateDisplayForPicker:(UITableView*)tableView {
    if (tableView == _questionTable) {
        NSMutableArray* selectedQIDs = [NSMutableArray array];
        for (NSNumber* key in self.selectedQuestions) {
            if (![key isEqualToNumber:[NSNumber numberWithInt:0]]) {
                [selectedQIDs addObject:[self.questionMapping objectForKey:key]];
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"QuestionFilterChanged" object:self userInfo:[NSDictionary dictionaryWithObject:selectedQIDs forKey:@"filterArray"]];
    }
    else if (tableView == _userTable) {
        NSMutableArray* selectedUIDs = [NSMutableArray array];
        for (NSNumber* key in self.selectedUsers) {
            if (![key isEqualToNumber:[NSNumber numberWithInt:0]]) {
                [selectedUIDs addObject:[self.userMapping objectForKey:key]];
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserFilterChanged" object:self userInfo:[NSDictionary dictionaryWithObject:selectedUIDs forKey:@"filterArray"]];
    }
}

- (NSArray*)getSelectedQIDs {
    NSMutableArray* selectedQIDs = [NSMutableArray array];
    for (NSNumber* key in self.selectedQuestions) {
        if (![key isEqualToNumber:[NSNumber numberWithInt:0]]) {
            [selectedQIDs addObject:[self.questionMapping objectForKey:key]];
        }
    }
    return selectedQIDs;
}

- (NSArray*)getSelectedUIDs {
    NSMutableArray* selectedUIDs = [NSMutableArray array];
    for (NSNumber* key in self.selectedUsers) {
        if (![key isEqualToNumber:[NSNumber numberWithInt:0]]) {
            [selectedUIDs addObject:[self.userMapping objectForKey:key]];
        }
    }
    return selectedUIDs;
}

- (void)selectAllUsers {
    UITableView* userTableView;
    UITableViewCell* cell = [self.userTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    for (UIView* v in cell.subviews) {
        if ([v isKindOfClass:[UITableView class]]) {
            userTableView = (UITableView*)v;
        }
    }
    
    for (int i = 0; i < [userTableView numberOfRowsInSection:0]; i++) {
        if (![self.selectedUsers containsObject:[NSNumber numberWithInt:i]]) {
            [self.selectedUsers addObject:[NSNumber numberWithInt:i]];
        }
    }
    [_userTable reloadData];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableView*)questionTable {
    if (!_questionTable) {
        _questionTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 350, 200) style:UITableViewStylePlain];
        _questionTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _questionTable.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        _questionTable.dataSource = self;
        _questionTable.delegate = self;
        _questionTable.bounces = NO;
        [_questionTable reloadData];
    }
    return _questionTable;
}

- (UITableView*)userTable {
    if (!_userTable) {
        _userTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 200, 350, 200) style:UITableViewStylePlain];
        _userTable.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        _userTable.autoresizingMask =  UIViewAutoresizingFlexibleWidth;
        _userTable.dataSource = self;
        _userTable.delegate = self;
        _userTable.bounces = NO;
        [_userTable reloadData];
    }
    return _userTable;
}

- (NSMutableArray*)selectedQuestions {
    if (!_selectedQuestions) {
        _selectedQuestions = [NSMutableArray array];
        
        //Select All Questions on first load
        for (int i = 0; i < [self.questionPickerList count] + 1; i++) {
            [_selectedQuestions addObject:[NSNumber numberWithInt:i]];
        }
    }
    return _selectedQuestions;
}

- (NSMutableArray*)selectedUsers {
    if (!_selectedUsers) {
        _selectedUsers = [NSMutableArray array];
        
        //Select All Users on first load
        for (int i = 0; i < [self.userPickerList count] + 1; i++) {
            [_selectedUsers addObject:[NSNumber numberWithInt:i]];
        }
    }
    return _selectedUsers;
}

- (NSMutableDictionary*)questionMapping {
    if (!_questionMapping) {
        _questionMapping = [NSMutableDictionary dictionary];
        
        //Add mapping for question number to index
        for (int i = 1; i < [self.questionPickerList count] + 1; i++) {
            [_questionMapping setObject:[self.questionPickerList objectAtIndex:i - 1] forKey:[NSNumber numberWithInt:i]];
        }
    }
    return _questionMapping;
}

- (NSMutableDictionary*)userMapping {
    if (!_userMapping) {
        _userMapping = [NSMutableDictionary dictionary];
        
        //Add mapping for uid to index
        for (int i = 1; i < [self.userPickerList count] + 1; i++) {
            [_userMapping setObject:[[self.userPickerList allKeys] objectAtIndex:i - 1] forKey:[NSNumber numberWithInt:i]];
        }
    }
    return _userMapping;
}

@end
