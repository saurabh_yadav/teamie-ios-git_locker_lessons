//
//  QuizDefaultersListViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 10/09/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuizDefaultersListViewController.h"

@interface QuizDefaultersListViewController () <UITableViewDelegate, UITableViewDataSource>

@property NSNumber* quizId;
@property NSMutableArray* userArray;
@property UITableView* tableView;

@end

@implementation QuizDefaultersListViewController

- (id)initWithQuizId:(NSNumber *)qid {
    self = [super init];
    
    if (self) {
        _quizId = qid;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.apiCaller performRestRequest:TeamieUserQuizDefaultersList withParams:[NSDictionary dictionaryWithObjectsAndKeys:@"defaulters", @"type", _quizId, @"quiz_id", nil]];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 540, 620) style:UITableViewStylePlain];
    _tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
    
    [self.tableView registerClass: [UITableViewCell class] forCellReuseIdentifier:@"UserCell"];

    _userArray = [[NSMutableArray alloc] init];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [TeamieGlobals cancelTeamieRestRequests];
    [super viewWillDisappear:animated];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_userArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"UserCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if ([_userArray count]) {
        NSDictionary* user = [_userArray objectAtIndex:indexPath.row];

        cell.textLabel.text = [user valueForKey:@"real_name"] ;
        cell.imageView.image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:[[user valueForKey:@"user_profile_image"] valueForKey:@"path"]]]];
        
    }
    return cell;
}

#pragma mark - RestApi Delegate

- (void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    
    if (requestName == TeamieUserQuizDefaultersList) {
        
        for (NSDictionary* user in response) {
            [_userArray addObject:user];
        }
        
        [self.tableView reloadData];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapBehind:)];
    
    [recognizer setNumberOfTapsRequired:1];
    recognizer.cancelsTouchesInView = NO; //So the user can still interact with controls in the modal view
    [self.view.window addGestureRecognizer:recognizer];
}

- (void)handleTapBehind:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint location = [sender locationInView:nil]; //Passing nil gives us coordinates in the window
        
        //Then we convert the tap's location into the local view's coordinate system, and test to see if it's in or outside. If outside, dismiss the view.
        
        if (![self.view pointInside:[self.view convertPoint:location fromView:self.view.window] withEvent:nil])
        {
            // Remove the recognizer first so it's view.window is valid.
            [self.view.window removeGestureRecognizer:sender];
            [self dismissModalViewControllerAnimated:YES];
        }
    }
}

@end