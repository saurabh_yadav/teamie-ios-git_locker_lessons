//
//  ResourcesViewController.h
//  TeamieApp5
//
//  Created by Raunak on 12/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "WEPopoverController.h"
#import "ResourcesSettingsViewController.h"
#import "ResourcesImageViewController.h"
#import "ResourceWebViewController.h"
#import "ResourcesLinksViewController.h"

typedef enum {
    kDownloadSuccessful = 0,
    kDownloadInProgress,
    kDownloadFailed
} AddResourceStatus;

@interface ResourcesViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, WEPopoverControllerDelegate, SettingsControllerDelegate, UIAlertViewDelegate,ResourcesImageControllerDelegate, ResourcesWebControllerDelegate, ResourcesLinksControllerDelegate>

+(ResourcesViewController*)sharedController;
- (AddResourceStatus)addResource:(NewsfeedItemAttachment*)itemAttachment;
- (void)resetSharedController;

@end
