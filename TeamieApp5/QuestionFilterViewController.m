//
//  QuestionFilterViewController.m
//  TeamieApp5
//
//  Created by Raunak on 12/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuestionFilterViewController.h"
#import <objc/runtime.h>

@interface QuestionFilterViewController ()

@property (nonatomic,strong) NSMutableArray* selectedQuestions;
@property (nonatomic,strong) NSMutableArray* selectedUsers;
@property (nonatomic,strong) NSMutableDictionary* userMapping;
@property (nonatomic,strong) NSMutableDictionary* questionMapping;

@end

@implementation QuestionFilterViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.    

    self.contentSizeForViewInPopover = self.filterTable.contentSize;
    self.view = self.filterTable;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 216;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return @"By Question";
            break;
            
        case 1:
            return @"By Submitter";
            break;
            
        default:
            return @"";
            break;
    }
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *MyIdentifier = @"MyReuseIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:MyIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:MyIdentifier];
    }

    UIPickerView* picker = [[UIPickerView alloc] init];
    picker.frame = CGRectMake(10, 0, CGRectGetWidth(self.filterTable.frame) - 20, 216);
    picker.delegate = self;
    picker.dataSource = self;
    picker.tag = indexPath.section;
    
    [cell addSubview:picker];
    return cell;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (pickerView.tag) {
        case 0:
            return ([self.questionPickerList count] + 1);
            break;
        case 1:
            return ([self.userPickerList count] + 1);
            break;
        default:
            return 0;
            break;
    }
}

- (UIView*)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UITableViewCell* cell = (UITableViewCell*)view;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setBounds:CGRectMake(0, 0, pickerView.frame.size.width - 20, 44)];
        UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleSelection:)];
        singleTapGestureRecognizer.numberOfTapsRequired = 1;
        [cell addGestureRecognizer:singleTapGestureRecognizer];
        
        if (pickerView.tag == 0) {
            objc_setAssociatedObject(cell, "cellArray", self.selectedQuestions, OBJC_ASSOCIATION_ASSIGN);
        }
        else if (pickerView.tag == 1) {
            objc_setAssociatedObject(cell, "cellArray", self.selectedUsers, OBJC_ASSOCIATION_ASSIGN);
        }
        objc_setAssociatedObject(cell, "cellPicker", pickerView, OBJC_ASSOCIATION_ASSIGN);
    }

    switch (pickerView.tag) {
        case 0:
            if (row == 0) {
                cell.textLabel.text = @"All Questions";
            }
            else {
                cell.textLabel.text = [NSString stringWithFormat:@"Question %@", ((NSNumber*)[self.questionPickerList objectAtIndex:row - 1])];
            }
            
            if ([self.selectedQuestions indexOfObject:[NSNumber numberWithInt:row]] != NSNotFound) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            } else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            break;
        case 1:
            if (row == 0) {
                cell.textLabel.text = @"All Users";
            }
            else {
                cell.textLabel.text = [[self.userPickerList allValues] objectAtIndex:row - 1];
            }
            
            if ([self.selectedUsers indexOfObject:[NSNumber numberWithInt:row]] != NSNotFound) {
                [cell setAccessoryType:UITableViewCellAccessoryCheckmark];
            } else {
                [cell setAccessoryType:UITableViewCellAccessoryNone];
            }
            break;
        default:
            break;
    }
    cell.tag = row;
    return cell;
}

- (void)toggleSelection:(UITapGestureRecognizer *)recognizer {
    NSNumber *row = [NSNumber numberWithInt:recognizer.view.tag];
    NSMutableArray* arr = (NSMutableArray*)objc_getAssociatedObject(recognizer.view, "cellArray");
    UIPickerView* pickerView = (UIPickerView*)objc_getAssociatedObject(recognizer.view, "cellPicker");
    NSUInteger index = [arr indexOfObject:row];

    if ([row isEqualToNumber:[NSNumber numberWithInt:0]]) {
        if (index != NSNotFound) {
            [arr removeAllObjects];
        }
        else {
            for (int i = 0; i < [pickerView numberOfRowsInComponent:0]; i++) {
                if (![arr containsObject:[NSNumber numberWithInt:i]]) {
                    [arr addObject:[NSNumber numberWithInt:i]];
                }
            }
        }
    }
    else {  
        if (index != NSNotFound) {
            [arr removeObjectAtIndex:index];
            if ([arr count] == [pickerView numberOfRowsInComponent:0] - 1) {
                [arr removeObjectAtIndex:[arr indexOfObject:[NSNumber numberWithInt:0]]];
            }
        }
        else {
            [arr addObject:row];
            if ([arr count] == [pickerView numberOfRowsInComponent:0] - 1) {
                [arr addObject:[NSNumber numberWithInt:0]];
            }
        }
    }
    [pickerView reloadAllComponents];
    [TeamieGlobals addHUDTextOnlyLabel:@"Loading" margin:10.0 yOffset:10.0];
    [self performSelector:@selector(updateDisplayForPicker:) withObject:pickerView afterDelay:0.5];
}

- (void)updateDisplayForPicker:(UIPickerView*)pickerView {
    if (pickerView.tag == 0) {
        NSMutableArray* selectedQIDs = [NSMutableArray array];
        for (NSNumber* key in self.selectedQuestions) {
            if (![key isEqualToNumber:[NSNumber numberWithInt:0]]) {
                [selectedQIDs addObject:[self.questionMapping objectForKey:key]];
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"QuestionFilterChanged" object:self userInfo:[NSDictionary dictionaryWithObject:selectedQIDs forKey:@"filterArray"]];
    }
    else if (pickerView.tag == 1) {
        NSMutableArray* selectedUIDs = [NSMutableArray array];
        for (NSNumber* key in self.selectedUsers) {
            if (![key isEqualToNumber:[NSNumber numberWithInt:0]]) {
                [selectedUIDs addObject:[self.userMapping objectForKey:key]];
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserFilterChanged" object:self userInfo:[NSDictionary dictionaryWithObject:selectedUIDs forKey:@"filterArray"]];
    }
}

- (NSArray*)getSelectedQIDs {
    NSMutableArray* selectedQIDs = [NSMutableArray array];
    for (NSNumber* key in self.selectedQuestions) {
        if (![key isEqualToNumber:[NSNumber numberWithInt:0]]) {
            [selectedQIDs addObject:[self.questionMapping objectForKey:key]];
        }
    }
    return selectedQIDs;
}

- (NSArray*)getSelectedUIDs {
    NSMutableArray* selectedUIDs = [NSMutableArray array];
    for (NSNumber* key in self.selectedUsers) {
        if (![key isEqualToNumber:[NSNumber numberWithInt:0]]) {
            [selectedUIDs addObject:[self.userMapping objectForKey:key]];
        }
    }
    return selectedUIDs;
}

- (void)selectAllUsers {
    UIPickerView* userPicker;
    UITableViewCell* cell = [self.filterTable cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    for (UIView* v in cell.subviews) {
        if ([v isKindOfClass:[UIPickerView class]]) {
            userPicker = (UIPickerView*)v;
        }
    }
    
    for (int i = 0; i < [userPicker numberOfRowsInComponent:0]; i++) {
        if (![self.selectedUsers containsObject:[NSNumber numberWithInt:i]]) {
            [self.selectedUsers addObject:[NSNumber numberWithInt:i]];
        }
    }
    [userPicker reloadAllComponents];    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableView*)filterTable {
    if (!_filterTable) {
        _filterTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 350, 200) style:UITableViewStyleGrouped];
        _filterTable.separatorStyle = UITableViewCellSeparatorStyleNone;
        _filterTable.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        _filterTable.dataSource = self;
        _filterTable.delegate = self;
        _filterTable.bounces = NO;
        [_filterTable reloadData];
    }
    return _filterTable;
}

- (NSMutableArray*)selectedQuestions {
    if (!_selectedQuestions) {
        _selectedQuestions = [NSMutableArray array];
        
        //Select All Questions on first load
        for (int i = 0; i < [self.questionPickerList count] + 1; i++) {
            [_selectedQuestions addObject:[NSNumber numberWithInt:i]];
        }
    }
    return _selectedQuestions;
}

- (NSMutableArray*)selectedUsers {
    if (!_selectedUsers) {
        _selectedUsers = [NSMutableArray array];
        
        //Select All Users on first load
        for (int i = 0; i < [self.userPickerList count] + 1; i++) {
            [_selectedUsers addObject:[NSNumber numberWithInt:i]];
        }
    }
    return _selectedUsers;
}

- (NSMutableDictionary*)questionMapping {
    if (!_questionMapping) {
        _questionMapping = [NSMutableDictionary dictionary];
        
        //Add mapping for question number to index
        for (int i = 1; i < [self.questionPickerList count] + 1; i++) {
            [_questionMapping setObject:[self.questionPickerList objectAtIndex:i - 1] forKey:[NSNumber numberWithInt:i]];
        }
    }
    return _questionMapping;
}

- (NSMutableDictionary*)userMapping {
    if (!_userMapping) {
        _userMapping = [NSMutableDictionary dictionary];
        
        //Add mapping for uid to index
        for (int i = 1; i < [self.userPickerList count] + 1; i++) {
            [_userMapping setObject:[[self.userPickerList allKeys] objectAtIndex:i - 1] forKey:[NSNumber numberWithInt:i]];
        }
    }
    return _userMapping;
}


@end
