//
//  ImageObject.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 11/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"

@interface TMEImage : TMEModel

@property (nonatomic,retain) NSString *html;
@property (nonatomic,retain) NSURL *path;
@property (nonatomic,retain) NSNumber *width;
@property (nonatomic,retain) NSNumber *height;
@end
