//
//  LPTOCViewController.h
//  LS2
//
//  Created by Wei Wenbo on 4/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LPTOCTableViewCell.h"
#import "TableRestApiViewController.h"
#import "LessonObject.h"
@interface LPTOCViewController : TableRestApiViewController<SWTableViewCellDelegate>
- (id)initWithLesson:(LessonObject *)lesson;
@end
