//
//  TMESlideMenuCell.m
//  TeamieApp5
//
//  Created by Teamie on 11/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMESlideMenuCell.h"
#import "UILabelWithPadding.h"
#import <Masonry/Masonry.h>
#import "UIView+TMEEssentials.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface TMESlideMenuCell()

@property (nonatomic, weak) UILabelWithPadding *badgeLabel;

@end

@implementation TMESlideMenuCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.font = [TeamieGlobals appFontFor:@"sidebarMenuTitle"];

        self.badgeLabel = [self.contentView createAndAddSubView:UILabelWithPadding.class];
        self.badgeLabel.font = [TeamieGlobals appFontFor:@"notificationsBadgeText"];
        self.badgeLabel.textColor = [TeamieUIGlobalColors notificationBadgeTextColor];
        self.badgeLabel.backgroundColor = [TeamieUIGlobalColors notificationBadgeBackgroundColor];
        self.badgeLabel.edgeInsets = UIEdgeInsetsMake(0, 3, 0, 3);
        self.badgeLabel.textAlignment = NSTextAlignmentCenter;
        self.badgeLabel.layer.cornerRadius = 2.0;
        self.badgeLabel.layer.masksToBounds = YES;
        
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;

        [self.badgeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.equalTo(self.contentView.mas_left).with.offset(244);
            make.centerY.equalTo(self.contentView);
        }];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(12, 12, 20, 20);
    self.textLabel.frame = CGRectMake(44, 12, CGRectGetWidth(self.contentView.frame) - 44, 20);
}

- (void)setData:(TMESlideMenuItem *)item {
    self.badgeLabel.hidden = item.badgeText.length == 0;
    self.badgeLabel.text = item.badgeText;
    if (item.URL) {
        [self.imageView sd_setImageWithURL:item.URL placeholderImage:[TeamieUIGlobals defaultPicForPurpose:item.icon withSize:20]];
    }
    else {
        self.imageView.image = [TeamieUIGlobals defaultPicForPurpose:item.icon withSize:20];
    }
    self.textLabel.text = item.text;
}

@end
