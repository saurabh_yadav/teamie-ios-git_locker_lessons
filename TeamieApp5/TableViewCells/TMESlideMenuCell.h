//
//  TMESlideMenuCell.h
//  TeamieApp5
//
//  Created by Teamie on 11/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMESlideMenuItem.h"

@interface TMESlideMenuCell : UITableViewCell

- (void)setData:(TMESlideMenuItem *)item;

@end
