//
//  UITextView+SizeAdjustment.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 5/23/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <objc/runtime.h>
#import "UITextView+SizeAdjustment.h"
#import "TeamieUIGlobalColors.h"

typedef NS_ENUM(NSUInteger, ShowMode) {
    kExpand,
    kCollapse
};

#define kNumTruncateCharsForExpandLabel 15
#define kNumTruncateCharsForEllipsis 4

#define kExpandLabelText @"Show more"
#define kCollapseLabelText @"Show less"
#define kEllipsis @"... "
#define kLabelAccessibilityLabel @"SeeMoreLessLabel"

#define kLabelTextSizeRatioToViewTextSize 1.0

#define kChangeConstraintAnimationDuration 1.0

@interface UITextView ()

@property (nonatomic, strong) NSAttributedString* fullAttributedText;
@property (nonatomic, strong) NSNumber* maxNumLines;
@property (nonatomic, strong) UILabel* modeLabel;

@property (nonatomic, strong) NSLayoutConstraint* heightConstraint;

@end

@implementation UITextView (SizeAdjustment)

#pragma mark - Getters and Setters for properties
- (NSAttributedString*)fullAttributedText{
    return objc_getAssociatedObject(self, @selector(fullAttributedText));
}
- (void)setFullAttributedText:(NSAttributedString *)fullAttributedText{
    objc_setAssociatedObject(self, @selector(fullAttributedText), fullAttributedText, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSNumber*)maxNumLines{
    return objc_getAssociatedObject(self, @selector(maxNumLines));
}
- (void)setMaxNumLines:(NSNumber*)maxNumLines{
    objc_setAssociatedObject(self, @selector(maxNumLines), maxNumLines, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UILabel*)modeLabel{
    return objc_getAssociatedObject(self, @selector(modeLabel));
}
- (void)setModeLabel:(UILabel *)modeLabel{
    objc_setAssociatedObject(self, @selector(modeLabel), modeLabel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSLayoutConstraint*)heightConstraint{
    NSLayoutConstraint* hConstraint = objc_getAssociatedObject(self, @selector(heightConstraint));
    if (hConstraint == nil){
        // If height constraint is not set, find it and store it
        for (NSLayoutConstraint* constraint in self.constraints){
            if (constraint.firstItem == self && constraint.firstAttribute == NSLayoutAttributeHeight && constraint.secondItem == nil){
                hConstraint = constraint;
                [self setHeightConstraint:hConstraint];
                break;
            }
        }
    }
    
    return hConstraint;
}
- (void)setHeightConstraint:(NSLayoutConstraint *)heightConstraint{
    objc_setAssociatedObject(self, @selector(heightConstraint), heightConstraint, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Public API
- (void)fitWithMaxNumberOfLines:(int)maxNumLines{
    [self removePrevModeLabels];
    CGFloat prevHeightConstraint = self.heightConstraint.constant;
    
    self.maxNumLines = [NSNumber numberWithInt:maxNumLines];
    int contentLines = [self calcContentLines];
   
    if (contentLines > maxNumLines){
        // Save full text before truncating
        self.fullAttributedText = self.attributedText;
        
        [self truncateText];
        [self addLabelToChangeToMode:kExpand];
    }
    
    [self fitHeightConstraint];
    
    if (prevHeightConstraint != self.heightConstraint.constant){
        [self notifyDelegateOnHeightChange];
    }
}

- (void)reloadText{
    if (self.maxNumLines != nil){
        if (self.modeLabel == nil){
            [self fitWithMaxNumberOfLines:[self.maxNumLines intValue]];
        }
        else if ([self.modeLabel.text isEqualToString:kExpandLabelText]){
            [self collapseLabelDidSelected];
        }
        else{
            [self expandLabelDidSelected];
        }
    }
}

#pragma mark - Private Methods
- (void)removePrevModeLabels{
    for (UIView* view in self.subviews){
        if ([view.accessibilityLabel isEqualToString:kLabelAccessibilityLabel]){
            [view removeFromSuperview];
        }
    }
}

- (int)calcContentLines{
    CGSize contentSize = [self sizeThatFits:CGSizeMake(self.frame.size.width, FLT_MAX)];
    int contentLines = contentSize.height / self.font.lineHeight;
    
    return contentLines;
}

- (void)truncateText{
    // Work around for ios7 bug: characterRangeAtPoint always returns nil
//    [self select:self];
    [self setSelectedTextRange:nil];
   
    // In case text bound is too short to contain _maxNumLines
    [self fitHeightConstraint];
    
    CGFloat bottomLinePosition = [self.maxNumLines intValue] * self.font.lineHeight;
    CGPoint bottomLeftPoint = CGPointMake(CGRectGetMinX(self.bounds), bottomLinePosition);
    CGPoint bottomRightPoint = CGPointMake(CGRectGetMaxX(self.bounds), bottomLinePosition);
    
    UITextPosition* lastLineStartTextPosition = [self characterRangeAtPoint:bottomLeftPoint].start;
    UITextPosition* lastLineEndTextPosition = [self characterRangeAtPoint:bottomRightPoint].end;
    
    NSUInteger lastLineStartIndex = [self offsetFromPosition:self.beginningOfDocument
                                                  toPosition:lastLineStartTextPosition];
    NSUInteger lastLineEndIndex = [self offsetFromPosition:self.beginningOfDocument
                                                toPosition:lastLineEndTextPosition];
    
    int numTruncateText = kNumTruncateCharsForEllipsis + kNumTruncateCharsForExpandLabel;
    NSUInteger startTruncateIndex = lastLineEndIndex;
    
    if (lastLineEndIndex - lastLineStartIndex > numTruncateText){
        startTruncateIndex = lastLineEndIndex - numTruncateText;
    }
    
    NSRange truncateRange = NSMakeRange(startTruncateIndex, (self.text.length - 1) - startTruncateIndex + 1);
    
    // Truncate attributed text while preserving attributes
    NSMutableAttributedString* newAttText = [[NSMutableAttributedString alloc] initWithAttributedString:self.attributedText];
    [newAttText replaceCharactersInRange:truncateRange withString:kEllipsis];
    self.attributedText = newAttText;
}

- (void)addLabelToChangeToMode:(ShowMode)showMode{
    // Size to fit because endOfDocument works wrong with frame smaller than content
    [self fitHeightConstraint];
    
    // Append label to the end of text and attach tap gesture
    UITextPosition* lastCharPosition = [self positionFromPosition:self.endOfDocument offset:0];
    CGRect lastCharRect = [self caretRectForPosition:lastCharPosition];
    
    CGFloat labelX = lastCharRect.origin.x + lastCharRect.size.width;
    CGFloat labelY = lastCharRect.origin.y + lastCharRect.size.height * (1 - kLabelTextSizeRatioToViewTextSize)/2 + 1;
    CGFloat labelWidth = 1.0;
    CGFloat labelHeight = lastCharRect.size.height * kLabelTextSizeRatioToViewTextSize;
    
    CGRect frame = CGRectMake(labelX, labelY, labelWidth, labelHeight);
    UILabel* label = [[UILabel alloc]initWithFrame:frame];
    
    label.textColor = [TeamieUIGlobalColors primaryBlueColor];
    label.font = [UIFont fontWithName:self.font.fontName
                                 size:self.font.pointSize * kLabelTextSizeRatioToViewTextSize];
    label.accessibilityLabel = kLabelAccessibilityLabel;
    
    UITapGestureRecognizer* tapGesture;
    
    switch (showMode) {
        case kExpand:
            label.text = kExpandLabelText;
            tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                 action:@selector(expandLabelDidSelected)];
            [label sizeToFit];
            if (label.frame.origin.x + label.frame.size.width > self.frame.size.width){
                [self moveLabelToNextLine:label withCharacterRect:lastCharRect];
            }
            
            break;
        case kCollapse:
            label.text = kCollapseLabelText;
            tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                 action:@selector(collapseLabelDidSelected)];
            [self moveLabelToNextLine:label withCharacterRect:lastCharRect];
            
            break;
        default:
            break;
    }
    
    [tapGesture setNumberOfTapsRequired:1];
    [label addGestureRecognizer:tapGesture];
    [label setUserInteractionEnabled:YES];
    
    [label sizeToFit];
    
    [self addSubview:label];
    self.modeLabel = label;
}

- (void)moveLabelToNextLine:(UILabel*)label withCharacterRect:(CGRect)charRect{
    CGFloat newX = self.contentInset.left + self.textContainerInset.left + self.textContainer.lineFragmentPadding;
    CGFloat newY = label.frame.origin.y + charRect.size.height;
    
    CGRect newFrame = CGRectMake(newX, newY, label.frame.size.width, label.frame.size.height);
    label.frame = newFrame;
}

- (void)fitHeightConstraint{
    CGSize contentSize = [self sizeThatFits:CGSizeMake(self.frame.size.width, FLT_MAX)];
    
    [self setNeedsUpdateConstraints];
    
    self.heightConstraint.constant = ceilf(contentSize.height);
    
    // Expand height constraint to fit label because contentSize only finds the size to fit to the text
    if (self.modeLabel.frame.origin.y + self.modeLabel.frame.size.height > contentSize.height){
        self.heightConstraint.constant += self.modeLabel.frame.size.height;
    }
    
    [self updateConstraintsIfNeeded];
}

- (void)notifyDelegateOnHeightChange{
    if ([self.delegate respondsToSelector:@selector(textviewDidChangeHeight:)]){
        [self.delegate performSelector:@selector(textviewDidChangeHeight:) withObject:self];
    }
}

#pragma mark - Selector for collapse and expand label
- (void)expandLabelDidSelected{
    CGFloat prevHeightConstant = self.heightConstraint.constant;
    
    [self setNeedsLayout];

    self.attributedText = self.fullAttributedText;
    
    [self.modeLabel removeFromSuperview];
    [self addLabelToChangeToMode:kCollapse];
    
    [self fitHeightConstraint];
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        [self.superview layoutIfNeeded];
    }];
    
    if (prevHeightConstant != self.heightConstraint.constant){
        [self notifyDelegateOnHeightChange];
    }
}

- (void)collapseLabelDidSelected{
    CGFloat prevHeightConstant = self.heightConstraint.constant;
    
    [self setNeedsLayout];
    
    [self truncateText];
    
    [self.modeLabel removeFromSuperview];
    [self addLabelToChangeToMode:kExpand];
    
    [self fitHeightConstraint];
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        [self.superview layoutIfNeeded];
    }];
    
    if (prevHeightConstant != self.heightConstraint.constant){
        [self notifyDelegateOnHeightChange];
    }
}

@end