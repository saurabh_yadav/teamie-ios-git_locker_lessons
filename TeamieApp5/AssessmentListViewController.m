//
//  AssessmentListViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 27/10/14.
//  Copyright (c) 2014 ETH. All rights reserved.
//

#import "AssessmentListViewController.h"
#import "AssessmentCollectionViewCell.h"
#import "AssessmentListViewFlowLayout.h"
#import "TMEQuizSimple.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "TMEClient.h"

NSString *kCellID          = @"assessmentCell";                 // UICollectionViewCell storyboard id
NSString *kCellClass       = @"AssessmentCollectionViewCell";   // UICollectionViewCell subclass

CGFloat kPadCellWidth      = 369.0f;
CGFloat kPhoneCellWidth    = 290.0f;
CGFloat kCellHeight        = 240.0f;
CGFloat kReducedCellHeight = 191.0f;

@interface AssessmentListViewController () {
    /**
     *  Classroom ID for a particular class
     */
    NSNumber * classroomId;
    
    /**
     *  Classroom Title for a particular class
     */
    NSString * classroomTitle;

    /**
     *  List of QuizSimpleObject
     */
    NSMutableArray * quizzes;

    /**
     *  To load more items or not
     */
    BOOL loadMore;
    
    BOOL nibMyCellloaded;
    UIRefreshControl *refreshControl;
}

@property (strong, nonatomic) IBOutlet UICollectionView * collectionView;

@end

@implementation AssessmentListViewController

#pragma mark - Lifecycle

- (id)initWithClassroomId:(NSNumber *)nid andClassroomTitle:(NSString *)classroomName{
    self = [super init];
    if (self) {
        classroomId = nid;
        classroomTitle = classroomName;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // MARK: Initializations
    quizzes  = [[NSMutableArray alloc] init];
    loadMore = NO;
    nibMyCellloaded = NO;
    refreshControl = [[UIRefreshControl alloc] init];
    
    // MARK: Initiate Web Requests
    [self makeWebRequests:NO];

    // MARK: Add Collection View to the view
    AssessmentListViewFlowLayout * layout = [[AssessmentListViewFlowLayout alloc] init];
    self.collectionView                   = [[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [self.collectionView registerClass:[AssessmentCollectionViewCell class] forCellWithReuseIdentifier:kCellID];
    [self.collectionView registerNib:[UINib nibWithNibName:kCellClass bundle:nil] forCellWithReuseIdentifier:kCellID];
    self.collectionView.backgroundColor   = TeamieUIGlobalColors.baseBgColor;
    self.view                             = self.collectionView;
    
    // MARK: Set collection view data source and delegate
    self.collectionView.delegate   = self;
    self.collectionView.dataSource = self;
    
    self.navigationItem.title = [NSString stringWithFormat:@"%@: %@", classroomTitle, TMELocalize(@"menu.assessments")];
    
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"back2" withSize:25 andColor:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(backButtonPressed)];
    
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshQuizzes) forControlEvents:UIControlEventValueChanged];
    [self.collectionView addSubview:refreshControl];
    self.collectionView.alwaysBounceVertical = YES;
    
    self.navigationItem.leftBarButtonItem = backButton;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.backBarButtonItem = nil;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/assessments"];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return quizzes.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AssessmentCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCellID forIndexPath:indexPath];
    
    if (!nibMyCellloaded) {
        UINib *nib      = [UINib nibWithNibName:@"AssessmentCollectionViewCell" bundle: nil];
        [collectionView registerNib:nib forCellWithReuseIdentifier:kCellID];
        nibMyCellloaded = YES;
    }
    
    TMEQuizSimple *quiz = quizzes[indexPath.row];
    [cell setCellData:quiz withClassroomId:classroomId];

    // Hack for collectionView:willDisplayCell:forItemAtIndexPath: for iOS 7 and iOS 6
    if (loadMore) {
        if (indexPath.row == quizzes.count - 1) {
            self.collectionView.infiniteScrollingView.enabled = YES;
            __unsafe_unretained typeof(self) weakSelf = self;
            [weakSelf.collectionView addInfiniteScrollingWithActionHandler:^{
                [weakSelf makeWebRequests:YES];
            }];
        }
    }
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

// MARK: This delegate was introduced in iOS 8. Won't work on previous versions
// Refer to the hack above in collectionView:cellForItemAtIndexPath: delegate method
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    // When second last cell is displayed, send a request to server and load infinitely
    if (loadMore) {
        if (indexPath.row == quizzes.count - 1) {
            self.collectionView.infiniteScrollingView.enabled = YES;
            __unsafe_unretained typeof(self) weakSelf = self;
            [weakSelf.collectionView addInfiniteScrollingWithActionHandler:^{
                [weakSelf makeWebRequests:YES];
            }];
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [TeamieGlobals openInappBrowserWithURL:[NSURL URLWithString:((TMEQuizSimple *)quizzes[indexPath.row]).webUrl] fromResponder:self withTitle:((TMEQuizSimple *)quizzes[indexPath.row]).title];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    TMEQuizSimple *quiz = quizzes[indexPath.row];
    CGSize customLayout;
    if (!quiz.hasAccess.boolValue) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            customLayout = CGSizeMake(kPadCellWidth, kReducedCellHeight);
        }
        else {
            customLayout = CGSizeMake(kPhoneCellWidth, kReducedCellHeight);
        }
    }
    else {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            customLayout = CGSizeMake(kPadCellWidth, kCellHeight);
        }
        else {
            customLayout = CGSizeMake(kPhoneCellWidth, kCellHeight);
        }
    }
    return customLayout;
}

- (UIEdgeInsets)collectionView:(UICollectionView*)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0); // top, left, bottom, right
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 10.0;
}

#pragma mark - Private Methods

- (void)refreshQuizzes {
    [self makeWebRequests:NO];
}

- (void)makeWebRequests:(BOOL)withParameters {
    NSDictionary* params = [[NSDictionary alloc] init];
    if (!withParameters) {
        loadMore = YES;
        [quizzes removeAllObjects];
    }
    
    if (loadMore && classroomId) {
        params = @{@"nid": classroomId, @"page": [NSNumber numberWithInteger:quizzes.count/10 + 1], @"items_per_page": @10};
    }
    else {
        params = @{@"nid": classroomId};
    }
    
    [[TMEClient sharedClient]
     request:TeamieUserQuizzesRequest
     parameters:params
     loadingMessage:TMELocalize(@"message.loading")
     completion:^{
         if (!quizzes.count) {
             [self viewForEmptyListWithTitleText:nil subtitleText:nil andImage:nil];
         }
         
         [refreshControl endRefreshing];
         [self.collectionView.infiniteScrollingView stopAnimating];
         [self.collectionView reloadData];
     } success:^(id response) {
         NSArray *quizObjects = [TMEQuizSimple parseList:response[@"quizzes"]];
         
         // If total object count is not a multiple of 5 that means it's the end of list
         if (quizObjects.count && quizObjects.count % 10 == 0) {
             loadMore = YES;
         }
         else {
             loadMore = NO;
         }
         
         for (TMEQuizSimple *quiz in quizObjects) {
             [quizzes addObject:quiz];
         }
     } failure:nil];
}

- (void)backButtonPressed {
    self.parentViewController.navigationController.navigationBarHidden = NO;
    [self.navigationController popViewControllerAnimated:YES];
}

@end
