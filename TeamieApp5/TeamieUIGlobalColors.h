//
//  TeamieUIGlobalColors.h
//  TeamieApp5
//
//  Created by Teamie on 3/4/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TeamieUIGlobalColors : NSObject

+ (UIColor*)primaryRedColor;
+ (UIColor*)secondaryRedColor;
+ (UIColor*)primaryBlueColor;
+ (UIColor*)secondaryBlueColor;
+ (UIColor*)primaryGreenColor;
+ (UIColor*)secondaryGreenColor;
+ (UIColor *)secondaryYellowColor;

+ (UIColor*)navigationBarBackgroundColor;
+ (UIColor*)navigationBarTextColor;
+ (UIColor*)toolbarBackgroundColor;
+ (UIColor*)toolbarTextColor;
+ (UIColor*)inputFieldBorderColor;
+ (UIColor*)inputFieldTextColor;

+ (UIColor*)defaultTextColor;
+ (UIColor*)buttonTitleColor;
+ (UIColor*)thoughtCommentTextColor;
+ (UIColor*)thoughtReplyTextColor;
+ (UIColor*)postCommentBackgroundColor;
+ (UIColor*)postCommentReplyBackgroundColor;
+ (UIColor*)borderColor;

+ (UIColor*)tableViewCellBorderColor;
+ (UIColor*)tableViewIconColor;

+ (UIColor*)primaryActionColor;
+ (UIColor*)genericBackgroundColor;

+ (UIColor *)baseBgColor;
+ (UIColor *)grayColor;
+ (UIColor *)supplementaryViewElementTextColor;

+ (UIColor *)headerViewBackgroundColor;
+ (UIColor *)headerTitleLabelTextColor;
+ (UIColor *)headerSubtitleLabelTextColor;

+ (UIColor *)warningLabelTextColor;
+ (UIColor *)warningLabelBackgroundColor;

+ (UIColor *)primaryLabelTextColor;
+ (UIColor *)primaryLabelBackgroundColor;

+ (UIColor *)infoLabelTextColor;
+ (UIColor *)infoLabelBackgroundColor;

+ (UIColor *)detailLabelTitleTextColor;
+ (UIColor *)detailLabelTitleNumberColor;
+ (UIColor *)detailLabelBackgroundColor;

+ (UIColor *)subviewBorderColor;
+ (UIColor *)cellShadowColor;

+ (UIColor *)notificationBadgeTextColor;
+ (UIColor *)notificationBadgeBackgroundColor;

+ (UIColor *)publishStatusDraftLabelTextColor;
+ (UIColor *)publishStatusPublishedLabelTextColor;
+ (UIColor *)publishStatusDraftLabelBackGroundColor;
+ (UIColor *)publishStatusPublishedLabelBackGroundColor;

+ (UIColor *)pollOptionHighlightedLabelBackgroundColor;
+ (UIColor *)pollOptionSubmitButtonColor;

+ (UIColor *)disabledColor;

@end
