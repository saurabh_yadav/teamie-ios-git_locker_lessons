//
//  TMEHomework.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 24/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPost.h"

@interface TMEHomework : TMEPost

@property (nonatomic, strong) NSDate *deadline;

@end
