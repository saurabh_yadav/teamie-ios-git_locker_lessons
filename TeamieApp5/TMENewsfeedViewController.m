//
//  TMENewsfeedViewController.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 21/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMENewsfeedViewController.h"

#import "TMEPost.h"
#import "TMEPostView.h"
#import "TMEClient.h"
#import "TMEPostCreateViewController.h"
#import "TMEPostViewController.h"
#import "UIView+TMEEssentials.h"
#import "TMECardTableViewCell.h"

@interface TMENewsfeedViewController()
<TMEPostViewDelegate, TMERevealDelegate>

@property (nonatomic, strong) NSMutableDictionary *cachedHeights;
@property (nonatomic, strong) NSMutableDictionary *cachedFooters;
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic) BOOL hasMorePosts;
@property (nonatomic, strong) TMEPostView *postView;
@property (nonatomic, strong) NSMutableDictionary *showMoreForPostID;
@property (nonatomic, weak) TMEPost *updatedPost;
@property (strong, nonatomic) NSMutableArray *seenPosts;
@property (nonatomic) CGFloat pollHeight;
@property (nonatomic, strong) NSString* htmlContent;
@property (nonatomic, strong) NSMutableDictionary *setWebViewHeight;


@end
CGFloat setWebViewHeight;

BOOL cellVisibility;
@implementation TMENewsfeedViewController

static NSString * const kReuseIdentifier = @"NewsfeedPost";

#pragma mark - Lifecycle

+ (instancetype)newsfeed {
    return [self new];
}

- (void)viewDidLoad {
    self.postView = [[TMEPostView alloc] init];
    self.cachedHeights = [NSMutableDictionary new];
    self.showMoreForPostID = [NSMutableDictionary new];
    self.cachedFooters = [NSMutableDictionary new];
    self.setWebViewHeight = [NSMutableDictionary new];
    self.seenPosts = [NSMutableArray new];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshSeenPosts) name:@"refreshSeenPosts" object:nil];
    
    self.navigationItem.title = TMELocalize(@"menu.newsfeed");
    [self addPostShareButton];
    [self addRefreshControl];
    [self addFooterView];
    
    [self.tableView registerClass:TMECardTableViewCell.class forCellReuseIdentifier:kReuseIdentifier];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [TeamieUIGlobalColors baseBgColor];
    self.currentPage = 1;
    self.posts = [NSMutableArray new];
    self.showMore = 1;
    [self request:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/newsfeed"];
}

- (void) viewDidDisappear:(BOOL)animated{
    [self refreshSeenPosts];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.posts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TMEPostView *postView = [[TMEPostView alloc] initWithMode:self.mode andShowOption:self.showMore andShowOnPostID:self.showMoreForPostID];
    postView.delegate = self;
    [postView loadPost:self.posts[indexPath.row]];
    
    TMECardTableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kReuseIdentifier forIndexPath:indexPath];
    cell.cardView = postView;
    
    [postView setNeedsLayout];
    [postView layoutIfNeeded];
    [cell updateConstraints];
    [cell.contentView setNeedsLayout];
    [cell.contentView layoutIfNeeded];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TMETrackEvent(@"newsfeed", @"tap_post_body", nil);
    [self.navigationController pushViewController:[TMEPostViewController postWithId:((TMEPost *)self.posts[indexPath.row]).tid]
                                         animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self getHeight:self.posts[indexPath.row]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self getHeight:self.posts[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.hasMorePosts) {
        if (indexPath.row == self.posts.count - 4) {
            [self request:YES];
        }
    }
    if (indexPath.row < 3) {
        TMEPost *post = [self.posts objectAtIndex:indexPath.row];
        if (post.isNew) {
            if (![self.seenPosts containsObject:post.tid]) {
                [self.seenPosts addObject:post.tid];
            }
        }
    }
}

#pragma mark - Helper methods

- (void)addRefreshControl {
    self.refreshControl = [self.tableView createAndAddSubView:UIRefreshControl.class];
    self.refreshControl.tintColor = [UIColor grayColor];
    [self.refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
}

- (void)addFooterView {
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 13)];
    self.tableView.backgroundColor = [TeamieUIGlobalColors baseBgColor];
}

- (void)addPostShareButton {
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"post"
                                                                                         withSize:25.0
                                                                                         andColor:[UIColor whiteColor]]
                                              style:UIBarButtonItemStylePlain
                                              target:self
                                              action:@selector(openPostShareView)];
}

- (void)openPostShareView {
    [self.navigationController pushViewController:[TMEPostCreateViewController new]
                                         animated:YES];
}

- (CGFloat)getHeight:(TMEPost *)post {
    //instead of setting show less, setting it on the basis of number of lines, getting text height can work here
    CGFloat textHeight = [self getTextHeight:post.mainText];

    if (textHeight >= 89) {
        int value = [[self.showMoreForPostID objectForKey:post.tid] integerValue];
        if (value == 1) {
            self.showMore = 1;
        }
        else{
            self.showMore = 0;
        }
    }
    if (post.mainText.length > 0) {
        textHeight = textHeight + 35;
    }
    else{
        textHeight = 0;
    }
    
    if (self.cachedHeights[post.tid] == nil || self.cachedFooters[post.tid] == nil) {
        if ([(NSNumber *)self.cachedFooters[post.tid] boolValue]) {
            self.pollHeight = (post.poll? (((post.poll.choiceCount.intValue) *
                                            50) + 23): 0);
        }
        if ([self.cachedFooters[post.tid] integerValue] == 1) {
            self.pollHeight = (post.poll? (((post.poll.choiceCount.intValue + 1) * 50) + 23): 0);
        }
        else {
            self.pollHeight = (post.poll? (((post.poll.choiceCount.intValue ) * 50) + 23): 0);
        }
        setWebViewHeight = [self.setWebViewHeight[post.tid] floatValue] ;
        
        
        
        if (textHeight < 90){
            self.cachedHeights[post.tid] = @((55) + ( textHeight) + (post.type == TMEPostHomework? 38 : 0)
             + (self.pollHeight) + setWebViewHeight + (post.attachments.count * 45) + ([self getImagesHeight:post.images.count]) + (38) + (13));
        }
        else{
            if (self.showMore) {
                self.cachedHeights[post.tid] = @((55) + (10 + 10 + 89 + 15 + 10) + 20  + setWebViewHeight + (post.type == TMEPostHomework? 38 : 0)
                + (post.attachments.count * 45) + (self.pollHeight) + ([self getImagesHeight:post.images.count]) + (38) + (13));
            }
            else{
                self.cachedHeights[post.tid] = @((55) + (textHeight + 20 + 15 + 15) + (post.type == TMEPostHomework? 38 : 0)
                + setWebViewHeight + (post.attachments.count * 45) + (self.pollHeight) + ([self getImagesHeight:post.images.count]) + (38) + (13));
            }
        }
    }
    return [(NSNumber *)self.cachedHeights[post.tid] floatValue];
}

- (CGFloat)getTextHeight:(NSAttributedString *)text {
    CGFloat textWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - (2 * (kDefaultCardPadding + kTextPadding));
    return CGRectGetHeight(CGRectIntegral([text boundingRectWithSize:CGSizeMake(textWidth, CGFLOAT_MAX)
                                                             options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                                             context:nil]));
}

/**
 * Calculate the total height of the image attachments using the logic similar to ThoughtImageAttachmentView
 */
- (CGFloat)getImagesHeight:(NSUInteger)numberOfImages {

    CGFloat imageHeight = 0;
    CGFloat cardWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - (2 * kDefaultCardPadding);
    switch (numberOfImages) {
        case 0:
            imageHeight = 0;
            break;
        case 1:
            imageHeight = cardWidth * 2/3;
            break;
        case 2:
            imageHeight = cardWidth * 1/2;
            break;
        case 3:
            imageHeight = cardWidth;
            break;
        default:
            //TODO: Check if it works for > 3 images
            imageHeight = cardWidth * 2/3;
            break;
    }

    return imageHeight;
}

#pragma mark - Network

- (TMERequest)requestName {
    return TeamieUserNewsfeedRequest;
}

- (NSDictionary *)requestParameters {
    return @{};
}

- (void)refresh {
    [self.refreshControl beginRefreshing];
    self.showMore = 1;
    [self refreshSeenPosts];
    [self request:NO];
    [self.cachedHeights removeAllObjects];
}

- (void)refreshSeenPosts{
    if (self.seenPosts.count > 0) {
        NSMutableDictionary *parameters = [NSMutableDictionary new];
        for (int i = 0; i < self.seenPosts.count; i ++) {
            [parameters setObject:self.seenPosts[i] forKey:[NSString stringWithFormat:@"tid[%d]",i]];
        }
        [self.seenPosts removeAllObjects];
        [[TMEClient sharedClient]
         request:TeamieUserSeenPostRequest
         parameters:parameters
         loadingMessage:nil
         completion:^{
             if ([self.refreshControl isRefreshing]) {
                 [self.refreshControl endRefreshing];
             }
         }
         success:^(NSDictionary *response) {
             nil;
         } failure:nil];
    }
}

- (void)request:(BOOL)more {
    NSMutableDictionary *parameters = [[self requestParameters] mutableCopy];
    parameters[@"items_per_page"] = @(NUMBER_OF_NEWSFEED_ITEMS);
    if (more) {
        self.currentPage += 1;
        parameters[@"page"] = @(self.currentPage);
    }
    
    [[TMEClient sharedClient]
     request:[self requestName]
     parameters:parameters
     loadingMessage: [self.refreshControl isRefreshing]? nil : TMELocalize(@"message.loading")
     completion:^{
         if ([self.refreshControl isRefreshing]) {
             [self.refreshControl endRefreshing];
         }
     }
     success:^(NSDictionary *response) {
         [self parseResponse:response append:more];
     } failure:nil];

    TMETrackEvent(@"newsfeed", more? @"load_old" : @"load_new", @"post");
}

- (void)parseResponse:(NSDictionary *)response append:(BOOL)append {
    NSArray *parsedPosts = [TMEPost parseList:response[@"thoughts"][@"thought"]];
    self.hasMorePosts = parsedPosts.count == NUMBER_OF_NEWSFEED_ITEMS;
    if (!append) {
        [self.posts removeAllObjects];
    }
    [self.posts addObjectsFromArray:parsedPosts];
    if (self.posts.count == 0) {
        [self viewForEmptyListWithTitleText:nil subtitleText:nil andImage:nil];
    } 
    else {
        [self removeEmptyView];
    }
    NSInteger postCount = [self.posts count];
    for (int i = 0; i < postCount; i++) {
        TMEPost *post = self.posts[i];
        
        [self.showMoreForPostID setObject:[NSNumber numberWithInteger:1] forKey:post.tid];
        
        [self.setWebViewHeight setObject:[NSNumber numberWithInteger:0] forKey:post.tid];
    }
    for (TMEPost *posts in self.posts) {
        self.cachedFooters[posts.tid] = @0;
    }
    [self.tableView reloadData];
}

#pragma mark - TMEPostViewDelegate

- (void)showPostMoreActionsSheet:(TMEPostView *)postView {
    [[postView moreActionsSheet] showFromRect:[postView.moreButton convertRect:postView.moreButton.bounds toView:self.view]
                                       inView:self.view
                                     animated:YES];
}

- (void)commentButtonClicked:(TMEPost *)post {
    [self.navigationController pushViewController:[TMEPostViewController postWithId:post.tid openComment:YES]
                                         animated:YES];
}

- (void)removePost:(TMEPost *)post {
    [self.tableView beginUpdates];
    NSInteger deleteIndex = -1;
    deleteIndex = [self indexForPostWithID:post.tid];
    if (deleteIndex >= 0) {
        [self.posts removeObjectAtIndex:deleteIndex];
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:deleteIndex
                                                                    inSection:0]]
                              withRowAnimation:UITableViewRowAnimationAutomatic];
        if (self.posts.count == 0) {
            [self viewForEmptyListWithTitleText:nil subtitleText:nil andImage:nil];
        }
    }
    [self.tableView endUpdates];
}

-(NSInteger)indexForPostWithID: (NSNumber *)postID{
    __block NSInteger deleteIndex = -1;
    [self.posts enumerateObjectsUsingBlock:^(TMEPost *newsfeedPost, NSUInteger idx, BOOL *stop) {
        if ([newsfeedPost.tid isEqualToNumber:postID]) {
            deleteIndex = idx;
            *stop = YES;
        }
    }];
    return deleteIndex;
}

-(void) updateTheRowForIndex: (TMEPost *)post withOption:(BOOL)showMore{
   NSInteger updateIndex = -1;
    [self.cachedHeights setValue:nil forKey:[post.tid stringValue]];
    if (showMore) {
        self.showMore = 1;
        [self.showMoreForPostID setObject:@1 forKey:post.tid];
    }
    else{
        self.showMore = 0;
        [self.showMoreForPostID setObject:@0 forKey:post.tid];
    }
    updateIndex = [self indexForPostWithID:post.tid];
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:updateIndex
                                                                inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
}

-(void) updateTheRowForIndex: (TMEPost *)post withHeight:(CGFloat)webViewHeight{
    NSInteger updateIndex = -1;
    [self.cachedHeights setValue:nil forKey:[post.tid stringValue]];
    if (post.richText) {
        [self.setWebViewHeight setObject:@(webViewHeight) forKey:post.tid];
    }
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:updateIndex
                                                                inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)updatePostViewHeightWithUpdatedPost:(TMEPost *)post {
    [self.tableView beginUpdates];
    [self.cachedHeights setValue:nil forKey:[post.tid stringValue]];
    self.updatedPost = post;
    if (self.updatedPost.poll.isFooterVisible) {
        self.cachedFooters[post.tid] = @1;
    }
    else{
        self.cachedFooters[post.tid] = @0;
    }
    
    [self.tableView endUpdates];
}

- (void)checkVisibilityOfCell:(UITableViewCell *)cell inScrollView:(UIScrollView *)aScrollView {
    CGRect cellRect = [aScrollView convertRect:cell.frame toView:aScrollView.superview];
    
    if (CGRectContainsRect(aScrollView.frame, cellRect)){
        NSIndexPath *indexPathforCell = [self.tableView indexPathForCell:cell];
        [self notifyCompletelyVisible:indexPathforCell];
    }
    else
        [self notifyNotCompletelyVisible];
}

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    NSArray* cells = self.tableView.visibleCells;
    NSUInteger cellCount = [cells count];
    if (cellCount == 0)
        return;
    
    // Check the visibility of the first cell
    [self checkVisibilityOfCell:[cells firstObject] inScrollView:aScrollView];
    if (cellCount == 1)
        return;
    
    // Check the visibility of the last cell
    [self checkVisibilityOfCell:[cells lastObject] inScrollView:aScrollView];
    if (cellCount == 2)
        return;
    
    // All of the rest of the cells are visible: Loop through the 2nd through n-1 cells
    for (NSUInteger i = 1; i < cellCount - 1; i++){
        NSIndexPath *indexPathforCell = [self.tableView indexPathForCell:cells[i]];
        [self notifyCompletelyVisible:indexPathforCell];
    }
}

-(void)notifyCompletelyVisible:(NSIndexPath*)indexPath{
    TMEPost *post = [self.posts objectAtIndex:indexPath.row];
    if (post.isNew) {
        if (![self.seenPosts containsObject:post.tid]) {
            [self.seenPosts addObject:post.tid];
        }
    }
}

-(void)notifyNotCompletelyVisible{
    cellVisibility = 0;
}

@end
