//
//  ResourcesSettingsViewController.m
//  TeamieApp5
//
//  Created by Raunak on 16/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ResourcesSettingsViewController.h"

@interface ResourcesSettingsViewController ()
@property (nonatomic, strong) UITableView* tableView;
@end

@implementation ResourcesSettingsViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.contentSizeForViewInPopover = CGSizeMake(275, 140);
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.scrollEnabled = NO;
    self.tableView.bounces = NO;
    
    [self.view addSubview:self.tableView];
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return @"Delete All Resources..";
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"settingsCellIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"settingsCellIdentifier"];
    }
    
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"From This Account";
            cell.textLabel.enabled = [self.delegate settingsControllerShouldDisplayCurrentAccountOption];
            break;
        case 1:
            cell.textLabel.text = @"From Other Accounts";
            cell.textLabel.enabled = [self.delegate settingsControllerShouldDisplayOtherAccountsOptions];
            break;
        default:
            break;
    }
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.delegate settingsControllerDidSelectDeleteOption:((SettingsDeleteOptions)indexPath.row)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
