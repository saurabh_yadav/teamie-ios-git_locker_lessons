//
//  NewsfeedItem.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 12/3/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "NewsfeedItem.h"
#import "TMEClassroom.h"
#import "NewsfeedItemAction.h"
#import "NewsfeedItemAttachment.h"
#import "NewsfeedItemComment.h"
#import "UserProfile.h"


@implementation NewsfeedItem

@dynamic countComment;
@dynamic countEcho;
@dynamic countLike;
@dynamic htmlmessage;
@dynamic message;
@dynamic tid;
@dynamic timestamp;
@dynamic timestampText;
@dynamic type;
@dynamic actions;
@dynamic attachment;
@dynamic images;
@dynamic author;
@dynamic classrooms;
@dynamic comments;
@dynamic isAnnouncement;
@dynamic homeworkDeadline;

- (NSString*)description {
    NSString* line = @"\n----------------------\n";
    NSString* debugMsg = [NSString stringWithFormat:@"%@(%@): %@ %@Thought HTML: %@%@ %@ (%@) - %@ likes - %@ echoes - %@ comments - Classroom: %@\n%@", self.author.fullname, self.author.email, self.message, line, self.htmlmessage, line, self.timestampText, self.timestamp, self.countLike, self.countEcho, self.countComment, self.classrooms.name, line];
    for (NewsfeedItemAttachment* item in self.attachment) {
        [debugMsg stringByAppendingString:[item description]];
    }
    
    return debugMsg;
}

@end
