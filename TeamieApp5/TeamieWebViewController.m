//
//  TeamieWebViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 9/20/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TeamieWebViewController.h"

@interface TeamieWebViewController ()

@end

@implementation TeamieWebViewController

@synthesize parentRevealController = _parentRevealController;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //[_toolbar setTintColor:[UIColor lightGrayColor]];
    //self.navigationBarTintColor = [UIColor colorWithHex:@"#BE202E" alpha:1.0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Add a back button
    // IF the current view controller is not at the root of a reveal controller
    // AND IF the current view controller is being presented by another view controller
    if (!self.parentRevealController && self.presentingViewController) {
        [TeamieGlobals addBackButtonToViewController:self withCallback:@selector(closeButtonPressed:)];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)closeButtonPressed:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

-(void)setParentRevealController:(id)parentRevealController {
    
    _parentRevealController = parentRevealController;
    
    // Add event listeners for user menu navigation
    if ([_parentRevealController respondsToSelector:@selector(addEventListeners:)]) {
        [_parentRevealController performSelector:@selector(addEventListeners:) withObject:self];
    }
    
}

@end
