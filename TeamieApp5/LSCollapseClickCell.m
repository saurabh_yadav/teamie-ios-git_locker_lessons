   //
//  LSCollapseClickCell.m
//  CollapseClickDemo
//
//  Created by Wei Wenbo on 28/5/14.
//  Copyright (c) 2014 Ben Gordon. All rights reserved.
//

#import "LSCollapseClickCell.h"
#import "UIImageView+WebCache.h"
#import "UserActions.h"
#import "TMEClassroomAlt.h"
#import "TMEUser.h"


@interface LSCollapseClickCell()

@property BOOL has_delete_access;
@property BOOL has_toggle_access;
@property BOOL has_edit_access;

@end

@implementation LSCollapseClickCell

+ (LSCollapseClickCell *)newCollapseClickCellWithTitle:(NSString *)title index:(int)index content:(UIView *)content lesson:(TMELesson *)lesson classroomID:(NSNumber *)classroomID{
    NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"LSCollapseClickCell" owner:nil options:nil];
    LSCollapseClickCell *cell = [[LSCollapseClickCell alloc] initWithFrame:CGRectMake(0, 0, 320, kCCHeaderHeight + LSLessonHeaderHeight)];
    cell = [views objectAtIndex:0];
    
    [cell accessControlSetup:lesson.actions];

    // Initialization Here
    cell.authorImageButton.layer.cornerRadius = 3.0f;
    cell.authorImageButton.layer.masksToBounds = YES;
    cell.lessonHeaderButton.tag = index;
    cell.TitleLabel.text = title;
    cell.index = index;
    cell.TitleButton.tag = index;
    cell.authorImageButton.tag = index;
    cell.ContentView.frame = CGRectMake(cell.ContentView.frame.origin.x, cell.ContentView.frame.origin.y, cell.ContentView.frame.size.width, content.frame.size.height);
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:cell.ContentView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:content attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
    [cell.ContentView addConstraint:heightConstraint];
    
    CGRect containerFrame = cell.containerView.frame;
    containerFrame.size = CGSizeMake(containerFrame.size.width, cell.LessonInfo.frame.size.height + cell.TitleView.frame.size.height);
    cell.containerView.frame = containerFrame;

    cell.lessonTitle.text = lesson.title;
    cell.lessonTitle.lineBreakMode = NSLineBreakByTruncatingMiddle;
    
    
    [cell.ContentView addSubview:content];
    
    cell.containerView.layer.cornerRadius = 5.0;
    cell.containerView.layer.masksToBounds = YES;
    
    cell.backgroundImage.contentMode = UIViewContentModeScaleAspectFill;
    [cell.backgroundImage sd_setImageWithURL:lesson.coverImageURL];

    NSString *pagesReadString;
    if ([lesson.num_pages_read_by_current_user intValue] != 1) {
        pagesReadString = [NSString stringWithFormat:@"%d pages read",[lesson.num_pages_read_by_current_user intValue]];
    }else{
        pagesReadString = [NSString stringWithFormat:@"1 page read"];
    }
    
    //if the current user is a teacher (has toggle access), then we do not show the number of pages the user read; otherwise, we show it.
//    if (cell.has_toggle_access == YES) {
//        [cell.readStats removeFromSuperview];
//    }else{
//        [cell.readStats setTextWhileStickToRightTopCornor:pagesReadString];
//    }
    [cell.readStats removeFromSuperview];
//    cell.publishStatusLabel.font = [TeamieGlobals appFontFor:@"boldFontWithSize11"];
//    [cell.publishStatusLabel setPreferredMaxLayoutWidth:67.0];
    
    if (cell.has_edit_access == YES) {
        if ([lesson.status boolValue] == YES) {
            [cell.publishStatusLabelLessons configurePublishStatusLabelWithStatus:PublishStatusPublished];
        }else if([lesson.status boolValue] == NO) {
            [cell.publishStatusLabelLessons configurePublishStatusLabelWithStatus:PublishStatusDrafted];
        }
    }else{
        [cell.publishStatusLabelLessons removeFromSuperview];
    }
    
    cell.authorImageView.layer.masksToBounds = YES;
    cell.authorImageContentView.layer.cornerRadius = 3.0f;
    cell.authorImageView.layer.cornerRadius = 3.0f;
    [cell.authorImageView sd_setImageWithURL:lesson.author.user_profile_image.path];
    
    [cell setupPublishAndDeadlineLabelsWithPublishDate:lesson.publishDate deadline:lesson.deadline];
    
    [cell setupClassroomLabel:lesson.classrooms classroomID:classroomID];
    
    return cell;
}


- (void)accessControlSetup:(NSArray *)actions
{
    self.has_edit_access = NO;
    self.has_delete_access = NO;
    self.has_toggle_access = NO;
    
    for (UserActions *action in actions) {
        if ([action.name isEqualToString:@"edit"]) {
            self.has_edit_access = YES;
        }
        
        if ([action.name isEqualToString:@"delete"]) {
            self.has_delete_access = YES;
        }
        
        if ([action.name isEqualToString:@"toggle"]) {
            self.has_toggle_access = YES;
        }
    }
}

- (NSString *)currentClassroomName:(NSNumber *)classroomID fromClassrooms:(NSArray *)classrooms
{
    for (TMEClassroomAlt *classroom in classrooms) {
        if ([classroom.nid isEqualToNumber:classroomID]) {
            return classroom.name;
        }
        else if(classroomID){
            return classroom.name;
        }
    }
    return @"Not Specified";
}

- (void)setupPublishAndDeadlineLabelsWithPublishDate:(NSDate *)publishDate deadline:(NSDate *)deadline
{
    UIFont* dateFont = [TeamieGlobals appFontFor:@"boldFontWithSize11"];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"MMM d"];
    
    NSString *publishDateString;
    if ([dateFormatter stringFromDate:publishDate]) {
        publishDateString = [NSString stringWithFormat:@"From: %@",[dateFormatter stringFromDate:publishDate]];
    }else{
        publishDateString = [NSString stringWithFormat:@""];
    }
    
    NSString *deadlineString;
    if ([dateFormatter stringFromDate:deadline]) {
        deadlineString = [NSString stringWithFormat:@"To: %@",[dateFormatter stringFromDate:deadline]];
    } else {
        deadlineString = [NSString stringWithFormat:@""];
    }
    
    //Setup the publish date label
    CGSize expectedSizeForPublishedDateLabel = [publishDateString sizeWithFont:dateFont];
    
    self.publishDate.text = publishDateString;
    self.publishDate.font = dateFont;
    CGRect pdFrame = self.publishDate.frame;
    pdFrame.size = expectedSizeForPublishedDateLabel;
    self.publishDate.frame = pdFrame;
    
    
    //Setup the deadline label
    CGSize expectedSizeForDeadlieLabel = [deadlineString sizeWithFont:dateFont];
    self.deadline.font = dateFont;
    self.deadline.text = deadlineString;

    CGRect dlFrame = self.deadline.frame;
    dlFrame.size = expectedSizeForDeadlieLabel;
    
    CGPoint dlOrigin = CGPointMake(pdFrame.origin.x + pdFrame.size.width + 5, pdFrame.origin.y);
    
    dlFrame.origin = dlOrigin;
    self.deadline.frame = dlFrame;
    
    
}

- (void)setupClassroomLabel:(NSArray *)classrooms classroomID:(NSNumber *)classroomID
{
    NSString *classroomInfoString;
    UIFont* classroomFont = [TeamieGlobals appFontFor:@"regularFontWithSize11"];

    if (classrooms.count >1) {
        classroomInfoString = [NSString stringWithFormat:@"%@ + %lu more",[self currentClassroomName:classroomID fromClassrooms:classrooms],classrooms.count - 1];
    }else{
        classroomInfoString = [self currentClassroomName:classroomID fromClassrooms:classrooms];
    }
    
    CGSize expectedSize = [classroomInfoString sizeWithFont:classroomFont];
    
    self.classroomInfo.text = classroomInfoString;
    self.classroomInfo.font = classroomFont;
    //Text properties setting for the lesson title
    self.lessonTitle.font = [TeamieGlobals appFontFor:@"regularFontWithSize18"];
    self.TitleLabel.font = [TeamieGlobals appFontFor:@"regularFontWithSize14"];
    self.classroomInfo.font = [TeamieGlobals appFontFor:@"boldFontWithSize11"];
    
    CGRect frame = self.classroomInfo.frame;
    frame.size = expectedSize;
    self.classroomInfo.frame = frame;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
