//
//  UserProfileCollapseClickCell.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 7/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UserProfileCollapseClickCell.h"

@implementation UserProfileCollapseClickCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (UserProfileCollapseClickCell *)newCollapseClickCellWithTitle:(NSString *)title index:(int)index content:(UIView *)content
{
    NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"UserProfileCollapseClickCell" owner:nil options:nil];
    UserProfileCollapseClickCell *cell = [views objectAtIndex:0];
    
    // Initialization Here
    cell.TitleLabel.text = title;
    cell.index = index;
    cell.TitleButton.tag = index;
    cell.ContentView.frame = CGRectMake(cell.ContentView.frame.origin.x, cell.ContentView.frame.origin.y, cell.ContentView.frame.size.width, content.frame.size.height);
    
    [cell.ContentView addSubview:content];
    
    return cell;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
