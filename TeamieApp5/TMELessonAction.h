//
//  TMELessonAction.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 27/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEModel.h"

@interface TMELessonAction : TMEModel

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* icon;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* url;
@property (nonatomic, strong) NSString* type;

@end
