//
//  TMEPostShareClassroomsTableViewController.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 4/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostCreateViewModel.h"

@protocol TMEPostCreateClassroomsViewDelegate <NSObject>

- (void)didFinishPickingClassrooms:(NSArray *)classrooms;

@end

@interface TMEPostCreateClassroomsViewController : UITableViewController

@property (nonatomic, strong) NSMutableSet *selectedClassrooms;
@property (nonatomic) id <TMEPostCreateClassroomsViewDelegate> delegate;

- (instancetype)initWithDelegate:(id)delegate;

@end
