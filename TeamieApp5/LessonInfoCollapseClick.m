//
//  LessonInfoCollapseClick.m
//  LS2
//
//  Created by Wei Wenbo on 3/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import "LessonInfoCollapseClick.h"
#import "LessonInfoCollapseClickCell.h"
#import "Masonry.h"

@interface CollapseClick()
- (void)didSelectCollapseClickButton:(UIButton *)button;
@end

@implementation LessonInfoCollapseClick

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

#pragma mark - Load Data
-(void)reloadCollapseClick {

    // If Arrays aren't Init'd, Init them
    if (!(self.isClickedArray)) {
        self.isClickedArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
    }
    
    if (!(self.dataArray)) {
        self.dataArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
    }
    
    // Make sure they are clear
    [self.isClickedArray removeAllObjects];
    [self.dataArray removeAllObjects];
    
    // Remove all subviews
    for (UIView *subview in self.subviews) {
        [subview removeFromSuperview];
    }
    // Set Up: Height
    UIView *coverImageView = [self.CollapseClickDelegate viewForCoverImage];
    float totalHeight = coverImageView.frame.size.height;
    [self addSubview:coverImageView];
    coverImageView.layer.masksToBounds = YES;
    [coverImageView mas_makeConstraints:^(MASConstraintMaker *make){
        if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad){
            make.width.equalTo(@1520);
        }
        else{
            make.width.equalTo(self);
        }
        make.height.equalTo(@112);
    }];

    
    UIView *authorField = [self.CollapseClickDelegate viewForAuthorField];
    CGRect authorFieldFrame = CGRectMake(CGRectGetMinX(authorField.frame), CGRectGetMinY(authorField.frame), self.bounds.size.width, CGRectGetHeight(authorField.frame));
    CGPoint origin = CGPointMake(0,totalHeight + HeaderContentPaddings);
    authorFieldFrame.origin = origin;
    authorField.frame = authorFieldFrame;
    [self addSubview:authorField];
    
    totalHeight += authorField.frame.size.height + 2 * HeaderContentPaddings;
    
    // Add cells
    for (int xx = 0; xx < [CollapseClickDelegate numberOfCellsForCollapseClick]; xx++) {
        
        // Create Cell
        LessonInfoCollapseClickCell *cell = [LessonInfoCollapseClickCell newCollapseClickCellWithTitle:[CollapseClickDelegate titleForCollapseClickAtIndex:xx] index:xx content:[CollapseClickDelegate viewForCollapseClickContentViewAtIndex:xx]];
        
        
        // Set cell.TitleView's backgroundColor
        if ([(id)CollapseClickDelegate respondsToSelector:@selector(colorForCollapseClickTitleViewAtIndex:)]) {
            cell.TitleView.backgroundColor = [CollapseClickDelegate colorForCollapseClickTitleViewAtIndex:xx];
        }
        else {
            cell.TitleView.backgroundColor = [UIColor colorWithWhite:0.4 alpha:1.0];
        }
        
        
        // Set cell.TitleLabel's Color
        if ([(id)CollapseClickDelegate respondsToSelector:@selector(colorForTitleLabelAtIndex:)]) {
            cell.TitleLabel.textColor = [CollapseClickDelegate colorForTitleLabelAtIndex:xx];
        }
        else {
            cell.TitleLabel.textColor = [UIColor whiteColor];
        }
        
        
        // Set cell.TitleArrow's Color
        if ([(id)CollapseClickDelegate respondsToSelector:@selector(colorForTitleArrowAtIndex:)]) {
//            [cell.TitleArrow drawWithColor:[CollapseClickDelegate colorForTitleArrowAtIndex:xx]];
            
            cell.arrow.image = [TeamieUIGlobals defaultPicForPurpose:@"right-arrow" withSize:25 andColor:[CollapseClickDelegate colorForTitleArrowAtIndex:xx]];
        }
        else {
//            [cell.TitleArrow drawWithColor:[UIColor colorWithWhite:0.0 alpha:0.35]];
            cell.arrow.image = [TeamieUIGlobals defaultPicForPurpose:@"right-arrow" withSize:25 andColor:[UIColor colorWithWhite:0.0 alpha:0.35]];
        }
        
        // Set cell.ContentView's size
        CGFloat ContentViewHeight = 0.0f;
        for (UIView *view in cell.ContentView.subviews) {
            ContentViewHeight += view.frame.size.height;
        }
        cell.ContentView.frame = CGRectMake(0, LessonInfoHeaderHeight + HeaderContentPaddings, self.frame.size.width, ContentViewHeight);
        
        // Set cell's size
        cell.frame = CGRectMake(0, totalHeight, self.frame.size.width, LessonInfoHeaderHeight);
        
        
        // Add target to Button
        [cell.TitleButton addTarget:self action:@selector(didSelectCollapseClickButton:) forControlEvents:UIControlEventTouchUpInside];
        
        // Add cell
        [self addSubview:cell];
        
        // Add to DataArray & isClickedArray
        [self.isClickedArray addObject:[NSNumber numberWithBool:NO]];
        [self.dataArray addObject:cell];
        
        // Calculate totalHeight
        totalHeight += LessonInfoHeaderHeight + HeaderContentPaddings;
    }
//    [self configureCompleteReadersField:totalHeight];
//    [self addSubview:self.completeReadersField];
//    totalHeight += self.completeReadersField.frame.size.height + HeaderContentPaddings;
    [self.CollapseClickDelegate showLessonPagesReadStats];

    // Set self's ContentSize and ContentOffset
    [self setContentSize:CGSizeMake(self.frame.size.width, totalHeight)];
    [self setContentOffset:CGPointZero];
    
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark - Close CollapseClickCell
-(void)closeCollapseClickCellAtIndex:(int)index animated:(BOOL)animated {
    // Check if it's open first
    
    if ((self.isClickedArray.count > 0)&&([[self.isClickedArray objectAtIndex:index] boolValue] == YES)) {
        float duration = 0;
        if (animated) {
            duration = 0.25;
        }
        [UIView animateWithDuration:duration animations:^{
            // Resize Cell
            LessonInfoCollapseClickCell *cell = [self.dataArray objectAtIndex:index];
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width,LessonInfoHeaderHeight);
            
            // Change Arrow orientation
            CGAffineTransform transform = CGAffineTransformMakeRotation(0);
            cell.arrow.transform = transform;
            
            // Change isClickedArray
            [self.isClickedArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:NO]];
            
            // Reposition all CollapseClickCells below Cell
            [self repositionCollapseClickCellsBelowIndex:index withOffset:-1*(cell.ContentView.frame.size.height + 1 * HeaderContentPaddings)];
        }];
    }
}


//#pragma mark - Open CollapseClickCell
-(void)openCollapseClickCellAtIndex:(int)index animated:(BOOL)animated {
    // Check if it's not open first
    if ((self.isClickedArray.count > 0) && ([[self.isClickedArray objectAtIndex:index] boolValue] != YES)) {
        float duration = 0;
        if (animated) {
            duration = 0.25;
        }
        [UIView animateWithDuration:duration animations:^{
            // Resize Cell
            LessonInfoCollapseClickCell *cell = [self.dataArray objectAtIndex:index];
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.ContentView.frame.origin.y + cell.ContentView.frame.size.height);
            // Change Arrow orientation
            CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI/2);
            cell.arrow.transform = transform;
            
            // Change isClickedArray
            [self.isClickedArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:YES]];
            
            // Reposition all CollapseClickCells below Cell
            [self repositionCollapseClickCellsBelowIndex:index withOffset:cell.ContentView.frame.size.height + 1 * HeaderContentPaddings];
        }];
    }
}

//-(void)repositionCollapseClickCellsBelowIndex:(int)index withOffset:(float)offset {
//    for (int yy = index+1; yy < self.dataArray.count; yy++) {
//        CollapseClickCell *cell = [self.dataArray objectAtIndex:yy];
//        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y + offset, cell.frame.size.width, cell.frame.size.height);
//    }
//    
//    // Resize self.ContentSize
////    CollapseClickCell *lastCell = [self.dataArray objectAtIndex:self.dataArray.count - 1];
////    [self setContentSize:CGSizeMake(self.frame.size.width, frame.origin.y + frame.size.height)];
//}

-(void)repositionCollapseClickCellsBelowIndex:(int)index withOffset:(float)offset {
    for (int yy = index+1; yy < self.dataArray.count; yy++) {
        CollapseClickCell *cell = [self.dataArray objectAtIndex:yy];
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y + offset, cell.frame.size.width, cell.frame.size.height);
    }
    
    // Resize self.ContentSize
    CollapseClickCell *lastCell = [self.dataArray objectAtIndex:self.dataArray.count - 1];
    [self setContentSize:CGSizeMake(self.frame.size.width, lastCell.frame.origin.y + lastCell.frame.size.height)];
}

- (void)reframeCell:(int)index withContent:(UIView *)content;
{
    LessonInfoCollapseClickCell *cell = [self.dataArray objectAtIndex:index];

    CGFloat offset = content.frame.size.height -cell.ContentView.frame.size.height;
    cell.ContentView.frame = CGRectMake(cell.ContentView.frame.origin.x, cell.ContentView.frame.origin.y, cell.ContentView.frame.size.width, content.frame.size.height);
    if ([[self.isClickedArray objectAtIndex:index]boolValue] == YES) {
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.ContentView.frame.origin.y + cell.ContentView.frame.size.height);
    }

    [self repositionCollapseClickCellsBelowIndex:index withOffset:offset];
}

//#pragma mark - Private Methods
//- (void)configureCompleteReadersField:(CGFloat)originY
//{
//    self.completeReadersField = [self.CollapseClickDelegate viewForCompleteReadersField];
//    CGPoint origin = CGPointMake(0, originY);
//    CGRect frame = self.completeReadersField.frame;
//    frame.origin = origin;
//    self.completeReadersField.frame = frame;
//}

- (void)changeToTitle:(NSString *)title cellIndex:(int)index
{
    LessonInfoCollapseClickCell *cell = [self.dataArray objectAtIndex:index];
    cell.TitleLabel.text = title;
}
@end
