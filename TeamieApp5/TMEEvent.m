//
//  TMEEvent.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEEvent.h"

@implementation TMEEvent


+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"eventID": @"ciid",
        @"calendarID": @"cid",
        @"calendarName": @"name",
        @"userID": @"uid",
        @"from": @"from_date",
        @"to": @"to_date",
        @"allDay": @"all_day",
        @"desc": @"description",
    };
}

+ (NSValueTransformer *)fromJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)toJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{
        @"default": @(TMEEventDefault),
        @"lesson_published": @(TMEEventLesson),
        @"quiz_published": @(TMEEventQuiz),
        @"homework": @(TMEEventHomework),
        @"video_conference": @(TMEEventVideoConference),
    }];
}

- (NSString *)eventTypeName {
    return [[TMEEvent typeJSONTransformer] reverseTransformedValue:@(self.type)];
}

@end
