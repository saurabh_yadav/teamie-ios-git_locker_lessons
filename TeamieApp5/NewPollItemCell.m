//
//  NewPollItemCell.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 23/05/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "NewPollItemCell.h"

@implementation NewPollItemCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
// set background image for cell
-(void)customInit {
    
    UIImage *bg = [UIImage imageNamed:@"ItemNewInput"];
    UIImageView *bgView = [[UIImageView alloc] initWithImage:bg];
    self.backgroundView = bgView;
    
}

// we do not want this cell to change its appearance 
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    return;
    
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
