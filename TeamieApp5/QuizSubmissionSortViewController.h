//
//  QuizSubmissionSortViewController.h
//  TeamieApp5
//
//  Created by Raunak on 23/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    kScore = 0,
    kName,
    kSubmissionTime,
    kCompletionTime
} QuizSortType;

typedef enum {
    kAscending = 0,
    kDescending
}QuizSortOrder;

@protocol QuizSortControllerDelegate <NSObject>
- (void)quizSortControllerDidSelectType:(QuizSortType)type order:(QuizSortOrder)order;
@end

@interface QuizSubmissionSortViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) id<QuizSortControllerDelegate> delegate;
@property (nonatomic) QuizSortType selectedType;
@property (nonatomic) QuizSortOrder selectedOrder;
@end
