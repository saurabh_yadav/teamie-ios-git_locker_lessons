//
//  Lesson.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TMELesson.h"
#import "TMEClassroomAlt.h"
#import "TMEPostAction.h"
#import "TMEReader.h"
#import "TMELessonPageShort.h"
#import "TMELessonAction.h"
#import "TMELessonAdditionalInfo.h"

@implementation TMELesson

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"desc": @"description",
        @"publishDate": @"published_date",
        @"deadline": @"to_date",
        @"numVotes": @"votes.five-star.count",
        @"num_pages_read_by_current_user": @"stats.current_user_read",
        @"tags": @"additional_info.field_lesson_tag",
        @"subjects": @"additional_info.field_subject",
        @"campuses": @"additional_info.field_campus",
        @"levels": @"additional_info.field_school_level",
        @"coverImageURL": @"cover_image.href",
        @"num_reader": @"stats.num_read",
        @"topReaders": @"stats.top_readers",
    };
}

+ (NSValueTransformer *)nidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

//Seems to be indeterminate
+ (NSValueTransformer *)descJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^id(id desc) {
        if ([desc isKindOfClass:NSString.class]) {
            return desc;
        }
        else if ([desc isKindOfClass:NSDictionary.class]) {
            if (((NSDictionary *)desc)[@"value"]) {
                return ((NSDictionary *)desc)[@"value"];
            }
        }
        return @"";
    } reverseBlock:^(NSString *desc) {
        return desc;
    }];}

+ (NSValueTransformer *)authorJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEUser.class];
}

+ (NSValueTransformer *)publishDateJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)deadlineJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)classroomsJSONTransformer {
    return [TMEModel listJSONTransformer:TMEClassroomAlt.class];
}

+ (NSValueTransformer *)coverImageURLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)nextURLJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

+ (NSValueTransformer *)actionsJSONTransformer {
    return [TMEModel listJSONTransformer:TMELessonAction.class];
}

+ (NSValueTransformer *)lesson_pagesJSONTransformer {
    return [TMEModel listJSONTransformer:TMELessonPageShort.class];
}

+ (NSValueTransformer *)topReadersJSONTransformer {
    return [TMEModel listJSONTransformer:TMEReader.class];
}

+ (NSValueTransformer *)tagsJSONTransformer {
    return [TMEModel listJSONTransformer:TMELessonAdditionalInfo.class];
}

+ (NSValueTransformer *)subjectsJSONTransformer {
    return [TMEModel listJSONTransformer:TMELessonAdditionalInfo.class];
}

+ (NSValueTransformer *)campusesJSONTransformer {
    return [TMEModel listJSONTransformer:TMELessonAdditionalInfo.class];
}

+ (NSValueTransformer *)levelsJSONTransformer {
    return [TMEModel listJSONTransformer:TMELessonAdditionalInfo.class];
}

+ (NSValueTransformer *)web_urlJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

- (NSString*)description {
    return [NSString stringWithFormat:@"Nid: %qi \n Title: %@ \n URL: %@ \n Status: %@ Published: %@ Pages: %i Type: %@", [self.nid longLongValue], self.title, self.web_url.absoluteString, self.status, self.publishDate, [self.pages intValue], self.type];
}

@end
