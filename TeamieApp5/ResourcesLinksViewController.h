//
//  ResourcesLinksViewController.h
//  TeamieApp5
//
//  Created by Raunak on 20/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "TeamieWebViewController.h"
#import "WEPopoverController.h"
#import "ResourcesShareMenuViewController.h"

@class ResourcesLinksViewController;

@protocol ResourcesLinksControllerDelegate
- (void)resourcesLinksControllerDidSelectShareOption:(ResourcesLinksViewController*)linksController;
@end


@interface ResourcesLinksViewController : TeamieWebViewController <UIDocumentInteractionControllerDelegate, ShareMenuControllerDelegate, WEPopoverControllerDelegate>

@property (nonatomic, weak) id<ResourcesLinksControllerDelegate> delegate;
@property (nonatomic, strong) NSURL* linkURL;
- (void)openURL:(NSURL*)url WithThoughtID:(NSNumber*)tid;

@end
