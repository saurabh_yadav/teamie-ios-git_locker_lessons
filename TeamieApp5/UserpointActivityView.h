//
//  UserpointActivityView.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 12/14/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserpointActivityView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *activityIcon;
@property (weak, nonatomic) IBOutlet UILabel *activityDayLabel;
@property (weak, nonatomic) IBOutlet UILabel *activityTextLabel;

@end
