//
//  QuestionCellBackViewController.m
//  TeamieApp5
//
//  Created by Raunak on 10/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

/*@interface FileViewLinkController : UIViewController
@property (nonatomic,strong) NSURLRequest* requestURL;
@property (nonatomic,strong) UIWebView* linkWebView;
@property (nonatomic,strong) UIToolbar* toolbar;
@property (nonatomic,strong) UIBarButtonItem* backButton;
@end

@implementation FileViewLinkController
@synthesize requestURL;
@synthesize linkWebView;
@synthesize toolbar;
@synthesize backButton;

- (id)initWithRequestURL:(NSURLRequest*)url {
    self = [super init];
    if (self) {
        requestURL = url;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.parentViewController.navigationController.navigationBarHidden = YES;
    self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
    self.toolbar.tintColor = [UIColor colorWithHex:@"#BE202E" alpha:1.0];
    self.backButton = [[UIBarButtonItem alloc] initWithTitle:TeamieLocalizedString(@"BTN_BACK", nil) style:UIBarButtonItemStyleBordered target:self action:@selector(backButtonPressed)];
    NSArray* toolbarArray = [NSArray arrayWithObject:self.backButton];
    self.toolbar.items = toolbarArray;
    
    self.linkWebView = [[UIWebView alloc] initWithFrame:CGRectMake(self.view.frame.origin.x, 44, self.view.frame.size.width,  self.view.frame.size.height - 44)];
    self.linkWebView.scalesPageToFit = YES;
    [self.view addSubview:self.linkWebView];
    [self.view addSubview:self.toolbar];
    [self.linkWebView loadRequest:self.requestURL];
    
}

- (void)backButtonPressed {
    self.parentViewController.navigationController.navigationBarHidden = NO;
    [self dismissModalViewControllerAnimated:YES];
}

@end*/

#import "QuestionCellBackViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "QuestionFileViewController.h"
#import "TeamieWebViewController.h"

@interface QuestionCellBackViewController ()
@property (nonatomic,strong) UIPopoverController* attachmentsController;
@end

@implementation QuestionCellBackViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.view.clipsToBounds = YES;
    self.view.layer.cornerRadius = 8.0;
    self.view.layer.borderColor = [UIColor blackColor].CGColor;
    self.view.layer.borderWidth = 2.0;
    
    self.questionNumberLabel.text = [self.questionNumber stringValue];
    self.questionNumberLabel.layer.borderColor = [UIColor blackColor].CGColor;
    self.questionNumberLabel.layer.borderWidth = 1.0;
    
    self.questionTypeLabel.text = self.question.questionType;
    self.questionTypeLabel.layer.borderColor = [UIColor blackColor].CGColor;
    self.questionTypeLabel.layer.borderWidth = 1.0;
    self.questionTypeLabel.edgeInsets = UIEdgeInsetsMake(0, 40, 0, 0);
    
    self.closeButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.closeButton.layer.borderWidth = 1.0;
    
    NSString* submission = [[NSString stringWithFormat:@"<strong>Question:</strong></br></br>%@", self.question.questionText] stringByAppendingFormat:@"</br></br><strong>Answer:</strong></br></br>%@", self.answer.answerHtml];
    self.submissionView.scrollView.bounces = NO;
    [self.submissionView loadHTMLString:submission baseURL:nil];
    self.submissionView.layer.borderColor = [UIColor blackColor].CGColor;
    self.submissionView.layer.borderWidth = 1.0;
    
    self.viewFileButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.viewFileButton.layer.borderWidth = 1.0;
    self.viewFileButton.backgroundColor = [UIColor colorWithHex:@"#eeeeee" alpha:1.0];
    self.viewFileButton.layer.cornerRadius = 5.0;
    // Hide the view file button as of now.
    self.submissionView.frame = CGRectMake(CGRectGetMinX(self.submissionView.frame), CGRectGetMinY(self.submissionView.frame), CGRectGetWidth(self.submissionView.frame), CGRectGetMaxY(self.viewFileButton.frame) - CGRectGetMinY(self.submissionView.frame));
    self.viewFileButton.hidden = YES;
    
    // If there is an answer file path available, then make use of it and show that file in the web view
    if (self.answer.answerFilePath) {
        // better to do merge session since the file URL might be a teamie web URL
        [self.submissionView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[TeamieGlobals mergeSessionURLForPath:self.answer.answerFilePath]]]];
    }
    
    self.scoreField.text = [self.answer.currentScore stringValue];
    self.scoreField.delegate = self;
    self.scoreField.layer.borderColor = [UIColor blackColor].CGColor;
    self.scoreField.layer.borderWidth = 0.8;
    self.scoreField.backgroundColor = [UIColor colorWithHex:@"#eeeeee" alpha:1.0];
    
    self.maxScoreLabel.text = [self.question.maxScore stringValue];
    self.maxScoreLabel.layer.borderColor = [UIColor blackColor].CGColor;
    self.maxScoreLabel.layer.borderWidth = 0.8;
    self.maxScoreLabel.backgroundColor = [UIColor colorWithHex:@"#eeeeee" alpha:1.0];
    
    self.saveScoreButton.layer.borderColor = [UIColor blackColor].CGColor;
    self.saveScoreButton.layer.borderWidth = 1.0;
    self.saveScoreButton.backgroundColor = [UIColor colorWithHex:@"#eeeeee" alpha:1.0];
}

- (void)viewDidUnload {
    [self setSubmissionView:nil];
    [self setQuestionNumberLabel:nil];
    [self setQuestionTypeLabel:nil];
    [self setCloseButton:nil];
    [self setViewFileButton:nil];
    [self setMaxScoreLabel:nil];
    [self setSaveScoreButton:nil];
    [self setScoreField:nil];
    [super viewDidUnload];
}

- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary* userInfo = [notification userInfo];
    
    CGRect keyboardRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	
	// Wouldn't it be fantastic if, when in landscape mode, width was actually width and not height?
	CGFloat keyboardHeight = keyboardRect.size.height > keyboardRect.size.width ? keyboardRect.size.width : keyboardRect.size.height;
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    // Animate up or down
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.submissionView.frame;
    newFrame.size.height = newFrame.size.height - keyboardHeight;
    self.submissionView.frame = newFrame;
    
    self.viewFileButton.frame = CGRectMake(self.viewFileButton.frame.origin.x, self.viewFileButton.frame.origin.y - keyboardHeight, self.viewFileButton.frame.size.width, self.viewFileButton.frame.size.height);
    
    self.scoreField.frame = CGRectMake(self.scoreField.frame.origin.x, self.scoreField.frame.origin.y - keyboardHeight, self.scoreField.frame.size.width, self.scoreField.frame.size.height);
    
    self.maxScoreLabel.frame = CGRectMake(self.maxScoreLabel.frame.origin.x, self.maxScoreLabel.frame.origin.y - keyboardHeight, self.maxScoreLabel.frame.size.width, self.maxScoreLabel.frame.size.height);
    
    self.saveScoreButton.frame = CGRectMake(self.saveScoreButton.frame.origin.x, self.saveScoreButton.frame.origin.y - keyboardHeight, self.saveScoreButton.frame.size.width, self.saveScoreButton.frame.size.height);
    [UIView commitAnimations];
}

- (void)keyboardWillHide:(NSNotification *)notification {
	NSDictionary* userInfo = [notification userInfo];
    
    CGRect keyboardRect = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	
	// Wouldn't it be fantastic if, when in landscape mode, width was actually width and not height?
	CGFloat keyboardHeight = keyboardRect.size.height > keyboardRect.size.width ? keyboardRect.size.width : keyboardRect.size.height;
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    // Animate up or down
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
	
    CGRect newFrame = self.submissionView.frame;
    newFrame.size.height = newFrame.size.height + keyboardHeight;
    self.submissionView.frame = newFrame;
    
    self.viewFileButton.frame = CGRectMake(self.viewFileButton.frame.origin.x, self.viewFileButton.frame.origin.y + keyboardHeight, self.viewFileButton.frame.size.width, self.viewFileButton.frame.size.height);
    
    self.scoreField.frame = CGRectMake(self.scoreField.frame.origin.x, self.scoreField.frame.origin.y + keyboardHeight, self.scoreField.frame.size.width, self.scoreField.frame.size.height);
    
    self.maxScoreLabel.frame = CGRectMake(self.maxScoreLabel.frame.origin.x, self.maxScoreLabel.frame.origin.y + keyboardHeight, self.maxScoreLabel.frame.size.width, self.maxScoreLabel.frame.size.height);
    
    self.saveScoreButton.frame = CGRectMake(self.saveScoreButton.frame.origin.x, self.saveScoreButton.frame.origin.y + keyboardHeight, self.saveScoreButton.frame.size.width, self.saveScoreButton.frame.size.height);
    [UIView commitAnimations];
}

- (IBAction)closeButtonPressed:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
    [self.delegate viewDidExit];
}

- (IBAction)viewFileButtonPressed:(id)sender {
    UIButton* button = sender;
    if (!self.attachmentsController) {
        QuestionFileViewController* contentViewController = [[QuestionFileViewController alloc] initWithFileAttachments:self.answer.answerFiles];
        CGFloat maxWidth, maxHeight;
        maxWidth = 350;
        maxHeight = 600;
        
        self.attachmentsController = [[UIPopoverController alloc] initWithContentViewController:contentViewController];
        
        CGFloat height = [self.answer.answerFiles count] * 44;
        if (height > maxHeight) {
            height = maxHeight;
        }
        [self.attachmentsController setPopoverContentSize:(CGSizeMake(maxWidth, height))];
        self.attachmentsController.delegate = self;
        [self.attachmentsController presentPopoverFromRect:button.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
    }
    else {
        [self.attachmentsController dismissPopoverAnimated:YES];
        self.attachmentsController= nil;
    }
}

- (void)popoverControllerDidDismissPopover:(UIPopoverController *)popoverController {
    self.attachmentsController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(UIPopoverController *)popoverController {
    return YES;
}

- (void)lessonAttachmentsControllerDidSelectOptionWithData:(NSDictionary *)option {
    [self.attachmentsController dismissPopoverAnimated:YES];
    self.attachmentsController = nil;
    
    /*TeamieWebViewController * webController = [[TeamieWebViewController alloc] init];
    [webController openURL:[NSURL URLWithString:[option objectForKey:@"preview"]]];
    webController.modalPresentationStyle = UIModalPresentationFullScreen;
    webController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webController];
    [self presentViewController:navigationController animated:YES completion:nil];*/
//    FileViewLinkController* webViewController = [[FileViewLinkController alloc] initWithRequestURL:[NSURLRequest requestWithURL:[NSURL URLWithString:[option objectForKey:@"preview"]]]];
//    [self presentModalViewController:webViewController animated:YES];
}


- (IBAction)saveScoreButtonPressed:(id)sender {
    if ([self.scoreField isFirstResponder]) {
        [self.scoreField resignFirstResponder];
    }
    
    NSString *strMatchstring=@"\\b([0-9%_.+\\-]+)\\b";
    NSPredicate *textpredicate=[NSPredicate predicateWithFormat:@"SELF MATCHES %@", strMatchstring];
    
    if(![textpredicate evaluateWithObject:self.scoreField.text])
    {
        self.scoreField.text = @"";
        [TeamieGlobals addTSMessageInController:self title:@"Invalid Score" message:@"Please Enter a Valid Score!!" type:@"error" duration:3.0 withCallback:nil];
    }
    else {
        NSNumber* enteredScore = [NSNumber numberWithInteger:[self.scoreField.text integerValue]];
        if ([enteredScore doubleValue] < 0 || [enteredScore doubleValue] > [self.question.maxScore doubleValue]) {
            self.scoreField.text = @"";
            [TeamieGlobals addTSMessageInController:self title:@"Invalid Score" message:@"Please Enter a Valid Score!!" type:@"error" duration:3.0 withCallback:nil];
        }
        else {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:TeamieLocalizedString(@"Save Score", nil) message:TeamieLocalizedString(@"Are you sure you want to save this score?", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
            [alertView show];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    }
    else if (buttonIndex ==1) {
        NSArray* objects = [NSArray arrayWithObjects:self.answer.aid, self.question.qid, self.quizID, self.userID, [NSNumber numberWithDouble:[self.scoreField.text doubleValue]], nil];
        NSArray* keys = [NSArray arrayWithObjects:@"aid", @"question_id", @"quiz_id", @"uid", @"score", nil];
        NSDictionary* params = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
        [self.apiCaller performRestRequest:TeamieUserQuizSaveScoreRequest withParams:params];
        [TeamieGlobals addHUDTextOnlyLabel:@"Saving" margin:10.0 yOffset:10.0];
    }
}

- (void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    [TeamieGlobals dismissActivityLabels];
    if (response == nil || [response count] == 0) {
        return;
    }
    [TeamieGlobals addTSMessageInController:self title:@"Save Successful" message:@"The Score was Saved Successfully" type:@"success" duration:3.0 withCallback:nil];
    [self.delegate userScoreChangedWithResponse:[response objectAtIndex:0]];
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserScoreChanged" object:self userInfo:[NSDictionary dictionaryWithObject:[response objectAtIndex:0] forKey:@"newSubmissionDetails"]];
    
}

- (void)requestFailed:(NSString *)message forRequest:(TeamieRESTRequest)requestName {
    [TeamieGlobals dismissActivityLabels];
    [TeamieGlobals addTSMessageInController:self title:@"Save Failed" message:@"Unable to Save Score" type:@"error" duration:3.0 withCallback:nil];
}

@end
