//
//  LPTOCViewController.m
//  LS2
//
//  Created by Wei Wenbo on 4/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import "LPTOCViewController.h"
#import "TeamieWebViewController.h"
#import "LessonPageViewController.h"
#import "NSMutableArray+LPTOCUtilityButtons.h"
#import "FontAwesome+iOS/NSString+FontAwesome.h"
@interface LPTOCViewController ()
@property LessonObject *lesson;
@property NSArray *lessonPages;
@end

@implementation LPTOCViewController{
    BOOL shouldDisplayLabel;
}

- (id)initWithLesson:(LessonObject *)lesson
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Lesson" bundle:nil];
    self = (LPTOCViewController *)[storyboard instantiateViewControllerWithIdentifier:@"LPTOCViewController"];
    
    if (self) {
        self.lesson = lesson;
        shouldDisplayLabel = YES;
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self makeWebRequest];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (shouldDisplayLabel) {
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING", nil) margin:10.0 yOffset:10.0 afterDelay:3.0];
    }

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.lessonPages.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"LPTOCTableViewCell";
    
    LPTOCTableViewCell *cell = (LPTOCTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    // Add utility buttons
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
//    [rightUtilityButtons sw_addUtilityButtonWithColor:
//     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
//                                                title:@"More"];
//    [rightUtilityButtons sw_addUtilityButtonWithColor:
//     [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
//                                                title:@"Delete"];
    
    [rightUtilityButtons lptoc_addUtilityButtonWithColor:[UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0] icon:@"ok"];
    
    [rightUtilityButtons lptoc_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] icon:@"remove"];
    
    cell.rightUtilityButtons = rightUtilityButtons;
    cell.delegate = self;
    
    
    NSDictionary *lessonPage = [self.lessonPages objectAtIndex:indexPath.row];
    
    cell.textLabel.text = [lessonPage valueForKey:@"title"];
    
    return cell;
}

#pragma mark -Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    LessonPageViewController *lessonPageViewController = [[LessonPageViewController alloc]init];
    lessonPageViewController.lessonsList = self.lessonPages;
    
    lessonPageViewController.currentPage = indexPath.row;
    
    [self.navigationController pushViewController:lessonPageViewController animated:YES];
}

#pragma mark - SWTableViewCellDelegate
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSDictionary *lessonPage = [self.lessonPages objectAtIndex:indexPath.row];

    switch (index) {
        case 0:
        {
            // Enable/Disable button is pressed
//            UIActionSheet *shareActionSheet = [[UIActionSheet alloc] initWithTitle:@"Share" delegate:nil cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Share on Facebook", @"Share on Twitter", nil];
//            [shareActionSheet showInView:self.view];
            NSMutableDictionary* params = [[NSMutableDictionary alloc] init];

            [params setValue:[NSString stringWithFormat:@"%qi", [[lessonPage objectForKey:@"lessonID"] longLongValue]] forKey:@"lesson_id"];
            
            [self.apiCaller performRestRequest:TeamieUserLessonToggleStatusRequest withParams:params];

            [cell hideUtilityButtonsAnimated:YES];
            break;
        }
        case 1:
        {
            // Delete button is pressed
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:cell];
//            [patterns removeObjectAtIndex:cellIndexPath.row];
//            [patternImages removeObjectAtIndex:cellIndexPath.row];
            [self.tableView deleteRowsAtIndexPaths:@[cellIndexPath] withRowAnimation:UITableViewRowAnimationLeft];
            break;
        }
        default:
            break;
    }
}


#pragma mark - RestAPI Delegate Methods
- (void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response
{
    [TeamieGlobals dismissActivityLabels];
    shouldDisplayLabel = NO;
    NSLog(@"requestSuccessful is called in LPTableViewController");

    if (requestName == TeamieUserLessonPagesListRequest) {
        NSAssert([self.lesson.pages intValue] == [((LessonPagesList*)[response objectAtIndex:0]).lessonPages count], @"the page number obtained in the request for lesson object should be consistent with that obtained in the request for lesson page list object");
        if ([((LessonPagesList*)[response objectAtIndex:0]).lessonPages count] == 0) {
            NSString* entityUrl = nil;
            NSString* baseUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"baseURL"];
            if (OAUTH_LOGIN_ENABLED) {
                entityUrl = [NSString stringWithFormat:@"%@/merge-session/?access_token=%@&redirect_url=%@", TEAMIE_DEFAULT_OAUTH_REST_URL, [[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"], [NSString stringWithFormat:@"node/%qi", [self.lesson.nid longLongValue]]];
            }
            else {
                entityUrl = [NSString stringWithFormat:@"%@/node/%qi", baseUrl, [self.lesson.nid longLongValue]];
            }
            TeamieWebViewController * webController = [[TeamieWebViewController alloc] init];
            //TODO
            /**********************************
             ***********TODO: openURL**********
             *********************************/
            //        [webController openURL:[NSURL URLWithString:
            webController.modalPresentationStyle = UIModalPresentationFullScreen;
            webController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
            UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webController];
            [self presentViewController:navigationController animated:YES completion:nil];
            [self.navigationController popViewControllerAnimated:NO];
            
        }
        
        else {
            self.lessonPages = ((LessonPagesList*)[response objectAtIndex:0]).lessonPages;
            [self.tableView reloadData];
        }
    }else if (requestName == TeamieUserLessonToggleStatusRequest){
        NSLog(@"Toggle lesson page status successfully");
    }
}

- (void)requestFailed:(NSString *)message forRequest:(TeamieRESTRequest)requestName
{
    [TeamieGlobals dismissActivityLabels];
    shouldDisplayLabel = NO;
}

#pragma mark - Private Methods
- (void)makeWebRequest
{
    NSDictionary* dict = [NSDictionary dictionaryWithObject:self.lesson.nid forKey:@"lid"];
    [self.apiCaller performRestRequest:TeamieUserLessonPagesListRequest withParams:dict];
}
@end
