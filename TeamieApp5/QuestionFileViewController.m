//
//  QuestionFileViewController.m
//  TeamieApp5
//
//  Created by Raunak on 30/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuestionFileViewController.h"

@interface QuestionFileViewController ()
@property (nonatomic, strong) UITableView* tableView;
@end

@implementation QuestionFileViewController

- (id)initWithFileAttachments:(NSArray*)fileAttachments {
    self = [super init];
    if (self) {
        _fileAttachments = fileAttachments;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    //    self.tableView.scrollEnabled = NO;
    //    self.tableView.bounces = NO;
    [self.view addSubview:self.tableView];
    [self.tableView reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.fileAttachments count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"menuCellIdentifier"];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menuCellIdentifier"];
    }
    
    NSDictionary *tempDict;
    tempDict = [self.fileAttachments objectAtIndex:indexPath.row];
    cell.textLabel.text = [[NSURL URLWithString:(NSString*)[tempDict objectForKey:@"href"]] lastPathComponent];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.delegate questionFileControllerDidSelectOptionWithData:[self.fileAttachments objectAtIndex:indexPath.row]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
