//
//  QuestionCellBackViewController.h
//  TeamieApp5
//
//  Created by Raunak on 10/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SubmissionQuestion.h"
#import "SubmissionAnswer.h"
#import "PaddedUILabel.h"
#import "QuizSubmissions.h"

@protocol QuestionCellBackViewControllerDelegate <NSObject>
- (void)userScoreChangedWithResponse:(QuizSubmissions*)newSubmissionObject;
- (void)viewDidExit;
@end

@interface QuestionCellBackViewController : UIViewController <UITextFieldDelegate, UIAlertViewDelegate, UIPopoverControllerDelegate>

@property (strong, nonatomic) SubmissionAnswer* answer;
@property (strong, nonatomic) SubmissionQuestion* question;
@property (strong, nonatomic) NSNumber* questionNumber;
@property (strong, nonatomic) NSNumber* quizID;
@property (strong, nonatomic) NSNumber* userID;
@property (nonatomic, weak) id<QuestionCellBackViewControllerDelegate> delegate;

@property (strong, nonatomic) IBOutlet UIButton *closeButton;
@property (strong, nonatomic) IBOutlet UIButton *viewFileButton;
@property (strong, nonatomic) IBOutlet UILabel *maxScoreLabel;
@property (strong, nonatomic) IBOutlet UIButton *saveScoreButton;
@property (strong, nonatomic) IBOutlet UITextField *scoreField;
@property (strong, nonatomic) IBOutlet UILabel *questionNumberLabel;
@property (strong, nonatomic) IBOutlet PaddedUILabel *questionTypeLabel;
@property (strong, nonatomic) IBOutlet UIWebView *submissionView;

- (IBAction)closeButtonPressed:(id)sender;
- (IBAction)viewFileButtonPressed:(id)sender;
- (IBAction)saveScoreButtonPressed:(id)sender;


@end
