//
//  SlideMenuController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/6/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"
#import "ClassroomMenuViewController.h"

@interface SlideMenuController : UITableViewController <UIAlertViewDelegate>

@property (nonatomic, retain) NSMutableArray* menuSections;
@property (nonatomic, retain) NSMutableArray* menuItems;
@property (strong, nonatomic) NSMutableArray* userClassrooms;

@end
