//
//  TMEPostCreateAttachment.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 10/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEModel.h"

@interface TMEPostCreateAttachment : TMEModel

@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) UIImage *thumbnail;

@end

@interface TMEPostCreateLinkAttachment : TMEPostCreateAttachment

@property (nonatomic, strong) NSString *url;

@end

@interface TMEPostCreateMediaAttachment : TMEPostCreateAttachment

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *hashedName;
@property (nonatomic, strong) NSNumber *fileSize;
@property (nonatomic, strong) NSString *fileMime;
@property (nonatomic, strong) NSString *url;
@property (nonatomic, strong) NSURL *fileURL;

- (NSString *)hashedName;
- (UIImage *)thumbnail;

@end

@interface TMEPostCreateImageAttachment : TMEPostCreateMediaAttachment

@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSNumber *fid;

@end

@interface TMEPostCreateVideoAttachment : TMEPostCreateMediaAttachment

@end

@interface TMEPostCreateAudioAttachment : TMEPostCreateMediaAttachment

@end
