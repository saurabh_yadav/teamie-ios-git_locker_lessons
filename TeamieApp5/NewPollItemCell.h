//
//  NewPollItemCell.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 23/05/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewPollItemCell : UITableViewCell
// textfield for new items
@property (weak, nonatomic) IBOutlet UITextField *textField;
// custom initialization for cell
-(void)customInit;

@end
