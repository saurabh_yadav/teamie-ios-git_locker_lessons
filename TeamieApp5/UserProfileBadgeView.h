//
//  UserProfileBadgeView.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 12/7/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileBadgeView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *badgeImageView;
@property (weak, nonatomic) IBOutlet UILabel *badgeTitleLabel;

@end
