//
//  QuizSubmissionSortViewController.m
//  TeamieApp5
//
//  Created by Raunak on 23/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuizSubmissionSortViewController.h"

@interface QuizSortTableViewCell : UITableViewCell
@property (nonatomic, strong) UISegmentedControl* toggleButton;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier;
@end

@implementation QuizSortTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _toggleButton = [[UISegmentedControl alloc] initWithItems:@[@"Asc", @"Desc"]];
        _toggleButton.segmentedControlStyle = UISegmentedControlStyleBar;
        _toggleButton.selectedSegmentIndex = 0;
        self.accessoryView = _toggleButton;
        
        //Change to Font Awesome after merge
        self.imageView.image = [UIImage imageNamed:@"icon-question-comment-correct.png"];        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [self.imageView setHidden:!selected];
    [self.toggleButton setEnabled:selected];
}
@end

@interface QuizSubmissionSortViewController ()
@property (nonatomic, strong) UITableView* tableView;
@end

@implementation QuizSubmissionSortViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self setContentSizeForViewInPopover:CGSizeMake(320, (45.0 * 4) + 20.0)];
	// Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.scrollEnabled = NO;
    [self.view addSubview:self.tableView];
    [self.tableView reloadData];
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedType inSection:0] animated:NO scrollPosition:UITableViewScrollPositionNone];
    ((QuizSortTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.selectedType inSection:0]]).toggleButton.selectedSegmentIndex = self.selectedOrder;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"menuCellIdentifier"];
    if (!cell) {
        cell = [[QuizSortTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menuCellIdentifier"];
    }
    [((QuizSortTableViewCell*)cell).toggleButton addTarget:self action:@selector(toggleValueChanged:) forControlEvents:UIControlEventValueChanged];
    ((QuizSortTableViewCell*)cell).toggleButton.tag = indexPath.row;
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Score";
            break;
        case 1:
            cell.textLabel.text = @"User";
            break;
        case 2:
            cell.textLabel.text = @"Submission Time";
            break;
        case 3:
            cell.textLabel.text = @"Completion Time";
            break;
        default:
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedType = indexPath.row;
    self.selectedOrder = ((QuizSortTableViewCell*)[self.tableView cellForRowAtIndexPath:indexPath]).toggleButton.selectedSegmentIndex;
    [self.delegate quizSortControllerDidSelectType:self.selectedType order:self.selectedOrder];
}

- (void)toggleValueChanged:(id)sender {
    self.selectedType = ((UISegmentedControl*)sender).tag;
    self.selectedOrder = ((UISegmentedControl*)sender).selectedSegmentIndex;
    [self.delegate quizSortControllerDidSelectType:self.selectedType order:self.selectedOrder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
