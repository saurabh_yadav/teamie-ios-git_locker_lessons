//
//  PageStatsLabel.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 2/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#define PageStatsLeftRightPaddings 5
#define FontSizeForPageReadStats 11

@interface PageStatsLabel : UILabel

- (void)setTextWhileStickToRightTopCornor:(NSString *)pageReadStatsString;
@end
