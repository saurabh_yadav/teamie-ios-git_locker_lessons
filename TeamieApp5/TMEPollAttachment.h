//
//  TMEPollAttachment.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 02/01/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEModel.h"

typedef NS_ENUM (NSUInteger, PollType) {
    PollTypeSingle,
    PollTypeMultiple
};

@interface TMEPollAttachment : TMEModel

@property (nonatomic) PollType pollType;
@property (nonatomic) BOOL isAllowedToVote;
@property (nonatomic) BOOL showPollResults;
@property (nonatomic, strong) NSNumber * choiceCount;
@property (nonatomic, strong) NSNumber * totalVotes;
@property (nonatomic, strong) NSArray *choices;
@property (nonatomic, strong) NSMutableArray * selectedPollOptions;
@property (nonatomic) BOOL isFooterVisible;
@property (nonatomic) BOOL didSubmitRequest;

@end