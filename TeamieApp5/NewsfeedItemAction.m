//
//  NewsfeedItemAction.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 11/29/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "NewsfeedItemAction.h"
#import "NewsfeedItem.h"


@implementation NewsfeedItemAction

@dynamic name;
@dynamic access;
@dynamic status;
@dynamic title;
@dynamic href;
@dynamic method;
@dynamic newsfeedItem;

- (NSString*)description {
    
    return [NSString stringWithFormat:@"Name: %@ - Access: %i - Status: %i - Title: %@ - Href: %@ - Method: %@", self.name, [self.access intValue], [self.status intValue], self.title, self.href, self.method];
    
}
@end
