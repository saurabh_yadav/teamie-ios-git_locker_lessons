//
//  UserProfileCollapseClickCell.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 7/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "CollapseClickCell.h"
#define UserProfileHeaderHeight 49
#define CellDistance 1
#define HeaderContentPaddings 1
@interface UserProfileCollapseClickCell : CollapseClickCell
//Header
@property (weak, nonatomic) IBOutlet UIView *TitleView;
@property (weak, nonatomic) IBOutlet UILabel *TitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *TitleButton;
@property (weak, nonatomic) IBOutlet UIImageView *arrow;

//body
@property (weak, nonatomic) IBOutlet UIView *ContentView;

+ (UserProfileCollapseClickCell *)newCollapseClickCellWithTitle:(NSString *)title index:(int)index content:(UIView *)content;
@end