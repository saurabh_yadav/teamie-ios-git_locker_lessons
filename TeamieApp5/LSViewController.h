//
//  LSViewController.h
//  LS2
//
//  Created by Wei Wenbo on 2/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LSCollapseClick.h"

@interface LSViewController : UIViewController <LSCollapseClickDelegate,UITableViewDataSource,UITableViewDelegate, UIScrollViewDelegate>{
    __weak IBOutlet LSCollapseClick *myCollapseClick;
}

//Methods
- (id)initWithSubjectID:(NSNumber *)subjectID withNavTitle:(NSString *)title;
- (id)initWithtitle:(NSString *)title;


//Properties
@property (nonatomic)NSNumber *subjectID;
@property NSString *classroomName;
@property (nonatomic, strong) UIView      *emptyView;

@end
