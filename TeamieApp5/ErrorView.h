//
//  ErrorView.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 10/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ErrorView : UIView

- (id)initWithFrame:(CGRect)frame title:(NSString*)title subtitle:(NSString*)subtitle image:(UIImage*)image;

@end
