//
//  UserProfileStatsTableViewCell.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 8/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UserProfileStatsTableCellHeight 44
@interface UserProfileStatsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *statsValue;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *statsTitle;

@end
