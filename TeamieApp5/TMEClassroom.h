//
//  TMEClassroom.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TMEModel.h"

@interface TMEClassroom : TMEModel

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * nid;
@property (nonatomic, retain) NSNumber * freshThoughts;
@property (nonatomic, retain) NSDictionary *permissions;

- (BOOL)hasPermission:(NSString *)permission;

@end