//
//  TMENewsfeedViewController.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 21/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TeamieRevealController.h"
#import "TMEListViewController.h"
#import "TMEPostView.h"

@interface TMENewsfeedViewController : TMEListViewController <TMERevealDelegate>

@property (nonatomic, weak) UIViewController *parentRevealController;
@property (nonatomic, strong) NSMutableArray *posts;
@property (nonatomic) TMEPostViewMode mode;
@property (nonatomic) BOOL showMore;

+ (instancetype)newsfeed;
- (void)refresh;
- (NSInteger)indexForPostWithID: (NSNumber *)postID;
@end
