//
//  PaddedUILabel.h
//  TeamieApp5
//
//  Created by Raunak on 19/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaddedUILabel : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

@end
