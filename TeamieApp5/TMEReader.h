//
//  ReaderObject.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 14/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEImage.h"
#import "TMEModel.h"

@interface TMEReader : TMEModel

@property (nonatomic,retain) NSString *name;
@property (nonatomic,retain) NSNumber *uid;
@property (nonatomic,retain) NSNumber *num_read;
@property (nonatomic,retain) NSDate *last_access;
@property (nonatomic,retain) NSTimeZone *access_timezone;
@property (nonatomic,retain) TMEImage *user_profile_image;

@end
