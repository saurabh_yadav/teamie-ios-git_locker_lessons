//
//  BulletinBoardViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 2/7/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "BulletinBoardViewController.h"
#import "TMENotification.h"
#import "BulletinBoardEntryCell.h"
#import "TMEClient.h"
#import "UIScrollView+SVInfiniteScrolling.h"
#import "TMEPostViewController.h"
#import "UIView+TMEEssentials.h"

@interface BulletinBoardViewController()

@property (nonatomic, weak) UIViewController *parentRevealController;

@end

static NSString *const cellIdentifier = @"BBEC";
static BulletinBoardEntryCell *sampleCell;
BOOL loadMore;
UIRefreshControl *refreshControl;
NSMutableArray *consolidateBulletinBoardEntries;

@implementation BulletinBoardViewController

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    loadMore = NO;
    consolidateBulletinBoardEntries = [NSMutableArray array];
    self.view.backgroundColor = [TeamieUIGlobalColors baseBgColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[UINib nibWithNibName:@"BulletinBoardEntryCell" bundle:[NSBundle mainBundle]]
         forCellReuseIdentifier:cellIdentifier];

    //Sample cell is used to calculate row height
    [self createSampleCell];
    
    //This is used to calculate the scroll bar. Estimate is for two line notifications
    self.tableView.estimatedRowHeight = 78;

    // Set the title of the Bulletin board view controller
    self.navigationItem.title = TMELocalize(@"menu.notifications");

    if (!consolidateBulletinBoardEntries.count) {
        [self loadWithParameters:NO];
    }
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refresh) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [TMEAnalyticsController trackScreen:@"/notifications"];
}

- (void)refresh {
    [self loadWithParameters:YES];
}

- (void)loadWithParameters:(BOOL)withParameters {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"reset": @YES}];
    if (!withParameters) {
        loadMore = YES;
        params[@"items_per_page"] = @(NUMBER_OF_BULLETIN_BOARD_ITEMS);
        [consolidateBulletinBoardEntries removeAllObjects];
    }
    else if (loadMore) {
        params[@"page"] = @(consolidateBulletinBoardEntries.count/NUMBER_OF_BULLETIN_BOARD_ITEMS + 1);
        params[@"items_per_page"] = @(NUMBER_OF_BULLETIN_BOARD_ITEMS);
    }

    [[TMEClient sharedClient]
     request:TeamieUserBulletinBoardRequest
     parameters:params
     loadingMessage:TMELocalize(@"message.loading")
     completion:^{
         if (!consolidateBulletinBoardEntries.count) {
             [self viewForEmptyListWithTitleText:nil subtitleText:nil andImage:nil];
         }
     }
     success:^(NSDictionary *response) {
         self.bulletinBoardEntries = [TMENotification parseList:response[@"bulletin-board"]];
         [consolidateBulletinBoardEntries addObjectsFromArray:self.bulletinBoardEntries];
         
         [self.tableView reloadData];
         [self.tableView.infiniteScrollingView stopAnimating];
         [refreshControl endRefreshing];
         
         if (self.bulletinBoardEntries.count && self.bulletinBoardEntries.count == NUMBER_OF_BULLETIN_BOARD_ITEMS) {
             loadMore = YES;
         }
         else {
             loadMore = NO;
         }
     } failure:nil];
}

#pragma mark - UITableViewDataSourceDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return consolidateBulletinBoardEntries.count;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (loadMore) {
        if (indexPath.row == consolidateBulletinBoardEntries.count - 1) {
            self.tableView.infiniteScrollingView.enabled = YES;
            __unsafe_unretained typeof(self) weakSelf = self;
            [weakSelf.tableView addInfiniteScrollingWithActionHandler:^{
                [weakSelf loadWithParameters:YES];
            }];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    BulletinBoardEntryCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    [cell setCellData:[consolidateBulletinBoardEntries objectAtIndex:indexPath.row]];
    if (indexPath.row > 0) {
        [cell addBorderToTop:YES bottom:NO left:NO right:NO];
    }
    return cell;
}

//Load the static sampleCell and load it with the mainText and calculate it's height
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [sampleCell getCellHeight:[consolidateBulletinBoardEntries objectAtIndex:indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TMENotification *entry = [consolidateBulletinBoardEntries objectAtIndex:indexPath.row];
    
    if (!entry.isRead) {
        //Send notice to mark it as read
        [[TMEClient sharedClient]
         request:TeamieUserMarkNotificationReadRequest
         parameters:@{@"msgid": entry.msgid,
                      @"reset": @0}
         makeURL:^NSString *(NSString *URL) {
             return [NSString stringWithFormat:URL, entry.msgid];
         } loadingMessage:nil
         completion:^{
             [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
         } success:^(id response) {
             entry.readStatus = YES;
         } failure:^(NSError *error) {
             entry.readStatus = NO;
         }];
    }
    
    NSString *linkToOpen;

    switch (entry.notificationType) {
        case PlainBulletinNotification:
            linkToOpen = @"bulletin-board";
            break;
        case BadgeAwardNotification:
            linkToOpen = @"user";
            break;
        case LessonPostNotification:
            linkToOpen = [NSString stringWithFormat:@"node/%@", entry.entityID];
            break;
        case QuizPostNotification:
            linkToOpen =[NSString stringWithFormat:@"quiz/%@", entry.entityID];
            break;
        case ThoughtAnnouncementNotification:
        case ThoughtEchoNotification:
        case ThoughtCommentNotification:
        case ThoughtCommentActionNotification:
        case ThoughtLikeNotification: {
            [self.navigationController pushViewController:[TMEPostViewController postWithId:@([entry.entityID longLongValue])]
                                                 animated:YES];
            return;
        }
    }

    [TeamieGlobals openInappBrowserWithURL:[NSURL URLWithString:linkToOpen] fromResponder:self];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [self.navigationController pushViewController:viewController animated:animated];
}

#pragma mark - Private

- (void)createSampleCell {
    sampleCell = [[[NSBundle mainBundle] loadNibNamed:@"BulletinBoardEntryCell" owner:self options:nil] objectAtIndex:0];
    [sampleCell setNeedsUpdateConstraints];
    [sampleCell updateConstraintsIfNeeded];
    sampleCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.bounds), CGRectGetHeight(sampleCell.bounds));
}

- (void)setParentRevealController:(id)controller {
    _parentRevealController = controller;
}

@end
