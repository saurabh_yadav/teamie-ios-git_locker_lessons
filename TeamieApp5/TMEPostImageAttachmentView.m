//
//  ThoughtImageAttachmentView.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 16/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostImageAttachmentView.h"
#import "MWPhotoBrowser.h"
#import "TMEPostAttachment.h"
#import "TMEImageAttachment.h"
#import <SDWebImage/SDWebImageDownloader.h>
#import "SDWebImageManager.h"
#import "Masonry.h"

#import "TMENewsfeedViewController.h"
#import "TMEPostViewController.h"
#import "TMEPostView.h"
#import "TMEPostCommentView.h"
#import "TMEPostCommentReplyView.h"

#import <ReactiveCocoa/RACEXTScope.h>

#import "TeamieUIGlobalColors.h"

@interface TMEPostImageAttachmentView() <MWPhotoBrowserDelegate>

@property (nonatomic, strong) NSArray* imageSource;
@property (nonatomic, strong) NSArray* viewArray;
@property (nonatomic, strong) NSArray* MVPhotoArray;
@property (nonatomic) NSInteger numberOfImagesDownloaded;

@property (nonatomic, strong) NSLayoutConstraint* heightConstraint;
@property (nonatomic, weak) UIViewController *parentRevealController;

@property (strong, nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong, nonatomic) NSArray *imageAttachments;

@end

@implementation TMEPostImageAttachmentView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithImageAttachments:(NSArray *)imageAttachments delegate:(id<ThoughtAttachmentViewDelegate>)delegate width:(CGFloat)attachmentWidth
{
    self = [super init];
    if (self){
        self.delegate = delegate;
        self.imageSource = [self createImageSource:imageAttachments];
        self.imageAttachments = imageAttachments;
    }
    self.frame = CGRectMake(0, 0,attachmentWidth, 100);
    
    return self;
}
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}
#pragma mark - Layout image views
- (void)layoutImages{
    
    int numSourceImages = (int)[self.imageSource count];
    
    if (numSourceImages == 0){
        return;
    }
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    switch (numSourceImages) {
        case 1:
            self.viewArray = @[[self layoutOneImage:[self.imageSource firstObject]]];
            break;
        case 2:
            self.viewArray = [self layoutTwoImages:self.imageSource];
            break;
        case 3:
            self.viewArray = [self layoutThreeImages:self.imageSource];
            break;
        default:
            self.viewArray = [self layoutMoreThanThreeImages:self.imageSource];
            break;
    }
    
    int numViews = (int)[self.viewArray count];
        
    for (int i = 0; i < numViews; i++){
        UIImageView* view = [self.viewArray objectAtIndex:i];

        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(imageViewDidTapped:)];
        view.tag = i;
        view.userInteractionEnabled = YES;
        [view addGestureRecognizer:tapGesture];
    }
    
    self.heightConstraint.constant = [self calcFrameHeight];
}

- (UIView*)layoutOneImage:(UIImage*)image{
    CGFloat maxWidth = self.frame.size.width;
    CGFloat maxHeight = maxWidth * 2.0 / 3.0;
    
    UIImageView* view = [[UIImageView alloc] initWithImage:image];
    view.contentMode = UIViewContentModeScaleAspectFill ;
    view.clipsToBounds = YES;
    view.frame = CGRectMake(0, 0, maxWidth, maxHeight);
   
    [self addSubview:view];
    
    return view;
}

- (NSArray*)layoutTwoImages:(NSArray*)images{
    assert([images count] == 2);
    
    CGFloat width = self.frame.size.width / 2;
    CGFloat height = width;
    CGFloat x1 = 0;
    CGFloat x2 = x1 + width;
    CGFloat y1 = 0;
    CGFloat y2 = 0;
    
    UIImageView* view1 = [[UIImageView alloc] initWithImage:[images firstObject]];
    UIImageView* view2 = [[UIImageView alloc] initWithImage:[images lastObject]];
    view1.contentMode = UIViewContentModeScaleAspectFill;
    view2.contentMode = UIViewContentModeScaleAspectFill;
    view1.clipsToBounds = YES;
    view2.clipsToBounds = YES;
    
    view1.frame = CGRectMake(x1, y1, width, height);
    view2.frame = CGRectMake(x2, y2, width, height);
    
    [self addSubview:view1];
    [self addSubview:view2];
    
    return @[view1, view2];
}

- (NSArray*)layoutThreeImages:(NSArray*)images{
    assert([images count] == 3);
    
    CGFloat largeWidth = self.frame.size.width;
    CGFloat largeHeight = largeWidth * 2 / 3;
    CGFloat smallWidth = largeWidth / 2;
    CGFloat smallHeight = smallWidth * 2 / 3;
    
    CGFloat x1 = 0;
    CGFloat y1 = 0;
    CGFloat x2 = 0;
    CGFloat x3 = x2 + smallWidth;
    CGFloat y2 = largeHeight;
    CGFloat y3 = y2;
    
    UIImageView* view1 = [[UIImageView alloc] initWithImage:[images firstObject]];
    UIImageView* view2 = [[UIImageView alloc] initWithImage:[images objectAtIndex:1]];
    UIImageView* view3 = [[UIImageView alloc] initWithImage:[images lastObject]];
    view1.contentMode = UIViewContentModeScaleAspectFill;
    view2.contentMode = UIViewContentModeScaleAspectFill;
    view3.contentMode = UIViewContentModeScaleAspectFill;
    view1.clipsToBounds = YES;
    view2.clipsToBounds = YES;
    view3.clipsToBounds = YES;
    
    view1.frame = CGRectMake(x1, y1, largeWidth, largeHeight);
    view2.frame = CGRectMake(x2, y2, smallWidth, smallHeight);
    view3.frame = CGRectMake(x3, y3, smallWidth, smallHeight);
    
    [self addSubview:view1];
    [self addSubview:view2];
    [self addSubview:view3];
    
    return @[view1, view2, view3];
}

- (NSArray*)layoutMoreThanThreeImages:(NSArray*)images{
    assert([images count] > 3);
    
    CGFloat largeWidth = self.frame.size.width;
    CGFloat largeHeight = largeWidth * 2 / 3;
    CGFloat smallWidth = largeWidth / 3;
    CGFloat smallHeight = smallWidth * 2 / 3;
    
    CGFloat x1 = 0;
    CGFloat y1 = 0;
    CGFloat x2 = 0;
    CGFloat x3 = x2 + smallWidth;
    CGFloat x4 = x3 + smallWidth;
    CGFloat y2 = largeHeight - smallHeight;
    CGFloat y3 = y2;
    CGFloat y4 = y3;
    
    UIImageView* view1 = [[UIImageView alloc] initWithImage:[images firstObject]];
    UIImageView* view2 = [[UIImageView alloc] initWithImage:[images objectAtIndex:1]];
    UIImageView* view3 = [[UIImageView alloc] initWithImage:[images lastObject]];
    view1.contentMode = UIViewContentModeScaleAspectFill;
    view2.contentMode = UIViewContentModeScaleAspectFill;
    view3.contentMode = UIViewContentModeScaleAspectFill;
    view1.clipsToBounds = YES;
    view2.clipsToBounds = YES;
    view3.clipsToBounds = YES;
    
    view1.frame = CGRectMake(x1, y1, largeWidth, largeHeight);
    view2.frame = CGRectMake(x2, y2, smallWidth, smallHeight);
    view3.frame = CGRectMake(x3, y3, smallWidth, smallHeight);
    
    UILabel* view4 = [[UILabel alloc] initWithFrame:CGRectMake(x4, y4, smallWidth, smallHeight)];
    view4.backgroundColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.6];
    view4.textColor = [UIColor whiteColor];
    view4.font = [TeamieGlobals appFontFor:@"thoughtBody"];
    view4.textAlignment = NSTextAlignmentCenter;
    view4.text = [NSString stringWithFormat:@"%d more", [images count] - 3];
    
    [self addSubview:view1];
    [self addSubview:view2];
    [self addSubview:view3];
    [self addSubview:view4];
    
    return @[view1, view2, view3];
}

#pragma mark - Handle image view tapped
- (void)imageViewDidTapped:(UITapGestureRecognizer*)tapGesture{
    
    int index = tapGesture.view.tag;
    
    
    if (self.MVPhotoArray == nil || [self.MVPhotoArray count] == 0){
        NSMutableArray* array = [[NSMutableArray alloc] init];
        for (UIImage* image in self.imageSource){
            MWPhoto* photo = [MWPhoto photoWithImage:image];
            [array addObject:photo];
        }
        self.MVPhotoArray = array;
    }
    
    // Create photo browser. MVPhotoBrowser cannot be reused
    MWPhotoBrowser* browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.displayNavArrows = YES;
    browser.alwaysShowControls = YES;
    browser.wantsFullScreenLayout = NO;
    [browser.navigationItem setHidesBackButton:NO];
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    [nc.navigationController setNeedsStatusBarAppearanceUpdate];
    
    
    [self.viewController presentViewController:nc animated:YES completion:nil];
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:index];
}

#pragma mark - MVPhotoBrowser delegate
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return [self.MVPhotoArray count];
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    if (index < [self.MVPhotoArray count]){
        return [self.MVPhotoArray objectAtIndex:index];
    }
    return nil;
}

#pragma mark - Lazy Instantiation
- (NSLayoutConstraint*)heightConstraint{
    if (!_heightConstraint){
        _heightConstraint = [NSLayoutConstraint constraintWithItem:self
                                                         attribute:NSLayoutAttributeHeight
                                                         relatedBy:NSLayoutRelationEqual
                                                            toItem:nil
                                                         attribute:NSLayoutAttributeNotAnAttribute
                                                        multiplier:1.0
                                                          constant:1.0];
        self.translatesAutoresizingMaskIntoConstraints = NO;
        [self addConstraint:_heightConstraint];
    }
    return _heightConstraint;
}

#pragma mark - Private methods
- (NSArray*)createImageSource:(NSArray*)attachments{
    NSMutableArray* result = [[NSMutableArray alloc] init];
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(10, 10, 100, 100)];
    self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
    __block CGSize targetSize;
    __block UIImage *newImage;
    [self addSubview:self.activityIndicator];
    
    [self.activityIndicator mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self).centerOffset(CGPointMake(0, 50));
    }];
    int i = 0;
    for (TMEImageAttachment* attachment in attachments){
        i = i + 1;
        [self.activityIndicator setHidden:NO];
        [self.activityIndicator startAnimating];
        
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        [manager downloadImageWithURL: attachment.href
                              options:0
                             progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                                 
                                 [self.activityIndicator setHidden:NO];
                                 [self.activityIndicator startAnimating];
                             }
                            completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                                [self.activityIndicator setHidden:YES];
                                [self.activityIndicator stopAnimating];
                                targetSize = [[UIScreen mainScreen] bounds].size;
                                if (image)
                                {
                                    if (image.size.width * image.size.height > targetSize.width * targetSize.height) {
                                        newImage = [self imageWithImage:image scaledToMaxWidth:targetSize.width maxHeight:targetSize.height];
                                    }
                                    else{
                                        newImage = image;
                                    }
                                    ++self.numberOfImagesDownloaded;
                                    [result addObject:newImage];
                                    if (self.numberOfImagesDownloaded == attachments.count) {
                                        dispatch_async(dispatch_get_main_queue(), ^(){
                                            [self layoutImages];
                                        });
                                    }
                                }
                            }];
    }
return result;
}

- (CGFloat)calcFrameHeight{
    CGRect rect = CGRectZero;
    
    for (UIView* view in self.subviews){
        rect = CGRectUnion(rect, view.frame);
    }
    
    return rect.size.height;
}
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [self.viewController presentViewController:viewController animated:YES completion:nil];
}
- (void)backPressed:(id)sender{
    [self.viewController dismissViewControllerAnimated:YES completion:nil];
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage *)imageWithImage:(UIImage *)image scaledToMaxWidth:(CGFloat)width maxHeight:(CGFloat)height {
    CGFloat oldWidth = image.size.width;
    CGFloat oldHeight = image.size.height;
    
    CGFloat scaleFactor = (oldWidth > oldHeight) ? width / oldWidth : height / oldHeight;
    
    CGFloat newHeight = oldHeight * scaleFactor;
    CGFloat newWidth = oldWidth * scaleFactor;
    CGSize newSize = CGSizeMake(newWidth, newHeight);
    
    return [self imageWithImage:image scaledToSize:newSize];
}


@end
