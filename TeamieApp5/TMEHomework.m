//
//  TMEHomework.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 24/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEHomework.h"

@implementation TMEHomework

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    mapping[@"deadline"] = @"attachment.homework.deadline";
    return mapping;
}

+ (NSValueTransformer *)deadlineJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

@end
