//
//  StatViewItem.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 9/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "StatViewItem.h"

@implementation StatViewItem

+ (StatViewItem*)itemWithText:(NSString *)text
                     subtitle:(NSString *)subtitle
                     statText:(NSString *)statText
                  statSubtext:(NSString *)statSubtext
                     barWidth:(NSNumber *)barWidth{
    
    StatViewItem* item = [[StatViewItem alloc] init];
    
    if (item){
        item.text = text;
        item.subtitle = subtitle;
        item.statText = statText;
        item.statSubtext = statSubtext;
        item.barWidth = barWidth;
    }
    
    return item;
}

@end
