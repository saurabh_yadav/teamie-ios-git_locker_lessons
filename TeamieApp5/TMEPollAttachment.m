//
//  TMEPollAttachment.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 02/01/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEPollAttachment.h"
#import "TMEPollOption.h"

@implementation TMEPollAttachment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"pollType": @"poll_type",
             @"isAllowedToVote": @"allowed_to_vote",
             @"showPollResults": @"show_poll_results",
             @"choiceCount": @"choice_count",
             @"totalVotes": @"total_votes",
             @"choices": @"choices",
             };
}

#pragma mark - Mapping Transformations

+ (NSValueTransformer *)choicesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEPollOption.class];
}

@end
