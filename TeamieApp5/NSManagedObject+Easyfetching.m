//
//  NSManagedObject+Easyfetching.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/27/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "NSManagedObject+Easyfetching.h"

@implementation NSManagedObject (EasyFetching)

+ (NSEntityDescription *)entityDescriptionInContext:(NSManagedObjectContext *)context;
{
    return [self respondsToSelector:@selector(entityInManagedObjectContext:)] ?
    [self performSelector:@selector(entityInManagedObjectContext:) withObject:context] :
    [NSEntityDescription entityForName:NSStringFromClass(self) inManagedObjectContext:context];
}

+ (NSArray *)findAllObjects;
{
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    return [self findAllObjectsInContext:context];
}

+ (NSArray *)findAllObjectsInContext:(NSManagedObjectContext *)context;
{
    NSEntityDescription *entity = [self entityDescriptionInContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if (error != nil)
    {
        //handle errors
    }
    return results;
}

+ (NSArray *)findAllObjectsWithPredicate:(id)predicate {
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    return [self findAllObjectsWithPredicate:predicate inContext:context];
}

+ (NSArray *)findAllObjectsWithPredicate:(id)predicate inContext:(NSManagedObjectContext *)context {
    // there is no context. Error!
    if (!context) {
        return nil;
    }
    
    NSPredicate * predicateObj;
    if ([predicate isKindOfClass:[NSString class]]) {
        predicateObj = [NSPredicate predicateWithFormat:predicate];
    }
    else if ([predicate isKindOfClass:[NSPredicate class]]) {
        predicateObj = predicate;
    }
    else {
#ifdef DEBUG
        NSLog(@"The predicate argument supplied is not a string or NSPredicate object");
#endif
        return nil;
    }
    
    NSEntityDescription *entity = [self entityDescriptionInContext:context];
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entity];
    
    [request setPredicate:predicateObj];
    
    NSError *error = nil;
    NSArray *results = [context executeFetchRequest:request error:&error];
    if (error != nil)
    {
#ifdef DEBUG
        NSLog(@"The following error occurred while retrieving objects from the database: %@", error);
#endif
    }
    return results;
}

+ (NSManagedObject*)findFirstObjectWithPredicate:(id)predicate {
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    return [self findFirstObjectWithPredicate:predicate inContext:context];
}

+ (NSManagedObject*)findFirstObjectWithPredicate:(id)predicate inContext:(NSManagedObjectContext *)context {
    
    NSArray* results = [self findAllObjectsWithPredicate:predicate inContext:context];
    
    if ([results count] > 0) {
        return [results objectAtIndex:0];
    }
    else {
        return nil;
    }
}

+ (NSInteger)updateAllObjectsWithPredicate:(id)predicate withValues:(NSDictionary *)values {
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    return [self updateAllObjectsWithPredicate:predicate withValues:values inContext:context];
}

+(NSInteger)updateAllObjectsWithPredicate:(id)predicate withValues:(NSDictionary *)values inContext:(NSManagedObjectContext *)context {
    if (!context) {
        return 0;
    }
    
    NSPredicate * predicateObj;
    
    if ([predicate isKindOfClass:[NSString class]]) {
        predicateObj = [NSPredicate predicateWithFormat:predicate];
    }
    else if ([predicate isKindOfClass:[NSPredicate class]]) {
        predicateObj = predicate;
    }
    else {
#ifdef DEBUG
        NSLog(@"The predicate argument supplied is not a string or NSPredicate object");
#endif
        return 0;
    }
    
    NSArray * objects = [self findAllObjectsWithPredicate:predicateObj];
    if (!objects) {
#ifdef DEBUG
        NSLog(@"No objects updated.");
#endif
        return 0;
    }
    
    NSInteger n = 0;
    
    for (id object in objects) { // for each retrieved record
        for (id key in values) { // for each key-value pair in the dictionary
            [object setValue:[values valueForKey:key] forKey:key];
        }
        n++;
    }
    
    NSError *error = nil;
    [context save:&error];
    
    if (error != nil) {
#ifdef DEBUG
        NSLog(@"The following error occurred while updating the database: %@", error);
#endif
    }
    
#ifdef DEBUG
    NSLog(@"%i records updated successfully.", n);
#endif
    
    return n;
}

+(NSEntityDescription*)newObjectWithValues:(NSDictionary *)values {
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    return [self newObjectWithValues:values inContext:context];
}

+(NSEntityDescription*)newObjectWithValues:(NSDictionary *)values inContext:(NSManagedObjectContext *)context {
    // The context for the new object can be nil, in which case it will be a temporary unassociated nsmanagedobject
    id newObject;
    
    if (context != nil) {
        newObject = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass(self) inManagedObjectContext:context];
    }
    else {
        NSManagedObjectContext *myMOC = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
        NSEntityDescription* entity = [NSEntityDescription entityForName:NSStringFromClass(self) inManagedObjectContext:myMOC];
        newObject = [[NSManagedObject alloc] initWithEntity:entity insertIntoManagedObjectContext:nil];
    }
    
    // add each entry in the dictionary to the object
    for (id key in values) {
        [newObject setValue:[values objectForKey:key] forKey:key];
    }
    
    if (context != nil) {       // save data in db only if context is not nil
        
        NSError *error;
        [context save:&error];
        
        if (error != nil) {
#ifdef DEBUG
            NSLog(@"The following error occurred while creating new database entry: %@", error);
#endif
        }
        else {
#ifdef DEBUG
            NSLog(@"Database record created successfully.");
#endif
        }
    }
    
    return newObject;
}

+ (NSEntityDescription*)newObjectWithValues:(NSDictionary *)values forEntity:(NSString*)entityName {
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    return [self newObjectWithValues:values forEntity:entityName inContext:context];
}

+ (NSEntityDescription*)newObjectWithValues:(NSDictionary *)values forEntity:(NSString*)entityName inContext:(NSManagedObjectContext*)context {
    if (!context) { return nil; }
    NSEntityDescription *newObject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    
    // add each entry in the dictionary to the object
    for (id key in values) {
        [newObject setValue:[values valueForKey:key] forKey:key];
    }
    
    if (context != nil) {       // save object to db only if context is not nil
        
        NSError *error;
        [context save:&error];
        
        if (error != nil) {
#ifdef DEBUG
            NSLog(@"The following error occurred while creating new database entry: %@", error);
#endif
        }
        else {
#ifdef DEBUG
            NSLog(@"Database record created successfully.");
#endif
        }

    }
    
    return newObject;
}

- (void)saveObject {
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    [self saveObjectInContext:context];
}

- (void)saveObjectInContext:(NSManagedObjectContext *)context {
    if (context == nil) {
        return;
    }
    
    NSError *error;
    [context save:&error];
    
    if (error != nil) {
#ifdef DEBUG
        NSLog(@"The following error occurred while saving the context: %@", error);
#endif
    }
    else {
#ifdef DEBUG
        NSLog(@"Database record saved successfully.");
#endif
    }
}

- (void)deleteObject {
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    [self deleteObjectInContext:context];
}

- (void)deleteObjectInContext:(NSManagedObjectContext *)context {
    if (context == nil) {
        return;
    }
    
    [context deleteObject:self];
    
    NSError *error;
    [context save:&error];
    
    if (error != nil) {
#ifdef DEBUG
        NSLog(@"The following error occurred while deleting the object: %@", error);
#endif
    }
    else {
#ifdef DEBUG
        NSLog(@"Database record deleted successfully.");
#endif
    }
}

+ (void)deleteAllObjects {
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    [self deleteAllObjectsInContext:context];
}

+ (void)deleteAllObjectsInContext:(NSManagedObjectContext *)context {
    if (context == nil) {
        return;
    }
    NSArray* objects = [self findAllObjects];
    for (id obj in objects) {
        [context deleteObject:obj];
    }
    
    NSError *error;
    [context save:&error];
    
    if (error != nil) {
#ifdef DEBUG
        NSLog(@"The following error occurred while deleting all objects: %@", error);
#endif
    }
    else {
#ifdef DEBUG
        NSLog(@"All database records of %@ class deleted successfully.", [self class]);
#endif
    }
}


- (NSManagedObject *)cloneAttributesInContext:(NSManagedObjectContext *)context {
    
    NSString *entityName = [[self entity] name];
    
    NSManagedObject *cloned;
    
    //create new object in data store
    cloned = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    
    //loop through all attributes and assign then to the clone
    NSDictionary *attributes = [[NSEntityDescription entityForName:entityName inManagedObjectContext:context] attributesByName];
    
    for (NSString *attr in attributes) {
        
        [cloned setValue:[self valueForKey:attr] forKey:attr];
        
    }
    
    return cloned;
}

- (NSManagedObject *)cloneAttributes {
    
    NSManagedObjectContext *context = [(TeamieAppDelegate*)[[UIApplication sharedApplication] delegate] managedObjectContext];
    return [self cloneAttributesInContext:context];
}


@end
