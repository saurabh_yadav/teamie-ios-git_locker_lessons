//
//  TMEWebViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 05/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "SVModalWebViewController.h"

@interface TMEWebViewController : SVModalWebViewController <UIWebViewDelegate>

+ (void)openWebPageViewerWithURL:(NSURL *)URL andResponder:(UIResponder *)responder withTitle:(NSString *)title;
+ (NSURL *)getMergeSessionURL:(NSURL *)URL;

@end
