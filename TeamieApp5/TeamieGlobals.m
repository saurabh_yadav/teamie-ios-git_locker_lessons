//
//  TeamieGlobals.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/5/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TeamieGlobals.h"
#import <MessageUI/MessageUI.h>
#import <CommonCrypto/CommonHMAC.h>
#import <TSMessages/TSMessage.h>

#import "MBProgressHUD.h"
#import "ErrorView.h"
#import "TMEClient.h"
#import "TMEWebViewController.h"

//Constants
#define SECOND 1
#define MINUTE (60 * SECOND)
#define HOUR (60 * MINUTE)
#define DAY (24 * HOUR)
#define MONTH (30 * DAY)

NSString * const GOOGLE_CLIENT_ID = @"70956496195-qoi4taojstba3l445rlcba4vlsp2jrcb.apps.googleusercontent.com";

NSString * const SITE_PERMISSIONS_KEY = @"site_permissions";
NSString * const WHITELIST_TERMS_KEY = @"whitelist_terms";
NSString * const WHITELIST_ORDER_KEY = @"whitelist_order";
NSString * const S3_KEY = @"amazon_s3";
NSString * const SITE_INFO_KEY = @"site_info";


@implementation TeamieGlobals

+ (NSString *)TMELocalize:(NSString *)key comment:(NSString *)comment {
    //Localize
    NSString *localizedString = NSLocalizedString(key, comment);
    NSArray *beforeWhiteListing = [localizedString componentsSeparatedByString:@" "];
    NSMutableArray *afterWhiteListing = [NSMutableArray arrayWithCapacity:beforeWhiteListing.count];
    
    //Replace whitelist terms from site.json
    NSDictionary *whitelistTerms = [[NSUserDefaults standardUserDefaults] objectForKey:WHITELIST_TERMS_KEY];

    for (NSString *word in beforeWhiteListing) {
        [afterWhiteListing addObject:whitelistTerms[word]?:word];
    }
    
    return [afterWhiteListing componentsJoinedByString:@" "];
}

+ (NSString*)storyboardIdentifier {
    
    return [[NSBundle mainBundle].infoDictionary objectForKey:@"UIMainStoryboardFile"];
}

+ (NSString*)stringFromInteger:(NSInteger)number {
    NSString* result = [NSString stringWithFormat:@"%ld", (long)number];
    if (number % 10 == 1 && number != 11) {
        result = [result stringByAppendingString:@"st"];
    }
    else if (number % 10 == 2 && number != 12) {
        result = [result stringByAppendingString:@"nd"];
    }
    else if (number % 10 == 3 && number != 13) {
        result = [result stringByAppendingString:@"rd"];
    }
    else {
        result = [result stringByAppendingString:@"th"];
    }
    return result;
}

+ (NSString*)timeIntervalToText:(NSTimeInterval)seconds {
    // Get the system calendar
    NSCalendar *sysCalendar = [NSCalendar currentCalendar];
    
    // Create the NSDates
    NSDate *date1 = [NSDate date];
    NSDate *date2 = [NSDate dateWithTimeInterval:seconds sinceDate:date1];
    
    // Get conversion to months, days, hours, minutes
    unsigned int unitFlags = NSHourCalendarUnit | NSMinuteCalendarUnit | NSDayCalendarUnit | NSMonthCalendarUnit | NSSecondCalendarUnit;
    
    NSDateComponents *conversionInfo = [sysCalendar components:unitFlags fromDate:date1  toDate:date2  options:0];
    
    NSString* result = @"";
    if ([conversionInfo month] > 0) {
        result = [result stringByAppendingFormat:@"%ld %@", (long)[conversionInfo month], @"month"];
        if ([conversionInfo month] > 1) {
            result = [result stringByAppendingString:@"s "];
        }
        else {
            result = [result stringByAppendingString:@" "];
        }
    }
    
    if ([conversionInfo day] > 0) {
        result = [result stringByAppendingFormat:@"%ld %@", (long)[conversionInfo day], @"day"];
        if ([conversionInfo day] > 1) {
            result = [result stringByAppendingString:@"s "];
        }
        else {
            result = [result stringByAppendingString:@" "];
        }
    }
    
    if ([conversionInfo hour] > 0) {
        result = [result stringByAppendingFormat:@"%ld %@", (long)[conversionInfo hour], @"hour"];
        if ([conversionInfo hour] > 1) {
            result = [result stringByAppendingString:@"s "];
        }
        else {
            result = [result stringByAppendingString:@" "];
        }
    }
    
    if ([conversionInfo minute] > 0) {
        result = [result stringByAppendingFormat:@"%ld %@", (long)[conversionInfo minute], @"minute"];
        if ([conversionInfo minute] > 1) {
            result = [result stringByAppendingString:@"s "];
        }
        else {
            result = [result stringByAppendingString:@" "];
        }
    }
    
    if ([conversionInfo second] > 0) {
        result = [result stringByAppendingFormat:@"%ld %@", (long)[conversionInfo second], @"second"];
        if ([conversionInfo second] > 1) {
            result = [result stringByAppendingString:@"s "];
        }
        else {
            result = [result stringByAppendingString:@" "];
        }
    }
    if ([result length] == 0) {  // If result is empty then return string as 0 seconds.
        result = @"0s";
    }
    return result;
}

+ (NSString*)timeIntervalWithStartTime:(NSTimeInterval)d1 withEndTime:(NSTimeInterval)d2 {
    
    NSDate* date1 = [NSDate dateWithTimeIntervalSince1970:d1];
    NSDate* date2;
    if (d2 == 0) {  // if d2 is zero, then use current timestamp
        date2 = [NSDate date];
    }
    else {
        date2 = [NSDate dateWithTimeIntervalSince1970:d2];
    }
    
    return [TeamieGlobals timeIntervalWithStartDate:date1 withEndDate:date2 brevity:0];
}

+ (NSString *)timeTillDate:(NSDate *)date {
    NSString *reverse = [self timeIntervalWithStartDate:[NSDate date] withEndDate:date brevity:0];
    return [reverse stringByReplacingOccurrencesOfString:@"ago" withString:@"later"];
}

+ (NSString*)timeIntervalWithStartDate:(NSDate*)d1 withEndDate:(NSDate*)d2 brevity:(NSUInteger)brevLevel
{
    if (d2 == nil) {    // if the end date is nil, set it to the current date
        d2 = [NSDate date];
    }
    
    NSArray* longStrings = [NSArray arrayWithObjects:@"a moment ago",
                            @"one second ago", 
                            @"%d seconds ago", 
                            @"a minute ago", 
                            @"%d minutes ago",
                            @"an hour ago",
                            @"%d hours ago",
                            @"yesterday",
                            @"%d days ago",
                            @"one month ago",
                            @"%d months ago",
                            @"one year ago",
                            @"%d years ago", 
                            nil];
    NSArray* mediumStrings = [NSArray arrayWithObjects:@"just now",
                              @"1sec ago", 
                              @"%dsecs ago", 
                              @"1min ago", 
                              @"%dmins ago",
                              @"1hr ago",
                              @"%dhrs ago",
                              @"yesterday",
                              @"%d days ago",
                              @"1mth ago",
                              @"%dmths ago",
                              @"1yr ago",
                              @"%dyrs ago", 
                              nil];

    NSArray* briefStrings = [NSArray arrayWithObjects:@"just now",
                             @"1s ago", 
                             @"%ds ago", 
                             @"1m ago", 
                             @"%dm ago",
                             @"1h ago",
                             @"%dh ago",
                             @"1d ago",
                             @"%dd ago",
                             @"1mth ago",
                             @"%dmth ago",
                             @"1yr ago",
                             @"%dyr ago", 
                             nil];
    
    NSArray* minStrings = [NSArray arrayWithObjects:@"just now",
                           @"1s", 
                           @"%ds", 
                           @"1m", 
                           @"%dm",
                           @"1h",
                           @"%dh",
                           @"1d",
                           @"%dd",
                           @"1mth",
                           @"%dmth",
                           @"1yr",
                           @"%dyr", 
                           nil];
    
    NSArray* str;
    if (brevLevel == 0) {
        str = longStrings;
    }
    else if (brevLevel == 1) {
        str = mediumStrings;
    }
    else if (brevLevel == 2) {
        str = briefStrings;
    }
    else {
        str = minStrings;
    }
    
    //Calculate the delta in seconds between the two dates
    NSTimeInterval delta = [d2 timeIntervalSinceDate:d1];
    
    if (delta < 1 * MINUTE)
    {
        return (delta < 0) ? [str objectAtIndex:0] : (delta == 1 ? [str objectAtIndex:1] : [NSString stringWithFormat:[str objectAtIndex:2], (int)delta]);
    }
    if (delta < 2 * MINUTE)
    {
        return [str objectAtIndex:3];
    }
    if (delta < 45 * MINUTE)
    {
        int minutes = floor((double)delta/MINUTE);
        return [NSString stringWithFormat:[str objectAtIndex:4], minutes];
    }
    if (delta < 90 * MINUTE)
    {
        return [str objectAtIndex:5];
    }
    if (delta < 24 * HOUR)
    {
        int hours = floor((double)delta/HOUR);
        return (hours == 1) ? [str objectAtIndex:5] : [NSString stringWithFormat:[str objectAtIndex:6], hours];
    }
    if (delta < 48 * HOUR)
    {
        return [str objectAtIndex:7];
    }
    if (delta < 30 * DAY)
    {
        int days = floor((double)delta/DAY);
        return [NSString stringWithFormat:[str objectAtIndex:8], days];
    }
    if (delta < 12 * MONTH)
    {
        int months = floor((double)delta/MONTH);
        return months <= 1 ? [str objectAtIndex:9] : [NSString stringWithFormat:[str objectAtIndex:10], months];
    }
    else
    {
        int years = floor((double)delta/MONTH/12.0);
        return years <= 1 ? [str objectAtIndex:11] : [NSString stringWithFormat:[str objectAtIndex:12], years];
    }
}

+ (void)addBackButtonToViewController:(UIViewController*)controller withCallback:(SEL)selector {
    UIImage* backButtonImage = [TeamieUIGlobals defaultPicForPurpose:@"back" withSize:17.0 andColor:[UIColor whiteColor]];
    
    UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithImage:backButtonImage style:UIBarButtonItemStyleBordered target:controller action:selector];
        
    for (id viewItem in controller.view.subviews) {
        // if the view controller has a navigation bar that is not hidden
        if ([viewItem isKindOfClass:[UINavigationBar class]] && ((UINavigationBar*)viewItem).isHidden == NO) {
        
            ((UINavigationBar*)viewItem).topItem.leftBarButtonItem = backButton;
             
            return;
        }
    }
    
    // if control reaches here UINavigationbar could not be found inside the viewController's view
    // so look inside it's navigationController
    if (controller.navigationItem) {
        
        controller.navigationItem.leftBarButtonItem = backButton;

    }
    else if (controller.navigationController) {
        controller.navigationController.navigationItem.leftBarButtonItem = backButton;
    }

}

+ (void)addHUDIndeterminateLabel:(NSString *)labelText {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] windows] lastObject] animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = labelText;
    hud.removeFromSuperViewOnHide = YES;
}

+ (void)addHUDTextOnlyLabel:(NSString *)labelText afterDelay:(NSTimeInterval)delay {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] windows] lastObject] animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.labelText = labelText;
    hud.removeFromSuperViewOnHide = YES;
    [hud hide:YES afterDelay:delay];
}

// TSMessage method which adds button callback and dismisses message on tap with endless duration
+ (void)addTSMessageInController:(UIViewController *)viewController title:(NSString *)title message:(NSString *)message buttonCallback:(id)buttonCallback {
    [TSMessage showNotificationInViewController:viewController
                                          title:title
                                       subtitle:message
                                          image:nil
                                           type:TSMessageNotificationTypeMessage
                                       duration:TSMessageNotificationDurationEndless
                                       callback:nil
                                    buttonTitle:@"Attach Link"
                                 buttonCallback:buttonCallback
     // MARK: I think bottom was added to show pasteboard link detection at the bottom, not valid anymore
                                     atPosition:TSMessageNotificationPositionTop
                           canBeDismissedByUser:YES];
}

+ (void)addTSMessageInController:(UIViewController *)viewController title:(NSString *)title message:(NSString *)message type:(NSString *)type duration:(NSTimeInterval)duration withCallback:(id)callback {
    int notificationType = 0;
    
    // 4 types of Messages
    if (type) {
        if ([type isEqualToString:@"message"]) {
            notificationType = TSMessageNotificationTypeMessage;
        }
        if ([type isEqualToString:@"warning"]) {
            notificationType = TSMessageNotificationTypeWarning;
        }
        if ([type isEqualToString:@"error"]) {
            notificationType = TSMessageNotificationTypeError;
        }
        if ([type isEqualToString:@"success"]) {
            notificationType = TSMessageNotificationTypeSuccess;
        }
    }
    
    if (viewController == nil) {
        // In case the view controller to show the message in is not available, then simply use the TSMessage library method
        [TSMessage showNotificationWithTitle:title
                                    subtitle:message
                                        type:notificationType];
    }
    // Don't want message to hide itself or want the user to tap the message and disable it? Set the duration to 0, no callback allowed here
    else if (duration == 0) {
        [TSMessage showNotificationInViewController:viewController
                                              title:title
                                           subtitle:message
                                               type:notificationType];
    }
    else {
        [TSMessage showNotificationInViewController:viewController
                                              title:title
                                           subtitle:message
                                              image:nil
                                               type:notificationType
                                           duration:duration
                                           callback:callback
                                        buttonTitle:nil
                                     buttonCallback:nil
         // MARK: I think bottom was added to show pasteboard link detection at the bottom, not valid anymore
                                         atPosition:TSMessageNotificationPositionTop
                               canBeDismissedByUser:YES];
    }
}

+ (void)addActivityLabelWithStyle:(int)style progress:(BOOL)progress withText:(NSString*)text atPosition:(NSInteger)position {

}

+ (void)dismissActivityLabels { // dismiss all the activity labels
    
    NSArray* windowArray = [[UIApplication sharedApplication] windows];
    
    for (UIView* viewToLookIn in windowArray){
        for (id viewItem in viewToLookIn.subviews) {
            if ([viewItem isKindOfClass:[MBProgressHUD class]]) {
                [viewItem removeFromSuperview];
            }
        }
    }
}

+ (void)dismissTSMessage {
    [TSMessage dismissActiveNotification];
}

+ (UIButton*)createCheckboxButton:(CGRect)frame {
    
    if (CGRectIsNull(frame)) {  // if the frame is not set
        frame = CGRectMake(0, 0, 30, 30);
    }
    
    UIButton* checkBox = [[UIButton alloc] initWithFrame:frame];
    [checkBox setBackgroundImage:[UIImage imageNamed:@"checkbox-unselected.png"] forState:UIControlStateNormal];
    [checkBox setBackgroundImage:[UIImage imageNamed:@"checkbox-selected.png"] forState:UIControlStateSelected];
    [checkBox setBackgroundImage:[UIImage imageNamed:@"checkbox-highlighted.png"] forState:UIControlStateHighlighted];
    
    return checkBox;
}

+ (NSUInteger)extractNumberFromString:(NSString*)originalString {
    
    // Intermediate
    NSString *numberString;
    
    NSScanner *scanner = [NSScanner scannerWithString:originalString];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Throw away characters before the first number.
    [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    
    // Result.
    NSUInteger number = [numberString integerValue];
    
    return number;
}

+ (NSString*)extractNumberFromURL:(NSString*)urlString prefix:(NSString*)urlPrefix {
    
    NSError* error;
    
    NSRegularExpression* regex = [[NSRegularExpression alloc] initWithPattern:[NSString stringWithFormat:@"href=\"[^\"]*?\/%@\/([0-9]+)\"", urlPrefix] options:NSRegularExpressionCaseInsensitive error:&error];
    NSArray* matches = [regex matchesInString:urlString options:0 range:NSMakeRange(0, [urlString length])];
    
    for (NSTextCheckingResult* result in matches) {
        if (result.numberOfRanges >= 1) {
            NSRange numberRange = [result rangeAtIndex:1];
            NSString* str = [urlString substringWithRange:numberRange];

            // check if str is a number
            NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
            [f setLocale:[[NSLocale alloc] initWithLocaleIdentifier: @"en_US"]];
            [f setAllowsFloats:NO];
            if ([f numberFromString:str]) {
                return str;
            }
        }
    }

    return nil;
}

+ (void)performSelectionHighlightAnimation:(UIView*)view {
    
    UIColor* originalColor = view.backgroundColor;
    
    [UIView animateWithDuration:0.2 delay:0.0 options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [view setBackgroundColor:[UIColor colorWithWhite:0.7 alpha:1.0]];
                     }
                     completion:^(BOOL finished){
                         [view setBackgroundColor:originalColor];
                     }];
}

+ (void) moveControl:(id)uiControl forKeyboard:(NSNotification*)aNotification up:(BOOL)up distance:(CGFloat)distance inView:(UIView*)view {
    if (![uiControl respondsToSelector:@selector(frame)]) { // if the uiControl does not hav a frame property, return
        return;
    }
    
    NSDictionary* userInfo = [aNotification userInfo];
    
    // Get animation info from userInfo
    NSTimeInterval animationDuration;
    UIViewAnimationCurve animationCurve;
    
    CGRect keyboardEndFrame;
    
    [[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
    [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
    
    
    [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
    
    
    // Animate up or down
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = [uiControl frame];
    CGRect keyboardFrame = [view convertRect:keyboardEndFrame toView:nil];
    
    if (distance == 0) {
        distance = keyboardFrame.size.height;
    }
    
    newFrame.origin.y -= distance * (up? 1 : -1);
    [uiControl setFrame:newFrame];
    
    [UIView commitAnimations];
}

+ (void)moveBottomChildOfView:(UIView *)view withBottomConstraint:(NSLayoutConstraint *)bottomConstraint andKeyboardState:(BOOL)showKeyboard andDefaultConstant:(CGFloat)defaultConstant andKeyboardInfo:(NSDictionary *)info{

    // transform the UIViewAnimationCurve to a UIViewAnimationOptions mask
    UIViewAnimationCurve animationCurve = [info[UIKeyboardAnimationCurveUserInfoKey] unsignedIntegerValue];
    UIViewAnimationOptions animationOptions = UIViewAnimationOptionBeginFromCurrentState;
    if (animationCurve == UIViewAnimationCurveEaseIn) {
        animationOptions |= UIViewAnimationOptionCurveEaseIn;
    }
    else if (animationCurve == UIViewAnimationCurveEaseInOut) {
        animationOptions |= UIViewAnimationOptionCurveEaseInOut;
    }
    else if (animationCurve == UIViewAnimationCurveEaseOut) {
        animationOptions |= UIViewAnimationOptionCurveEaseOut;
    }
    else if (animationCurve == UIViewAnimationCurveLinear) {
        animationOptions |= UIViewAnimationOptionCurveLinear;
    }

    NSValue *keyboardFrameVal = [info objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardFrame = [keyboardFrameVal CGRectValue];

    [view setNeedsUpdateConstraints];
    
    if (showKeyboard){
        bottomConstraint.constant = defaultConstant + keyboardFrame.size.height;
    }
    else{
        bottomConstraint.constant = defaultConstant;
    }
    
    NSTimeInterval animationDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:animationDuration delay:0 options:animationOptions animations:^{
        [view layoutIfNeeded];
    } completion:nil];
}

+(NSDictionary*)instituteDomainMappings {
    return [NSDictionary dictionaryWithObjectsAndKeys:@"https://ascend.theteamie.com/api", @"Ascend Education Centre",
                @"https://leb.theteamie.com/api", @"London Examination Board",
                @"https://src.theteamie.com/api", @"Stamford Raffles College",
                @"https://olympia.theteamie.com/api", @"Olympia College",
                @"https://instant.theteamie.com/api", @"Teamie Instant Classroom",
                @"https://demoasia.theteamie.com/api", @"Demo Asia",
                nil];
}

+ (NSString*)encodeStringToXHTML:(NSString*)stringToEncode {
    return [[[stringToEncode stringByReplacingOccurrencesOfString:@"&" withString:@"&amp;"] stringByReplacingOccurrencesOfString:@"\r\n" withString:@"<br/>"] stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"];
}

+ (void)showMailComposer:(NSDictionary *)params fromController:(id)controller {
    
    if (![MFMailComposeViewController canSendMail]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:TMELocalize(@"error.failure")
                                                        message:TMELocalize(@"error.composer-unsupported")
                                                       delegate:nil
                                              cancelButtonTitle:TMELocalize(@"button.ok")
                                              otherButtonTitles: nil];
        [alert show];
        return;
    }
    
    MFMailComposeViewController *mailer = [[MFMailComposeViewController alloc] init];
    mailer.mailComposeDelegate = controller;
    mailer.navigationBar.tintColor = [UIColor colorWithHex:@"#BE202E" alpha:1.0];
    
    if ([[params valueForKey:@"subject"] isKindOfClass:[NSString class]]) {
        [mailer setSubject:[params valueForKey:@"subject"]];
    }
    if ([[params valueForKey:@"to"] isKindOfClass:[NSArray class]]) {
        [mailer setToRecipients:[params valueForKey:@"to"]];
    }
    if ([[params valueForKey:@"body"] isKindOfClass:[NSString class]]) {
        [mailer setMessageBody:[params valueForKey:@"body"] isHTML:NO];
    }
    if ([[params valueForKey:@"cc"] isKindOfClass:[NSArray class]]) {
        [mailer setToRecipients:[params valueForKey:@"cc"]];
    }
    if ([[params valueForKey:@"bcc"] isKindOfClass:[NSArray class]]) {
        [mailer setToRecipients:[params valueForKey:@"bcc"]];
    }
    if ([[params valueForKey:@"attachments"] isKindOfClass:[NSArray class]]) {
        for (NSDictionary* fileInfo in [params valueForKey:@"attachments"]) {
            if ([fileInfo valueForKey:@"filename"] && [fileInfo valueForKey:@"mimetype"] && [fileInfo valueForKey:@"filepath"]) {
                NSData *myData = [NSData dataWithContentsOfFile:[fileInfo valueForKey:@"filepath"]];
                [mailer addAttachmentData:myData mimeType:[fileInfo valueForKey:@"mimetype"] fileName:[fileInfo valueForKey:@"filename"]];
            }
        }
    }
    
    [controller presentModalViewController:mailer animated:YES];
}

+ (UIFont*)appFontFor:(NSString *)purpose {
    if ([purpose isEqualToString:@"slogan"]) {
        return [UIFont fontWithName:@"OpenSans-Light" size:21.0];
    }
    else if ([purpose isEqualToString:@"subslogan"]) {
        return [UIFont fontWithName:@"OpenSans-Light" size:14.0];
    }
    else if ([purpose isEqualToString:@"inputText"]) {
        return [UIFont fontWithName:@"OpenSans-Light" size:12.0];
    }
    else if ([purpose isEqualToString:@"actionButton"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:14.0];
    }
    else if ([purpose isEqualToString:@"actionButtonText"]) {
        return [UIFont fontWithName:@"OpenSans" size:14.0];
    }
    else if ([purpose isEqualToString:@"navigationTitle"]) {
        return [UIFont fontWithName:@"OpenSans" size:18.0];
    }
    else if ([purpose isEqualToString:@"textLink"]) {
        return [UIFont fontWithName:@"OpenSans" size:10.0];
    }
    else if ([purpose isEqualToString:@"sidebarMenuTitle"]) {
        return [UIFont fontWithName:@"OpenSans" size:12.0];
    }
    else if ([purpose isEqualToString:@"sidebarMenuBadge"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:12.0];
    }
    else if ([purpose isEqualToString:@"tableviewSectionHeader"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:13.0];
    }
    else if ([purpose isEqualToString:@"thoughtBody"]){
        return [UIFont fontWithName:@"OpenSans-Light" size:14.0];
    }
    else if ([purpose isEqualToString:@"thoughtBodyBold"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:14.0];
    }
    else if ([purpose  isEqualToString:@"AppFontForPurposeCellTitleText"]) {
        return [UIFont fontWithName:@"OpenSans-Regular" size:15.0];
    }
    else if ([purpose isEqualToString:@"AppFontForPurposeCellSubtitleText" ]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:12.5];
    }
    else if ([purpose  isEqualToString: @"AppFontForPurposeCellLabelText"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:10.0];
    }
    else if ([purpose isEqualToString:@"AppFontForPurposeCellDetailLabelText"]) {
        return [UIFont fontWithName:@"OpenSans" size:10.5];
    }
    else if ([purpose isEqualToString:@"AppFontForPurposeSupplementaryViewText"]) {
        return [UIFont fontWithName:@"OpenSans" size:14];
    }
    else if ([purpose isEqualToString:@"notificationText"]) {
        return [UIFont fontWithName:@"OpenSans" size:12.0];
    }
    else if ([purpose isEqualToString:@"notificationTextHighlight"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:12.0];
    }
    else if ([purpose isEqualToString:@"notificationsBadgeText"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:11.0];
    }
    else if ([purpose isEqualToString:@"userProfileSectionTitleText"]) {
        return [UIFont fontWithName:@"OpenSans" size:14.0];
    }
    else if ([purpose isEqualToString:@"userProfileSectionContentText"]) {
        return [UIFont fontWithName:@"OpenSans" size:12.0];
    }
    else if ([purpose isEqualToString:@"userProfileBadgeTitleText"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:11.0];
    }
    else if ([purpose isEqualToString:@"postTitle"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:14.0];
    }
    else if ([purpose isEqualToString:@"postSubtitle"]) {
        return [UIFont fontWithName:@"OpenSans-Light" size:11.0];
    }
    else if ([purpose isEqualToString:@"postSubtitleClassroom"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:11.0];
    }
    else if ([purpose isEqualToString:@"postSubtitleTimestamp"]) {
        return [UIFont fontWithName:@"OpenSans" size:11.0];
    }
    else if ([purpose isEqualToString:@"postBody"]) {
        return [UIFont fontWithName:@"OpenSans" size:13.0];
    }
    else if ([purpose isEqualToString:@"deadlineText"]) {
        return [UIFont fontWithName:@"OpenSans" size:11.5];
    }
    else if ([purpose isEqualToString:@"allDayText"]) {
        return [UIFont fontWithName:@"OpenSans" size:12.5];
    }
    else if ([purpose isEqualToString:@"postAttachmentTitle"]) {
        return [UIFont fontWithName:@"OpenSans" size:11.0];
    }
    else if ([purpose isEqualToString:@"postAttachmentSubTitle"]) {
        return [UIFont fontWithName:@"OpenSans-Light" size:10.0];
    }
    else if ([purpose isEqualToString:@"eventTitle"]) {
        return [UIFont fontWithName:@"OpenSans" size:13.0];
    }
    else if ([purpose isEqualToString:@"eventDetails"]) {
        return [UIFont fontWithName:@"OpenSans" size:11.0];
    }
    else if ([purpose isEqualToString:@"eventTypeLabel"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:10.0];
    }
    else if ([purpose isEqualToString:@"eventDate"]) {
        return [UIFont fontWithName:@"OpenSans" size:14.0];
    }
    else if ([purpose isEqualToString:@"eventTime"]) {
        return [UIFont fontWithName:@"OpenSans" size:12.0];
    }
    else if ([purpose isEqualToString:@"remainingStringFont"]){
        return [UIFont fontWithName:@"OpenSans" size:11.0];
    }
    else if ([purpose isEqualToString:@"tableTitleRegularFont"]){
        return [UIFont fontWithName:@"OpenSans" size:13.0];
    }
    else if ([purpose isEqualToString:@"regularFontWithSize11"]){
        return [UIFont fontWithName:@"OpenSans" size:11.0];
    }
    else if ([purpose isEqualToString:@"regularFontWithSize9"]){
        return [UIFont fontWithName:@"OpenSans" size:9.0];
    }
    else if ([purpose isEqualToString:@"regularFontWithSize11"]){
        return [UIFont fontWithName:@"OpenSans" size:11.0];
    }
    else if ([purpose isEqualToString:@"boldFontWithSize18"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:18.0];
    }
    else if ([purpose isEqualToString:@"commentTitle"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:12.0];
    }
    else if ([purpose isEqualToString:@"commentSubtitle"]){
        return [UIFont fontWithName:@"OpenSans" size:11.0];
    }
    else if ([purpose isEqualToString:@"commentBody"]){
        return [UIFont fontWithName:@"OpenSans" size:12.0];
    }
    else if ([purpose isEqualToString:@"commentAction"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:11.0];
    }
    else if ([purpose isEqualToString:@"replyTitle"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:10.5];
    }
    else if ([purpose isEqualToString:@"replySubtitle"]){
        return [UIFont fontWithName:@"OpenSans" size:9.5];
    }
    else if ([purpose isEqualToString:@"replyBody"]){
        return [UIFont fontWithName:@"OpenSans" size:10.5];
    }
    else if ([purpose isEqualToString:@"actionText"]) {
        return [UIFont fontWithName:@"OpenSans" size:11.5];
    }
    else if ([purpose isEqualToString:@"postNewComment"]) {
        return [UIFont fontWithName:@"OpenSans" size:12.5];
    }
    else if ([purpose isEqualToString:@"emptyListTitleText"]) {
        return [UIFont fontWithName:@"OpenSans-Bold" size:16];
    }
    else if ([purpose isEqualToString:@"emptyListSubtitleText"]) {
        return [UIFont fontWithName:@"OpenSans" size:14];
    }
    else if ([purpose isEqualToString:@"regularFontWithSize17"]){
        return [UIFont fontWithName:@"OpenSans" size:17.0];
    }
    else if ([purpose isEqualToString:@"regularFontWithSize14"]){
        return [UIFont fontWithName:@"OpenSans" size:14.0];
    }
    else if ([purpose isEqualToString:@"boldFontWithSize17"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:17.0];
    }
    else if ([purpose isEqualToString:@"boldFontWithSize11"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:11.0];
    }
    else if ([purpose isEqualToString:@"regularFontWithSize18"]){
        return [UIFont fontWithName:@"OpenSans" size:18.0];
    }
    else if ([purpose isEqualToString:@"boldFontWithSize10"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:10];
    }
    else if ([purpose isEqualToString:@"regularFontWithSize13"]){
        return [UIFont fontWithName:@"OpenSans" size:13.0];
    }
    else if ([purpose isEqualToString:@"boldFontWithSize13"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:13.0];
    }
    else if ([purpose isEqualToString:@"regularFontWithSize16"]){
        return [UIFont fontWithName:@"OpenSans" size:16.0];
    }
    else if ([purpose isEqualToString:@"boldFontWithSize12"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:12.0];
    }
    else if ([purpose isEqualToString:@"newPostLabel"]){
        return [UIFont fontWithName:@"OpenSans-Bold" size:10];
    }
    else {
        return [UIFont systemFontOfSize:14];
    }
}
//add these fonts in the end of app for purpose method

// Creates a file with the given data. The structure of the data varies with the type of file being created.
// Eg: For a CSV file, data would be an array of strings or numbers

/* The function returns an NSDictionary on success and nil on failure. The NSDictionary contains the following keys:
        - filename: The name of the file that was creating (without file extensions like .xls, .csv etc.)
        - filepath: The absolute path to the file
        - mimetype: The mime type for the file should the need arise to attach it to an email
    These dictionary keys are in sync with what is required by the showMailComposer:fromController: method. If the dictionary returned by this method is passed to it, then the file gets attached to in the mail composer.
 */
+ (NSDictionary*)createFileOfType:(NSString *)fileType withData:(id)data andName:(NSString*)filename {
    if ([fileType isEqualToString:@"csv"]) {
        if ([data isKindOfClass:[NSArray class]]) {
            
            NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *fileFullname = [NSString stringWithFormat:@"%@.csv", filename];
            NSString *filePathLib = [NSString stringWithFormat:@"%@", [docDir stringByAppendingPathComponent:fileFullname]];
            
            [[data componentsJoinedByString:@","] writeToFile:filePathLib atomically:YES encoding:NSUTF8StringEncoding error:NULL];
            
            return [NSDictionary dictionaryWithObjectsAndKeys:fileFullname, @"filename", @"text/csv", @"mimetype", filePathLib, @"filepath", nil];
        }
    }
    
    return nil;
}

+ (void)showErrorViewOnView:(UIView*)superview withFrame:(CGRect)frame withTitle:(NSString*)title subtitle:(NSString*)subtitle image:(UIImage*)image {
    if (title.length || subtitle.length || image) {
        ErrorView* errorView = [[ErrorView alloc] initWithFrame:frame
                                                          title:title
                                                       subtitle:subtitle
                                                          image:image];
        
        errorView.backgroundColor = superview.backgroundColor;
        
        [superview addSubview:errorView];
        [superview bringSubviewToFront:errorView];
    }
}

+ (void)dismissErrorViewsOnView:(UIView*)superview {
     for (UIView* view in superview.subviews){
        if ([view isKindOfClass:[ErrorView class]]){
            [view removeFromSuperview];
        }
    }
}

+ (BOOL)hasFourInchDisplay {
    return ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && [UIScreen mainScreen].bounds.size.height == 568.0);
}

+ (void)cancelTeamieRestRequests {
//TODO: Nalin
}

//Responder should be either a UIViewController (or it's subclasses) or somewhere in the subview heirarchy of one
+ (void)openInappBrowserWithURL:(NSURL *)URL fromResponder:(UIResponder *)responder {
    [self openInappBrowserWithURL:URL fromResponder:responder withTitle:nil];
}

+ (void)openInappBrowserWithURL:(NSURL *)URL fromResponder:(UIResponder *)responder withTitle:(NSString *)title {
    [TMEWebViewController openWebPageViewerWithURL:URL andResponder:responder withTitle:title];
}

//Identical to drupal_hmac_base64
+ (NSString *)encryptHmacSHA256:(NSString *)input key:(NSString *)key {
    uint8_t digest[CC_SHA256_DIGEST_LENGTH];
    CCHmac(kCCHmacAlgSHA256, key.UTF8String, key.length, input.UTF8String, input.length, digest);

    NSData *hashData = [NSData dataWithBytes:digest length:CC_SHA256_DIGEST_LENGTH];
    NSString *hash = [[hashData base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn] mutableCopy];
    hash = [hash stringByReplacingOccurrencesOfString:@"+" withString:@"-"];
    hash = [hash stringByReplacingOccurrencesOfString:@"/" withString:@"_"];
    hash = [hash stringByReplacingOccurrencesOfString:@"=" withString:@""];
    return hash;
}

//MD5 string to string
+ (NSString *)md5:(NSString *)input {
    const char *cStr = [input UTF8String];
    unsigned char digest[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest); // This is the md5 call
    
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    return  output;
}

+ (NSDictionary *)prepareLoginParametersWithEmail:(NSString *)email site:(NSString *)site {
    NSString *timestampInMilliseconds = [NSString stringWithFormat:@"%lld",
                                         (long long)(1000 * [[NSDate date] timeIntervalSince1970])];
    NSString *hashInput = [NSString stringWithFormat:@"%@|%@", email, timestampInMilliseconds];
    NSString *hash = [TeamieGlobals encryptHmacSHA256:hashInput key:TEAMIE_DOMAIN_LIST_KEY];
    
    return @{@"institute": site,
             @"grant_type": TEAMIE_OAUTH_SSO_GRANT_TYPE,
             @"email": email,
             @"timestamp": timestampInMilliseconds,
             @"hash": hash};
}

@end
