//
//  TMEPostAttachment.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 5/15/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEPost.h"

@interface TMEPostAttachment : TMEModel

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSURL *href;
@property (nonatomic, readonly) NSString *attachmentType;

- (UIImage *)previewImage;
- (NSString *)displaySubTitle;

@end
