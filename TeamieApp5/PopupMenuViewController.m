//
//  PopupMenuViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 2/14/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "PopupMenuViewController.h"

@interface PopupMenuViewController()

@property (nonatomic) CellStyle cellStyle;
@property (nonatomic, strong) NSIndexPath* selectedIndexPath;

@property (nonatomic, assign) NSInteger maxMenuItems;

@end

@implementation PopupMenuViewController

@synthesize menuDelegate;
@synthesize maxMenuItems = _maxMenuItems;

#pragma mark - View lifecycle
- (id)initWithTableStyle:(UITableViewStyle)tableStyle cellStyle:(CellStyle)cellStyle selectedRowIndex:(NSNumber*)selectedRowIndex{
    self = [super initWithStyle:tableStyle];
    
    if (self){
        self.contentSizeForViewInPopover = CGSizeMake(175, 130);
        self.tableView.scrollEnabled = NO;
        self.tableView.bounces = NO;
        
        self.cellStyle = cellStyle;
        if (selectedRowIndex){
            self.selectedIndexPath = [NSIndexPath indexPathForItem:[selectedRowIndex integerValue] inSection:0];
        }
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewStyle)style {
    return [self initWithTableStyle:style cellStyle:kCellStylePlain selectedRowIndex:nil];
}

#pragma mark - UITableview delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.entries count];
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"Cell";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                       reuseIdentifier:cellIdentifier];
    }
    
    switch (self.cellStyle) {
        case kCellStyleCheckmark:
            [self setupCheckmarkStyleCell:cell atIndexPath:indexPath];
            break;
        case kCellStylePlain:
            break;
        default:
            break;
    }
    
    cell.textLabel.text = [self.entries objectAtIndex:indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedIndexPath = indexPath;
    [self.tableView reloadData];
    
    UITableViewCell* cell = [self.tableView cellForRowAtIndexPath:indexPath];
    
    if ([self.menuDelegate respondsToSelector:@selector(menuDidExitWithData:)]){
        [self.menuDelegate performSelector:@selector(menuDidExitWithData:) withObject:cell.textLabel.text];
    }
}

- (void)setupCheckmarkStyleCell:(UITableViewCell*)cell atIndexPath:(NSIndexPath*)indexPath{
    if ([indexPath compare:self.selectedIndexPath] == NSOrderedSame)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
}

#pragma mark - public methods
- (void)setEntries:(NSArray *)entries{
    _entries = entries;
    [self.tableView reloadData];
}

- (void)setNumMenuItems:(NSInteger)maxMenuItems {

    _maxMenuItems = maxMenuItems;
    
    if (_maxMenuItems > 6) {
        self.contentSizeForViewInPopover = CGSizeMake(self.contentSizeForViewInPopover.width, 6 * 42.0 + 4.0);
        self.tableView.scrollEnabled = YES;
    }
    else {
        self.contentSizeForViewInPopover = CGSizeMake(self.contentSizeForViewInPopover.width, maxMenuItems * 42.0 + 4.0);
    }
    
}

@end
