//
//  TMEPollAttachmentCell.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 02/01/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEPollOption.h"
#import "TMEPollAttachment.h"

@interface TMEPollAttachmentCell : UITableViewCell

#pragma mark - Subviews

@property (nonatomic, weak) UIView *pollTitleView;
@property (nonatomic, weak) UILabel *titleLabel;

@property (nonatomic, weak) UIView *voteCountView;
@property (nonatomic, weak) UILabel *voteCountLabel;

- (void)loadPollAttachment:(TMEPollOption *)pollOption withOptions:(TMEPollAttachment *)options;

@end
