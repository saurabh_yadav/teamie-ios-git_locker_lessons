//
//  LessonPageToolBar.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 25/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PublishStatusLabel.h"
#import "UILabelWithPadding.h"

@interface LessonPageToolBar : UIView
@property (weak, nonatomic) IBOutlet UIButton *landingPageButton;
@property (weak, nonatomic) IBOutlet UIButton *enableButton;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;
@property (weak, nonatomic) IBOutlet UILabel *pagesLabel;
@property (weak, nonatomic) IBOutlet UILabelWithPadding *publishStatusLabelLessonPage;

@end
