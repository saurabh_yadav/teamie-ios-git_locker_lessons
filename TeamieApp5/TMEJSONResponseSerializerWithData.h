//
//  TMEJSONResponseSerializerWithData.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 6/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "AFURLResponseSerialization.h"

//Subclassing this so that the error message in the response can be captured
@interface TMEJSONResponseSerializerWithData : AFJSONResponseSerializer

extern NSString * const TMEJSONResponseErrorKey;

@end
