//
//  MessageThread.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/25/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class MessageItem, UserProfile;

@interface MessageThread : NSManagedObject

@property (nonatomic, retain) NSNumber * is_new;
@property (nonatomic, retain) NSNumber * lastUpdated;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSNumber * tid;
@property (nonatomic, retain) NSSet *messageItems;
@property (nonatomic, retain) NSSet *participants;
@end

@interface MessageThread (CoreDataGeneratedAccessors)

- (void)addMessageItemsObject:(MessageItem *)value;
- (void)removeMessageItemsObject:(MessageItem *)value;
- (void)addMessageItems:(NSSet *)values;
- (void)removeMessageItems:(NSSet *)values;

- (void)addParticipantsObject:(UserProfile *)value;
- (void)removeParticipantsObject:(UserProfile *)value;
- (void)addParticipants:(NSSet *)values;
- (void)removeParticipants:(NSSet *)values;

@end
