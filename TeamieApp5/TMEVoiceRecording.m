//
//  TMEVoiceRecording.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 26/03/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEVoiceRecording.h"
#import "TMEPostCreateAudioAttachmentViewController.h"

#import <AVFoundation/AVFoundation.h>

#define WAVE_UPDATE_FREQUENCY   0.01

@interface TMEVoiceRecording () <AVAudioRecorderDelegate> {
    NSTimer *timer;
    TMEPostCreateAudioAttachmentViewController *audioController;
}

@property (nonatomic, retain) AVAudioRecorder * recorder;

@end

@implementation TMEVoiceRecording

#pragma mark - Lifecycle

- (void)dealloc {
    if (self.recorder.isRecording) {
        [self.recorder stop];
    }
    
    self.recorder = nil;
    self.recordPath = nil;
}

#pragma mark - Public Methods

- (void)startRecordWithPath:(NSString *)path delegate:(id)viewController {
    NSError *error = nil;
    audioController = viewController;
    
    // Creates an audio session
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    [audioSession setCategory :AVAudioSessionCategoryPlayAndRecord error:&error];
    
    if(error){
        NSLog(@"Audio Session: %@ %ld %@", error.domain, (long)error.code, error.userInfo.description);
        return;
    }
    
    // Activates the app audio session. This ensures that other app sessions are dismissed
    error = nil;
    [audioSession setActive:YES error:&error];
    
    if(error) {
        NSLog(@"Audio Session: %@ %ld %@", error.domain, (long)error.code, error.userInfo.description);
        return;
    }
    
    // Record Settings
    // Encoder: MPED Layer-4 AAC
    // Sample Rate: 16000 Hz
    // Channels: 1 (Mono, to save space)
    NSMutableDictionary * recordSetting = [NSMutableDictionary dictionary];
    recordSetting[AVFormatIDKey]         = @(kAudioFormatMPEG4AAC);
    recordSetting[AVSampleRateKey]       = @16000.0;
    recordSetting[AVNumberOfChannelsKey] = @1;
    
    self.recordPath = path;
    NSURL *url = [NSURL fileURLWithPath:self.recordPath];
    
    error = nil;
    
    NSData *audioData = [NSData dataWithContentsOfFile:url.path options:0 error:&error];
    
    if(audioData) {
        NSFileManager *manager = [NSFileManager defaultManager];
        [manager removeItemAtPath:[url path] error:&error];
    }
    
    error = nil;
    
    if(self.recorder) {
        [self.recorder stop];
        self.recorder = nil;
    }
    
    // Initialize the Recorder
    self.recorder = [[AVAudioRecorder alloc] initWithURL:url settings:recordSetting error:&error];
    
    if(!self.recorder) {
        NSLog(@"Recorder: %@ %ld %@", error.domain, (long)error.code, error.userInfo.description);
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    self.recorder.delegate = self;
    [self.recorder prepareToRecord];            // Creates a file and prepares to record
    self.recorder.meteringEnabled = YES;        // We want to animate the mic so we are enabling Metering. Metering is Processor intensive task
    
    [_recorder recordForDuration:(NSTimeInterval) 7200];
    
    self.recordTime = 0;
    [self resetTimer];
    
    // Timer to update the meters. Microphone recording level animation
    // updateMeters will be called for each update interval (WAVE_UPDATE_FREQUENCY = 0.01 sec)
    timer = [NSTimer scheduledTimerWithTimeInterval:WAVE_UPDATE_FREQUENCY target:self selector:@selector(updateMeters) userInfo:nil repeats:YES];
}

- (void)stopRecordWithCompletionBlock:(void (^)())completion {
    dispatch_async(dispatch_get_main_queue(),completion);
    
    [self resetTimer];
    [self.recorder stop];
}

#pragma mark - Timer Update

- (void)updateMeters {
    self.recordTime += WAVE_UPDATE_FREQUENCY;
    
    if (audioController) {
        if (self.recorder) {
            [self.recorder updateMeters];
        }
        // Peak Power is the average power in a channel being received from the hardware in decibels
        float peakPower = [_recorder averagePowerForChannel:0];
        double ALPHA = 0.05;
        double peakPowerForChannel = pow(20, (ALPHA * peakPower));  // This gives a nice value for animation
        
        // Setting the progress value in the parent
        [audioController setProgress:peakPowerForChannel];
    }
}

#pragma mark - Helper Function

- (void)resetTimer {
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}

- (void)cancelRecording {
    if (self.recorder.isRecording) {
        [self.recorder stop];
    }
    
    self.recorder = nil;
}

- (void)cancelled {
    [self resetTimer];
    [self cancelRecording];
}

@end
