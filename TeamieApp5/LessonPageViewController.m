 //
//  LessonPageViewController.m
//  TeamieApp5
//
//  Created by Raunak on 23/5/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "LessonPageViewController.h"
#import "LessonContentViewController.h"
#import "TeamieWebViewController.h"
#import "TMELessonPageShort.h"
#import "NSString+FontAwesome.h"
#import "TMEClient.h"
#import "UILabelWithPadding.h"
#import "TMELesson.h"

#import "Masonry.h"

#define ToolBarHeight 44
@interface LessonPageViewController ()

@property (nonatomic,strong) NSDictionary *access;
@property TMELesson *lesson;

@property (nonatomic,strong) UIButton* enableButton;
@property (nonatomic,strong) UIButton* deleteButton;
@property (nonatomic,strong) UILabelWithPadding* publishedStatusLabel;

@property (nonatomic,strong) NSArray* lessonMapping;
@property (nonatomic,strong) NSMutableDictionary* lessonControllersMapping;


@end

@implementation LessonPageViewController

@synthesize pageController;
@synthesize currentPage;
@synthesize lessonsList;
@synthesize lessonMapping;
@synthesize lessonControllersMapping;
@synthesize attachmentButton;
@synthesize quizButton;

#pragma mark - View Lifecycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSBundle mainBundle] loadNibNamed:@"LessonPageToolBar" owner:self options:nil];

    //add properties for keeping strong reference for buttons and labels on the toolbar
    self.enableButton = self.toolBar.enableButton;
    self.deleteButton = self.toolBar.deleteButton;
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:TMELocalize(@"menu.contents") style:UIBarButtonItemStylePlain target:self action:@selector(contentButtonPressed:)];
    [self.navigationItem setHidesBackButton:YES];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:TMELocalize(@"menu.lessons") style:UIBarButtonItemStylePlain target:self action:@selector(lessonButtonPressed:)];

	// Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self addChildViewController:self.pageController];
    [self.view addSubview:self.pageController.view];
    [self.pageController didMoveToParentViewController:self];

//    UIBarButtonItem* flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
//    [self.toolbar bringSubviewToFront:self.attachmentButton];
//    self.attachmentButton.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
//    [TMEAnalyticsController trackScreen:@"/lesson-page/*"];

    LessonContentViewController *initialViewController;
    if ([self.lessonControllersMapping objectForKey:[NSNumber numberWithInteger:self.currentPage]] == nil) {
        initialViewController = [[LessonContentViewController alloc] initWithLessonID:[self.lessonMapping objectAtIndex:self.currentPage]];
        initialViewController.delegate = self;
        [self.lessonControllersMapping setObject:initialViewController forKey:[NSNumber numberWithInteger:self.currentPage]];
    }
    else {
        initialViewController = [self.lessonControllersMapping objectForKey:[NSNumber numberWithInteger:self.currentPage]];
        initialViewController.delegate = self;
    }

    NSArray *viewControllers = [NSArray arrayWithObject:initialViewController];
    [self.pageController setViewControllers:viewControllers
                                  direction:UIPageViewControllerNavigationDirectionForward
                                   animated:NO
                                 completion:nil];
    
    [self configureToolBar];

    [self setPageNumberLabelDisplay];
    
    [self configureLandPageButton];
    
    [self configureDeleteButton];
    
    [self configureEnableButton];
    
    [self configurePublishStatusLabel];
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [((LessonContentViewController*)[self.pageController.viewControllers objectAtIndex:0]).view removeFromSuperview];
    [self.navigationController.navigationBar setHidden:NO];
    [super viewDidDisappear:animated];
}

#pragma mark - Special Methods

- (void)configureLandPageButton
{
    self.toolBar.landingPageButton.titleLabel.font = [UIFont fontWithName:kFontAwesomeFamilyName size:15];
    NSString *title = [NSString fontAwesomeIconStringForIconIdentifier:@"icon-book"];

    [self.toolBar.landingPageButton setTitle:title forState:UIControlStateNormal];
    [self.toolBar.landingPageButton setTintColor:[UIColor whiteColor]];
    [self.toolBar.landingPageButton addTarget:self action:@selector(backToLandingPage) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configureEnableButton
{
    self.toolBar.enableButton.titleLabel.font = [UIFont fontWithName:@"FontAwesome" size:15];
    NSString *title = [NSString fontAwesomeIconStringForIconIdentifier:@"icon-ok"];
    [self.toolBar.enableButton setTitle:title forState:UIControlStateNormal];
    [self.toolBar.enableButton setTintColor:[UIColor colorWithHex:@"#41A389" alpha:1.0]];
    [self.toolBar.enableButton addTarget:self action:@selector(toggleStatus) forControlEvents:UIControlEventTouchUpInside];
    
    self.toolBar.enableButton.adjustsImageWhenHighlighted = NO;
    
}

- (void)configureDeleteButton
{
    self.toolBar.deleteButton.titleLabel.font = [UIFont fontWithName:@"FontAwesome" size:15];
    NSString *title = [NSString fontAwesomeIconStringForIconIdentifier:@"icon-remove"];
    [self.toolBar.deleteButton setTitle:title forState:UIControlStateNormal];
    [self.toolBar.deleteButton setTintColor:[UIColor colorWithHex:@"#FA726C" alpha:1.0]];
    [self.toolBar.deleteButton addTarget:self action:@selector(deletePage) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configurePublishStatusLabel
{
    NSString* publishStatus;
    publishStatus =[NSString stringWithFormat:@"%lld",[[[self.lessonsList objectAtIndex:self.currentPage] valueForKey:@"status"] longLongValue]];
    if([publishStatus boolValue] == YES){
        [self.toolBar.publishStatusLabelLessonPage configurePublishStatusLabelWithStatus:PublishStatusPublished];
    }
    else
        [self.toolBar.publishStatusLabelLessonPage configurePublishStatusLabelWithStatus:PublishStatusDrafted];
}

- (void)setPageNumberLabelDisplay {
    [self configurePublishStatusLabel];
    UIFont *font = [TeamieGlobals appFontFor:@"tableTitleRegularFont"];
    NSString *text = [NSString stringWithFormat:@"%ld / %lu",self.currentPage + 1,(unsigned long)[self.lessonsList count]];
    
    CGSize expectedTextSize = [text sizeWithFont:font];
    
    CGSize expectedSize = CGSizeMake(expectedTextSize.width + 2 * 5, expectedTextSize.height);
    //Step 1, keep the origin, change the frame size to expected size
    CGRect frame = self.toolBar.pagesLabel.frame;
    CGPoint oldOrigin = frame.origin;
    CGFloat oldFrameWidth = frame.size.width;
    CGFloat centerY = self.toolBar.pagesLabel.center.y;
    frame.size = expectedSize;
    
    //Step 2, find the width offset
    CGFloat newFrameWidth = frame.size.width;
    CGFloat widthOffset = newFrameWidth - oldFrameWidth;
    
    //Step 3,move the origin left with the same width offset.
    CGPoint newOrigin = CGPointMake(oldOrigin.x - widthOffset, oldOrigin.y);
    frame.origin = newOrigin;
    
    //Step 4, configure the UIlabel
    self.toolBar.pagesLabel.font = font;
    self.toolBar.pagesLabel.text = text;
    self.toolBar.pagesLabel.textColor = [UIColor whiteColor];
    
    self.toolBar.pagesLabel.frame = frame;
    self.toolBar.pagesLabel.textAlignment = NSTextAlignmentCenter;
    
    //Step 5,move the center back to the middle area
    self.toolBar.pagesLabel.center = CGPointMake(self.toolBar.pagesLabel.center.x, centerY);
}

- (UIViewController*)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    LessonContentViewController *lessonContentViewController;
    if ([((LessonContentViewController*)viewController).lessonID isEqualToNumber:[self.lessonMapping objectAtIndex:0]]) {
        return nil;
    }
    if ([self.lessonControllersMapping objectForKey:[NSNumber numberWithInteger:(self.currentPage - 1)]] != nil) {
        lessonContentViewController = [self.lessonControllersMapping objectForKey:[NSNumber numberWithInteger:(self.currentPage - 1)]];
    }
    else {
        lessonContentViewController = [[LessonContentViewController alloc] initWithLessonID:[self.lessonMapping objectAtIndex:(self.currentPage - 1)]];
        lessonContentViewController.delegate = self;
    }
    
    return lessonContentViewController;
}

- (UIViewController*)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    [self configurePublishStatusLabel];
    LessonContentViewController *lessonContentViewController;
    if (self.currentPage == [self.lessonMapping count] - 1) {
        return nil;
    }
    if ([self.lessonControllersMapping objectForKey:[NSNumber numberWithInteger:(self.currentPage + 1)]] != nil) {
        lessonContentViewController = [self.lessonControllersMapping objectForKey:[NSNumber numberWithInteger:(self.currentPage + 1)]];
    }
    else {
        lessonContentViewController = [[LessonContentViewController alloc] initWithLessonID:[self.lessonMapping objectAtIndex:(self.currentPage + 1)]];
        lessonContentViewController.delegate = self;
    }
    
    [lessonContentViewController updateToolbar];
    
    return lessonContentViewController;
}

- (void) pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed {
    [self configurePublishStatusLabel];
    if (completed) {
        self.currentPage = [self.lessonMapping indexOfObject:((LessonContentViewController*)[self.pageController.viewControllers objectAtIndex:0]).lessonID];
        [self setPageNumberLabelDisplay];
        
        if ([self.lessonControllersMapping objectForKey:[NSNumber numberWithInteger:self.currentPage]] == nil) {
            [self.lessonControllersMapping setObject:[self.pageController.viewControllers objectAtIndex:0] forKey:[NSNumber numberWithInteger:self.currentPage]];
        }
    }
}

#pragma mark - Button Tap Callbacks

- (void)contentButtonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)lessonButtonPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)attachmentButtonPressed:(id)sender event:(UIEvent*)event {
//    [((LessonContentViewController*)[self.pageController.viewControllers objectAtIndex:0]) displayAttachmentsForEvent:event];
}

- (void)quizButtonPressed:(id)sender {
//    NSString* entityUrl = nil;
//    NSString* baseUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"baseURL"];
//    entityUrl = [NSString stringWithFormat:@"%@/merge-session/?access_token=%@&redirect_url=%@", TEAMIE_DEFAULT_OAUTH_REST_URL, [[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"], [NSString stringWithFormat:@"node/%qi", [((LessonContentViewController*)[self.pageController.viewControllers objectAtIndex:0]).lessonID longLongValue]]];
    /*TeamieWebViewController * webController = [[TeamieWebViewController alloc] init];
    [webController openURL:[NSURL URLWithString:entityUrl]];
    webController.modalPresentationStyle = UIModalPresentationFullScreen;
    webController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webController];
    [self presentViewController:navigationController animated:YES completion:nil];*/
}

#pragma mark - Lazy Instantiation

- (NSMutableDictionary*)lessonControllersMapping {
    if (!lessonControllersMapping) {
        lessonControllersMapping = [NSMutableDictionary dictionary];
    }
    return lessonControllersMapping;
}

- (NSArray*)lessonMapping {
    if (!lessonMapping) {
        NSMutableArray* tempArray = [NSMutableArray array];
        for (TMELessonPageShort* lessonListItem in self.lessonsList) {
            [tempArray addObject:lessonListItem.nid];
        }
        lessonMapping = tempArray;
    }
    return lessonMapping;
}

- (UIPageViewController*)pageController {
    if (!pageController) {
        NSDictionary *options = [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:UIPageViewControllerSpineLocationMin] forKey: UIPageViewControllerOptionSpineLocationKey];
        pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:options];
        
        pageController.dataSource = self;
        pageController.delegate = self;
        
        [pageController.view setFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.view.bounds.size.height-88)];
    }
    return pageController;
}

- (UIBarButtonItem*)attachmentButton {
    if (!attachmentButton) {
        attachmentButton = [[UIBarButtonItem alloc] initWithTitle:@"Attachments" style:UIBarButtonItemStyleDone target:self action:@selector(attachmentButtonPressed:event:)];
    }
    return attachmentButton;
}

- (UIBarButtonItem*)quizButton {
    if (!quizButton) {
        quizButton = [[UIBarButtonItem alloc] initWithTitle:@"Take Quiz" style:UIBarButtonItemStyleDone target:self action:@selector(quizButtonPressed:)];
    }
    return quizButton;
}

#pragma mark - Lesson Content Delegate
- (void)hideToolBar
{

    [UIView animateWithDuration:0.3
                     animations:^(void){
                         CGRect frame = CGRectMake(0, self.view.bounds.size.height, self.view.bounds.size.width, ToolBarHeight);
                         self.toolBar.frame = frame;
                     }
                     completion:nil
     ];
    
}

-(void)showToolBar
{
    [UIView animateWithDuration:0.3
                     animations:^(void){
                         CGRect frame, remain;
                         CGRectDivide(self.view.bounds, &frame, &remain, ToolBarHeight, CGRectMaxYEdge);
                         self.toolBar.frame = frame;
                     }
                     completion:nil];
    
}

- (void)backToLandingPage
{
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)configureToolBar
{

    [self.view addSubview:self.toolBar];
    CGRect frame, remain;
    CGRectDivide(self.view.bounds, &frame, &remain, ToolBarHeight, CGRectMaxYEdge);
    self.toolBar.frame = frame;
    
    self.toolBar.backgroundColor = [TeamieUIGlobalColors navigationBarBackgroundColor];
}

- (void)toggleStatus
{
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    TMELessonPageShort *lessonPage = [self.lessonsList objectAtIndex:self.currentPage];
    [params setValue:[NSString stringWithFormat:@"%qi", [lessonPage.nid longLongValue]] forKey:@"lesson_id"];
    
    [[TMEClient sharedClient]
     request:TeamieUserLessonToggleStatusRequest
     parameters:params
     loadingMessage:TMELocalize(@"message.loading")
     success:^(NSDictionary *response) {
         LessonContentViewController *lessonContentViewController = [self.lessonControllersMapping objectForKey:[NSNumber numberWithInteger:self.currentPage]];
         
         BOOL status = [lessonContentViewController.lessonPage.status boolValue];
//         [self.toolBar.publishStatusLabel setHidden:YES];
         status ^= YES;
         lessonContentViewController.lessonPage.status = [NSNumber numberWithBool:status];
         [self updateLessonStatusWithStatus:status];
         [lessonContentViewController updateToolbar];
     } failure:nil];
}

- (void)deletePage
{
    TMELessonPageShort *lessonPage = [self.lessonsList objectAtIndex:self.currentPage];
    [[TMEClient sharedClient]
     request:TeamieUserLessonDeleteRequest
     parameters:nil
     makeURL:^NSString *(NSString *URL) {
         return [NSString stringWithFormat:URL, lessonPage.nid];
     }
     loadingMessage:TMELocalize(@"message.loading")
     success:^(NSDictionary *response) {
         [self.navigationController setNavigationBarHidden:NO animated:YES];
         [self.navigationController popViewControllerAnimated:YES];
     } failure:nil];
}

- (void)setAccess:(NSDictionary *)access
{
    if ([[access objectForKey:@"edit_access"]boolValue] == YES) {
        [self.toolBar addSubview: self.enableButton];
        [self.enableButton mas_makeConstraints:^(MASConstraintMaker *make){
            make.left.equalTo(self.toolBar.landingPageButton.mas_trailing).with.offset(23);
            make.width.height.baseline.equalTo(self.toolBar.landingPageButton);
        }];
        
        [self.toolBar addSubview: self.publishedStatusLabel];
        [self.publishedStatusLabel mas_makeConstraints:^(MASConstraintMaker *make){
            make.width.equalTo(@55);
            make.top.height.equalTo(self.toolBar.pagesLabel);
            make.right.equalTo(self.toolBar.pagesLabel.mas_leading);
        }];

    }else{
        [self.enableButton removeFromSuperview];
        [self.publishedStatusLabel removeFromSuperview];
    }
    
    if ([[access objectForKey:@"delete_access"]boolValue] == YES) {
        [self.toolBar addSubview: self.deleteButton];
        [self.deleteButton mas_makeConstraints:^(MASConstraintMaker *make){
            make.left.equalTo(self.enableButton.mas_trailing).with.offset(16);
            make.width.height.baseline.equalTo(self.toolBar.landingPageButton);
        }];

    }else{
        [self.deleteButton removeFromSuperview];
    }
    
    _access = [NSDictionary dictionaryWithDictionary:access];
}
-(void) updateLessonStatusWithStatus :(BOOL)status{
    
    if(status == YES){
        [self.toolBar.publishStatusLabelLessonPage configurePublishStatusLabelWithStatus:PublishStatusPublished];
    }
    else{
        [self.toolBar.publishStatusLabelLessonPage configurePublishStatusLabelWithStatus:PublishStatusDrafted];
    }
    [[TMEClient sharedClient]
          request:TeamieUserLessonRequest
          parameters:nil
          makeURL:^NSString *(NSString *URL) {
              return [NSString stringWithFormat:URL, self.lessonID];
          }
          loadingMessage:TMELocalize(@"message.processing")
          success:^(NSDictionary *response) {
              self.lesson = [TMELesson parseDictionary:response[@"lesson"]];
              self.lessonsList = self.lesson.lesson_pages;
          }
          failure:nil];
    
}

- (void)setEnableButtonColor:(NSString *)colorHex
{
//    [self.enableButton setTitleColor:[UIColor colorWithHex:colorHex alpha:1.0] forState:UIControlStateNormal];
    
    [self.enableButton setTintColor:[UIColor colorWithHex:colorHex alpha:1.0]];
//    self.enableButton
}

- (void)setPublishStatus:(PublishStatus)publishStatus
{
    [self.publishedStatusLabel configurePublishStatusLabelWithStatus:publishStatus];
}
@end
