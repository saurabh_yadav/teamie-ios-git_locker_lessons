//
//  UserProfileViewController.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 7/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UserProfileCollapseClick.h"
#import "UserBadgesViewController.h"
#import "TeamieRevealController.h"
#import "PKRevealController.h"

typedef enum{
    StatsSection = 0,
    BadgesSection,
    ClassroomsSection,
    InfoSection,
    UserProfileSectionCount,
}UserProfileSectionName;
@interface UserProfileViewController : UIViewController<UserProfileCollapseClickDelegate, TMERevealDelegate>

@property (weak, nonatomic) IBOutlet UserProfileCollapseClick *myCollapseClick;
@property (nonatomic) UserBadgesViewController *bagesViewController;
- (id)initWithUserID:(NSInteger)uid;
@end
