//
//  TMEPostComment.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 5/16/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostComment.h"
#import "TMEImageAttachment.h"
#import "TMEFileAttachment.h"
#import "TMEVideoAttachment.h"
#import "TMEResourceAttachment.h"
#import "TMELinkAttachment.h"
#import "TMEPostCommentReply.h"
#import "TMEPostAction.h"

@implementation TMEPostComment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
         @"htmlMessage": @"message.html",
         @"message": @"message.text",
         @"timestampText": @"timestamp.time.text",
         @"isAnonymous": @"is_anonymous",
         @"author": @"user",
         @"repliesCount": @"total_replies",
         @"actionList": @"action",
         @"images": @"attachment.images",
         @"links": @"attachment.links",
         @"files": @"attachment.files",
         @"videos": @"attachment.videos",
         @"resources": @"attachment.resource",
         @"type": @"thought_type",
         @"likeCount": @"statistics.like.count",
         @"voteCount": @"statistics.vote.count",
         @"isMarkedRight": @"statistics.mark-right.count",
         @"attachments": NSNull.null,
    };
}

+ (NSValueTransformer *)cidJSONTransformer {
    return [self numberJSONTransformer];
}

+ (NSValueTransformer *)createdJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)updatedJSONTransformer {
    return [TMEModel dateJSONTransformer];
}

+ (NSValueTransformer *)imagesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEImageAttachment.class];
}

+ (NSValueTransformer *)linksJSONTransformer {
    return [TMEModel listJSONTransformer:TMELinkAttachment.class];
}

+ (NSValueTransformer *)filesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEFileAttachment.class];
}

+ (NSValueTransformer *)videosJSONTransformer {
    return [TMEModel listJSONTransformer:TMEVideoAttachment.class];
}

+ (NSValueTransformer *)resourcesJSONTransformer {
    return [TMEModel listJSONTransformer:TMEResourceAttachment.class];
}

+ (NSValueTransformer *)actionListJSONTransformer {
    return [TMEModel dictionaryListJSONTransformer];
}

+ (NSValueTransformer *)authorJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEUser.class];
}

+ (NSValueTransformer *)repliesJSONTransformer {
    return [self listJSONTransformer:TMEPostCommentReply.class];
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{
       @"comment_thought_thought": @(TMEPostCommentTypeThought),
       @"comment_thought_question": @(TMEPostCommentTypeQuestion),
       @"comment_thought_homework": @(TMEPostCommentTypeHomework),
    }];
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionaryValue error:(NSError *__autoreleasing *)error {
    self = [super initWithDictionary:dictionaryValue error:error];
    if (self) {
        NSMutableDictionary *mutableActions = [[NSMutableDictionary alloc] initWithCapacity:self.actionList.count];
        for (NSDictionary *actionDict in self.actionList) {
            TMEPostAction *action = [TMEPostAction parseDictionary:actionDict];
            mutableActions[action.name] = action;
        }
        self.actions = [mutableActions copy];

        NSMutableArray *attachments = [NSMutableArray new];
        [attachments addObjectsFromArray:self.images];
        [attachments addObjectsFromArray:self.links];
        [attachments addObjectsFromArray:self.files];
        [attachments addObjectsFromArray:self.videos];
        self.attachments = attachments;
        
        for (TMEPostCommentReply *reply in self.replies) {
            reply.parentComment = self;
            reply.type = self.type;
        }
    }
    return self;
}

- (TMEPostAction *)action:(NSString *)actionName {
    return self.actions[actionName];
}

@end
