//
//  TMECardTableViewCell.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMEPost.h"
#import "TMEPostView.h"

@interface TMECardTableViewCell : UITableViewCell

extern const CGFloat kDefaultCardPadding;

@property (nonatomic, strong) UIView *cardView;

@end
