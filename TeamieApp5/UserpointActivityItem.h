//
//  UserpointActivityItem.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 12/14/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserpointActivityItem : NSObject

@property (nonatomic, strong) NSNumber* daypoints;
@property (nonatomic, strong) NSNumber* timestamp;
@property (nonatomic, strong) NSDate* pointday;
@property (nonatomic, strong) NSNumber* totalpoints;

@end
