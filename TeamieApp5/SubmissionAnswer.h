//
//  SubmissionAnswer.h
//  TeamieApp5
//
//  Created by Raunak on 23/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubmissionAnswer : NSObject

@property (nonatomic,strong) NSNumber* aid;
@property (nonatomic,strong) NSNumber* qid;
@property (nonatomic) BOOL isCorrect;
@property (nonatomic,strong) NSNumber* currentScore;
@property (nonatomic,strong) NSString* answerHtml;
@property (nonatomic,strong) NSArray* answerFiles;
@property (nonatomic,strong) NSString* answerFilePath;
@property (nonatomic, strong) NSString* answerSummary;
@property (nonatomic) BOOL isGraded;

@end
