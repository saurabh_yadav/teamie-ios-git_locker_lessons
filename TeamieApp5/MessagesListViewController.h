//
//  MessagesListViewController.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/25/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"

@interface MessagesListViewController : TableRestApiViewController

@property (nonatomic, strong) NSArray * messageThreads;
@property (nonatomic, weak) id delegate;

- (id)initForPopover:(NSArray*)entries;

@end
