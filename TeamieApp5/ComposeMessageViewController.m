//
//  ComposeMessageViewController.m
//  TeamieApp5
//
//  Created by Raunak on 21/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ComposeMessageViewController.h"
#import "TSMessage.h"

@interface ComposeMessageViewController () {
	CGFloat keyboardHeight;
    BOOL allConnectionsLoaded;
    BOOL isMessageSent;
}
@property (nonatomic,strong) UIView* subjectSeparator;
- (void)resizeViews;
@end

@implementation ComposeMessageViewController

- (id)initWithRecipients:(NSArray*)recipients {
    self = [super init];
    if (self) {
        _users = [NSArray arrayWithArray:recipients];
    }
    return self;
}

- (void)viewDidLoad {
	[self setUpView];
    allConnectionsLoaded = NO;
    isMessageSent = NO;
    [self.tokenFieldView.tokenField becomeFirstResponder];
    
    if (self.users != nil && [self.users count] > 0) {
        for (NSNumber* uid in self.users) {
            [self.apiCaller performRestRequest:TeamieUserConnectionsByUIDRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:uid, @"hasUID", nil]];
        }
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cache_message"];
    }
    
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [self.apiCaller performRestRequest:TeamieUserAllConnectionsRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:@"0", @"items_per_page", nil]];
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"cache_message"]) {
        NSDictionary* dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"cache_message"];
        [self updateMessageFromCache:dict];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cache_message"];
    }
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    if (!isMessageSent) {
        if ([self.tokenFieldView.tokenField.tokenObjects count] > 0
            || ![self.subjectView.text isEqualToString:@""]
            || ![self.messageView.text isEqualToString:@""]) {
            
            NSDictionary* dict = [NSMutableDictionary dictionary];
            if ([self.tokenFieldView.tokenField.tokenObjects count] > 0) {
                NSArray* arr = [NSMutableArray array];
                for(UserProfile* user in self.tokenFieldView.tokenField.tokenObjects)
                {
                    [((NSMutableArray*) arr)addObject:user.uid];
                }
                [((NSMutableDictionary*)dict) setObject:arr forKey:@"participants"];
            }
            if (![self.subjectView.text isEqualToString:@""] && !(self.subjectView.text == nil)) {
                [((NSMutableDictionary*)dict) setObject:self.subjectView.text forKey:@"subject"];
            }
            if (![self.messageView.text isEqualToString:@""] && !(self.messageView.text == nil)) {
                [((NSMutableDictionary*)dict) setObject:self.messageView.text forKey:@"message"];
            }
            [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"cache_message"];
        }
    }
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    self.tokenFieldView = nil;
    self.subjectView = nil;
    self.messageView = nil;
    self.users = nil;
    self.subjectSeparator = nil;
    [super viewDidDisappear:animated];    
}

- (void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    [TeamieGlobals dismissActivityLabels];
    if(requestName == TeamieUserConnectionsRequest) {
        if(![response isKindOfClass:[NSArray class]])
            return;
        [self.tokenFieldView setSourceArray:response];
        [self.tokenFieldView showSearchResults];
    }
    else if(requestName == TeamieUserConnectionsByUIDRequest) {
        if(![response isKindOfClass:[NSArray class]])
            return;
        if ([response count] > 0) {
            UserProfile* user = [response objectAtIndex:0];
            [self.tokenFieldView.tokenField addToken:[[TIToken alloc] initWithTitle:[self tokenField:nil displayStringForRepresentedObject:user] representedObject:user]];
            [self.tokenFieldView.tokenField resignFirstResponder];
            [self.subjectView becomeFirstResponder];
        }
    }
    else if(requestName == TeamieUserAllConnectionsRequest) {
        if(![response isKindOfClass:[NSArray class]])
            return;
        allConnectionsLoaded = YES;
        if (!self.tokenFieldView.tokenField.addButton.enabled) {
            [self.tokenFieldView.tokenField.addButton setEnabled:YES];
        }
        [self.tokenFieldView setSourceArray:response];
    }
    else if(requestName == TeamieUserMessageCreateRequest) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"MessageSentNotification" object:self];
        isMessageSent = YES;
        [self dismissViewControllerAnimated:YES completion:nil];
        [TeamieUIGlobals displayStatusMessage:TeamieLocalizedString(@"MSG_MESSAGE_SEND_SUCCESS", nil) forPurpose:@"ok"];
    }
}

- (void)requestFailed:(NSString *)message forRequest:(TeamieRESTRequest)requestName {
    if (requestName == TeamieUserMessageCreateRequest) {
        [self enableButtons];
        [TeamieGlobals addTSMessageInController:self
                                          title:@"Error"
                                        message:TeamieLocalizedString(@"MSG_MESSAGE_SEND_FAILURE", nil)
                                           type:@"error"
                                       duration:5
                                   withCallback:nil];

    }
}

- (void)setUpView {
    [self.view setBackgroundColor:[UIColor whiteColor]];
//	[self.navigationItem setTitle:@"New"];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithHex:@"#BE202E" alpha:1.0];
    UIBarButtonItem* cancelButton = [[UIBarButtonItem alloc] initWithTitle:TeamieLocalizedString(@"BTN_CANCEL", nil) style:UIBarButtonItemStylePlain target:self action:@selector(cancelButtonPressed:)];
    UIBarButtonItem* clearButton = [[UIBarButtonItem alloc] initWithTitle:TeamieLocalizedString(@"BTN_CLEAR", nil) style:UIBarButtonItemStylePlain target:self action:@selector(clearButtonPressed:)];
    UIBarButtonItem* sendButton = [[UIBarButtonItem alloc] initWithTitle:TeamieLocalizedString(@"BTN_SEND", nil) style:UIBarButtonItemStyleDone target:self action:@selector(sendButtonPressed:)];
    self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:cancelButton, clearButton, nil];
    self.navigationItem.rightBarButtonItem = sendButton;
    
    [self.tokenFieldView.contentView addSubview:self.subjectView];
    [self.tokenFieldView.contentView addSubview:self.subjectSeparator];
	[self.tokenFieldView.contentView addSubview:self.messageView];
    
	[self.view addSubview:self.tokenFieldView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
	return (toInterfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	[UIView animateWithDuration:duration animations:^{[self resizeViews];}]; // Make it pweeetty.
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	[self resizeViews];
}

- (void)keyboardWillShow:(NSNotification *)notification {
	
	CGRect keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
	
	// Wouldn't it be fantastic if, when in landscape mode, width was actually width and not height?
	keyboardHeight = keyboardRect.size.height > keyboardRect.size.width ? keyboardRect.size.width : keyboardRect.size.height;
	
	[self resizeViews];
}

- (void)keyboardWillHide:(NSNotification *)notification {
	keyboardHeight = 0;
	[self resizeViews];
}

#pragma mark Private Methods

- (void)resizeViews {
	
	CGRect newFrame = self.tokenFieldView.frame;
	newFrame.size.width = self.view.bounds.size.width;
	newFrame.size.height = self.view.bounds.size.height - keyboardHeight;
	[self.tokenFieldView setFrame:newFrame];
    [self.subjectView setFrame:CGRectMake(CGRectGetMinX(self.tokenFieldView.contentView.bounds), CGRectGetMinY(self.tokenFieldView.contentView.bounds), CGRectGetWidth(self.tokenFieldView.contentView.bounds), 42)];
    [self.subjectSeparator setFrame:CGRectMake(0, CGRectGetMaxY(self.subjectView.frame), self.tokenFieldView.bounds.size.width, 1)];
	[self.messageView setFrame:CGRectMake(0, CGRectGetMaxY(self.subjectView.frame) + 1, CGRectGetWidth(self.tokenFieldView.contentView.bounds), CGRectGetHeight(self.tokenFieldView.contentView.bounds) - CGRectGetMaxY(self.subjectView.frame) - 1)];
}

- (void)cancelButtonPressed:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)clearButtonPressed:(id)sender {
    if ([TSMessage isNotificationActive]) {
        [TSMessage dismissActiveNotification];
    }
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:TeamieLocalizedString(@"MSG_TITLE_MESSAGE_CLEAR", nil) message:TeamieLocalizedString(@"MSG_MESSAGE_CLEAR", nil) delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alertView show];
}

- (void)sendButtonPressed:(id)sender {
    if ([self.tokenFieldView.tokenTitles count] == 0) {
        [TeamieGlobals addTSMessageInController:self title:TeamieLocalizedString(@"MSG_TITLE_MESSAGE_TO_MISSING", nil) message:TeamieLocalizedString(@"MSG_MESSAGE_TO_MISSING", nil) type:@"error" duration:3 withCallback:nil];
    }
    else if ([[self.subjectView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        [TeamieGlobals addTSMessageInController:self title:TeamieLocalizedString(@"MSG_TITLE_MESSAGE_SUBJECT_MISSING", nil) message:TeamieLocalizedString(@"MSG_MESSAGE_SUBJECT_MISSING", nil) type:@"error" duration:3 withCallback:nil];
    }
    else if ([[self.messageView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqualToString:@""]) {
        [TSMessage showNotificationInViewController:self.navigationController
                                              title:TeamieLocalizedString(@"MSG_TITLE_MESSAGE_EMPTY", nil)
                                           subtitle:TeamieLocalizedString(@"MSG_MESSAGE_EMPTY", nil)
                                              image:nil
                                               type:TSMessageNotificationTypeWarning
                                           duration:5
                                           callback:nil
                                        buttonTitle:TeamieLocalizedString(@"BTN_SEND", nil)
                                     buttonCallback:^{[self sendMessage];}
                                         atPosition:TSMessageNotificationPositionTop
                               canBeDismissedByUser:YES];
    }
    else {
        [self sendMessage];
    }
}

- (void)sendMessage {
    
    [self disableButtons];
    if ([self.messageView isFirstResponder]) {
        [self.messageView resignFirstResponder];
    }
    else if([self.subjectView isFirstResponder]) {
        [self.subjectView resignFirstResponder];
    }
    [self.tokenFieldView resignFirstResponder];
    
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    [dict setObject:[NSString stringWithFormat:@"%d",[self.tokenFieldView.tokenTitles count]] forKey:@"num_participants"];
    for (int i = 0; i < [self.tokenFieldView.tokenField.tokenObjects count]; i++) {
        UserProfile* user = [self.tokenFieldView.tokenField.tokenObjects objectAtIndex:i];
        [dict setObject:user.uid forKey:[NSString stringWithFormat:@"participant[%d]",i]];
    }
    [dict setObject:self.subjectView.text forKey:@"subject"];
    [dict setObject:self.messageView.text forKey:@"message"];
    
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_SEND_IN_PROGRESS", nil) margin:10.f yOffset:10.f];
    [self.apiCaller performRestRequest:TeamieUserMessageCreateRequest withParams:dict];
}

- (void)showContactsPicker {
	[self.tokenFieldView resignFirstResponder];
    [self.tokenFieldView showSearchResults];
}

- (void)downloadImageWithURL:(NSURL *)url completionBlock:(void (^)(BOOL succeeded, UIImage *image))completionBlock
{
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if ( !error )
                               {
                                   UIImage *image = [[UIImage alloc] initWithData:data];
                                   completionBlock(YES,image);
                               } else{
                                   completionBlock(NO,nil);
                               }
                           }];
}

- (void)updateMessageFromCache:(NSDictionary*)dict {
    if ([dict objectForKey:@"participants"]) {
        for (NSNumber* uid in ((NSArray*)[dict objectForKey:@"participants"])) {
            [self.apiCaller performRestRequest:TeamieUserConnectionsByUIDRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:uid, @"hasUID", nil]];
        }
    }
    if ([dict objectForKey:@"subject"]) {
        self.subjectView.text = [dict objectForKey:@"subject"];
    }
    if ([dict objectForKey:@"message"]) {
        self.messageView.text = [dict objectForKey:@"message"];
    }
}

- (void)disableButtons {
    for (UIBarButtonItem* button in self.navigationItem.rightBarButtonItems) {
        [button setEnabled:NO];
    }
    for (UIBarButtonItem* button in self.navigationItem.leftBarButtonItems) {
        [button setEnabled:NO];
    }
}

- (void)enableButtons {
    for (UIBarButtonItem* button in self.navigationItem.rightBarButtonItems) {
        [button setEnabled:YES];
    }
    for (UIBarButtonItem* button in self.navigationItem.leftBarButtonItems) {
        [button setEnabled:YES];
    }
}


#pragma mark UIAlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [alertView dismissWithClickedButtonIndex:0 animated:YES];
    }
    else if (buttonIndex ==1) {
        for (TIToken* token in self.tokenFieldView.tokenField.tokens) {
            [self.tokenFieldView.tokenField removeToken:token];
        }
        self.subjectView.text = @"";
        self.messageView.text = @"";
    }
}

#pragma mark TITokenFieldView Delegate Methods

- (UITableViewCell *)tokenField:(TITokenField *)tokenField resultsTableView:(UITableView *)tableView cellForRepresentedObject:(id)object atIndexPath:(NSIndexPath *)indexPath {
    
    static NSString * CellIdentifier = @"ResultsCell";
    
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    }
	[cell.textLabel setText:((UserProfile*)object).fullname];
    [cell.detailTextLabel setText:((UserProfile*)object).email];
    cell.imageView.image = [UIImage imageNamed:@"Icon.png"];
    [self downloadImageWithURL:[NSURL URLWithString:((UserProfile*)object).displayPic] completionBlock:^(BOOL succeeded, UIImage *image) {
        if (succeeded) {
            cell.imageView.image = image;
        }
    }];	
    return cell;
    
}

- (NSString *)tokenField:(TITokenField *)tokenField displayStringForRepresentedObject:(id)object {
    UserProfile* user = (UserProfile*)object;
    if ([user.fullname isEqualToString:@""]) {
        return user.email;
    }
    return user.fullname;
}

- (NSString *)tokenField:(TITokenField *)tokenField searchResultStringForRepresentedObject:(id)object {
    UserProfile* user = (UserProfile*)object;
    if ([user.fullname isEqualToString:@""]) {
        return user.email;
    }
    return user.fullname;
}

- (void)tokenField:(TITokenField *)tokenField didChangeToFrame:(CGRect)frame {
	[self textViewDidChange:self.messageView];
}

- (void)tokenField:(TITokenField *)tokenField didEditText:(NSString *)substring {
    NSString * strippedString = [[substring stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
    
    if (!allConnectionsLoaded) {
        [self.apiCaller performRestRequest:TeamieUserConnectionsRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:strippedString, @"containsName", nil]];
    }
    else {
        [self.tokenFieldView resultsForSubstring:strippedString];
    }
}

#pragma mark UITextView Delegate Methods

- (void)textViewDidChange:(UITextView *)textView {
	
	CGFloat oldHeight = self.tokenFieldView.frame.size.height - self.tokenFieldView.tokenField.frame.size.height;
	CGFloat newHeight = textView.contentSize.height + textView.font.lineHeight;
	
	CGRect newTextFrame = textView.frame;
	newTextFrame.size = textView.contentSize;
	newTextFrame.size.height = newHeight;
	
	CGRect newFrame = self.tokenFieldView.contentView.frame;
	newFrame.size.height = newHeight;
	
	if (newHeight < oldHeight){
		newTextFrame.size.height = oldHeight;
		newFrame.size.height = oldHeight;
	}
    
	[self.tokenFieldView.contentView setFrame:newFrame];
	[textView setFrame:newTextFrame];
	[self.tokenFieldView updateContentSize];
}

#pragma mark Lazy Instantiation Methods

- (TITokenFieldView*)tokenFieldView {
    if (!_tokenFieldView) {
        _tokenFieldView = [[TITokenFieldView alloc] initWithFrame:self.view.bounds];
        [_tokenFieldView setDelegate:self];
        [_tokenFieldView.tokenField setAddButtonAction:@selector(showContactsPicker) target:self];
        [_tokenFieldView.tokenField setTokenizingCharacters:[NSCharacterSet characterSetWithCharactersInString:@",;."]]; // Default is a comma
        [_tokenFieldView.tokenField.addButton setEnabled:NO];
    }
    return _tokenFieldView;
}

- (MessageSubjectField*)subjectView {
    if (!_subjectView) {
        _subjectView = [[MessageSubjectField alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.tokenFieldView.contentView.bounds), CGRectGetMinY(self.tokenFieldView.contentView.bounds), CGRectGetWidth(self.tokenFieldView.contentView.bounds), 42)];
        _subjectView.text = @"";
    }
    return _subjectView;
}

- (UIView*)subjectSeparator {
    if (!_subjectSeparator) {
        _subjectSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.subjectView.frame), self.tokenFieldView.bounds.size.width, 1)];
        [_subjectSeparator setBackgroundColor:[UIColor colorWithWhite:0.7 alpha:1]];
        [_subjectSeparator setAutoresizingMask:UIViewAutoresizingNone];
    }
    return _subjectSeparator;
}

- (UITextView*)messageView {
    if (!_messageView) {
        _messageView = [[UITextView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.subjectView.frame) + 1, CGRectGetWidth(self.tokenFieldView.contentView.bounds), CGRectGetHeight(self.tokenFieldView.contentView.bounds) - CGRectGetMaxY(self.subjectView.frame) - 1)];
        [_messageView setScrollEnabled:NO];
        [_messageView setAutoresizingMask:UIViewAutoresizingNone];
        [_messageView setDelegate:self];
        [_messageView setFont:[UIFont systemFontOfSize:15]];
        _messageView.text = @"";
    }
    return _messageView;
}


@end