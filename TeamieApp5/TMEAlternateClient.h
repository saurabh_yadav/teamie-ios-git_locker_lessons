//
//  TeamieAlternateClient.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEBaseClient.h"

@interface TMEAlternateClient : TMEBaseClient

+ (instancetype)sharedClient;

@end