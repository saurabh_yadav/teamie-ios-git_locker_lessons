//
//  NewsfeedItemAttachmentPollOption.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/28/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "NewsfeedItemAttachmentPollOption.h"
#import "NewsfeedItemAttachment.h"


@implementation NewsfeedItemAttachmentPollOption

@dynamic currentUserVote;
@dynamic numVotes;
@dynamic title;
@dynamic choiceKey;
@dynamic newsfeedItemAttachment;

- (NSNumber*)getFraction {
    return [NSNumber numberWithDouble:[self.numVotes doubleValue] / [self.newsfeedItemAttachment.totalVotes doubleValue]];
}

@end
