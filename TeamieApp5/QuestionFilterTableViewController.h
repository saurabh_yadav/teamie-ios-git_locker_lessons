//
//  QuestionFilterTableViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 13/12/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionFilterTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) UITableView* questionTable;
@property (nonatomic,strong) UITableView* userTable;
@property (nonatomic,strong) NSArray* questionPickerList;
@property (nonatomic,strong) NSDictionary* userPickerList;

- (NSArray*)getSelectedQIDs;
- (NSArray*)getSelectedUIDs;
- (void)selectAllUsers;

@end
