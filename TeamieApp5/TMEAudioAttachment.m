//
//  TMEAudioAttachment.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 17/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEAudioAttachment.h"

@implementation TMEAudioAttachment

+ (NSValueTransformer *)fidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

- (UIImage *)previewImage {
    return [TeamieUIGlobals defaultPicForPurpose:@"audio"
                                        withSize:15
                                        andColor:[TeamieUIGlobalColors defaultTextColor]];
}

- (NSString *)displaySubTitle {
    return @"Audio file";
}

@end
