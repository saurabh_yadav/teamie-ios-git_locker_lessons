//
//  UILabelWithPadding.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 22/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum{
    PublishStatusRemoval = 0,
    PublishStatusDrafted,
    PublishStatusPublished,
    PublishStatusViewed,
    PublishStatusCount
}PublishStatus;
/**
 *  Subclass of UILabel to add margins to normal UILabel Text on
 *  Set edgeInsets using UIEdgeInsets (top, left, bottom, right)
 */
@interface UILabelWithPadding : UILabel

@property (nonatomic, assign) UIEdgeInsets edgeInsets;

- (void)configurePublishStatusLabelWithStatus:(PublishStatus)publishStatus;

@end
