//
//  UIView+AutoLayout.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 20/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UIView+AutoLayout.h"

@implementation UIView(AutoLayout)

- (void)pinSubview:(UIView *)view withEdgeInsets:(UIEdgeInsets)edgeInsets flexibleWidth:(BOOL)isFlexibleWidth flexibleHeight:(BOOL)isFlexibleHeight{
    NSDictionary* views = NSDictionaryOfVariableBindings(view);
    NSDictionary* metrics = @{@"top":[NSNumber numberWithFloat:edgeInsets.top],
                              @"left":[NSNumber numberWithFloat:edgeInsets.left],
                              @"bottom":[NSNumber numberWithFloat:edgeInsets.bottom],
                              @"right":[NSNumber numberWithFloat:edgeInsets.right],
                              @"width":[NSNumber numberWithFloat:CGRectGetWidth(self.frame) - edgeInsets.left - edgeInsets.right],
                              @"height":[NSNumber numberWithFloat:CGRectGetHeight(self.frame) - edgeInsets.top - edgeInsets.bottom]};
    
    NSString* hFormat = isFlexibleWidth? @"H:|-left-[view]-right-|" : @"H:|-left-[view(width)]-right-|";
    NSString* vFormat = isFlexibleHeight? @"V:|-top-[view]-bottom-|" : @"V:|-top-[view(height)]-bottom-|";
    
    NSArray* hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:hFormat
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views];
    NSArray* vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:vFormat
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views];
    [self addConstraints:hConstraints];
    [self addConstraints:vConstraints];
}

- (NSArray*)verticallyPinSubviews:(NSArray *)subviews underTopView:(UIView *)topView bottomView:(UIView *)bottomView topMargin:(CGFloat)topMargin bottomMargin:(CGFloat)bottomMargin leftRightMargin:(CGFloat)hMargin vDistance:(CGFloat)vDistance{
        
    NSLayoutConstraint* firstVConstraint;
    NSLayoutConstraint* lastVConstraint;
    
    NSDictionary* metrics = @{@"topMargin":[NSNumber numberWithFloat:topMargin],
                              @"bottomMargin":[NSNumber numberWithFloat:bottomMargin],
                              @"hMargin":[NSNumber numberWithFloat:hMargin],
                              @"vDistance":[NSNumber numberWithFloat:vDistance]};
    UIView* prevView;
    
    for (UIView* view in subviews){
        view.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSMutableDictionary* views = [NSMutableDictionary dictionaryWithObject:view forKey:@"view"];
        
        NSArray* hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-hMargin-[view]-hMargin-|"
                                                                        options:0
                                                                        metrics:metrics
                                                                          views:views];
        NSMutableString* vFormat;
        
        if (view == [subviews firstObject]){
            if (topView){
                [views setObject:topView forKey:@"topView"];
                vFormat = [NSMutableString stringWithString:@"V:[topView]-topMargin-[view]"];
            }
            else{
                vFormat = [NSMutableString stringWithString:@"V:|-topMargin-[view]"];
            }
        }
        else{
            assert(prevView != nil);
            [views setObject:prevView forKey:@"prevView"];
            vFormat = [NSMutableString stringWithString:@"V:[prevView]-vDistance-[view]"];
        }
        if (view == [subviews lastObject]){
            if (bottomView){
                [views setObject:bottomView forKey:@"bottomView"];
                [vFormat appendString:@"-bottomMargin-[bottomView]"];
            }
            else{
                [vFormat appendString:@"-bottomMargin-|"];
            }
        }
            
        NSArray* vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:vFormat
                                                                        options:0
                                                                        metrics:metrics
                                                                          views:views];
        
        [self addConstraints:hConstraints];
        [self addConstraints:vConstraints];
        
        if (view == [subviews firstObject]){
            firstVConstraint = [vConstraints firstObject];
        }
        
        lastVConstraint = [vConstraints lastObject];
        prevView = view;
    }
    
    return @[firstVConstraint, lastVConstraint];
}

@end
