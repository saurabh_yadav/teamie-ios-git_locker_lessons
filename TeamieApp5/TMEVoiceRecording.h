//
//  TMEVoiceRecording.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 26/03/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEModel.h"

@interface TMEVoiceRecording : TMEModel

@property (nonatomic, retain) NSString * recordPath;
@property (nonatomic) CGFloat recordTime;

- (void)startRecordWithPath:(NSString *)path delegate:(id)viewController;
- (void)stopRecordWithCompletionBlock:(void (^)())completion;
- (void)cancelled;

@end
