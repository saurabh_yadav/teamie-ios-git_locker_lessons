//
//  ResourceWebViewController.m
//  TeamieApp5
//
//  Created by Raunak on 16/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ResourceWebViewController.h"

@interface ResourceWebViewController () {
    BOOL openInOptionsVisible;
}
@property (nonatomic, strong) UIWebView* webView;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSNumber* tid;
@property (nonatomic, strong) UIDocumentInteractionController* openInOptionsController;
@property (nonatomic, strong) WEPopoverController* menuController;
@end

@implementation ResourceWebViewController

- (void)loadFileWithUrl:(NSURL*)url WithTitle:(NSString*)title WithThoughtID:(NSNumber*)tid {
    self.fileURL = url;
    self.title = title;
    self.tid = tid;
    self.openInOptionsController = [UIDocumentInteractionController interactionControllerWithURL:self.fileURL];
    self.openInOptionsController.delegate = self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = self.title;
    
    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.webView.scalesPageToFit = YES;
    [self.view addSubview:self.webView];
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:self.fileURL]];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(menuButtonPressed:)];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStyleBordered target:self action:@selector(backButtonPressed:)];
}

- (void)backButtonPressed:(id)sender {
    [self dismissModalViewControllerAnimated:YES];
}

- (void)menuButtonPressed:(id)sender {
    if (openInOptionsVisible) {
        [self.openInOptionsController dismissMenuAnimated:YES];
    }
    else {
        if(!self.menuController) {
            ResourcesShareMenuViewController* contentViewController = [[ResourcesShareMenuViewController alloc] init];
            contentViewController.delegate = self;
            
            Class popoverClass;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                popoverClass = [UIPopoverController class];
            }
            else {
                popoverClass = [WEPopoverController class];
            }
            
            self.menuController = [[popoverClass alloc] initWithContentViewController:contentViewController];
            self.menuController.delegate = self;
            [self.menuController presentPopoverFromBarButtonItem:(UIBarButtonItem*)sender permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown) animated:YES];
        }
        else {
            [self.menuController dismissPopoverAnimated:YES];
            self.menuController= nil;
        }
    }
}

- (void) shareMenuDidSelectDeleteOption:(ShareMenuOptions)option {
    
    [self.menuController dismissPopoverAnimated:YES];
    self.menuController= nil;
    
    switch (option) {
        case kShare:
            [self.delegate resourcesWebControllerDidSelectShareOption:self];
            break;
        case kOpenIn:
            if (!openInOptionsVisible) {
                [self.openInOptionsController presentOptionsMenuFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
            }
            break;
        default:
            break;
    }
}

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    self.menuController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

- (void)documentInteractionControllerWillPresentOptionsMenu:(UIDocumentInteractionController *)controller {
    openInOptionsVisible = YES;
}

- (void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller {
    openInOptionsVisible = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
