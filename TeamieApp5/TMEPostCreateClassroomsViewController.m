//
//  TMEPostShareClassroomsTableViewController.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 4/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostCreateClassroomsViewController.h"
#import "TMEClassroom.h"

@interface TMEPostCreateClassroomsViewController ()

@property (nonatomic, weak) TMEPostCreateViewModel *viewModel;

@end

#pragma mark -

@implementation TMEPostCreateClassroomsViewController

NSArray *classrooms;

- (instancetype)initWithDelegate:(id)delegate {
    self = [super init];
    if (self) {
        classrooms = [TMEClassroom getFromUserDefaults];
        self.selectedClassrooms = [NSMutableSet new];
        self.delegate = delegate;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.topItem.title = @"Done";
    self.navigationItem.title = @"Select Classrooms";
    NSDictionary *sitePermissions = [[NSUserDefaults standardUserDefaults] objectForKey:SITE_PERMISSIONS_KEY];
    if ([sitePermissions[@"allow multiple group post"] boolValue]) {
        self.tableView.allowsMultipleSelection = YES;
    }
    else {
        self.tableView.allowsMultipleSelection = NO;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (classrooms.count == 0) {
        classrooms = [TMEClassroom getFromUserDefaults];
    }
    if (self.selectedClassrooms.count > 0) {
        [classrooms enumerateObjectsUsingBlock:^(TMEClassroom *classroom, NSUInteger idx, BOOL *stop) {
            if ([self.selectedClassrooms containsObject:classroom]) {
                [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]
                                            animated:NO
                                      scrollPosition:UITableViewScrollPositionNone];
            }
        }];
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.delegate didFinishPickingClassrooms:[self.selectedClassrooms allObjects]];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return classrooms.count;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (cell.selected) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    //For such a small number of cells, dequeueing will be slower than creating one here.
    UITableViewCell *cell = [UITableViewCell new];
    cell.textLabel.text = ((TMEClassroom *)classrooms[indexPath.row]).name;
    return cell;
}
                             
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    [self.selectedClassrooms addObject:classrooms[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    [self.selectedClassrooms removeObject:classrooms[indexPath.row]];
}

@end
