//
//  TMEImageAttachment.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostAttachment.h"

@interface TMEImageAttachment : TMEPostAttachment

@property (nonatomic, strong) NSURL *thumbnail;
@property (nonatomic, strong) NSString *html;

@end
