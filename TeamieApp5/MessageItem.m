//
//  MessageItem.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/26/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "MessageItem.h"
#import "MessageThread.h"
#import "UserProfile.h"


@implementation MessageItem

@dynamic body;
@dynamic is_new;
@dynamic mid;
@dynamic time_stamp;
@dynamic author;
@dynamic messageThread;

@end
