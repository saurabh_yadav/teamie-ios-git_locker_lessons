//
//  UserProfileStatsViewController.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 8/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileStatsViewController : UITableViewController

- (id)initWithUserStats:(NSDictionary *)userStats;
@end
