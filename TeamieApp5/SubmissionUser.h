//
//  SubmissionUser.h
//  TeamieApp5
//
//  Created by Raunak on 17/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SubmissionUser : NSObject

@property (nonatomic,strong) NSString* name;
@property (nonatomic,strong) NSNumber* uid;
@property (nonatomic,strong) NSString* mail;
@property (nonatomic,strong) NSString* realName;
@property (nonatomic,strong) NSString* displayPic;

@end
