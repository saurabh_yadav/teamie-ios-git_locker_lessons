//
//  MessageItem.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/26/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class MessageThread, UserProfile;

@interface MessageItem : NSManagedObject

@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSNumber * is_new;
@property (nonatomic, retain) NSNumber * mid;
@property (nonatomic, retain) NSNumber * time_stamp;
@property (nonatomic, retain) UserProfile *author;
@property (nonatomic, retain) MessageThread *messageThread;

@end
