//
//  UserProfileCollapseClick.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 7/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "CollapseClick.h"
@protocol UserProfileCollapseClickDelegate<CollapseClickDelegate>

@required

@optional

@end
@interface UserProfileCollapseClick : CollapseClick
@property (weak) id<UserProfileCollapseClickDelegate>CollapseClickDelegate;
@end
