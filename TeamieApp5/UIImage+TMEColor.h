//
//  UIImage+TMEColor.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 25/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage (TMEColor)

+ (UIImage *)imageWithColor:(UIColor *)color;

@end
