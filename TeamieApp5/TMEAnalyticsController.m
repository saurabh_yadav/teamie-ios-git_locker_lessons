//
//  TMEAnalyticsController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 06/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEAnalyticsController.h"
#import "TeamieGlobals.h"

#import "TMEPostViewController.h"
#import "TMENewsfeedViewController.h"
#import "TMEClassroomNewsfeedViewController.h"
#import "BulletinBoardViewController.h"
#import "InstituteSelectViewController.h"
#import "LoginViewController.h"
#import "UserProfileViewController.h"
#import "TMEPostCreateViewController.h"
#import "TMEUpcomingEventsViewController.h"
#import "LSViewController.h"
#import "LessonInfoViewController.h"
#import "LessonContentViewController.h"
#import "LessonPageViewController.h"
#import "AssessmentListViewController.h"

#import <GoogleAnalytics-iOS-SDK/GAI.h>
#import <GoogleAnalytics-iOS-SDK/GAIFields.h>
#import <GoogleAnalytics-iOS-SDK/GAIDictionaryBuilder.h>

@implementation TMEAnalyticsController

NSString * const kAppDynamicsMobileEUMKey = @"AD-AAB-AAA-KYR";

#ifdef DEBUG
NSString * const kGoogleAnalyticsID = @"UA-22650816-8";
#else
NSString * const kGoogleAnalyticsID = @"UA-22650816-7";
#endif

+ (void)startTrackingOnGoogleAnalytics {
    [[GAI sharedInstance] trackerWithTrackingId:kGoogleAnalyticsID];
    
#ifdef DEBUG
    //Set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    //Faster dispatch time for testing. For production, the normal default of 120 seconds will apply
    [GAI sharedInstance].dispatchInterval = 20;
#endif
}

void TMETrackEvent(NSString *category, NSString *action, NSString *label) {
    [[[GAI sharedInstance] defaultTracker] send:
     [[GAIDictionaryBuilder createEventWithCategory:category
                                             action:action
                                              label:label
                                              value:nil]
      build]];
}

+ (void)trackScreen:(NSString *)screenName {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value:screenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
}

+ (void)trackDimension:(TMEAnalyticsCustomDimension)dimension value:(NSString *)value {
    [[[GAI sharedInstance] defaultTracker] send:
     [[[GAIDictionaryBuilder createScreenView]
       set:value
       forKey:[GAIFields customDimensionForIndex:dimension]] build]];
}

@end
