//
//  URLParam.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class UserMenu;

@interface URLParam : NSManagedObject

@property (nonatomic, retain) NSNumber * isOptional;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * value;
@property (nonatomic, retain) UserMenu *paramMenu;

@end
