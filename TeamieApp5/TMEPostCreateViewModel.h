//
//  TMEPostShareViewModel.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 2/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

#import "TMEPost.h"

@interface TMEPostCreateViewModel : NSObject

// Necessary State
@property (nonatomic, strong) NSString *postContentText;
@property (nonatomic, readonly) NSAttributedString *classroomsText;
@property (nonatomic, readonly) NSAttributedString *deadlineText;
@property (nonatomic, readonly) NSString *attachmentsText;
@property (nonatomic, strong) NSArray *selectedClassrooms;
@property (nonatomic) BOOL anonymous;
@property (nonatomic) BOOL announcement;
@property (nonatomic) BOOL notificationAnnouncement;
@property (nonatomic) BOOL emailAnnouncement;
@property (nonatomic) BOOL restrictComments;
@property (nonatomic) TMEPostType postType;
@property (nonatomic, strong) NSDate *deadline;
@property (nonatomic) BOOL allDay;
@property (nonatomic, strong) NSMutableArray *attachments;

@end