//
//  TeamieUIGlobals.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 6/27/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WSCoachMarksView.h"
#import "UIView+UIView_WSCoachMarks.h"
#import "TeamieUIGlobalColors.h"

extern NSString * const COACH_MARKS_SHOWN_PREFIX;

@interface TeamieUIGlobals : NSObject

extern const CGFloat BORDER_THICKNESS;

// Return the image to be used as default image for a given purpose
+ (UIImage*)defaultPicForPurpose:(NSString*)purpose;
+ (UIImage*)defaultPicForPurpose:(NSString*)purpose withSize:(CGFloat)size;
+ (UIImage*)defaultPicForPurpose:(NSString*)purpose withSize:(CGFloat)size andColor:(UIColor*)color;

// Display a message on screen with the help of MBProgressHUD along with an icon
+ (void)displayStatusMessage:(NSString*)msg forPurpose:(NSString*)purpose;

// Display an Alert Message
+ (id)showAlertWithTitle:(NSString*)title message:(NSString*)msg delegate:(id)delegate tag:(NSInteger)tag cancelButton:(NSString*)cancelTitle otherTitles:(NSString*)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION;

// Method for initiliazing the Coach Marks View for a given view controller
// This method will return the generated coach marks view
+ (id)initializeCoachMarksView:(NSString*)viewControllerClass forView:(UIView*)containerView forElements:(NSArray*)elements;

@end
