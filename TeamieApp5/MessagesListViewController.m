//
//  MessagesListViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 4/25/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "MessagesListViewController.h"
#import "MessageThread.h"
#import "MessageThreadViewController.h"

#import "ComposeMessageViewController.h"

static const NSString* NUMBER_OF_MESSAGE_ITEMS = @"50";

@interface MessagesListViewController ()

@property (nonatomic,strong) ComposeMessageViewController* cmVC;
-(void)generateTableItems;

@end

@implementation MessagesListViewController

@synthesize messageThreads;

#pragma mark - View lifecycle

- (id)initForPopover:(NSArray*)entries {
    if (self = [super init]) {
        //self.contentSizeForViewInPopover = TTNEWSFEEDSTYLEVAR(kDefaultMessagesPopoverSize);
        self.messageThreads = entries;
        //[TTStyleSheet setGlobalStyleSheet:[[MessageListStyleSheet alloc] init]];
    }
    return self;
}

#pragma mark - View Lifecycle methods

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationItem.title = TeamieLocalizedString(@"TITLE_MESSAGES", nil);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Compose" style:UIBarButtonItemStyleBordered target:self action:@selector(composeMessageSelected:)];
    //self.navigationBarTintColor = [UIColor blackColor];
    
    // Drawing out the table with the message threads
    //self.tableViewStyle = UITableViewStylePlain;
    //self.variableHeightRows = YES;

    [self generateTableItems];
}

- (void)composeMessageSelected:(id)sender {
    /*if (self.delegate != nil && [self.delegate isKindOfClass:[NewsfeedViewController class]]) {
        self.cmVC = [[ComposeMessageViewController alloc] initWithRecipients:nil];
        [((NewsfeedViewController*) self.delegate) presentModalViewController:[[UINavigationController alloc] initWithRootViewController:self.cmVC] animated:YES];
        [((NewsfeedViewController*) self.delegate) dismissPopovers];
    }*/
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    //[[GANTracker sharedTracker] trackPageview:@"/messages" withError:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath {
    
    [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING_MSG_THREAD", nil) margin:10.f yOffset:10.f];
    
    if ([self.messageThreads count] > 0) {
        if (indexPath.row < [self.messageThreads count]) {
            MessageThread* threadObject = (MessageThread*)[self.messageThreads objectAtIndex:indexPath.row];
            NSNumber* threadId = threadObject.tid;
            [self.apiCaller performRestRequest:TeamieUserMessageThreadRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:threadId, @"thread_id", NUMBER_OF_MESSAGE_ITEMS, @"items_per_page", nil]];
            
            // mark thread object as read & save it
            threadObject.is_new = NO;
            [threadObject saveObject];
            
            [self generateTableItems];
            
            //[[GANTracker sharedTracker] trackEvent:@"Messages" action:@"Messages Thread Open" label:nil value:[threadId longLongValue] withError:nil];
        }
    }
    else {
        //[super didSelectObject:object atIndexPath:indexPath];
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    
    [TeamieGlobals dismissActivityLabels];
    
    if (requestName == TeamieUserMessageThreadRequest) {
        MessageThreadViewController* threadViewController = [[MessageThreadViewController alloc] initWithItems:response];
        [self.navigationController pushViewController:threadViewController animated:YES];
    }
    else {
        [super requestSuccessful:requestName withResponse:response];
    }
}

#pragma mark - Private methods

- (void)generateTableItems {
    
    NSMutableArray * entries = [[NSMutableArray alloc] init];
    
    for (MessageThread* item in self.messageThreads) {
        NSString* messageParticipants = nil;
        NSString* newLabel = nil;
        
        for (UserProfile* participant in item.participants) {
            if (messageParticipants) {
                messageParticipants = [messageParticipants stringByAppendingFormat:@", %@", participant.fullname];
            }
            else {
                messageParticipants = participant.fullname;
            }
        }
        
        if ([messageParticipants length] == 0) {
            messageParticipants = TeamieLocalizedString(@"ANONYMOUS", nil);
        }
        
        if ([item.is_new boolValue]) {
            newLabel = @"bundle://icon-message-new.png";
        }
        
        // NSString* messageHtml = @"<p>\nFor messages, also can add that?</p>";
        NSString* messageHtml = [TeamieGlobals encodeStringToXHTML:[NSString stringWithFormat:@"<div class=\"msgParticipants\">%@</div><div class=\"msgSubject\">%@</div><div class=\"dateText\">%@</div>", messageParticipants, item.subject, [TeamieGlobals timeIntervalWithStartTime:[item.lastUpdated doubleValue] withEndTime:0]]];
/*        TTTableBulletinItem* messageEntry = [TTTableBulletinItem itemWithText:[TTStyledText textFromXHTML:messageHtml] imageURL:newLabel delegate:nil selector:nil];
        messageEntry.margin = UIEdgeInsetsMake(5, 5, 0, 5);
        
        [entries addObject:messageEntry];*/
    }
    
    //TeamieListDataSource* dataSource = [[TeamieListDataSource alloc] initWithItems:entries];
    
    //self.dataSource = dataSource;
    
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
}

@end
