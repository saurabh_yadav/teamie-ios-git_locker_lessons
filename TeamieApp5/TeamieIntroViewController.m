//
//  TeamieIntroViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 27/11/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "TeamieIntroViewController.h"

#import <QuartzCore/QuartzCore.h>

static NSString * const sampleDesc1 = @"Make learning social, collaborative and engaging";
static NSString * const sampleDesc2 = @"Share & discuss with your peers and attach polls, files, links, drawings and images";
static NSString * const sampleDesc3 = @"Read lessons, view attachments and annotate on documents";
static NSString * const sampleDesc4 = @"Get notifications, send messages, bookmark resources, or simply shake your phone";
static NSString * const sampleDesc5 = @"View submissions, grade open-ended questions and access class gradebook";

@implementation TeamieIntroViewController

- (void)viewDidAppear:(BOOL)animated {
    // Show intro
    BOOL showTeamieIntro = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShowTeamieIntro"];
    if (showTeamieIntro == NO) {
        // Don't show again
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ShowTeamieIntro"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self showIntroWithCrossDissolve];
}

- (void)introDidFinish:(EAIntroView *)introView {
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ShowTeamieIntro"];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)showIntroWithCrossDissolve {
    UIImage* bg = [self imageWithColor:[UIColor colorWithHex:@"#D1EEFC" alpha:1.0]];
    UIColor* titleColor = [UIColor colorWithHex:@"#3498db" alpha:1.0];
    
//    BOOL isiPhoneFourInch = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone && [TeamieGlobals hasFourInchDisplay]);
    BOOL isiPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
    
//    CGFloat threePointFiveInchTitleYCoord = 130;    // Y coordinate for Title on 3.5-inch display
//    CGFloat threePointFiveInchDescYCoord = 115;     // Y coordinate for Description on 3.5-inch display
//    CGFloat fourInchTitleYCoord = 121;              // Y coordinate for Title on 4-inch display
//    CGFloat fourInchDescYCoord = 106;               // Y coordinate for Description on 4-inch display
//    CGFloat iPadTitleYCoord = 230;                  // Y coordinate for Title on iPad
//    CGFloat iPadDescYCoord = 205;                   // Y coordinate for Description on iPad
    UIFont* titleFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:22];                     // Title font size on iPad
    UIFont* descFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:18];                     // Description font size on iPad
    
    EAIntroPage *page1 = [EAIntroPage page];
    page1.title = @"Welcome to Teamie";
    page1.titleColor = titleColor;
    page1.desc = sampleDesc1;
    page1.descColor = titleColor;
    page1.bgImage = bg;
//    page1.titleImage = (isiPad)?[UIImage imageNamed:@"iPad-Story1"]:[UIImage imageNamed:@"iPhone-Story1"];
    if (isiPad) {
//        page1.titlePositionY = iPadTitleYCoord;
//        page1.descPositionY = iPadDescYCoord;
        page1.titleFont = titleFont;
        page1.descFont = descFont;
    }
    else {
//        page1.titlePositionY = (isiPhoneFourInch)?fourInchTitleYCoord:threePointFiveInchTitleYCoord;
//        page1.descPositionY = (isiPhoneFourInch)?fourInchDescYCoord:threePointFiveInchDescYCoord;
    }
    
    EAIntroPage *page2 = [EAIntroPage page];
    page2.title = @"Social Learning Network";
    page2.titleColor = titleColor;
    page2.desc = sampleDesc2;
    page2.descColor = titleColor;
    page2.bgImage = bg;
//    page2.titleImage = (isiPad)?[UIImage imageNamed:@"iPad-Story2"]:[UIImage imageNamed:@"iPhone-Story2"];
    if (isiPad) {
//        page2.titlePositionY = iPadTitleYCoord;
//        page2.descPositionY = iPadDescYCoord;
        page2.titleFont = titleFont;
        page2.descFont = descFont;
    }
    else {
//        page2.titlePositionY = (isiPhoneFourInch)?fourInchTitleYCoord:threePointFiveInchTitleYCoord;
//        page2.descPositionY = (isiPhoneFourInch)?fourInchDescYCoord:threePointFiveInchDescYCoord;
    }
    
    EAIntroPage *page3 = [EAIntroPage page];
    page3.title = @"Learn on the go";
    page3.titleColor = titleColor;
    page3.desc = sampleDesc3;
    page3.descColor = titleColor;
    page3.bgImage = bg;
//    page3.titleImage = (isiPad)?[UIImage imageNamed:@"iPad-Story3"]:[UIImage imageNamed:@"iPhone-Story3"];
    if (isiPad) {
//        page3.titlePositionY = iPadTitleYCoord;
//        page3.descPositionY = iPadDescYCoord;
        page3.titleFont = titleFont;
        page3.descFont = descFont;
    }
    else {
//        page3.titlePositionY = (isiPhoneFourInch)?fourInchTitleYCoord:threePointFiveInchTitleYCoord;
//        page3.descPositionY = (isiPhoneFourInch)?fourInchDescYCoord:threePointFiveInchDescYCoord;
    }
    
    EAIntroPage *page4 = [EAIntroPage page];
    page4.title = @"Stay connected";
    page4.titleColor = titleColor;
    page4.desc = sampleDesc4;
    page4.descColor = titleColor;
    page4.bgImage = bg;
//    page4.titleImage = (isiPad)?[UIImage imageNamed:@"iPad-Story5"]:[UIImage imageNamed:@"iPhone-Story4"];
    if (isiPad) {
//        page4.titlePositionY = iPadTitleYCoord;
//        page4.descPositionY = iPadDescYCoord;
        page4.titleFont = titleFont;
        page4.descFont = descFont;
    }
    else {
//        page4.titlePositionY = (isiPhoneFourInch)?fourInchTitleYCoord:threePointFiveInchTitleYCoord;
//        page4.descPositionY = (isiPhoneFourInch)?fourInchDescYCoord:threePointFiveInchDescYCoord;
    }
    
    EAIntroPage *page5 = [EAIntroPage page];
    page5.title = @"Grading on the go";
    page5.titleColor = titleColor;
    page5.desc = sampleDesc5;
    page5.descColor = titleColor;
    page5.bgImage = bg;
//    page5.titleImage = [UIImage imageNamed:@"iPad-Story4"];
    if (isiPad) {
//        page5.titlePositionY = iPadTitleYCoord;
//        page5.descPositionY = iPadDescYCoord;
        page5.titleFont = titleFont;
        page5.descFont = descFont;
    }
    
    
    EAIntroView *intro = [[EAIntroView alloc] init];
    if (isiPad) {
        // 4th Page is iPad only. Page 4 on iPad is page5 in code. Page 5 is page4 on iPad and Page 4 on iPhone
        intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2,page3,page5,page4]];
    }
    else {
        intro = [[EAIntroView alloc] initWithFrame:self.view.bounds andPages:@[page1,page2,page3,page4]];
    }
    
    [intro setDelegate:self];
    intro.skipButton.titleLabel.textColor = [UIColor darkGrayColor];
    
    [intro showInView:self.view animateDuration:0.3];
}

- (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
