//
//  ClassroomHeaderView.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 16/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ClassroomHeaderProtocol <NSObject>

@required
- (void)didTapOnClassroomSupplementaryViewButton:(UIButton *)sender;

@end

@interface ClassroomHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIButton *lessonButton;
@property (weak, nonatomic) IBOutlet UIButton *assessmentButton;

@property (strong, nonatomic) id <ClassroomHeaderProtocol> protocol;

- (void)styleView;

@end
