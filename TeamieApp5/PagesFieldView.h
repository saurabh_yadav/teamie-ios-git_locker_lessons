//
//  PagesFieldView.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 1/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PagesFieldView : UIView
@property (weak, nonatomic) IBOutlet UITableView *pagesTableView;

@end
