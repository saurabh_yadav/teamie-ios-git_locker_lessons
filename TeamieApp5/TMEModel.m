//
//  TeamieModel.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEModel.h"

@implementation TMEModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{};
}

+ (instancetype)parseDictionary:(NSDictionary *)jsonObject {
    if (jsonObject == nil || jsonObject.count == 0) {
        return nil;
    }
    NSError *error;
    return [MTLJSONAdapter modelOfClass:self.class
                     fromJSONDictionary:jsonObject
                                  error:&error];
}

+ (NSArray *)parseList:(id)jsonObjects {
    if ([jsonObjects isKindOfClass:NSArray.class]) {
        return [self parseArray:jsonObjects];
    }
    else if ([jsonObjects isKindOfClass:NSDictionary.class]) {
        return [self parseDictionaryAsArray:jsonObjects];
    }
    else {
        //This should not happen, but in case the API returns an incorrect response
        //This will cause it to fail gracefully
        return nil;
    }
}
+ (NSArray *)parseArray:(NSArray *)jsonObjects {
    if (jsonObjects == nil || jsonObjects.count == 0) {
        return nil;
    }
    NSError *error;
    return [MTLJSONAdapter modelsOfClass:self.class
                           fromJSONArray:jsonObjects
                                   error:&error];
}

+ (NSArray *)parseDictionaryAsArray:(NSDictionary *)jsonObjects {
    if (jsonObjects == nil || jsonObjects.count == 0) {
        return nil;
    }
    NSMutableArray *objects = [NSMutableArray arrayWithCapacity:jsonObjects.count];
    NSArray *temp = jsonObjects.allValues;
    for (NSDictionary *dict in temp) {
        [objects addObject:[self parseDictionary:dict]];
    }
    
    return objects;
}

+ (NSValueTransformer *)numberJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^NSNumber *(NSString *str) {
        return @(str.longLongValue);
    } reverseBlock:^NSString *(NSNumber *number) {
        return number.stringValue;
    }];
}

+ (NSValueTransformer *)boolJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^NSNumber *(id inp) {
        return @([inp boolValue]);
    } reverseBlock:^NSString *(NSNumber *number) {
        return number.stringValue;
    }];
}

+ (NSValueTransformer *)dateJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^NSDate *(id date) {
        if ([date isKindOfClass:NSString.class]) {
            return [NSDate dateWithTimeIntervalSince1970:[((NSString *)date) longLongValue]];
        }
        else if ([date isKindOfClass:NSNumber.class]){
            return [NSDate dateWithTimeIntervalSince1970:[((NSNumber *)date) longLongValue]];
        }
        else if([date isKindOfClass:NSDictionary.class]){
            NSTimeInterval publishTime = [[date valueForKey:@"on"] doubleValue];
            return [NSDate dateWithTimeIntervalSince1970: publishTime];
        }
        else {
            return nil;
        }
    } reverseBlock:^NSString *(NSDate *dateValue) {
        return [NSString stringWithFormat:@"%f", [dateValue timeIntervalSince1970]];
    }];
}

+ (NSValueTransformer *)timezoneJSONTransformer {
    return [MTLValueTransformer reversibleTransformerWithForwardBlock:^NSTimeZone *(NSString *str) {
        return [NSTimeZone timeZoneWithName:str];
    } reverseBlock:^NSString *(NSTimeZone *timezone) {
        return [timezone name];
    }];
}

+ (NSValueTransformer *)dictionaryListJSONTransformer {
    return [MTLValueTransformer transformerWithBlock:^ id (id responseObject) {
        if (responseObject == nil) return nil;
        if ([responseObject isKindOfClass:NSDictionary.class]) {
            return ((NSDictionary *)responseObject).allValues;
        }
        else if ([responseObject isKindOfClass:NSArray.class]) {
            return responseObject;
        }
        else {
            return nil;
        }
    }];
}

+ (NSValueTransformer *)listJSONTransformer:(Class)modelClass {
    return [MTLValueTransformer transformerWithBlock:^ id (id responseObject) {
        if (responseObject == nil) return nil;
        NSError *error;
        if ([responseObject isKindOfClass:NSDictionary.class]) {
            NSDictionary *response = (NSDictionary *)responseObject;
            if (response.count == 0) return nil;
            NSMutableArray *objects = [NSMutableArray arrayWithCapacity:response.count];
            for (NSDictionary *dict in response.allValues) {
                [objects addObject:[MTLJSONAdapter modelOfClass:modelClass
                                             fromJSONDictionary:dict
                                                          error:&error]];
            }
            return objects;
        }
        else if ([responseObject isKindOfClass:NSArray.class]) {
            return [MTLJSONAdapter modelsOfClass:modelClass
                                   fromJSONArray:responseObject
                                           error:&error];
        }
        else {
            return nil;
        }
    }];
}

+ (void)storeToUserDefaults:(NSArray *)objects {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:objects];
    [[NSUserDefaults standardUserDefaults] setObject:data forKey:[self getUserDefaultsKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSArray *)getFromUserDefaults {
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:[self getUserDefaultsKey]];
    return (NSArray *)[NSKeyedUnarchiver unarchiveObjectWithData:data];
}

+ (void)deleteFromUserDefaults {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:[self getUserDefaultsKey]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)getUserDefaultsKey {
    return [NSString stringWithFormat:@"%@Entity", self.class];
}

@end
