//
//  UserpointActivityItem.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 12/14/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "UserpointActivityItem.h"

@implementation UserpointActivityItem

@synthesize daypoints, timestamp, pointday, totalpoints;

- (NSString*)description {

    return [NSString stringWithFormat:@"%f points on %f (Total: %f)", [self.daypoints doubleValue], [self.timestamp doubleValue], [self.totalpoints doubleValue]];
    
}

- (NSDate*)pointday {
    return [NSDate dateWithTimeIntervalSince1970:[self.timestamp doubleValue]];
}

@end
