//
//  LPTOCTableViewCell.m
//  LS2
//
//  Created by Wei Wenbo on 4/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import "LPTOCTableViewCell.h"
@interface SWTableViewCell()
@property (nonatomic, strong) UIScrollView *cellScrollView;
@end
@implementation LPTOCTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.cellScrollView.showsHorizontalScrollIndicator = NO;
    }
    
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
