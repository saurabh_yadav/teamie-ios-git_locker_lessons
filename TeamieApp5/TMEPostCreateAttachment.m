//
//  TMEPostCreateAttachment.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 10/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostCreateAttachment.h"

#import "TMEUser.h"

#define ATTACHMENT_THUMBNAIL_SIZE 33.0

@implementation TMEPostCreateAttachment

@end

@implementation TMEPostCreateLinkAttachment

- (NSString *)title {
    return self.url;
}

- (UIImage *)thumbnail {
    return [TeamieUIGlobals defaultPicForPurpose:@"link" withSize:ATTACHMENT_THUMBNAIL_SIZE];
}

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
         @"title": NSNull.null,
         @"thumbnail": NSNull.null
     };
}


@end

@implementation TMEPostCreateMediaAttachment

- (NSString *)hashedName {
    if (_hashedName == nil) {
        NSString *md5Input = [NSString stringWithFormat:@"%@|%lld|%u",
                          [TMEUser currentUser].uid,
                          (long long)[[NSDate date] timeIntervalSince1970],
                          arc4random_uniform(100000)];
        
        if ([self isKindOfClass:TMEPostCreateAudioAttachment.class]) {
            _hashedName = [NSString stringWithFormat:@"%@.%@", [TeamieGlobals md5:md5Input], @"mp3"];
        } else {
            _hashedName = [NSString stringWithFormat:@"%@.%@", [TeamieGlobals md5:md5Input], [self.name pathExtension]];
        }
    }
    return _hashedName;
}

- (NSString *)title {
    return self.name;
}

- (UIImage *)thumbnail {
    return [TeamieUIGlobals defaultPicForPurpose:@"attachment" withSize:ATTACHMENT_THUMBNAIL_SIZE];
}

@end

@implementation TMEPostCreateImageAttachment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"fileSize": @"file_size",
             @"fileMime": @"filemime",
             @"fileURL": NSNull.null,
             @"image": NSNull.null,
             @"title": NSNull.null,
             @"thumbnail": NSNull.null,
             @"hashedName": @"name",
             @"name": NSNull.null
    };
}

- (UIImage *)thumbnail {
    return self.image;
}


@end

@implementation TMEPostCreateVideoAttachment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"fileSize": @"file_size",
        @"fileMime": @"filemime",
        @"fileURL": NSNull.null,
        @"image": NSNull.null,
        @"title": NSNull.null,
        @"thumbnail": NSNull.null,
        @"hashedName": NSNull.null,
    };
}

- (UIImage *)thumbnail {
    return [TeamieUIGlobals defaultPicForPurpose:@"video" withSize:ATTACHMENT_THUMBNAIL_SIZE];
}

@end

@implementation TMEPostCreateAudioAttachment

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"fileSize": @"file_size",
             @"fileMime": @"filemime",
             @"fileURL": NSNull.null,
             @"image": NSNull.null,
             @"title": NSNull.null,
             @"thumbnail": NSNull.null,
             @"hashedName": NSNull.null,
             };
}

- (UIImage *)thumbnail {
    return [TeamieUIGlobals defaultPicForPurpose:@"audio" withSize:ATTACHMENT_THUMBNAIL_SIZE];
}

@end