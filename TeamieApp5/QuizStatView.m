//
//  QuizStatView.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 10/23/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "QuizStatView.h"

@implementation QuizStatView

- (id)init {
    // load the view from the XIB file
    self = [[[NSBundle mainBundle] loadNibNamed:@"QuizStatView" owner:self options:nil] objectAtIndex:0];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [[[NSBundle mainBundle] loadNibNamed:@"QuizStatView" owner:self options:nil] objectAtIndex:0];
    
    if (self) {
        // Initialization code
        [self setFrame:frame];
    }
    return self;
}

@end
