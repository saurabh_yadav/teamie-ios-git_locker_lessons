//
//  QuizStatsViewController.m
//  TeamieApp5
//
//  Created by Raunak on 28/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuizStatsViewController.h"
#import "QuizStatCellView.h"
#import <QuartzCore/QuartzCore.h>

@interface QuizStatsViewController ()

@property (nonatomic, strong) UIToolbar* toolbar;
@end

@implementation QuizStatsViewController

- (id)initWithQuizStatistics:(QuizStatistics *)stats andTitle:(NSString *)title {
    self = [super init];
    if (self) {
        _stats = stats;
        _quizTitle = title;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, 44.0)];
    self.toolbar.barStyle = UIBarStyleDefault;
    self.toolbar.tintColor = [UIColor colorWithHex:@"#BE202E" alpha:1.0];
    
    UILabel* titleLabel = [[UILabel alloc] initWithFrame:self.toolbar.bounds];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:20.0];
    titleLabel.text = self.quizTitle;
    titleLabel.adjustsFontSizeToFitWidth = YES;
    
    [self.toolbar addSubview:titleLabel];
    [self.view addSubview:self.toolbar];
    
    QuizStatCellView* cell1 = [QuizStatCellView cellWithText:@"Total Score" value:self.stats.totalScore baseValue:self.stats.totalScore];
    cell1.frame = CGRectMake(20, 60, 500, 120);
    
    QuizStatCellView* cell2 = [QuizStatCellView cellWithText:@"Max Score" value:self.stats.maxScore baseValue:self.stats.totalScore];
    cell2.frame = CGRectMake(20, 200, 500, 120);

    QuizStatCellView* cell3 = [QuizStatCellView cellWithText:@"Min Score" value:self.stats.minScore baseValue:self.stats.totalScore];
    cell3.frame = CGRectMake(20, 340, 500, 120);
    
    QuizStatCellView* cell4 = [QuizStatCellView cellWithText:@"Average Score" value:self.stats.averageScore baseValue:self.stats.totalScore];
    cell4.frame = CGRectMake(20, 480, 500, 120);
    
    [self.view addSubview:cell1];
    [self.view addSubview:cell2];
    [self.view addSubview:cell3];
    [self.view addSubview:cell4];
}

- (void)viewDidAppear:(BOOL)animated {
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapBehind:)];
    
    [recognizer setNumberOfTapsRequired:1];
    recognizer.cancelsTouchesInView = NO; //So the user can still interact with controls in the modal view
    [self.view.window addGestureRecognizer:recognizer]; 
}

- (void)handleTapBehind:(UITapGestureRecognizer *)sender
{
    if (sender.state == UIGestureRecognizerStateEnded)
    {
        CGPoint location = [sender locationInView:nil]; //Passing nil gives us coordinates in the window
        
        //Then we convert the tap's location into the local view's coordinate system, and test to see if it's in or outside. If outside, dismiss the view.
        
        if (![self.view pointInside:[self.view convertPoint:location fromView:self.view.window] withEvent:nil])
        {
            // Remove the recognizer first so it's view.window is valid.
            [self.view.window removeGestureRecognizer:sender];
            [self dismissModalViewControllerAnimated:YES];
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
