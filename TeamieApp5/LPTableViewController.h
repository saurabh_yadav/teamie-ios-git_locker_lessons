//
//  LPTableViewController.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 17/6/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TableRestApiViewController.h"
#import "LessonObject.h"
#import "LessonPageViewController.h"

@protocol LPTableViewControllerDelegate
- (void)navigateToLessonPageViewController:(LessonPageViewController *)lessonPageViewController;
@end

@interface LPTableViewController : TableRestApiViewController

//Methods
- (id)initWithLesson:(LessonObject *)lesson;

//Properties
@property (weak) id<LPTableViewControllerDelegate> delegate;
@property (readonly) NSArray *lessonPages;

@end
