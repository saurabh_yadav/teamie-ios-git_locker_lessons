//
//  TMEPostPollAttachmentViewController.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 02/01/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import "TMEPostPollAttachmentViewController.h"
#import "UIView+TMEEssentials.h"
#import "TeamieUIGlobalColors.h"
#import "TMEPollAttachmentCell.h"
#import "TMEPollOption.h"
#import "TMEClient.h"
#import "TMEPost.h"

#import <Masonry/Masonry.h>
#import <SpinKit/RTSpinKitView.h>

static CGFloat kCellHeight = 50.0f;
static CGFloat kDefaultPadding = 10.0f;

@interface TMEPostPollAttachmentViewController ()

@property (nonatomic, strong) TMEPollAttachment *poll;
@property (nonatomic, strong) NSNumber *thoughtID;

@property (nonatomic, strong) UIView *submitPoll;
@property (nonatomic, weak) UIView *titleView;
@property (nonatomic, weak) UILabel *titleLabel;

@property (nonatomic, weak) UIView *submitView;
@property (nonatomic, weak) UIButton *submitButton;

#pragma mark - Constraints

@property (nonatomic, weak) MASConstraint *submitPollViewHeight;
@property (nonatomic, weak) MASConstraint *rightViewWidthConstraint;
@property (nonatomic, weak) MASConstraint *labelWidthConstraint;
@property (nonatomic, weak) MASConstraint *labelLeftConstraint;

@end

@implementation TMEPostPollAttachmentViewController

static NSString * const kReuseIdentifier = @"polloption";

#pragma mark - Lifecycle

- (instancetype)initWithPoll:(TMEPollAttachment *)poll postID:(NSNumber *)tid {
    self = [super init];
    if (self) {
        self.poll = poll;
        self.thoughtID = tid;
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:TMEPollAttachmentCell.class forCellReuseIdentifier:kReuseIdentifier];
    
    self.poll.selectedPollOptions = (!self.poll.selectedPollOptions)?[NSMutableArray array]:self.poll.selectedPollOptions;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.poll.choiceCount.integerValue;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return kCellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TMEPollAttachmentCell *cell = [self.tableView dequeueReusableCellWithIdentifier:kReuseIdentifier forIndexPath:indexPath];
    TMEPollOption *pollOption = self.poll.choices[indexPath.row];
    [cell loadPollAttachment:pollOption withOptions:self.poll];
    
    cell.backgroundColor = ([self.poll.selectedPollOptions containsObject:[NSNumber numberWithInteger:indexPath.row]])? [TeamieUIGlobalColors pollOptionHighlightedLabelBackgroundColor]: UIColor.clearColor;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!self.poll.isAllowedToVote) {
        BOOL hasVotedAlready = NO;
        for (TMEPollOption *option in self.poll.choices) {
            hasVotedAlready = (hasVotedAlready + option.currentUserVote);
        }
        if (hasVotedAlready) {
            [TeamieGlobals addHUDTextOnlyLabel:TMELocalize(@"post.poll.voted_already") afterDelay:2.0];
        }
        else {
            [TeamieGlobals addHUDTextOnlyLabel:TMELocalize(@"post.poll.no_permission") afterDelay:2.0];
        }
        return nil;
    }
    return indexPath;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.poll.didSubmitRequest)
        return;
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if ([self.poll.selectedPollOptions containsObject:[NSNumber numberWithInteger:indexPath.row]]) {
        [self.poll.selectedPollOptions removeObject:[NSNumber numberWithInteger:indexPath.row]];
    }
    else {
        if (self.poll.pollType == PollTypeMultiple) {
            [self.poll.selectedPollOptions addObject:[NSNumber numberWithInteger:indexPath.row]];
        }
        else {
            self.poll.selectedPollOptions = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithInteger:indexPath.row], nil];
        }
    }
    [tableView reloadData];
    
    self.poll.isFooterVisible = (self.poll.selectedPollOptions.count)? YES: NO;
    
    if([self.delegate conformsToProtocol:@protocol(TMEPollAttachmentDelegate)]) {
        [self.delegate performSelector:@selector(updatePollAttachmentViewHeightWithPoll:) withObject:self.poll];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 23.f;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return (self.poll.pollType == PollTypeSingle)? TMELocalize(@"post.poll.single_choice"): TMELocalize(@"post.poll.multiple_choice");
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 23.f)];
    header.backgroundColor = [TeamieUIGlobalColors headerViewBackgroundColor];
    
    UILabel *pollTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(kDefaultPadding, 0, CGRectGetWidth(header.frame), CGRectGetHeight(header.frame))];
    pollTypeLabel.text = (self.poll.pollType == PollTypeSingle)? TMELocalize(@"post.poll.single_choice"): TMELocalize(@"post.poll.multiple_choice");
    pollTypeLabel.font = [TeamieGlobals appFontFor:@"boldFontWithSize12"];
    pollTypeLabel.textColor = [TeamieUIGlobalColors grayColor];
    [header addSubview:pollTypeLabel];
    
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return (self.poll.selectedPollOptions.count)? kCellHeight: 0.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [self addSubmitPollView];
}

#pragma mark - Submit Poll View

- (UIView *)addSubmitPollView {
    self.submitPoll = [[UIView alloc] initWithFrame:CGRectZero];
    [self.submitPoll addBorderToTop:YES bottom:NO left:NO right:NO];
    [self addRightView];
    [self addMainView];
    [self addConstraints];
    [self loadSubmitCellData];
    
    return self.submitPoll;
}

- (void)addRightView {
    self.submitView = [self.submitPoll createAndAddSubView:UIView.class];
    self.submitButton = [self.submitView createAndAddSubView:UIButton.class];
}

- (void)addMainView {
    self.titleView = [self.submitPoll createAndAddSubView:UIView.class];
    
    self.titleLabel = [self.titleView createAndAddSubView:UILabel.class];
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.titleLabel.contentMode = UIViewContentModeCenter;
}

- (void)addConstraints {
    [self.submitView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(self.submitPoll);
        make.height.equalTo(@(kCellHeight));
        self.rightViewWidthConstraint = make.width.equalTo(@(kCellHeight));
    }];
    [self.submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.submitView);
    }];
    
    [self.titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.submitPoll);
        make.left.equalTo(self.submitPoll);
        make.right.equalTo(self.submitView.mas_left);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.titleView).with.offset(kDefaultPadding);
        make.right.equalTo(self.titleView).with.offset(-kDefaultPadding);
    }];
}

- (void)loadSubmitCellData {
    self.titleLabel.text = TMELocalize(@"post.poll.submit_poll");
    self.titleLabel.font = [TeamieGlobals appFontFor:@"boldFontWithSize13"];
    
    self.submitButton.backgroundColor = [TeamieUIGlobalColors pollOptionSubmitButtonColor];
    [self.submitButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"checkmark" withSize:17.f andColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.submitButton addTarget:self action:@selector(submitData) forControlEvents:UIControlEventTouchUpInside];
}

- (void)submitData {
    self.submitButton.enabled = NO;
    self.poll.didSubmitRequest = YES;
    self.titleLabel.text = TMELocalize(@"post.poll.submitting_poll");
    
    RTSpinKitView *spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleBounce color:[UIColor whiteColor] spinnerSize:30.f];
    [self.submitView addSubview:spinner];
    [spinner mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.submitView);
    }];
    self.submitButton.enabled = NO;
    [self.submitButton setImage:nil forState:UIControlStateNormal];
    
    NSMutableDictionary *parameters = [NSMutableDictionary new];
    parameters[@"tid"] = self.thoughtID;
    
    NSString *choices = @"";
    for (NSNumber *index in self.poll.selectedPollOptions) {
        choices = [choices stringByAppendingString:[NSString stringWithFormat:@"%ld,", index.integerValue + 1]];
    }
    parameters[@"choices"] = choices;
    
    [[TMEClient sharedClient] request:TeamieUserSubmitPollRequest parameters:parameters loadingMessage:TMELocalize(@"message.loading")
    completion:^{
        self.poll.isFooterVisible = NO;
        self.poll.didSubmitRequest = NO;
        if([self.delegate conformsToProtocol:@protocol(TMEPollAttachmentDelegate)]) {
            [self.delegate performSelector:@selector(updatePollAttachmentViewHeightWithPoll:) withObject:self.poll];
        }
        [self.poll.selectedPollOptions removeAllObjects];
        [self.tableView reloadData];
    } success:^(id response) {
        TMEPost *updatedPost = [TMEPost parseDictionary:response];
        self.poll = updatedPost.poll;
    } failure:^(NSError *error) {
        
    }];
}

@end