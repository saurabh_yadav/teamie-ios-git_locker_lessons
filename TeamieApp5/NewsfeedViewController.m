//
//  NewsfeedViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/23/12.
//  Copyright (c) 2012 ETH. All rights reserved.
//

#import <objc/runtime.h>
#import "NewsfeedViewController.h"
#import "UserProfileViewController.h"
#import "BulletinBoardViewController.h"
#import "MessagesListViewController.h"
#import "TeamieWebViewController.h"

#import "UIBarButtonItem+WEPopover.h"

#import "NewsfeedItem.h"
#import "Classroom.h"

#import "TTTableCheckedTextItem.h"
#import "TTTableCheckedTextItemDataSource.h"

#import "TTTableTextUniqueItem.h"

#import "ThoughtViewController.h"
#import "TeamieListViewController.h"
#import "ThoughtImageViewController.h"
#import "TeamieChartsViewController.h"

#import "TTTableNewsfeedItem.h"
#import "TTTableNewsfeedItemDataSource.h"

#import "TTTableBulletinItem.h"
#import "ResourcesViewController.h"
#import "AudioToolbox/AudioServices.h"   // To access vibration methods
#import "ClassroomMenu.h"

// define a class to hold the MosaicTileInfo data
@interface MosaicTileInfo : NSObject
@property (nonatomic, retain) NSNumber* positionX;
@property (nonatomic, retain) NSNumber* positionY;
@property (nonatomic, retain) NSNumber* frameWidth;
@property (nonatomic, retain) NSNumber* frameHeight;
@end
@implementation MosaicTileInfo
@synthesize positionX, positionY, frameWidth, frameHeight;
@end

@interface NewsfeedViewController() {
    BOOL messageReload;
}

// Private Properties:
@property (retain, nonatomic) UIPanGestureRecognizer *navigationBarPanGestureRecognizer;
@property (nonatomic, strong) ClassroomMenu* classroomMenuMapping;

// Private methods:
-(void)addClassroomContentButton;
-(void)userNewsfeedDidLoad:(id)newsfeed;
-(void)userNewsfeedFiltersDidLoad:(id)filters;
-(void)userBulletinBoardDidLoad:(id)bulletinBoard;
-(void)userMessageListDidLoad:(id)messageList;
- (WEPopoverContainerViewProperties *)improvedContainerViewProperties;
-(void)customizeNewsfeedView;
-(void)newsfeedItemCellGroupSelected:(id)sender;
-(void)showNewsfeedAsMosaic;
-(NSArray*)generateRandomLayoutForElements:(NSUInteger)numElements forGrid:(NSUInteger)numDivisions;

@end

@implementation NewsfeedViewController

@synthesize bulletinBoardNotificationCount;
@synthesize messagesNotificationCount;
//@synthesize newsfeedFilterButton;
@synthesize bottomToolbar;
@synthesize newsfeedNavigationBar;
@synthesize popoverController, shareBox, userProfile;
@synthesize wePopoverController, bulletinBoardController;
@synthesize tempUserMenus, userNewsfeedFilterSelected;
@synthesize newsfeedItems;
@synthesize newsfeedClassroomID;
@synthesize filterStatus = _filterStatus;
@synthesize newsfeedMosaicView;
@synthesize navigationBarPanGestureRecognizer;

- (id)initWithClassroom:(NSInteger)classroomID revealController:(id)revealController {

    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:[TeamieGlobals storyboardIdentifier] bundle:nil];
    self = [storyboard instantiateViewControllerWithIdentifier:@"NewsfeedViewController"];
    if (self) {
        if (classroomID != TEAMIE_DEFAULT_ID_NOT_DEFINED) {
            // do something with classroomID that's given
            self.newsfeedClassroomID = [NSNumber numberWithInteger:classroomID];
            self.tableViewStyle = UITableViewStylePlain;
#ifdef DEBUG
            NSLog(@"Newsfeed View Controller instantiated for classroomID: %d", classroomID);
#endif
        }
        self.apiCaller.delegate = self;
        self.dataSource = nil;
        self.parentRevealController = revealController;
        messageReload = NO;
    }
    return self;
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.apiCaller.delegate = self;
        self.dataSource = nil;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        popoverClass = [UIPopoverController class];
    }
    else {
        popoverClass = [WEPopoverController class];
    }
    
    // Add a filter status label
    self.filterStatus.text = [NSString stringWithFormat:@""];
    
    // bring the navigation bar to the front, since when the filter status label animates, it must appear from behind the navigation bar
    [self.view bringSubviewToFront:self.newsfeedNavigationBar];
    
    // customize the TTTableViewController view for showing the Newsfeed (you know, navigation bar on top & toolbar at the bottom)
    [self customizeNewsfeedView];
    
    if (self.newsfeedClassroomID) { // if a classroom ID is set
        
        // Find out the name of the current classroom from the local database
        // in which all classrooms of the current user are stored
        NSPredicate* predicate = [NSPredicate predicateWithFormat:@"(nid == %qi)", [self.newsfeedClassroomID longLongValue]];
        NSArray* matchingClassrooms = [self.apiCaller getStoredEntitiesOfClass:[Classroom class] withPredicate:predicate];
        
        if ([matchingClassrooms count] >= 1) {
            self.newsfeedNavigationBar.topItem.title = [[matchingClassrooms objectAtIndex:0] valueForKey:@"name"];
        }
        
        // add a Bar button to the bottom toolbar for displaying Classroom Content popover
//        [self addClassroomContentButton];
        
        // differentiate newsfeed and whiteboard with navigationBar.title
        [self.navigationItem.title isEqualToString:[[matchingClassrooms objectAtIndex:0] valueForKey:@"name"]];
        
        [self.apiCaller performRestRequest:TeamieUserClassroomMenuRequest withParams:[NSDictionary dictionaryWithObject:self.newsfeedClassroomID forKey:@"nid"]];
    }
    
    // Make the bulletin board request (to know how many unread bulletin notifications the current user has
    [self.apiCaller performRestRequest:TeamieUserBulletinBoardCountRequest withParams:nil];
    // Make the Teamie user profile request for currently logged-in user
    [self.apiCaller performRestRequest:TeamieUserProfileRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%qi", [self.apiCaller.currentUser.uid longLongValue]], @"uid", nil]];
    // Make the user messages request
    [self.apiCaller performRestRequest:TeamieUserMessagesCountRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:NUMBER_OF_MESSAGE_THREADS, @"items_per_page", nil]];
    
    // adjust UILabel settings
    self.bulletinBoardNotificationCount.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"badge-bg.png"]];
    
    // Shows up the loading activity indicator within the tableView
    [self showLoading:YES];
    
    // Add a filter button to the navigation bar
    UIBarButtonItem *anotherButton = [[UIBarButtonItem alloc] initWithImage:[TeamieUIGlobals defaultPicForPurpose:@"filter" withSize:20.0f]
                                                                 style:UIBarButtonItemStyleBordered
                                                                target:self
                                                                action:@selector(newsfeedFilterOpen:)];
    
    if (self.navigationController) {
        self.navigationItem.rightBarButtonItem = anotherButton;
    }
    else {
        NSMutableArray* rightBarButtons = [NSMutableArray arrayWithArray:self.newsfeedNavigationBar.topItem.rightBarButtonItems];
        [rightBarButtons addObject:anotherButton];
        
        [self.newsfeedNavigationBar.topItem setRightBarButtonItems:rightBarButtons animated:YES];
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidReceiveNotificationWhenActive:)
                                                 name:@"UIApplicationDidReceiveNotificationWhenActive"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendPasteboardUrlToShareBox:)
                                                 name:@"pasteboardUrlToShareBox"
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(messageSentSuccessfully:) name:@"MessageSentNotification"
                                               object:nil];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    // initialize the coachMarksView
    self.coachMarksView = [TeamieUIGlobals initializeCoachMarksView:NSStringFromClass([self class]) forView:self.view forElements:nil];
    
    [self.coachMarksView start];
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"%@%@", COACH_MARKS_SHOWN_PREFIX, NSStringFromClass([self class])]];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self becomeFirstResponder];
    if (!self.newsfeedClassroomID) {    // to track newsfeed pageview
        [[GANTracker sharedTracker] trackPageview:@"/newsfeed" withError:nil];
    }
    else {  // to track the particular classroom whose newsfeed is being viewed
        [[GANTracker sharedTracker] trackPageview:[NSString stringWithFormat:@"/newsfeed/%qi", [self.newsfeedClassroomID longLongValue]] withError:nil];
    }
    
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"launch_thought_link"]) {
        
        NSLog(@"Attached link on launch %@", [[NSUserDefaults standardUserDefaults] stringForKey:@"launch_thought_link"]);
        
        // save the link as cached link attached to a thought
        [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] stringForKey:@"launch_thought_link"] forKey:@"cache_thought_attached_link"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"launch_thought_link"];
        
        // invoke the segue to transition to the thought share box
        [self performSegueWithIdentifier:@"showShareBox" sender:self];
    }
}

- (void)viewDidUnload
{
    [self setBulletinBoardNotificationCount:nil];
    
    [NewsfeedItem deleteAllObjects];
    //[self setNewsfeedFilterButton:nil];
    
    self.shareBox = nil;
    self.userProfile = nil;
    [self.popoverController dismissPopoverAnimated:NO];
    self.popoverController = nil;
    [self.wePopoverController dismissPopoverAnimated:NO];
    self.wePopoverController = nil;
    self.bulletinBoardController = nil;
    
    self.newsfeedClassroomID = nil;
    self.newsfeedMosaicView = nil;
    self.navigationBarPanGestureRecognizer = nil;
    
    [self setBottomToolbar:nil];
    [self setNewsfeedNavigationBar:nil];

    [self setMessagesNotificationCount:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark - IBAction methods
- (IBAction)shareButtonPressed:(id)sender {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        if (self.popoverController.isPopoverVisible) {  // the popover is already visible
            [self.popoverController dismissPopoverAnimated:YES];
            return;
        }
    }

    [self performSegueWithIdentifier:@"showShareBox" sender:sender];
    [[GANTracker sharedTracker] trackEvent:@"Thought Share" action:@"Thought Share Open" label:nil value:0 withError:nil];

}

- (IBAction)bulletinBoardSelected:(id)sender {
    if (!self.wePopoverController) {
        // the userBulletinBoardDidLoad method will display the popover
        // the notifications to display will be loaded by the BulletinBoardViewController
        // look at the viewDidLoad method of that class
        [self userBulletinBoardDidLoad:nil];
        [[GANTracker sharedTracker] trackEvent:@"Bulletin Board" action:@"Bulletin Board Open" label:nil value:0 withError:nil];
	} 
    else {
		[self.wePopoverController dismissPopoverAnimated:YES];
		self.wePopoverController = nil;
	}
}

- (IBAction)messagesIconSelected:(id)sender {
    if (!messageReload) {
        [self.apiCaller performRestRequest:TeamieUserMessagesRequest withParams:nil];
    }
    else {
        [self.apiCaller performRestRequest:TeamieUserMessagesRequest withParams:[NSDictionary dictionaryWithObject:@"YES" forKey:@"reload"]];
        messageReload = NO;
    }
    
    [[GANTracker sharedTracker] trackEvent:@"Messages" action:@"Messages List Open" label:nil value:0 withError:nil];
    
}

- (IBAction)newsfeedFilterOpen:(id)sender {
    if (!self.wePopoverController) {
        // get the newsfeed filters to display
        [TeamieGlobals cancelTeamieRestRequests];
        [self.apiCaller performRestRequest:TeamieUserNewsfeedFilterRequest withParams:nil];
        [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Newsfeed Filter Open" label:nil value:0 withError:nil];
    }
    else {
        [self.wePopoverController dismissPopoverAnimated:YES];
        self.wePopoverController = nil;
    }
}

- (IBAction)classroomContentMenuOpen:(id)sender event:(UIEvent*)event {
    
    if (!self.wePopoverController) {
        
        PopupMenuViewController *contentViewController = [[PopupMenuViewController alloc] initWithStyle:UITableViewStylePlain];
        
        // populate the popover menu
        NSMutableArray * entries = [[NSMutableArray alloc] init];
        
        if (self.classroomMenuMapping.lessonTitle != nil && ![self.classroomMenuMapping.lessonTitle isEqualToString:@""]) {
            [entries addObject:[TTTableTextItem itemWithText:self.classroomMenuMapping.lessonTitle]];
        }
        if (self.classroomMenuMapping.quizTitle != nil && ![self.classroomMenuMapping.quizTitle isEqualToString:@""]) {
            [entries addObject:[TTTableTextItem itemWithText:self.classroomMenuMapping.quizTitle]];
        }
        if (self.classroomMenuMapping.memberTitle != nil && ![self.classroomMenuMapping.memberTitle isEqualToString:@""]) {
            [entries addObject:[TTTableTextItem itemWithText:self.classroomMenuMapping.memberTitle]];
        }
        if (self.classroomMenuMapping.leaderboardTitle != nil && ![self.classroomMenuMapping.leaderboardTitle isEqualToString:@""]) {
            [entries addObject:[TTTableTextItem itemWithText:self.classroomMenuMapping.leaderboardTitle]];
        }
        if (self.classroomMenuMapping.gradebookTitle != nil && ![self.classroomMenuMapping.gradebookTitle isEqualToString:@""]) {
            [entries addObject:[TTTableTextItem itemWithText:self.classroomMenuMapping.gradebookTitle]];
        }
        
        if ([entries count] > 0) {
            TTListDataSource * dataSource = [[TTListDataSource alloc] initWithItems:entries];
            contentViewController.dataSource = dataSource;
            contentViewController.menuDelegate = self;
            [contentViewController setNumMenuItems:entries.count];
            
            if (self.wePopoverController) {
                [self.wePopoverController dismissPopoverAnimated:YES];
                self.wePopoverController = nil;
            }
            
            self.wePopoverController = [[popoverClass alloc] initWithContentViewController:contentViewController];
            self.wePopoverController.delegate = self;
            
            // set the frame from which to display the popover
            CGRect showFrame = CGRectZero;
            for (UITouch* touch in event.allTouches) {
                if (touch.phase == UITouchPhaseEnded) {
                    showFrame = [[touch view] frame];
                    break;
                }
            }
            showFrame.origin.y = self.bottomToolbar.frame.origin.y + 5.0;
            
            [self.wePopoverController presentPopoverFromRect:showFrame inView:self.view permittedArrowDirections:(UIPopoverArrowDirectionDown) animated:YES];
        }
    }
    else {
        [self.wePopoverController dismissPopoverAnimated:YES];
        self.wePopoverController = nil;
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Irrespective of what segue is being performed, make sure there are no popovers hanging around
    [self.wePopoverController dismissPopoverAnimated:YES];
    self.wePopoverController = nil;
    
    //////////////////////////////////////
    // Segue to Post thought screen
    //////////////////////////////////////
    if ([[segue identifier] isEqualToString:@"showShareBox"]) {
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { // if device is an iPad
            // self.popoverController = [(UIStoryboardPopoverSegue*)segue popoverController];
        }
        
        if ([segue.destinationViewController isKindOfClass:[UINavigationController class]]) {
            self.shareBox = [[[segue destinationViewController] viewControllers] objectAtIndex:0];
        }
        else if ([segue.destinationViewController isKindOfClass:[ShareBoxViewController class]]) {
            self.shareBox = segue.destinationViewController;
        }
        else {
#ifdef DEBUG
            NSLog(@"Destination view controller is of unexpected type!");
#endif
            return;
        }
        
        [self.shareBox performSelector:@selector(updateDefaultClassroom:) withObject:self.newsfeedClassroomID];
        
        [self.shareBox setShareboxControllerDelegate:self];
        
    }
    //////////////////////////////////////
    // Segue to Thought View Screen
    //////////////////////////////////////
    else if ([[segue identifier] isEqualToString:@"showThoughtView"]) {
        
        NSNumber* thoughtID = nil;
        
        if ([sender isKindOfClass:[TTTableTextUniqueItem class]]) {
            thoughtID = ((TTTableTextUniqueItem*)sender).uniqueId;
        }
        else if ([sender isKindOfClass:[NewsfeedItem class]]) {
            thoughtID = ((NewsfeedItem*)sender).tid;
        }
        else if ([sender isKindOfClass:[TTTableNewsfeedItem class]]) {
            thoughtID = ((TTTableNewsfeedItem*)sender).tid;
        }
        else if ([sender isKindOfClass:[NSNumber class]]) {
            thoughtID = sender;
        }
        else if (UIEventSubtypeMotionShake) {
            // fetch NewsfeedItem objects stored locally
            NSArray* existingNewsfeedItems = [NewsfeedItem findAllInContext:[RKObjectManager sharedManager].objectStore.primaryManagedObjectContext];
            NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:NO];
            existingNewsfeedItems = [existingNewsfeedItems sortedArrayUsingDescriptors:[NSArray arrayWithObject:descriptor]];
            
            if ([existingNewsfeedItems count] >= 1) {
                // randomIndex for generating a random Index from the NewsfeedObjects count
                NSUInteger randomIndex = arc4random() % [existingNewsfeedItems count];
                NSLog(@"Random Index: %u", randomIndex);
                NewsfeedItem *thoughtItem = [existingNewsfeedItems objectAtIndex:randomIndex];
                NSLog(@"Loading Thought with tid: %@...", [thoughtItem tid]);
                thoughtID = [thoughtItem tid];
            }
        }
        
        ThoughtViewController* newController;
        
        // load thought view controller with the given thought
        if ([segue.destinationViewController isKindOfClass:[UINavigationController class]]) {
            newController = [[[segue destinationViewController] viewControllers] objectAtIndex:0];
        }
        else {
            newController = segue.destinationViewController;
        }
        
        [newController setThoughtViewDelegate:self];    // set the thought view delegate as self
        [newController loadThoughtWithTid:thoughtID reload:YES];    // always reload the thought (ensures latest comments & likes would be included)
    }
    //////////////////////////////////////
    // Segue to Thought Image View screen
    //////////////////////////////////////
    else if ([[segue identifier] isEqualToString:@"showAttachedImage"]) {
        NewsfeedItemAttachment* attachedImage = (NewsfeedItemAttachment*)sender;
        ThoughtImageViewController* newController;
        
        if ([segue.destinationViewController isKindOfClass:[UINavigationController class]]) {
            newController = ((ThoughtImageViewController*)[[[segue destinationViewController] viewControllers] objectAtIndex:0]);
        }
        else {
            newController = ((ThoughtImageViewController*)segue.destinationViewController);
        }
        
        // Removing [<email address>] from User Tag
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\[.+?\\]" options:NSRegularExpressionCaseInsensitive error:nil];
        attachedImage.newsfeedItem.message = [regex stringByReplacingMatchesInString:attachedImage.newsfeedItem.message options:0 range:NSMakeRange(0, [attachedImage.newsfeedItem.message length]) withTemplate:@""];
        
        // Removing "@" from User Tag
        attachedImage.newsfeedItem.message = [attachedImage.newsfeedItem.message stringByReplacingOccurrencesOfString:@"@" withString:@""];
        
        [newController loadImageWithUrl:attachedImage.href WithCaption:[NSString stringWithFormat:@"%@: %@", attachedImage.newsfeedItem.author.fullname, attachedImage.newsfeedItem.message] WithTitle:[NSString stringWithFormat:@"Shared by %@", attachedImage.newsfeedItem.author.fullname]];
    }
}

#pragma mark - Teamie REST API Delegate methods

-(void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {

    [TeamieGlobals dismissActivityLabels];
    
    if (requestName == TeamieUserBulletinBoardRequest || requestName == TeamieUserBulletinBoardCountRequest) {
        [self userBulletinBoardDidLoad:response];
    }
    else if (requestName == TeamieUserMessagesCountRequest) {   // only the count of new messages has been obtained
        if ([response isKindOfClass:[NSNumber class]]) {
            if ([response integerValue] != 0) {
                self.messagesNotificationCount.text = [response stringValue];
                [self.messagesNotificationCount setHidden:NO];
            }
        }
    }
    else if (requestName == TeamieUserMessagesRequest) {    // the message items have been obtained
        [self userMessageListDidLoad:response];
    }
    else if (requestName == TeamieUserNewsfeedFilterRequest) {
        [self userNewsfeedFiltersDidLoad:response];
    }
    else if (requestName == TeamieUserClassroomMenuRequest) {
        if ([response count] > 0) {
            // add a Bar button to the bottom toolbar for displaying Classroom Content popover
            [self addClassroomContentButton];
            self.classroomMenuMapping = [response objectAtIndex:0];
        }
    }
    else if (requestName == TeamieUserSameThoughtRequest || requestName == TeamieUserLikeThoughtRequest) {
        if ([response isKindOfClass:[NSString class]]) {
            [TeamieUIGlobals displayStatusMessage:response forPurpose:@"ok"];
        }
        else if ([response isKindOfClass:[NewsfeedItem class]]) {
            // To-do: Update the no. of likes on the screen
            NewsfeedItem* item = (NewsfeedItem*)response;
            [[((TTTableNewsfeedItemDataSource*)self.dataSource) dataModel] didUpdateObjectWithId:item.tid];
        }
    }
    else {
#ifdef DEBUG
        NSLog(@"Warning: Unhandled REST request in NewsfeedViewController");
#endif
    }
}

-(void)requestFailed:(NSString *)message forRequest:(TeamieRESTRequest)requestName {
    
    [TeamieGlobals dismissActivityLabels];
    
    if ([message isEqualToString:@"401"]) {
        // user login session expired
        // take user back to login screen
        [((UIViewController*)self.parentRevealController).navigationController popToRootViewControllerAnimated:YES];
    }
    else if (requestName == TeamieUserNewsfeedRequest || requestName == TeamieUserClassroomNewsfeedRequest) {
        // display an error message only if the TeamieUserNewsfeedRequest failed.
        [TeamieGlobals addTSMessageInController:self title:@"Error" message:message type:@"error" duration:5 withCallback:nil];
//        [TeamieGlobals addHUDTextOnlyLabel:message margin:10.f yOffset:10.f afterDelay:3];
    }
}

#pragma mark - Private methods
// the function that is called once the Newsfeed is loaded
// Also contains code to parse throught the JSON structure
-(void)userNewsfeedDidLoad:(id)response {
    self.tableViewStyle = UITableViewStylePlain;
    self.variableHeightRows = YES;
    
    NSArray* sortedResponse = nil;
    if ([response isKindOfClass:[NSSet class]]) {
        // sort the newsfeed by descending timestamp
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp" ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        sortedResponse = [[response allObjects] sortedArrayUsingDescriptors:sortDescriptors];
    }
    else if ([response isKindOfClass:[NSArray class]]) {
        sortedResponse = response;
    }
    else {
#ifdef DEBUG
        NSLog(@"The Newsfeed data is given in improper format to NewsfeedViewController");
#endif
        return;
    }
    
    self.newsfeedItems = sortedResponse;
    
    if (self.userNewsfeedFilterSelected && (![self.userNewsfeedFilterSelected isEqualToString:@"All"])) {
        self.filterStatus.text = [NSString stringWithFormat:@"  Displaying only '%@'", self.userNewsfeedFilterSelected];
    }
    else {
        // hide the goddamn label
        [self.filterStatus removeFromSuperview];
        self.filterStatus = nil;
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        // display newsfeed as a normal table list
        NSMutableArray * entries = [[NSMutableArray alloc] init];
        for (NewsfeedItem* item in sortedResponse) {
            NSString* actionText = [NSString stringWithFormat:@"%i %@ ・ %i Comments", [item.countLike intValue], ([item.type isEqualToString:@"thought"] ? @"Likes" : @"Echoes"), [item.countComment intValue]];
            TTTableNewsfeedItem* newsfeedItem = [TTTableNewsfeedItem itemWithTitle:item.author.fullname caption:nil htmlText:item.message timestamp:[NSDate dateWithTimeIntervalSince1970:[item.timestamp doubleValue]] imageURL:item.author.displayPic URL:nil type:item.type actionText:actionText groupName:item.classrooms.name tag:item.tid delegate:self];
            [entries addObject:newsfeedItem];
        }
        
        TTTableNewsfeedItemDataSource * dataSource = [[TTTableNewsfeedItemDataSource alloc] initWithItems:entries];
        self.dataSource = dataSource;
        [self customizeNewsfeedView];
        [self.tableView reloadData];
    }
    else {  // ipad is the device
        self.newsfeedMosaicView = [[UIScrollView alloc] init];
        self.newsfeedMosaicView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height - self.bottomToolbar.frame.size.height);
        [self customizeNewsfeedView];
        [self showNewsfeedAsMosaic];
    }
}

-(void)userNewsfeedFiltersDidLoad:(id)filters {
    
    PopupMenuViewController *contentViewController = [[PopupMenuViewController alloc] initWithStyle:UITableViewStylePlain];
    
    if ([filters isKindOfClass:[NSArray class]]) {
        self.tempUserMenus = filters;
    }
    else if ([filters isKindOfClass:[NSSet class]]) {
        self.tempUserMenus = [filters allObjects];
    }
    else {
#ifdef DEBUG
        NSLog(@"Error: Unable to store the newsfeed filter menu locally as it is not of type NSSet or NSArray but instead %@", [filters class]);
#endif
    }
    
    if (self.tempUserMenus.count) {
        // sort the newsfeed filters in ascending order by title, so ideally 'All' would appear first
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"title" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        // store the sorted newsfeed filters temporarily
        self.tempUserMenus = [self.tempUserMenus sortedArrayUsingDescriptors:sortDescriptors];
    }
    else {
        // there are no newsfeed filters to display
        [TeamieGlobals addTSMessageInController:self title:nil message:TeamieLocalizedString(@"MSG_NO_THOUGHT_FILTERS", nil) type:@"error" duration:3 withCallback:nil];
//        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_NO_THOUGHT_FILTERS", nil) margin:10.f yOffset:10.f afterDelay:3];
        NSLog(@"Error: There are no newsfeed filters to store! Absolutely zero!");
        return;
    }
    
    if (self.userNewsfeedFilterSelected == nil) {
        self.userNewsfeedFilterSelected = @"All";
    }
    // populate the popover menu
    NSMutableArray * entries = [[NSMutableArray alloc] init];
    for (UserMenu* item in self.tempUserMenus) {
        TTTableCheckedTextItem* filterItem = nil;
        if ([item.title isEqualToString:self.userNewsfeedFilterSelected]) {
            filterItem = [TTTableCheckedTextItem itemWithText:item.title state:CheckmarkChecked];
        }
        else {
            filterItem = [TTTableCheckedTextItem itemWithText:item.title state:CheckmarkNone];
        }
        [entries addObject:filterItem];
    }

    TTTableCheckedTextItemDataSource * dataSource = [[TTTableCheckedTextItemDataSource alloc] initWithItems:entries];
    contentViewController.dataSource = dataSource;
    contentViewController.menuDelegate = self;
    [contentViewController setNumMenuItems:entries.count];
    
    self.wePopoverController = [[popoverClass alloc] initWithContentViewController:contentViewController];
    self.wePopoverController.delegate = self;
    
    if ([self.newsfeedNavigationBar isHidden]) {
        self.wePopoverController.passthroughViews = [NSArray arrayWithObject:self.navigationController.navigationBar];
        [self.wePopoverController presentPopoverFromBarButtonItem:self.navigationItem.rightBarButtonItem permittedArrowDirections:(UIPopoverArrowDirectionDown|UIPopoverArrowDirectionUp) animated:YES];
    }
    else {
        self.wePopoverController.passthroughViews = [NSArray arrayWithObject:self.newsfeedNavigationBar];
        [self.wePopoverController presentPopoverFromBarButtonItem:[self.newsfeedNavigationBar.topItem.rightBarButtonItems lastObject] permittedArrowDirections:(UIPopoverArrowDirectionDown|UIPopoverArrowDirectionUp) animated:YES];
    }
}

-(void)userBulletinBoardDidLoad:(id)bulletinBoard {
    if ([bulletinBoard isKindOfClass:[NSArray class]] || bulletinBoard == nil) {  // implies loaded bulletinboard needs to be displayed
          
        BulletinBoardViewController *contentViewController = [[BulletinBoardViewController alloc] initForPopover:bulletinBoard];
        [contentViewController setControllerDelegate:self];
        
		self.wePopoverController = [[popoverClass alloc] initWithContentViewController:contentViewController];
        
        if ([self.wePopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
			[self.wePopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
		}
        
		self.wePopoverController.delegate = self;
		
        // The popover must appear just above the bottom toolbar
        CGFloat popoverY = self.bottomToolbar.frame.origin.y + 5.0;
        
        [self.wePopoverController presentPopoverFromRect:CGRectMake(10, popoverY, 40, 40) inView:self.view permittedArrowDirections:(UIPopoverArrowDirectionDown|UIPopoverArrowDirectionUp) animated:YES];
        
        if (self.bulletinBoardNotificationCount.hidden == NO) {
            // if there are any unread bulletin notificaitons count, then
            // send off an API request to unset the number of Bulletin Board notifications
            [self.apiCaller performRestRequest:TeamieUserUnsetBulletinCountRequest withParams:nil];
        }
        
        [self.bulletinBoardNotificationCount setHidden:YES];
    }
    else if ([bulletinBoard isKindOfClass:[NSDictionary class]]) { // implies bulletin board flag needs to be updated
        NSNumber * numNotifications = [bulletinBoard valueForKey:@"unread"];
        if ([numNotifications integerValue] != 0) {
            self.bulletinBoardNotificationCount.text = [NSString stringWithFormat:@"%d", [numNotifications integerValue]];
            [self.bulletinBoardNotificationCount setHidden:NO];
        }
    }
    else {
#ifdef DEBUG
        NSLog(@"Error in Bulletin Board load response. It's neither NSSet nor NSDictionary. Something fishy!");
#endif
    }
}

-(void)userMessageListDidLoad:(id)messageList {
    
    if (![messageList isKindOfClass:[NSArray class]]) { // if messageList is not an array of items, return
        return;
    }
    
    MessagesListViewController* messagesController = [[MessagesListViewController alloc] initForPopover:messageList];
    messagesController.delegate = self;
    UINavigationController* contentViewController = [[UINavigationController alloc] initWithRootViewController:messagesController];
    
    self.wePopoverController = [[popoverClass alloc] initWithContentViewController:contentViewController];
    
    if ([self.wePopoverController respondsToSelector:@selector(setContainerViewProperties:)]) {
        [self.wePopoverController setContainerViewProperties:[self improvedContainerViewProperties]];
    }
    
    self.wePopoverController.delegate = self;
    
    // The popover must appear just above the bottom toolbar
    CGFloat popoverY = self.bottomToolbar.frame.origin.y + 5.0;
    
    [self.wePopoverController presentPopoverFromRect:CGRectMake(50, popoverY, 40, 40) inView:self.view permittedArrowDirections:(UIPopoverArrowDirectionDown|UIPopoverArrowDirectionUp) animated:YES];
    
    [self.messagesNotificationCount setHidden:YES];
    
}

-(void)showNewsfeedAsMosaic {
    CGFloat mosaicGap = 5.0f;
    self.newsfeedMosaicView.scrollEnabled = YES;
    self.newsfeedMosaicView.bounces = NO;
    
    NSUInteger numGrids = 4;
    NSArray* layoutParams = [self generateRandomLayoutForElements:self.newsfeedItems.count forGrid:numGrids];
    CGFloat itemUnitWidth = self.view.frame.size.width / numGrids;
    CGFloat itemUnitHeight = itemUnitWidth;
    NSUInteger counter = 0;
    NSUInteger bottomMost = 0;
    
    // layout the newsfeed items according to the layoutParams
    for (NewsfeedItem* item in self.newsfeedItems) {
        NSArray* nibViews = [[NSBundle mainBundle] loadNibNamed:@"ThoughtMosaicView" owner:self options:nil];
        UIView* myView = [nibViews objectAtIndex:0];
        [myView.layer setCornerRadius:10.0f];
        [myView.layer setBorderColor:[UIColor lightGrayColor].CGColor];
        [myView.layer setBorderWidth:1.0f];
        [myView.layer setBackgroundColor:[UIColor whiteColor].CGColor];
        myView.layer.masksToBounds = YES;

        //adjust myView frame
        CGRect tempFrame = myView.frame;
        tempFrame.origin.y = [[[layoutParams objectAtIndex:counter] positionY] integerValue] * itemUnitHeight + mosaicGap;
        tempFrame.origin.x = [[[layoutParams objectAtIndex:counter] positionX] integerValue] * itemUnitWidth + mosaicGap;
        tempFrame.size.height = [[[layoutParams objectAtIndex:counter] frameHeight] integerValue] * itemUnitHeight - (2 * mosaicGap);
        tempFrame.size.width = [[[layoutParams objectAtIndex:counter] frameWidth] integerValue] * itemUnitHeight - (2 * mosaicGap);
        myView.frame = tempFrame;
        [self.newsfeedMosaicView addSubview:myView];
        
        // Tag 1: Author name UILabel
        // Tag 2: NumComments UILabel
        // Tag 3: Comments bg UIView
        // Tag 4: Author display Pic TTImageView
        // Tag 5: Thought timestamp text UILabel
        // Tag 6: Classroom name UIButton
        // Tag 7: Thought action UIButton
        // Tag 8: Body of thought TTStyledTextLabel
        // Tag 9: Thumbnail of attached image in thought TTImageView
        // Tag 10: Bottom white toolbar UIToolbar
        
        ((UILabel*)[myView viewWithTag:1]).text = item.author.fullname;
        //CGFloat left = ((UIButton*)[myView viewWithTag:7]).frame.origin.x + ((UIButton*)[myView viewWithTag:7]).frame.size.width + 5.0;

        if ([item.countComment longValue] != 0) {
            ((UILabel*)[myView viewWithTag:2]).text = [NSString stringWithFormat:@"%d", [item.countComment integerValue]];
            //left = ((UILabel*)[myView viewWithTag:2]).frame.origin.x + ((UILabel*)[myView viewWithTag:2]).frame.size.width + 5.0;
        }
        else {  // if there are no comments, then don't show the comment icon
            [[myView viewWithTag:2] setHidden:YES];
            [[myView viewWithTag:3] setHidden:YES];
        }
        
        // set body of thought
        TTStyledText* styledText = [TTStyledText textFromXHTML:item.htmlmessage];
        ((TTStyledTextLabel*)[myView viewWithTag:8]).text = styledText;
        ((TTStyledTextLabel*)[myView viewWithTag:8]).font = [UIFont systemFontOfSize:15.0];
        ((TTStyledTextLabel*)[myView viewWithTag:8]).textColor = [UIColor darkGrayColor];
        ((TTStyledTextLabel*)[myView viewWithTag:8]).contentInset = UIEdgeInsetsMake(10.0, 10.0, 10.0, 10.0);
        CGRect messageFrame = [myView viewWithTag:8].frame;
        messageFrame.size.width = myView.frame.size.width;
        messageFrame.origin.x = 0;
        [myView viewWithTag:8].frame = messageFrame;
        
        ((TTImageView*)[myView viewWithTag:4]).defaultImage = [UIImage imageWithContentsOfFile:@"icon-user-defaultpic.png"];
        ((TTImageView*)[myView viewWithTag:4]).urlPath = item.author.displayPic;
        /* if ([item.attachment.type isEqualToString:@"image"]) {  // if thought has an image attachment
            ((TTImageView*)[myView viewWithTag:9]).urlPath = item.attachment.thumbnail;
            [[myView viewWithTag:9] setHidden:NO];
            CGRect myViewFrame = [[myView viewWithTag:9] frame];
            CGFloat aspectRatio = myViewFrame.size.width / myViewFrame.size.height;
            if ([[[layoutParams objectAtIndex:counter] frameHeight] intValue] == 1 && [[[layoutParams objectAtIndex:counter] frameWidth] intValue] == 2) {
                myViewFrame.size.height = myView.frame.size.height - 45.0 - 40.0;
                myViewFrame.size.width = myViewFrame.size.height * aspectRatio;
                CGRect messageFrame = [myView viewWithTag:8].frame;
                messageFrame.origin.x = myViewFrame.size.width;
                messageFrame.size.width = myView.frame.size.width - myViewFrame.size.width;
                [myView viewWithTag:8].frame = messageFrame;
            }
            else {
                myViewFrame.size.width = myView.frame.size.width;
                myViewFrame.size.height = myViewFrame.size.width / aspectRatio;
                if ([[[layoutParams objectAtIndex:counter] frameHeight] intValue] == 2 && [[[layoutParams objectAtIndex:counter] frameWidth] intValue] == 1) {
                    CGRect messageFrame = [myView viewWithTag:8].frame;
                    messageFrame.origin.x = 0;
                    messageFrame.origin.y = myViewFrame.size.height;
                    messageFrame.size.width = myView.frame.size.width;
                    [myView viewWithTag:8].frame = messageFrame;
                }
                else {
                    [[myView viewWithTag:8] setHidden:YES];
                }
            }
            
            [[myView viewWithTag:9] setFrame:myViewFrame];
        }*/
        
        ((UILabel*)[myView viewWithTag:5]).text = item.timestampText;
        [((UIButton*)[myView viewWithTag:6]) setTitle:item.classrooms.name forState:UIControlStateNormal];
        [[myView viewWithTag:6] setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
        [myView viewWithTag:6].layer.cornerRadius = 5.0;
        [myView viewWithTag:6].layer.masksToBounds = YES;
        // Resize the classroom name button to fit properly
        [[myView viewWithTag:6] sizeToFit];
        tempFrame = [myView viewWithTag:6].frame;
        tempFrame.size.height += 10.0;
        tempFrame.size.width += 5.0;
        CGFloat thresholdWidth = myView.frame.size.width - [myView viewWithTag:7].frame.size.height - 15.0;
        if (tempFrame.size.width > thresholdWidth) {
            tempFrame.size.width = thresholdWidth;
        }
        [myView viewWithTag:6].frame = tempFrame;
        
        NSUInteger itemBottom = [[[layoutParams objectAtIndex:counter] positionY] integerValue] + [[[layoutParams objectAtIndex:counter] frameHeight] integerValue];
        if (bottomMost < itemBottom) {
            bottomMost = itemBottom;
        }
        counter++;
    }

    [self.view addSubview:self.newsfeedMosaicView];
    self.newsfeedMosaicView.contentSize = CGSizeMake(self.view.frame.size.width, bottomMost * itemUnitHeight + mosaicGap);
    self.tableView.frame = CGRectZero;

    [self.view setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
}

-(NSArray*)generateRandomLayoutForElements:(NSUInteger)numElements forGrid:(NSUInteger)numDivisions {
    NSMutableArray* layout = [[NSMutableArray alloc] init];
    NSUInteger maxWidth = 2;
    NSUInteger maxHeight = 2;
    for (NSInteger i = 0; i < maxHeight * numDivisions; i++) {
        [layout addObject:[NSNumber numberWithInt:0]];
    }
    NSUInteger currentIndex = 0;
    NSMutableArray* finalResult = [[NSMutableArray alloc] init];
    //NSRange currentRange = NSMakeRange(0, layout.count);
    
    while (currentIndex < numElements) {
        NSUInteger startIndex = [layout indexOfObject:[NSNumber numberWithInt:0]]; // need to add a range condition
        if (startIndex == NSNotFound) {
            startIndex = [layout count];
            for (NSUInteger i = 0; i < maxHeight * numDivisions; i++) {
                [layout addObject:[NSNumber numberWithInt:0]];
            }
        }
        
        NSUInteger colNo = startIndex % numDivisions;
        NSUInteger rowNo = startIndex / numDivisions;
        NSRange spaceInCurrentRow = NSMakeRange(startIndex, numDivisions - colNo);
        
        NSUInteger endIndex = [layout indexOfObject:[NSNumber numberWithInt:1] inRange:spaceInCurrentRow];
        NSRange freeSpace;
        if (endIndex == NSNotFound) {
            endIndex = startIndex + spaceInCurrentRow.length;
        }
        freeSpace = NSMakeRange(startIndex, endIndex - startIndex);
        if (freeSpace.length > maxWidth) {
            freeSpace.length = maxWidth;
        }
        
        NSUInteger x = (arc4random() % freeSpace.length) + 1;
        NSUInteger y = (arc4random() % maxHeight) + 1;
        
        NSUInteger maxIndex = startIndex + (x-1) + (y-1) * numDivisions;
        while (layout.count <= maxIndex) {
            for (NSUInteger i = 0; i < numDivisions; i++) {
                [layout addObject:[NSNumber numberWithInt:0]];
            }
        }
        for (NSUInteger i=0; i < x; i++) {
            for (NSUInteger j=0; j < y; j++) {
                [layout replaceObjectAtIndex:(startIndex + i + j * numDivisions) withObject:[NSNumber numberWithInt:1]];
            }
        }
        
        MosaicTileInfo* values = [[MosaicTileInfo alloc] init];
        values.positionX = [NSNumber numberWithInteger:colNo];
        values.positionY = [NSNumber numberWithInteger:rowNo];
        values.frameWidth = [NSNumber numberWithInteger:x];
        values.frameHeight = [NSNumber numberWithInteger:y];
        
        //NSDictionary* values = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:x], @"width", [NSNumber numberWithInteger:y], @"height", [NSNumber numberWithInteger:colNo], @"x", [NSNumber numberWithInteger:rowNo], @"y", nil];
        [finalResult addObject:values];
        
        // update values
        currentIndex++;
        //currentRange.location = startIndex + x;
        //currentRange.length = layout.count - currentRange.location;
    }
    
    NSLog(@"Dictionary of values: %@", finalResult);
    
    return finalResult;
}

-(void)addClassroomContentButton {
    
    if (self.newsfeedClassroomID && self.bottomToolbar) {
        
        UIBarButtonItem* contentButton = [[UIBarButtonItem alloc] initWithTitle:@"Stuff" style:UIBarButtonItemStyleBordered target:self action:@selector(classroomContentMenuOpen:event:)];
        [contentButton setTintColor:[UIColor blackColor]];
        
        if (![self.bottomToolbar.items containsObject:contentButton]) {
            
            NSMutableArray* toolItems = [NSMutableArray arrayWithArray:self.bottomToolbar.items];
            [toolItems insertObject:contentButton atIndex:self.bottomToolbar.items.count - 1]; // make it the second last item

            [self.bottomToolbar setItems:toolItems animated:YES];
        }
        
    }
    
}

- (void)itemsDidFinishLoading {
    
    [self customizeNewsfeedView];
    
}

#pragma mark - ComposeMessageViewControllerDelegate methods

-(void)messageSentSuccessfully:(NSNotification*)notification {
    messageReload = YES;
}

#pragma mark - ShareBoxControllerDelegate methods

-(void)shareBoxDidExit:(NSNumber*)thoughtPostStatus {
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.popoverController dismissPopoverAnimated:YES];
    }
    else {
        [self.modalViewController dismissModalViewControllerAnimated:YES];
    }
    
    // empty the Sharebox variable that points to the ShareboxViewController
    self.shareBox = nil;
    
    if ([thoughtPostStatus intValue] == 1) {    // The thought was posted successfully
        [TeamieUIGlobals displayStatusMessage:TeamieLocalizedString(@"MSG_THOUGHT_POST_SUCCESS", @"The thought was posted successfully!") forPurpose:@"ok"];
    
        // reload the newsfeed items
        self.dataSource = nil;
    }
}
-(void)didDismissThoughtView:(NewsfeedItem *)thoughtData {
    
    if (thoughtData == nil) {
        // 'thoughtData' will be nil whenever the thought that was opened was deleted. So in that case refresh the newsfeed to update it.
        self.dataSource = nil;
        // Display a status message saying thought was deleted successfully
        [TeamieUIGlobals displayStatusMessage:TeamieLocalizedString(@"MSG_THOUGHT_DELETED", @"Thought deleted successfully") forPurpose:@"ok"];
    }
    else {
        // notify the data model about the thought object that was updated
        [((TTTableNewsfeedItemDataSource*)self.dataSource).dataModel didUpdateObjectWithId:thoughtData.tid];
    }
}

#pragma mark - WEPopoverControllerDelegate methods
- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    self.wePopoverController = nil;
    self.bulletinBoardController = nil; // optional
    NSLog(@"Popover dismissed...");
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

- (WEPopoverContainerViewProperties *)improvedContainerViewProperties {
	
	WEPopoverContainerViewProperties *props = [WEPopoverContainerViewProperties alloc];
	NSString *bgImageName = nil;
	CGFloat bgMargin = 0.0;
	CGFloat bgCapSize = 0.0;
	CGFloat contentMargin = 4.0;
	
	bgImageName = @"popoverBg.png";
	
	// These constants are determined by the popoverBg.png image file and are image dependent
	bgMargin = 13; // margin width of 13 pixels on all sides popoverBg.png (62 pixels wide - 36 pixel background) / 2 == 26 / 2 == 13 
	bgCapSize = 31; // ImageSize/2  == 62 / 2 == 31 pixels
	
	props.leftBgMargin = bgMargin;
	props.rightBgMargin = bgMargin;
	props.topBgMargin = bgMargin;
	props.bottomBgMargin = bgMargin;
	props.leftBgCapSize = bgCapSize;
	props.topBgCapSize = bgCapSize;
	props.bgImageName = bgImageName;
	props.leftContentMargin = contentMargin;
	props.rightContentMargin = contentMargin - 1; // Need to shift one pixel for border to look correct
	props.topContentMargin = contentMargin; 
	props.bottomContentMargin = contentMargin;
	
	props.arrowMargin = 4.0;
	
	props.upArrowImageName = @"popoverArrowUp.png";
	props.downArrowImageName = @"popoverArrowDown.png";
	props.leftArrowImageName = @"popoverArrowLeft.png";
	props.rightArrowImageName = @"popoverArrowRight.png";
	return props;	
}

#pragma mark - UI Customization methods

// Since NewsfeedViewController inherits from TTTableViewController, some customizations would hav to be made in order to show the other UI elements on screen
-(void)customizeNewsfeedView {
    
    BOOL willFilterStatusBeVisible = NO;
    
    CGFloat navigationBarHeight = 0.0f;
    
    if (!(self.newsfeedNavigationBar.hidden == YES)) {
        navigationBarHeight = self.newsfeedNavigationBar.frame.size.height;
    }
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        navigationBarHeight = self.newsfeedNavigationBar.frame.size.height + 20.0;
    }
    
    if (self.userNewsfeedFilterSelected && ![self.userNewsfeedFilterSelected isEqualToString:@"All"]) {
        self.filterStatus.text = [NSString stringWithFormat:@"  Displaying only %@", self.userNewsfeedFilterSelected];
        [self.filterStatus setHidden:NO];
        
        if (self.filterStatus.frame.origin.y - navigationBarHeight > 0.00001 || self.filterStatus.frame.origin.y - navigationBarHeight < -0.00001) {  // if the UILabel is not already in its final position

            //self.filterStatus.frame = CGRectMake(0, 0, self.view.frame.size.width, 35);
            
            // animate the label out of view
            [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut
                             animations:^{
                                 self.filterStatus.frame = CGRectMake(0, navigationBarHeight, self.view.frame.size.width, 35);
                             }
                             completion:^(BOOL finished) { 
                                 [self.filterStatus setHidden:NO];
                             }];
            /*
            // animate the filterStatus label
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:0.5];
            [UIView setAnimationDelay:0.0];
            [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
            
            CGRect tempFrame = self.filterStatus.frame;
            tempFrame.origin.y = navigationBarHeight;
            self.filterStatus.frame = tempFrame;
            
            [UIView commitAnimations];*/
        }
        
        willFilterStatusBeVisible = YES;
    }
    
    CGRect tempFrame = self.tableView.frame;
    if (self.newsfeedMosaicView) {
        tempFrame = self.newsfeedMosaicView.frame;
    }
    
    // the table view should occupy the space between the navigation bar and the bottom toolbar
    tempFrame.size.height = self.view.frame.size.height - self.bottomToolbar.frame.size.height - navigationBarHeight;
    tempFrame.origin.y = navigationBarHeight;
    
    if (willFilterStatusBeVisible) {        // if the filter status label is going to be visible, make space for it
        tempFrame.origin.y += self.filterStatus.frame.size.height;
        tempFrame.size.height -= self.filterStatus.frame.size.height;
    }
    
    if (self.newsfeedMosaicView) {
        self.newsfeedMosaicView.frame = tempFrame;
        self.tableView.frame = CGRectZero;
    }
    else {
        self.tableView.frame = tempFrame;
        self.newsfeedMosaicView.frame = CGRectZero;
    }

    // position the toolbar at the bottom of the tableview
//    [self.view bringSubviewToFront:self.bottomToolbar];
    tempFrame = self.bottomToolbar.frame;
    tempFrame.origin.y = self.tableView.frame.origin.y + self.tableView.frame.size.height;
    self.bottomToolbar.frame = tempFrame;
    [self.view sendSubviewToBack:self.bottomToolbar];
    
    tempFrame = self.messagesNotificationCount.frame;
    tempFrame.origin.y = self.tableView.frame.origin.y + self.tableView.frame.size.height + 5;
    self.messagesNotificationCount.frame = tempFrame;
    [self.view bringSubviewToFront:self.messagesNotificationCount];
    
    tempFrame = self.bulletinBoardNotificationCount.frame;
    tempFrame.origin.y = self.tableView.frame.origin.y + self.tableView.frame.size.height + 5;
    self.bulletinBoardNotificationCount.frame = tempFrame;
    [self.view bringSubviewToFront:self.bulletinBoardNotificationCount];
}

#pragma mark - PopupMenuDelegate methods
-(void)menuDidExitWithData:(id)sender {
    id data = [sender valueForKey:@"text"];
    
    if ([data isKindOfClass:[NSString class]]) {
        if (([data isEqualToString:self.classroomMenuMapping.lessonTitle] || [data isEqualToString:self.classroomMenuMapping.quizTitle] || [data isEqualToString:self.classroomMenuMapping.memberTitle] || [data isEqualToString:self.classroomMenuMapping.leaderboardTitle] || [data isEqualToString:self.classroomMenuMapping.gradebookTitle]) && self.newsfeedClassroomID) {
            
            NSMutableDictionary* params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:self.newsfeedClassroomID, @"nid", nil];
            [params setValue:data forKey:@"name"];
            
            [self.wePopoverController dismissPopoverAnimated:YES];
            self.wePopoverController = nil;
            
            if ([data isEqualToString:self.classroomMenuMapping.gradebookTitle]) {
                TeamieChartsViewController* chartVC = [[TeamieChartsViewController alloc] initWithReportType:TeamieGradebookReport];
                // set the classroom newsfeed ID for the view controller
                [chartVC setClassroomID:newsfeedClassroomID];
                UINavigationController * navVC = [[UINavigationController alloc] initWithRootViewController:chartVC];
                [self presentModalViewController:navVC animated:YES];
            }
            else {  // use the reveal controller to show the lesson/quiz listing
                TeamieListViewController * fVC;
                
                // configure the TeamieListViewController to display according to the entity to be displayed
                if ([data isEqualToString:self.classroomMenuMapping.lessonTitle]) {
                    fVC = [[TeamieListViewController alloc] initWithEntity:TeamieLessonEntity withId:[params valueForKey:@"nid"]];
                }
                else if ([data isEqualToString:self.classroomMenuMapping.quizTitle]) {
                    fVC = [[TeamieListViewController alloc] initWithEntity:TeamieQuizEntity withId:[params valueForKey:@"nid"]];
                }
                else if ([data isEqualToString:self.classroomMenuMapping.leaderboardTitle]) {
                    fVC = [[TeamieListViewController alloc] initWithEntity:TeamieLeaderboardEntity withId:[params valueForKey:@"nid"]];
                }
                else if ([data isEqualToString:self.classroomMenuMapping.memberTitle]) {
                    fVC = [[TeamieListViewController alloc] initWithEntity:TeamieMembersEntity withId:[params valueForKey:@"nid"]];
                }
                [[GANTracker sharedTracker] trackEvent:@"Classroom" action:[NSString stringWithFormat:@"%@ List Open", data] label:nil value:[[params valueForKey:@"nid"] longLongValue] withError:nil];
                
                // create a navigation view controller and present it modally so that it can be easily dismissed
                UINavigationController * navVC = [[UINavigationController alloc] initWithRootViewController:fVC];
                [self presentModalViewController:navVC animated:YES];
            }
            return;
        }
        else if ([[TeamieGlobals defaultTeamieThoughtActionMenus] containsObject:data]) {
            
            // if the menu name is one of the default Thought Actions then call the ThoughtActionSelected method
            [self newsfeedItemThoughtActionSelected:sender];
            return;
            
        }

        
        // if control reaches it implies that newsfeed filters were applied
        if (![self.userNewsfeedFilterSelected isEqualToString:data]) {  // if the selected filter is not already selected
            self.userNewsfeedFilterSelected = data;
            
            if (!self.filterStatus.isHidden) {
                // animate the label out of view
                [UIView animateWithDuration:0.5 delay:0.0 options:UIViewAnimationCurveEaseIn
                                 animations:^{
                                     self.filterStatus.frame = CGRectMake(0, -35, self.view.frame.size.width, 35);
                                 }
                                 completion:nil];
            }
            
            // dismiss the popover displaying newsfeed filters
            [self.wePopoverController dismissPopoverAnimated:YES];
            self.wePopoverController = nil;
            
            self.dataSource = nil;  // making dataSource nil, calls the self.createModel method
            
            [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Newsfeed Filter Apply" label:self.userNewsfeedFilterSelected value:0 withError:nil];
        }
    }
}

#pragma mark - TTTableNewsfeedItemCell delegate methods
-(void)newsfeedItemCellGroupSelected:(id)sender {
    
    if (sender == nil || ![sender isKindOfClass:[NewsfeedItem class]]) {
#ifdef DEBUG
        NSLog(@"Error: Unable to find NewsfeedItem associated with the table cell selected.");
#endif
        return;
    }
    
    NewsfeedItem* feedItem = (NewsfeedItem*)sender;
    
#ifdef DEBUG
    NSLog(@"Classroom button of thought ID #%qi was pressed!", [feedItem.tid longLongValue]);
#endif
    
    if (self.newsfeedClassroomID && [feedItem.classrooms.nid isEqualToNumber:self.newsfeedClassroomID]) { // if the user is already inside a classroom, do nothing
        // Show an alert saying the user is already in that classroom
        [TeamieGlobals addTSMessageInController:self
                                          title:TeamieLocalizedString(@"HEY", nil)
                                        message:[NSString stringWithFormat:TeamieLocalizedString(@"ALREADY_IN_CLASSROOM_ERROR", nil), feedItem.classrooms.name]
                                           type:@"warning"
                                       duration:0
                                   withCallback:nil];
        
        return;
    }
    
    if ([self.parentRevealController respondsToSelector:@selector(slideMenuCellSelected:)]) {
        [self.parentRevealController performSelector:@selector(slideMenuCellSelected:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:@"classroom", @"name", feedItem.classrooms.nid, @"nid", nil]];
    }
    else {
#ifdef DEBUG
        NSLog(@"%@ object does not define method slideMenuCellSelected:", [self.parentRevealController class]);
#endif
    }
    
    [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Classroom Open" label:nil value:[feedItem.classrooms.nid longLongValue] withError:nil];
}

-(void)newsfeedItemCellImageSelected:(id)sender {
    
    if (sender == nil || ![sender isKindOfClass:[NewsfeedItem class]]) {
#ifdef DEBUG
        NSLog(@"Error: Unable to find NewsfeedItem associated with the table cell selected.");
#endif
        return;
    }
    
    NewsfeedItem* feedItem = (NewsfeedItem*)sender;
    
    // check if the author uid is 0. If yes, then do nothing.
    if ([feedItem.author.uid longLongValue] == 0) {
        return;
    }
    
    // initialize a user profile view controller to view the user profile of the user, whose display pic was tapped
    UserProfileViewController* uPVC = [[UserProfileViewController alloc] initWithUserID:[feedItem.author.uid longLongValue]];
    uPVC.modalPresentationStyle = UIModalPresentationFullScreen;
    uPVC.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    //[self presentViewController:uPVC animated:YES completion:nil];
    UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:uPVC];
    [self presentViewController:navigationController animated:YES completion:nil];
    
    [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Profile Open" label:nil value:[feedItem.author.uid longLongValue] withError:nil];
}

-(void)newsfeedItemCellThoughtActionSelected:(id)sender newsfeedItem:(id)item {
    PopupMenuViewController *contentViewController = [[PopupMenuViewController alloc] initWithStyle:UITableViewStylePlain];

    NewsfeedItem* feedItem = (NewsfeedItem*)item;

    NSNumber* tid = feedItem.tid;   // the thought ID
    NSArray* defaultMenus = [TeamieGlobals defaultTeamieThoughtActionMenus];

    // populate the popover menu
    NSMutableArray * entries = [[NSMutableArray alloc] init];
    
    for (id actionItem in feedItem.actions) {
        NSLog(@"The action item is %@", actionItem);
    }
    // NOTE: only like, comment and echo thought actions are allowed currently
    NSPredicate* onlySupportedActions = [NSPredicate predicateWithFormat:@"((self.name == %@) || (self.name == %@) || (self.name == %@)) && self.access == 1", @"like", @"same", @"thought-comment", nil];
    // get the filtered set of thought actions to which user has access
    NSSet* filteredActions = [feedItem.actions filteredSetUsingPredicate:onlySupportedActions];
    
    // if there are no thought actions that user has access to
    if ([filteredActions count] <= 0) {
        // set an HUDMessage message and return
        [TeamieUIGlobals displayStatusMessage:TeamieLocalizedString(@"MSG_NO_THOUGHT_ACTIONS", @"No thought actions to load.") forPurpose:@"error"];
        return;
    }
    
    // sort the thought actions in descending order of their 'name' property. That way comment will always appear last.
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray* finalThoughtActions = [filteredActions sortedArrayUsingDescriptors:sortDescriptors];
    
    // if there are some thought actions to be shown, then display those filtered actions
    for (NewsfeedItemAction* thoughtAction in finalThoughtActions) {
        
        NSString* linkMenuname = nil;
        
        if ([thoughtAction.name isEqualToString:@"like"]) {
            if ([thoughtAction.status boolValue]) { // Unlike
                linkMenuname = [defaultMenus objectAtIndex:1];
            }
            else {
                linkMenuname = [defaultMenus objectAtIndex:0];
            }
        }
        else if ([thoughtAction.name isEqualToString:@"same"]) {
            if ([thoughtAction.status boolValue]) { // Unecho
                linkMenuname = [defaultMenus objectAtIndex:3];
            }
            else {
                linkMenuname = [defaultMenus objectAtIndex:2];
            }
        }
        else {
            linkMenuname = thoughtAction.title;
        }
        
        // Add the like/unlike/echo/unecho button
        [entries addObject:[TTTableTextUniqueItem itemWithText:linkMenuname delegate:self selector:nil uniqueId:tid]];
    }
    
    TTListDataSource * dataSource = [[TTListDataSource alloc] initWithItems:entries];
    contentViewController.dataSource = dataSource;
    contentViewController.menuDelegate = self;
    // set the no. of menu items in the popover
    [contentViewController setNumMenuItems:[entries count]];
    
    if (self.wePopoverController) {
        [self.wePopoverController dismissPopoverAnimated:YES];
        self.wePopoverController = nil;
    }
    
    self.wePopoverController = [[popoverClass alloc] initWithContentViewController:contentViewController];
    self.wePopoverController.delegate = self;
    
    CGRect showFrame = [self.view convertRect:((UIButton*)sender).bounds fromView:sender];
    
    [self.wePopoverController presentPopoverFromRect:showFrame inView:self.view permittedArrowDirections:(UIPopoverArrowDirectionLeft) animated:YES];
    
    [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Thought Actions Open" label:nil value:[feedItem.tid longLongValue] withError:nil];
}

// The method that is called when Like / Same / Report / Subscribe is pressed for a given thought
- (void)newsfeedItemThoughtActionSelected:(id)sender {
    
    NSArray* defaultMenus = [TeamieGlobals defaultTeamieThoughtActionMenus];
    
    if (![sender isKindOfClass:[TTTableTextUniqueItem class]]) {
#ifdef DEBUG
        NSLog(@"Error: The sender is an object of %@ class, which is not supported!", [sender class]);
#endif
    }
    
    // Check the titles inside the sender item
    NSString* menuTitle = ((TTTableTextUniqueItem*)sender).text;
    NSNumber* uniqueId = nil;
    
    if ([((TTTableTextUniqueItem*)sender).uniqueId isKindOfClass:[NSNumber class]]) {
        uniqueId = ((TTTableTextUniqueItem*)sender).uniqueId;
    }
    else {
#ifdef DEBUG
        NSLog(@"Error: The TableItem in the thought Actions popup does not have a unique ID that is a Newsfeed item object");
#endif
    }
    
    NSInteger menuIndex = [defaultMenus indexOfObject:menuTitle];
    
    if (menuIndex != NSNotFound) {  // Make sure the menu title is one of the thought actions
    
        if (menuIndex == 5) {   // "Comment"
            // Perform segue to the Thought View screen
            [self performSegueWithIdentifier:@"showThoughtView" sender:sender];
            // check prepareForSegue method for the preparations done before the segue
        }
        else if (menuIndex == 0 || menuIndex == 1) {    // "Like" or "Unlike"
            
            // perform REST request to Like / Unlike the thought
            [self.apiCaller performRestRequest:TeamieUserLikeThoughtRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%qi", [uniqueId longLongValue]], @"tid", nil]];
            
            // display an MBProgressGUD message
            NSString* HUDMessage = TeamieLocalizedString(@"MSG_PROCESSING", nil);
            if (menuIndex == 0) {
                HUDMessage = TeamieLocalizedString(@"MSG_LIKE_IN_PROGRESS", nil);
            }
            else if (menuIndex == 1) {
                HUDMessage = TeamieLocalizedString(@"MSG_UNLIKE_IN_PROGRESS", nil);
            }

        [TeamieGlobals addHUDTextOnlyLabel:HUDMessage margin:10.f yOffset:10.f];
            
        }
        else if (menuIndex == 2 || menuIndex == 3) {    // "Echo" or "Unecho"
            
            // perform REST request to echo / unecho a question
            [self.apiCaller performRestRequest:TeamieUserSameThoughtRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%qi", [uniqueId longLongValue]], @"tid", nil]];
            
            // display an MBProgressHUD message
            NSString* HUDMessage = TeamieLocalizedString(@"MSG_PROCESSING", nil);
            if (menuIndex == 2) {
                HUDMessage = TeamieLocalizedString(@"MSG_ECHO_IN_PROGRESS", nil);
            }
            else if (menuIndex == 3) {
                HUDMessage = TeamieLocalizedString(@"MSG_UNECHO_IN_PROGRESS", nil);
            }
            
            [TeamieGlobals addHUDTextOnlyLabel:HUDMessage margin:10.f yOffset:10.f];
            
        }
        else {
            UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:TeamieLocalizedString(@"SORRY", nil) message:TeamieLocalizedString(@"NOT_IMPLEMENTED_ERROR", nil) delegate:nil cancelButtonTitle:TeamieLocalizedString(@"BTN_OK", nil) otherButtonTitles:nil, nil];
            [alertView show];
        }
    }
    
    // Dismiss the popover once an action is selected
    [self.wePopoverController dismissPopoverAnimated:YES];
    [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Thought Action Done" label:menuTitle value:[uniqueId longLongValue] withError:nil];
}

- (void)newsfeedItemDownloadAttachmentTapped:(id)sender newsfeedItem:(id)feedItem {
    if ((![feedItem isKindOfClass:[NewsfeedItem class]]) || (![sender isKindOfClass:[UIButton class]])) {
        return;
    }
    NewsfeedItem* item = feedItem;
    NSString* attachmentPressed = ((UIButton*)sender).superview.accessibilityIdentifier;
    
    for (NewsfeedItemAttachment* itemAttachment in item.attachment) {
        if ([attachmentPressed isEqualToString:itemAttachment.type]) {
            AddResourceStatus result = [[ResourcesViewController sharedController] addResource:itemAttachment];
            if (result == kDownloadSuccessful) {
                [TeamieGlobals addTSMessageInController:self title:@"" message:TeamieLocalizedString(@"MSG_RESOURCE_SAVED", nil) type:@"success" duration:2.0 withCallback:nil];
            }
            else if (result == kDownloadFailed){
                [TeamieGlobals addTSMessageInController:self title:@"" message:TeamieLocalizedString(@"MSG_RESOURCE_EXISTS", nil) type:@"error" duration:2.0 withCallback:nil];
            }
            else {
                [TeamieGlobals addTSMessageInController:self title:@"" message:TeamieLocalizedString(@"MSG_RESOURCE_DOWNLOADING", nil) type:@"warning" duration:2.0 withCallback:nil];
            }
            break;
        }
    }
}

- (void)newsfeedItemAttachmentTapped:(id)sender newsfeedItem:(id)feedItem {
    if ((![feedItem isKindOfClass:[NewsfeedItem class]]) || (![sender isKindOfClass:[UITapGestureRecognizer class]])) {
        return;
    }
    
    NewsfeedItem* item = feedItem;
    NSString* attachmentPressed = ((UITapGestureRecognizer*)sender).view.accessibilityIdentifier;
    
    for (NewsfeedItemAttachment* itemAttachment in item.attachment) {
        if ([attachmentPressed isEqualToString:itemAttachment.type]) {
            if ([attachmentPressed isEqualToString:@"image"] && itemAttachment.href) {
                // open the attached image
                // Open up the image view controller, pass the attachment info along
                [self performSegueWithIdentifier:@"showAttachedImage" sender:itemAttachment];
                [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Attached Image Open" label:nil value:[item.tid intValue] withError:nil];
            }
            else {
                NSString* linkToOpen = nil;
                if ([attachmentPressed isEqualToString:@"link"] && itemAttachment.href) {
                    // the thought's attached link was pressed
                    linkToOpen = itemAttachment.href;
                }
                else if ([attachmentPressed isEqualToString:@"file"] && itemAttachment.previewURL) {
                    linkToOpen = [NSString stringWithFormat:@"%@/merge-session/?access_token=%@&redirect_url=%@", TEAMIE_DEFAULT_OAUTH_REST_URL, [[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"], itemAttachment.previewURL];
                }
                else if ([attachmentPressed isEqualToString:@"audio"] && itemAttachment.href) {
                    linkToOpen = itemAttachment.href;
                }
                else {
                    // just open up the thought in a web browser and let the user do whatever he wants to do
                    linkToOpen = [NSString stringWithFormat:@"%@/merge-session/?access_token=%@&redirect_url=%@", TEAMIE_DEFAULT_OAUTH_REST_URL, [[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"], [NSString stringWithFormat:@"thought/%qi", [item.tid longLongValue]]];
                }
                
                // Open up the link inside an in-app browser
                TeamieWebViewController * webController = [[TeamieWebViewController alloc] init];
                [webController openURL:[NSURL URLWithString:linkToOpen]];
                webController.modalPresentationStyle = UIModalPresentationFullScreen;
                webController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
                UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webController];
                [self presentViewController:navigationController animated:YES completion:nil];
                
                [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Attached Link Open" label:nil value:[item.tid longLongValue] withError:nil];
            }
            // once the required action is taken, then break
            break;
        }
    }
}

// Deprecated: This method is called whenever the thought / question's message body part is tapped. Should be used to display the image or navigate to the link contained in the thought / question
- (void)newsfeedItemMessageSelected:(id)feedItem {
    
    if (![feedItem isKindOfClass:[NewsfeedItem class]]) {
#ifdef DEBUG
        NSLog(@"Error: The feedItem returned was not of type %@", [NewsfeedItem class]);
#endif
        return;
    }
    
    NewsfeedItem* item = feedItem;
    
    // Perform segue to the Thought View screen
    [self performSegueWithIdentifier:@"showThoughtView" sender:feedItem];
    [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Thought Open" label:nil value:[item.tid longLongValue] withError:nil];
    // check prepareForSegue method for the preparations done before the segue
}

- (void)newsfeedItemLikesCommentsSelected:(id)feedItem {
    
    if (![feedItem isKindOfClass:[NewsfeedItem class]]) {
        NSLog(@"Error: The Likes and comments tapped sender is not of class NewsfeedItem, instead it's %@", [feedItem class]);
        return;
    }
    
    [self performSegueWithIdentifier:@"showThoughtView" sender:feedItem];
    [[GANTracker sharedTracker] trackEvent:@"Newsfeed" action:@"Thought Open" label:nil value:[((NewsfeedItem*)feedItem).tid longLongValue] withError:nil];
}

#pragma mark --
#pragma mark TTModelViewController methods

- (void)createModel {
    
    TTTableNewsfeedItemDataSource *feedDataSource;
    NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    
    if (self.userNewsfeedFilterSelected) {
        for (UserMenu* menuItem in self.tempUserMenus) {
            if ([menuItem.title isEqualToString:self.userNewsfeedFilterSelected]) {

                for (URLParam* param in menuItem.attributes) {
                    if (param.value.length > 0) {
                        // To-do:Remove once subu fixes Newsfeed bug #tempcode
                        if ([param.value hasSuffix:@"s"]) {
                            [params setValue:[param.value substringToIndex:(param.value.length - 1)] forKey:param.name];
                        }
                        else {
                            [params setValue:param.value forKey:param.name];
                        }
                    }
                }
                
                //[self.newsfeedMosaicView removeFromSuperview];
                //self.newsfeedMosaicView = nil;
                
                break;
            }
        }
    }
    
    if (self.newsfeedClassroomID) { // if the classroom ID is set, then add that to params
        
        [params setValue:[NSString stringWithFormat:@"%qi", [self.newsfeedClassroomID longLongValue]] forKey:@"nid"];
        
    }
    
    [params setValue:NUMBER_OF_NEWSFEED_ITEMS forKey:@"items_per_page"];
    
    TeamieRESTRequest requestType = (self.newsfeedClassroomID) ? TeamieUserClassroomNewsfeedRequest : TeamieUserNewsfeedRequest;
    feedDataSource = [[TTTableNewsfeedItemDataSource alloc] initWithRequest:requestType withParams:params delegate:self];
    
    self.dataSource = feedDataSource;
    
}

- (id<UITableViewDelegate>)createDelegate {
    
    return [[TTTableViewDragRefreshDelegate alloc]
             initWithController:self];
    
}

#pragma mark - Table Delegate methods

// Callback method that is called whenever user taps on any part of the newsfeed item other than those for which listeners have been added
- (void)didSelectObject:(id)object atIndexPath:(NSIndexPath *)indexPath {
    
    /*if ([object isKindOfClass:[TTTableNewsfeedItem class]]) {
        
        [TeamieGlobals performSelectionHighlightAnimation:[self.tableView cellForRowAtIndexPath:indexPath]];
        
        NSNumber* selectedTid = ((TTTableNewsfeedItem*)object).tid;
        
        // for each thought currently displayed
        for (NewsfeedItem* item in [((TTTableNewsfeedItemDataSource*)self.dataSource).dataModel modelItems]) {
            if ([selectedTid isEqualToNumber:item.tid]) {
                if (item.attachment) {  // if the thought / question has an attachment
                    [self newsfeedItemMessageSelected:item];
                }
                else {
                    // perform segue to the ThoughtView
                    [self performSegueWithIdentifier:@"showThoughtView" sender:object];
                }
            }
        } 
    }
    else {*/
        [super didSelectObject:object atIndexPath:indexPath];
    //}
    
}

#pragma mark - NSNotification methods

- (void)receiveRefreshTokenNotification:(NSNotification*)notification {
    // This code gets executed after a new access_token has been obtained using refresh_token
    // Make the bulletin board request
    [self.apiCaller performRestRequest:TeamieUserBulletinBoardCountRequest withParams:nil];
    // Make the Teamie user profile request for currently logged-in user
    [self.apiCaller performRestRequest:TeamieUserProfileRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%qi", [self.apiCaller.currentUser.uid longLongValue]], @"uid", nil]];
    // Make the user messages request
    [self.apiCaller performRestRequest:TeamieUserMessagesCountRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:NUMBER_OF_MESSAGE_THREADS, @"items_per_page", nil]];
    
    self.dataSource = nil;
}

- (void)viewDidDisappear:(BOOL)animated {
    
//    [self becomeFirstResponder];
}

///////////////////////////////////////////////////////////////////////////////
//Shaking the device will show up random thoughts from the newsfeed
///////////////////////////////////////////////////////////////////////////////

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event {
    
    if (event.subtype == UIEventSubtypeMotionShake) {
        // If device is shaked, capture the Motion Shake Event and do something
        NSLog(@"Device Shaked");
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
        [self performSegueWithIdentifier:@"showThoughtView" sender:self];
    }
    
    if ([super respondsToSelector:@selector(motionEnded:withEvent:)]) {
        [super motionEnded:motion withEvent:event];
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)dismissPopovers {
    
    [self.wePopoverController dismissPopoverAnimated:YES];
    
    self.wePopoverController = nil;
}

// NOTE: Filter status label is hidden when created. Should explicitly be shown to display.
- (UILabel*)filterStatus {
    
    if (_filterStatus == nil) {
        _filterStatus = [[UILabel alloc] initWithFrame:CGRectMake(0, -35, self.view.frame.size.width, 35)];
        // set the filter status label properties
        _filterStatus.autoresizingMask = UIViewAutoresizingFlexibleWidth;   // resize for screen orientations
        [_filterStatus setBackgroundColor:[UIColor colorWithWhite:0.8 alpha:1.0]];
        [_filterStatus setFont:[UIFont systemFontOfSize:12.0]];
        
        // add the filter status to current view
        [self.view addSubview:_filterStatus];
        
        [_filterStatus setHidden:YES];  // hide the filter status label. To be shown only when a filter is applied
    }
    
    return _filterStatus;
}

- (void)sendPasteboardUrlToShareBox:(NSNotification*)notification {
    NSString* url = [[notification userInfo] objectForKey:@"urlString"];
    NSLog(@"URL to Attach: %@", url);
    [[NSUserDefaults standardUserDefaults] setObject:url forKey:@"cache_thought_attached_link"];
    
    // invoke the segue to transition to the thought share box
    [self performSegueWithIdentifier:@"showShareBox" sender:self];
}

- (void)applicationDidReceiveNotificationWhenActive:(NSNotification *)notification {
    NSLog(@"Received Push Notification");
    AudioServicesPlaySystemSound(1007);
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);

    [TeamieGlobals addTSMessageInController:self title:TeamieLocalizedString(@"MSG_TITLE_BULLETIN_NEW", nil) message:TeamieLocalizedString(@"MSG_BULLETIN_NEW", nil) type:@"success" duration:5 withCallback:^{
        BulletinBoardViewController* bulletinVC = [[BulletinBoardViewController alloc] initWithEntries:nil];
        UIBarButtonItem* backButton = [[UIBarButtonItem alloc] initWithTitle:TeamieLocalizedString(@"BTN_BACK", nil) style:UIBarButtonItemStylePlain target:bulletinVC action:@selector(dismissModalViewControllerAnimated:)];
        bulletinVC.navigationItem.leftBarButtonItem = backButton;
        [self presentModalViewController:[[UINavigationController alloc] initWithRootViewController:bulletinVC] animated:YES];
        [[GANTracker sharedTracker] trackEvent:@"Bulletin" action:@"Bulletin Menu Open" label:nil value:0 withError:nil];
    }];
}

@end
