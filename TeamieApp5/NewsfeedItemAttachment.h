//
//  NewsfeedItemAttachment.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/27/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class NewsfeedItem;

@interface NewsfeedItemAttachment : NSManagedObject

@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * href;
@property (nonatomic, retain) NSString * thumbnail;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * iconURL;
@property (nonatomic, retain) NSString * previewURL;
@property (nonatomic, retain) NSNumber * totalVotes;
@property (nonatomic, retain) NSNumber * choiceCount;
@property (nonatomic, retain) NewsfeedItem *newsfeedItem;
@property (nonatomic, retain) NSSet *pollOptions;
@end

@interface NewsfeedItemAttachment (CoreDataGeneratedAccessors)

- (void)addPollOptionsObject:(NSManagedObject *)value;
- (void)removePollOptionsObject:(NSManagedObject *)value;
- (void)addPollOptions:(NSSet *)values;
- (void)removePollOptions:(NSSet *)values;

@end
