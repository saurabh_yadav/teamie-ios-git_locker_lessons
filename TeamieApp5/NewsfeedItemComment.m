//
//  NewsfeedItemComment.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "NewsfeedItemComment.h"
#import "NewsfeedItem.h"
#import "UserProfile.h"


@implementation NewsfeedItemComment

@dynamic cid;
@dynamic created;
@dynamic message;
@dynamic timestampText;
@dynamic author;
@dynamic newsfeedItem;
@dynamic voteCount;
@dynamic isCorrect;
@dynamic markRightAccess;
@dynamic markRightStatus;
@dynamic reportStatus;
@dynamic reportAccess;
@dynamic voteUpStatus;
@dynamic voteDownStatus;
@dynamic htmlMessage;

@end
