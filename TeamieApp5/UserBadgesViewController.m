//
//  UserBadgesViewController.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 9/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UserBadgesViewController.h"

// NDCollectionViewFlowLayout.h
@interface NDCollectionViewFlowLayout : UICollectionViewFlowLayout
@end

// NDCollectionViewFlowLayout.m
@implementation NDCollectionViewFlowLayout
//- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
//    NSArray *attributes = [super layoutAttributesForElementsInRect:rect];
//    NSMutableArray *newAttributes = [NSMutableArray arrayWithCapacity:attributes.count];
//    for (UICollectionViewLayoutAttributes *attribute in attributes) {
//        if ((attribute.frame.origin.x + attribute.frame.size.width <= self.collectionViewContentSize.width) &&
//            (attribute.frame.origin.y + attribute.frame.size.height <= self.collectionViewContentSize.height)) {
//            [newAttributes addObject:attribute];
//        }
//    }
//    return newAttributes;
//}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds{
    return YES;
}

@end
@interface UserBadgesViewController ()
@property (nonatomic) NSArray *badges;
@end

@implementation UserBadgesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithBadges:(NSArray *)badges
{
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"UserProfile" bundle:nil];
    self = (UserBadgesViewController *)[storyBoard instantiateViewControllerWithIdentifier:@"UserBadgesViewController"];
    
    self.badges = badges;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.badges.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    
    cell.backgroundColor = [UIColor blueColor];
    return cell;
}


#pragma mark - UICollectionViewDelegate

//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
//    return 2.0;
//}
//
//- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
//    return 2.0;
//}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
