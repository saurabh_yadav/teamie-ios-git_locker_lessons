//
//  TMEUserProfile.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEModel.h"
#import "TMEUser.h"

@interface TMEUserProfile : TMEModel

@property (nonatomic, strong) TMEUser *user;
@property (nonatomic, strong) NSDictionary *userStats;
@property (nonatomic, strong) NSDictionary *otherInfo;
@property (nonatomic, strong) NSDictionary *personal;
@property (nonatomic, strong) NSArray *badges;

@end
