//
//  PublishStatusLabel.h
//  TeamieApp5
//
//  Created by Wei Wenbo on 3/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#define PublishStatusLeftRightPaddings 5
#define FontSizeForPublishStatus 9

@interface PublishStatusLabel : UILabel
//- (void)configurePublishStatusLabelWithStatus:(PublishStatus)publishStatus;
@end
