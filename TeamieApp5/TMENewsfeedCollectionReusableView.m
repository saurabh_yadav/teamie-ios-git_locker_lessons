//
//  TMENewsfeedCollectionReusableView.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 06/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMENewsfeedCollectionReusableView.h"
#import "UIView+Border.h"

/**
 *  Header Corner Radius
 */
//CGFloat kHeaderCornerRadius = 8.0;

@implementation TMENewsfeedCollectionReusableView

- (void)styleButtons {
    self.lessonsButton.titleLabel.text = TMELocalize(@"menu.lessons");
    self.lessonsButton.titleLabel.font = [TeamieGlobals appFontFor:@"AppFontForPurposeSupplementaryViewText"];
    self.lessonsButton.titleLabel.textColor = TeamieUIGlobalColors.supplementaryViewElementTextColor;
    self.lessonsButton.backgroundColor = [UIColor whiteColor];
    [self.lessonsButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"lesson" withSize:14.0f andColor:TeamieUIGlobalColors.supplementaryViewElementTextColor] forState:UIControlStateNormal];
    self.lessonsButton.tag = 0;
    
    self.assessmentsButton.titleLabel.text = TMELocalize(@"menu.assessments");
    self.assessmentsButton.titleLabel.font = [TeamieGlobals appFontFor:@"AppFontForPurposeSupplementaryViewText"];
    self.assessmentsButton.titleLabel.textColor = TeamieUIGlobalColors.supplementaryViewElementTextColor;
    self.assessmentsButton.backgroundColor = [UIColor whiteColor];
    [self.assessmentsButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"quiz" withSize:14.0f andColor:TeamieUIGlobalColors.supplementaryViewElementTextColor] forState:UIControlStateNormal];
    [self.assessmentsButton drawLeftBorderWithColor:[TeamieUIGlobalColors borderColor].CGColor borderWidth:1.0f];
    self.assessmentsButton.tag = 1;
}

- (void)styleView {
//    self.layer.cornerRadius = kHeaderCornerRadius;
    [self drawBottomBorderWithColor:[TeamieUIGlobalColors borderColor].CGColor borderWidth:1.0];
    
    [self styleButtons];
}

@end
