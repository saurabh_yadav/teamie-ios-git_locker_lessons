//
//  QuestionCellViewController.h
//  TeamieApp5
//
//  Created by Raunak on 7/6/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionCellView.h"
#import "QuestionCellBackViewController.h"
#import "SubmissionQuestion.h"
#import "SubmissionAnswer.h"

@interface QuestionCellFrontViewController : UIViewController

@property (nonatomic,strong) SubmissionQuestion* question;
@property (nonatomic,strong) SubmissionAnswer* answer;
@property (nonatomic,strong) NSNumber* questionNumber;
- (id)initWithSubmissionQuestion:(SubmissionQuestion*)question andAnswer:(SubmissionAnswer*)answer forQuestionNumber:(NSNumber*)qNo;

@end
