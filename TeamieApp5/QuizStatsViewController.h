//
//  QuizStatsViewController.h
//  TeamieApp5
//
//  Created by Raunak on 28/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuizStatistics.h"

@interface QuizStatsViewController : UIViewController

@property (nonatomic, strong) QuizStatistics* stats;
@property (nonatomic, strong) NSString* quizTitle;
- (id)initWithQuizStatistics:(QuizStatistics*)stats andTitle:(NSString*)title;

@end
