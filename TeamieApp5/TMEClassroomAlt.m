//
//  TMEClassroomAlt.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 31/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEClassroomAlt.h"

@implementation TMEClassroomAlt

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    NSMutableDictionary *mapping = [[super JSONKeyPathsByPropertyKey] mutableCopy];
    mapping[@"name"] = @"title";
    return mapping;
}

@end
