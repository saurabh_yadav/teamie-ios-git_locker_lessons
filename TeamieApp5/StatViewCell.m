//
//  StatViewCell.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 9/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "StatViewCell.h"

static const NSString * kDefaultBackgroundBarColor = @"#2492d7";
static const NSString * kDefaultTitleTextColor = @"#3c3c3c";
static const NSString * kDefaultTitleTextShadowColor = @"#ffffff";
static const NSString * kDefaultSubtitleTextColor = @"#3c3c3c";
static const NSString * kDefaultStatTextColor = @"#ffffff";
static const NSString * kDefaultStatTextShadowColor = @"#3c3c3c";
static const NSString * kDefaultStatTextBackgroundColor = @"#000000";
static const NSString * kDefaultStatSubtitleTextColor = @"#3c3c3c";
static const NSString * kDefaultCellBackgroundColor = @"#f1f2f3";

#define kDefaultTextShadowSize CGSizeMake(0.0, 2.0)
#define kDefaultBackgroundBarRadius 5.0
#define kTableCellSmallMargin 6.0f
#define kTableCellMargin 10.0f

@interface StatViewCell()

@property (nonatomic, strong) UILabel * subtitleTextLabel;
@property (nonatomic, strong) UILabel * statTextLabel;
@property (nonatomic, strong) UILabel * statSubtextLabel;
@property (nonatomic, strong) UIView * backgroundBar;

@end

@implementation StatViewCell

#pragma mark - UITableViewCell methods
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.textLabel.textColor = [UIColor colorWithHex:(NSString*)kDefaultTitleTextColor alpha:1.0];
        self.textLabel.adjustsFontSizeToFitWidth = YES;
        self.textLabel.minimumScaleFactor = 8.0 / [UIFont labelFontSize];
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Public methods
- (void)setCellData:(StatViewItem *)item{
    CGFloat tableCellSmallMargin = kTableCellSmallMargin;
    
    if (item.text != (id)[NSNull null] && item.text.length) {
        self.textLabel.text = item.text;
        self.textLabel.adjustsFontSizeToFitWidth = YES;
        self.textLabel.minimumScaleFactor = 8.0 / [UIFont labelFontSize];
    }
    if (item.subtitle != (id)[NSNull null] && item.subtitle.length) {
        self.subtitleTextLabel.text = item.subtitle;
    }
    if (item.statText != (id)[NSNull null] && item.statText.length) {
        self.statTextLabel.text = item.statText;
    }
    if (item.statSubtext != (id)[NSNull null] && item.statSubtext.length) {
        self.statSubtextLabel.text = item.statSubtext;
    }
    if ((![item.barWidth isEqualToNumber:[NSNumber numberWithFloat:0.0]]) && (![item.barWidth isEqualToNumber:[NSDecimalNumber notANumber]])) {
        self.backgroundBar.frame = CGRectMake(tableCellSmallMargin, tableCellSmallMargin, (self.contentView.frame.size.width - 2 * tableCellSmallMargin) * [item.barWidth floatValue], self.contentView.frame.size.height - 2 * tableCellSmallMargin);
    }
    else {
        self.backgroundBar.frame = CGRectMake(0.0, 0.0, 0.0, self.contentView.frame.size.height);
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat left = kTableCellSmallMargin + kTableCellSmallMargin;
    CGFloat top = (self.contentView.frame.size.height - self.textLabel.font.lineHeight) / 2.0;
    // if subtitle is also present then move the top value accordingly
    if (self.subtitleTextLabel.text.length) {
        top = (self.contentView.frame.size.height - self.textLabel.font.lineHeight - self.subtitleTextLabel.font.lineHeight) / 2.0;
    }
    CGFloat width = self.contentView.frame.size.width - (2 * kTableCellMargin) - (2 * kTableCellSmallMargin);
    
    // each cell is split into two parts using 80 / 20 ratio.
    // 80% of the width is for the title while 20% of the width is for the numbers
    
    if (self.textLabel.text.length) {
        self.textLabel.frame = CGRectMake(left, top, width * 0.8, self.textLabel.font.lineHeight);
        self.textLabel.textColor = [UIColor colorWithHex:(NSString*)kDefaultTitleTextColor alpha:1.0];
        self.textLabel.shadowColor = [UIColor colorWithHex:@"#CCCCCC" alpha:0.5];
        self.textLabel.shadowOffset = kDefaultTextShadowSize;
        self.textLabel.font = [TeamieGlobals appFontFor:@"heading2"];
        self.textLabel.adjustsFontSizeToFitWidth = YES;
        self.textLabel.minimumScaleFactor = 8.0 / [UIFont labelFontSize];
        top = self.textLabel.frame.origin.y;
    } else {
        self.textLabel.frame = CGRectZero;
    }
    
    if (self.subtitleTextLabel.text.length) {
        self.subtitleTextLabel.frame = CGRectMake(left, top + self.textLabel.font.lineHeight, width * 0.8, self.subtitleTextLabel.font.lineHeight);
    }
    
    left += self.textLabel.frame.size.width;
    
    if (self.statTextLabel.text.length) {
        self.statTextLabel.frame = CGRectMake(left, top, width * 0.2, self.statTextLabel.font.lineHeight);
        top += self.statTextLabel.frame.size.height;
    }
    
    if (self.statSubtextLabel.text.length) {
        self.statSubtextLabel.frame = CGRectMake(left, top, width * 0.2, self.statSubtextLabel.font.lineHeight);
        top += self.statSubtextLabel.font.lineHeight;
    }
    
    // if we hav a background bar make it have some margin
    // so that bars in the adjacent rows do not touch each other
    if (!CGRectIsEmpty(self.backgroundBar.frame)) {
        CGRect bgBarFrame = self.backgroundBar.frame;
        bgBarFrame.size.height = self.contentView.frame.size.height - 2 * kTableCellSmallMargin;
        self.backgroundBar.frame = bgBarFrame;
        // set the top border width to be the content view width
        for (CALayer* layer in self.backgroundBar.layer.sublayers) {
            if ([layer.name isEqualToString:@"topborder"]) {
                CGRect layerFrame = layer.frame;
                layerFrame.size.width = self.contentView.frame.size.width;
                layer.frame = layerFrame;
                break;
            }
        }
    }
    
    [self.contentView sendSubviewToBack:self.backgroundBar];
    self.contentView.backgroundColor = [UIColor clearColor];
}

#pragma mark - Lazy Instantiation Methods
- (UIView*)backgroundBar {
    if (!_backgroundBar) {
        _backgroundBar = [[UIView alloc] init];
        _backgroundBar.backgroundColor = [UIColor colorWithHex:(NSString*)kDefaultBackgroundBarColor alpha:1.0];
        _backgroundBar.layer.cornerRadius = kDefaultBackgroundBarRadius;
        _backgroundBar.layer.masksToBounds = YES;
        CALayer *topBorder = [CALayer layer];
        topBorder.frame = CGRectMake(0.0f, 0.0f, self.contentView.frame.size.width, 2.0f);
        // set a name to this top border layer so you can resize it in the layoutSubviews method
        [topBorder setName:@"topborder"];
        topBorder.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.7].CGColor;
        
        [_backgroundBar.layer addSublayer:topBorder];
        [self.contentView addSubview:_backgroundBar];
        // make the background bar sit right at the back... the troublesome kid!
        [self.contentView sendSubviewToBack:_backgroundBar];
    }
    return _backgroundBar;
}

- (UILabel*)subtitleTextLabel {
    if (!_subtitleTextLabel) {
        _subtitleTextLabel = [[UILabel alloc] init];
        _subtitleTextLabel.textColor = [UIColor colorWithHex:(NSString*)kDefaultSubtitleTextColor alpha:1.0];
        _subtitleTextLabel.backgroundColor = [UIColor clearColor];
        _subtitleTextLabel.font = [TeamieGlobals appFontFor:@"cell-meta"];
        _subtitleTextLabel.adjustsFontSizeToFitWidth = YES;
        _subtitleTextLabel.minimumScaleFactor = 7.0 / [UIFont labelFontSize];
        [self.contentView addSubview:_subtitleTextLabel];
    }
    return _subtitleTextLabel;
}

- (UILabel*)statTextLabel {
    if (!_statTextLabel) {
        _statTextLabel = [[UILabel alloc] init];
        _statTextLabel.textColor = [UIColor colorWithHex:(NSString*)kDefaultStatTextColor alpha:1.0];
        _statTextLabel.textAlignment = NSTextAlignmentCenter;
        _statTextLabel.backgroundColor = [UIColor colorWithHex:(NSString*)kDefaultStatTextBackgroundColor alpha:0.4];
        _statTextLabel.layer.cornerRadius = kDefaultBackgroundBarRadius;
        _statTextLabel.layer.masksToBounds = YES;
        _statTextLabel.font = [TeamieGlobals appFontFor:@"numberStat-big"];
        _statTextLabel.shadowColor = [UIColor colorWithHex:(NSString*)kDefaultStatTextShadowColor alpha:1.0];
        _statTextLabel.shadowOffset = kDefaultTextShadowSize;
        _statTextLabel.layer.shadowColor = [UIColor whiteColor].CGColor;
        _statTextLabel.layer.shadowOffset = kDefaultTextShadowSize;
        _statTextLabel.adjustsFontSizeToFitWidth = YES;
        _statTextLabel.minimumScaleFactor = 7.0 / [UIFont labelFontSize];
        [self.contentView addSubview:_statTextLabel];
    }
    return _statTextLabel;
}

- (UILabel*)statSubtextLabel {
    if (!_statSubtextLabel) {
        _statSubtextLabel = [[UILabel alloc] init];
        _statSubtextLabel.textColor = [UIColor colorWithHex:(NSString*)kDefaultStatSubtitleTextColor alpha:1.0];
        _statSubtextLabel.textAlignment = NSTextAlignmentCenter;
        _statSubtextLabel.backgroundColor = [UIColor clearColor];
        _statSubtextLabel.font = [TeamieGlobals appFontFor:@"cell-meta"];
        _statSubtextLabel.adjustsFontSizeToFitWidth = YES;
        _statSubtextLabel.minimumScaleFactor = 7.0 / [UIFont labelFontSize];
        
        [self.contentView addSubview:_statSubtextLabel];
    }
    return _statSubtextLabel;
}

@end
