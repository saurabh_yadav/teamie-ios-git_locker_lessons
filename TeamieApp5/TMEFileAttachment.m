//
//  TMEFileAttachment.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEFileAttachment.h"

@implementation TMEFileAttachment

+ (NSValueTransformer *)fidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

- (UIImage *)previewImage {
    return [TeamieUIGlobals defaultPicForPurpose:@"file"
                                        withSize:15
                                        andColor:[TeamieUIGlobalColors defaultTextColor]];
}

- (NSString *)displaySubTitle {
    return @"Document";
}

@end
