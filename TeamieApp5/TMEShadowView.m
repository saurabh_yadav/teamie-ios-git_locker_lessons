//
//  TMEShadowView.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 19/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEShadowView.h"

@implementation TMEShadowView

- (instancetype)init {
    self = [super init];
    if (self) {
        self.layer.masksToBounds = NO;
        self.layer.shadowColor = [[UIColor colorWithHex:@"#A2A2A2" alpha:1.0] CGColor];
        self.layer.shadowOffset = CGSizeMake(0.0f, 1.5f);
        self.layer.shadowOpacity = 0.7f;
        self.layer.shadowRadius = 0.0f;
    }
    return self;
}

@end
