//
//  QuizStatCellView.m
//  TeamieApp5
//
//  Created by Raunak on 28/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "QuizStatCellView.h"
#import <QuartzCore/QuartzCore.h>

@implementation QuizStatCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+(QuizStatCellView*)cellWithText:(NSString *)text value:(NSNumber *)value baseValue:(NSNumber *)baseValue {
    NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"QuizStatCellView" owner:nil options:nil];
    QuizStatCellView *cell = [[QuizStatCellView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    cell = [views objectAtIndex:0];
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@: %.1f", text, [value floatValue]];
    
    cell.statBar.baseColor = [UIColor lightGrayColor];
    cell.statBar.barColor = [UIColor blueColor];
    cell.statBar.barRatio = [value doubleValue]  / [baseValue doubleValue];
    
    // Create the path (with only the top-left corner rounded)
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:cell.statBar.bounds
                                                   byRoundingCorners:UIRectCornerAllCorners
                                                         cornerRadii:CGSizeMake(15.0, 15.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = cell.statBar.bounds;
    maskLayer.path = maskPath.CGPath;
    
    // Set the newly created shape layer as the mask for the image view's layer
    cell.statBar.layer.mask = maskLayer;
    
//    cell.statBar.layer.mask.borderColor = [UIColor blackColor].CGColor;
//    cell.statBar.layer.mask.borderWidth = 1.0;
    
    cell.layer.borderColor = [UIColor blackColor].CGColor;
    cell.layer.borderWidth = 1.0;
    cell.layer.cornerRadius = 10.0;
    
    return cell;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
