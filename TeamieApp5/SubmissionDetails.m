//
//  SubmissionDetails.m
//  TeamieApp5
//
//  Created by Raunak on 23/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "SubmissionDetails.h"

@implementation SubmissionDetails

@synthesize user;
@synthesize attemptNumber;
@synthesize submittedTime;
@synthesize isDelayed;
@synthesize completionTime;
@synthesize totalScore;
@synthesize isGraded;
@synthesize submissionAnswers;

@end
