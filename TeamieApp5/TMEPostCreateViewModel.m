//
//  TMEPostShareViewModel.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 2/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostCreateViewModel.h"
#import "TMEClassroom.h"

#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation TMEPostCreateViewModel

NSString * const CLASSROOMS_PREFIX = @"To:";
NSString * const DEADLINE_PREFIX = @"Due on:";
NSString * const DEADLINE_DATE_FORMAT = @"EEEE d MMM h:mm a";
NSString * const ALL_DAY_DATE_FORMAT = @"EEEE d MMM";

- (instancetype)init {
    self = [super init];
    if (self) {
        [self initialize];
    }
    return self;
}

- (void)initialize {
    self.postContentText = @"";
    self.selectedClassrooms = [NSMutableArray new];
    self.anonymous = NO;
    self.announcement = NO;
    self.notificationAnnouncement = YES;
    self.emailAnnouncement = NO;
    self.restrictComments = NO;
    self.postType = TMEPostThought;
    self.deadline = [NSDate date];
    self.allDay = NO;
    self.attachments = [NSMutableArray new];
}

- (NSAttributedString *)classroomsText {
    NSString *nonFormatted;
    if (self.selectedClassrooms.count == 0) {
        nonFormatted = CLASSROOMS_PREFIX;
    }
    else if (self.selectedClassrooms.count == 1) {
        nonFormatted = [NSString stringWithFormat:@"%@ %@",
                CLASSROOMS_PREFIX,
                ((TMEClassroom *)self.selectedClassrooms.firstObject).name];
    }
    else {
        nonFormatted = [NSString stringWithFormat:@"%@ %@    + %@ more",
                CLASSROOMS_PREFIX,
                ((TMEClassroom *)self.selectedClassrooms.firstObject).name,
                @(self.selectedClassrooms.count - 1)];
    }
    NSMutableAttributedString *formatted = [[NSMutableAttributedString alloc] initWithString:nonFormatted];
    [formatted setAttributes:@{NSFontAttributeName: [TeamieGlobals appFontFor:@"actionButton"]}
                       range:NSMakeRange(0, CLASSROOMS_PREFIX.length)];
    return formatted;

}

- (NSAttributedString *)deadlineText {
    NSDateFormatter *formatter = [NSDateFormatter new];
    formatter.dateFormat = self.allDay? ALL_DAY_DATE_FORMAT : DEADLINE_DATE_FORMAT;
    NSString *nonFormatted = [NSString stringWithFormat:@"%@  %@%@",
                              DEADLINE_PREFIX,
                              [formatter stringFromDate:_deadline],
                              self.allDay? @" (All Day)" : @""];
    NSMutableAttributedString *formatted = [[NSMutableAttributedString alloc] initWithString:nonFormatted];
    [formatted setAttributes:@{NSFontAttributeName: [TeamieGlobals appFontFor:@"actionButton"]}
                       range:NSMakeRange(0, DEADLINE_PREFIX.length)];
    return formatted;
}

- (NSString *)attachmentsText {
    return [NSString stringWithFormat:@"%lu %@",
            (unsigned long)self.attachments.count,
            self.attachments.count > 1? @"Attachments" : @"Attachment"];
}

@end