//
//  TeamieUITextField.m
//  TeamieApp5
//
//  Created by Teamie on 17/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TeamieUITextField.h"

@implementation TeamieUITextField

// placeholder position
- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 10 );
}

// text position
- (CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 10 );
}

@end
