//
//  CollapseClickView.m
//  CollapseClick
//
//  Created by Ben Gordon on 2/28/13.
//  Copyright (c) 2013 Ben Gordon. All rights reserved.
//

#import "CollapseClickView.h"

@implementation CollapseClickView

@synthesize CollapseClickDelegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.isClickedArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
        self.dataArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
        self.displayArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
        [self reloadCollapseClick];
    }
    return self;
}

#pragma mark - Load Data
-(void)reloadCollapseClick {
    if ([CollapseClickDelegate numberOfCellsForCollapseClick] == 0) {
//        TKEmptyView* view = [[TKEmptyView alloc] initWithFrame:self.bounds emptyViewImage:TKEmptyViewImageSearch title:@"No Submissions Found" subtitle:@""];
//        [self addSubview:view];
    }
    else {
        // Set Up: Height
        float totalHeight = 10;
        
        // If Arrays aren't Init'd, Init them
        if (!(self.isClickedArray)) {
            self.isClickedArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
        }
        
        if (!(self.dataArray)) {
            self.dataArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
        }
        
        if (!(self.displayArray)) {
            self.displayArray = [[NSMutableArray alloc] initWithCapacity:[CollapseClickDelegate numberOfCellsForCollapseClick]];
        }
        
        // Make sure they are clear
        [self.isClickedArray removeAllObjects];
        [self.dataArray removeAllObjects];
        [self.displayArray removeAllObjects];
        
        // Remove all subviews
        for (UIView *subview in self.subviews) {
            [subview removeFromSuperview];
        }
        
        // Add cells
        for (int xx = 0; xx < [CollapseClickDelegate numberOfCellsForCollapseClick]; xx++) {
            // Create Cell
            CollapseClickCellView *cell = [CollapseClickCellView newCollapseClickCellWithDetails:[CollapseClickDelegate detailsForCollapseClickAtIndex:xx] maxScore:[CollapseClickDelegate maxScoreForCollapseClickAtIndex:xx] index:xx];
            
            // Set cell's size
            cell.frame = CGRectMake(10, totalHeight, cell.frame.size.width, kCCHeaderHeight);
            
            // Set cell.ContentView's size
            cell.contentView.frame = CGRectMake(10, kCCHeaderHeight, CGRectGetWidth(cell.frame) - 20, cell.contentView.frame.size.height);
            
            // Add target to Button
            [cell.headerButton addTarget:self action:@selector(didSelectCollapseClickButton:) forControlEvents:UIControlEventTouchUpInside];
            
            // Add cell
            [self addSubview:cell];
            
            // Add to DataArray & isClickedArray
            [self.isClickedArray addObject:[NSNumber numberWithBool:NO]];
            [self.dataArray addObject:cell];
            [self.displayArray addObject:cell];
            
            // Calculate totalHeight
            totalHeight += kCCHeaderHeight + kCCPad;
        }
        
        // Set self's ContentSize and ContentOffset
        [self setContentSize:CGSizeMake(self.frame.size.width, totalHeight)];
        [self setContentOffset:CGPointZero];
    }
    [CollapseClickDelegate didFinishLoad];
}



#pragma mark - Reposition Cells
-(void)repositionCollapseClickCellsBelowIndex:(int)index withOffset:(float)offset {
    for (int yy = index+1; yy < self.dataArray.count; yy++) {
        CollapseClickCellView *cell = [self.dataArray objectAtIndex:yy];
        cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y + offset, cell.frame.size.width, cell.frame.size.height);
    }
    
    // Resize self.ContentSize
    CollapseClickCellView *lastCell = [self.dataArray objectAtIndex:self.dataArray.count - 1];
    [self setContentSize:CGSizeMake(self.frame.size.width, lastCell.frame.origin.y + lastCell.frame.size.height + kCCPad)];
}


#pragma mark - Did Click
-(void)didSelectCollapseClickButton:(UIButton *)titleButton {
    BOOL isOpen = NO;
    
    // Cell is OPEN -> CLOSED
    if ([[self.isClickedArray objectAtIndex:titleButton.tag] boolValue] == YES) {
        [self closeCollapseClickCellAtIndex:titleButton.tag animated:YES];
    }
    // Cell is CLOSED -> OPEN
    else {
        [self openCollapseClickCellAtIndex:titleButton.tag animated:YES];
        isOpen = YES;
    }
    
    // Call delegate method
    if ([(id)CollapseClickDelegate respondsToSelector:@selector(didClickCollapseClickCellAtIndex:isNowOpen:)]) {
        [CollapseClickDelegate didClickCollapseClickCellAtIndex:titleButton.tag isNowOpen:isOpen];
    }
}

#pragma mark - Open CollapseClickCell
-(void)openCollapseClickCellAtIndex:(int)index animated:(BOOL)animated {
    // Check if it's not open first
    if ([[self.isClickedArray objectAtIndex:index] boolValue] != YES) {
        float duration = 0;
        if (animated) {
            duration = 0.25;
        }
        CollapseClickCellView *cell = [self.dataArray objectAtIndex:index];
        [cell.contentView addSubview:[CollapseClickDelegate detailsForCollapseClickAtIndex:index].view
         ];

        [UIView animateWithDuration:duration animations:^{
            // Resize Cell
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, cell.contentView.frame.origin.y + cell.contentView.frame.size.height + kCCPad);
            
            // Change Arrow orientation
//            CGAffineTransform transform = CGAffineTransformMakeRotation(M_PI/2);
//            cell.arrow.transform = transform;
            
            // Change isClickedArray
            [self.isClickedArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:YES]];
            
            // Reposition all CollapseClickCells below Cell
            [self repositionCollapseClickCellsBelowIndex:index withOffset:cell.contentView.frame.size.height + kCCPad];
            
        }];
    }
}

-(void)openCollapseClickCellsWithIndexes:(NSArray *)indexArray animated:(BOOL)animated {
    // This works off of NSNumbers for each Index
    for (int ii = 0; ii < indexArray.count; ii++) {
        id indexID = indexArray[ii];
        if ([indexID isKindOfClass:[NSNumber class]]) {
            [self openCollapseClickCellAtIndex:[indexID intValue] animated:animated];
        }
    }
    
}



#pragma mark - Close CollapseClickCell
-(void)closeCollapseClickCellAtIndex:(int)index animated:(BOOL)animated {
    // Check if it's open first
    if ([[self.isClickedArray objectAtIndex:index] boolValue] == YES) {
        float duration = 0;
        if (animated) {
            duration = 0.25;
        }
        CollapseClickCellView *cell = [self.dataArray objectAtIndex:index];
        [UIView animateWithDuration:duration animations:^{
            // Resize Cell
            cell.frame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y, cell.frame.size.width, kCCHeaderHeight);
            
            // Change Arrow orientation
//            CGAffineTransform transform = CGAffineTransformMakeRotation(0);
//            cell.arrow.transform = transform;
            
            // Change isClickedArray
            [self.isClickedArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:NO]];
            
            // Reposition all CollapseClickCells below Cell
            [self repositionCollapseClickCellsBelowIndex:index withOffset:-1*(cell.contentView.frame.size.height + kCCPad)];
        }];
        for (__strong UIView *subview in cell.contentView.subviews) {
            [subview removeFromSuperview];
            subview = nil;
        }
    }
}

-(void)closeCollapseClickCellsWithIndexes:(NSArray *)indexArray animated:(BOOL)animated {
    // This works off of NSNumbers for each Index
    for (int ii = 0; ii < indexArray.count; ii++) {
        id indexID = indexArray[ii];
        if ([indexID isKindOfClass:[NSNumber class]]) {
            [self closeCollapseClickCellAtIndex:[indexID intValue] animated:animated];
        }
    }
}


#pragma mark - CollapseClickCell for Index
-(CollapseClickCellView *)collapseClickCellForIndex:(int)index {
    if ([[self.dataArray objectAtIndex:index] isKindOfClass:[CollapseClickCellView class]]) {
        return [self.dataArray objectAtIndex:index];
    }
    
    return nil;
}


#pragma mark - Scroll To Cell
-(void)scrollToCollapseClickCellAtIndex:(int)index animated:(BOOL)animated {
    CollapseClickCellView *cell = [self.dataArray objectAtIndex:index];
    [self setContentOffset:CGPointMake(cell.frame.origin.x, cell.frame.origin.y) animated:animated];
}


#pragma mark - Content View for Cell
-(UIView *)contentViewForCellAtIndex:(int)index {
    CollapseClickCellView *cell = [self.subviews objectAtIndex:index];
    return cell.contentView;
}

@end
