//
//  QuizStatView.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 10/23/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuizStatView : UIView

@property (weak, nonatomic) IBOutlet UILabel *totalScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *highScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *lowScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *avgScoreLabel;

@end
