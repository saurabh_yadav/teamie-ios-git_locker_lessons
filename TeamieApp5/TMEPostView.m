//
//  TMEPostView.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 17/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostView.h"
#import "UITextView+SizeAdjustment.h"

#import "TMEClient.h"
#import "TMEHomework.h"
#import "TMEQuestion.h"
#import "TMEThought.h"
#import "TMEPostAttachmentView.h"
#import "TMEPostAttachment.h"
#import "TMELinkAttachment.h"
#import "TMEImageAttachment.h"
#import "TMEFileAttachment.h"
#import "TMEVideoAttachment.h"
#import "TMEAudioAttachment.h"
#import "TMEPostViewController.h"
#import "UserProfileViewController.h"
#import "TMEClassroomNewsfeedViewController.h"
#import "TMEPostComment.h"
#import "TMEPostCommentView.h"
#import "TMEPollAttachment.h"
#import "TMEPostPollAttachmentViewController.h"

#import "UIImage+TMEColor.h"
#import "UIView+TMEEssentials.h"
#import "UILabelWithPadding.h"

#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/UIButton+WebCache.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import <MediaPlayer/MediaPlayer.h>

#import "TMEPostImageAttachmentView.h"
#import "ThoughtAttachmentView.h"

#import "TMECardTableViewCell.h"
#import "TMEUser.h"

@interface TMEPostView()
<UIActionSheetDelegate, TMEPostAttachmentViewDelegate, UITextViewDelegate, TMEPollAttachmentDelegate, UIWebViewDelegate>

#pragma mark - SubViews
@property (nonatomic, weak) UIView *headerView;
@property (nonatomic, weak) UIButton *authorButton;
@property (nonatomic, weak) UILabel *authorLabel;
@property (nonatomic, weak) UILabel *postTimeLabel;
@property (nonatomic, weak) UILabel *classroomLabel;
@property (nonatomic, weak) UIImageView *postTypeImageView;

@property (nonatomic, weak) UITextView *postTextView;
@property (nonatomic, strong) UIView *richTextView;

@property (nonatomic, weak) UIView *deadlineView;
@property (nonatomic, weak) UILabel *deadlineLabel;

@property (nonatomic, weak) UIView *pollAttachmentView;

@property (nonatomic, weak) UIView *attachmentsView;
@property (nonatomic, weak) UIView *imageAttachmentsView;

@property (nonatomic, weak) UIView *actionBarView;
@property (nonatomic, weak) UIButton *likeButton;
@property (nonatomic, weak) UIButton *sameButton;
@property (nonatomic, weak) UIButton *commentButton;
@property (nonatomic, weak, readwrite) UIButton *moreButton;
@property (nonatomic, weak) UILabel *seenPostLabel;
@property (nonatomic, weak) UILabelWithPadding *postStatusLabel;
@property (nonatomic, strong) UIButton *showMoreButton;
@property (nonatomic) BOOL showMoreState;
@property (nonatomic) BOOL richTextViewState;

#pragma mark - Variables
@property (nonatomic, strong) TMEPost *post;
@property (nonatomic) BOOL constraintsAdded;
@property (nonatomic) BOOL showDeadline;
@property (nonatomic) BOOL showPollAttachment;
@property (nonatomic) TMEPostViewMode postViewMode;
@property (nonatomic, strong) TMEPostAction *delete;
@property (nonatomic, strong) TMEPostAction *report;
@property (nonatomic,strong) NSMutableDictionary *showMoreOnPostID;

@end

int seenCount;

CGFloat newWebViewHeight;
#pragma mark -

@implementation TMEPostView

#pragma mark - Layout Constants
static const CGFloat kDefaultSizeWithPadding = 38;
static const CGFloat kDefaultSize = 18;
static const CGFloat kDefaultPadding = 10.0f;
const CGFloat kTextPadding = 10.0f;
static UIEdgeInsets kDefaultInsets;
static const NSInteger kActionSheetTag = 1;
static NSInteger kReportPostAction = -1;
static NSInteger kMoreCommentsAction = -1;
static NSString * kTrackingCategory;

#pragma mark - Lifecycle

- (instancetype)initWithMode:(TMEPostViewMode)mode {
    self = [super init];
    if (self) {
        //MARK: Initialize constants
        kDefaultInsets = UIEdgeInsetsMake(kDefaultPadding, kDefaultPadding, kDefaultPadding, kDefaultPadding);
        self.postViewMode = mode;
        kTrackingCategory = mode == TMEPostViewModeFull? @"full_post": @"newsfeed";
        
        //MARK: Initialize variables
        self.constraintsAdded = NO;
        self.backgroundColor = [UIColor whiteColor];
        
        //MARK: Add Subviews
        self.translatesAutoresizingMaskIntoConstraints = NO;
        [self createHeaderView];
        [self createShowMoreButton];
        [self createPostView];
        [self createRichTextView];
        [self createImageAttachmentsView];
        [self createDeadlineView];
        [self createPollAttachment];
        [self createAttachmentsView];
        [self createActionBarView];
        seenCount = 1;
        
    }
    return self;
}

- (instancetype)initWithMode:(TMEPostViewMode)mode andShowOption:(BOOL)showMore andShowOnPostID:(NSMutableDictionary*)showMoreforPostID{
    self = [self initWithMode:mode];
    self.showMore = showMore;
    self.showMoreOnPostID = showMoreforPostID;
    return self;
}

- (void)updateConstraints {
    MASAttachKeys(
      self.headerView,
      self.authorLabel,
      self.authorButton,
      self.deadlineView,
      self.deadlineLabel,
      self.classroomLabel,
      self.postTimeLabel,
      self.postTypeImageView,
      self.postTextView,
      self.imageAttachmentsView,
      self.actionBarView,
      self.likeButton,
      self.sameButton,
      self.commentButton,
      self.commentsCountButton,
      self.moreButton,
      self.postTimeLabel,
      self.attachmentsView,
      self.actionBarView,
      self.pollAttachmentView
    );
    
    if (!self.constraintsAdded) {
        [self.headerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.top.right.equalTo(self);
            make.height.equalTo(@55);
        }];
        [self.authorButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.headerView).with.offset(kDefaultPadding);
            make.top.equalTo(self.headerView).with.offset(kDefaultPadding);
            make.bottom.equalTo(self.headerView).with.offset(-kDefaultPadding);
            make.width.height.equalTo(@35);
        }];
        [self.authorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.authorButton.mas_right).with.offset(kDefaultPadding);
            make.top.equalTo(self.headerView).with.offset(kDefaultPadding).key(@"authorLabelTopOffset");
            [self.authorLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        }];
        [self.postStatusLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.authorLabel.mas_right).with.offset(6);
            make.top.equalTo(self.headerView).with.offset(13);
            [self.postStatusLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        }];
        [self.postTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.authorLabel.mas_bottom);
            make.left.equalTo(self.authorButton.mas_right).with.offset(kDefaultPadding);
            make.height.equalTo(@15);
            [self.postTimeLabel setContentCompressionResistancePriority:UILayoutPriorityRequired forAxis:UILayoutConstraintAxisHorizontal];
        }];
        [self.classroomLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.authorLabel.mas_bottom);
            make.left.equalTo(self.postTimeLabel.mas_right);
            [self.classroomLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh forAxis:UILayoutConstraintAxisHorizontal];
        }];
        [self.postTypeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.greaterThanOrEqualTo(self.authorLabel.mas_right).with.offset(kDefaultPadding);
            make.left.greaterThanOrEqualTo(self.classroomLabel.mas_right).with.offset(kDefaultPadding);
            make.right.equalTo(self.headerView).with.offset(-10);
            make.top.equalTo(self.headerView).offset(10).priorityLow();
        }];
        [self.seenPostLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.greaterThanOrEqualTo(self.classroomLabel.mas_right).with.offset(kDefaultPadding);
            make.top.equalTo(self.postTypeImageView.mas_bottom).offset(5);
            make.right.equalTo(self.headerView).with.offset(-kDefaultPadding);
            make.centerX.equalTo(self.postTypeImageView).centerOffset(CGPointMake(-10, 0));
            make.width.equalTo(@30);
        }];
        [self.postTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(kTextPadding);
            make.right.equalTo(self).with.offset(-kTextPadding);
            make.top.equalTo(self.headerView.mas_bottom).with.offset(kTextPadding - 2);
            if (self.post.images) {
                make.bottom.equalTo(self.showMoreButton.mas_top).priorityLow();
            }
            else{
                make.bottom.equalTo(self.showMoreButton.mas_top).priorityHigh();
            }
        }];
        [self.showMoreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            if (self.showMoreState){
            make.top.equalTo(self.postTextView.mas_bottom).offset(4);
            make.left.equalTo(self).offset(kTextPadding);
            make.width.equalTo(@80);
            make.height.equalTo(@18);
            make.bottom.equalTo(self.richTextView.mas_top).offset(-8);
            }
            else{
                [self.showMoreButton setHidden:YES];
            }
        }];
        [self.richTextView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).with.offset(kTextPadding-5);
            make.right.equalTo(self).with.offset(-kTextPadding-5);
            if (self.showMore) {
                make.top.equalTo(self.showMoreButton.mas_bottom).with.offset(kTextPadding - 2);
            }
            else{
                make.top.equalTo(self.postTextView.mas_bottom).with.offset(kTextPadding - 2);
            }
        }];
        [self.deadlineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self.richTextView.mas_bottom).with.offset(kTextPadding - 2);
        }];
        [self.deadlineLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.deadlineView).with.insets(kDefaultInsets).priorityHigh();
        }];
        [self.pollAttachmentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self.deadlineView.mas_bottom);
        }];
        [self.imageAttachmentsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.headerView);
            make.width.height.equalTo(@0).priorityLow();
            make.top.equalTo(self.pollAttachmentView.mas_bottom);
        }];
        [self.attachmentsView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self.imageAttachmentsView.mas_bottom);
            make.height.equalTo(@0).priorityLow();
        }];
        [self.actionBarView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.equalTo(self);
            make.top.equalTo(self.attachmentsView.mas_bottom);
            make.height.equalTo(@(kDefaultSizeWithPadding));
        }];
        [self.likeButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.left.bottom.equalTo(self.actionBarView);
        }];
        [self.sameButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self.actionBarView);
            make.left.equalTo(self.likeButton.mas_right);
        }];
        [self.commentsCountButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self.actionBarView);
            make.left.equalTo(self.sameButton.mas_right);
            make.width.equalTo(@((kDefaultSize + kDefaultPadding) * 2));
        }];
        [self.commentButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.equalTo(self.actionBarView);
            make.left.equalTo(self.commentsCountButton.mas_right);
        }];
        [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.right.equalTo(self.actionBarView);
            make.left.equalTo(self.commentButton.mas_right);
        }];
        [self.commentButton mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.lessThanOrEqualTo(self.mas_width);
        }];
        self.constraintsAdded = YES;
    }
    [self.deadlineView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(self.showDeadline? kDefaultSizeWithPadding:0));
    }];
    
    [self.pollAttachmentView mas_updateConstraints:^(MASConstraintMaker *make) {
        if (self.showPollAttachment) {
            if (self.post.poll.isFooterVisible) {
                make.height.equalTo(@(((self.post.poll.choiceCount.intValue + 1) * 50) + 23));
            }
            else {
                make.height.equalTo(@((self.post.poll.choiceCount.intValue * 50) + 23));
            }
        } else {
            make.height.equalTo(@(0));
        }
    }];
    
    [self.likeButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(self.post.type == TMEPostThought? (kDefaultSize + kDefaultPadding) * 2 : 0));
    }];
    [self.sameButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(self.post.type == TMEPostQuestion? (kDefaultSize + kDefaultPadding) * 2 : 0));
    }];
    [self.moreButton mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(@(self.postViewMode != TMEPostViewModeFull && (self.delete.access || self.report.access)?
                           (kDefaultSize + kDefaultPadding) * 2 : 0));
    }];
    [super updateConstraints];
}

#pragma mark - Adding Subviews

- (void)createHeaderView {
    self.headerView = [self createAndAddSubView:UIView.class];
    
    self.authorButton = [self.headerView createAndAddSubView:UIButton.class];
    self.authorButton.contentMode = UIViewContentModeScaleAspectFill;
    self.authorButton.layer.cornerRadius = 3.0f;
    self.authorButton.clipsToBounds = YES;
    [self.authorButton addTarget:self action:@selector(openAuthorProfile) forControlEvents:UIControlEventTouchUpInside];
    
    self.authorLabel = [self.headerView createAndAddSubView:UILabel.class];
    self.authorLabel.numberOfLines = 1;
    self.authorLabel.font = [TeamieGlobals appFontFor:@"postTitle"];
    
    self.postTimeLabel = [self.headerView createAndAddSubView:UILabel.class];
    self.postTimeLabel.font = [TeamieGlobals appFontFor:@"postSubtitleTimestamp"];
    self.postTimeLabel.numberOfLines = 1;
    
    self.classroomLabel = [self.headerView createAndAddSubView:UILabel.class];
    [self.classroomLabel addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openClassroom)]];
    self.classroomLabel.numberOfLines = 1;
    self.classroomLabel.userInteractionEnabled = YES;
    
    self.seenPostLabel = [self.headerView createAndAddSubView:UILabel.class];

    self.postStatusLabel = [self.headerView createAndAddSubView:UILabelWithPadding.class];
    
    self.postTypeImageView = [self.headerView createAndAddSubView:UIImageView.class];
    self.postTypeImageView.contentMode = UIViewContentModeScaleAspectFill;
}

- (void)createPostView {
    self.postTextView = [self createAndAddSubView:UITextView.class];
    self.postTextView.scrollEnabled = NO;
    self.postTextView.editable = NO;
    self.postTextView.font = [TeamieGlobals appFontFor:@"postBody"];
}

- (void)createRichTextView {
    self.richTextView = [self createAndAddSubView:UIView.class];
    self.postTextView.scrollEnabled = NO;
    self.postTextView.editable = NO;
}

- (void)createShowMoreButton{
    self.showMoreButton = [[UIButton alloc] init];
    [self addSubview:self.showMoreButton];
}

- (void)createDeadlineView {
    self.deadlineView = [self createAndAddSubView:UIView.class];
    self.deadlineView.backgroundColor = [TeamieUIGlobalColors secondaryYellowColor];
    [self.deadlineView addBorderToTop:YES bottom:NO left:NO right:NO];
    
    self.deadlineLabel = [self.deadlineView createAndAddSubView:UILabel.class];
    self.deadlineLabel.font = [TeamieGlobals appFontFor:@"deadlineText"];
}

- (void)createPollAttachment {
    self.pollAttachmentView = [self createAndAddSubView:UIView.class];
    [self.pollAttachmentView addBorderToTop:YES bottom:NO left:NO right:NO];
}

- (void)createAttachmentsView {
    self.attachmentsView = [self createAndAddSubView:UIView.class];
    self.attachmentsView.translatesAutoresizingMaskIntoConstraints = NO;
}
- (void)createImageAttachmentsView {
    self.imageAttachmentsView = [self createAndAddSubView:UIView.class];
    self.imageAttachmentsView.translatesAutoresizingMaskIntoConstraints = NO;
}

- (void)createActionBarView {
    self.actionBarView = [self createAndAddSubView:UIView.class];
    [self.actionBarView addBorderToTop:YES bottom:NO left:NO right:NO];
    
    self.likeButton = [self.actionBarView createAndAddSubView:UIButton.class];
    [self.likeButton addBorderToTop:YES bottom:NO left:NO right:YES];
    self.likeButton.titleLabel.font = [TeamieGlobals appFontFor:@"actionText"];
    [self.likeButton setTitleColor:[TeamieUIGlobalColors grayColor] forState:UIControlStateNormal];
    [self.likeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    self.likeButton.contentEdgeInsets = UIEdgeInsetsMake(kDefaultPadding, kDefaultPadding, kDefaultPadding, kDefaultPadding);
    self.likeButton.titleEdgeInsets = UIEdgeInsetsMake(0, -3, 0, kDefaultSize);
    self.likeButton.imageEdgeInsets = UIEdgeInsetsMake(0, kDefaultSize, 0, 0);
    [self.likeButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]
                               forState:UIControlStateNormal];
    [self.likeButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"like"
                                                           withSize:11.5
                                                           andColor:[TeamieUIGlobalColors grayColor]]
                     forState:UIControlStateNormal];
    [self.likeButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors primaryActionColor]]
                               forState:UIControlStateSelected];
    [self.likeButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors disabledColor]]
                               forState:UIControlStateDisabled];
    [self.likeButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"like"
                                                           withSize:11.5
                                                           andColor:[UIColor whiteColor]]
                     forState:UIControlStateSelected];
    [self.likeButton addTarget:self action:@selector(toggleLikePost) forControlEvents:UIControlEventTouchUpInside];
    self.sameButton = [self.actionBarView createAndAddSubView:UIButton.class];
    [self.sameButton addBorderToTop:NO bottom:NO left:NO right:YES];
    self.sameButton.titleLabel.font = [TeamieGlobals appFontFor:@"actionText"];
    [self.sameButton setTitleColor:[TeamieUIGlobalColors grayColor] forState:UIControlStateNormal];
    [self.sameButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    self.sameButton.contentEdgeInsets = UIEdgeInsetsMake(kDefaultPadding, kDefaultPadding, kDefaultPadding, kDefaultPadding);
    self.sameButton.titleEdgeInsets = UIEdgeInsetsMake(0, -3, 0, kDefaultSize);
    self.sameButton.imageEdgeInsets = UIEdgeInsetsMake(0, kDefaultSize, 0, 0);
    [self.sameButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]
                               forState:UIControlStateNormal];
    [self.sameButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"question"
                                                           withSize:11.5
                                                           andColor:[TeamieUIGlobalColors grayColor]]
                     forState:UIControlStateNormal];
    [self.sameButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors primaryActionColor]]
                               forState:UIControlStateSelected];
    [self.sameButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors disabledColor]]
                               forState:UIControlStateDisabled];
    [self.sameButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"question"
                                                           withSize:11.5
                                                           andColor:[UIColor whiteColor]]
                     forState:UIControlStateSelected];
    [self.sameButton addTarget:self action:@selector(toggleSamePost) forControlEvents:UIControlEventTouchUpInside];
    
    self.commentsCountButton = [self.actionBarView createAndAddSubView:UIButton.class];
    [self.commentsCountButton addBorderToTop:NO bottom:NO left:NO right:YES];
    self.commentsCountButton.titleLabel.font = [TeamieGlobals appFontFor:@"actionText"];
    [self.commentsCountButton setTitleColor:[TeamieUIGlobalColors grayColor] forState:UIControlStateNormal];
    [self.commentsCountButton setTitle:[self.post.commentsCount stringValue]
                              forState:UIControlStateNormal];
    [self.commentsCountButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"comment"
                                                                    withSize:11.5
                                                                    andColor:[TeamieUIGlobalColors grayColor]]
                              forState:UIControlStateNormal];
    self.commentsCountButton.contentEdgeInsets = UIEdgeInsetsMake(kDefaultPadding, kDefaultPadding , kDefaultPadding, kDefaultPadding);
    self.commentsCountButton.titleEdgeInsets = UIEdgeInsetsMake(0, -4, 0, kDefaultSize);
    self.commentsCountButton.imageEdgeInsets = UIEdgeInsetsMake(-2, kDefaultSize, 0, -1);
    [self.commentsCountButton addTarget:self
                                 action:@selector(commentCountsButtonClicked)
                       forControlEvents:UIControlEventTouchUpInside];

    self.commentButton = [self.actionBarView createAndAddSubView:UIButton.class];
    self.commentButton.titleLabel.font = [TeamieGlobals appFontFor:@"actionText"];
    [self.commentButton setTitleColor:[TeamieUIGlobalColors grayColor] forState:UIControlStateNormal];
    [self.commentButton setTitle:@"Comment" forState:UIControlStateNormal];
    [self.commentButton addTarget:self
                           action:@selector(commentButtonClicked)
                 forControlEvents:UIControlEventTouchUpInside];
    
    self.moreButton = [self.actionBarView createAndAddSubView:UIButton.class];
    [self.moreButton addBorderToTop:NO bottom:NO left:YES right:NO];
    [self.moreButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"ellipsis"
                                                           withSize:15
                                                           andColor:[TeamieUIGlobalColors grayColor]]
                     forState:UIControlStateNormal];
    [self.moreButton addTarget:self action:@selector(showActions) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Load Post

- (void)loadPost:(TMEPost *)post {
    self.post = post;
    self.showDeadline = post.type == TMEPostHomework;
    self.showPollAttachment = (post.poll)? YES: NO;
    self.delete = self.post.actions[@"delete-thought"];
    self.report = self.post.actions[@"report"];
    
    [self loadHeaderView];
    [self loadDeadline];
    [self loadPostText];
    [self loadRichText];
    [self loadAttachments];
    [self loadImageAttachments];
    [self loadPollAttachment];
    [self loadActionBar];
    
    [self updateConstraints];
}

- (void)loadHeaderView {
    self.headerView.backgroundColor = self.post.isAnnouncement?
    [TeamieUIGlobalColors secondaryYellowColor] :
    [TeamieUIGlobalColors headerViewBackgroundColor];
    
    static UIImage *defaultPicture;
    defaultPicture = [TeamieUIGlobals defaultPicForPurpose:@"profile"];
    [self.authorButton sd_setBackgroundImageWithURL:self.post.author.user_profile_image.path
                                           forState:UIControlStateNormal
                                   placeholderImage:defaultPicture];
    self.authorLabel.text = self.post.author.realName;
    NSString *posTimeLabel = [TeamieGlobals timeIntervalWithStartDate:self.post.created withEndDate:nil brevity:0];
    
    self.postTimeLabel.text = posTimeLabel;
    
    NSMutableAttributedString *label = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@" in %@",self.post.classroom.name]];
    [label addAttribute:NSFontAttributeName value:[TeamieGlobals appFontFor:@"postSubtitle"]
                  range:NSMakeRange(0, 4)];
    [label addAttribute:NSFontAttributeName value:[TeamieGlobals appFontFor:@"postSubtitleClassroom"]
                  range:NSMakeRange(4, self.post.classroom.name.length)];
    self.classroomLabel.attributedText = label;

    //Not show this when in the classroom newsfeed
    self.classroomLabel.hidden = self.postViewMode == TMEPostViewModeClassroom;
    
    self.postTypeImageView.image = [TeamieUIGlobals defaultPicForPurpose:[[TMEPost typeJSONTransformer]
                                                                          reverseTransformedValue:@(self.post.type)]
                                                                withSize:15
                                                                andColor:[TeamieUIGlobalColors grayColor]];
    
    if (self.post.isNew) {
        self.seenPostLabel.layer.masksToBounds = YES;
        self.seenPostLabel.layer.cornerRadius = 3.0f;
        self.seenPostLabel.backgroundColor = [UIColor colorWithHex:@"#5F6F8C" alpha:1.0f];
        self.seenPostLabel.font = [TeamieGlobals appFontFor:@"newPostLabel"];
        self.seenPostLabel.textColor = [UIColor whiteColor];
        self.seenPostLabel.textAlignment = NSTextAlignmentCenter;
        self.seenPostLabel.text = @"New";
    }
    else{
        [self.postTypeImageView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(self.headerView);
        }];
    }
    
    if (self.post.status == TMEPostStatusPinned) {
        self.postStatusLabel.layer.masksToBounds = YES;
        self.postStatusLabel.layer.cornerRadius = 2.0f;
        self.postStatusLabel.backgroundColor = [UIColor colorWithHex:@"#999999" alpha:1.0f];
        self.postStatusLabel.font = [UIFont fontWithName:@"OpenSans-Bold" size:15.0];
        self.postStatusLabel.textColor = [UIColor whiteColor];
        self.postStatusLabel.text = @"Pinned";
    }
}

- (void)loadPostText {

    CGFloat textWidth = CGRectGetWidth([UIScreen mainScreen].bounds) - (2 * 23);
    CGFloat textHeight =  CGRectGetHeight(CGRectIntegral([self.post.mainText boundingRectWithSize:CGSizeMake(textWidth, CGFLOAT_MAX)
                                                             options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading
                                                             context:nil]));
    if (textHeight > 81 && (self.postViewMode != TMEPostViewModeFull)) {
        //
        self.showMoreState = 1;
        int value = [self.showMoreOnPostID[self.post.tid] integerValue];
        
        if (value == 1) {
            self.showMore = 1;
        }
        else{
            self.showMore = 0;
        }
        if(self.showMore){
            [self.showMoreButton setTitle:@"Show More" forState:UIControlStateNormal];
            self.showMoreButton.titleLabel.font = [TeamieGlobals appFontFor:@"postBody"];
            [self.showMoreButton setTitleColor:UIColor.blueColor forState:UIControlStateNormal];
            self.postTextView.textContainer.maximumNumberOfLines = 5;
            self.postTextView.textContainer.lineBreakMode = NSLineBreakByTruncatingTail;
            NSString *testString2;
            testString2 = (NSString *)[self.post.mainText attributedSubstringFromRange:(NSMakeRange(0, self.post.mainText.length))];
            NSMutableAttributedString *shortStringWithShowMoreButton = [[NSMutableAttributedString alloc] init];
            shortStringWithShowMoreButton = (NSMutableAttributedString *)[testString2 mutableCopy];
            UIFont *font = [TeamieGlobals appFontFor:@"postBody"];
           [shortStringWithShowMoreButton addAttribute:NSFontAttributeName value:font range:NSMakeRange(0, self.post.mainText.length)];
            [self.showMoreButton addTarget:self action:@selector(showButtonPressed) forControlEvents:UIControlEventTouchUpInside];
            self.postTextView.attributedText = shortStringWithShowMoreButton;
            self.postTextView.font = font;
            self.postTextView.delegate = self;
        }
        else{
            self.postTextView.attributedText = self.post.mainText;
            [self.showMoreButton setTitle:@"Show Less" forState:UIControlStateNormal];
            self.showMoreButton.titleLabel.font = [TeamieGlobals appFontFor:@"postBody"];
            [self.showMoreButton setTitleColor:UIColor.blueColor forState:UIControlStateNormal];
            [self.showMoreButton addTarget:self action:@selector(showButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    else{
        self.showMoreState = 0;
        self.postTextView.attributedText = self.post.mainText;
    }
}
-(void)loadRichText{

    if (self.post.richText) {
        NSString *postRichText = (NSString *)self.post.richText;
        int webViewWidth = (int)([[UIScreen mainScreen] bounds].size.width - 46);

        NSString *cssString = [NSString stringWithFormat:@"<style type='text/css'>img {width: %dpx; height: auto;}p{word-wrap:break-word;}</style>",webViewWidth];
        NSString *htmlString = [NSString stringWithFormat:@"%@%@",cssString,postRichText];
        
        //setting the frame of the webview temporarily to which will updated once it finishes loading
        UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, webViewWidth, 5)];
        [webView loadHTMLString:htmlString
                        baseURL:[NSURL URLWithString:[[NSUserDefaults standardUserDefaults] stringForKey:kBaseURL]]];
        webView.scrollView.scrollEnabled = NO;
        webView.scrollView.bounces = NO;
        [self.richTextView addSubview:webView];
        [webView.scrollView setShowsVerticalScrollIndicator:NO];
        webView.scrollView.delegate = (id)self;
        webView.delegate = self;
    }
    else{
        [self.richTextView setHidden:YES];
    }
}
- (void)loadDeadline {
    if (self.showDeadline) {
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"MMM dd"];
        NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
        [timeFormat setDateFormat:@"hh:mm a"];
        NSString *theDate = [dateFormat stringFromDate:((TMEHomework *)self.post).deadline];
        NSString *theTime = [timeFormat stringFromDate:((TMEHomework *)self.post).deadline];
        NSString *combinedString = [NSString stringWithFormat:@"Due on: %@ at %@",theDate, theTime];
        NSMutableAttributedString *combinedDateAndTime = [[NSMutableAttributedString alloc]initWithString:combinedString];
        UIFont *boldFont = [TeamieGlobals appFontFor:@"tableviewSectionHeader"];
        [combinedDateAndTime addAttribute:NSFontAttributeName value:boldFont range:NSMakeRange(8, combinedDateAndTime.length-8)];
        self.deadlineLabel.attributedText = (NSAttributedString *)combinedDateAndTime;
    }
    else {
        self.deadlineView.hidden = YES;
    }
}

- (void)loadPollAttachment {
    if (self.showPollAttachment){
        TMEPostPollAttachmentViewController *pollAttachmentController = [[TMEPostPollAttachmentViewController alloc] initWithPoll:self.post.poll postID:self.post.tid];
        pollAttachmentController.delegate = self;
        [self.pollAttachmentView addSubview:pollAttachmentController.tableView];

        [self.delegate addChildViewController:pollAttachmentController];
        [pollAttachmentController didMoveToParentViewController:self.delegate];

        pollAttachmentController.tableView.scrollsToTop = NO;
        pollAttachmentController.tableView.scrollEnabled = NO;
        [pollAttachmentController.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.pollAttachmentView);
        }];
    }
    else {
        self.pollAttachmentView.hidden = YES;
    }
}

- (void)loadAttachments {
    __block TMEPostAttachmentView *prevAttachment;
    [self.attachmentsView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    @weakify(self)
    [self.post.attachments enumerateObjectsUsingBlock:^(TMEPostAttachment *attachment, NSUInteger idx, BOOL *stop) {
        @strongify(self)
        TMEPostAttachmentView *attachmentView = [[TMEPostAttachmentView alloc] initWithDelegate:self attachment:attachment];
        MASAttachKeys(attachmentView);
        [attachmentView addBorderToTop:YES bottom:NO left:NO right:NO];
        attachmentView.previewImageView.image = attachment.previewImage;
        attachmentView.mainTextLabel.text = attachment.title;
        attachmentView.href = attachment.href;
        attachmentView.subTextLabel.text = attachment.displaySubTitle;
        [self.attachmentsView addSubview:attachmentView];
        
        [attachmentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.headerView);
            make.top.equalTo((prevAttachment)? prevAttachment.mas_bottom : self.attachmentsView.mas_top).key(@"attachmentTop");
            if (idx == self.post.attachments.count - 1) {
                make.bottom.equalTo(self.attachmentsView).key(@"attachmentBottom");
            }
        }];
        prevAttachment = attachmentView;
    }];
}

- (void)loadImageAttachments{
    TMEPostImageAttachmentView* imageAttachmentView;
    
    if (self.postViewMode != TMEPostViewModeFull) {
    imageAttachmentView = [[TMEPostImageAttachmentView alloc] initWithImageAttachments:self.post.images delegate:self width:[[UIScreen mainScreen] bounds].size.width - (2 * kDefaultCardPadding)];
    }
    else{
    imageAttachmentView = [[TMEPostImageAttachmentView alloc] initWithImageAttachments:self.post.images delegate:self width:[[UIScreen mainScreen] bounds].size.width];
    }
    imageAttachmentView.delegate = (id)self.delegate;
    imageAttachmentView.viewController = self.delegate;
    [self.imageAttachmentsView addSubview:imageAttachmentView];
    [imageAttachmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.imageAttachmentsView);
    }];
    if (self.post.mainText.length <1) {
        [imageAttachmentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.pollAttachmentView.mas_bottom);
        }];
        [self.pollAttachmentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.headerView.mas_bottom);
        }];
    }
}
- (void)loadActionBar {
    TMEPostAction *same = self.post.actions[@"same"];
    TMEPostAction *like = self.post.actions[@"like"];
    if (like) {
        self.likeButton.selected = like.status;
        self.likeButton.enabled = like.access;
    }
    if (same) {
        self.sameButton.selected = same.status;
        self.sameButton.enabled = same.access;
    }
    
    self.likeButton.hidden = self.post.type != TMEPostThought;
    self.sameButton.hidden = self.post.type != TMEPostQuestion;
    
    if (self.post.type == TMEPostQuestion) {
        [self.sameButton setTitle:[((TMEQuestion *)self.post).echosCount stringValue]
                         forState:UIControlStateNormal];
    }
    else if (self.post.type == TMEPostThought) {
        [self.likeButton setTitle:[((TMEThought *)self.post).likesCount stringValue]
                         forState:UIControlStateNormal];
    }
    [self.commentsCountButton setTitle:[self.post.commentsCount stringValue]
                              forState:UIControlStateNormal];
    self.moreButton.hidden = !(self.postViewMode != TMEPostViewModeFull && [self hasMoreActions]);
}

#pragma mark - Actions

- (void)showActions {
    [self.delegate showPostMoreActionsSheet:self];
}

- (void)commentCountsButtonClicked {
    TMETrackEvent(kTrackingCategory, @"tap_comment_count", nil);
    if (self.postViewMode != TMEPostViewModeFull) {
        [self.delegate.navigationController pushViewController:[TMEPostViewController postWithId:self.post.tid openComment:NO]
                                                      animated:YES];
    }
}

- (void)toggleLikePost {
    TMEPostAction *like = self.post.actions[@"like"];
    TMETrackEvent(kTrackingCategory, @"flag_post", like.status? @"unlike" : @"like");
    if (like.access) {
        NSString *messageKey = [NSString stringWithFormat:@"message.%@-%@", like.status? @"unliking" : @"liking", self.post.typeString];
        [[TMEClient sharedClient]
         request:TeamieUserLikeThoughtRequest
         parameters:@{@"tid": self.post.tid}
         loadingMessage:TMELocalize(messageKey)
         success:^(NSDictionary *response) {
             self.likeButton.selected ^= YES;
             like.status = self.likeButton.selected;
             [self.likeButton setTitle:[(NSNumber *)response[@"count"] stringValue] forState:UIControlStateNormal];
         } failure:nil];
    }
}
- (void)toggleSamePost {
    TMEPostAction *same = self.post.actions[@"same"];
    TMETrackEvent(kTrackingCategory, @"flag_post", same.status? @"unsame" : @"same");
    if (same.access) {
        [[TMEClient sharedClient]
         request:TeamieUserSameThoughtRequest
         parameters:@{@"tid": self.post.tid}
         loadingMessage:TMELocalize(same.status? @"message.unecho": @"message.echo")
         success:^(NSDictionary *response) {
             self.sameButton.selected ^= YES;
             same.status = self.sameButton.selected;
             [self.sameButton setTitle:[(NSNumber *)response[@"count"] stringValue] forState:UIControlStateNormal];
         } failure:nil];
    }
}

- (UIActionSheet *)moreActionsSheet {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:TMELocalize(@"message.more-actions")
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    actionSheet.destructiveButtonIndex = self.delete.access ? [actionSheet addButtonWithTitle:self.delete.title] : -1;
    kReportPostAction = self.report.access ? [actionSheet addButtonWithTitle:self.report.title] : -1;
    if (self.postViewMode == TMEPostViewModeFull && [self.post.commentsCount unsignedIntegerValue] > self.post.comments.count) {
        kMoreCommentsAction = [actionSheet addButtonWithTitle:TMELocalize(@"button.all-comments")];
    }
    else {
        kMoreCommentsAction = -1;
    }
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:TMELocalize(@"button.cancel")];
    
    actionSheet.tag = kActionSheetTag;
    return actionSheet;
}

- (BOOL)hasMoreActions {
    return self.delete.access || self.report.access;
}

- (void)openAuthorProfile {
    TMETrackEvent(kTrackingCategory, @"tap_user_picture", @"post");
    [self.delegate.navigationController pushViewController:[[UserProfileViewController alloc] initWithUserID:[self.post.author.uid integerValue]] animated:YES];
}

- (void)openClassroom {
    TMETrackEvent(kTrackingCategory, @"tap_classroom_name", nil);
    if (self.post.classroom.nid) {
        [self.delegate.navigationController pushViewController:[TMEClassroomNewsfeedViewController newsfeedForClassroom:self.post.classroom]
                                                      animated:YES];
    }
}

- (void)commentButtonClicked {
    TMETrackEvent(kTrackingCategory, @"tap_comment", nil);
    [self.delegate commentButtonClicked:self.post];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (actionSheet.tag) {
        case kActionSheetTag:
            if (buttonIndex == actionSheet.cancelButtonIndex) {
                //Do nothing
            }
            else if (buttonIndex == actionSheet.destructiveButtonIndex) {
                TMETrackEvent(kTrackingCategory, @"flag_more_post", @"delete");
                NSString *messageKey = [NSString stringWithFormat:@"message.deleting-%@", self.post.typeString];
                [[TMEClient sharedClient]
                 request:TeamieUserDeleteThoughtRequest
                 parameters:nil
                 makeURL:^NSString *(NSString *URL) {
                     return [NSString stringWithFormat:URL, self.post.tid];
                 }
                 loadingMessage:TMELocalize(messageKey)
                 success:^(id response) {
                     [self.delegate removePost:self.post];
                 } failure:nil];
            }
            else if (buttonIndex == kReportPostAction) {
                TMETrackEvent(kTrackingCategory, @"flag_more_post", @"report");
                [[TMEClient sharedClient]
                 request:TeamieUserReportPost
                 parameters:@{@"tid":self.post.tid}
                 loadingMessage:TMELocalize(@"message.report-post")
                 success:^(id response) {
                     [TeamieGlobals addHUDTextOnlyLabel:@"Reported the post!" afterDelay:1];
                 } failure:nil];
            }
            else if (buttonIndex == kMoreCommentsAction ) {
                TMETrackEvent(kTrackingCategory, @"load_old", @"comment");
                [[TMEClient sharedClient]
                 request:TeamieUserGetAllThoughtCommentsRequest
                 parameters:@{@"items_per_page":@(200)}
                 makeURL:^NSString *(NSString *URL) {
                     return [NSString stringWithFormat:URL, self.post.tid];
                 }
                 loadingMessage:TMELocalize(@"message.loading")
                 success:^(NSDictionary *response) {
                     self.post.comments = [TMEPostComment parseList:response];
                     if ([self.delegate conformsToProtocol:@protocol(TMEPostCommentViewDelegate)]) {
                         [self.delegate performSelector:@selector(loadComments)];
                     }
                 } failure:nil];
            }
    }
}


- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange {
    if ([[URL scheme] isEqualToString:@"Show More"]) {
        
        // do something with this username
        // ...
        NSLog(@"pressed the link");
        return NO;
    }
    if(self.showMore){
        NSLog(@"calling it fron outside class");
        self.showMore = 1;
        [self.delegate updateTheRowForIndex:self.post withOption:0];
        return nil; // let the system open this URL
    }
    else{
         NSLog(@"calling it fron outside class");
        self.showMore = 0;
        [self.delegate updateTheRowForIndex:self.post withOption:1];
        return nil; // let the system open this URL
    }
}

- (void) showButtonPressed{
    if(self.showMore){
        self.showMore = 1;
        [self.delegate updateTheRowForIndex:self.post withOption:0];
        [self.showMoreButton setTitle:@"Show Less" forState:UIControlStateNormal];
    }
    else{
        self.showMore = 0;
        [self.delegate updateTheRowForIndex:self.post withOption:1];
        [self.showMoreButton setTitle:@"Show More" forState:UIControlStateNormal];
    }
}

#pragma mark - TMEPollAttachmentDelegate

- (void)updatePollAttachmentViewHeightWithPoll:(TMEPollAttachment *)poll {
    CGFloat pollHeight;
    self.post.poll = poll;
    if (self.post.poll.isFooterVisible) {
        pollHeight = (self.post.poll? (((self.post.poll.choiceCount.intValue + 1) * 50) + 23): 0);
    }
    else {
        pollHeight = (self.post.poll? ((self.post.poll.choiceCount.intValue * 50) + 23): 0);
    }

    [self.pollAttachmentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(self.showPollAttachment? pollHeight:0));
    }];
    
    if([self.delegate conformsToProtocol:@protocol(TMEPostViewDelegate)]) {
        if ([self.delegate isKindOfClass:TMENewsfeedViewController.class]) {
            [self.delegate performSelector:@selector(updatePostViewHeightWithUpdatedPost:) withObject:self.post];
        }
    }
}

#pragma - TMEPostAttachmentViewDelegate

- (void)trackAttachmentClick:(NSString *)attachmentType {
    TMETrackEvent(kTrackingCategory, @"tap_post_attachment", attachmentType);
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    NSString *result = [aWebView stringByEvaluatingJavaScriptFromString:@"Math.max( document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight );"];
    int heightForWebView = [result integerValue];
    aWebView.frame = CGRectMake(0, 0, aWebView.frame.size.width, heightForWebView);
    if(self.postViewMode != TMEPostViewModeNewsfeed) {
        [self.richTextView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@(heightForWebView));
        }];
    }
        [self.delegate updateTheRowForIndex:self.post withHeight:heightForWebView];
        NSLog(@"size: %f, %f", aWebView.frame.size.width, heightForWebView);
}

-(BOOL) webView:(UIWebView *)inWeb shouldStartLoadWithRequest:(NSURLRequest *)inRequest navigationType:(UIWebViewNavigationType)inType {
    if ( inType == UIWebViewNavigationTypeLinkClicked ) {
        [TeamieGlobals openInappBrowserWithURL:inRequest.URL fromResponder:self];
        return NO;
    }
    return YES;
}


@end
