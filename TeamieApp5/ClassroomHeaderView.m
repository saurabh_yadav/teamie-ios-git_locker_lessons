//
//  ClassroomHeaderView.m
//  TeamieApp5
//
//  Created by Anuj Rajput on 16/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "ClassroomHeaderView.h"
#import "UIView+TMEEssentials.h"
#import "Masonry.h"

/**
 *  Header Corner Radius
 */
CGFloat kHeaderCornerRadius = 5.0;

@implementation ClassroomHeaderView

- (void)styleButtons {
    [self.lessonButton setTitle:TMELocalize(@"menu.lessons") forState:UIControlStateNormal];
    self.lessonButton.backgroundColor            = [UIColor whiteColor];
    self.lessonButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.lessonButton.contentEdgeInsets          = UIEdgeInsetsMake(0, 20, 0, 0);
    self.lessonButton.imageEdgeInsets            = UIEdgeInsetsMake(0, -5, 0, 0);
    self.lessonButton.tag                        = 0;
    [self.lessonButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"lesson" withSize:14.0f andColor:TeamieUIGlobalColors.supplementaryViewElementTextColor] forState:UIControlStateNormal];
    self.lessonButton.titleLabel.font            = [TeamieGlobals appFontFor:@"AppFontForPurposeSupplementaryViewText"];
    [self.lessonButton setTitleColor:TeamieUIGlobalColors.supplementaryViewElementTextColor forState:UIControlStateNormal];
    [self.lessonButton addTarget:self.protocol action:@selector(didTapOnClassroomSupplementaryViewButton:) forControlEvents:UIControlEventTouchUpInside];

    
    [self.assessmentButton setTitle:TMELocalize(@"menu.assessments") forState:UIControlStateNormal];
    self.assessmentButton.titleLabel.font = [TeamieGlobals appFontFor:@"AppFontForPurposeSupplementaryViewText"];
    self.assessmentButton.backgroundColor = [UIColor whiteColor];
    self.assessmentButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    self.assessmentButton.contentEdgeInsets = UIEdgeInsetsMake(0, 20, 0, 0);
    self.assessmentButton.imageEdgeInsets = UIEdgeInsetsMake(0, -5, 0, 0);
    [self.assessmentButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"quiz" withSize:14.0f andColor:TeamieUIGlobalColors.supplementaryViewElementTextColor] forState:UIControlStateNormal];
    [self.assessmentButton setTitleColor:TeamieUIGlobalColors.supplementaryViewElementTextColor forState:UIControlStateNormal];
    [self.assessmentButton addTarget:self.protocol action:@selector(didTapOnClassroomSupplementaryViewButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.assessmentButton addBorderToTop:NO bottom:NO left:YES right:NO];
    self.assessmentButton.tag = 1;
}

- (void)styleView {
    self.layer.cornerRadius = kHeaderCornerRadius;
    self.layer.masksToBounds = YES;
    [self styleButtons];
}

@end
