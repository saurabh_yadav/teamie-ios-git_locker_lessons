//
//  LessonTableViewController.m
//  TeamieApp5
//
//  Created by Raunak on 10/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "LessonTableViewController.h"
#import "LessonPageViewController.h"
#import <TapkuLibrary/TKEmptyView.h>
#import "TeamieWebViewController.h"

#import "LessonObject.h"

@interface LessonTableViewController () {
    BOOL showLoading;
}

@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) NSArray* lessonsList;
@property (nonatomic, strong) LessonPageViewController* lessonPageViewController;

- (void)lessonPageSelected:(id)sender;

@end

@implementation LessonTableViewController

@synthesize lessonPageViewController;

- (id)initWithLessonDetails:(LessonObject*)lesson {
    self = [super init];
    if (self) {
        _lesson = lesson;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.title = self.lesson.title;
    self.view.backgroundColor = [UIColor whiteColor];
    
    NSDictionary* dict = [NSDictionary dictionaryWithObject:self.lesson.nid forKey:@"lid"];
    [self.apiCaller performRestRequest:TeamieUserLessonPagesListRequest withParams:dict];
    showLoading = YES;
	// Do any additional setup after loading the view.
    
    self.tableView.bounces = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.lessonsList == nil && showLoading) {
        [TeamieGlobals addHUDTextOnlyLabel:TeamieLocalizedString(@"MSG_LOADING", nil) margin:10.0 yOffset:10.0];
    }
}

- (void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    [TeamieGlobals dismissActivityLabels];
    showLoading = NO;
    if (![response isKindOfClass:[NSArray class]]) {
        return;
    }
    if ([((LessonPagesList*)[response objectAtIndex:0]).lessonPages count] == 0) {
        NSString* entityUrl = nil;
        NSString* baseUrl = [[NSUserDefaults standardUserDefaults] stringForKey:@"baseURL"];
        if (OAUTH_LOGIN_ENABLED) {
            entityUrl = [NSString stringWithFormat:@"%@/merge-session/?access_token=%@&redirect_url=%@", TEAMIE_DEFAULT_OAUTH_REST_URL, [[NSUserDefaults standardUserDefaults] stringForKey:@"access_token"], [NSString stringWithFormat:@"node/%qi", [self.lesson.nid longLongValue]]];
        }
        else {
            entityUrl = [NSString stringWithFormat:@"%@/node/%qi", baseUrl, [self.lesson.nid longLongValue]];
        }
        /*TeamieWebViewController * webController = [[TeamieWebViewController alloc] init];
        [webController openURL:[NSURL URLWithString:entityUrl]];
        webController.modalPresentationStyle = UIModalPresentationFullScreen;
        webController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        UINavigationController * navigationController = [[UINavigationController alloc] initWithRootViewController:webController];
        [self presentViewController:navigationController animated:YES completion:nil];
        [self.navigationController popViewControllerAnimated:NO];*/

    }
    
    else {
        self.lessonsList = ((LessonPagesList*)[response objectAtIndex:0]).lessonPages;
        NSMutableArray* cellItems = [[NSMutableArray alloc] init];
        
        /*for (NSDictionary* item in self.lessonsList) {
            TTTableTextItem* cellItem = [TTTableTextItem itemWithText:[item valueForKey:@"title"] delegate:self selector:@selector(lessonPageSelected:)];
            [cellItems addObject:cellItem];
        }
        TTListDataSource* datasource = [TTListDataSource dataSourceWithItems:cellItems];
        self.dataSource = datasource;*/
    }
}

- (LessonPageViewController*)lessonPageViewController {
    if (!lessonPageViewController) {
        lessonPageViewController = [[LessonPageViewController alloc] init];
        lessonPageViewController.lessonsList = self.lessonsList;
    }
    return lessonPageViewController;
}

#pragma mark - Private Methods

- (void)lessonPageSelected:(id)sender {
    /*if ([sender isKindOfClass:[TTTableTextItem class]]) {   // Find out which row it was and make appropriate request
        NSInteger rowIndex = [((TTListDataSource*)self.dataSource).items indexOfObject:sender];
        // Set the page number of the lesson page view controller based on the row index.
        self.lessonPageViewController.currentPage = rowIndex;
        [self.navigationController pushViewController:self.lessonPageViewController animated:YES];
    }*/
}

@end
