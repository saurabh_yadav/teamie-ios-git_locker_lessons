//
//  UserMenu.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "UserMenu.h"
#import "URLParam.h"
#import "UserLoginData.h"


@implementation UserMenu

@dynamic category;
@dynamic href;
@dynamic method;
@dynamic name;
@dynamic title;
@dynamic attributes;
@dynamic userObject;

@end
