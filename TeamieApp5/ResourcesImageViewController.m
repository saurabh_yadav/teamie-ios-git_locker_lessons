//
//  ResourcesImageViewController.m
//  TeamieApp5
//
//  Created by Raunak on 16/7/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "ResourcesImageViewController.h"

@interface ResourcesImageViewController () {
    BOOL openInOptionsVisible;
}
@property (nonatomic, strong) NSNumber* tid;
@property (nonatomic, strong) UIDocumentInteractionController* openInOptionsController;
@property (nonatomic, strong) WEPopoverController* menuController;
@end

@implementation ResourcesImageViewController

- (void)loadImageWithUrl:(NSString*)url WithCaption:(NSString*)caption WithTitle:(NSString*)title WithThoughtID:(NSNumber*)tid WithFilePath:(NSURL*)path {
    [super loadImageWithUrl:url WithCaption:caption WithTitle:title];
    self.tid = tid;
    self.filePath = path;
    self.openInOptionsController = [UIDocumentInteractionController interactionControllerWithURL:self.filePath];
    self.openInOptionsController.delegate = self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    //self.navigationBarTintColor = [UIColor colorWithHex:@"#BE202E" alpha:1.0];
    openInOptionsVisible = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(menuButtonPressed:)];
}

- (void)menuButtonPressed:(id)sender {
    if (openInOptionsVisible) {
        [self.openInOptionsController dismissMenuAnimated:YES];
    }
    else {
        if(!self.menuController) {
            ResourcesShareMenuViewController* contentViewController = [[ResourcesShareMenuViewController alloc] init];
            contentViewController.delegate = self;
            
            Class popoverClass;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                popoverClass = [UIPopoverController class];
            }
            else {
                popoverClass = [WEPopoverController class];
            }
            
            self.menuController = [[popoverClass alloc] initWithContentViewController:contentViewController];
            self.menuController.delegate = self;
            [self.menuController presentPopoverFromBarButtonItem:(UIBarButtonItem*)sender permittedArrowDirections:(UIPopoverArrowDirectionUp|UIPopoverArrowDirectionDown) animated:YES];
        }
        else {
            [self.menuController dismissPopoverAnimated:YES];
            self.menuController= nil;
        }
    }
}

- (void) shareMenuDidSelectDeleteOption:(ShareMenuOptions)option {
    
    [self.menuController dismissPopoverAnimated:YES];
    self.menuController= nil;
    
    switch (option) {
        case kShare:
            [self.delegate resourcesImageControllerDidSelectShareOption:self];
            break;
        case kOpenIn:
            if (!openInOptionsVisible) {
                [self.openInOptionsController presentOptionsMenuFromBarButtonItem:self.navigationItem.rightBarButtonItem animated:YES];
            }
            break;
        default:
            break;
    }
}

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    self.menuController = nil;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

- (void)documentInteractionControllerWillPresentOptionsMenu:(UIDocumentInteractionController *)controller {
    openInOptionsVisible = YES;
}

- (void)documentInteractionControllerDidDismissOptionsMenu:(UIDocumentInteractionController *)controller {
    openInOptionsVisible = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
