//
//  TMEMessagesViewController.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 29/6/15.
//  Copyright (c) 2015 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVWebViewController/SVWebViewController.h>

@interface TMEMessagesViewController : UIViewController

@property (nonatomic, weak) UIViewController *parentRevealController;

@end
