//
//  UserProfileStatsTableViewCell.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 8/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UserProfileStatsTableViewCell.h"
#import <Masonry/Masonry.h>

@implementation UserProfileStatsTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    self.statsValue.font = [TeamieGlobals appFontFor:@"userProfileSectionContentText"];
    self.statsTitle.font = [TeamieGlobals appFontFor:@"userProfileSectionContentText"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(0,0,32,32);
}

- (void)updateConstraints {
//    
//    [self.iconImage mas_makeConstraints:^(MASConstraintMaker *make){
//        make.left.equalTo(self).with.offset(20.0);
//        make.top.equalTo(self).with.offset(10.0);
//        make.width.height.equalTo(@22);
//    }];
//
//    [self.statsTitle mas_makeConstraints:^(MASConstraintMaker *make){
//        make.leading.equalTo(self.iconImage.mas_right).with.offset(8.0);
//        make.trailing.lessThanOrEqualTo(self.statsValue.mas_left).with.offset(10.0);
//        make.width.lessThanOrEqualTo(@200);
//        make.top.equalTo(self).with.offset(10.0);
//    }];
//
//    [self.statsValue mas_makeConstraints:^(MASConstraintMaker *make){
//        make.right.equalTo(self).with.offset(20.0);
//        make.top.equalTo(self).with.offset(10.0);
//        make.width.equalTo(@15);
//        make.baseline.equalTo(self.statsTitle);
//    }];
//    
    [super updateConstraints];
}

@end
