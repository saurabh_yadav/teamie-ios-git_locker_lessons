//
//  TeamieAppDelegate.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/16/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TeamieAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (NSURL *)applicationDocumentsDirectory;

- (void)transitionToHomeScreen;
- (void)transitionToLoginScreen;

@end
