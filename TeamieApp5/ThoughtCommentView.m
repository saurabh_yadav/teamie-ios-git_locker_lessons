//
//  ThoughtCommentView.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 14/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "ThoughtCommentView.h"
#import "TeamieUIGlobalColors.h"
#import "UIView+Border.h"
#import "UIView+AutoLayout.h"
#import "ThoughtImageAttachmentView.h"
#import "ThoughtDefaultAttachmentView.h"
#import "ThoughtCommentReplyView.h"
#import "TMEPostAction.h"

#define kVoteButtonDefaultColor [UIColor colorWithHex:@"#FFFFFF" alpha:1.0]
#define kVoteButtonDefaultBorderColor [UIColor colorWithHex:@"#DDDDDD" alpha:1.0]
#define kVoteUpDownButtonActiveColor [UIColor colorWithHex:@"#5F6F8C" alpha:1.0]
#define kMarkRightButtonActiveColor [UIColor colorWithHex:@"#41A389" alpha:1.0]
#define kVoteButtonActiveTitleColor [UIColor colorWithHex:@"#FFFFFF" alpha:1.0]

#define kButtonBorderColor [UIColor colorWithHex:@"#CCCCCC" alpha:1.0]

static const NSString* kNibName = @"ThoughtCommentView";

static const CGFloat kVoteBarTopMargin = 10.0;
static const CGFloat kVoteBarRightMargin = 10.0;
static const CGFloat kVoteButtonWidth = 36.0;
static const CGFloat kVoteButtonHeight = 27.0;

static const CGFloat kReplyButtonHeight = 28.0;

static const CGFloat kVerticalDistance = 10.0;
static const CGFloat kLeftMargin = 10.0;

static const UIFont* kCmtFont;
static const UIFont* kCmtFontBold;
static const UIColor* kCmtTextColor;
static const UIColor* kButtonTitleColor;
static const UIFont* kButtonFont;

@interface ThoughtCommentView() <UITextViewDelegate, ThoughtAttachmentViewDelegate, ThoughtCommentReplyViewDelegate>

@property (nonatomic, readwrite) TMEPostComment* comment;
@property (nonatomic, readwrite) int replyPage;

@property (nonatomic, strong) NSArray* imageAttachments;
@property (nonatomic, strong) NSArray* nonImageAttachments;

@property (nonatomic) BOOL hasShownLoadReplyButton;

@property (nonatomic, strong) UIView* voteBar;
@property (nonatomic, strong) ThoughtAttachmentView* bottomAttachmentView;
@property (nonatomic, strong) UIView* actionBar;

@property (nonatomic, strong) UIView* replyStream;
@property (nonatomic, strong) ThoughtCommentReplyView* topReplyView;
@property (nonatomic, strong) ThoughtCommentReplyView* bottomReplyView;
@property (nonatomic, strong) UIButton* loadReplyButton;

@property (nonatomic, strong) NSLayoutConstraint* bottomAttachmentViewBottomConstraint;
@property (nonatomic, strong) NSArray* replyStreamHorConstraints;
@property (nonatomic, strong) NSLayoutConstraint* topReplyViewTopConstraint;
@property (nonatomic, strong) NSLayoutConstraint* bottomReplyViewBottomConstraint;
@property (nonatomic, strong) NSArray* loadReplyButtonHorConstraints;

@property (nonatomic) int prevMessageBodyWidth;

@end

@implementation ThoughtCommentView

+ (void)initialize{
    UIFont* thoughtFont = [TeamieGlobals appFontFor:@"thoughtBody"];
    UIFont* thoughtFontBold = [TeamieGlobals appFontFor:@"thoughtBodyBold"];
    
    kCmtFont = [UIFont fontWithName:thoughtFont.fontName size:thoughtFont.pointSize * 0.9];
    kCmtFontBold = [UIFont fontWithName:thoughtFontBold.fontName size:thoughtFontBold.pointSize * 0.9];
    kCmtTextColor = [TeamieUIGlobalColors thoughtCommentTextColor];
    kButtonTitleColor = [TeamieUIGlobalColors buttonTitleColor];
    kButtonFont = [UIFont fontWithName:kCmtFont.fontName size:kCmtFont.pointSize * 0.9];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (ThoughtCommentView*) viewWithComment:(TMEPostComment *)comment delegate:(id<ThoughtCommentViewDelegate>)delegate
{
    ThoughtCommentView* commentView = [[ThoughtCommentView alloc] initWithComment:comment
                                                                         delegate:delegate];
    return commentView;
}

- (id)initWithComment:(TMEPostComment*)comment delegate:(id<ThoughtCommentViewDelegate>)delegate
{
    self = [[[NSBundle mainBundle] loadNibNamed:[kNibName copy] owner:nil options:nil] lastObject];
    if (self){
        self.delegate = delegate;
        self.replyPage = 1;
        self.hasShownLoadReplyButton = NO;
        
        [self setupLayout];
        [self loadComment:comment];
    }
    
    return self;
}

- (void)setupLayout{
    self.cmtAuthorName.font = [kCmtFontBold copy];
    self.cmtAuthorName.textColor = [kCmtTextColor copy];
    self.cmtTimestamp.font = [kCmtFont copy];
    self.cmtTimestamp.textColor = [kCmtTextColor copy];
    
    self.cmtMessageBody.textColor = [kCmtTextColor copy];
    self.cmtMessageBody.font = [kCmtFont copy];
    self.cmtMessageBody.delegate = self;
    
    self.prevMessageBodyWidth = self.cmtMessageBody.frame.size.width;
    self.translatesAutoresizingMaskIntoConstraints = NO;
}

#pragma mark - Overrided methods
- (void)layoutSubviews{
    if (self.cmtMessageBody.frame.size.width != self.prevMessageBodyWidth){
        self.prevMessageBodyWidth = self.cmtMessageBody.frame.size.width;
    }
    
    [super layoutSubviews];
    
    // Layout vote bar
    CGFloat barX = self.frame.size.width - kVoteBarRightMargin - self.voteBar.frame.size.width;
    CGFloat barY = kVoteBarTopMargin;
    self.voteBar.frame = CGRectMake(barX, barY, CGRectGetWidth(self.voteBar.frame), CGRectGetHeight(self.voteBar.frame));
    
    // Anything with CALayer's anchor point and frame should be done here
    if (_replyStream){
        [_replyStream clearBorderLayers];
        [_replyStream drawInnerShadowWithColor:[[UIColor colorWithHex:@"#CCCCCC" alpha:1.0]CGColor]
                                        radius:4.0];
    }
    
    if ([self.delegate respondsToSelector:@selector(commentViewDidLayoutSubviews:)]){
        [self.delegate performSelector:@selector(commentViewDidLayoutSubviews:) withObject:self];
    }
}

- (void)setBackgroundColor:(UIColor *)backgroundColor{
    [super setBackgroundColor:backgroundColor];
    _cmtMessageBody.backgroundColor = backgroundColor;
}

#pragma mark - Public APIs
- (void)updateVoteStat:(NSNumber*)voteCount markRightStatus:(NSNumber *)isMarkedRight actions:(NSArray *)actions{
    self.comment.voteCount = voteCount;
    self.comment.isMarkedRight = isMarkedRight;
    self.comment.actions = actions;
    
    [self.voteBar removeFromSuperview];
    [self showVoteBarIfNeeded];
}

- (void)loadReplies:(NSArray *)replyArray attachToTop:(BOOL)shouldAttachToTop increaseReplyPage:(BOOL)shouldIncreaseReplyPage updateReplyCount:(BOOL)shouldUpdateReplyCount{
    if ([replyArray count] == 0){
        return;
    }
    
    // Copy newly loaded replies
    if (shouldAttachToTop){
        self.comment.replies = [self copyFromArray:replyArray followByArray:self.comment.replies];
    }
    else{
        self.comment.replies = [self copyFromArray:self.comment.replies followByArray:replyArray];
    }
    
    // Update reply count if needed
    if (shouldUpdateReplyCount){
        self.comment.repliesCount = [NSNumber numberWithInt:[self.comment.repliesCount intValue] + [replyArray count]];
        
        [self.actionBar removeFromSuperview];
        [self showActionBar];
    }
    
    // Show replies
    if (![self.replyStream isDescendantOfView:self]){
        [self showReplyStream];
    }
    [self showReplies:replyArray attachToTop:shouldAttachToTop];
    
    // Increase reply page if needed
    if (shouldIncreaseReplyPage){
        self.replyPage++;
    }
}

#pragma mark - Load comment content
- (void)loadComment:(TMEPostComment*)comment{
    self.comment = comment;
    
    [self loadCommentContent];
    
    [self showVoteBarIfNeeded];
    
    if ([self.comment.attachments count] > 0){
        [self showAttachments];
    }
    
    if ([self.comment actionWithName:@"reply"]){
        [self showActionBar];
    }
}

- (void)loadCommentContent{
    [self createAuthorImgView];
    
    self.cmtAuthorName.text = self.comment.author.realName;
    self.cmtTimestamp.text = [self createTimestampText];
    self.cmtMessageBody.attributedText = [self createMessageBodyText];
    
}

- (void)createAuthorImgView{
    UIImage* img = nil;
    NSURL* imgURL = self.comment.author.user_profile_image.path;
    
    if (imgURL == nil){
        img = [TeamieUIGlobals defaultPicForPurpose:@"profile"
                                           withSize:2 * kCmtFont.pointSize
                                           andColor:[UIColor colorWithHex:@"#6F6F6F" alpha:1.0]];
        [self.cmtAuthorImg setContentMode:UIViewContentModeCenter];
    }
    else{
        img = [UIImage imageWithData:[NSData dataWithContentsOfURL:imgURL]];
        [self.cmtAuthorImg setContentMode:UIViewContentModeScaleAspectFill];
    }
    
    [self.cmtAuthorImg setImage:img];
}

- (NSString*)createTimestampText{
    if (self.comment.timestampText){
        return self.comment.timestampText;
    }
    
    NSDate * timestamp = [NSDate dateWithTimeIntervalSince1970:[self.comment.created doubleValue]];
    NSString * timeOver = [TeamieGlobals timeIntervalWithStartDate:timestamp
                                                       withEndDate:[NSDate date] brevity:0];
    
    return timeOver;
}

- (NSAttributedString*)createMessageBodyText{
    NSData* htmlData = [self.comment.htmlMessage dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary* parseOptions = @{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                   NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)};
    NSDictionary* attributes = @{NSFontAttributeName:self.cmtMessageBody.font,
                                 NSForegroundColorAttributeName:self.cmtMessageBody.textColor,
                                 NSUnderlineStyleAttributeName:@(NSUnderlineStyleNone)};
    
    NSMutableAttributedString* attText = [[NSMutableAttributedString alloc] initWithData:htmlData
                                                                                 options:parseOptions
                                                                      documentAttributes:nil
                                                                                   error:nil];
    [attText addAttributes:attributes range:NSMakeRange(0, [attText length])];
    
    return attText;
}

#pragma mark - Vote statistics and actions
- (void)showVoteBarIfNeeded{
    TMEPostAction* voteupAction = [self.comment actionWithName:@"voteup"];
    TMEPostAction* votedownAction = [self.comment actionWithName:@"votedown"];
    
    UIView* voteBar = [[UIView alloc] init];
    
    CGFloat nowX = 0;
    
    // Style UI
    voteBar.clipsToBounds = YES;
    voteBar.layer.cornerRadius = 4.0;
    
    // Vote count
    if (self.comment.voteCount != nil){
        UIButton* voteCountButton = [self createVoteButtonAtX:nowX];
        [self configureVoteCountButton:voteCountButton];
        
        [voteBar addSubview:voteCountButton];
        nowX += voteCountButton.frame.size.width;
    }
    
    // Voteup button
    if (voteupAction && [voteupAction.access boolValue]){
        UIButton* voteUpButton = [self createVoteButtonAtX:nowX];
        [self configureVoteButton:voteUpButton name:@"voteup"];
        
        [voteBar addSubview:voteUpButton];
        nowX += voteUpButton.frame.size.width;
    }
    
    // Votedown button
    if (votedownAction && [votedownAction.access boolValue]){
        UIButton* voteDownButton = [self createVoteButtonAtX:nowX];
        [self configureVoteButton:voteDownButton name:@"votedown"];
        
        [voteBar addSubview:voteDownButton];
        nowX += voteDownButton.frame.size.width;
    }
    
    // mark-right button
    if (self.comment.isMarkedRight != nil){
        UIButton* markRightButton = [self createVoteButtonAtX:nowX];
        [self configureMarkRightButton:markRightButton];
        
        [voteBar addSubview:markRightButton];
        nowX += markRightButton.frame.size.width;
    }
    
    // Show vote bar if needed
    if (nowX != 0){
        CGFloat barWidth = nowX;
        CGFloat barX = self.frame.size.width - kVoteBarRightMargin - barWidth;
        CGFloat barY = kVoteBarTopMargin;
        CGFloat barHeight = kVoteButtonHeight;

        voteBar.frame = CGRectMake(barX, barY, barWidth, barHeight);
        // Add votebar to self
        [self addSubview:voteBar];
        self.voteBar = voteBar;
        
        // Draw border for subviews
        int count = [voteBar.subviews count];
        for (int i = 0; i < count; i++){
            UIView* view = [voteBar.subviews objectAtIndex:i];
            if (i == 0 && i == count - 1){
                view.layer.borderWidth = 1.0;
                view.layer.cornerRadius = voteBar.layer.cornerRadius;
            }
            else if (i == 0){
                [view drawBorderAtTop:YES right:NO bottom:YES left:YES
                            withColor:view.layer.borderColor
                           borderWith:1.0
                         cornerRadius:voteBar.layer.cornerRadius];
                
                [view drawRightBorderWithColor:view.layer.borderColor borderWidth:1.0];
            }
            else if (i == count - 1){
                [view drawBorderAtTop:YES right:YES bottom:YES left:NO
                            withColor:view.layer.borderColor
                           borderWith:1.0
                         cornerRadius:voteBar.layer.cornerRadius];
            }
            else{
                [view drawBorderAtTop:YES right:YES bottom:YES left:NO
                            withColor:view.layer.borderColor
                           borderWith:1.0
                         cornerRadius:0.0];
            }
        }
    }
}

- (UIButton*)createVoteButtonAtX:(CGFloat)xPosition{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(xPosition, 0, kVoteButtonWidth, kVoteButtonHeight);

    button.backgroundColor = kVoteButtonDefaultColor;
    
    [button setTitleColor:[kButtonTitleColor copy] forState:UIControlStateNormal];
    button.titleLabel.font = [kButtonFont copy];
    
    return button;
}

- (void)configureVoteCountButton:(UIButton*)button{
    NSString* voteCountTitle = [NSString stringWithFormat:@"%+d", [self.comment.voteCount intValue]];
    [button setTitle:voteCountTitle forState:UIControlStateNormal];
    button.layer.borderColor = [kVoteButtonDefaultBorderColor CGColor];
}

- (void)configureVoteButton:(UIButton*)button name:(NSString*)name{
    SEL selector;
    if ([name isEqualToString:@"voteup"]){
        selector = @selector(voteUpButtonDidTapped:);
    }
    else{
        selector = @selector(voteDownButtonDidTapped:);
    }

    TMEPostAction* action = [self.comment actionWithName:name];
    
    UIColor* backgroundColor = ([action.status boolValue]) ? kVoteUpDownButtonActiveColor : kVoteButtonDefaultColor;
    UIColor* imgColor = ([action.status boolValue]) ? kVoteButtonActiveTitleColor : kButtonTitleColor;
    UIImage* img = [TeamieUIGlobals defaultPicForPurpose:name
                                                      withSize:kButtonFont.pointSize
                                                      andColor:imgColor];
    [button setImage:img forState:UIControlStateNormal];
    
    button.backgroundColor = backgroundColor;
    button.layer.borderColor = [action.status boolValue]? [backgroundColor CGColor] : [kVoteButtonDefaultBorderColor CGColor];
    
    if ([action.access boolValue]){
        [button addTarget:self
                   action:selector
         forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)configureMarkRightButton:(UIButton*)button{
    UIColor* backgroundColor = ([self.comment.isMarkedRight boolValue]) ? kMarkRightButtonActiveColor : kVoteButtonDefaultColor;
    UIColor* imgColor = ([self.comment.isMarkedRight boolValue]) ? kVoteButtonActiveTitleColor : kButtonTitleColor;
    UIImage* img = [TeamieUIGlobals defaultPicForPurpose:@"ok"
                                                      withSize:kButtonFont.pointSize
                                                      andColor:imgColor];
    
    button.backgroundColor = backgroundColor;
    button.layer.borderColor = [self.comment.isMarkedRight boolValue]? [backgroundColor CGColor] : [kVoteButtonDefaultBorderColor CGColor];
    
    [button setImage:img forState:UIControlStateNormal];
    
    TMEPostAction* markRightAction = [self.comment actionWithName:@"mark-right"];
    if (markRightAction && [markRightAction.access boolValue]){
        [button addTarget:self
                   action:@selector(markRightButtonDidTapped:)
         forControlEvents:UIControlEventTouchUpInside];
    }
}

#pragma mark - Comment Attachments
- (void)showAttachments{
    if ([self.comment.attachments count] == 0){
        return;
    }
    
    self.imageAttachments = [self filterImageAttachments];
    NSMutableArray* nonImageAttachments = [NSMutableArray arrayWithArray:self.comment.attachments];
    [nonImageAttachments removeObjectsInArray:self.imageAttachments];
    self.nonImageAttachments = [nonImageAttachments copy];
    
    NSMutableArray* attachmentViews = [[NSMutableArray alloc]init];
    if ([self.imageAttachments count] > 0){
        [attachmentViews addObject:[self createImageAttachmentView]];
    }
    
    if ([self.nonImageAttachments count] > 0){
        [attachmentViews addObjectsFromArray:[self createNonImageAttachmentViews]];
    }
    
    [self layoutAttachmentViews:attachmentViews];
}

- (UIView*)createImageAttachmentView{
    ThoughtAttachmentView* imageAttachmentView = [[ThoughtImageAttachmentView alloc] initWithImageAttachments:self.imageAttachments delegate:self];
    
    self.bottomAttachmentView = imageAttachmentView;
    [self addSubview:imageAttachmentView];
    
    return imageAttachmentView;
}

- (NSArray*)createNonImageAttachmentViews{
    int attachmentCount = [self.nonImageAttachments count];
    NSMutableArray* viewArray = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < attachmentCount; i++){
        TMEPostAttachment* attachment = [self.nonImageAttachments objectAtIndex:i];
        
        ThoughtDefaultAttachmentView* attachmentView = [[ThoughtDefaultAttachmentView alloc] initWithAttachment:attachment];
        [self addSubview:attachmentView];
        
        // UI Style
        attachmentView.backgroundColor = [UIColor whiteColor];
        attachmentView.layer.borderColor = [[UIColor colorWithHex:@"#EEEEEE" alpha:1.0] CGColor];
        attachmentView.layer.borderWidth = 1.0;
        attachmentView.layer.cornerRadius = 3.0;
        
        // Tap gesture
        UITapGestureRecognizer* gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(nonImageAttachmentDidTapped:)];
        attachmentView.tag = i;
        
        [attachmentView addGestureRecognizer:gesture];
        
        [viewArray addObject:attachmentView];
    }
    
    if ([viewArray count] > 0){
        self.bottomAttachmentView = [viewArray lastObject];
    }
    
    return viewArray;
}

- (void)layoutAttachmentViews:(NSArray*)views{
    [self removeConstraint:self.cmtMessageBodyBottomConstraint];
    
    NSArray* constraints = [self verticallyPinSubviews:views
                      underTopView:self.cmtMessageBody
                        bottomView:nil
                         topMargin:2.0
                      bottomMargin:kVerticalDistance
                   leftRightMargin:kLeftMargin
                         vDistance:kVerticalDistance];
    
    self.bottomAttachmentViewBottomConstraint = [constraints lastObject];
}

#pragma mark - Reply statistics and actions
- (void)showActionBar{
    UIView* actionBar = [[UIView alloc] init];
    [self addSubview:actionBar];
    actionBar.translatesAutoresizingMaskIntoConstraints = NO;
    
    // Add Subviews
    CGFloat nowX = 0;
    if ([self.comment.repliesCount intValue] > 0){
        UIButton* replyStatButton = [self createReplyStatButton];
        replyStatButton.frame = CGRectMake(nowX, 0.0, replyStatButton.frame.size.width, kReplyButtonHeight);
        
        [actionBar addSubview:replyStatButton];
        nowX += replyStatButton.frame.size.width;
    }
    
    if ([self.comment actionWithName:@"reply"]){
        UIButton* replyButton = [self createReplyButton];
        replyButton.frame = CGRectMake(nowX, 0.0, replyButton.frame.size.width, kReplyButtonHeight);
    
        [actionBar addSubview:replyButton];
        nowX += replyButton.frame.size.width;
    }
    
    // UI Style
    actionBar.backgroundColor = [UIColor colorWithHex:@"#FFFFFF" alpha:1.0];
    actionBar.layer.borderColor = [[UIColor colorWithHex:@"#CCCCCC" alpha:1.0] CGColor];
    actionBar.layer.borderWidth = 1.0;
    actionBar.layer.cornerRadius = 4.0;
    
    NSPredicate* predicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if ([evaluatedObject isKindOfClass:[UIButton class]]){
            return YES;
        }
        return NO;
    }];
    NSArray* buttonArray = [actionBar.subviews filteredArrayUsingPredicate:predicate];
    for (UIButton* button in buttonArray){
        if (button != [buttonArray lastObject]){
            [button drawRightBorderWithColor:[kButtonBorderColor CGColor] borderWidth:1.0];
        }
    }
    
    // Layout Constraints
    [self removeConstraint:self.cmtMessageBodyBottomConstraint];
    [self removeConstraint:self.bottomAttachmentViewBottomConstraint];
    
    UIView* topView = self.bottomAttachmentView ? self.bottomAttachmentView : self.cmtMessageBody;
    CGFloat barWidth = nowX;
    CGFloat topMargin = topView == self.cmtMessageBody? 2.0 : kVerticalDistance;
    
    NSDictionary* metrics = @{@"topMargin":[NSNumber numberWithFloat:topMargin],
                              @"leftMargin":[NSNumber numberWithFloat:kLeftMargin],
                              @"bottomMargin":[NSNumber numberWithFloat:kVerticalDistance],
                              @"width":[NSNumber numberWithFloat:barWidth],
                              @"height":[NSNumber numberWithFloat:kReplyButtonHeight]};
    NSDictionary* views = NSDictionaryOfVariableBindings(actionBar, topView);
    
    NSArray* hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-(leftMargin)-[actionBar(width)]"
                                                                    options:NSLayoutFormatAlignAllLeading
                                                                    metrics:metrics
                                                                      views:views];
    
    NSArray* vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topView]-topMargin-[actionBar(height)]-(bottomMargin)-|"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views];
    [self addConstraints:hConstraints];
    [self addConstraints:vConstraints];
    
    self.actionBar = actionBar;
}

- (UIButton*)createReplyStatButton{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];

    [self styleReplyBarButton:button];
    
    // Set image
    UIImage* image = [TeamieUIGlobals defaultPicForPurpose:@"reply"
                                                  withSize:button.titleLabel.font.pointSize
                                                  andColor:button.titleLabel.textColor];
    [button setImage:image forState:UIControlStateNormal];
    
    // Set text
    UIFontDescriptor* boldDescriptor = [button.titleLabel.font.fontDescriptor fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitBold];
    UIFont* font = [UIFont fontWithDescriptor:boldDescriptor
                                                  size:button.titleLabel.font.pointSize];
    UIColor* color = button.titleLabel.textColor;
    NSDictionary* attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,
                                color, NSForegroundColorAttributeName, nil];
    
    NSAttributedString* replyCountText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@" %i",[self.comment.repliesCount intValue]] attributes:attributes];
    
    NSMutableAttributedString* title = [[NSMutableAttributedString alloc] initWithAttributedString:replyCountText];
    [title appendAttributedString:[[NSAttributedString alloc] initWithString:@" replies"]];
    
    [button setAttributedTitle:title forState:UIControlStateNormal];
    
    [button sizeToFit];
    
    // Add target
    [button addTarget:self action:@selector(replyStatButtonDidTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (UIButton*)createReplyButton{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    // Style button
    [self styleReplyBarButton:button];
    
    UIImage* image = [TeamieUIGlobals defaultPicForPurpose:@"reply"
                                                  withSize:button.titleLabel.font.pointSize
                                                  andColor:button.titleLabel.textColor];
    [button setImage:image forState:UIControlStateNormal];

    [button setTitle:@" Reply" forState:UIControlStateNormal];
    
    [button sizeToFit];
    
    // Add target
    [button addTarget:self
               action:@selector(replyButtonDidTapped:)
     forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

- (UIButton*)createMoreActionButton{
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    [self styleReplyBarButton:button];
    
    UIImage* image = [TeamieUIGlobals defaultPicForPurpose:@"ellipsis"
                                                  withSize:button.titleLabel.font.pointSize
                                                  andColor:button.titleLabel.textColor];
    [button setImage:image forState:UIControlStateNormal];

    [button sizeToFit];
    
    return button;
}

- (void)styleReplyBarButton:(UIButton*)button{
    button.contentEdgeInsets = UIEdgeInsetsMake(0.0, 12.0, 0.0, 12.0);
    [button setTitleColor:[kButtonTitleColor copy] forState:UIControlStateNormal];
    button.titleLabel.textColor = [kButtonTitleColor copy];
    button.titleLabel.font = [kButtonFont copy];
}

#pragma mark - Reply Stream
- (void)showReplyStream{
    if (!self.topReplyView && [self.comment.replies count] > 0){    // has replies but not shown yet
        [self showReplies:self.comment.replies attachToTop:YES];
    }
    
    [self addSubview:self.replyStream];
    
    //Add reply stream to self
    UIView* topView = self.actionBar? self.actionBar : self.bottomAttachmentView? self.bottomAttachmentView : self.cmtMessageBody;
    
    NSDictionary* metrics = @{@"margin":[NSNumber numberWithFloat:kVerticalDistance]};
    NSDictionary* views = @{@"replyStream":self.replyStream,
                            @"topView":topView};
    NSArray* vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[topView]-(margin)-[replyStream]|"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views];
    
    // Animate showing reply stream
    [self layoutIfNeeded];
    
    NSLayoutConstraint* topViewBottomConstraint = [self bottomConstraintOfView:topView];
    
    [self setNeedsUpdateConstraints];
    
    CGFloat topViewBottomDistance = self.replyStream.frame.size.height + kVerticalDistance;
    topViewBottomConstraint.constant = (topViewBottomConstraint.firstItem == self)? topViewBottomDistance : -topViewBottomDistance;
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        [self.superview layoutIfNeeded];
    }];

    [self removeConstraint:topViewBottomConstraint];
    
    [self addConstraints:self.replyStreamHorConstraints];
    [self addConstraints:vConstraints];
}

- (void)removeReplyStream{
    UIView* topView = self.actionBar? self.actionBar : self.bottomAttachmentView? self.bottomAttachmentView : self.cmtMessageBody;
    
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    
    CGFloat topViewBottomToSuperBottom = kVerticalDistance * 2 + self.replyStream.bounds.size.height;
    
    NSLayoutConstraint* topViewBottomConstraint = [NSLayoutConstraint constraintWithItem:topView
                                                                  attribute:NSLayoutAttributeBottom
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self
                                                                  attribute:NSLayoutAttributeBottom
                                                                 multiplier:1.0
                                                                   constant:-topViewBottomToSuperBottom];
    
    // Animate removing reply stream
    [self removeConstraint:[self bottomConstraintOfView:self.replyStream]];
    [self addConstraint:topViewBottomConstraint];
    
    [self setNeedsUpdateConstraints];
    [self updateConstraintsIfNeeded];
    
    [self setNeedsUpdateConstraints];
    
    topViewBottomConstraint.constant = -kVerticalDistance;
    
    [UIView animateWithDuration:kAnimationDuration animations:^{
        [self.superview layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.replyStream removeFromSuperview];
    }];
}

- (void)showReplies:(NSArray*)replyArray attachToTop:(BOOL)shouldAttachToTop{
    if ([replyArray count] == 0){
        return;
    }
    
    // Create views
    NSMutableArray* replyViews = [[NSMutableArray alloc] init];
    
    for (TMEPostCommentReply* reply in replyArray){
        ThoughtCommentReplyView* view = [[ThoughtCommentReplyView alloc] initWithReply:reply
                                                                              delegate:self];
        view.backgroundColor = [UIColor colorWithHex:@"#E7EAEF" alpha:1.0];
        
        [self.replyStream addSubview:view];
        [replyViews addObject:view];
    }
    
    // Add layout constraints
    UIView* topView;
    UIView* bottomView;
    if (shouldAttachToTop || !self.topReplyView){ // added replies should stay at top
        topView = self.hasShownLoadReplyButton? self.loadReplyButton : nil;
        bottomView = self.topReplyView;
        
        if (self.topReplyViewTopConstraint){
            [self.replyStream removeConstraint:self.topReplyViewTopConstraint];
        }
    }
    else{
        topView = self.bottomReplyView;
        bottomView = nil;
        
        if (self.bottomReplyViewBottomConstraint){
            [self.replyStream removeConstraint:self.bottomReplyViewBottomConstraint];
        }
    }
    
    NSArray* constraints = [self.replyStream verticallyPinSubviews:replyViews
                                                         underTopView:topView
                                                           bottomView:bottomView
                                                            topMargin:10.0
                                                         bottomMargin:10.0
                                                      leftRightMargin:10.0
                                                            vDistance:0.0];
    
    if (!self.topReplyView && !self.bottomReplyView){   // has not shown comments
        self.topReplyView = [replyViews firstObject];
        self.bottomReplyView = [replyViews lastObject];
        self.topReplyViewTopConstraint = [constraints firstObject];
        self.bottomReplyViewBottomConstraint = [constraints lastObject];
    }
    else if (shouldAttachToTop){
        self.topReplyView = [replyViews firstObject];
        self.topReplyViewTopConstraint = [constraints firstObject];
    }
    else{
        self.bottomReplyView = [replyViews lastObject];
        self.bottomReplyViewBottomConstraint = [constraints lastObject];
    }
    
    [self.replyStream addConstraints:constraints];
    
    // Handle load reply button
    if ([self.comment.replies count] < [self.comment.repliesCount intValue] && !self.hasShownLoadReplyButton){
        [self showLoadReplyButton];
    }
    else if ([self.comment.replies count] >= [self.comment.repliesCount intValue] && self.hasShownLoadReplyButton){
        [self removeLoadReplyButton];
    }
}

- (void)showLoadReplyButton{
    if (self.hasShownLoadReplyButton){
        return;
    }
    
    // Add load comment button
    [self.replyStream addSubview:self.loadReplyButton];
    self.hasShownLoadReplyButton = YES;
    
    [self.replyStream removeConstraint:self.topReplyViewTopConstraint];
    
    // Update constraints in comment stream view
    NSDictionary* metrics = @{@"height":@20};
    NSDictionary* views = @{@"loadButton":self.loadReplyButton,
                            @"topReplyView":self.topReplyView};
    
    NSArray* vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[loadButton(height)]-10-[topReplyView]"
                                                                    options:0
                                                                    metrics:metrics
                                                                      views:views];
    
    [self.replyStream addConstraints:vConstraints];
    [self.replyStream addConstraints:self.loadReplyButtonHorConstraints];
    self.topReplyViewTopConstraint = [vConstraints lastObject];
}

- (void)removeLoadReplyButton{
    if (!self.hasShownLoadReplyButton){
        return;
    }
        
    // Remove load comment button
    [self.loadReplyButton removeFromSuperview];
    self.hasShownLoadReplyButton = NO;
    
    // Update constraint in comment stream view
    self.topReplyViewTopConstraint = [NSLayoutConstraint constraintWithItem:self.topReplyView
                                                                    attribute:NSLayoutAttributeTop
                                                                    relatedBy:NSLayoutRelationEqual
                                                                       toItem:self.replyStream
                                                                    attribute:NSLayoutAttributeTop
                                                                   multiplier:1.0
                                                                     constant:0.0];
    [self.replyStream addConstraint:self.topReplyViewTopConstraint];
    
}

#pragma mark - UITextview delegate
//- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange{
//    [TeamieGlobals openInappBrowserWithURL:URL
//                        fromViewController:(UIViewController*)self.delegate];
//    return NO;
//}

#pragma mark - ThoughtCommentReplyView delegate
- (void)replyViewDidLayoutSubviews:(ThoughtCommentReplyView *)replyView{
    // Any stuffs related to CALayer's anchor point and frame should be here
    //      because CALayer does not autosize with autolayout
    
    if (replyView != self.bottomReplyView){
        [replyView clearBorderLayers];
        [replyView drawBorderAtTop:NO right:NO bottom:YES left:NO
                         withColor:[[UIColor colorWithHex:@"#CCCCCC" alpha:1.0]CGColor]
                        borderWith:1.0
                      cornerRadius:0.0
                         lineStyle:kDashedLine];
    }
}

#pragma mark - Delegate methods for navigating
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    if ([self.delegate respondsToSelector:@selector(pushViewController:animated:)]){
        [self.delegate performSelector:@selector(pushViewController:animated:)
                            withObject:viewController
                            withObject:[NSNumber numberWithBool:animated]];
    }
}

#pragma mark - Target Action methods
- (void)voteUpButtonDidTapped:(id)sender{
    if ([self.delegate respondsToSelector:@selector(commentVoteUpButtonDidTapped:)]){
        [self.delegate performSelector:@selector(commentVoteUpButtonDidTapped:) withObject:self];
    }
}

- (void)voteDownButtonDidTapped:(id)sender{
    if ([self.delegate respondsToSelector:@selector(commentVoteDownButtonDidTapped:)]){
        [self.delegate performSelector:@selector(commentVoteDownButtonDidTapped:) withObject:self];
    }
}

- (void)markRightButtonDidTapped:(id)sender{
    if ([self.delegate respondsToSelector:@selector(commentMarkRightButtonDidTapped:)]){
        [self.delegate performSelector:@selector(commentMarkRightButtonDidTapped:) withObject:self];
    }
}

- (void)nonImageAttachmentDidTapped:(UITapGestureRecognizer*)sender{
//    int index = (int)sender.view.tag;
    
//    TMEPostAttachment* attachment = [self.nonImageAttachments objectAtIndex:index];
//    if ([attachment respondsToSelector:@selector(href)]) {
//        NSNumber *attachmentType;
//        if ([attachment.type isEqualToString:@"link"]) {
//            attachmentType = [NSNumber numberWithInt:kGoogleAnalyticsLinkReferenceValue];
//        }
//        else if ([attachment.type isEqualToString:@"video"]) {
//            attachmentType = [NSNumber numberWithInt:kGoogleAnalyticsVideoReferenceValue];
//        }
//        [ARAnalytics event:@"tap_attachment" withProperties:@{@"category": @"full_post",
//                                                              @"label": @"comment",
//                                                              @"value": attachmentType}];
//        
//        [TeamieGlobals openInappBrowserWithURL:[NSURL URLWithString:[attachment performSelector:@selector(href)]]
//                            fromViewController:(UIViewController*)self.delegate];
//    }
}

- (void)replyStatButtonDidTapped:(id)sender{
    if (self.comment.repliesCount > 0){
        if (![self.replyStream isDescendantOfView:self]){
            [self showReplyStream];
        }
        else{
            [self removeReplyStream];
        }
    }
}

- (void)replyButtonDidTapped:(id)sender{
    if ([self.delegate respondsToSelector:@selector(commentReplyButtonDidTapped:)]){
        [self.delegate performSelector:@selector(commentReplyButtonDidTapped:) withObject:self];
    }
}

- (void)loadReplyButtonDidTapped:(id)sender{
    if ([self.delegate respondsToSelector:@selector(commentLoadReplyButtonDidTapped:)]){
        [self.delegate performSelector:@selector(commentLoadReplyButtonDidTapped:) withObject:self];
    }
}

#pragma mark - Lazy Instantiation
- (CGRect)actionBarFrame{
    if (_actionBar){
        return _actionBar.frame;
    }
    return CGRectZero;
}

- (UIView*)replyStream{
    if (!_replyStream){
        _replyStream = [[UIView alloc] init];        
        _replyStream.backgroundColor = [UIColor colorWithHex:@"#E7EAEF" alpha:1.0];
        
        _replyStream.translatesAutoresizingMaskIntoConstraints = NO;
    }
    return _replyStream;
}

- (NSArray*)replyStreamHorConstraints{
    if (!_replyStreamHorConstraints){
        NSDictionary* views = @{@"replyStream":self.replyStream};
        _replyStreamHorConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[replyStream]|"
                                                                             options:0
                                                                             metrics:0
                                                                               views:views];
    }
    return _replyStreamHorConstraints;
}

- (UIButton*)loadReplyButton{
    if (!_loadReplyButton){
        UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:@"Load previous replies" forState:UIControlStateNormal];
        [button setTitleColor:[TeamieUIGlobalColors defaultTextColor] forState:UIControlStateNormal];
        button.titleLabel.font = [kCmtFont copy];
        
        button.translatesAutoresizingMaskIntoConstraints = NO;
        
        [button addTarget:self
                   action:@selector(loadReplyButtonDidTapped:)
         forControlEvents:UIControlEventTouchUpInside];
        
        _loadReplyButton = button;
    }
    return _loadReplyButton;
}

- (NSArray*)loadReplyButtonHorConstraints{
    if (!_loadReplyButtonHorConstraints){
        UIButton* loadButton = self.loadReplyButton;
        NSDictionary* views = NSDictionaryOfVariableBindings(loadButton);
        _loadReplyButtonHorConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"|[loadButton]|"
                                                                                   options:NSLayoutFormatAlignAllCenterX
                                                                                   metrics:nil
                                                                                     views:views];
    }
    return _loadReplyButtonHorConstraints;
}

#pragma mark - Other helper methods
- (NSArray*)copyFromArray:(NSArray*)firstArray followByArray:(NSArray*)secondArray{
    NSMutableArray* result = [NSMutableArray arrayWithArray:firstArray];
    [result addObjectsFromArray:secondArray];
    return result;
}

- (NSArray*)filterImageAttachments{
    NSPredicate* predicate = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject, NSDictionary *bindings) {
        if (![evaluatedObject isKindOfClass:[TMEPostAttachment class]]){
            return NO;
        }
        
//        TMEPostAttachment* attachment = (TMEPostAttachment*)evaluatedObject;
//        if ([attachment.type isEqualToString:@"image"]){
//            return YES;
//        }
        
        return NO;
    }];
    
    return [self.comment.attachments filteredArrayUsingPredicate:predicate];
}

- (NSLayoutConstraint*)bottomConstraintOfView:(UIView*)view{
    for (NSLayoutConstraint* constraint in self.constraints){
        if ((constraint.firstItem == view && constraint.firstAttribute == NSLayoutAttributeBottom) || (constraint.secondItem == view && constraint.secondAttribute == NSLayoutAttributeBottom)){
            return constraint;
        }
    }
    return nil;
}

@end
