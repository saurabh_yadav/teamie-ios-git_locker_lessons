//
//  NewsfeedItem.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 12/3/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "NSManagedObject+Easyfetching.h"

@class TMEClassroom, NewsfeedItemAction, NewsfeedItemAttachment, NewsfeedItemComment, UserProfile;

@interface NewsfeedItem : NSManagedObject

@property (nonatomic, retain) NSNumber * countComment;
@property (nonatomic, retain) NSNumber * countEcho;
@property (nonatomic, retain) NSNumber * countLike;
@property (nonatomic, retain) NSString * htmlmessage;
@property (nonatomic, retain) NSString * message;
@property (nonatomic, retain) NSNumber * tid;
@property (nonatomic, retain) NSNumber * timestamp;
@property (nonatomic, retain) NSString * timestampText;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSSet *actions;
@property (nonatomic, retain) NSSet *attachment;
@property (nonatomic, retain) NSSet *images;        // a temporary property added to map array of image attachments
@property (nonatomic, retain) UserProfile *author;
@property (nonatomic, retain) TMEClassroom *classrooms;
@property (nonatomic, retain) NSSet *comments;
@property (nonatomic, retain) NSNumber * isAnnouncement;
@property (nonatomic, retain) NSNumber * homeworkDeadline;
@end

@interface NewsfeedItem (CoreDataGeneratedAccessors)

- (void)addActionsObject:(NewsfeedItemAction *)value;
- (void)removeActionsObject:(NewsfeedItemAction *)value;
- (void)addActions:(NSSet *)values;
- (void)removeActions:(NSSet *)values;

- (void)addAttachmentObject:(NewsfeedItemAttachment *)value;
- (void)removeAttachmentObject:(NewsfeedItemAttachment *)value;
- (void)addAttachment:(NSSet *)values;
- (void)removeAttachment:(NSSet *)values;

- (void)addCommentsObject:(NewsfeedItemComment *)value;
- (void)removeCommentsObject:(NewsfeedItemComment *)value;
- (void)addComments:(NSSet *)values;
- (void)removeComments:(NSSet *)values;

@end
