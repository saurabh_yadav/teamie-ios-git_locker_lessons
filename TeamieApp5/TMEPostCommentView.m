//
//  TMEPostCommentView.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 23/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostCommentView.h"

#import "TMEPostAttachment.h"
#import "TMEImageAttachment.h"
#import "TMEVideoAttachment.h"
#import "TMEPostAttachmentView.h"
#import "TMEPostCommentReply.h"
#import "TMEPostCommentReplyView.h"
#import "TMEClient.h"
#import "TMEPostAction.h"
#import "UserProfileViewController.h"

#import "UIView+TMEEssentials.h"
#import "UIImage+TMEColor.h"

#import <Masonry/Masonry.h>
#import <ReactiveCocoa/RACEXTScope.h>
#import <SDWebImage/UIImageView+WebCache.h>

@interface TMEPostCommentView()
<TMEPostAttachmentViewDelegate>

@property (nonatomic, weak) MASConstraint *likeLabelWidthConstraint;
@property (nonatomic, weak) MASConstraint *likeButtonWidthConstraint;
@property (nonatomic, weak) MASConstraint *voteLabelWidthConstraint;
@property (nonatomic, weak) MASConstraint *voteUpButtonWidthConstraint;
@property (nonatomic, weak) MASConstraint *voteDownButtonWidthConstraint;
@property (nonatomic, weak) MASConstraint *markRightButtonWidthConstraint;

@property (nonatomic) BOOL areRepliesShown;

@end

@implementation TMEPostCommentView

static const CGFloat kDefaultPadding = 10;
static const CGFloat kDefaultSize = 12;
static const NSInteger kActionSheetTag = 1;
static NSInteger kReportAction = -1;
static NSInteger kAllRepliesAction = -1;

+ (instancetype)viewWithComment:(TMEPostComment *)comment delegate:(UIViewController<TMEPostCommentViewDelegate> *)delegate {
    TMEPostCommentView *postCommentView = [super new];
    if (postCommentView) {
        postCommentView.postCommentLevel = TMEPostCommentLevelComment;
        postCommentView.comment = comment;
        postCommentView.delegate = delegate;
        postCommentView.backgroundColor = [TeamieUIGlobalColors postCommentBackgroundColor];
        postCommentView.areRepliesShown = NO;
        
        [postCommentView createHeader];
        [postCommentView loadContent];
        [postCommentView loadAttachments];
        [postCommentView loadRepliesView];
        [postCommentView addActions];
        [postCommentView setStatus];
        
        postCommentView.imageSize = @30;
        postCommentView.leftIndent = 0;
        postCommentView.rightIndent = 0;
        [postCommentView addConstraints];
    }
    return postCommentView;
}

- (void)addConstraints {
    [self.authorImageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(kDefaultPadding + (30 - self.imageSize.intValue)/4);
        make.left.equalTo(self).offset(kDefaultPadding + self.leftIndent);
        make.height.equalTo(self.imageSize);
        make.width.equalTo(self.imageSize);
    }];
    [self.authorNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.authorImageButton.mas_right).offset(kDefaultPadding);
        make.top.equalTo(self).offset(8);
    }];
    [self.timePostedLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.authorImageButton.mas_right).offset(kDefaultPadding);
        make.top.equalTo(self.authorNameLabel.mas_bottom);
    }];
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.authorNameLabel.mas_right).offset(kDefaultPadding);
        make.left.equalTo(self.timePostedLabel.mas_right).offset(kDefaultPadding);
        make.centerY.equalTo(self.authorImageButton);
        make.right.equalTo(self).offset(-(kDefaultPadding + self.rightIndent));
        make.height.width.equalTo(@15);
    }];
    [self.commentTextView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.timePostedLabel.mas_bottom);
        make.left.equalTo(self).offset(kDefaultPadding + self.leftIndent);
        make.right.equalTo(self).offset(-(kDefaultPadding  + self.rightIndent));
    }];
    [self.attachmentsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.commentTextView.mas_bottom);
        make.left.equalTo(self).offset(kDefaultPadding + self.leftIndent);
        make.right.equalTo(self).offset(-(kDefaultPadding  + self.rightIndent));
    }];
    [self.leftActionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.attachmentsView.mas_bottom);
        make.left.equalTo(self).offset(kDefaultPadding + self.leftIndent);
        make.height.equalTo(self.leftActionView.hidden? @0 : @25);
    }];
    [self.repliesCountButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.top.equalTo(self.leftActionView);
        make.width.equalTo(@((kDefaultPadding + kDefaultSize) * 2));
    }];
    [self.replyButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.top.equalTo(self.leftActionView);
        make.left.equalTo(self.repliesCountButton.mas_right);
        make.width.equalTo(@((kDefaultPadding + kDefaultSize) * 2));
    }];
    [self.rightActionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.attachmentsView.mas_bottom);
        make.right.equalTo(self).offset(-(kDefaultPadding + self.rightIndent));
        make.height.equalTo(self.rightActionView.hidden? @0 : @25);
    }];
    [self.likesCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.bottom.equalTo(self.rightActionView);
        make.width.equalTo(@(self.likesCountLabel.hidden? 0 : kDefaultSize + kDefaultPadding * 2));
    }];
    [self.likeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.rightActionView);
        make.left.equalTo(self.likesCountLabel.mas_right);
        make.width.equalTo(@(self.likeButton.hidden? 0 : kDefaultSize + kDefaultPadding * 2));
    }];
    [self.votesCountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.rightActionView);
        make.left.equalTo(self.likeButton.mas_right);
        make.width.equalTo(@(self.votesCountLabel.hidden? 0 : kDefaultSize + kDefaultPadding * 2));
    }];
    [self.voteUpButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.rightActionView);
        make.left.equalTo(self.votesCountLabel.mas_right);
        make.width.equalTo(@(self.voteUpButton.hidden? 0 : kDefaultSize + kDefaultPadding * 2));
    }];
    [self.voteDownButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.rightActionView);
        make.left.equalTo(self.voteUpButton.mas_right);
        make.width.equalTo(@(self.voteDownButton.hidden? 0 : kDefaultSize + kDefaultPadding * 2));
    }];
    [self.markRightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.right.equalTo(self.rightActionView);
        make.left.equalTo(self.voteDownButton.mas_right);
        make.width.equalTo(@(self.markRightButton.hidden? 0 : kDefaultSize + kDefaultPadding * 2));
    }];
    [self.repliesView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.greaterThanOrEqualTo(self.leftActionView.mas_bottom).offset(kDefaultPadding);
        make.top.greaterThanOrEqualTo(self.rightActionView.mas_bottom).offset(kDefaultPadding);
        make.left.right.bottom.equalTo(self);
    }];
}

#pragma mark - Adding Subviews

- (void)createHeader {
    self.authorImageButton = [self createAndAddSubView:UIButton.class];
    self.authorImageButton.layer.cornerRadius = 2;
    self.authorImageButton.layer.masksToBounds = YES;
    self.authorImageButton.contentMode = UIViewContentModeScaleAspectFill;
    
    if (self.comment.author.picture) {
        [self.authorImageButton setImage:self.comment.author.picture
                                forState:UIControlStateNormal];
        [self.authorImageButton addTarget:self action:@selector(openAuthorProfile) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        [self.authorImageButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"profile"]
                                forState:UIControlStateNormal];
    }
    
    self.authorNameLabel = [self createAndAddSubView:UILabel.class];
    self.authorNameLabel.text = self.comment.author.realName;
    self.authorNameLabel.numberOfLines = 0;
    self.authorNameLabel.font = [TeamieGlobals appFontFor:@"commentTitle"];
    self.authorNameLabel.textColor = [TeamieUIGlobalColors defaultTextColor];

    self.timePostedLabel = [self createAndAddSubView:UILabel.class];
    self.timePostedLabel.text = self.comment.timestampText;
    self.timePostedLabel.font = [TeamieGlobals appFontFor:@"commentSubtitle"];
    self.authorNameLabel.textColor = [TeamieUIGlobalColors defaultTextColor];
    
    self.moreButton = [self createAndAddSubView:UIButton.class];
    self.moreButton.contentMode = UIViewContentModeScaleAspectFill;
    [self.moreButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"ellipsis"
                                                           withSize:12
                                                           andColor:[UIColor darkGrayColor]]
                     forState:UIControlStateNormal];
    [self.moreButton addTarget:self action:@selector(moreButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    TMEPostAction *delete = self.comment.actions[@"delete-thought-comment"];
    TMEPostAction *report = self.comment.actions[@"report"];
    self.moreButton.hidden = !(delete.access || report.access || self.comment.repliesCount.unsignedIntegerValue > self.comment.replies.count);
}

- (void)loadContent {
    self.commentTextView = [self createAndAddSubView:UITextView.class];
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        self.commentTextView.attributedText = [[NSMutableAttributedString alloc]
                                               initWithData:[self.comment.htmlMessage dataUsingEncoding:NSUTF8StringEncoding]
                                               options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                         NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                               documentAttributes:nil
                                               error:nil];
    }
    else {
        self.commentTextView.text = self.comment.message;
    }
    self.commentTextView.font = [TeamieGlobals appFontFor:@"commentBody"];
    self.commentTextView.textColor = [TeamieUIGlobalColors defaultTextColor];
    self.commentTextView.contentInset = UIEdgeInsetsZero;
    self.commentTextView.scrollEnabled = NO;
    self.commentTextView.editable = NO;
    self.commentTextView.backgroundColor = [TeamieUIGlobalColors postCommentBackgroundColor];
}

- (void)loadAttachments {
    self.attachmentsView = [self createAndAddSubView:UIView.class];
    self.attachmentsView.backgroundColor = [UIColor whiteColor];
    self.attachmentsView.layer.cornerRadius = 3;
    self.attachmentsView.layer.masksToBounds = YES;
    
    __block TMEPostAttachmentView *prevAttachment;
    
    @weakify(self)
    [self.comment.attachments enumerateObjectsUsingBlock:^(TMEPostAttachment *attachment, NSUInteger idx, BOOL *stop) {
        @strongify(self)
        TMEPostAttachmentView *attachmentView = [[TMEPostAttachmentView alloc] initWithDelegate:self attachment:attachment];
        [self.attachmentsView addSubview:attachmentView];
        
        [attachmentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.attachmentsView);
            make.top.equalTo((prevAttachment)? prevAttachment.mas_bottom : self.attachmentsView.mas_top);
            if (idx == self.comment.attachments.count - 1) {
                make.bottom.equalTo(self.attachmentsView);
            }
        }];
        prevAttachment = attachmentView;
    }];
}

- (void)loadRepliesView {
    self.repliesView = [self createAndAddSubView:UIView.class];
    self.repliesView.translatesAutoresizingMaskIntoConstraints = NO;
    self.repliesView.alpha = 0;
    self.repliesView.backgroundColor = [TeamieUIGlobalColors postCommentBackgroundColor];
}

- (void)loadReplies {
    [self.repliesView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    __block TMEPostCommentReplyView *prevReply = nil;

    @weakify(self)
    [self.comment.replies enumerateObjectsUsingBlock:^(TMEPostCommentReply *reply, NSUInteger idx, BOOL *stop) {
        @strongify(self)
        TMEPostCommentReplyView *replyView = [TMEPostCommentReplyView viewWithReply:reply delegate:self.delegate];
        [replyView addBorderToTop:YES bottom:NO left:NO right:NO];
        [self.repliesView addSubview:replyView];
        
        [replyView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self.repliesView);
            make.top.equalTo((prevReply)? prevReply.mas_bottom : self.repliesView.mas_top);
            if (idx == self.comment.replies.count - 1) {
                make.bottom.equalTo(self.repliesView);
            }
        }];
        prevReply = replyView;
    }];
    self.areRepliesShown = YES;
}

- (void)deleteReplies {
    [self.repliesView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    self.areRepliesShown = NO;
}

- (void)addActions {
    self.leftActionView = [self createAndAddSubView:UIView.class];
    self.leftActionView.backgroundColor = [UIColor whiteColor];
    self.leftActionView.layer.masksToBounds = YES;
    self.leftActionView.layer.cornerRadius = 4;
    self.leftActionView.layer.borderColor = [TeamieUIGlobalColors borderColor].CGColor;
    self.leftActionView.layer.borderWidth = 1;

    self.repliesCountButton = [self.leftActionView createAndAddSubView:UIButton.class];
    self.repliesCountButton.titleLabel.font = [TeamieGlobals appFontFor:@"commentAction"];
    [self.repliesCountButton setTitleColor:[TeamieUIGlobalColors grayColor]
                                  forState:UIControlStateNormal];
    [self.repliesCountButton setTitle:[self.comment.repliesCount stringValue]
                             forState:UIControlStateNormal];
    [self.repliesCountButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"reply"
                                                                   withSize:11
                                                                   andColor:[TeamieUIGlobalColors grayColor]]
                             forState:UIControlStateNormal];
    self.repliesCountButton.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.repliesCountButton.contentEdgeInsets = UIEdgeInsetsMake(kDefaultPadding, kDefaultPadding, kDefaultPadding, kDefaultPadding);
    self.repliesCountButton.titleEdgeInsets = UIEdgeInsetsMake(0, -8, 0, kDefaultSize);
    self.repliesCountButton.imageEdgeInsets = UIEdgeInsetsMake(-3, kDefaultSize, -3, 0);
    [self.repliesCountButton addTarget:self action:@selector(replyCountButtonClicked) forControlEvents:UIControlEventTouchUpInside];
    
    self.replyButton = [self.leftActionView createAndAddSubView:UIButton.class];
    [self.replyButton addBorderToTop:NO bottom:NO left:YES right:NO];
    [self.replyButton setTitleColor:[TeamieUIGlobalColors grayColor] forState:UIControlStateNormal];
    self.replyButton.titleLabel.font = [TeamieGlobals appFontFor:@"commentSubtitle"];
    [self.replyButton setTitle:@"Reply" forState:UIControlStateNormal];
    [self.replyButton addTarget:self action:@selector(replyButtonClicked) forControlEvents:UIControlEventTouchUpInside];

    self.rightActionView = [self createAndAddSubView:UIView.class];
    self.rightActionView.backgroundColor = [UIColor whiteColor];
    self.rightActionView.layer.masksToBounds = YES;
    self.rightActionView.layer.cornerRadius = 4;
    self.rightActionView.layer.borderColor = [TeamieUIGlobalColors borderColor].CGColor;
    self.rightActionView.layer.borderWidth = 1;

    self.likesCountLabel = [self.rightActionView createAndAddSubView:UILabel.class];
    self.likesCountLabel.font = [TeamieGlobals appFontFor:@"commentAction"];
    self.likesCountLabel.textColor = [TeamieUIGlobalColors grayColor];
    self.likesCountLabel.textAlignment = NSTextAlignmentCenter;

    self.likeButton = [self.rightActionView createAndAddSubView:UIButton.class];
    [self.likeButton addBorderToTop:NO bottom:NO left:YES right:NO];
    [self.likeButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"like" withSize:11 andColor:[TeamieUIGlobalColors grayColor]]
                       forState:UIControlStateNormal];
    [self.likeButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]
                                 forState:UIControlStateNormal];
    [self.likeButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"like" withSize:11 andColor:[UIColor whiteColor]]
                       forState:UIControlStateSelected];
    [self.likeButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors primaryActionColor]]
                                 forState:UIControlStateSelected];
    [self.likeButton addTarget:self action:@selector(likeToggle) forControlEvents:UIControlEventTouchUpInside];

    self.votesCountLabel = [self.rightActionView createAndAddSubView:UILabel.class];
    self.votesCountLabel.font = [TeamieGlobals appFontFor:@"commentAction"];
    self.votesCountLabel.textColor = [TeamieUIGlobalColors grayColor];
    self.votesCountLabel.textAlignment = NSTextAlignmentCenter;
    
    self.voteUpButton = [self.rightActionView createAndAddSubView:UIButton.class];
    [self.voteUpButton addBorderToTop:NO bottom:NO left:YES right:NO];
    [self.voteUpButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"voteup" withSize:11 andColor:[TeamieUIGlobalColors grayColor]]
                       forState:UIControlStateNormal];
    [self.voteUpButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]
                                 forState:UIControlStateNormal];
    [self.voteUpButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"voteup" withSize:11 andColor:[UIColor whiteColor]]
                       forState:UIControlStateSelected];
    [self.voteUpButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors primaryActionColor]]
                                 forState:UIControlStateSelected];
    [self.voteUpButton addTarget:self action:@selector(voteUpToggle) forControlEvents:UIControlEventTouchUpInside];

    self.voteDownButton = [self.rightActionView createAndAddSubView:UIButton.class];
    [self.voteDownButton addBorderToTop:NO bottom:NO left:YES right:NO];
    [self.voteDownButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"votedown" withSize:11 andColor:[TeamieUIGlobalColors grayColor]]
                       forState:UIControlStateNormal];
    [self.voteDownButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]
                                 forState:UIControlStateNormal];
    [self.voteDownButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"votedown" withSize:11 andColor:[UIColor whiteColor]]
                       forState:UIControlStateSelected];
    [self.voteDownButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors primaryActionColor]]
                                 forState:UIControlStateSelected];
    [self.voteDownButton addTarget:self action:@selector(voteDownToggle) forControlEvents:UIControlEventTouchUpInside];

    self.markRightButton = [self.self.rightActionView createAndAddSubView:UIButton.class];
    [self.markRightButton addBorderToTop:NO bottom:NO left:YES right:NO];
    [self.markRightButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"markright" withSize:11 andColor:[TeamieUIGlobalColors grayColor]]
                       forState:UIControlStateNormal];
    [self.markRightButton setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]
                                 forState:UIControlStateNormal];
    [self.markRightButton setImage:[TeamieUIGlobals defaultPicForPurpose:@"markright" withSize:11 andColor:[UIColor whiteColor]]
                       forState:UIControlStateSelected];
    [self.markRightButton setBackgroundImage:[UIImage imageWithColor:[TeamieUIGlobalColors primaryGreenColor]]
                                 forState:UIControlStateSelected];
    [self.markRightButton addTarget:self action:@selector(markRightToggle) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setStatus {
    if ([self.comment.likeCount respondsToSelector:@selector(stringValue)]) {
        self.likesCountLabel.text = [self.comment.likeCount stringValue];
        self.likesCountLabel.hidden = NO;
    }
    else {
        self.likesCountLabel.hidden = YES;
    }
    if ([self.comment action:@"like"]) {
        self.likeButton.selected = [self.comment action:@"like"].status;
        self.likeButton.hidden = NO;
    } else {
        self.likeButton.hidden = YES;
    }
    if ([self.comment.voteCount respondsToSelector:@selector(stringValue)]) {
        self.votesCountLabel.text = [self.comment.voteCount stringValue];
        self.votesCountLabel.hidden = NO;
    }
    else {
        self.votesCountLabel.hidden = YES;
    }
    if ([self.comment action:@"voteup"]) {
        self.voteUpButton.selected = [self.comment action:@"voteup"].status;
        self.voteUpButton.hidden = ![self.comment action:@"voteup"].access;
    } else {
        self.voteUpButton.hidden = YES;
    }
    if ([self.comment action:@"votedown"]) {
        self.voteDownButton.selected = [self.comment action:@"votedown"].status;
        self.voteDownButton.hidden = ![self.comment action:@"votedown"].access;
    } else {
        self.voteDownButton.hidden = YES;
    }
    if ([self.comment action:@"mark-right"]) {
        self.markRightButton.selected = [self.comment action:@"mark-right"].status;
        self.markRightButton.hidden = NO;
    } else {
        self.markRightButton.hidden = YES;
    }
    
    self.rightActionView.hidden = self.likesCountLabel.hidden &&
                                    self.likeButton.hidden &&
                                    self.votesCountLabel.hidden &&
                                    self.voteUpButton.hidden &&
                                    self.voteDownButton.hidden &&
                                    self.markRightButton.hidden;
}

#pragma mark - Action

- (void)likeToggle {
    TMEPostAction *like = [self.comment action:@"like"];
    TMETrackEvent(@"full_post",
                  self.postCommentLevel == TMEPostCommentLevelComment? @"flag_comment": @"flag_reply",
                  like.status? @"unlike" : @"like");
    if (like.access) {
        NSString *messageKey = like.status? @"message.unliking-comment": @"message.liking-comment";
        if ([self isKindOfClass:TMEPostCommentReplyView.class]) {
            messageKey = like.status? @"message.unliking-reply": @"message.liking-reply";
        }
        [[TMEClient sharedClient]
         request:TeamieUserLikeThoughtCommentRequest
         parameters:@{@"cid": self.comment.cid}
         loadingMessage:TMELocalize(messageKey)
         success:^(NSDictionary *response) {
             self.likeButton.selected ^= YES;
             like.status = self.likeButton.selected;
             self.likesCountLabel.text = [(NSNumber *)response[@"count"] stringValue];
         } failure:nil];
    }
}

- (void)voteUpToggle {
    TMEPostAction *voteup = [self.comment action:@"voteup"];
    TMETrackEvent(@"full_post",
                  self.postCommentLevel == TMEPostCommentLevelComment? @"flag_comment": @"flag_reply",
                  @"voteup");
    if (voteup.access) {
        [[TMEClient sharedClient]
         request:TeamieUserThoughtCommentVoteRequest
         parameters:@{@"cid": self.comment.cid, @"action": @"up"}
         loadingMessage:TMELocalize(voteup.status? @"message.unvote": @"message.vote")
         success:^(NSDictionary *response) {
             self.voteUpButton.selected ^= YES;
             voteup.status = self.voteUpButton.selected;
             
             self.voteDownButton.selected = !self.voteUpButton.selected;
             [self.comment action:@"votedown"].status = self.voteDownButton.selected;
             
             self.votesCountLabel.text = [(NSNumber *)response[@"count"] stringValue];
         } failure:nil];
    }
}

- (void)voteDownToggle {
    TMETrackEvent(@"full_post",
                  self.postCommentLevel == TMEPostCommentLevelComment? @"flag_comment": @"flag_reply",
                  @"votedown");
    TMEPostAction *votedown = [self.comment action:@"votedown"];
    if (votedown.access) {
        [[TMEClient sharedClient]
         request:TeamieUserThoughtCommentVoteRequest
         parameters:@{@"cid": self.comment.cid, @"action": @"down"}
         loadingMessage:TMELocalize(votedown.status? @"message.unvote": @"message.vote")
         success:^(NSDictionary *response) {
             self.voteDownButton.selected ^= YES;
             votedown.status = self.voteDownButton.selected;

             self.voteUpButton.selected = !self.voteDownButton.selected;
             [self.comment action:@"voteup"].status = self.voteUpButton.selected;
             
             self.votesCountLabel.text = [(NSNumber *)response[@"count"] stringValue];
         } failure:nil];
    }
}

- (void)markRightToggle {
    TMETrackEvent(@"full_post",
                  self.postCommentLevel == TMEPostCommentLevelComment? @"flag_comment": @"flag_reply",
                  @"markright");
    TMEPostAction *markRight = [self.comment action:@"mark-right"];
    if (markRight.access) {
        [[TMEClient sharedClient]
         request:TeamieUserThoughtCommentMarkRightRequest
         parameters:@{@"cid": self.comment.cid}
         loadingMessage:TMELocalize(markRight.status? @"message.mark-right": @"message.unmark-right")
         success:^(NSDictionary *response) {
             self.markRightButton.selected ^= YES;
             markRight.status = self.markRightButton.selected;
         } failure:nil];
    }
}

- (void)openAuthorProfile {
    TMETrackEvent(@"full_post", @"tap_user_picture", self.postCommentLevel == TMEPostCommentLevelComment? @"comment": @"reply");
    [self.delegate.navigationController pushViewController:[[UserProfileViewController alloc]
                                                            initWithUserID:[self.comment.author.uid integerValue]]
                                                  animated:YES];
}

- (void)moreButtonTapped {
    [self.delegate showPostCommentMoreActionsSheet:self];
}

- (UIActionSheet *)moreActionsSheet {
    TMEPostAction *delete = self.comment.actions[@"delete-thought-comment"];
    TMEPostAction *report = self.comment.actions[@"report"];

    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:TMELocalize(@"message.more-actions")
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    if (delete.access) {
        actionSheet.destructiveButtonIndex = [actionSheet addButtonWithTitle:delete.title];
    }
    if (report.access) {
        kReportAction = [actionSheet addButtonWithTitle:report.title];
    }
    if (self.comment.repliesCount.unsignedIntegerValue > self.comment.replies.count) {
        kAllRepliesAction = [actionSheet addButtonWithTitle:TMELocalize(@"button.all-replies")];
    }
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:TMELocalize(@"button.cancel")];
    actionSheet.tag = kActionSheetTag;
    return actionSheet;
}

- (void)replyButtonClicked {
    TMETrackEvent(@"full_post", @"tap_reply", nil);
    [self.delegate replyButtonClicked:self.comment.cid];
}

- (void)replyCountButtonClicked {
    TMETrackEvent(@"full_post", @"tap_reply_count", nil);

    if (self.areRepliesShown) {
        [self deleteReplies];
    } else {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.delegate.view animated:YES];
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = TMELocalize(@"message.loading");
        [self loadReplies];
        [hud hide:YES];
    }

    [UIView animateWithDuration:0.2 animations:^{
        [self layoutIfNeeded];
        self.repliesView.alpha += self.areRepliesShown? 1 : -1;
    } completion:nil];
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (actionSheet.tag) {
        case kActionSheetTag:
            if (buttonIndex == actionSheet.cancelButtonIndex) {
                //Do nothing
            }
            else if (buttonIndex == actionSheet.destructiveButtonIndex) {
                TMETrackEvent(@"full_post",
                              self.postCommentLevel == TMEPostCommentLevelComment? @"flag_more_comment": @"flag_more_reply",
                              @"delete");

                NSString *messageKey = @"message.deleting-comment";
                if ([self isKindOfClass:TMEPostCommentReplyView.class]) {
                    messageKey = @"message.deleting-reply";
                }

                [[TMEClient sharedClient]
                 request:DeletePostCommentRequest
                 parameters:nil
                 makeURL:^NSString *(NSString *URL) {
                     return [NSString stringWithFormat:URL, self.comment.cid];
                 }
                 loadingMessage:TMELocalize(messageKey)
                 success:^(id response) {
                     [self delete];
                 } failure:nil];
            }
            else if (buttonIndex == kReportAction) {
                TMETrackEvent(@"full_post",
                              self.postCommentLevel == TMEPostCommentLevelComment? @"flag_more_comment": @"flag_more_reply",
                              @"report");

                NSString *messageKey = @"message.report-comment";
                if ([self isKindOfClass:TMEPostCommentReplyView.class]) {
                    messageKey = @"message.report-reply";
                }

                [[TMEClient sharedClient]
                 request:TeamieUserReportPostComment
                 parameters:@{@"cid":self.comment.cid}
                 loadingMessage:TMELocalize(messageKey)
                 success:^(id response) {
                     [TeamieGlobals addHUDTextOnlyLabel:@"Reported!" afterDelay:1];
                 } failure:nil];
            }
            else if (buttonIndex == kAllRepliesAction) {
                TMETrackEvent(@"full_post", @"load_old", @"reply");
                [[TMEClient sharedClient]
                 request:TeamieUserGetAllThoughtCommentRepliesRequest
                 parameters:@{@"items_per_page":@(200)}
                 makeURL:^NSString *(NSString *URL) {
                     return [NSString stringWithFormat:URL, self.comment.cid];
                 }
                 loadingMessage:TMELocalize(@"message.loading")
                 success:^(NSDictionary *response) {
                     self.comment.replies = [TMEPostCommentReply parseList:response];
                     for (TMEPostCommentReply *reply in self.comment.replies) {
                         reply.parentComment = self.comment;
                         reply.type = self.comment.type;
                     }
                     [self loadReplies];
                 } failure:nil];
            }
    }
}

- (void)delete {
    [self.delegate deleteComment:self.comment];
}

#pragma - TMEPostAttachmentViewDelegate

- (void)trackAttachmentClick:(NSString *)attachmentType {
    TMETrackEvent(@"full_post",
                  self.postCommentLevel == TMEPostCommentLevelComment? @"tap_comment_attachment": @"tap_reply_attachment",
                  attachmentType);
}

@end
