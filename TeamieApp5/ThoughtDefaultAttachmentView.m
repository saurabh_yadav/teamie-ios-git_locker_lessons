//
//  ThoughtDefaultAttachmentView.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 16/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "ThoughtDefaultAttachmentView.h"
#import "TMELinkAttachment.h"
#import "TMEFileAttachment.h"
#import "TMEImageAttachment.h"
#import "TMEVideoAttachment.h"
#import "TMEAudioAttachment.h"

#import <SDWebImage/UIImageView+WebCache.h>

static const NSString* kNibName = @"ThoughtDefaultAttachmentView";

static const UIFont* bodyFont;

@interface ThoughtDefaultAttachmentView()

@property (nonatomic, strong) UIColor* textColor;

@end

@implementation ThoughtDefaultAttachmentView

+ (void)initialize{
    bodyFont = [TeamieGlobals appFontFor:@"thoughtBody"];
}

- (id)initWithAttachment:(TMEPostAttachment *)attachment{
    
    return [self initWithAttachment:attachment
                      withTextColor:[TeamieUIGlobalColors defaultTextColor]];
}

- (id)initWithAttachment:(TMEPostAttachment *)attachment
           withTextColor:(UIColor *)textColor{
    self = [[[NSBundle mainBundle] loadNibNamed:[kNibName copy] owner:nil options:nil] lastObject];
    
    if (self){
        self.textColor = textColor;
        [self setupLayout];
        [self loadAttachment:attachment];
    }
    
    return self;
}

- (void)setupLayout{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.attachmentTitle.textColor = self.textColor;
    self.attachmentTitle.font = [UIFont fontWithName:bodyFont.fontName size:bodyFont.pointSize * 0.9];
    self.attachmentMeta.textColor = self.textColor;
    self.attachmentMeta.font = [UIFont fontWithName:bodyFont.fontName size:bodyFont.pointSize * 0.85];
}

- (void)loadAttachment:(TMEPostAttachment *)attachment{
    if ([attachment isKindOfClass:TMEImageAttachment.class]) {
        [self.attachmentIcon sd_setImageWithURL:((TMEImageAttachment *)attachment).href
                               placeholderImage:[TeamieUIGlobals defaultPicForPurpose:@"image"]];
    }
    else {
        self.attachmentIcon.image = [self iconImageForAttachment:attachment];
    }
    
    
    self.attachmentMeta.text = [self metaForAttachment:attachment];
}

- (UIImage*)iconImageForAttachment:(TMEPostAttachment *)attachment{
    if ([attachment isKindOfClass:TMELinkAttachment.class]) {
        return [TeamieUIGlobals defaultPicForPurpose:@"link"];
    }
    else if ([attachment isKindOfClass:TMEFileAttachment.class]){
        return [TeamieUIGlobals defaultPicForPurpose:@"file"];
    }
    else if ([attachment isKindOfClass:TMEVideoAttachment.class]){
        return [TeamieUIGlobals defaultPicForPurpose:@"video"];
    }
    else if ([attachment isKindOfClass:TMEAudioAttachment.class]){
        return [TeamieUIGlobals defaultPicForPurpose:@"audio"];
    }
    
    return nil;
}

- (NSString*)metaForAttachment:(TMEPostAttachment *)attachment{
    if ([attachment isKindOfClass:TMELinkAttachment.class]) {
        return attachment.href.absoluteString;
    }
    else if ([attachment isKindOfClass:TMEFileAttachment.class]){
        return @"Document";
    }
    else if ([attachment isKindOfClass:TMEVideoAttachment.class]){
        return @"Video";
    }
    else if ([attachment isKindOfClass:TMEImageAttachment.class]){
        return @"Image";
    }
    else if ([attachment isKindOfClass:TMEAudioAttachment.class]){
        return @"Audio";
    }
    
    return nil;
}

@end
