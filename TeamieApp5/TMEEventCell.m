
//
//  TMEEventCell.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEEventCell.h"

#import "UIView+TMEEssentials.h"
#import <Masonry/Masonry.h>
#import "UILabelWithPadding.h"

@interface TMEEventCell()

#pragma mark - Subviews

@property (nonatomic, weak) UIView *leftDateView;
@property (nonatomic, weak) UILabel *leftDateLabel;
@property (nonatomic, weak) UILabel *leftTimeLabel;

@property (nonatomic, weak) UIView *infoView;
@property (nonatomic, weak) UILabel *titleLabel;
@property (nonatomic, weak) UILabelWithPadding *typeLabel;
@property (nonatomic, weak) UILabel *calendarLabel;

@property (nonatomic, weak) UIView *rightDateView;
@property (nonatomic, weak) UILabel *rightDateLabel;
@property (nonatomic, weak) UILabel *rightTimeLabel;

#pragma mark - Constraints

@property (nonatomic, weak) MASConstraint *rightViewWidthConstraint;
@property (nonatomic, weak) MASConstraint *labelWidthConstraint;
@property (nonatomic, weak) MASConstraint *labelLeftConstraint;

#pragma mark - Local Properties

@property (nonatomic) BOOL showLabel;

@end

@implementation TMEEventCell

static const CGFloat kDefaultPadding = 10.0f;
static const CGFloat kCellHeight = 70.0f;
static NSString * const kDateFormat = @"MMM d";
static NSString * const kTimeFormat = @"ha";
static UIEdgeInsets kLabelInsets;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        kLabelInsets = UIEdgeInsetsMake(1.0f, 3.0f, 1.0f, 3.0f);
        
        [self addBorderToTop:YES bottom:NO left:NO right:NO];
        [self addLeftView];
        [self addRightView];
        [self addMainView];
        [self addConstraints];
    }
    return self;
}

- (void)addLeftView {
    self.leftDateView = [self.contentView createAndAddSubView:UIView.class];
    self.leftDateView.backgroundColor = [TeamieUIGlobalColors headerViewBackgroundColor];
    
    self.leftDateLabel = [self.leftDateView createAndAddSubView:UILabel.class];
    self.leftDateLabel.font = [TeamieGlobals appFontFor:@"eventDate"];
    
    self.leftTimeLabel = [self.leftDateView createAndAddSubView:UILabel.class];
    self.leftTimeLabel.font = [TeamieGlobals appFontFor:@"eventTime"];
    self.leftTimeLabel.textColor = [TeamieUIGlobalColors thoughtCommentTextColor];
}

- (void)addRightView {
    self.rightDateView = [self.contentView createAndAddSubView:UIView.class];
    self.rightDateView.backgroundColor = [TeamieUIGlobalColors headerViewBackgroundColor];
    
    self.rightDateLabel = [self.rightDateView createAndAddSubView:UILabel.class];
    self.rightDateLabel.font = [TeamieGlobals appFontFor:@"eventDate"];
    
    self.rightTimeLabel = [self.rightDateView createAndAddSubView:UILabel.class];
    self.rightTimeLabel.font = [TeamieGlobals appFontFor:@"eventTime"];
    self.rightTimeLabel.textColor = [TeamieUIGlobalColors thoughtCommentTextColor];
}

- (void)addMainView {
    self.infoView = [self.contentView createAndAddSubView:UIView.class];

    self.titleLabel = [self.infoView createAndAddSubView:UILabel.class];
    self.titleLabel.numberOfLines = 1;
    self.titleLabel.font = [TeamieGlobals appFontFor:@"eventTitle"];

    self.typeLabel = [self.infoView createAndAddSubView:UILabelWithPadding.class];
    self.typeLabel.font = [TeamieGlobals appFontFor:@"eventTypeLabel"];
    self.typeLabel.textColor = [UIColor whiteColor];
    self.typeLabel.layer.masksToBounds = YES;
    self.typeLabel.layer.cornerRadius = 2.0f;
    self.typeLabel.backgroundColor = [UIColor colorWithHex:@"#5F6F8C" alpha:1.0f];
    self.typeLabel.edgeInsets = UIEdgeInsetsMake(0, 3, 0, 3);
    
    self.calendarLabel = [self.infoView createAndAddSubView:UILabel.class];
    self.calendarLabel.font = [TeamieGlobals appFontFor:@"eventDetails"];
    self.calendarLabel.textColor = [TeamieUIGlobalColors defaultTextColor];
    self.calendarLabel.numberOfLines = 1;
}

- (void)addConstraints {
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
    [self.leftDateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.bottom.equalTo(self.contentView);
        make.width.height.equalTo(@(kCellHeight));
    }];
    [self.leftDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.leftDateView);
        make.top.equalTo(self.leftDateView).with.offset(12.0f);
    }];
    [self.leftTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.leftDateView);
        make.top.equalTo(self.leftDateLabel.mas_bottom).with.offset(6.0f);
    }];
    
    [self.rightDateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.top.bottom.equalTo(self.contentView);
        make.height.equalTo(@(kCellHeight));
        self.rightViewWidthConstraint = make.width.equalTo(@(kCellHeight));
    }];
    [self.rightDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.rightDateView);
        make.top.equalTo(self.rightDateView).with.offset(12.0f);
    }];
    [self.rightTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.rightDateView);
        make.top.equalTo(self.rightDateLabel.mas_bottom).with.offset(6.0f);
    }];
    
    [self.infoView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self.contentView);
        make.left.equalTo(self.leftDateView.mas_right);
        make.right.equalTo(self.rightDateView.mas_left);
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.infoView).with.offset(kDefaultPadding);
        make.right.equalTo(self.infoView).with.offset(-kDefaultPadding);
    }];
    [self.typeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).with.offset(kDefaultPadding);
        self.labelLeftConstraint = make.left.equalTo(self.infoView).with.offset(kDefaultPadding);
        self.labelWidthConstraint = make.width.equalTo(@(CGRectGetWidth(self.typeLabel.frame)));
        [self.typeLabel setContentHuggingPriority:UILayoutPriorityRequired
                                          forAxis:UILayoutConstraintAxisHorizontal];
        [self.typeLabel setContentCompressionResistancePriority:UILayoutPriorityRequired
                                                        forAxis:UILayoutConstraintAxisHorizontal];
    }];
    [self.calendarLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).with.offset(kDefaultPadding);
        make.left.equalTo(self.typeLabel.mas_right).with.offset(kDefaultPadding);
        make.right.equalTo(self.infoView).with.offset(-kDefaultPadding);
        [self.calendarLabel setContentHuggingPriority:UILayoutPriorityDefaultHigh
                                              forAxis:UILayoutConstraintAxisHorizontal];
        [self.calendarLabel setContentCompressionResistancePriority:UILayoutPriorityDefaultHigh
                                                            forAxis:UILayoutConstraintAxisHorizontal];
    }];
}

- (void)loadEvent:(TMEEvent *)event {
    self.titleLabel.text = event.title;
    self.calendarLabel.text = event.calendarName;
    NSString *key = [NSString stringWithFormat:@"event.%@", event.eventTypeName];
    NSString *label = TMELocalize(key);
    self.typeLabel.text = label;
    self.labelLeftConstraint.offset(event.type == TMEEventDefault? 0.0f : kDefaultPadding);
    self.labelWidthConstraint.offset(event.type == TMEEventDefault?
                                     0.0f :
                                     [label sizeWithFont:[TeamieGlobals appFontFor:@"eventTypeLabel"]].width
                                        + self.typeLabel.edgeInsets.left
                                        + self.typeLabel.edgeInsets.right);
    
    //Formatters are expensive, since this is in a loop making it static increases performance by a lot
    static NSDateFormatter *dateFormatter = nil;
    static NSDateFormatter *timeFormatter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateFormatter = [NSDateFormatter new];
        dateFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:kDateFormat options:0 locale:[NSLocale currentLocale]];

        timeFormatter = [NSDateFormatter new];
        timeFormatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:kTimeFormat options:0 locale:[NSLocale currentLocale]];
    });
    
    self.leftDateLabel.text = [dateFormatter stringFromDate:event.from];
    self.rightDateLabel.text = [dateFormatter stringFromDate:event.to];
    
    if (event.allDay) {
        self.leftTimeLabel.text = @"All day";
        self.rightTimeLabel.text = @"All day";
    }
    else {
        self.leftTimeLabel.text = [timeFormatter stringFromDate:event.from];
        self.rightTimeLabel.text = [timeFormatter stringFromDate:event.to];
    }

    //Only one side because same day
    if ([self.leftDateLabel.text isEqualToString:self.rightDateLabel.text]) {
        self.rightViewWidthConstraint.offset(0.0f);
        self.rightDateView.hidden = YES;
        
        if (!event.allDay && ![self.leftTimeLabel.text isEqualToString:self.rightTimeLabel.text]) {
            self.leftTimeLabel.text = [NSString stringWithFormat:@"%@-%@", self.leftTimeLabel.text, self.rightTimeLabel.text];
        }
    }
    //Both sides
    else {
        self.rightViewWidthConstraint.offset(kCellHeight);
        self.rightDateView.hidden = NO;
    }
}

@end
