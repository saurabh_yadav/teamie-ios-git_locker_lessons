//
//  TMEEventCell.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 20/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "TMEEvent.h"

@interface TMEEventCell : UITableViewCell

- (void)loadEvent:(TMEEvent *)event;

@end
