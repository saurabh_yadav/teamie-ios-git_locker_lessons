//
//  TMEPostCommentReply.h
//  TeamieApp5
//
//  Created by Thao Nguyen on 19/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMEModel.h"
#import "TMEUser.h"
#import "TMEPostAction.h"
#import "TMEPostComment.h"

//The reply entity is essentially the same as the comment
@interface TMEPostCommentReply : TMEPostComment

@property (nonatomic, weak) TMEPostComment *parentComment;

@end
