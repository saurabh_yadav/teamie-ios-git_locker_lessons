//
//  TMEUser.m
//  Classmates
//
//  Created by Anuj Rajput on 23/08/12.
//  Copyright (c) 2012 Anuj Rajput. All rights reserved.
//

#import "TMEUser.h"
#import "TMEClient.h"
#import "TMEClassroomAlt.h"

@implementation TMEUser

static NSString *kCurrentUserKey = @"currentAuthenticatedUserInfo";

#pragma mark - Mantle

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
        @"username": @"name",
        @"email": @"mail",
        @"realName": @"real_name",
        @"userPoints": @"teamie-points",
    };
}

+ (NSValueTransformer *)uidJSONTransformer {
    return [TMEModel numberJSONTransformer];
}

+ (NSValueTransformer *)user_profile_imageJSONTransformer {
    return [NSValueTransformer mtl_JSONDictionaryTransformerWithModelClass:TMEImage.class];
}

+ (NSValueTransformer *)classroomsJSONTransformer {
    return [TMEModel listJSONTransformer:TMEClassroomAlt.class];
}

#pragma mark - API

+ (TMEUser *)currentUser {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:kCurrentUserKey];
    return [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
}

+ (void)setCurrentUser:(TMEUser *)user {
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:user];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:kCurrentUserKey];
    [defaults synchronize];
}

- (UIImage *)picture {
    return [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:self.user_profile_image.path]];}

@end
