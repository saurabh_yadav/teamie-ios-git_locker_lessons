//
//  LP3ViewController.m
//  LS2
//
//  Created by Wei Wenbo on 11/6/14.
//  Copyright (c) 2014 Wei Wenbo. All rights reserved.
//

#import "LP3ViewController.h"

@interface LP3ViewController ()
@end

@implementation LP3ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSString *ss=@"<meta name='viewport' content='initial-scale=1.0,maximum-scale=10.0'/><h3 style=\"color:Blue;\"><tt>Welcome to Teamie! Teamie is a Social Learning platform that creates a private and structured learning network for a school. </tt></h3> <p>As you would notice from the top navigation bar on the page,s you might be part of one or many classrooms. Each classroom is essentially a social workspace where you can share thoughts and interesting content (files, links, images, videos), and ask questions (including polls). Teamie acts as a place where you can continue to speak with classmates after class or school, help answer each others' questions and clear your doubts &amp; discuss ideas. </p> <p>Some of you might have heard of Facebook (okay, many of you might have heard of Facebook). Teamie is not Facebook. It is similar in a way that it lets you connect with your classmates and share interesting stuff in a newsfeed (which we call the <em>\"Whiteboard\"</em>, but different as it is a private network where you can't \"Add a Friend\". Other big differences between Facebook and Teamie is that Teamie lets you access your Class Lessons and Assignments, Teamie allows you to make your assignment submission and check grades online, on Teamie your teacher can give feedback on your discussions and mark certain comments as \"Right\" or \"Good Answers\", and Teamie allows you to filter conversations in the whiteboard and attach context so that you can get to the right conversations quickly. </p> <p><img alt=\"\" src=\"\" /></p> <p>A good way to start using Teamie would be to share something interesting with your class. Here are some tips on how you could participate in Socia Learning on Teamie:</p> <ol><li>Share what you learnt about the topic that was discussed in class yesterday</li> <li>Ask what are some of the questions that you couldn't get an answer to during class, or they just occured to you as you were reading about it</li> <li>Share that interesting youtube video or picture about the topic that was covered during class today</li> <li>Start a poll to find out what your classmates know about the topic</li> <li>Help to answer your classmates question</li> <li>Like or Vote up answers that you find are helpful</li> </ol><p> </p> <blockquote><p><em>As you are sharing with your classmates, remember that you can:</em></p> <ul><li><em>Tag users by using @ symbol followed by their first name, which would automatically populate a list of matching names</em></li> <li><em>Use #Hashtags to suggest what topic this post belongs to. You can then click on the #hashtag to check all discussions tagged with that hashtag</em></li> <li><em>Add Context to that post. Context is like a tag that is auto-populated by the platform by looking for lessons, assignments and other class-related activities  </em></li> </ul></blockquote> <p>     </p><br></br>";
    
    [self.webview loadHTMLString:ss baseURL:nil];
    self.webview.delegate = self;
    self.webview.scrollView.scrollEnabled = NO;
    
    
    //configure zooming
    self.scrollView.delegate = self;
    self.scrollView.minimumZoomScale = 0.5;
    self.scrollView.maximumZoomScale = 5.0;
    // Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma -mark UIScrollViewDelegate
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return self.containerView;
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale{
}
#pragma -mark UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    CGRect frame = self.webview.frame;
    frame.size = self.webview.scrollView.contentSize;
    self.webview.frame = frame;
    
    //set the outter container's frame
    CGRect cframe = self.containerView.frame;
    cframe.size = self.webview.scrollView.contentSize;
    
    UILabel *myLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, cframe.size.height, 320, 90)];
    myLabel.text = @"My Test: Hello World";
    cframe.size.height += 500;
    self.containerView.frame = cframe;
    [self.containerView addSubview: myLabel];
    //set the outter scrollview size
    self.scrollView.contentSize = self.containerView.frame.size;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
