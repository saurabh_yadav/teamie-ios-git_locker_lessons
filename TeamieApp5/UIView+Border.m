//
//  UIView+UIView_Border.m
//  TeamieApp5
//
//  Created by Thao Nguyen on 16/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UIView+Border.h"

@implementation UIView (Border)

- (void)drawTopBorderWithColor:(CGColorRef)borderColor borderWidth:(CGFloat)borderWidth{
    [self drawBorderAtTop:YES right:NO bottom:NO left:NO
                withColor:borderColor
               borderWith:borderWidth
             cornerRadius:0.0];
}

- (void)drawRightBorderWithColor:(CGColorRef)borderColor borderWidth:(CGFloat)borderWidth{
    [self drawBorderAtTop:NO right:YES bottom:NO left:NO
                withColor:borderColor
               borderWith:borderWidth
             cornerRadius:0.0];
}

- (void)drawBottomBorderWithColor:(CGColorRef)borderColor borderWidth:(CGFloat)borderWidth{
    [self drawBorderAtTop:NO right:NO bottom:YES left:NO
                withColor:borderColor
               borderWith:borderWidth
             cornerRadius:0.0];
}

- (void)drawLeftBorderWithColor:(CGColorRef)borderColor borderWidth:(CGFloat)borderWidth{
    [self drawBorderAtTop:NO right:NO bottom:NO left:YES
                withColor:borderColor
               borderWith:borderWidth
             cornerRadius:0.0];
}

- (void)drawBorderAtTop:(BOOL)drawTop right:(BOOL)drawRight bottom:(BOOL)drawBottom left:(BOOL)drawLeft withColor:(CGColorRef)borderColor borderWith:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius{
    [self drawBorderAtTop:drawTop
                    right:drawRight
                   bottom:drawBottom
                     left:drawLeft
                withColor:borderColor
               borderWith:borderWidth
             cornerRadius:cornerRadius
                lineStyle:kSolidLine];
}

- (void)drawBorderAtTop:(BOOL)drawTop right:(BOOL)drawRight bottom:(BOOL)drawBottom left:(BOOL)drawLeft withColor:(CGColorRef)borderColor borderWith:(CGFloat)borderWidth cornerRadius:(CGFloat)cornerRadius lineStyle:(LineStyle)lineStyle{
    
    self.clipsToBounds = YES;
    
    CGFloat x = 0.0;
    CGFloat y = 0.0;
    CGFloat width = CGRectGetWidth(self.frame);
    CGFloat height = CGRectGetHeight(self.frame);
    CGFloat padding = MAX(borderWidth, cornerRadius);
    
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.name = @"borderLayer";
    
    if (!drawTop){
        y -= padding;
    }
    if (!drawLeft){
        x -= padding;
    }
    
    if (!drawTop && !drawBottom){
        height += padding * 2;
    }
    else if ((drawTop && !drawBottom) || (!drawTop && drawBottom)){
        height += padding;
    }
    
    if (!drawLeft && !drawRight){
        width += padding * 2;
    }
    else if ((drawLeft && !drawRight) || (!drawLeft && drawRight)){
        width += padding;
    }
    
    layer.frame = CGRectMake(x, y, width, height);
    layer.fillColor = [[UIColor clearColor] CGColor];

    if (lineStyle == kDashedLine){
        layer.strokeColor = borderColor;
        layer.lineWidth = borderWidth;
        layer.lineDashPattern = @[@4, @2];
        layer.path = [[UIBezierPath bezierPathWithRoundedRect:layer.bounds cornerRadius:cornerRadius] CGPath];
    }
    else{
        layer.borderColor = borderColor;
        layer.borderWidth = borderWidth;
        layer.cornerRadius = cornerRadius;
    }
    
    [self.layer addSublayer:layer];
}

- (void)drawInnerShadowWithColor:(CGColorRef)color radius:(CGFloat)radius{
    // Modified the code from this library: https://github.com/inamiy/YIInnerShadowView
    
    self.clipsToBounds = YES;
    
    CAShapeLayer* shadowLayer = [CAShapeLayer layer];
    shadowLayer.name = @"borderLayer";
    
    [shadowLayer setFrame:[self bounds]];
    
    // Standard shadow stuff
    [shadowLayer setShadowColor:color];
    [shadowLayer setShadowOffset:CGSizeMake(0.0f, 0.0f)];
    [shadowLayer setShadowOpacity:1.0f];
    [shadowLayer setShadowRadius:radius];
    
    // Causes the inner region in this example to NOT be filled.
    [shadowLayer setFillRule:kCAFillRuleEvenOdd];
    
    CGRect largerRect = CGRectMake(self.bounds.origin.x - radius,
                                   self.bounds.origin.y - radius,
                                   self.bounds.size.width + 2 * radius,
                                   self.bounds.size.height + 2 * radius);
    
    // Create the larger rectangle path.
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, largerRect);
    
    // Add the inner path so it's subtracted from the outer path.
    CGFloat cornerRadius = self.layer.cornerRadius;
    UIBezierPath *bezier;
    if (cornerRadius) {
        bezier = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:cornerRadius];
    } else {
        bezier = [UIBezierPath bezierPathWithRect:self.bounds];
    }
    CGPathAddPath(path, NULL, bezier.CGPath);
    CGPathCloseSubpath(path);
    
    [shadowLayer setPath:path];
    
    CGPathRelease(path);
    
    [self.layer addSublayer:shadowLayer];
}

- (void)clearBorderLayers{
    NSMutableArray* layerArray = [[NSMutableArray alloc] init];
    for (CALayer* layer in self.layer.sublayers){
        if ([layer.name isEqualToString:@"borderLayer"]){
            [layerArray addObject:layer];
        }
    }
    [layerArray makeObjectsPerformSelector:@selector(removeFromSuperlayer)];
}

@end
