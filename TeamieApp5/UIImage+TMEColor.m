//
//  UIImage+TMEColor.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 25/11/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UIImage+TMEColor.h"

@implementation UIImage (TMEColor)

+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0, 0, 1, 1);
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0);
    [color setFill];
    UIRectFill(rect);   // Fill it with your color
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
