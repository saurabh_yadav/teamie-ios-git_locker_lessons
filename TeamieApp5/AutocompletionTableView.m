//
//  AutocompletionTableView.m
//
//  Created by Gushin Arseniy on 11.03.12.
//  Copyright (c) 2012 Arseniy Gushin. All rights reserved.
//

#import "AutocompletionTableView.h"

@interface AutocompletionTableView () {
    UITableView* _tableView;
}
@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) NSArray *suggestionOptions; // of selected NSStrings 
@property (nonatomic, strong) UITextField *textField; // will set automatically as user enters text
@property (nonatomic, strong) UIFont *cellLabelFont; // will copy style from assigned textfield
@end

@implementation AutocompletionTableView

@synthesize tableView = _tableView;
@synthesize suggestionsDictionary = _suggestionsDictionary;
@synthesize suggestionOptions = _suggestionOptions;
@synthesize textField = _textField;
@synthesize cellLabelFont = _cellLabelFont;
@synthesize options = _options;

#pragma mark - Initialization
- (UIView *)initWithTextField:(UITextField *)textField inViewController:(UIViewController *) parentViewController withOptions:(NSDictionary *)options
{
    self = [super initWithFrame:parentViewController.view.bounds];
    if (self) {
        //set the options first
        self.options = options;
        self.backgroundColor = [UIColor colorWithHex:@"#222222" alpha:0.9];
        self.hidden = YES;
       
        // frame must align to the textfield
        CGRect frame = CGRectMake(textField.frame.origin.x, textField.frame.origin.y, textField.frame.size.width, 0);
        
        // save the font info to reuse in cells
        self.cellLabelFont = textField.font;
        
        self.tableView = [[UITableView alloc] initWithFrame:frame
                              style:UITableViewStylePlain];
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        self.tableView.backgroundColor = [UIColor whiteColor];
        self.tableView.layer.cornerRadius = UI_NORMAL_BORDER_RADIUS;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        self.tableView.separatorColor = [TeamieUIGlobalColors tableViewCellBorderColor];
        
        // turn off standard correction
        textField.autocorrectionType = UITextAutocorrectionTypeNo;
        
        // to get rid of "extra empty cell" on the bottom
        // when there's only one cell in the table
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, textField.frame.size.width, 1)];
        v.backgroundColor = [UIColor clearColor];
        [self.tableView setTableFooterView:v];
        [self addSubview:self.tableView];
        
        [parentViewController.view insertSubview:self belowSubview:[parentViewController.view viewWithTag:1]];
    }
    return self;
}

#pragma mark - Logic staff
- (BOOL) substringIsInDictionary:(NSString *)subString
{
    NSMutableArray *tmpArray = [NSMutableArray array];
    BOOL shouldAdd;
    NSString* searchString;
    
    if ([subString length] < 1) {
        return NO;
    }
    
    if (_autoCompleteDelegate && [_autoCompleteDelegate respondsToSelector:@selector(autoCompletion:suggestionsFor:)]) {
        self.suggestionsDictionary = [_autoCompleteDelegate autoCompletion:self suggestionsFor:subString];
    }
    
    for (NSString *tmpString in self.suggestionsDictionary)
    {
        shouldAdd = NO;
        if ([tmpString length] < [subString length]) {
            continue;
        }
        searchString = [tmpString substringToIndex:[subString length]];
        if  ([[self.options valueForKey:ACOCaseSensitive] isEqualToNumber:[NSNumber numberWithInt:1]]) {
            if ([searchString compare:subString] == NSOrderedSame) {
                shouldAdd = YES;
            }
        }
        else {
            if ([searchString caseInsensitiveCompare:subString] == NSOrderedSame) {
                shouldAdd = YES;
            }
        }
        if (shouldAdd) [tmpArray addObject:tmpString];
    }
    if ([tmpArray count]>0)
    {
        self.suggestionOptions = tmpArray;
        return YES;
    }
    return NO;
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger count = ([self.suggestionOptions count] > 4) ? 4 : [self.suggestionOptions count];
    CGRect frame = CGRectMake(self.textField.frame.origin.x, self.textField.frame.origin.y - (count * 38.0) - 5, self.textField.frame.size.width, (count * 38.0));
    self.tableView.frame = frame;
    return count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 38.0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *AutoCompleteRowIdentifier = @"AutoCompleteRowIdentifier";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:AutoCompleteRowIdentifier];
    if (cell == nil) 
    {
        cell = [[UITableViewCell alloc] 
                 initWithStyle:UITableViewCellStyleDefault reuseIdentifier:AutoCompleteRowIdentifier];
    }
    
    if ([self.options valueForKey:ACOUseSourceFont]) 
    {
        cell.textLabel.font = [self.options valueForKey:ACOUseSourceFont];
    } else 
    {
        cell.textLabel.font = self.cellLabelFont;
    }

    cell.textLabel.adjustsFontSizeToFitWidth = NO;
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.text = [self.suggestionOptions objectAtIndex:indexPath.row];

    return cell;
}

#pragma mark - Table view delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.textField setText:[self.suggestionOptions objectAtIndex:indexPath.row]];
    
    if (_autoCompleteDelegate && [_autoCompleteDelegate respondsToSelector:@selector(autoCompletion:didSelectAutoCompleteSuggestionWithIndex:)]) {
        [_autoCompleteDelegate autoCompletion:self didSelectAutoCompleteSuggestionWithIndex:indexPath.row];
    }
    
    [self hideOptionsView];
}

#pragma mark - UITextField delegate
- (void)textFieldValueChanged:(UITextField *)textField
{
    self.textField = textField;
    NSString *curString = textField.text;
    
    if (![curString length])
    {
        [self hideOptionsView];
        return;
    }
    else if ([self substringIsInDictionary:curString])
    {
        [self showOptionsView];
        [self.tableView reloadData];
    }
    else
        [self hideOptionsView];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
//    BOOL shouldClear = YES;
//    NSString* substring = [[textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] lowercaseString];
//    for (NSString* option in self.suggestionsDictionary) {
//        if ([substring caseInsensitiveCompare:option] == NSOrderedSame) {
//            shouldClear = NO;
//            substring = option;
//            break;
//        }
//    }
//    if (shouldClear) {
//        textField.text = @"";
//    }
//    else {
//        textField.text = substring;
//    }
    [self hideOptionsView];
}


#pragma mark - Options view control
- (void)showOptionsView
{
    self.hidden = NO;
}

- (void) hideOptionsView
{
    self.hidden = YES;
}

@end
