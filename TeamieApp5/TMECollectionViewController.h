//
//  TMECollectionViewController.h
//  TeamieApp5
//
//  Created by Anuj Rajput on 02/12/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TMECollectionViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UIView      *emptyView;


/**
 *  This method should be called on all datasource based table views, if the number of items in datasource is zero then this method would add a default empty text view
 *
 *  @param title    Title of the list
 *  @param subtitle Subtitle message, this should probably be some description or clever text
 *  @param image    Image to be displayed if view is empty
 *
 *  @return View which overlays the Table View in case of Empty Data source
 */
- (void)viewForEmptyListWithTitleText:(NSString *)title subtitleText:(NSString *)subtitle andImage:(UIImage *)image;

@end
