//
//  TMEVideoAttachment.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 26/10/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEPostAttachment.h"

@interface TMEVideoAttachment : TMEPostAttachment

@property (nonatomic, strong) NSNumber *fid;
@property (nonatomic, strong) NSString *filemime;
@property (nonatomic, strong) NSString *icon;
@property (nonatomic, strong) NSURL *href_mp4;
@end
