//
//  TeamieUIGlobals.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 6/27/13.
//  Copyright (c) 2013 Teamie. All rights reserved.
//

#import "TeamieUIGlobals.h"

#import <FontAwesomeIconFactory/NIKFontAwesomeIconFactory.h>
#import <FontAwesomeIconFactory/NIKFontAwesomeIconFactory+iOS.h>

#import "MBProgressHUD.h"

NSString * const COACH_MARKS_SHOWN_PREFIX = @"coachMarksShown-";
NSString * const COACH_MARKS_SETTINGS_BUNDLE_STRING = @"coachMarksEnabled";

@implementation TeamieUIGlobals

const CGFloat BORDER_THICKNESS = 1.0f;

+ (UIImage*)defaultPicForPurpose:(NSString *)purpose {
    return [TeamieUIGlobals defaultPicForPurpose:purpose withSize:50.0];
}

+ (UIImage*)defaultPicForPurpose:(NSString*)purpose withSize:(CGFloat)size {
    return [TeamieUIGlobals defaultPicForPurpose:purpose withSize:size andColor:[UIColor grayColor]];
}

+ (UIImage*)defaultPicForPurpose:(NSString*)purpose withSize:(CGFloat)size andColor:(UIColor*)color {
    NIKFontAwesomeIcon iconName;
    
    if ([purpose isEqualToString:@"profile"]) {
        iconName = NIKFontAwesomeIconUser;
    }
    else if ([purpose isEqualToString:@"users"]) {
        iconName = NIKFontAwesomeIconUserMd;
    }
    else if ([purpose isEqualToString:@"badge"]) {
        iconName = NIKFontAwesomeIconStar;
    }
    else if ([purpose isEqualToString:@"bulletin"]) {
        iconName = NIKFontAwesomeIconGlobe;
    }
    else if ([purpose isEqualToString:@"message"]) {
        iconName = NIKFontAwesomeIconEnvelope;
    }
    else if ([purpose isEqualToString:@"classroom"]) {
        iconName = NIKFontAwesomeIconThList;
    }
    else if ([purpose isEqualToString:@"menu"]) {
        iconName = NIKFontAwesomeIconAlignJustify;
    }
    else if ([purpose isEqualToString:@"back"]) {
        iconName = NIKFontAwesomeIconArrowLeft;
    }
    else if ([purpose isEqualToString:@"search"]) {
        iconName = NIKFontAwesomeIconSearch;
    }
    else if ([purpose isEqualToString:@"filter"]) {
        iconName = NIKFontAwesomeIconFilter;
    }
    else if ([purpose isEqualToString:@"ok"]) {
        iconName = NIKFontAwesomeIconCheckCircle;
    }
    else if ([purpose isEqualToString:@"remove"] || [purpose isEqualToString:@"error"]) {
        iconName = NIKFontAwesomeIconRemove;
    }
    else if ([purpose isEqualToString:@"frown"]) {
        iconName = NIKFontAwesomeIconFrownO;
    }
    else if ([purpose isEqualToString:@"invisible"]) {
        iconName = NIKFontAwesomeIconEyeSlash;
    }
    else if ([purpose isEqualToString:@"download"]) {
        iconName = NIKFontAwesomeIconDownload;
    }
    else if ([purpose isEqualToString:@"settings"]) {
        iconName = NIKFontAwesomeIconCog;
    }
    else if ([purpose isEqualToString:@"file"]) {
        iconName = NIKFontAwesomeIconFileTextO;
    }
    else if ([purpose isEqualToString:@"link"]){
        iconName = NIKFontAwesomeIconLink;
    }
    else if ([purpose isEqualToString:@"image"]){
        iconName = NIKFontAwesomeIconPictureO;
    }
    else if ([purpose isEqualToString:@"video"]){
        iconName = NIKFontAwesomeIconVideoCamera;
    }
    else if ([purpose isEqualToString:@"thought"]) {
        iconName = NIKFontAwesomeIconComment;
    }
    else if ([purpose isEqualToString:@"question"]) {
        iconName = NIKFontAwesomeIconHandOUp;
    }
    else if ([purpose isEqualToString:@"homework"]) {
        iconName = NIKFontAwesomeIconHome;
    }
    else if ([purpose isEqualToString:@"announcement"]) {
        iconName = NIKFontAwesomeIconBullhorn;
    }
    else if ([purpose isEqualToString:@"comment"]) {
        iconName = NIKFontAwesomeIconComment;
    }
    else if ([purpose isEqualToString:@"reply"]) {
        iconName = NIKFontAwesomeIconComments;
    }
    else if ([purpose isEqualToString:@"lesson"]) {
        iconName = NIKFontAwesomeIconBook;
    }
    else if ([purpose isEqualToString:@"quiz"]) {
        iconName = NIKFontAwesomeIconListAlt;
    }
    else if ([purpose isEqualToString:@"like"]) {
        iconName = NIKFontAwesomeIconSmileO;
    }
    else if ([purpose isEqualToString:@"report"]) {
        iconName = NIKFontAwesomeIconBarChart;
    }
    else if ([purpose isEqualToString:@"calendar"]) {
        iconName = NIKFontAwesomeIconCalendar;
    }
    else if ([purpose isEqualToString:@"logout"]) {
        iconName = NIKFontAwesomeIconSignOut;
    }
    else if ([purpose isEqualToString:@"resource"]) {
        iconName = NIKFontAwesomeIconBookmark;
    }
    else if ([purpose isEqualToString:@"voteup"]){
        iconName = NIKFontAwesomeIconArrowUp;
    }
    else if ([purpose isEqualToString:@"votedown"]){
        iconName = NIKFontAwesomeIconArrowDown;
    }
    else if ([purpose isEqualToString:@"markright"]){
        iconName = NIKFontAwesomeIconCheck;
    }
    else if ([purpose isEqualToString:@"ellipsis"]){
        iconName = NIKFontAwesomeIconEllipsisH;
    }
    else if ([purpose isEqualToString:@"angle-right"]){
        iconName = NIKFontAwesomeIconAngleRight;
    }
    else if ([purpose isEqualToString:@"list"]){
        iconName = NIKFontAwesomeIconReorder;
    }
    else if ([purpose isEqualToString:@"back2"]){
        iconName = NIKFontAwesomeIconChevronLeft;
    }
    else if ([purpose isEqualToString:@"goRight"]){
        iconName = NIKFontAwesomeIconArrowRight;
    }
    else if ([purpose isEqualToString:@"right-arrow"]){
        iconName = NIKFontAwesomeIconAngleDoubleRight;
    }
    else if ([purpose isEqualToString:@"post"]) {
        iconName = NIKFontAwesomeIconEdit;
    }
    else if ([purpose isEqualToString:@"close"]) {
        iconName = NIKFontAwesomeIconClose;
    }
    else if ([purpose isEqualToString:@"send"]) {
        iconName = NIKFontAwesomeIconSend;
    }
    else if ([purpose isEqualToString:@"attachment"]) {
        iconName = NIKFontAwesomeIconPaperclip;
    }
    else if ([purpose isEqualToString:@"lock"]) {
        iconName = NIKFontAwesomeIconLock;
    }
    else if ([purpose isEqualToString:@"unlock"]) {
        iconName = NIKFontAwesomeIconUnlock;
    }
    else if ([purpose isEqualToString:@"audio"]) {
        iconName = NIKFontAwesomeIconVolumeUp;
    }
    else if ([purpose isEqualToString:@"emptylist"]) {
        iconName = NIKFontAwesomeIconStackOverflow;
    }
    else if ([purpose isEqualToString:@"switch_user"]) {
        iconName = NIKFontAwesomeIconUsers;
    }
    else if ([purpose isEqualToString:@"checkmark"]) {
        iconName = NIKFontAwesomeIconCheck;
    }
    else if ([purpose isEqualToString:@"info"]) {
        iconName = NIKFontAwesomeIconInfoCircle;
    }
    else {
        iconName = NIKFontAwesomeIconPictureO;
    }
    
    NIKFontAwesomeIconFactory* factory = [[NIKFontAwesomeIconFactory alloc] init];
    factory.colors = @[color];
    factory.size = size;
    
    return [factory createImageForIcon:iconName];
}

// Display a temporary message on screen with a given icon (The icon is generated using FontAwesome)
// This method is useful for displaying success or error messages. And an alternative to TSMessage

+ (void)displayStatusMessage:(NSString*)msg forPurpose:(NSString*)purpose {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:[[[UIApplication sharedApplication] windows] objectAtIndex:0] animated:YES];
    hud.mode = MBProgressHUDModeCustomView;
    // Set the image for the message based on the 'purpose' string
    hud.customView = [[UIImageView alloc] initWithImage:[TeamieUIGlobals defaultPicForPurpose:purpose
                                                                                     withSize:30.0
                                                                                     andColor:[UIColor whiteColor]]];
    if ([msg length] > 20) {    // In case the message is slightly longer, then use the detailsLabelText
        hud.detailsLabelText = msg;
    }
    else {
        hud.labelText = msg;
    }
    
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hide:YES afterDelay:2.0];
}

+ (id)showAlertWithTitle:(NSString *)title message:(NSString *)msg delegate:(id)delegate tag:(NSInteger)tag cancelButton:(NSString *)cancelTitle otherTitles:(NSString *)otherButtonTitles, ... NS_REQUIRES_NIL_TERMINATION {
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                          message:msg
                                                         delegate:delegate cancelButtonTitle:cancelTitle
                                                otherButtonTitles:nil];
    
    if (otherButtonTitles != nil) {
        [alertView addButtonWithTitle:otherButtonTitles];
        va_list args;
        va_start(args, otherButtonTitles);
        NSString * otherTitle = nil;
        while((otherTitle = va_arg(args,NSString*))) {
            [alertView addButtonWithTitle:otherTitle];
        }
        va_end(args);
    }
    
    [alertView setTag:tag];
    
    [alertView show];
    
    return alertView;
}

+ (id)initializeCoachMarksView:(NSString*)viewControllerClass forView:(UIView*)containerView forElements:(NSArray *)elements {
    NSMutableArray* coachMarks;
    NSArray* marks;
    NSArray* labelItems;
    
    // Check if the coachMarks have already been shown for the given string
    // If it has already been shown, then return nil.
    if ([[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@%@", COACH_MARKS_SHOWN_PREFIX, viewControllerClass]] || ![[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@", COACH_MARKS_SETTINGS_BUNDLE_STRING]]) {
        return nil;
    }
    
    if ([viewControllerClass isEqualToString:@"TeamieChartsViewController"]) {
        marks = @[
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0, 64},{containerView.frame.size.width, 45}}],
                           @"caption": @"This is a dropdown that shows the currently selected report. Tap here to view the other reports you have access to."
                        }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0, 104},{containerView.frame.size.width, 65}}],
                           @"caption": @"This region gives a brief description of the currently selected report."
                        }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0, 169},{containerView.frame.size.width, containerView.frame.size.height - 65}}],
                           @"caption": @"This region displays the report data. It will be refreshed each time you select a report from the dropdown."
                        }],
        ];
        coachMarks = [[NSMutableArray alloc] initWithArray:marks];
    }
    else if ([viewControllerClass isEqualToString:@"NewsfeedViewController"]) {
        marks = @[
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{containerView.frame.size.width / 2 - 50, 20},{100, 44}}],
                           @"caption": @"Welcome to the Newsfeed screen where you can see all thoughts and questions shared in your classrooms."
                           }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0, 20},{50, 50}}],
                           @"caption": @"Tap on this menu button to view the app menu. Or, simply swipe left on the screen to reveal the menu."
                           }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{containerView.frame.size.width - 105, 20},{50, 50}}],
                           @"caption": @"Filter your newsfeed, by tapping on this filter button and selecting one of the filter options."
                           }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{containerView.frame.size.width - 70, 20},{75, 50}}],
                           @"caption": @"Post a new thought or question to your classroom by tapping on this Share button."
                           }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{0, containerView.frame.size.height -         (SYSTEM_VERSION_LESS_THAN(@"7.0")?30:50)},{50,50}}],
                           @"caption": @"Tap this button to view Bulletin Board notifications. If there are any new bulletin board notifications, a red label will appear on top of this button."
                           }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{50, containerView.frame.size.height -         (SYSTEM_VERSION_LESS_THAN(@"7.0")?30:50)},{50,50}}],
                           @"caption": @"Tap this button to view your private messages. A red label will appear on top of this button if you have any unread messages."
                           }],
        ];

        coachMarks = [[NSMutableArray alloc] initWithArray:marks];
    }
    else if ([viewControllerClass isEqualToString:@"ShareBoxViewController"]) {
        /*labelItems = @[
                       @"Enter the text you want to share here.",
                       @"You can post a thought or a question by switching the option here.",
                       @"Tap here to attach an image to your post. You can attach images from your Gallery or capture a new image.",
                       @"Tap this button to draw, scribble or sketch something and attach that as an image.",
                       @"Tap this button to attach a poll. NOTE: Polls can be attached only to questions.",
                       @"Tap this button to post anonymously. You may tap again to disable anonymous posting.",
                       @"Tap here to pick the classroom you want to share to.",
                       @"Once your are done, tap on the Post button to share your thought!"
                       ];*/
    }
    else if ([viewControllerClass isEqualToString:@"UserCalendarViewController"]) {
        marks = @[
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{5, 64},{44, 50}}],
                           @"caption": @"Tap on the previous arrow to view previous day's events. Or, swipe left to do the same."
                           }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{containerView.frame.size.width - 50, 64},{44, 50}}],
                           @"caption": @"Tap on the next arrow to view next day's events. Or, swipe right to do the same."
                           }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{45, 64},{containerView.frame.size.width - 90, 44}}],
                           @"caption": @"The Calendar now shows events for today. Tap on the date here to pick the day for which you wish to see the events."
                           }],
                       [NSMutableDictionary dictionaryWithDictionary:@{
                           @"rect": [NSValue valueWithCGRect:(CGRect){{containerView.frame.size.width - 50, 20},{50, 50}}],
                           @"caption": @"Tap this filter button, to filter the events by the calendar they belong to."
                           }],
                       ];
        coachMarks = [[NSMutableArray alloc] initWithArray:marks];
    }
    
    if (SYSTEM_VERSION_LESS_THAN(@"7.0")) {
        for (NSMutableDictionary* coachMark in coachMarks) {
            CGRect rect = [[coachMark valueForKey:@"rect"] CGRectValue];
            CGFloat x = rect.origin.x;
            CGFloat y = rect.origin.y - 20;
            CGFloat w = rect.size.width;
            CGFloat h = rect.size.height;
            
            rect = CGRectMake(x, y, w, h);
            
            [coachMark removeObjectForKey:@"rect"];
            [coachMark setObject:[NSValue valueWithCGRect:rect] forKey:@"rect"];
        }
    }
    
    if ([labelItems count] && [coachMarks count] == 0 && [elements count] <= [labelItems count]) {        // Labels are available and coachmarks are not and element are available
        NSInteger counter = -1;
        for (id element in elements) {
            counter++;
            NSMutableDictionary* coachMarkItem = [[NSMutableDictionary alloc] init];
            if ([element isKindOfClass:[UIView class]]) {
                [coachMarkItem setValue:[NSValue valueWithCGRect:[((UIView*)element) rectToMarkInView:nil]] forKey:@"rect"];
            }
            else if ([element isKindOfClass:[UIBarButtonItem class]]) {
                UIView* tempView = [((UIBarButtonItem*)element) valueForKey:@"view"];
                if (tempView) {
                    [coachMarkItem setValue:[NSValue valueWithCGRect:[tempView rectToMarkInView:nil]] forKey:@"rect"];
                }
                else {
                    continue;
                }
            }
            else {
                continue;
            }
            
            [coachMarkItem setValue:[labelItems objectAtIndex:counter] forKey:@"caption"];
            if (!coachMarks) {
                coachMarks = [[NSMutableArray alloc] initWithObjects:coachMarkItem, nil];
            }
            else {
                coachMarks = [[coachMarks arrayByAddingObject:coachMarkItem] mutableCopy];
            }
        }
    }
    
    if ([coachMarks count]) {
        WSCoachMarksView *coachMarksView = [[WSCoachMarksView alloc] initWithFrame:containerView.bounds coachMarks:coachMarks];
        coachMarksView.animationDuration = 0.5f;
        coachMarksView.enableContinueLabel = YES;
        coachMarksView.maskColor = [UIColor colorWithWhite:0.0 alpha:0.7];
        
        [containerView addSubview:coachMarksView];      // Add the coachmarks view to the container view
        
        return coachMarksView;
    }
    else {
        return nil;
    }
}

@end
