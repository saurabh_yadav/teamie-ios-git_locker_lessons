//
//  ShareBoxViewController.m
//  TeamieApp5
//
//  Created by Ramchander Krishna on 1/16/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import "ShareBoxViewController.h"
#import "NSData+Base64.h"
#import "ShareBoxPollOptions.h"
#import "PollOptionsViewController.h"
#import "TeamieClient.h"
#import "Classroom.h"

@interface ShareBoxViewController() {
    BOOL attachFlag;
}
-(void)updateClassroomSelectorView:(NSArray*)classrooms;
-(void)setAttachmentPreviewState:(BOOL)isEnabled;
-(void)updateConnectionsPopover:(id)connectionsResponse;
@end

@implementation ShareBoxViewController
@synthesize postThoughtButton;
@synthesize attachmentPreview;
@synthesize attachmentPreviewThumbnail;
@synthesize attachmentPreviewTitle;
@synthesize attachmentPreviewMeta;

@synthesize thoughtQuestionFlag;
@synthesize userDisplayPic;
@synthesize thoughtQuestionFlagLabel;
@synthesize thoughtActionsBar;
@synthesize topActionsBar;
@synthesize thoughtTextView;
@synthesize classroomSelector;
@synthesize selectedClassroom, classroomList=_classroomList;
@synthesize defaultClassroom = _defaultClassroom;
@synthesize attachmentImageName, attachmentImageData, attachmentLink, attachmentPollOptions;
@synthesize shareboxControllerDelegate;
@synthesize attachmentImageBase64String;
@synthesize attachmentFileName;
@synthesize attachmentFileData;
@synthesize attachmentFileBase64String;

@synthesize wePopoverController;

@synthesize requestProgress;

@synthesize context = _context;
@synthesize entity = _entity;

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

        //TODO: Nalin
//    if (self.coachMarksView) {
//        [self.coachMarksView start];
//        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:[NSString stringWithFormat:@"%@%@", COACH_MARKS_SHOWN_PREFIX, NSStringFromClass([self class])]];
//        [[NSUserDefaults standardUserDefaults] synchronize];
//        self.coachMarksView.delegate = self;
//    }
//    else {
//        [self.thoughtTextView becomeFirstResponder];
//    }
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _isThoughtPosted = NO;
    connectionPopoverInitFrame = CGRectZero;
    _isThoughtAnonymous = NO;
    attachFlag = NO;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        popoverClass = [WEPopoverController class];
    }
    else {
        popoverClass = [UIPopoverController class];
    }
    
    self.thoughtTextView.delegate = self;   // Notify self whenever the TextView is updated
    
    // set curved edge for display pic
    [userDisplayPic.layer setCornerRadius:UI_PICTURE_BORDER_RADIUS];
    [userDisplayPic.layer setMasksToBounds:YES];
    
    // move the uipickerview to the bottom of the screen
    CGRect tempFrame = self.classroomSelector.frame;
    tempFrame.origin.y = self.view.frame.size.height - self.navigationController.navigationBar.frame.size.height;
    self.classroomSelector.frame = tempFrame;
    [self.classroomSelector setHidden:YES];

    //TODO: request
//    [self.apiCaller performRestRequest:TeamieUserClassroomsRequest withParams:nil];
//    [self.apiCaller performRestRequest:TeamieUserMenuRequest withParams:nil];

    // if there are any variables stored in NSUserDefaults related to thought cache, then retrieve it & display
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"cache_thought_text"]) {
        self.thoughtTextView.text = [[NSUserDefaults standardUserDefaults] stringForKey:@"cache_thought_text"];
        // enable the share button once the text is entered
        [self.postThoughtButton setEnabled:YES];
    }
    if ([[NSUserDefaults standardUserDefaults] integerForKey:@"cache_thought_type"]) {
        [self.thoughtQuestionFlag setSelectedSegmentIndex:[[NSUserDefaults standardUserDefaults] integerForKey:@"cache_thought_type"]];
    }
    if ([[NSUserDefaults standardUserDefaults] stringForKey:@"cache_thought_attached_link"]) {
        [self updateAttachmentLink:[[NSUserDefaults standardUserDefaults] stringForKey:@"cache_thought_attached_link"]];
        attachFlag = YES;
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"cache_thought_image"]) {
        [self updateAttachmentImage:[UIImage imageWithData:[[NSUserDefaults standardUserDefaults] objectForKey:@"cache_thought_image"]]];
        attachFlag = YES;
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"cache_thought_file"] && [[NSUserDefaults standardUserDefaults] objectForKey:@"cache_thought_file_extension"]) {
        [self updateAttachmentFile:[[NSUserDefaults standardUserDefaults] objectForKey:@"cache_thought_file"]withExtension:[[NSUserDefaults standardUserDefaults] objectForKey:@"cache_thought_file_extension"]];
        attachFlag = YES;
    }
    
    // remove the cached values so that they do not continue to stay in memory if user posts the thought.
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cache_thought_text"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cache_thought_type"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cache_thought_attached_link"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cache_thought_image"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cache_thought_file"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cache_thought_file_extension"];

    [self.attachmentPreview addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(attachmentPreviewSelected:)]];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        // Add notification observers for the keyboard that pops up, only for the iPad
        NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
        
        [nc addObserver:self selector:@selector(keyboardWillShow:) name:
         UIKeyboardWillShowNotification object:nil];
        
        [nc addObserver:self selector:@selector(keyboardWillHide:) name:
         UIKeyboardWillHideNotification object:nil];
    }
    
    NSArray* coachMarkElements = @[
                                   self.thoughtTextView,
                                   self.thoughtQuestionFlag,
                                   [[self.thoughtActionsBar items] objectAtIndex:0],
                                   [[self.thoughtActionsBar items] objectAtIndex:1],
                                   [[self.thoughtActionsBar items] objectAtIndex:2],
                                   [[self.thoughtActionsBar items] objectAtIndex:3]
                                   ];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
//        self.coachMarksView = [TeamieUIGlobals initializeCoachMarksView:NSStringFromClass([self class]) forView:self.navigationController.view forElements:coachMarkElements];
    }
    else {
//        self.coachMarksView = [TeamieUIGlobals initializeCoachMarksView:NSStringFromClass([self class]) forView:self.view forElements:coachMarkElements];
    }
}

- (void)viewDidUnload {
    [self setThoughtTextView:nil];
    [self setClassroomSelector:nil];
    [self setSelectedClassroom:nil];
    [self setSelectedClassroom:nil];
    [self setThoughtQuestionFlag:nil];
    
    self.classroomList = nil;
    [self setUserDisplayPic:nil];
    [self setThoughtTextView:nil];
    [self setPostThoughtButton:nil];
    [self setAttachmentPreview:nil];
    [self setAttachmentPreviewThumbnail:nil];
    [self setAttachmentPreviewTitle:nil];
    [self setAttachmentPreviewMeta:nil];
    [self setAttachmentImageBase64String:nil];
    [self setAttachmentFileData:nil];
    [self setAttachmentFileName:nil];
    [self setAttachmentFileBase64String:nil];
    
    [self setShareboxControllerDelegate:nil];
    [self setThoughtQuestionFlagLabel:nil];
    [self setThoughtActionsBar:nil];
    [self setTopActionsBar:nil];
    
    [self.wePopoverController dismissPopoverAnimated:NO];
    [self setWePopoverController:nil];
    
    [self setThoughtCameraOption:nil];
    [super viewDidUnload];
}

- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    
    if (!_isThoughtPosted) {  // if the view is going to disappear bcos of user tapping outside the thought share box
        // cache the thought text, thought type and any attached links
        // @To-do: Cache attached images so that they are still attached the next time thought Share box is opened.
        
        if ([self.thoughtTextView text]) {
            [[NSUserDefaults standardUserDefaults] setObject:[self.thoughtTextView text] forKey:@"cache_thought_text"];
        }
        [[NSUserDefaults standardUserDefaults] setInteger:self.thoughtQuestionFlag.selectedSegmentIndex forKey:@"cache_thought_type"];
        
        if (self.attachmentLink != nil) {  // if there's a link to be attached
            [[NSUserDefaults standardUserDefaults] setObject:self.attachmentLink forKey:@"cache_thought_attached_link"];
        }
    }
}

#pragma mark - IBAction methods
- (IBAction)classroomSelectorPressed:(id)sender {
    
    if (self.classroomSelector.isHidden == YES) {   // if the UIPickerView is hidden, then bring it into the screen
        
        [self.classroomSelector setHidden:NO];
        
        // animate the UIPickerView into the frame
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseIn];
        
        CGRect tempFrame = self.classroomSelector.frame;
        tempFrame.origin.y -= self.classroomSelector.frame.size.height;
        self.classroomSelector.frame = tempFrame;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { // animate the thoughtActions Bar also
            tempFrame = self.thoughtActionsBar.frame;
            tempFrame.origin.y -= self.classroomSelector.frame.size.height;
            self.thoughtActionsBar.frame = tempFrame;
        }
        
        [UIView commitAnimations];
        
        // resign the textview as the first responder
        [self.thoughtTextView resignFirstResponder];
        
        //[[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:@"Thought Share" action:@"Classroom Select" label:nil value:0] build]];
    }
}

- (IBAction)postThought:(id)sender {
    
    // disable the share thought button
    [self.postThoughtButton setEnabled:NO];
    
    NSString * thoughtText = [thoughtTextView text];
    NSInteger selectedClassIndex = [self.classroomSelector selectedRowInComponent:0];
    if (selectedClassIndex >= [self.classroomList count]) {
        // the selected class index is greater than the actual number of classrooms there is
        [TeamieGlobals addTSMessageInController:self title:nil message:TeamieLocalizedString(@"MSG_THOUGHT_POST_CANNOT", nil) type:@"error" duration:2 withCallback:nil];
        return;
    }
    
    NSNumber * classroomID = ((Classroom *)[self.classroomList objectAtIndex:selectedClassIndex]).nid;
    
    NSString * thoughtType = (thoughtQuestionFlag.selectedSegmentIndex == 0) ? @"thought" : @"question";
    
    NSMutableDictionary * thoughtParams = [[NSMutableDictionary alloc] initWithObjectsAndKeys:classroomID, @"nid", thoughtType, @"type", thoughtText, @"message", nil];
    
    // show an activity label
    [TeamieGlobals addHUDTextOnlyLabel:[NSString stringWithFormat:@"Posting %@...", thoughtType] margin:10.f yOffset:10.f];
    
    if (self.attachmentImageName != nil && self.attachmentImageData != nil) {   // if there's an image to be attached
        [thoughtParams setObject:self.attachmentImageName forKey:@"image[filename]"];
        [thoughtParams setValue:self.attachmentImageBase64String forKey:@"image[file]"];
        [thoughtParams setObject:@"image" forKey:@"attachment-type"];
    }
    else if (self.attachmentLink != nil) {  // if there's a link to be attached
        [thoughtParams setObject:self.attachmentLink forKey:@"link"];
        [thoughtParams setObject:@"link" forKey:@"attachment-type"];
    }
    else if (self.attachmentFileName != nil && self.attachmentFileData != nil) {
        [thoughtParams setObject:self.attachmentFileName forKey:@"file[filename]"];
        [thoughtParams setValue:self.attachmentFileBase64String forKey:@"file[file]"];
        [thoughtParams setObject:@"file" forKey:@"attachment-type"];
    }
    else if (self.attachmentPollOptions != nil) {
        
        [thoughtParams setObject:@"poll" forKey:@"attachment-type"];
        [thoughtParams setObject:[NSString stringWithFormat:@"%d", attachmentPollOptions.count] forKey:@"poll[choice_count]"];
        for (ShareBoxPollOptions* option in attachmentPollOptions) {
            [thoughtParams setObject:option.title forKey:[NSString stringWithFormat:@"poll[choice][%@]", option.order]];
        }
    }
    
    // set the thought is announcement parameter if it's set to true!
    if (_isThoughtAnonymous) {
        [thoughtParams setObject:@"true" forKey:@"is_anonymous"];
    }
    
    // create a progressView
    self.requestProgress = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
    [self.requestProgress setProgress:0.0f];
    
    // add it navigationBar if it's an iPhone
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.navigationItem.titleView = self.requestProgress;
    }
    else {  // if it's iPad
        CGRect tempFrame = self.requestProgress.frame;
        tempFrame.origin.x = UI_MEDIUM_LARGE_MARGIN + 60.0; // since there would be a cancel button
        tempFrame.origin.y = UI_MEDIUM_LARGE_MARGIN;
        self.requestProgress.frame = tempFrame;
        
        [self.topActionsBar addSubview:self.requestProgress];
    }
            //TODO: Nalin
//    [self.apiCaller performRestRequest:TeamieUserPostThoughtRequest withParams:thoughtParams];

    //[[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:@"Thought Share" action:@"Thought Post" label:thoughtType value:0] build]];
}

- (IBAction)thoughtOptionSelected:(UIBarButtonItem*)sender {
    if (sender.tag == 0) { // attach image option selected
        //display action sheet to select camera or gallery
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:TeamieLocalizedString(@"ATTACH_IMAGE_OPTIONS", nil) delegate:self cancelButtonTitle:TeamieLocalizedString(@"BTN_CANCEL", nil) destructiveButtonTitle:nil otherButtonTitles:TeamieLocalizedString(@"BTN_IMAGE_FROM_GALLERY", nil), TeamieLocalizedString(@"BTN_NEW_IMAGE", nil), nil];
        [actionSheet showInView:self.view];
        //[[GANTracker sharedTracker] trackEvent:@"Thought Share" action:@"Thought Attach Image Select" label:nil value:0 withError:nil];
    }
    else if (sender.tag == 1) {
#ifdef DEBUG
        NSLog(@"Opening drawing view...");
#endif
        //[[GANTracker sharedTracker] trackEvent:@"Thought Share" action:@"Drawing Image" label:nil value:0 withError:nil];
        
        UIStoryboard *secondStoryboard = [UIStoryboard storyboardWithName:[TeamieGlobals storyboardIdentifier] bundle:nil];
        DrawScribbleViewController *drawView = [secondStoryboard instantiateViewControllerWithIdentifier: @"DrawScribbleViewController"];
        [drawView setDelegate:self];
        [self presentViewController:drawView animated:YES completion:nil];
    }
    else if (sender.tag == 2) { // post as announcement option selected or unselected depending on the value of
        _isThoughtAnonymous = !(_isThoughtAnonymous);
        if (_isThoughtAnonymous) {
            // set the button background image to be announcement enabled
            // display a temporary message that announcement is enabled
            [sender setTintColor:[UIColor blueColor]];
            
            [TeamieUIGlobals displayStatusMessage:TeamieLocalizedString(@"MSG_MAKE_THOUGHT_ANONYMOUS", @"This message will now be posted anonymously.") forPurpose:@"invisible"];
            
            [self.userDisplayPic setImage:[TeamieUIGlobals defaultPicForPurpose:@"profile" withSize:self.userDisplayPic.frame.size.width andColor:[UIColor darkTextColor]]];
        }
        else {
            // set the button background image to be normal
            [sender setTintColor:nil];
            NSString * displayPicURL = [self.apiCaller.currentUser.profileImage valueForKey:@"displayPic"];
            if (displayPicURL != nil) {
                UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:displayPicURL]]];
                [self.userDisplayPic setImage:image];
            }
        }
    }
    else if (sender.tag == 3) {
        if (self.thoughtQuestionFlag.selectedSegmentIndex == 0) {
            [TeamieGlobals addTSMessageInController:self title:TeamieLocalizedString(@"Can't Attach Poll to a $thought[s]$", nil) message:TeamieLocalizedString(@"Please select $thought[s]$ Type as question", nil) type:@"warning" duration:5 withCallback:nil];
        }
        else {
            // Show the poll attach screen
            [self performSegueWithIdentifier:@"attachPoll" sender:self];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    PollOptionsViewController* vc = segue.destinationViewController;
    vc.delegate = self;
}

- (void)pollOptionsViewControllerDidDismiss {
    [self getListOfObjectsInContext:@"ShareBoxPollOptions"];
}

- (IBAction)dismissShareBox:(id)sender {
    
    if (attachFlag) {
        [self dismissModalViewControllerAnimated:YES];
    }
    else {
        // if it's an iPad view then dismiss the modal view controller
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [self dismissModalViewControllerAnimated:YES];
        }
        
        [self deleteExistingPollOptions];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"pollOptionsOrder"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSNumber* postStatus = nil;
        
        if (_isThoughtPosted) { // Set the thought post status as 1
            postStatus = [NSNumber numberWithInt:1];
        }
        
        [self.shareboxControllerDelegate shareBoxDidExit:postStatus];
        
        //[[GANTracker sharedTracker] trackEvent:@"Thought Share" action:@"Thought Share Cancel" label:nil value:0 withError:nil];
    }    
}

- (IBAction)thoughtQuestionFlagValueChanged:(id)sender {
    
    if (self.thoughtQuestionFlag.selectedSegmentIndex == 0) {
        
        self.thoughtQuestionFlagLabel.text = TeamieLocalizedString(@"$thought[s]$", nil);
        
    }
    else {
        self.thoughtQuestionFlagLabel.text = @"Question";
    }
    
    //[[GANTracker sharedTracker] trackEvent:@"Thought Share" action:@"Thought Type Change" label:self.thoughtQuestionFlagLabel.text value:0 withError:nil];
}

#pragma mark - TeamieRESTRequest Delegate methods
-(void)requestSuccessful:(TeamieRESTRequest)requestName withResponse:(id)response {
    [TeamieGlobals dismissActivityLabels];
    
    switch (requestName) {
        case TeamieUserClassroomsRequest:
            [self updateClassroomSelectorView:response];
            break;
        case TeamieUserMenuRequest:
            if (response != nil) {
//                [self.apiCaller performRestRequest:TeamieUserProfileRequest withParams:[NSDictionary dictionaryWithKeysAndObjects:@"uid", [NSString stringWithFormat:@"%qi", [self.apiCaller.currentUser.uid longLongValue]], nil]];
            }
            break;
        case TeamieUserProfileRequest: {
            NSString * displayPicURL = [self.apiCaller.currentUser.profileImage valueForKey:@"displayPic"];
#ifdef DEBUG
            NSLog(@"Display pic URL: %@", displayPicURL);
#endif
            if (displayPicURL != nil) {
                UIImage *image = [[UIImage alloc] initWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:displayPicURL]]];
                [self.userDisplayPic setImage:image];
            }
        }
            break;
        case TeamieUserConnectionsRequest:
            if ([response isKindOfClass:[NSArray class]]) {
                [self updateConnectionsPopover:response];
            }
            break;
        default:
            break;
    }
}

-(void)requestSuccessful:(TeamieRESTRequest)requestName {
    [TeamieGlobals dismissActivityLabels];
    
    switch (requestName) {
        case TeamieUserPostThoughtRequest:
            _isThoughtPosted = YES;
            
            // call dismissShareBox method with sender as the Post Thought button
            [self dismissShareBox:self.postThoughtButton];
            break;
        default:
            break;
    }
}

-(void)requestLoading:(float)progress forRequest:(TeamieRESTRequest)requestName {
    if (requestName == TeamieUserPostThoughtRequest) {
        [self.requestProgress setProgress:progress animated:YES];
    }
}

-(void)requestFailed:(NSString *)message forRequest:(TeamieRESTRequest)requestName {
    
    [TeamieGlobals dismissActivityLabels];
    
    [self.requestProgress setHidden:YES];
    
    [TeamieGlobals addTSMessageInController:self title:TeamieLocalizedString(@"MSG_REQUEST_FAILED", nil) message:message type:@"error" duration:3 withCallback:nil];
}

#pragma mark - Lazy Methods

-(NSMutableArray*)classroomList {
    if (_classroomList == nil) {
        _classroomList = [[NSMutableArray alloc] init];
    }
    return _classroomList;
}

-(NSNumber*)defaultClassroom {
    if (!_defaultClassroom) {
        _defaultClassroom = [NSNumber numberWithInt:0];
    }
    return _defaultClassroom;
}

#pragma mark - Private Methods

-(void)updateClassroomSelectorView:(NSArray*)classrooms {
    
    if ([classrooms count] == 0) {
        [TeamieGlobals addTSMessageInController:self title:TeamieLocalizedString(@"No $classroom[p]$ found", nil) message:@"Please contact your Admin" type:@"error" duration:3 withCallback:nil];
        
        [self dismissShareBox:nil];
        
        return;
    }
    
    [self.classroomList removeAllObjects];  // remove any previously contained objects
    
    NSUInteger selClassIndex = 0;
    NSUInteger counter = 0;
    NSUInteger defClassID = [self.defaultClassroom longLongValue];
    
    for (Classroom* classroom in classrooms) {        // add classrooms one by one
        [self.classroomList addObject:classroom];
        if (defClassID == [classroom.nid longLongValue]) {
            selClassIndex = counter;
        }
        counter++;
    }
    
    self.classroomSelector.delegate = self;
    [self.classroomSelector reloadAllComponents];   // redraw the UIPickerView
    // set the selected Classroom to be the first classroom in the list
    NSString * temp = [[self.classroomList objectAtIndex:selClassIndex] valueForKey:@"name"];
    // Update the classroom name display
    [self.selectedClassroom setTitle:temp forState:UIControlStateNormal];
    // update the UIPickerView
    [self.classroomSelector selectRow:selClassIndex inComponent:0 animated:NO];
    // make the text view the first responder so that the cursor is still blinking :)
    // But do this only if coach marks are not being shown.
    if (!self.coachMarksView) {
        [self.thoughtTextView becomeFirstResponder];
    }
}

- (void)updateDefaultClassroom:(NSNumber *)classID {
    self.defaultClassroom = [NSNumber numberWithLongLong:[classID longLongValue]];
}

-(void)setAttachmentPreviewState:(BOOL)isEnabled {
    if (isEnabled && [self.attachmentPreview isHidden] == NO) { return; }
    if ((!isEnabled) && [self.attachmentPreview isHidden] == YES) { return; }
        
    CGRect thoughtFrame = self.thoughtTextView.frame;
    CGRect previewFrame = self.attachmentPreview.frame;
    
    if (isEnabled) {
        [self.attachmentPreview setHidden:NO];
        // adjust height of the UITextView to make space for the height of preview pane
        thoughtFrame.size.height = thoughtFrame.size.height - previewFrame.size.height - UI_MEDIUM_MARGIN;
        [self.thoughtTextView setFrame:thoughtFrame];
    }
    else {
        [self.attachmentPreview setHidden:YES];
        // adjust height of UITextView to take up space of preview pane
        thoughtFrame.size.height = thoughtFrame.size.height + previewFrame.size.height + UI_MEDIUM_MARGIN;
        [self.thoughtTextView setFrame:thoughtFrame];
    }
}

-(void)attachmentPreviewSelected:(UITapGestureRecognizer *)sender {
    NSString* otherButtonTitles = nil;
    if (self.attachmentPollOptions != nil) {
        otherButtonTitles = @"Edit Poll Options";
    }
    if (sender.state == UIGestureRecognizerStateEnded) {
        [self.attachmentPreview setBackgroundColor:[UIColor whiteColor]];
        UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:TeamieLocalizedString(@"EDIT_ATTACHMENT", nil) delegate:self cancelButtonTitle:TeamieLocalizedString(@"BTN_CANCEL", nil) destructiveButtonTitle:TeamieLocalizedString(@"BTN_DELETE_ATTACHMENT", nil) otherButtonTitles:otherButtonTitles, nil];
        [actionSheet showInView:self.view];
    }
}

- (void)updateAttachmentImage:(UIImage*)image {
    if (image != nil) {
        self.attachmentImageData = UIImageJPEGRepresentation(image, 1.0);
        //    self.attachmentImageData = UIImagePNGRepresentation(imageInViewContext);
        self.attachmentImageName = [NSString stringWithFormat: @"Thought-image-%@.jpg", [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]], nil];
        
        // set the thumbnail preview as that image
        [self.attachmentPreviewThumbnail setImage:image];
        [self.attachmentPreviewTitle setText:TeamieLocalizedString(@"ATTACHED_IMAGE", nil)];
        [self.attachmentPreviewMeta setText:[NSString stringWithFormat: @"%.1lfMB, %.0lfx%.0lfpx", (float)self.attachmentImageData.length / (1024.0 * 1024.0), image.size.width, image.size.height]];
        [self setAttachmentPreviewState:YES];
        self.attachmentImageBase64String = [self.attachmentImageData base64EncodedString];
    }
    else {
        self.attachmentImageData = nil;
        self.attachmentImageName = nil;
        self.attachmentPreviewThumbnail = nil;
        self.attachmentPreviewTitle = nil;
        self.attachmentPreviewMeta = nil;
        self.attachmentImageBase64String = nil;
        [self setAttachmentPreviewState:NO];
    }
}


- (void)setImageData:(UIImage *)imageInViewContext {
    //    NSLog(@"attachedImageData: %@", attachedImageData);
    self.attachmentImageData = UIImageJPEGRepresentation(imageInViewContext, 1.0);
    //    self.attachmentImageData = UIImagePNGRepresentation(imageInViewContext);
    self.attachmentImageName = [NSString stringWithFormat: @"Thought-image-%@.jpg", [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]], nil];
    
    // set the thumbnail preview as that image
    [self.attachmentPreviewThumbnail setImage:imageInViewContext];
    [self.attachmentPreviewTitle setText:TeamieLocalizedString(@"ATTACHED_IMAGE", nil)];
    [self.attachmentPreviewMeta setText:[NSString stringWithFormat: @"%.1lfMB, %.0lfx%.0lfpx", (float)self.attachmentImageData.length / (1024.0 * 1024.0), imageInViewContext.size.width, imageInViewContext.size.height]];
    [self setAttachmentPreviewState:YES];
    
    // encode the image to base64. Since this is an intensive process, do it in another thread
    //    NSOperationQueue* queue = [NSOperationQueue new];
    //    NSInvocationOperation* operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(convertImageToBase64) object:nil];
    //    [queue addOperation:operation];
    
    self.attachmentImageBase64String = [self.attachmentImageData base64EncodedString];
    
#ifdef DEBUG
    NSLog(@"Captured image named: %@", self.attachmentImageName);
#endif
    [self dismissViewControllerAnimated:YES completion:nil];

}

- (void)convertImageToBase64 {
    if (self.attachmentImageData) {
        self.attachmentImageBase64String = [self.attachmentImageData base64EncodedString];
    }
}

- (void)updateAttachmentLink:(NSString*)link {
    if (link != nil) {
        self.attachmentLink = link;
        [self.attachmentPreviewThumbnail setImage:[UIImage imageNamed:@"icon-attached-link.png"]];
        self.attachmentPreviewTitle.text = TeamieLocalizedString(@"ATTACHED_LINK", nil);
        self.attachmentPreviewMeta.text = link;
        [self setAttachmentPreviewState:YES];
    }
    else { // if the link to be attached is given as nil, then unset all the stuff
        self.attachmentLink = nil;
        [self.attachmentPreviewThumbnail setImage:nil];
        self.attachmentPreviewMeta.text = nil;
        self.attachmentPreviewTitle.text = nil;
        [self setAttachmentPreviewState:NO];
    }
}

- (void)updateAttachmentPoll:(NSArray*)pollOptions {
    if ([pollOptions count] > 0) {
        self.attachmentPollOptions = pollOptions;
        [self.attachmentPreviewThumbnail setImage:[UIImage imageNamed:@"icon-attached-poll.png"]];
        self.attachmentPreviewTitle.text = @"Attached Poll";
        self.attachmentPreviewMeta.text = [NSString stringWithFormat:@"Number of Poll Options: %d", [pollOptions count]];
        [self setAttachmentPreviewState:YES];
    }
    else { // if the poll to be attached is given as nil, then unset all the stuff
        self.attachmentPollOptions = nil;
        [self.attachmentPreviewThumbnail setImage:nil];
        self.attachmentPreviewMeta.text = nil;
        self.attachmentPreviewTitle.text = nil;
        [self setAttachmentPreviewState:NO];
    }

}

- (void)updateAttachmentFile:(NSData*)fileData withExtension:(NSString*)ext {
    if (fileData != nil) {
        self.attachmentFileData = fileData;
        self.attachmentFileName = [NSString stringWithFormat: @"Thought-file-%@.%@", [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]], ext, nil];
        
        // set the thumbnail preview as that image
        [self.attachmentPreviewThumbnail setImage:[UIImage imageNamed:@"icon-attached-link.png"]];
        [self.attachmentPreviewTitle setText:TeamieLocalizedString(@"ATTACHED_FILE", nil)];
        [self.attachmentPreviewMeta setText:[NSString stringWithFormat: @"%.1lfMB", (float)self.attachmentFileData.length / (1024.0 * 1024.0)]];
        [self setAttachmentPreviewState:YES];
        self.attachmentFileBase64String = [self.attachmentFileData base64EncodedString];
    }
    else {
        self.attachmentFileData = nil;
        self.attachmentFileName = nil;
        self.attachmentPreviewThumbnail = nil;
        self.attachmentPreviewTitle = nil;
        self.attachmentPreviewMeta = nil;
        self.attachmentFileBase64String = nil;
        [self setAttachmentPreviewState:NO];
    }
}

// Method to scale and rotate the image so that it appears properly on the web
// Code from: http://discussions.apple.com/thread.jspa?messageID=7949889
- (UIImage *)scaleAndRotateImage:(UIImage *)image {
    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        }
        else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}

- (void)updateConnectionsPopover:(id)connectionsResponse {
    
    if (![connectionsResponse isKindOfClass:[NSArray class]] || [connectionsResponse count] <= 0) {
        if (self.wePopoverController) {
            [self.wePopoverController dismissPopoverAnimated:YES];
            [self.wePopoverController.delegate popoverControllerDidDismissPopover:self.wePopoverController];
        }
        return;
    }
    
    NSMutableArray *connectionItems = [[NSMutableArray alloc] init];
    
    /*for (UserProfile * user in connectionsResponse) {
        TTTeamieUserProfileItem *connectionItem = [TTTeamieUserProfileItem itemWithText:user.fullname subtitle:user.email imageURL:user.displayPic profileData:user delegate:nil selector:nil];
        [connectionItems addObject:connectionItem];
    }
    
    // prepare a datasource of the connections to show
    TeamieListDataSource * dataSource = [[TeamieListDataSource alloc] initWithItems:connectionItems];
    
    if (!self.wePopoverController) {
        // if the popover controller is not initialized
        PopupMenuViewController *contentViewController = [[PopupMenuViewController alloc] initWithStyle:UITableViewStylePlain];
        contentViewController.dataSource = dataSource;
        contentViewController.menuDelegate = self;
        contentViewController.contentSizeForViewInPopover = CGSizeMake(self.view.frame.size.width, 100.0);
        [contentViewController setNumMenuItems:connectionItems.count];
        contentViewController.tableView.scrollEnabled = YES;
        
        self.wePopoverController = [[popoverClass alloc] initWithContentViewController:contentViewController];
        NSLog(@"The popover is of class type : %@", popoverClass);
        self.wePopoverController.delegate = self;
        
        // convert the init popover frame CGRect to self.view co-ordinates
        connectionPopoverInitFrame = [self.view convertRect:connectionPopoverInitFrame fromView:self.thoughtTextView];
        NSLog(@"The popover init frame is %@ and self view is %@", NSStringFromCGRect(connectionPopoverInitFrame), NSStringFromCGRect(self.view.frame));
        
        [self.wePopoverController presentPopoverFromRect:connectionPopoverInitFrame inView:self.view permittedArrowDirections:((connectionPopoverInitFrame.origin.y <= self.thoughtActionsBar.frame.origin.y / 2.0) ? UIPopoverArrowDirectionUp : UIPopoverArrowDirectionDown) animated:YES];
    }
    else {
        ((TTTableViewController*)self.wePopoverController.contentViewController).dataSource = dataSource;
        // set the no. of connection items and the height of the popover menu accordingly
        [((PopupMenuViewController*)self.wePopoverController.contentViewController) setNumMenuItems:connectionItems.count];
    }*/
}

#pragma mark - UIPickerViewDelegate Methods

- (void)pickerView:(UIPickerView *)pickerView didSelectRow: (NSInteger)row inComponent:(NSInteger)component {
    NSString * selClass = [[self.classroomList objectAtIndex:row] valueForKey:@"name"];
    [self.selectedClassroom setTitle:selClass forState:UIControlStateNormal];
}

// tell the picker how many rows are available for a given component
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSUInteger numRows = [self.classroomList count];
    return numRows;
}

// tell the picker how many components it will have
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// tell the picker the title for a given component
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    NSString *title;
    title = [[self.classroomList objectAtIndex:row] valueForKey:@"name"];
    
    return title;
}

// tell the picker the width of each row for a given component
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    int sectionWidth = 300;
    return sectionWidth;
}

#pragma mark - UIActionSheetDelegate Methods

- (void)actionSheet:(UIActionSheet *)sender clickedButtonAtIndex:(int)index
{
    if ([sender.title isEqualToString:TeamieLocalizedString(@"EDIT_ATTACHMENT", nil)]) {
        if (index == sender.destructiveButtonIndex) {
            if (self.attachmentImageName != nil) { // remove the attached image
                self.attachmentImageName = nil;
                self.attachmentImageData = nil;
                [self setAttachmentPreviewState:NO];    // hide the attachment preview pane
                //[[GANTracker sharedTracker] trackEvent:@"Thought Share" action:@"Attach Image Remove" label:nil value:0 withError:nil];
            }
            else if (self.attachmentLink != nil) {
                self.attachmentLink = nil;
                [self setAttachmentPreviewState:NO];
                //[[GANTracker sharedTracker] trackEvent:@"Thought Share" action:@"Attach Link Remove" label:nil value:0 withError:nil];
            }
            else if (self.attachmentPollOptions != nil) {
                self.attachmentPollOptions = nil;
                [self deleteExistingPollOptions];
                [self setAttachmentPreviewState:NO];
                //[[GANTracker sharedTracker] trackEvent:@"Thought Share" action:@"Attached Poll Removed" label:nil value:0 withError:nil];
            }
        }
        if (index == sender.firstOtherButtonIndex) {
            [self performSegueWithIdentifier:@"attachPoll" sender:self];
        }
    }
    else if ([sender.title isEqualToString:TeamieLocalizedString(@"ATTACH_IMAGE_OPTIONS", nil)]) {
        if (index != sender.cancelButtonIndex) { // user did not press Cancel
            // Create image picker controller
            UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
            
            switch (index) {
                case 0: {
#ifdef DEBUG
                    NSLog(@"Open image gallery...");
#endif
                    // Set source to the photo library
                    imagePicker.sourceType =  UIImagePickerControllerSourceTypePhotoLibrary;
                    //[[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:@"Thought Share" action:@"Attach Image From Gallery" label:nil value:0] build]];
                }
                    break;
                case 1: {
#ifdef DEBUG
                    NSLog(@"Open camera for taking a pic...");
#endif
                    //[[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:@"Thought Share" action:@"Attach Image From Camera" label:nil value:0] build]];
                    
                    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO) {
#ifdef DEBUG
                        NSLog(@"Sorry! Camera is not available on this device!");
#endif
                        return;
                    }
                    
                    // Set source to the camera of the device
                    imagePicker.sourceType =  UIImagePickerControllerSourceTypeCamera;
                    imagePicker.allowsEditing = YES;
                }
                    break;
                default:
#ifdef DEBUG
                    NSLog(@"Action sheet button index not implemented.");
#endif
                    return;
                    break;
            }
            
            // Delegate is self
            if (index == 0 || index == 1) {
                
                imagePicker.delegate = self;
                
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
                    // Show image picker
                    [self presentModalViewController:imagePicker animated:YES];
                    //                [self presentViewController:imagePicker animated:YES completion:nil];
                }
                else {
                    // dismiss and dealloc a popover controller if it's already there!
                    [self.wePopoverController dismissPopoverAnimated:YES];
                    self.wePopoverController = nil;
                    
                    self.wePopoverController = [[popoverClass alloc] initWithContentViewController:imagePicker];
                    self.wePopoverController.delegate = self;
                    [self.wePopoverController presentPopoverFromRect:self.thoughtActionsBar.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
                }
            }
        }
        else {
            
            //[[GANTracker sharedTracker] trackEvent:@"Thought Share" action:@"Attach Image Cancel" label:nil value:0 withError:nil];
            
        }
    }
}

#pragma mark - UIImagePickerControllerDelegate methods

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    // dismiss the popovers no matter what
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.wePopoverController dismissPopoverAnimated:YES];
        self.wePopoverController = nil;
    }
    else {
        [self dismissModalViewControllerAnimated:YES];
    }
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera || picker.sourceType == UIImagePickerControllerSourceTypePhotoLibrary) {
        // Access the uncropped image from info dictionary
        UIImage *image = [info objectForKey:@"UIImagePickerControllerEditedImage"];
        if (image) {   // if the image was captured using the camera
            // apply some rotate transformations so that the image appears properly on the web
            image = [self scaleAndRotateImage:image];
            // save the cropped & scaled image to the Camera Roll or user device album
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
        }
        else {
            image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        }
        
        // Convert the image to JPEG
        self.attachmentImageData = UIImageJPEGRepresentation(image, 1.0);
        //self.attachmentImageData = UIImagePNGRepresentation(image);
        self.attachmentImageName = [NSString stringWithFormat: @"Thought-image-%@.jpg", [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]], nil];
        
        // set the thumbnail preview as that image
        [self.attachmentPreviewThumbnail setImage:image];
        [self.attachmentPreviewTitle setText:TeamieLocalizedString(@"ATTACHED_IMAGE", nil)];
        [self.attachmentPreviewMeta setText:[NSString stringWithFormat: @"%.1lfMB, %.0lfx%.0lfpx", (float)self.attachmentImageData.length / (1024.0 * 1024.0), image.size.width, image.size.height]];
        [self setAttachmentPreviewState:YES];
        
        // encode the image to base64. Since this is an intensive process, do it in another thread
        NSOperationQueue* queue = [NSOperationQueue new];
        NSInvocationOperation* operation = [[NSInvocationOperation alloc] initWithTarget:self selector:@selector(convertImageToBase64) object:nil];
        [queue addOperation:operation];
        
        // self.attachmentImageBase64String = [self.attachmentImageData base64EncodedString];
        
#ifdef DEBUG
        NSLog(@"Captured image named: %@", self.attachmentImageName);
#endif
        //[self dismissViewControllerAnimated:YES completion:nil];
    }
    else {
#ifdef DEBUG
        NSLog(@"The Image picker sourceType was not camera or photo library. Aborting...");
#endif
        // [self dismissModalViewControllerAnimated:YES];
    }
    // Save image to album
    //UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    // dismiss the popovers no matter what
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [self.wePopoverController dismissPopoverAnimated:YES];
        self.wePopoverController = nil;
    }
    else {
        [self dismissModalViewControllerAnimated:YES];
    }
}

#pragma mark - UINavigationControllerDelegate
-(void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // code to execute just before the image gallery is displayed
}
-(void)navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    // code to execute just after the image gallery is displayed
}

#pragma mark - UITextViewDelegate methods

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    NSRange atRange = [textView.text rangeOfString:@"@" options:NSBackwardsSearch range:NSMakeRange(0, range.location)];
    NSRange spaceRange = [textView.text rangeOfString:@"]" options:NSBackwardsSearch range:NSMakeRange(0, range.location)];
    
    if ((atRange.location != NSNotFound) && (spaceRange.location == NSNotFound || (spaceRange.location != NSNotFound && spaceRange.location < atRange.location))) {
        NSString * newtext = [textView.text stringByReplacingCharactersInRange:range withString:text];
        // find position of next space character after the @ symbol
        NSRange nextSpaceRange = [self.thoughtTextView selectedRange];
        NSRange tagNameRange;
        if ([text length] == 0) {
            tagNameRange = NSMakeRange(atRange.location + 1, nextSpaceRange.location - atRange.location - range.length - 1);
        }
        else {
            tagNameRange = NSMakeRange(atRange.location + 1, nextSpaceRange.location - atRange.location);
        }
        NSString * tagName = [newtext substringWithRange:tagNameRange];
        // so tagName now contains the string which we should search for in the username
#ifdef DEBUG
        NSLog(@"Looking for user with tag name: %@", tagName);
#endif
        if (tagName.length > 0) {
            // if a popover is not already created then store the CGRect from which it should appear
            if (CGRectIsEmpty(connectionPopoverInitFrame)) {
                UITextRange *textRange = [textView textRangeFromPosition:textView.beginningOfDocument toPosition:textView.endOfDocument];
                
                connectionPopoverInitFrame = [textView firstRectForRange:(UITextRange *)textRange];
#ifdef DEBUG
                NSLog(@"Gotta show the connections popover from %@", NSStringFromCGRect(connectionPopoverInitFrame));
#endif
            }
            [self.apiCaller performRestRequest:TeamieUserConnectionsRequest withParams:[NSDictionary dictionaryWithObjectsAndKeys:tagName, @"containsName", nil]];
        }
        else {
            // there's no tagname to search for, therefore dismiss the user list popover
            [self.wePopoverController dismissPopoverAnimated:YES];
            [self.wePopoverController.delegate popoverControllerDidDismissPopover:self.wePopoverController];
        }
    }
    
    if (self.attachmentLink == nil) {
        if ([text length] > 7) {    // to check that the string is at least having "http://"
            // break the newly added text into words
            NSCharacterSet *delimiterCharacterSet = [NSCharacterSet whitespaceAndNewlineCharacterSet];
            NSArray *allWords = [text componentsSeparatedByCharactersInSet:delimiterCharacterSet];
            for (id word in allWords) {
                if ([word hasPrefix:@"http://"] || [word hasPrefix:@"https://"]) {
#ifdef DEBUG
                    NSLog(@"Link detected in UITextView: %@", word);
#endif
                    [self updateAttachmentLink:word];
                }
            }
        }
        else {
            // Do nothing as of now
        }
    }
    
    return YES;
}

-(void)textViewDidChange:(UITextView *)textView {
    
    [self.postThoughtButton setEnabled:([textView.text length] > 0)];
    
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    
    if (self.classroomSelector.isHidden == NO) {
        // animate the UIPickerView out of the frame
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationDelay:0.0];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        
        CGRect tempFrame = self.classroomSelector.frame;
        tempFrame.origin.y += self.classroomSelector.frame.size.height;
        self.classroomSelector.frame = tempFrame;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) { // animate the thoughtActions Bar also
            tempFrame = self.thoughtActionsBar.frame;
            tempFrame.origin.y += self.classroomSelector.frame.size.height;
            self.thoughtActionsBar.frame = tempFrame;
        }
        
        [UIView commitAnimations];
        
        // hide the classroomselector after 0.5 secs
        [self.classroomSelector performSelector:@selector(setHidden:) withObject:self.classroomSelector afterDelay:0.5];
    }
}

#pragma mark - PopupMenuDelegate Methods

- (void)menuDidExitWithData:(id)data {
    // this method is invoked when user taps on any name on the popover
    NSLog(@"The data with which the connections menu quit %@", data);
    
    /*UserProfile * profData = ((TTTeamieUserProfileItem*)data).profileData;
    
    // get current cursor position
    NSRange cursorPos = [self.thoughtTextView selectedRange];
    if (cursorPos.location != NSNotFound) {
        // find the position of @ character immediately before the current cursor position
        NSRange atRange = [self.thoughtTextView.text rangeOfString:@"@" options:NSBackwardsSearch range:NSMakeRange(0, cursorPos.location)];
        if (atRange.location != NSNotFound) {
            self.thoughtTextView.text = [self.thoughtTextView.text stringByReplacingCharactersInRange:NSMakeRange(atRange.location + 1, cursorPos.location - atRange.location - 1) withString:[NSString stringWithFormat:@"%@ [%@] ", profData.fullname, profData.email]];
        }
    }*/
    
    [self.wePopoverController dismissPopoverAnimated:YES];
    // invoke the didDismiss delegate method to perform cleanup actions
    [self.wePopoverController.delegate popoverControllerDidDismissPopover:self.wePopoverController];
}

#pragma mark - WEPopoverControllerDelegate Methods

- (void)popoverControllerDidDismissPopover:(WEPopoverController *)popoverController {
    self.wePopoverController = nil;
    connectionPopoverInitFrame = CGRectZero;
}

- (BOOL)popoverControllerShouldDismissPopover:(WEPopoverController *)popoverController {
    return YES;
}

- (WEPopoverContainerViewProperties *)improvedContainerViewProperties {
	
	WEPopoverContainerViewProperties *props = [WEPopoverContainerViewProperties alloc];
	NSString *bgImageName = nil;
	CGFloat bgMargin = 0.0;
	CGFloat bgCapSize = 0.0;
	CGFloat contentMargin = 4.0;
	
	bgImageName = @"popoverBg.png";
	
	// These constants are determined by the popoverBg.png image file and are image dependent
	bgMargin = 13; // margin width of 13 pixels on all sides popoverBg.png (62 pixels wide - 36 pixel background) / 2 == 26 / 2 == 13
	bgCapSize = 31; // ImageSize/2  == 62 / 2 == 31 pixels
	
	props.leftBgMargin = bgMargin;
	props.rightBgMargin = bgMargin;
	props.topBgMargin = bgMargin;
	props.bottomBgMargin = bgMargin;
	props.leftBgCapSize = bgCapSize;
	props.topBgCapSize = bgCapSize;
	props.bgImageName = bgImageName;
	props.leftContentMargin = contentMargin;
	props.rightContentMargin = contentMargin - 1; // Need to shift one pixel for border to look correct
	props.topContentMargin = contentMargin;
	props.bottomContentMargin = contentMargin;
	
	props.arrowMargin = 4.0;
	
	props.upArrowImageName = @"popoverArrowUp.png";
	props.downArrowImageName = @"popoverArrowDown.png";
	props.leftArrowImageName = @"popoverArrowLeft.png";
	props.rightArrowImageName = @"popoverArrowRight.png";
	return props;
}

#pragma mark Keyboard Notification methods
- (void)keyboardWillShow:(NSNotification *)note {
        
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [TeamieGlobals moveControl:self.thoughtActionsBar forKeyboard:note up:YES distance:72.0 inView:self.view];
        [TeamieGlobals moveControl:self.classroomSelector forKeyboard:note up:YES distance:72.0 inView:self.view];
        [TeamieGlobals moveControl:self.attachmentPreview forKeyboard:note up:YES distance:72.0 inView:self.view];
    }
    
}

- (void)keyboardWillHide:(NSNotification *)note {
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        [TeamieGlobals moveControl:self.thoughtActionsBar forKeyboard:note up:NO distance:72.0 inView:self.view];
        [TeamieGlobals moveControl:self.classroomSelector forKeyboard:note up:NO distance:72.0 inView:self.view];
        [TeamieGlobals moveControl:self.attachmentPreview forKeyboard:note up:NO distance:72.0 inView:self.view];
    }
    
}


#pragma mark Landscape Orientation methods
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)orientation duration:(NSTimeInterval)duration {
    CGRect actionsBar = self.thoughtActionsBar.frame;
    CGRect thoughtText = self.thoughtTextView.frame;
    CGRect thoughtFlag = self.thoughtQuestionFlag.frame;
    CGRect thoughtLabel = self.thoughtQuestionFlagLabel.frame;
    CGRect classroomSel = self.selectedClassroom.frame;
    CGRect classroomList = self.classroomSelector.frame;
    CGRect userPic = self.userDisplayPic.frame;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        
        if (self.interfaceOrientation == UIInterfaceOrientationLandscapeLeft || self.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
            actionsBar.size.height = 32;
            actionsBar.origin.y = 0;
            actionsBar.origin.x = 0;
            
            userDisplayPic.hidden = YES;
            
            thoughtText.origin.x = 90;
            thoughtText.origin.y = 45;
            
            classroomList.origin.x = 0;
            classroomList.origin.y = 89;
            
            classroomSel.size.height = 28;
            selectedClassroom.titleLabel.font = [UIFont boldSystemFontOfSize:13];
            
            thoughtFlag.origin.x = 10;
            thoughtFlag.origin.y = 45;
            
            thoughtLabel.origin.x = 10;
            thoughtLabel.origin.y = 88;
        }
        
        if (self.interfaceOrientation == UIInterfaceOrientationPortrait) {
            actionsBar.size.height = 44;
            actionsBar.origin.x = 0;
            
            actionsBar.origin.y = ([TeamieGlobals hasFourInchDisplay])?244.0:156.0;
            
            userDisplayPic.hidden = NO;
            userPic.size.width = 70;
            userPic.size.height = 70;
            
            thoughtText.origin.x = 90;
            thoughtText.origin.y = 10;
            
            classroomList.origin.x = 0;
            classroomList.origin.y = ([TeamieGlobals hasFourInchDisplay])?288:200;
            
            classroomSel.size.height = 33;
            selectedClassroom.titleLabel.font = [UIFont boldSystemFontOfSize:15];
            
            thoughtFlag.origin.x = 10;
            thoughtFlag.origin.y = 89;
            
            thoughtLabel.origin.x = 10;
            thoughtLabel.origin.y = 133;
        }

        self.thoughtActionsBar.frame = actionsBar;
        self.thoughtTextView.frame = thoughtText;
        self.classroomSelector.frame = classroomList;
        self.selectedClassroom.frame = classroomSel;
        self.thoughtQuestionFlag.frame = thoughtFlag;
        self.userDisplayPic.frame = userPic;
        self.thoughtQuestionFlagLabel.frame = thoughtLabel;
    }
}

- (void)getListOfObjectsInContext:(NSString*)inEntity {
    NSFetchRequest* fetch = [[NSFetchRequest alloc] init];
    [fetch setEntity:[NSEntityDescription entityForName:inEntity inManagedObjectContext:self.context]];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES];
    NSArray* sortDescriptors = @[sortDescriptor];
    [fetch setSortDescriptors:sortDescriptors];
    [fetch setIncludesPropertyValues:NO]; //only fetch the managedObjectId
    
    NSError* error = nil;
    NSArray* items = [self.context executeFetchRequest:fetch error:&error];
    [self updateAttachmentPoll:items];
}

- (void)deleteExistingPollOptions {
    NSFetchRequest * allPollOptions = [[NSFetchRequest alloc] init];
    [allPollOptions setEntity:[NSEntityDescription entityForName:@"ShareBoxPollOptions" inManagedObjectContext:self.context]];
    [allPollOptions setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    
    NSError * error = nil;
    NSArray * options = [self.context executeFetchRequest:allPollOptions error:&error];
    //error handling goes here
    for (NSManagedObject * option in options) {
        [self.context deleteObject:option];
    }
    NSError *saveError = nil;
    [self.context save:&saveError];
//    NSUserDefaults user
}


- (NSEntityDescription *)entity {
    if(_entity == nil) {
        _entity = [NSEntityDescription entityForName:@"ShareBoxPollOptions" inManagedObjectContext:self.context];
    }
    
    return _entity;
}

- (NSManagedObjectContext *)context {
    
    if(_context == nil) {
        TeamieAppDelegate *del = [[UIApplication sharedApplication] delegate];
        _context = del.managedObjectContext;
    }
    
    return _context;
}

#pragma mark - WSCoachMarksViewDelegate methods

- (void)coachMarksViewWillCleanup:(WSCoachMarksView *)coachMarksView {
    // Done with showing the coachmarks. Now make the keyboard appear. The reason for showing the keyboard after the coachmarks is bcos otherwise the help text gets hidden beneath the keyboard.
    [self.thoughtTextView becomeFirstResponder];
}

@end