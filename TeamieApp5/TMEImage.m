//
//  ImageObject.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 11/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "TMEImage.h"

@implementation TMEImage

+ (NSValueTransformer *)pathJSONTransformer {
    return [NSValueTransformer valueTransformerForName:MTLURLValueTransformerName];
}

@end
