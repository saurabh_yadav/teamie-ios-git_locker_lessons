//
//  PKRevealController+Hide.m
//  TeamieApp5
//
//  Created by Wei Wenbo on 23/7/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "PKRevealController+Hide.h"

@implementation PKRevealController (Hide)
bool status = NO;
- (void)hideStatusBar:(BOOL)hide
{
    status = hide;
    [self setNeedsStatusBarAppearanceUpdate];
}

- (BOOL)prefersStatusBarHidden
{
    return status;
}

- (UIStatusBarAnimation)preferredStatusBarUpdateAnimation{
    return UIStatusBarAnimationFade;
}


@end
