//
//  TMENotification.h
//  TeamieApp5
//
//  Created by Ramchander Krishna on 3/15/12.
//  Copyright (c) 2012 Teamie. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TMEModel.h"

@class UserLoginData;

@interface TMENotification : TMEModel

@property (nonatomic, retain) NSString * body;
@property (nonatomic, retain) NSNumber * msgid;
@property (nonatomic, retain) NSString * nohtml;
@property (nonatomic, retain) NSNumber * sent;
@property (nonatomic, getter=isRead) BOOL readStatus;

@property (nonatomic, readonly) NSMutableAttributedString *mainText;
@property (nonatomic, readonly) NSString *subText;
@property (nonatomic, readonly) BulletinNotificationType notificationType;
@property (nonatomic, readonly) NSString *entityID;
@property (nonatomic, readonly) UIImage *postImage;

@end
