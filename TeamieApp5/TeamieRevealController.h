//
//  TeamieRevealController.h
//  TeamieApp5
//
//  Created by Nalin Ilango on 23/9/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "PKRevealController.h"

@protocol TMERevealDelegate <NSObject>

@required
- (void)setParentRevealController:(UIViewController *)parent;

@optional
+ (void)sendPasteboardUrlToShareBox:(NSNotification*)notification;
@end

@interface TeamieRevealController : PKRevealController

@property (nonatomic, strong, readonly) NSNumber *unreadNotificationsCount;

@end
