//
//  TeamieBadgedBarButtonItem.m
//  TeamieApp5
//
//  Created by Nalin Ilango on 9/9/14.
//  Copyright (c) 2014 Teamie. All rights reserved.
//

#import "UIBadgedBarButtonItem.h"

#import "UIView+TMEEssentials.h"
#import "UILabelWithPadding.h"

@interface UIBadgedBarButtonItem()

@property (nonatomic, weak) UILabelWithPadding * badgeLabel;

@end

@implementation UIBadgedBarButtonItem

- (instancetype)initWithTargetAndImage:(id)target action:(SEL)action forControlEvents:(UIControlEvents)event image:(UIImage *)image {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0, 0, 25, 25)];
    [button addTarget:target action:action forControlEvents:event];
    
    UILabelWithPadding *temp = [UILabelWithPadding new];
    [button insertSubview:temp atIndex:10];
    self.badgeLabel = temp;
    self.badgeLabel.font = [TeamieGlobals appFontFor:@"notificationsBadgeText"];
    self.badgeLabel.textColor = [TeamieUIGlobalColors notificationBadgeTextColor];
    self.badgeLabel.backgroundColor = [TeamieUIGlobalColors notificationBadgeBackgroundColor];
    self.badgeLabel.textAlignment = NSTextAlignmentCenter;
    self.badgeLabel.edgeInsets = UIEdgeInsetsMake(0, 3, 0, 3);
    self.badgeLabel.layer.cornerRadius = 2.0;
    self.badgeLabel.layer.masksToBounds = YES;
    self.badgeLabel.hidden = YES;
    
    return [super initWithCustomView:button];
}

- (void)updateBadgeValue:(int)value {
    [self updateBadgeValue:value refresh:FALSE];
}

- (void)updateBadgeValue:(int)value refresh:(BOOL)refresh {
    int badgeValue = [self.badgeLabel.text intValue];

    if (refresh) {
        badgeValue = value;
    } else {
        badgeValue += value;
    }

    NSString *badgeText = [NSString stringWithFormat:@"%d", badgeValue];
    [self.badgeLabel setText:badgeText];

    CGSize labelSize = [badgeText sizeWithFont:[TeamieGlobals appFontFor:@"notificationsBadgeText"]];
    self.badgeLabel.frame = CGRectMake(17, -5, labelSize.width + 6, labelSize.height);

    if (badgeValue <= 0) {
        if(![self.badgeLabel isHidden]) {
            [self.badgeLabel setHidden:TRUE];
        }
    } else {
        if ([self.badgeLabel isHidden]) {
            [self.badgeLabel setHidden:FALSE];
        }
    }
}

@end
